/*
    Plee the Bear - Image Cutter

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file application.hpp
 * \brief The main class.
 * \author Julien Jorge
 */
#ifndef __PTB_IC_APPLICATION_HPP__
#define __PTB_IC_APPLICATION_HPP__

#include <claw/application.hpp>
#include <claw/image.hpp>
#include <claw/math.hpp>

#include <string>
#include <set>

namespace ptb
{
  namespace ic
  {
    /**
     * \brief The main class.
     *
     * The program accept the following parameters :
     * - --output-as-layer Output the declaration for a decoration layer,
     * - --output-folder Path to the directory where we save the images,
     * - --output-format The type of the output files,
     * - -x X-coordinate of the picture in the level,
     * - -y Y-coordinate of the picture in the level,
     * - -max-x maximum size of the sub images on the X-coordinates,
     * - -max-y maximum size of the sub images on the Y-coordinates,
     * - --file-prefix the prefix of the name of the files,
     * - --help Print help and exit.
     * \author Julien Jorge
     */
    class application:
      public claw::application
    {
    private:
      /**
       * \brief informations on the sub images.
       */
      struct sub_image_info
      {
  /** \brief The position and the size of the sub image in the source
      image. */
  claw::math::rectangle<unsigned int> box;

  /** \brief The path of the file in which we save the sub image. */
  std::string path;

      }; // struct sub_image_info

    public:
      application( int& argc, char** &argv );

      int run();

    private:
      void help() const;
      void check_arguments( int& argc, char** &argv );
      void store_files( int& argc, char** &argv );

      void process_file( const std::string& name );
      void process_file( std::istream& is, const std::string& name );

      claw::graphic::image* load_image( std::istream& is ) const;
      void split_image( const claw::graphic::image& img,
      const std::string& name ) const;

      void save_image( const claw::graphic::image& img,
           const sub_image_info& infos ) const;

      void output_as_layer( const claw::graphic::image& img,
          const std::vector<sub_image_info>& infos ) const;

      void get_sub_images_info( const claw::graphic::image& img,
        std::vector<sub_image_info>& infos,
        const std::string& name ) const;

      void factorize( unsigned int n, std::vector<unsigned int>& vals,
          unsigned int max ) const;

      void extract_file_name( const std::string& file_name,
            std::string& name ) const;

      void format_file_name( std::string& str ) const;

    private:
      /** \brief The files to compile. */
      std::set<std::string> m_files;

      /** \brief Tell if we should quit immediatly. */
      bool m_quit;

      /** \brief The position of the picture in the layer. */
      claw::math::coordinate_2d<unsigned int> m_pos;

      /** \brief Maximum size of the sub images. */
      claw::math::coordinate_2d<unsigned int> m_max;

      /** \brief Tell if we must output the declaration of a decoration
    layer. */
      bool m_output_as_layer;

      /** \brief Path to the output folder. */
      std::string m_output_folder;

      /** \brief The prefix of the generated images files. */
      std::string m_file_prefix;

      /** \brief The type of the ouput files. */
      std::string m_output_type;

    }; // class application
  } // namespace ic
} // namespace ptb

#endif // __PTB_IC_APPLICATION_HPP__

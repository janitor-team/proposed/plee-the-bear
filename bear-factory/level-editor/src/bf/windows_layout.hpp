/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/windows_layout.hpp
 * \brief A class that groups all windows common to all levels.
 * \author Julien Jorge
 */
#ifndef __BF_WINDOWS_LAYOUT_HPP__
#define __BF_WINDOWS_LAYOUT_HPP__

#include "bf/level.hpp"

namespace bf
{
  class item_class_pool;
  class main_frame;
  class properties_frame;
  class layer_list_frame;
  class ingame_view_frame;

  /**
   * \brief A class that groups all windows common to all levels.
   * \author Julien Jorge
   */
  class windows_layout
  {
  public:
    typedef claw::wrapped_iterator
    < ingame_view_frame,
      std::set<ingame_view_frame*>::iterator,
      claw::dereference<ingame_view_frame>
    >::iterator_type iterator;

  public:
    windows_layout
    ( const item_class_pool& class_pool, main_frame& mf, properties_frame& pf,
      layer_list_frame& llf );

    const item_class_pool& get_item_class_pool() const;

    main_frame& get_main_frame();
    properties_frame& get_properties_frame();
    layer_list_frame& get_layer_list_frame();

    ingame_view_frame* get_current_level_view();
    bool set_current_level_view( ingame_view_frame& view );
    void add_level_view( ingame_view_frame& view );
    void remove_level_view( ingame_view_frame& view );

    iterator begin();
    iterator end();

  private:
    /** \brief The classes of the items. */
    const item_class_pool& m_class_pool;

    /** \brief The main window of the program. */
    main_frame& m_main_frame;

    /** \brief The window displaying the properties of an item. */
    properties_frame& m_properties_frame;

    /** \brief The list of the layers in the current level. */
    layer_list_frame& m_layer_list;

    /** \brief All level windows. */
    std::set<ingame_view_frame*> m_level_view;

    /** \brief Active level window. */
    ingame_view_frame* m_current_level_view;

  }; // class windows_layout
} // namespace bf

#endif // __BF_WINDOWS_LAYOUT_HPP__

/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level_file_xml_writer.hpp
 * \brief A class for writing source level files.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_FILE_XML_WRITER_HPP__
#define __BF_LEVEL_FILE_XML_WRITER_HPP__

#include <iostream>
#include <wx/xml/xml.h>

namespace bf
{
  class item_class;
  class item_instance;
  class layer;
  class level;

  /**
   * \brief A class for writing source level files.
   * \author Julien Jorge
   */
  class level_file_xml_writer
  {
  public:
    void save( std::ostream& f, const level& lvl ) const;

  private:
    void save_layer( std::ostream& os, const layer& the_layer ) const;
    void save_items( std::ostream& os, const layer& item ) const;
    void save_priority( std::ostream& os, const layer& item ) const;
    void save_item( std::ostream& os, const item_instance& item ) const;

  }; // class level_file_xml_writer
} // namespace bf

#endif // __BF_LEVEL_FILE_XML_WRITER_HPP__

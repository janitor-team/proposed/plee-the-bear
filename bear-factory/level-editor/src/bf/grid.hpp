/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/grid.hpp
 * \brief This class represents a grid.
 * \author Sebastien Angibaud
 */
#ifndef __BF_GRID_HPP__
#define __BF_GRID_HPP__

#include <wx/wx.h>
#include <wx/dnd.h>

namespace bf
{
  /** \brief A grid. */
  class grid
  {
  public:
    grid();

    void set_offset( const wxSize& offset);
    void set_step( const wxSize& step);
    void set_magnetism_active(bool value);
    void set_magnetism_force(unsigned int force);

    const wxSize& get_offset() const;
    const wxSize& get_step() const;
    bool get_magnetism_active() const;
    unsigned int get_magnetism_force() const;

  private:
    /** \brief The offset. */
    wxSize m_offset;

    /** \brief The step of the grid. */
    wxSize m_step;

    /** \brief Indicates if the magnetism is activate. **/
    bool m_magnetism_active;

    /** \brief Indicates if the magnetism force. **/
    unsigned int m_magnetism_force;

    /** \brief The default x-coordinate of the step. */
    static const unsigned int s_step_x_default;

    /** \brief The The default x-coordinate of the step. */
    static const unsigned int s_step_y_default;

    /** \brief The default magnetism force. */
    static const unsigned int s_magnetism_force_default;

  }; // class grid

} // namespace bf

#endif // __BF_GRID_HPP__


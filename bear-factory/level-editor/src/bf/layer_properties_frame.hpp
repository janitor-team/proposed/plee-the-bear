/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/layer_properties_frame.hpp
 * \brief The window showing the properties of a layer.
 * \author Julien Jorge
 */
#ifndef __BF_LAYER_PROPERTIES_FRAME_HPP__
#define __BF_LAYER_PROPERTIES_FRAME_HPP__

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <string>

namespace bf
{
  class layer;

  /**
   * \brief The layer_properties window of our program.
   * \author Julien Jorge
   */
  class layer_properties_frame:
    public wxDialog
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_FIT_LEVEL_SIZE
      }; // enum control_id

  public:
    layer_properties_frame( wxWindow* parent );

    bool get_layer_fits_level() const;
    unsigned int get_layer_width() const;
    unsigned int get_layer_height() const;
    std::string get_layer_class_name() const;

    void fill_from( const layer& lay );

  private:
    void create_controls();
    void create_member_controls();
    void create_sizer_controls();

    void on_ok( wxCommandEvent& event );
    void on_fit_level_size( wxCommandEvent& event );

  private:
    /** \brief Tell if the layer size is the same than the level size. */
    wxCheckBox* m_fit_level;

    /** \brief The text control in which we configure the width of the layer. */
    wxSpinCtrl* m_width;

    /** \brief The text control in which we configure the height of the
        layer. */
    wxSpinCtrl* m_height;

    /** \brief The text control in which we configure the name of the class of
        the layer. */
    wxChoice* m_class_name;

    /** \brief Minimum width of the layer. */
    static const unsigned int s_min_width;

    /** \brief Minimum height of the layer. */
    static const unsigned int s_min_height;

    DECLARE_EVENT_TABLE()

  }; // class layer_properties_frame
} // namespace bf

#endif // __BF_LAYER_PROPERTIES_FRAME_HPP__

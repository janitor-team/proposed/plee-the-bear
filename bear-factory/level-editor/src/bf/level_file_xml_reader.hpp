/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level_file_xml_reader.hpp
 * \brief A class for reading source level files.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_FILE_XML_READER_HPP__
#define __BF_LEVEL_FILE_XML_READER_HPP__

#include <iostream>
#include <wx/xml/xml.h>

namespace bf
{
  class gui_level;
  class item_class_pool;
  class item_instance;
  class layer;
  class type_field;

  /**
   * \brief A class for reading source level files.
   * \author Julien Jorge
   */
  class level_file_xml_reader
  {
  public:
    gui_level* load
    ( const item_class_pool& pool, const wxString& file_path ) const;

  private:
    gui_level*
    load_level( const item_class_pool& pool, const wxXmlNode* node ) const;
    void load_layers
    ( const item_class_pool& pool, gui_level& lvl,
      const wxXmlNode* node ) const;
    void load_layer
    ( const item_class_pool& pool, gui_level& lvl,
      const wxXmlNode* node ) const;

    void load_layer_content
    ( const item_class_pool& pool, layer& lay, const wxXmlNode* node ) const;

    void load_items
    ( const item_class_pool& pool, layer& lay, const wxXmlNode* node ) const;
    void load_priorities( layer& lay, const wxXmlNode* node ) const;

    void load_item
    ( const item_class_pool& pool, layer& lay, const wxXmlNode* node ) const;
    void load_fields( item_instance& item, const wxXmlNode* node ) const;

  }; // class level_file_xml_reader
} // namespace bf

#endif // __BF_LEVEL_FILE_XML_READER_HPP__

/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/gui_level.hpp
 * \brief A level in the graphical user interface.
 * \author Julien Jorge
 */
#ifndef __BF_GUI_LEVEL_HPP__
#define __BF_GUI_LEVEL_HPP__

#include "bf/item_selection.hpp"
#include "bf/level.hpp"

#include <vector>

#include <claw/non_copyable.hpp>

namespace bf
{
  /**
   * \brief A level in the graphical user interface.
   * \author Julien Jorge
   */
  class gui_level:
    public level,
    claw::pattern::non_copyable
  {
  public:
    gui_level
    ( const std::string& name, unsigned int width, unsigned int height,
      const std::string& mus );

    bool has_selection( unsigned int layer_index ) const;
    bool has_selection() const;

    const item_selection& get_selection( unsigned int layer_index ) const;
    const item_selection& get_selection() const;

    item_instance* get_main_selection( unsigned int layer_index ) const;
    item_instance* get_main_selection() const;

    void set_selection( unsigned int layer_index, const item_selection& s );

    bool item_is_selected
    ( unsigned int layer_index, item_instance const* item ) const;
    bool item_is_selected( item_instance const* item ) const;

    bool item_is_main_selection
    ( unsigned int layer_index, item_instance const* item ) const;
    bool item_is_main_selection( item_instance const* item ) const;

    void add_to_selection( unsigned int layer_index, const item_selection& s );
    void add_to_selection
    ( unsigned int layer_index, item_instance* item,
      bool main_selection = false );

    void add_to_selection( item_instance* item, bool main_selection = false );

    void remove_from_selection( unsigned int layer_index, item_instance* item );
    void
    remove_from_selection( unsigned int layer_index, const item_selection& s );
    void remove_from_selection( item_instance* item );

    void clear_selection( unsigned int layer_index );
    void clear_selection();

    void remove_item( unsigned int layer_index, item_instance* item );

    bool layer_is_visible( unsigned int layer_index ) const;

    void set_layer_visibility( unsigned int layer_index, bool b );

    void set_active_layer( unsigned int layer_index );
    layer& get_active_layer() const;

    unsigned int get_active_layer_index() const;

    layer& add_layer
    ( const std::string& class_name, bool fit_level = true,
      unsigned int width = 1, unsigned int height = 1 );
    void add_layer( layer* lay, unsigned int layer_index );
    layer* remove_layer( unsigned int layer_index );

    void move_backward( unsigned int layer_index );
    void move_forward( unsigned int layer_index );

  private:
    /** \brief The visibility of the layers. */
    std::vector<bool> m_layer_visibility;

    /** \brief The selection in each layer. */
    std::vector<item_selection> m_selection_by_layer;

    /** \brief Index of the layer on which we are working. */
    unsigned int m_active_layer;

  }; // class gui_level
} // namespace bf

#endif // __BF_GUI_LEVEL_HPP__

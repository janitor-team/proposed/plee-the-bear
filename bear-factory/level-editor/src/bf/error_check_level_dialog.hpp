/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/error_check_level_dialog.hpp
 * \brief \brief This window displays the errors of a level.
 * \author Julien Jorge
 */
#ifndef __BF_ERROR_CHECK_LEVEL_DIALOG_HPP__
#define __BF_ERROR_CHECK_LEVEL_DIALOG_HPP__

#include <wx/frame.h>
#include <wx/listctrl.h>
#include <wx/textctrl.h>

#include <vector>

namespace bf
{
  class item_instance;
  class item_check_result;
  class layer;
  class layer_check_result;
  class level_check_result;

  /**
   * \brief This window displays the errors of a level.
   * \author Sebastien Angibaud
   */
  class error_check_level_dialog:
    public wxFrame
  {
  private:
      /** \brief The identifiers of the controls. */
      enum control_id
        {
          ID_ERROR_LIST
        }; // enum control_id

  public:
    error_check_level_dialog( wxWindow* parent, wxWindowID id = wxID_ANY );

    void set_errors( const level_check_result& errors );

  private:
    void fill_layer_check_result
    ( layer* lay, const layer_check_result& result );
    void fill_item_check_result
    ( item_instance* item, const item_check_result& result,
        const wxString& lay_name );

    void create_controls();
    void create_sizers();
    void adjust_last_column_size() const;

    void on_close(wxCloseEvent& event);
    void on_size(wxSizeEvent& event);
    void on_error_selected(wxListEvent& event);

  private:
    /** \brief The control that displays the list of bad items. */
    wxListView* m_items_list;

    /** \brief The area where the complete message is displayed. */
    wxTextCtrl* m_details;

    /** \brief The items associated with each line of the list. */
    std::vector<item_instance*> m_items;

    DECLARE_EVENT_TABLE()

  }; // class error_check_level_dialog
} // namespace bf

#endif // __BF_ERROR_CHECK_LEVEL_DIALOG_HPP__

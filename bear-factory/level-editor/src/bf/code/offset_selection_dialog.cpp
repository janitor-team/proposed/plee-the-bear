/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/offset_selection_dialog.cpp
 * \brief Implementation of the bf::offset_selection_dialog class.
 * \author Julien Jorge
 */
#include "bf/offset_selection_dialog.hpp"

#include <wx/sizer.h>
#include <wx/stattext.h>

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param parent The parent window.
 */
bf::offset_selection_dialog::offset_selection_dialog( wxWindow& parent )
  : wxDialog( &parent, wxID_ANY, wxString(_("Offset")) )
{
  create_controls();
  create_sizers();

  Fit();
} // offset_selection_dialog::offset_selection_dialog()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the offset on the x-axis.
 */
double bf::offset_selection_dialog::get_x() const
{
  return m_x->GetValue();
} // offset_selection_dialog::get_x()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the offset on the y-axis.
 */
double bf::offset_selection_dialog::get_y() const
{
  return m_y->GetValue();
} // offset_selection_dialog::get_y()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the controls.
 */
void bf::offset_selection_dialog::create_controls()
{
  m_x = new spin_ctrl<double>(this);
  m_x->SetRange
    ( -std::numeric_limits<double>::max(), std::numeric_limits<double>::max() );
  m_x->SetValue(0);

  m_y = new spin_ctrl<double>(this);
  m_y->SetRange
    ( -std::numeric_limits<double>::max(), std::numeric_limits<double>::max() );
  m_y->SetValue(0);
} // offset_selection_dialog::create_controls()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the sizers.
 */
void bf::offset_selection_dialog::create_sizers()
{
  wxBoxSizer* sizer = new wxBoxSizer( wxVERTICAL );

  wxStaticBoxSizer* s_sizer =
    new wxStaticBoxSizer( wxHORIZONTAL, this, _("Offset"));

  s_sizer->Add( new wxStaticText(this, wxID_ANY, _("Horizontal:")), 0 );
  s_sizer->Add( m_x, 0 );
  s_sizer->Add( new wxStaticText(this, wxID_ANY, _("Vertical:")), 0 );
  s_sizer->Add( m_y, 0 );

  sizer->Add( s_sizer, 1, wxEXPAND | wxALL, 5 );

  sizer->Add
    ( CreateStdDialogButtonSizer(wxOK | wxCANCEL), 0, wxALL | wxCENTER, 5 );

  SetSizer(sizer);
} // offset_selection_dialog::create_sizers()

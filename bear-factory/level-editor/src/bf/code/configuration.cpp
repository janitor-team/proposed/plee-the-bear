/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/configuration.cpp
 * \brief Implementation of the bf::configuration class.
 * \author Julien Jorge
 */
#include "bf/configuration.hpp"

#include "bf/path_configuration.hpp"

#include <boost/filesystem/convenience.hpp>
#include <fstream>
#include <sstream>

/*----------------------------------------------------------------------------*/
const std::string bf::configuration::s_config_file_name = "level-editor.config";

const char bf::configuration::s_section_left = '[';
const char bf::configuration::s_section_right = ']';
const char bf::configuration::s_comment = '#';
const char bf::configuration::s_field_assign = '=';

const std::string bf::configuration::s_main_frame_section = "main_frame";
const std::string
bf::configuration::s_properties_frame_section = "properties_frame";
const std::string
bf::configuration::s_layer_list_frame_section = "layer_list_frame";
const std::string
bf::configuration::s_item_class_pool_frame_section = "item_class_pool_frame";
const std::string
bf::configuration::s_level_frame_section = "level_frame";

const std::string bf::configuration::s_x_field = "x";
const std::string bf::configuration::s_y_field = "y";
const std::string bf::configuration::s_width_field = "width";
const std::string bf::configuration::s_height_field = "height";
const std::string bf::configuration::s_visible_field = "visible";

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bf::configuration::configuration()
{
  load();
} // configuration::configuration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the configuration.
 */
void bf::configuration::load()
{
  if ( create_config_file() )
    {
      std::string path
        ( path_configuration::get_instance().get_config_directory()
          + s_config_file_name );

      std::ifstream f( path.c_str() );

      if (f)
        {
          claw::configuration_file config(f);

          main_rect = read_rect( config, s_main_frame_section );
          properties_visible = read_bool( config, s_properties_frame_section,
                                          s_visible_field, true );
          layer_list_visible = read_bool( config, s_layer_list_frame_section,
                                          s_visible_field, true );
          item_class_pool_visible =
            read_bool( config, s_item_class_pool_frame_section,
                       s_visible_field, true );

          default_level_window_size =
            read_size( config, s_level_frame_section );
        }
    }
} // configuration::load()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save the configuration.
 */
void bf::configuration::save() const
{
  if ( create_config_file() )
    {
      std::string path
        ( path_configuration::get_instance().get_config_directory()
          + s_config_file_name );

      std::ofstream f( path.c_str() );

      if (f)
        {
          f << '\n' << s_section_left << s_main_frame_section << s_section_right
            << '\n' << s_comment << " Position and size of the main window\n";
          write_rect( f, main_rect );
          f << '\n';

          f << s_comment << " Visibility of the item properties window\n"
            << s_visible_field << ' ' << s_field_assign << ' '
            << properties_visible << "\n\n";

          f << s_comment << " Visibility of the layer list window\n"
            << s_visible_field << ' ' << s_field_assign << ' '
            << layer_list_visible << "\n\n";

          f << s_comment << " Visibility of the item class pool window\n"
            << s_visible_field << ' ' << s_field_assign << ' '
            << item_class_pool_visible << "\n\n";

          f << s_section_left << s_level_frame_section
            << s_section_right << '\n'
            << s_comment << " Size of the level frame\n";
          write_size( f, default_level_window_size );
          f << '\n';
        }
    }
} // configuration::load()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the configuration file, if it does not exists.
 * \return true if the file already exists or if it has been created.
 */
bool bf::configuration::create_config_file() const
{
  bool result = false;

  boost::filesystem::path path
    ( path_configuration::get_instance().get_config_directory()
      + s_config_file_name,
      boost::filesystem::native );

  if ( !boost::filesystem::exists( path ) )
    {
      std::ofstream f( path.string().c_str() );
      f << s_comment << " Configuration file for Bear Factory - Level editor\n";
    }

  if ( boost::filesystem::exists( path ) )
    result = !boost::filesystem::is_directory( path );

  return result;
} // configuration::create_config_file()

/*----------------------------------------------------------------------------*/
/**
 * \brief Read a wxRect from a configuration file.
 * \param config The configuration file.
 * \param section The name of the section in which we get the rectangle.
 */
wxRect bf::configuration::read_rect
( const claw::configuration_file& config, const std::string& section ) const
{
  wxRect result( wxDefaultPosition, wxDefaultSize );

  std::istringstream iss
    ( config(section, s_x_field) + ' ' + config(section, s_y_field) + ' ' +
      config(section, s_width_field) + ' ' + config(section, s_height_field) );

  iss >> result.x >> result.y >> result.width >> result.height;

  return result;
} // configuration::read_rect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Read a wxSize from a configuration file.
 * \param config The configuration file.
 * \param section The name of the section in which we get the size.
 */
wxSize bf::configuration::read_size
( const claw::configuration_file& config, const std::string& section ) const
{
  wxSize result( wxDefaultSize );

  std::istringstream iss
    ( config(section, s_width_field) + ' ' + config(section, s_height_field) );

  iss >> result.x >> result.y;

  return result;
} // configuration::read_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Read a boolean value from a configuration file.
 * \param config The configuration file.
 * \param section The name of the section in which we get the bool.
 * \param field_name The name of the field to read.
 * \param default_value The default value in case of not defined.
 */
bool bf::configuration::read_bool
( const claw::configuration_file& config, const std::string& section,
  const std::string& field_name, bool default_value ) const
{
  bool result = default_value;
  std::istringstream iss( config(section, field_name) );

  iss >> result;

  return result;
} // configuration::read_bool()

/*----------------------------------------------------------------------------*/
/**
 * \brief Write a wxRect in a file.
 * \param f The file to write in.
 * \param r The rectangle to write.
 */
void bf::configuration::write_rect( std::ostream& f, const wxRect& r ) const
{
  f << s_x_field << ' ' << s_field_assign << ' ' << r.x << '\n'
    << s_y_field << ' ' << s_field_assign << ' ' << r.y << '\n'
    << s_width_field << ' ' << s_field_assign << ' ' << r.width << '\n'
    << s_height_field << ' ' << s_field_assign << ' ' << r.height << '\n';
} // configuration::write_rect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Write a wxSize in a file.
 * \param f The file to write in.
 * \param s The size to write.
 */
void bf::configuration::write_size( std::ostream& f, const wxSize& s ) const
{
  f << s_width_field << ' ' << s_field_assign << ' ' << s.x << '\n'
    << s_height_field << ' ' << s_field_assign << ' ' << s.y << '\n';
} // configuration::write_size()

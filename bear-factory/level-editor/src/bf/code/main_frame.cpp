/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/main_frame.cpp
 * \brief Implementation of the bf::main_frame class.
 * \author Julien Jorge
 */
#include "bf/main_frame.hpp"

#include "bf/about_dialog.hpp"
#include "bf/class_tree_ctrl.hpp"
#include "bf/config_frame.hpp"
#include "bf/gui_level.hpp"
#include "bf/image_pool.hpp"
#include "bf/ingame_view_frame.hpp"
#include "bf/item_instance.hpp"
#include "bf/layer_list_frame.hpp"
#include "bf/layer_properties_frame.hpp"
#include "bf/level_editor.hpp"
#include "bf/level_file_xml_reader.hpp"
#include "bf/level_properties_frame.hpp"
#include "bf/path_configuration.hpp"
#include "bf/properties_frame.hpp"
#include "bf/windows_layout.hpp"
#include "bf/wx_facilities.hpp"

#include <wx/notebook.h>
#include <wx/filename.h>
#include <wx/toolbar.h>
#include <wx/artprov.h>

#include <claw/logger.hpp>

DECLARE_APP(bf::level_editor)

/*----------------------------------------------------------------------------*/
/**
 * \brief Default constructor.
 */
bf::main_frame::main_frame()
  : wxFrame(NULL, wxID_ANY, wxT("Bear Factory - Level editor")),
    m_windows_layout(NULL)
{
  SetSize( wxGetApp().get_config().main_rect );
  Show();

  SetSizer( new wxBoxSizer(wxVERTICAL) );

  wxNotebook* tabs =
    new wxNotebook(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNB_TOP );

  layer_list_frame* lay = new layer_list_frame( tabs );
  lay->Show( wxGetApp().get_config().layer_list_visible );
  tabs->AddPage( lay, _("Layers") );

  m_class_tree =
    new class_tree_ctrl
    ( wxGetApp().get_item_class_pool(), tabs, IDC_CLASS_LIST );
  m_class_tree->Show( wxGetApp().get_config().item_class_pool_visible );
  tabs->AddPage( m_class_tree, _("Class") );

  properties_frame* prop = new properties_frame( tabs );
  prop->Show( wxGetApp().get_config().properties_visible );
  tabs->AddPage( prop, _("Properties") );

  m_windows_layout =
    new windows_layout( wxGetApp().get_item_class_pool(), *this, *prop, *lay );

  create_menu();
  create_controls();

  GetSizer()->Add( tabs, 1, wxALL | wxEXPAND );

  prop->set_window_layout( *m_windows_layout );

  turn_level_entries( false );
} // main_frame::main_frame()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::main_frame::~main_frame()
{
  if (m_windows_layout != NULL)
    delete m_windows_layout;
} // main_frame::~main_frame()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create an emty level.
 * \param path The path of the level file in which it will be saved.
 */
void bf::main_frame::new_level( const wxString& path )
{
  level_properties_frame dlg(this);

  if ( dlg.ShowModal() == wxID_OK )
    {
      gui_level* lvl =
        new gui_level
        ( dlg.get_name(), dlg.get_width(), dlg.get_height(), dlg.get_music() );

      add_level_view( new ingame_view_frame(*m_windows_layout, lvl, path) );
    }
} // main_frame::new_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a level.
 * \param path The path to the level to load.
 */
void bf::main_frame::load_level( const wxString& path )
{
  wxLogNull no_log;

  level_file_xml_reader reader;
  gui_level* lvl = reader.load(wxGetApp().get_item_class_pool(), path);
  add_level_view( new ingame_view_frame(*m_windows_layout, lvl, path) );
} // main_frame::load_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the active level window.
 * \param view The active level window.
 */
void bf::main_frame::set_active_level( ingame_view_frame* view )
{
  turn_level_entries(view!=NULL);
} // main_frame::set_active_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Enable/disable the controls relative to the levels.
 * \param enable Tell if the controls are enabled.
 */
void bf::main_frame::turn_level_entries( bool enable )
{
  GetMenuBar()->Enable( wxID_SAVE, enable );
  GetMenuBar()->Enable( wxID_SAVEAS, enable );
  GetMenuBar()->Enable( IDM_SAVE_ALL_LEVELS, enable );
  GetMenuBar()->Enable( IDM_UPDATE_IMAGE_POOL, enable );

  GetToolBar()->EnableTool( wxID_SAVE, enable );
} // main_frame::turn_level_entries()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the menu bar.
 */
void bf::main_frame::create_menu()
{
  wxMenuBar* menu_bar = new wxMenuBar();

  menu_bar->Append(create_level_menu(), _("&Level"));
  menu_bar->Append(create_window_menu(), _("&Tab"));
  menu_bar->Append(create_help_menu(), _("&Help"));

  SetMenuBar(menu_bar);
} // main_frame::create_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the controls.
 */
void bf::main_frame::create_controls()
{
  wxToolBar* bar = CreateToolBar();

  bar->AddTool
    ( wxID_NEW, _("&New"), wxArtProvider::GetBitmap(wxART_NEW), _("New") );
  bar->AddTool
    ( wxID_OPEN, _("&Open"), wxArtProvider::GetBitmap(wxART_FILE_OPEN),
      _("Open") );
  bar->AddTool
    ( wxID_SAVE, _("&Save"), wxArtProvider::GetBitmap(wxART_FILE_SAVE),
      _("Save") );

  bar->Realize();
} // main_frame::create_controls()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the "Level" menu of the menu bar.
 */
wxMenu* bf::main_frame::create_level_menu() const
{
  wxMenu* menu = new wxMenu();

  menu->Append( wxID_NEW, _("&New..."), _("Create a new level.") );
  menu->Append( wxID_OPEN, _("&Open..."), _("Open an existing level.") );
  menu->Append( wxID_SAVE, _("&Save"), _("Save the current level.") );
  menu->Append( wxID_SAVEAS, _("Save &as..."),
                _("Save the current level under a new name.") );
  menu->Append( IDM_SAVE_ALL_LEVELS, _("Save a&ll"), _("Save all levels.") );
  menu->AppendSeparator();
  menu->Append( wxID_PREFERENCES, _("&Configuration..."),
                _("Configure the paths and programs.") );
  menu->Append( IDM_UPDATE_IMAGE_POOL, _("&Update image pool"),
                _("Scan the directory of images for new images.") );
  menu->AppendSeparator();
  menu->Append( wxID_EXIT, _("&Exit"), _("Close everything and exit.") );

  return menu;
} // main_frame::create_level_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the "Window" menu of the menu bar.
 */
wxMenu* bf::main_frame::create_window_menu() const
{
  wxMenu* menu = new wxMenu();

  menu->AppendCheckItem
    ( IDM_LAYER_LIST, _("&Layer list"), _("Display the list of layers.") );

  menu->AppendCheckItem
    ( IDM_ITEM_CLASS_POOL, _("&Item classes"),
      _("Display the classes of items.") );

  menu->AppendCheckItem( IDM_ITEM_PROPERTIES, _("Item &properties"),
                         _("Display the properties of an item.") );
  return menu;
} // main_frame::create_window_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the "Help" menu of the menu bar.
 */
wxMenu* bf::main_frame::create_help_menu() const
{
  wxMenu* menu = new wxMenu();

  menu->Append( wxID_HELP, _("&Online documentation"),
                _("Open the online documentation in your web browser.") );

  menu->AppendSeparator();

  menu->Append( wxID_ABOUT, _("&About"),
                _("Display some informations about the data.") );

  return menu;
} // main_frame::create_help_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save the configuration of the program.
 */
void bf::main_frame::save_config()
{
  wxGetApp().get_config().main_rect = GetScreenRect();

  wxGetApp().get_config().properties_visible =
    m_windows_layout->get_properties_frame().IsShown();
  wxGetApp().get_config().layer_list_visible =
    m_windows_layout->get_layer_list_frame().IsShown();
  wxGetApp().get_config().item_class_pool_visible = m_class_tree->IsShown();

  if ( m_windows_layout->get_current_level_view() != NULL )
    wxGetApp().get_config().default_level_window_size =
      m_windows_layout->get_current_level_view()->GetSize();

  wxGetApp().get_config().save();
} // main_frame::save_config()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add an ingame_view_frame to the interface.
 * \param view The view to add.
 */
void bf::main_frame::add_level_view( ingame_view_frame* view )
{
  wxSize s;

  if ( m_windows_layout->get_current_level_view() != NULL )
    s = m_windows_layout->get_current_level_view()->GetSize();
  else
    s = wxGetApp().get_config().default_level_window_size;

  view->SetSize(s);
  view->Show();
} // main_frame::add_level_view()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Configuration" menu.
 * \param event This event occured.
 */
void bf::main_frame::on_configuration_menu( wxCommandEvent& WXUNUSED(event) )
{
  wxGetApp().configure();
} // main_frame::on_configuration_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Update image pool" menu.
 * \param event This event occured.
 */
void bf::main_frame::on_update_image_pool_menu
( wxCommandEvent& WXUNUSED(event) )
{
  wxGetApp().update_image_pool();
} // main_frame::on_update_image_pool_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Item properties" in the window menu.
 * \param event This event occured.
 */
void bf::main_frame::on_item_properties_menu( wxCommandEvent& event )
{
  m_windows_layout->get_properties_frame().Show( event.IsChecked() );
} // main_frame::on_item_properties_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Layer list" in the window menu.
 * \param event This event occured.
 */
void bf::main_frame::on_layer_list_menu( wxCommandEvent& event )
{
  m_windows_layout->get_layer_list_frame().Show( event.IsChecked() );
} // main_frame::on_layer_list_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Item classes" in the window menu.
 * \param event This event occured.
 */
void bf::main_frame::on_item_class_pool_menu( wxCommandEvent& event )
{
  m_class_tree->Show( event.IsChecked() );
} // main_frame::on_item_class_pool_menu()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "New level".
 * \param event This event occured.
 */
void bf::main_frame::on_new_level( wxCommandEvent& WXUNUSED(event) )
{
  new_level( wxEmptyString );
} // main_frame::on_new_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Open level".
 * \param event This event occured.
 */
void bf::main_frame::on_open_level( wxCommandEvent& WXUNUSED(event) )
{
  wxFileName path;

  if ( m_windows_layout->get_current_level_view() != NULL )
    {
      path = m_windows_layout->get_current_level_view()->get_level_file();

      for ( windows_layout::iterator it=m_windows_layout->begin();
            path.GetPath().empty() && (it!=m_windows_layout->end()); ++it )
        path = it->get_level_file();
    }

  wxFileDialog dlg( this, _("Choose a level"), path.GetPath(), wxEmptyString,
                    _("Level files (*.lvl)|*.lvl"),
                    wxFD_OPEN | wxFD_FILE_MUST_EXIST );

  if ( dlg.ShowModal() == wxID_OK )
    try
      {
        load_level( dlg.GetPath() );
      }
    catch( std::exception& e )
      {
        wxMessageDialog msg
          ( this, std_to_wx_string( e.what() ),
            _("Error in level loading."), wxOK );

        msg.ShowModal();
      }
} // main_frame::on_open_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Save as".
 * \param event This event occured.
 */
void bf::main_frame::on_save_as( wxCommandEvent& WXUNUSED(event) )
{
  m_windows_layout->get_current_level_view()->save_as();
} // main_frame::on_save_as()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Save".
 * \param event This event occured.
 */
void bf::main_frame::on_save( wxCommandEvent& WXUNUSED(event) )
{
  m_windows_layout->get_current_level_view()->save();
} // main_frame::on_save()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Save all".
 * \param event This event occured.
 */
void bf::main_frame::on_save_all( wxCommandEvent& WXUNUSED(event) )
{
  windows_layout::iterator it;

  for ( it=m_windows_layout->begin(); it!=m_windows_layout->end(); ++it )
    it->save();
} // main_frame::on_save_all()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on "Exit".
 * \param event This event occured.
 */
void bf::main_frame::on_exit( wxCommandEvent& WXUNUSED(event) )
{
  Close();
} // main_frame::on_exit()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to the event that occurs when opening menus.
 * \param event This event occured.
 */
void bf::main_frame::on_menu_open( wxMenuEvent& WXUNUSED(event) )
{
  GetMenuBar()->Check
    ( IDM_ITEM_PROPERTIES, m_windows_layout->get_properties_frame().IsShown() );

  GetMenuBar()->Check
    ( IDM_LAYER_LIST, m_windows_layout->get_layer_list_frame().IsShown() );

  GetMenuBar()->Check( IDM_ITEM_CLASS_POOL, m_class_tree->IsShown() );
} // main_frame::on_menu_open()

/*----------------------------------------------------------------------------*/
/**
 * \brief A menu entry is highlighted.
 * \param event This event occured.
 */
void bf::main_frame::on_menu_highlight( wxMenuEvent& event )
{
  ingame_view_frame* current_view = m_windows_layout->get_current_level_view();

  if ( current_view != NULL )
    current_view->GetStatusBar()->SetStatusText
      ( GetMenuBar()->GetHelpString(event.GetMenuId()), 0 );
} // main_frame::on_menu_highlight()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to an activation of the "online doc" menu.
 * \param event This event occured.
 */
void bf::main_frame::on_online_doc( wxCommandEvent& WXUNUSED(event) )
{
  wxLaunchDefaultBrowser
    ( wxT("http://plee-the-bear.sourceforge.net/" )
      wxT("wiki/index.php/%C9diteur_de_niveaux") );
} // main_frame::on_online_doc()

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to an activation of the "about" menu.
 * \param event This event occured.
 */
void bf::main_frame::on_about( wxCommandEvent& WXUNUSED(event) )
{
  about_dialog dlg(*this);

  dlg.ShowModal();
} // main_frame::on_about()

/*----------------------------------------------------------------------------*/
/**
 * \brief Procedure called when closing the window.
 * \param event This event occured.
 */
void bf::main_frame::on_close( wxCloseEvent& event )
{
  save_config();

  bool quit = !event.CanVeto();

  if ( !quit )
    {
      quit = true;

      bool changed = false;
      windows_layout::iterator it;

      for ( it=m_windows_layout->begin();
            !changed && it!=m_windows_layout->end(); ++it)
        changed = it->is_changed();

      if ( changed )
        {
          wxMessageDialog dlg
            ( this,
              _("Some levels are not saved."
                 " Do you want to save them now?"),
              _("Level is not saved."), wxYES_NO | wxCANCEL );

          int answer = dlg.ShowModal();

          if ( answer == wxID_CANCEL )
            quit = false;
          else if ( answer == wxID_YES )
            for ( it=m_windows_layout->begin();
                  quit && it!=m_windows_layout->end(); ++it)
              quit = it->save();
        }
    }

  if ( quit )
    event.Skip();
  else
    event.Veto();
} // main_frame::on_close()

/*----------------------------------------------------------------------------*/
/**
 * \brief Procedure called when the user picks a class in the class list.
 * \param event This event occured.
 */
void bf::main_frame::on_class_selected( class_selected_event& event )
{
  ingame_view_frame* view = m_windows_layout->get_current_level_view();

  if ( view != NULL )
    view->add_item(event.get_class_name());
} // main_frame::on_class_selected()

/*----------------------------------------------------------------------------*/
BEGIN_EVENT_TABLE(bf::main_frame, wxFrame)
  EVT_BUTTON( wxID_NEW, bf::main_frame::on_new_level )
  EVT_BUTTON( wxID_OPEN, bf::main_frame::on_open_level )
  EVT_BUTTON( wxID_SAVE, bf::main_frame::on_save )

  EVT_MENU( wxID_PREFERENCES, bf::main_frame::on_configuration_menu )
  EVT_MENU( bf::main_frame::IDM_UPDATE_IMAGE_POOL,
            bf::main_frame::on_update_image_pool_menu )
  EVT_MENU( bf::main_frame::IDM_ITEM_PROPERTIES,
            bf::main_frame::on_item_properties_menu )
  EVT_MENU( bf::main_frame::IDM_LAYER_LIST,
            bf::main_frame::on_layer_list_menu )
  EVT_MENU( bf::main_frame::IDM_ITEM_CLASS_POOL,
            bf::main_frame::on_item_class_pool_menu )

  EVT_MENU( wxID_NEW, bf::main_frame::on_new_level )
  EVT_MENU( wxID_OPEN, bf::main_frame::on_open_level )
  EVT_MENU( wxID_SAVE, bf::main_frame::on_save )
  EVT_MENU( wxID_SAVEAS, bf::main_frame::on_save_as )
  EVT_MENU( bf::main_frame::IDM_SAVE_ALL_LEVELS, bf::main_frame::on_save_all )

  EVT_MENU( wxID_EXIT, bf::main_frame::on_exit )
  EVT_MENU_OPEN( bf::main_frame::on_menu_open )
  EVT_MENU_HIGHLIGHT_ALL( bf::main_frame::on_menu_highlight )

  EVT_MENU( wxID_HELP, bf::main_frame::on_online_doc )
  EVT_MENU( wxID_ABOUT, bf::main_frame::on_about )

  EVT_CLOSE( bf::main_frame::on_close )

  EVT_CLASS_SELECTED( bf::main_frame::IDC_CLASS_LIST,
                      bf::main_frame::on_class_selected )
END_EVENT_TABLE()

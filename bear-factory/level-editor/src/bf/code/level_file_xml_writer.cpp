/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/level_file_xml_writer.cpp
 * \brief Implementation of the bf::level_file_xml_writer class.
 * \author Julien Jorge
 */
#include "bf/level_file_xml_writer.hpp"

#include "bf/item_class_pool.hpp"
#include "bf/item_comparator.hpp"
#include "bf/level.hpp"
#include "bf/xml/item_instance_node.hpp"

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Save a level.
 * \param os The stream in which we write.
 * \param lvl The level to save.
 */
void bf::level_file_xml_writer::save( std::ostream& os, const level& lvl ) const
{
  os << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
     << "<level name='" << lvl.get_name() << "' width='" << lvl.get_width()
     << "' height='" << lvl.get_height()
     << "' music='" << lvl.get_music() << "'>\n";

  for (unsigned int i=0; i!=lvl.layers_count(); ++i)
    save_layer( os, lvl.get_layer(i) );

  os << "</level>\n";
} // level_file_xml_writer::save()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save a layer.
 * \param os The stream in which we write.
 * \param the_layer The layer to save.
 */
void bf::level_file_xml_writer::save_layer
( std::ostream& os, const layer& the_layer ) const
{
  os << "  <layer class_name='" << the_layer.get_class_name() << "' width='"
     << the_layer.get_width() << "' height='" << the_layer.get_height()
     << "' fit_level='" << the_layer.fits_level() << "'>\n\n";

  save_items(os, the_layer);
  save_priority(os, the_layer);

  os << "  </layer><!-- " << the_layer.get_class_name() << " -->\n\n";
} // level_file_xml_writer::save_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save the items of a layer.
 * \param os The stream in which we write.
 * \param the_layer The layer to save.
 */
void bf::level_file_xml_writer::save_items
( std::ostream& os, const layer& the_layer ) const
{
  os << "<items>\n";

  std::list<item_instance> items
    ( the_layer.item_begin_no_filter(), the_layer.item_end_no_filter() );

  items.sort( item_comparator::by_place() );

  for ( ; !items.empty(); items.pop_front() )
    {
      const item_rendering_parameters& r
        ( items.front().get_rendering_parameters() );

      if ( (r.get_left() > the_layer.get_width())
           || (r.get_bottom() > the_layer.get_height())
           || (r.get_right() < 0)
           || (r.get_top() < 0) )
        claw::logger << claw::log_warning << "Item '"
                     << items.front().get_class().get_class_name()
                     << "' at position "
                     << r.get_left() << ' ' << r.get_bottom() << ' '
                     << r.get_right() << ' ' << r.get_top()
                     << " is outside layer '" << the_layer.get_class_name()
                     << "' and will not be saved." << std::endl;
      else
        save_item( os, items.front() );
    }

  os << "</items>\n\n";
} // level_file_xml_writer::save_items()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save the priority of the items.
 * \param os The stream in which we write.
 * \param the_layer The layer to save.
 */
void bf::level_file_xml_writer::save_priority
( std::ostream& os, const layer& the_layer ) const
{
  os << "<priority>\n";

  std::vector<item_instance*> items = the_layer.get_prioritized_items();

  for ( std::size_t i=0; i!=items.size(); ++i )
    {
      if ( !items[i]->get_id().empty() )
        os << "<ref id=\"" << items[i]->get_id() << "\"/>\n";
      else
        claw::logger << claw::log_warning << "Item '"
                     << items[i]->get_class().get_class_name()
                     << "' has no id. His priority is not saved." << std::endl;
    }

  os << "</priority>\n\n";
} // level_file_xml_writer::save_priority()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save an item.
 * \param os The stream in which we write.
 * \param item The item to save.
 */
void bf::level_file_xml_writer::save_item
( std::ostream& os, const item_instance& item ) const
{
  xml::item_instance_node item_node;
  item_node.write(item, os);
} // level_file_xml_writer::save_item()

/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/ingame_view.hpp
 * \brief This control displays the items of the level.
 * \author Julien Jorge
 */
#ifndef __BF_INGAME_VIEW_HPP__
#define __BF_INGAME_VIEW_HPP__

#include "bf/grid.hpp"
#include "bf/level_clipboard.hpp"
#include "bf/level_check_result.hpp"
#include "bf/sprite_image_cache.hpp"
#include "bf/sprite_with_position.hpp"
#include "bf/history/level_history.hpp"

#include <wx/wx.h>
#include <wx/dnd.h>

namespace bf
{
  class action_group;
  class layer;
  class ingame_view_frame;
  class windows_layout;

  /**
   * \brief This control display the items of the level.
   * \author Julien Jorge
   */
  class ingame_view:
    public wxWindow
  {
  public:
    typedef wxWindow super;

  private:
    /** \brief Informations on a drag local to the view. */
    class drag_info
    {
    public:
      enum drag_mode_type
        {
          drag_mode_none,
          drag_mode_selection,
          drag_mode_move,
          drag_mode_size,
          drag_mode_pick
        }; // enum drag_mode

    public:
      drag_info();

      wxSize delta() const;

    public:
      /** \brief The current mode of the drag. */
      drag_mode_type drag_mode;

      /** \brief The item picked at the begining of the drag. */
      item_instance* picked_item;

      /** \brief Origin of the drag. */
      wxPoint mouse_origin;

      /** \brief Position of the mouse during the drag. */
      wxPoint mouse_position;

      /** \brief Tell if the drag conterns the X-axis. */
      bool x_active;

      /** \brief Tell if the drag conterns the Y-axis. */
      bool y_active;

    }; // struct drag_info

    /** \brief A drop target for creating an item. The class of the item is
        passed as a simple text. */
    class item_drop_target:
      public wxTextDropTarget
    {
    public:
      item_drop_target( ingame_view& view );

      bool OnDropText(wxCoord x, wxCoord y, const wxString& data);

    private:
      /** \brief The view associated with this target. */
      ingame_view& m_view;

    }; // class item_drop_target

  public:
    ingame_view
    ( ingame_view_frame& parent, gui_level* lvl, windows_layout& layout );
    ~ingame_view();

    bool empty() const;

    const wxPoint& get_view_position() const;
    wxSize get_layer_view_size() const;
    wxSize get_view_size() const;
    void set_view_position( wxCoord x, wxCoord y );

    unsigned int get_zoom() const;
    void set_zoom(unsigned int z);
    void set_zoom(unsigned int z, wxPoint mouse_position);

    unsigned int get_active_index() const;
    void set_active_index( unsigned int index );

    layer& get_active_layer() const;
    const gui_level& get_level() const;
    gui_level& get_level();

    void show_grid( bool v );
    bool get_grid_visibility() const;

    void set_bright_background( bool b );
    bool get_bright_background() const;

    void set_id_visibility( bool v );
    bool get_id_visibility() const;

    void toggle_relationship_drawing();
    bool get_relationship_drawing() const;

    void toggle_graphic_drawing();
    bool get_graphic_drawing() const;

    void toggle_wireframe_drawing();
    bool get_wireframe_drawing() const;

    const grid& get_grid() const;
    grid& get_grid();
    void set_grid( const grid& g );
    void set_grid_on_selection();

    void do_action( level_action* action );
    void do_action( action_move_selection* action );

    bool add_item( const std::string& class_name, wxCoord x, wxCoord y );
    bool add_item( const std::string& class_name );

    bool check_level();
    const level_check_result& get_check_result() const;
    void save( std::ostream& os );

    void undo();
    void redo();

    const level_history& get_history() const;

    void update_layout();

    bool has_selection() const;
    void clear_selection();
    void select_all();
    void select_item_and_layer( item_instance* item );

    const level_clipboard& get_clipboard() const;

    void copy_to_clipboard() const;
    void cut_to_clipboard();
    void paste_from_clipboard();

    void get_structure_sprites( std::list<sprite_with_position>& s ) const;
    void render();

  private:
    void render_layers( wxDC& dc ) const;
    void render_layer( wxDC& dc, unsigned int i ) const;

    void render_items
    ( wxDC& dc, const std::multimap<int, item_instance*>& z_order,
      unsigned int i ) const;

    void render_relationship
    ( wxDC& dc, const item_instance& item,
      const std::multimap<int, item_instance*>& z_order ) const;
    void render_relationship
    ( wxDC& dc, const item_instance& ref, const item_instance& rel ) const;

    void render_item
    ( wxDC& dc, const item_instance& item, const wxPoint& pos,
      unsigned int index ) const;

    void
    render_item_sprite( wxDC& dc, const item_instance& item ) const;
    void render_item_wireframe
    ( wxDC& dc, const item_instance& item, unsigned int index ) const;
    void render_item_id( wxDC& dc, const item_instance& item ) const;

    void render_item_as_sprite
    ( wxDC& dc, const item_instance& item, const wxPoint& pos ) const;
    void render_item_as_wireframe
    ( wxDC& dc, const item_instance& item, const wxPoint& pos,
      unsigned int index ) const;
    void render_item_id
    ( wxDC& dc, const item_instance& item, const wxPoint& pos ) const;

    void render_item_as_point
    ( wxDC& dc, const item_instance& item, const wxPoint& pos,
      unsigned int index ) const;
    bool render_sprite
    ( wxDC& dc, const item_instance& item, const wxPoint& pos ) const;
    void render_item_bounding_box
    ( wxDC& dc, const item_instance& item, const wxPoint& pos,
      const wxSize& size, unsigned int index ) const;
    void render_non_valid_item( wxDC& dc, const item_instance& item) const;
    void render_non_valid_item_as_point
    ( wxDC& dc, const item_instance& item, const wxPoint& pos ) const;
    void render_non_valid_item_box
    ( wxDC& dc, const item_instance& item,
      const wxPoint& pos, const wxSize& size ) const;
    void render_grip( wxDC& dc ) const;
    void render_drag( wxDC& dc ) const;
    void render_drag_mode_selection( wxDC& dc ) const;
    void render_drag_mode_move( wxDC& dc ) const;
    void render_drag_mode_size( wxDC& dc ) const;
    void render_grid( wxDC& dc ) const;
    void render_grid_vertical_lines( wxDC& dc ) const;
    void render_grid_horizontal_lines( wxDC& dc ) const;

    std::pair<wxBitmap, wxPoint>
    get_item_visual( const item_instance& item ) const;
    wxPen
    get_display_pen( const item_instance& item, unsigned int index ) const;

    const layer& current_layer();

    bool exist_selected_item( const wxPoint& pos );
    item_instance* first_selected_item( const wxPoint& pos );
    item_instance* pick_first_item( const wxPoint& pos );
    void pick_item( const wxPoint& pos, std::set<bf::item_instance*>& items );
    void pick_item( std::list<item_instance*>& item, const wxRect& box );
    void toggle_selection( item_instance* item );
    void add_selection( item_instance* item );
    void add_selection( const std::list<item_instance*>& item);
    void set_selection( const std::list<item_instance*>& item);
    void set_selection
    ( const std::list<item_instance*>& item, item_instance* selected,
      bool add = false);
    void set_selection( item_instance* item );

    wxRect get_bounding_box( const item_instance& item ) const;
    wxRect get_presence_box( const item_instance& item ) const;

    void copy_selection(bool add = false);
    void move_selection();

    int update_coordinate_magnetism
    ( unsigned int item_position, unsigned int size_item,
      unsigned int offset_grid, unsigned int step_grid);
    void update_mouse_position( const wxPoint& mouse_position );

    void move_grid(int keycode);
    void move_view(int keycode);

    void write_mouse_position(const wxPoint& point);

    void set_tooltip( item_instance* item );

    bool set_drag_mode_size( const wxPoint& pos );
    wxRect get_drag_mode_size_box() const;

    void apply_drag_mode_move( bool ctrl, bool shift, bool alt );
    void apply_drag_mode_pick( bool ctrl, bool alt );
    void apply_drag_mode_selection( bool ctrl, bool alt );
    void apply_drag_mode_size();

    wxCoord zoom( wxCoord v ) const;
    wxPoint zoom( wxPoint v ) const;
    wxSize zoom( wxSize v ) const;
    wxCoord unzoom( wxCoord v ) const;
    wxPoint unzoom( wxPoint v ) const;
    wxSize unzoom( wxSize v ) const;

    template<typename Action>
    void apply_action( Action* action );

    void on_size(wxSizeEvent& event);
    void on_paint(wxPaintEvent& event);
    void on_mouse_left_down(wxMouseEvent& event);
    void on_mouse_move(wxMouseEvent& event);
    void on_mouse_left_up(wxMouseEvent& event);
    void on_mouse_middle_up(wxMouseEvent& event);
    void on_mouse_wheel_rotation(wxMouseEvent& event);
    void on_key_down(wxKeyEvent& event);

  private:
    /** \brief The frame containing the view. */
    ingame_view_frame& m_parent;

    /** \brief The windows of the interface. */
    windows_layout& m_layout;

    /** \brief The level and the undo/redo informations. */
    level_history m_history;

    /** \brief Position of the view in the (zoomed) level. */
    wxPoint m_view;

    /** \brief Info about the current dragging. */
    drag_info* m_drag_info;

    /** \brief The grid. */
    grid m_grid;

    /** \brief Tell if we draw the bounding box of the items. */
    bool m_wireframe_drawing;

    /** \brief Tell if we display the pictures of the items. */
    bool m_graphic_drawing;

    /** \brief Tell if we display the grid. */
    bool m_display_grid;

    /** \brief Tell if we display the id. */
    bool m_display_id;

    /** \brief Tell if we display the relationship among the items. */
    bool m_display_relationship;

    /** \brief Tell if we use a bright background. */
    bool m_bright_background;

    /** \brief A cache of the sprites of the level. */
    sprite_image_cache* m_image_cache;

    /** \brief The current level of zoom. */
    unsigned int m_zoom;

    /** \brief The result of the last check on the level. */
    level_check_result m_check_result;

    /** \brief The size of the shape diplayed for sizeless items. */
    static const wxCoord s_point_size;

    /** \brief The size of grip in the corner of the selected item. */
    static const wxCoord s_grip_size;

    /** \brief The items copied in the clipboard. */
    static level_clipboard s_clipboard;

    DECLARE_EVENT_TABLE()

  }; // class ingame_view
} // namespace bf

#endif // __BF_INGAME_VIEW_HPP__

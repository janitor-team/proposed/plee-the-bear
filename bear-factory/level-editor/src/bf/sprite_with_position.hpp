/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sprite_with_position.hpp
 * \brief A sprite with a position and an angle.
 * \author Julien Jorge
 */
#ifndef __BF_SPRITE_WITH_POSITION_HPP__
#define __BF_SPRITE_WITH_POSITION_HPP__

#include <wx/gdicmn.h>

#include "bf/sprite.hpp"

namespace bf
{
  /**
   * \brief A sprite with a position.
   * \author Julien Jorge
   */
  struct sprite_with_position
  {
    /** \brief The sprite. */
    sprite spr;

    /** \brief The position. */
    wxPoint pos;

  }; // struct sprite_with_position
} // namespace bf

#endif // __BF_SPRITE_WITH_POSITION_HPP__

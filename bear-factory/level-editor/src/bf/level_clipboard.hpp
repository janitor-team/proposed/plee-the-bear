/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level_clipboard.hpp
 * \brief A class that handles the things that can be copied and pasted in a
 *        level.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_CLIPBOARD_HPP__
#define __BF_LEVEL_CLIPBOARD_HPP__

#include "bf/item_instance.hpp"

namespace bf
{
  /**
   * \brief A class that handles the things that can be copied and pasted in a
   *        level.
   *
   * The data is not available for other applications, nor for an other
   * instance of this program.
   *
   * \author Julien Jorge
   */
  struct level_clipboard
  {
    /** \brief The x-position to take as a reference when placing the copied
        items. */
    double x;

    /** \brief The y-position to take as a reference when placing the copied
        items. */
    double y;

    /** \brief The copied items. */
    std::list<item_instance> items;

  }; // struct level_clipboard
} // namespace bf

#endif // __BF_LEVEL_CLIPBOARD_HPP__

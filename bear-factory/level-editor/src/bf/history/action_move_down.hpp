/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_move_down.hpp
 * \brief The action of moving selection in the bottom layer.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_MOVE_DOWN_HPP__
#define __BF_ACTION_MOVE_DOWN_HPP__

#include "bf/history/action_group.hpp"
#include "bf/item_selection.hpp"

namespace bf
{
  /**
   * \brief The action of moving selection in the bottom layer.
   * \author Sébastien Angibaud
   */
  class action_move_down:
    public action_group
  {
  public:
    /**
     * \brief Constructor.
     * \param lvl The level in which we take the selection.
     */
    action_move_down( const gui_level& lvl );

    wxString get_description() const;
    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

  private:
    /** \brief The new items. */
    item_selection m_new_items;

    /** \brief The previous selection. */
    item_selection m_previous_items;

    /** \brief The previous selection in the layer on the bottom. */
    item_selection m_previous_down_items;

    /** \brief The index of the layer in which the items are added. */
    unsigned int m_layer;
  }; // class action_move_down
} // namespace bf

#endif // __BF_ACTION_MOVE_DOWN_HPP__

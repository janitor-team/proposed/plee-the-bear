/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_set_item_bottom.hpp
 * \brief The action of changing the position of the bottom edge of an item.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_SET_ITEM_BOTTOM_HPP__
#define __BF_ACTION_SET_ITEM_BOTTOM_HPP__

#include "bf/history/level_action.hpp"

namespace bf
{
  class item_instance;

  /**
   * \brief The action of changing the position of the bottom edge of an item.
   * \author Julien Jorge
   */
  class action_set_item_bottom:
    public level_action
  {
  public:
    /**
     * \brief Constructor.
     * \param item The item to modify.
     * \param p The new position of the bottom edge.
     */
    action_set_item_bottom( item_instance* item, double p );

    /**
     * \remark Calling execute() two times will restore the initial position.
     */
    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    bool is_identity( const gui_level& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The item to modify. */
    item_instance* m_item;

    /** \brief The position of the bottom edge. */
    double m_position;

  }; // class action_set_item_bottom
} // namespace bf

#endif // __BF_ACTION_SET_ITEM_BOTTOM_HPP__

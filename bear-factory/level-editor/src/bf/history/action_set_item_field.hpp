/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_set_item_field.hpp
 * \brief The action of changing the value of a field of an item.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_SET_ITEM_FIELD_HPP__
#define __BF_ACTION_SET_ITEM_FIELD_HPP__

#include "bf/history/level_action.hpp"
#include <string>

namespace bf
{
  class item_instance;

  /**
   * \brief The action of changing the value of a field of an item.
   * \author Julien Jorge
   */
  template<typename ValueType>
  class action_set_item_field:
    public level_action
  {
  public:
    /**
     * \brief Constructor. Remove the value of a field.
     * \param item The item to modify.
     * \param name The name of the field to change.
     */
    action_set_item_field( item_instance* item, const std::string& name );

    /**
     * \brief Constructor. Give a new value to a field.
     * \param item The item to modify.
     * \param name The name of the field to change.
     * \param val The new value of the field.
     */
    action_set_item_field
    ( item_instance* item, const std::string& name, const ValueType& val );

    /**
     * \remark Calling execute() two times will restore the initial value.
     */
    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    bool is_identity( const gui_level& gui ) const;
    wxString get_description() const;

    static wxString get_action_description();

  private:
    /** \brief The item to modify. */
    item_instance* const m_item;

    /** \brief The name of the field to set. */
    const std::string m_name;

    /** \brief The new value of the field. */
    ValueType m_value;

    /** \brief Tell if a value has been given to m_value. */
    bool m_has_value;

  }; // class action_set_item_field
} // namespace bf

#include "bf/history/impl/action_set_item_field.tpp"

#endif // __BF_ACTION_SET_ITEM_FIELD_HPP__

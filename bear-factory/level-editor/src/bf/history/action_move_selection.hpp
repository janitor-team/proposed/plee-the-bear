/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_move_selection.hpp
 * \brief The action of moving the selected items.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_MOVE_SELECTION_HPP__
#define __BF_ACTION_MOVE_SELECTION_HPP__

#include "bf/history/action_group.hpp"

namespace bf
{
  /**
   * \brief The action of deleting the selected items.
   * \author Julien Jorge
   */
  class action_move_selection:
    public action_group
  {
  public:
    /**
     * \brief Constructor.
     * \param lvl The level in which we take the selection.
     * \param dx The movement on the x-axis.
     * \param dy The movement on the y-axis.
     */
    action_move_selection( const gui_level& lvl, double dx, double dy );

    wxString get_description() const;

  }; // class action_move_selection
} // namespace bf

#endif // __BF_ACTION_MOVE_SELECTION_HPP__

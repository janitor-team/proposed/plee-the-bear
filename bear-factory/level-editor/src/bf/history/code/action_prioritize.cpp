/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_prioritize.cpp
 * \brief Implementation of the bf::action_prioritize class.
 * \author Julien Jorge
 */
#include "bf/history/action_prioritize.hpp"

#include "bf/gui_level.hpp"
#include "bf/item_class.hpp"
#include "bf/wx_facilities.hpp"

#include <claw/assert.hpp>
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param item The item to prioritize.
 * \param lay The layer in which the priority is set.
 */
bf::action_prioritize::action_prioritize
( item_instance* item, unsigned int lay )
  : m_item(item), m_layer(lay)
{

} // action_prioritize::action_prioritize()

/*----------------------------------------------------------------------------*/
void bf::action_prioritize::execute( gui_level& lvl )
{
  CLAW_PRECOND( lvl.layers_count() > m_layer );

  lvl.get_layer(m_layer).prioritize(m_item);
} // action_prioritize::execute()

/*----------------------------------------------------------------------------*/
void bf::action_prioritize::undo( gui_level& lvl )
{
  CLAW_PRECOND( lvl.layers_count() > m_layer );

  lvl.get_layer(m_layer).deprioritize(m_item);
} // action_prioritize::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_prioritize::is_identity( const gui_level& lvl ) const
{
  return lvl.get_layer(m_layer).is_prioritized(m_item);
} // action_prioritize::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_prioritize::get_description() const
{
  return wxString::Format
    ( _("Set the compilation priority of %s (%s)"),
      std_to_wx_string(m_item->get_class().get_class_name()).c_str(),
      std_to_wx_string(m_item->get_id()).c_str() );
} // action_prioritize::get_description()

/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/copy_action_common.cpp
 * \brief Implementation of the bf::copy_action_common class.
 * \author Julien Jorge
 */
#include "bf/history/copy_action_common.hpp"

#include "bf/gui_level.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Rename identifiers.
 * \param lvl The level on which the items are created.
 * \param new_items The list of new items.
 */
void bf::copy_action_common::update_identifiers
( const gui_level& lvl, const item_selection& new_items )
{
  std::map<std::string, std::string> id_map;
  item_selection::const_iterator it;
  std::set<std::string> avoid;

  for (it=new_items.begin(); it!=new_items.end(); ++it)
    if ( !(*it)->get_id().empty() )
      {
        std::string id( (*it)->get_id() );
        lvl.generate_valid_id(id, avoid);
        avoid.insert(id);

        id_map[(*it)->get_id()] = id;
        (*it)->set_id(id);
      }

  for (it=new_items.begin(); it!=new_items.end(); ++it)
    (*it)->rename_item_reference_fields(id_map);
} // copy_action_common::update_identifiers

/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_copy_selection.cpp
 * \brief Implementation of the bf::action_copy_selection class.
 * \author Julien Jorge
 */
#include "bf/history/action_copy_selection.hpp"

#include "bf/item_instance.hpp"
#include "bf/gui_level.hpp"
#include "bf/history/action_add_item.hpp"

#include <claw/assert.hpp>
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_copy_selection::action_copy_selection
( const gui_level& lvl, double dx, double dy, bool add )
  : m_add_to_selection(add)
{
  if ( !lvl.empty() )
    if ( lvl.has_selection() )
      {
        m_layer = lvl.get_active_layer_index();

        item_selection::const_iterator it;
        const item_selection& selection( lvl.get_selection() );

        for (it=selection.begin(); it!=selection.end(); ++it)
          {
            double x = (*it)->get_rendering_parameters().get_left() + dx;
            double y = (*it)->get_rendering_parameters().get_bottom() + dy;

            item_instance* item = new item_instance(**it);
            item->get_rendering_parameters().set_position(x, y);

            add_action( new action_add_item(item, m_layer) );

            m_new_items.insert(item, (*it == selection.get_main_selection()) );
          }

        update_identifiers(lvl, m_new_items);
      }
} // action_copy_selection::action_copy_selection()

/*----------------------------------------------------------------------------*/
void bf::action_copy_selection::execute( gui_level& lvl )
{
  CLAW_PRECOND( m_layer < lvl.layers_count() );

  m_previous_items = lvl.get_selection(m_layer);

  action_group::execute(lvl);

  if (!m_add_to_selection)
    lvl.clear_selection(m_layer);

  lvl.add_to_selection(m_layer, m_new_items);
} // action_copy_selection::execute()

/*----------------------------------------------------------------------------*/
void bf::action_copy_selection::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_layer < lvl.layers_count() );

  lvl.remove_from_selection(m_layer, m_new_items);

  action_group::undo(lvl);

  lvl.add_to_selection(m_layer, m_previous_items);
} // action_copy_selection::undo()

/*----------------------------------------------------------------------------*/
wxString bf::action_copy_selection::get_description() const
{
  return _("Copy selected items");
} // action_copy_selection::get_description()

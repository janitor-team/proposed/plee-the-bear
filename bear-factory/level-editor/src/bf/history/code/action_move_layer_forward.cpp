/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_move_layer_forward.cpp
 * \brief Implementation of the bf::action_move_layer_forward class.
 * \author Julien Jorge
 */
#include "bf/history/action_move_layer_forward.hpp"

#include "bf/gui_level.hpp"
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_move_layer_forward::action_move_layer_forward( unsigned int index )
  : m_original_index(index)
{

} // action_move_layer_forward::action_move_layer_forward()

/*----------------------------------------------------------------------------*/
void bf::action_move_layer_forward::execute( gui_level& lvl )
{
  lvl.move_forward(m_original_index);
} // action_move_layer_forward::execute()

/*----------------------------------------------------------------------------*/
void bf::action_move_layer_forward::undo( gui_level& lvl )
{
  lvl.move_backward(m_original_index+1);
} // action_move_layer_forward::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_move_layer_forward::is_identity( const gui_level& lvl ) const
{
  return m_original_index + 1 == lvl.layers_count();
} // action_move_layer_forward::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_move_layer_forward::get_description() const
{
  return wxString::Format(_("Move layer %d forward"), m_original_index);
} // action_move_layer_forward::get_description()

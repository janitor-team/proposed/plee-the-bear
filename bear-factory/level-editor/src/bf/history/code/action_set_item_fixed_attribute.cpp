/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_set_item_fixed_attribute.cpp
 * \brief Implementation of the bf::action_set_item_fixed_attribute class.
 * \author Julien Jorge
 */
#include "bf/history/action_set_item_fixed_attribute.hpp"

#include "bf/item_instance.hpp"

#include <wx/intl.h>
#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
bf::action_set_item_fixed_attribute::action_set_item_fixed_attribute
( item_instance* item, bool fixed )
  : m_item(item), m_fixed(fixed)
{
  CLAW_PRECOND( item != NULL );
} // action_set_item_fixed_attribute::action_set_item_fixed_attribute()

/*----------------------------------------------------------------------------*/
void bf::action_set_item_fixed_attribute::execute( gui_level& lvl )
{
  CLAW_PRECOND( m_item != NULL );

  const bool old( m_item->get_fixed() );

  m_item->set_fixed( m_fixed );

  m_fixed = old;
} // action_set_item_fixed_attribute::execute()

/*----------------------------------------------------------------------------*/
void bf::action_set_item_fixed_attribute::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_item != NULL );

  /* Calling execute() two times will restore the initial value. */
  execute(lvl);
} // action_set_item_fixed_attribute::undo()

/*----------------------------------------------------------------------------*/
bool
bf::action_set_item_fixed_attribute::is_identity( const gui_level& gui ) const
{
  return m_item->get_fixed() == m_fixed;
} // action_set_item_fixed_attribute::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_set_item_fixed_attribute::get_description() const
{
  if ( m_fixed )
    return _("Set the item as fixed");
  else
    return _("Set the item as not fixed");
} // action_set_item_fixed_attribute::get_description()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a generic description of the action.
 */
wxString bf::action_set_item_fixed_attribute::get_action_description()
{
  return _("Set item 'fixed' attribute");
} // action_set_item_fixed_attribute::get_action_description()

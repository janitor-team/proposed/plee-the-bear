/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_add_item.cpp
 * \brief Implementation of the bf::action_add_item class.
 * \author Julien Jorge
 */
#include "bf/history/action_add_item.hpp"

#include "bf/item_class.hpp"
#include "bf/gui_level.hpp"
#include "bf/wx_facilities.hpp"

#include <wx/intl.h>
#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
bf::action_add_item::action_add_item( item_instance* item, unsigned int lay )
  : m_item(item), m_layer(lay), m_done(false)
{

} // action_add_item::action_add_item()

/*----------------------------------------------------------------------------*/
bf::action_add_item::~action_add_item()
{
  /* If the action has not been done, the item must be deleted here. Otherwise,
     it will be deleted by the level. */
  if ( !m_done )
    delete m_item;
} // action_add_item::~action_add_item()

/*----------------------------------------------------------------------------*/
void bf::action_add_item::execute( gui_level& lvl )
{
  CLAW_PRECOND( !m_done );
  CLAW_PRECOND( lvl.layers_count() > m_layer );

  lvl.get_layer( m_layer ).add_item( m_item );

  m_done = true;
} // action_add_item::execute()

/*----------------------------------------------------------------------------*/
void bf::action_add_item::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_done );
  CLAW_PRECOND( lvl.layers_count() > m_layer );

  lvl.remove_from_selection( m_layer, m_item );
  lvl.get_layer( m_layer ).remove_item( m_item );
  m_done = false;
} // action_add_item::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_add_item::is_identity( const gui_level& gui ) const
{
  return false;
} // action_add_item::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_add_item::get_description() const
{
  return _("Add item ")
    + std_to_wx_string(m_item->get_class().get_class_name());
} // action_add_item::get_description()

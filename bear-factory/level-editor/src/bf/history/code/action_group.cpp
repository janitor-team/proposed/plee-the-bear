/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_group.cpp
 * \brief Implementation of the bf::action_group class.
 * \author Julien Jorge
 */
#include "bf/history/action_group.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param desc A description of the actions in the group.
 */
bf::action_group::action_group( const wxString& desc )
  : m_description(desc)
{

} // action_group::action_group()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::action_group::~action_group()
{
  action_collection::iterator it;

  for (it=m_actions.begin(); it!=m_actions.end(); ++it)
    delete *it;
} // action_group::~action_group()

/*----------------------------------------------------------------------------*/
/**
 * \brief Insert an action at the end.
 * \param a The action to add.
 */
void bf::action_group::add_action( level_action* a )
{
  m_actions.push_back(a);
} // action_group::add_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the actions of an other group into this group.
 * \param g The group from which we take the actions.
 * \post g.m_actions.empty() == true
 */
void bf::action_group::move( action_group& g )
{
  m_actions.splice( m_actions.end(), g.m_actions );
} // action_group::move()

/*----------------------------------------------------------------------------*/
void bf::action_group::execute( gui_level& lvl )
{
  action_collection::iterator it;

  for (it=m_actions.begin(); it!=m_actions.end(); ++it)
    (*it)->execute(lvl);
} // action_group::execute()

/*----------------------------------------------------------------------------*/
void bf::action_group::undo( gui_level& lvl )
{
  action_collection::reverse_iterator it;

  for (it=m_actions.rbegin(); it!=m_actions.rend(); ++it)
    (*it)->undo(lvl);
} // action_group::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_group::is_identity( const gui_level& lvl ) const
{
  bool result(true);
  action_collection::const_reverse_iterator it;

  for (it=m_actions.rbegin(); result && (it!=m_actions.rend()); ++it)
    result = (*it)->is_identity(lvl);

  return result;
} // action_group::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_group::get_description() const
{
  return m_description;
} // action_group::get_description()

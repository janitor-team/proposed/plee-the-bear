/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_set_item_position_and_size.cpp
 * \brief Implementation of the bf::action_set_item_position_and_size class.
 * \author Julien Jorge
 */
#include "bf/history/action_set_item_position_and_size.hpp"

#include "bf/history/action_set_item_position.hpp"
#include "bf/history/action_set_item_size.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_set_item_position_and_size::action_set_item_position_and_size
( item_instance* item, double left, double bottom, double width, double height )
{
  add_action( new action_set_item_position(item, left, bottom) );
  add_action( new action_set_item_size(item, width, height) );
} // action_set_item_position_and_size::action_set_item_position_and_size()

/*----------------------------------------------------------------------------*/
wxString bf::action_set_item_position_and_size::get_description() const
{
  return _("Move and resize");
} // action_set_item_position_and_size::get_description()

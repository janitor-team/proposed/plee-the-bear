/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_paste_from_clipboard.cpp
 * \brief Implementation of the bf::action_paste_from_clipboard class.
 * \author Julien Jorge
 */
#include "bf/history/action_paste_from_clipboard.hpp"

#include "bf/gui_level.hpp"
#include "bf/level_clipboard.hpp"
#include "bf/history/action_add_item.hpp"

#include <wx/intl.h>
#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
bf::action_paste_from_clipboard::action_paste_from_clipboard
( const gui_level& lvl, const level_clipboard& clipboard, double x, double y )
{
  if ( !clipboard.items.empty() && !lvl.empty() )
    {
      std::list<item_instance>::const_iterator it;

      m_layer = lvl.get_active_layer_index();

      for (it=clipboard.items.begin(); it!=clipboard.items.end(); ++it)
        {
          item_instance* item = new item_instance(*it);

          item->get_rendering_parameters().set_position
            ( x + (it->get_rendering_parameters().get_left() - clipboard.x),
              y + (it->get_rendering_parameters().get_bottom() - clipboard.y));

          add_action( new action_add_item( item, m_layer ) );

          if ( (it->get_rendering_parameters().get_left() == clipboard.x)
               && (it->get_rendering_parameters().get_bottom() == clipboard.y) )
            m_new_items.insert(item, true);
          else
            m_new_items.insert(item, false);
        }

      update_identifiers(lvl, m_new_items);
    }
} // action_paste_from_clipboard::action_paste_from_clipboard()

/*----------------------------------------------------------------------------*/
void bf::action_paste_from_clipboard::execute( gui_level& lvl )
{
  CLAW_PRECOND( m_layer < lvl.layers_count() );

  m_previous_items = lvl.get_selection(m_layer);

  action_group::execute(lvl);

  lvl.clear_selection(m_layer);
  lvl.add_to_selection(m_layer, m_new_items);
} // action_paste_from_clipboard::execute()

/*----------------------------------------------------------------------------*/
void bf::action_paste_from_clipboard::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_layer < lvl.layers_count() );

  lvl.remove_from_selection(m_layer, m_new_items);

  action_group::undo(lvl);

  lvl.add_to_selection(m_layer, m_previous_items);
} // action_paste_from_clipboard::undo()

/*----------------------------------------------------------------------------*/
wxString bf::action_paste_from_clipboard::get_description() const
{
  return _("Paste from clipboard");
} // action_paste_from_clipboard::get_description()

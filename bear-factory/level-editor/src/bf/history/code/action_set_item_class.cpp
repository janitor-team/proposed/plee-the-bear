/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_set_item_class.cpp
 * \brief Implementation of the bf::action_set_item_class class.
 * \author Julien Jorge
 */
#include "bf/history/action_set_item_class.hpp"

#include "bf/item_class.hpp"
#include "bf/wx_facilities.hpp"

#include <wx/intl.h>
#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param item The item to modify.
 * \param c The new class of the item.
 */
bf::action_set_item_class::action_set_item_class
( item_instance* item, item_class const* c )
  : m_item(item), m_class(c), m_backup(*m_item)
{
  CLAW_PRECOND( item != NULL );
  CLAW_PRECOND( c != NULL );
} // action_set_item_class::action_set_item_class()

/*----------------------------------------------------------------------------*/
void bf::action_set_item_class::execute( gui_level& lvl )
{
  CLAW_PRECOND( m_item != NULL );

  m_item->set_class( m_class );
} // action_set_item_class::execute()

/*----------------------------------------------------------------------------*/
void bf::action_set_item_class::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_item != NULL );

  *m_item = m_backup;
} // action_set_item_class::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_set_item_class::is_identity( const gui_level& gui ) const
{
  CLAW_PRECOND( m_item != NULL );

  return m_item->get_class_ptr() == m_class;
} // action_set_item_class::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_set_item_class::get_description() const
{
  return
    wxString::Format
    ( _("Set item class to %s"),
      std_to_wx_string(m_class->get_class_name()).c_str() );
} // action_set_item_class::get_description()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a generic description of the action.
 */
wxString bf::action_set_item_class::get_action_description()
{
  return _("Set item class");
} // action_set_item_class::get_action_description()

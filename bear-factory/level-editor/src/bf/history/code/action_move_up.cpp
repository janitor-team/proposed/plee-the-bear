/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_move_up.cpp
 * \brief Implementation of the bf::action_move_up class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_move_up.hpp"

#include "bf/gui_level.hpp"
#include "bf/history/action_add_item.hpp"
#include "bf/history/action_remove_item.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_move_up::action_move_up( const gui_level& lvl )
{
  if ( !lvl.empty() )
    if ( lvl.has_selection() )
      {
        m_layer = lvl.get_active_layer_index();

        if ( m_layer > 0 )
          {
            item_selection::const_iterator it;
            const item_selection& selection( lvl.get_selection() );

            for (it=selection.begin(); it!=selection.end(); ++it)
              add_action( new action_remove_item( *it, m_layer ) );

            for (it=selection.begin(); it!=selection.end(); ++it)
              {
                item_instance* item = new item_instance(**it);
                add_action( new action_add_item( item, m_layer-1 ) );
                m_new_items.insert
                  (item, (*it == selection.get_main_selection()) );
              }
          }
      }
} // action_move_up::action_move_up()

/*----------------------------------------------------------------------------*/
wxString bf::action_move_up::get_description() const
{
  return _("Change the layer of the selection");
} // action_move_up::get_description()

/*----------------------------------------------------------------------------*/
void bf::action_move_up::execute( gui_level& lvl )
{
  CLAW_PRECOND( m_layer < lvl.layers_count() );
  CLAW_PRECOND( m_layer > 0 );

  m_previous_items = lvl.get_selection(m_layer);
  m_previous_up_items = lvl.get_selection(m_layer-1);

  action_group::execute(lvl);

  lvl.clear_selection(m_layer-1);
  lvl.add_to_selection(m_layer-1, m_new_items);
} // action_move_up::execute()

/*----------------------------------------------------------------------------*/
void bf::action_move_up::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_layer < lvl.layers_count() );
  CLAW_PRECOND( m_layer > 0 );

  action_group::undo(lvl);

  lvl.clear_selection(m_layer-1);
  lvl.add_to_selection(m_layer-1, m_previous_up_items);
  lvl.clear_selection(m_layer);
  lvl.add_to_selection(m_layer, m_previous_items);
} // action_move_up::undo()

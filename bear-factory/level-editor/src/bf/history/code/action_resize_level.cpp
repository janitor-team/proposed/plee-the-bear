/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_resize_level.cpp
 * \brief Implementation of the bf::action_resize_level class.
 * \author Julien Jorge
 */
#include "bf/history/action_resize_level.hpp"

#include "bf/gui_level.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_resize_level::action_resize_level
( const std::string& name, unsigned int width, unsigned int height,
  const std::string& music )
  : m_name(name), m_width(width), m_height(height), m_music(music)
{

} // action_resize_level::action_resize_level()

/*----------------------------------------------------------------------------*/
void bf::action_resize_level::execute( gui_level& lvl )
{
  const unsigned int w( lvl.get_width() );
  const unsigned int h( lvl.get_height() );
  const std::string m( lvl.get_music() );
  const std::string n( lvl.get_name() );

  lvl.set_size( m_width, m_height );
  lvl.set_music( m_music );
  lvl.set_name( m_name );

  m_width = w;
  m_height = h;
  m_music = m;
  m_name = n;
} // action_resize_level::execute()

/*----------------------------------------------------------------------------*/
void bf::action_resize_level::undo( gui_level& lvl )
{
  /* the first call to execute saved the initial size and music. */
  execute(lvl);
} // action_resize_level::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_resize_level::is_identity( const gui_level& lvl ) const
{
  return (lvl.get_width() == m_width) && (lvl.get_height() == m_height)
    && (lvl.get_music() == m_music) && (lvl.get_name() == m_name);
} // action_resize_level::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_resize_level::get_description() const
{
  return _("Set level's size, music and name");
} // action_resize_level::get_description()

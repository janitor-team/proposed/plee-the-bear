/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_resize_layer.cpp
 * \brief Implementation of the bf::action_resize_layer class.
 * \author Julien Jorge
 */
#include "bf/history/action_resize_layer.hpp"

#include "bf/gui_level.hpp"

#include <claw/assert.hpp>
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_resize_layer::action_resize_layer
( bool fit_level, unsigned int width, unsigned int height,
  const std::string& class_name, unsigned int lay )
  : m_fit_level(fit_level), m_width(width), m_height(height),
    m_class(class_name), m_layer(lay)
{

} // action_resize_layer::action_resize_layer()

/*----------------------------------------------------------------------------*/
void bf::action_resize_layer::execute( gui_level& lvl )
{
  CLAW_PRECOND( lvl.layers_count() > m_layer );
  CLAW_PRECOND( !m_fit_level
                || ( (m_width == lvl.get_width())
                     && (m_height == lvl.get_height()) ) );

  const unsigned int f( lvl.get_layer( m_layer ).fits_level() );
  const unsigned int w( lvl.get_layer( m_layer ).get_width() );
  const unsigned int h( lvl.get_layer( m_layer ).get_height() );
  const std::string c( lvl.get_layer( m_layer ).get_class_name() );

  lvl.get_layer( m_layer ).set_fit_level( m_fit_level );
  lvl.get_layer( m_layer ).resize( m_width, m_height );
  lvl.get_layer( m_layer ).set_class_name( m_class );

  m_fit_level = f;
  m_width = w;
  m_height = h;
  m_class = c;
} // action_resize_layer::execute()

/*----------------------------------------------------------------------------*/
void bf::action_resize_layer::undo( gui_level& lvl )
{
  CLAW_PRECOND( lvl.layers_count() > m_layer );

  /* the first call to execute saved the initial size. */
  execute(lvl);
} // action_resize_layer::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_resize_layer::is_identity( const gui_level& lvl ) const
{
  return (m_fit_level == lvl.get_layer( m_layer ).fits_level())
    && (m_width == lvl.get_layer( m_layer ).get_width())
    && (m_height == lvl.get_layer( m_layer ).get_height())
    && (m_class == lvl.get_layer( m_layer ).get_class_name());
} // action_resize_layer::is_identity( const gui_level& gui )

/*----------------------------------------------------------------------------*/
wxString bf::action_resize_layer::get_description() const
{
  return _("Set layer size and class");
} // action_resize_layer::get_description()

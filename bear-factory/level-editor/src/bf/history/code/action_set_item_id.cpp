/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_set_item_id.cpp
 * \brief Implementation of the bf::action_set_item_id class.
 * \author Julien Jorge
 */
#include "bf/history/action_set_item_id.hpp"

#include "bf/item_instance.hpp"
#include "bf/layer.hpp"
#include "bf/gui_level.hpp"
#include "bf/wx_facilities.hpp"

#include <wx/intl.h>
#include <claw/assert.hpp>
#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param item The item for which the identifier changes.
 * \param id The new identifier.
 */
bf::action_set_item_id::action_set_item_id
( item_instance* item, const std::string& id )
  : m_item(item), m_id(id),
    m_layer_index(std::numeric_limits<unsigned int>::max())
{
  CLAW_PRECOND( item != NULL );
} // action_set_item_id::action_set_item_id()

/*----------------------------------------------------------------------------*/
void bf::action_set_item_id::execute( gui_level& lvl )
{
  CLAW_PRECOND( m_item != NULL );

  if (m_layer_index >= lvl.layers_count())
    m_layer_index = lvl.get_layer_by_item(*m_item);

  std::map<std::string, std::string> id_map;

  const std::string old( m_item->get_id() );
  id_map[old] = m_id;

  m_item->set_id( m_id );

  layer::item_iterator it;
  const layer::item_iterator ite = lvl.get_layer(m_layer_index).item_end();

  for ( it = lvl.get_layer(m_layer_index).item_begin(); it != ite; ++it )
    it->rename_item_reference_fields(id_map);

  m_id = old;
} // action_set_item_id::execute()

/*----------------------------------------------------------------------------*/
void bf::action_set_item_id::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_item != NULL );

  /* Calling execute() two times will restore the initial identifier. */
  execute(lvl);
} // action_set_item_id::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_set_item_id::is_identity( const gui_level& gui ) const
{
  return m_item->get_id() == m_id;
} // action_set_item_id::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_set_item_id::get_description() const
{
  if ( m_id.empty() )
    return _("Remove item identifier");
  else
    return
      wxString::Format
      ( _("Set item identifier to %s"), std_to_wx_string(m_id).c_str() );
} // action_set_item_id::get_description()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a generic description of the action.
 */
wxString bf::action_set_item_id::get_action_description()
{
  return _("Set item identifier");
} // action_set_item_id::get_action_description()

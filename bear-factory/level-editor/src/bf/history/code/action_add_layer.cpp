/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_add_layer.cpp
 * \brief Implementation of the bf::action_add_layer class.
 * \author Julien Jorge
 */
#include "bf/history/action_add_layer.hpp"

#include "bf/gui_level.hpp"

#include <claw/assert.hpp>
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_add_layer::action_add_layer( layer* lay, unsigned int index )
  : m_layer(lay), m_index(index)
{

} // action_add_layer::action_add_layer()

/*----------------------------------------------------------------------------*/
bf::action_add_layer::~action_add_layer()
{
  /* If the action has not been done, the layer must be deleted here. Otherwise,
     it will be deleted by the level. */
  if ( m_layer != NULL )
    delete m_layer;
} // action_add_layer::~action_add_layer()

/*----------------------------------------------------------------------------*/
void bf::action_add_layer::execute( gui_level& lvl )
{
  CLAW_PRECOND( m_layer != NULL );
  CLAW_PRECOND( lvl.layers_count() >= m_index );

  lvl.add_layer( m_layer, m_index );
  m_layer = NULL;
} // action_add_layer::execute()

/*----------------------------------------------------------------------------*/
void bf::action_add_layer::undo( gui_level& lvl )
{
  CLAW_PRECOND( m_layer == NULL );
  CLAW_PRECOND( lvl.layers_count() > m_index );

  m_layer = lvl.remove_layer( m_index );
} // action_add_layer::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_add_layer::is_identity( const gui_level& lvl ) const
{
  return false;
} // action_add_layer::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_add_layer::get_description() const
{
  return _("Add layer");
} // action_add_layer::get_description()

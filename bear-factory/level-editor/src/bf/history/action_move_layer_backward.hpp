/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_move_layer_backward.hpp
 * \brief The action of moving a layer backward.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_MOVE_LAYER_BACKWARD_HPP__
#define __BF_ACTION_MOVE_LAYER_BACKWARD_HPP__

#include "bf/history/level_action.hpp"

namespace bf
{
  /**
   * \brief The action of moving a layer backward.
   * \author Julien Jorge
   */
  class action_move_layer_backward:
    public level_action
  {
  public:
    /**
     * \brief Constructor.
     * \param index The index of the layer to move.
     */
    action_move_layer_backward( unsigned int index );

    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    bool is_identity( const gui_level& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The index of the layer to move. */
    const unsigned int m_original_index;

  }; // class action_move_layer_backward
} // namespace bf

#endif // __BF_ACTION_MOVE_LAYER_BACKWARD_HPP__

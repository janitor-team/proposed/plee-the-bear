/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_paste_from_clipboard.hpp
 * \brief The action of pasteing the items from the clipboard.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_PASTE_FROM_CLIPBOARD_HPP__
#define __BF_ACTION_PASTE_FROM_CLIPBOARD_HPP__

#include "bf/item_selection.hpp"
#include "bf/history/action_group.hpp"
#include "bf/history/copy_action_common.hpp"

namespace bf
{
  struct level_clipboard;

  /**
   * \brief The action of pasteing the items from the clipboard.
   * \author Julien Jorge
   */
  class action_paste_from_clipboard:
    public action_group,
    public copy_action_common
  {
  public:
    /**
     * \brief Constructor.
     * \param lvl The level in which we take the selection.
     * \param clipboard The clipboard from which we take the items.
     * \param x The x-position of the main selection.
     * \param y The y-position of the main selection.
     */
    action_paste_from_clipboard
    ( const gui_level& lvl, const level_clipboard& clipboard, double x,
      double y );

    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    wxString get_description() const;

  private:
    /** \brief The new items. */
    item_selection m_new_items;

    /** \brief The previous selection. */
    item_selection m_previous_items;

    /** \brief The index of the layer in which the items are added. */
    unsigned int m_layer;

  }; // class action_paste_from_clipboard
} // namespace bf

#endif // __BF_ACTION_PASTE_FROM_CLIPBOARD_HPP__

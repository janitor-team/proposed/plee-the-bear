/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/level_action.hpp
 * \brief Base class for the actions on a level, that can be undone.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_ACTION_HPP__
#define __BF_LEVEL_ACTION_HPP__

#include <claw/non_copyable.hpp>
#include <wx/string.h>

namespace bf
{
  class gui_level;

  /**
   * \brief Base class for the actions on a level, that can be undone.
   * \author Julien Jorge
   */
  class level_action
    : public claw::pattern::non_copyable
  {
  public:
    /** \brief Destructor. */
    virtual ~level_action() { }

    /**
     * \brief Apply the action to a level.
     * \param lvl The level to which the action is applied.
     */
    virtual void execute( gui_level& lvl ) = 0;

    /**
     * \brief Undo the action.
     * \param lvl The level in which the action is undone.
     */
    virtual void undo( gui_level& lvl ) = 0;

    /** \brief Tell if the action does nothing. */
    virtual bool is_identity( const gui_level& gui ) const = 0;

    /** \brief Get a short description of the action. */
    virtual wxString get_description() const = 0;

  }; // class level_action
} // namespace bf

#endif // __BF_LEVEL_ACTION_HPP__

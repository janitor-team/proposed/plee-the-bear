/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_resize_layer.hpp
 * \brief The action of resizing a layer.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_RESIZE_LAYER_HPP__
#define __BF_ACTION_RESIZE_LAYER_HPP__

#include "bf/history/level_action.hpp"

#include <string>

namespace bf
{
  /**
   * \brief The action of resizing a layer.
   * \author Julien Jorge
   */
  class action_resize_layer:
    public level_action
  {
  public:
    /**
     * \brief Constructor.
     * \param fit_level Tell if the layer has the same size than the level.
     * \param width The new width of the layer.
     * \param height The new height of the layer.
     * \param class_name The name of the class of the layer.
     * \param lay The layer to resize.
     */
    action_resize_layer
    ( bool fit_level, unsigned int width, unsigned int height,
      const std::string& class_name, unsigned int lay );

    /**
     * \remark Calling execute() two times will restore the initial size.
     */
    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    bool is_identity( const gui_level& gui ) const;
    wxString get_description() const;

  private:
    /** \brief Tell if the layer has the same size than the level. */
    bool m_fit_level;

    /** \brief The new width of the layer. */
    unsigned int m_width;

    /** \brief The new height of the layer. */
    unsigned int m_height;

    /** \brief The name of the class of the layer. */
    std::string m_class;

    /** \brief The layer to resize. */
    unsigned int m_layer;

  }; // class action_resize_layer
} // namespace bf

#endif // __BF_ACTION_RESIZE_LAYER_HPP__

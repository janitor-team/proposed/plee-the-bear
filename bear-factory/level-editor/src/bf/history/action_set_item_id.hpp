/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_set_item_id.hpp
 * \brief The action of setting the identifier of an item.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_SET_ITEM_ID_HPP__
#define __BF_ACTION_SET_ITEM_ID_HPP__

#include "bf/history/level_action.hpp"

#include <string>

namespace bf
{
  class item_instance;

  /**
   * \brief The action of setting the identifier of an item.
   * \author Julien Jorge
   */
  class action_set_item_id:
    public level_action
  {
  public:
    /**
     * \brief Constructor.
     * \param item The item to modify.
     * \param id The new identifier of the item.
     */
    action_set_item_id
    ( item_instance* item, const std::string& id );

    /**
     * \remark Calling execute() two times will restore the initial identifier.
     */
    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    bool is_identity( const gui_level& gui ) const;
    wxString get_description() const;

    static wxString get_action_description();

  private:
    /** \brief The item to modify. */
    item_instance* const m_item;

    /** \brief The identifier of the item. */
    std::string m_id;

    /** \brief The index of the layer in which the item is. */
    unsigned int m_layer_index;

  }; // class action_set_item_id
} // namespace bf

#endif // __BF_ACTION_SET_ITEM_ID_HPP__

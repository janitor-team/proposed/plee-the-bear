/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/copy_action_common.hpp
 * \brief Common methods to action_copy_selection, action_paste_from_clipboard
 *        and similar.
 * \author Julien Jorge
 */
#ifndef __BF_COPY_ACTION_COMMON_HPP__
#define __BF_COPY_ACTION_COMMON_HPP__

#include "bf/item_selection.hpp"
#include <list>

namespace bf
{
  class gui_level;

  /**
   * \brief Common methods to action_copy_selection, action_paste_from_clipboard
   *        and similar.
   * \author Julien Jorge
   */
  class copy_action_common
  {
  protected:
    void update_identifiers
    ( const gui_level& lvl, const item_selection& new_items );

  }; // class copy_action_common
} // namespace bf

#endif // __BF_COPY_ACTION_COMMON_HPP__

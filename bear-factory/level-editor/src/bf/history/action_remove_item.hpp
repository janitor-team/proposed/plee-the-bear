/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_remove_item.hpp
 * \brief The action of removing an item in a layer.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_REMOVE_ITEM_HPP__
#define __BF_ACTION_REMOVE_ITEM_HPP__

#include "bf/history/action_group.hpp"

namespace bf
{
  class item_instance;
  class item_class;
  class type_field;

  /**
   * \brief The action of removing an item in a layer.
   * \author Julien Jorge
   */
  class action_remove_item:
    public level_action
  {
  public:
    action_remove_item( item_instance* item, unsigned int lay );

    /** \brief Destructor. */
    ~action_remove_item();

    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    bool is_identity( const gui_level& gui ) const;
    wxString get_description() const;

  private:
    void create_related_actions( gui_level& lvl );
    void create_related_actions_with( item_instance* item );
    void
    create_related_actions_with( item_instance* item, const item_class& c );

    void create_action_set_item_field_for
    ( item_instance* item, const type_field& f );
    void create_action_set_item_field_list_for
    ( item_instance* item, const type_field& f );

  private:
    /** \brief The item to remove. */
    item_instance* m_item;

    /** \brief The layer from which the item is removed. */
    unsigned int m_layer;

    /** \brief Tell if the item was selected. */
    bool m_selected;

    /** \brief Tell if the item was the main selection. */
    bool m_main_selection;

    /** \brief Tell if the action has been done or not. */
    bool m_done;

    /** \brief All actions happening before the removal of the item. */
    action_group* m_pre_actions;

    /** \brief All actions happening consequently to the removal of the item. */
    action_group* m_post_actions;

  }; // class action_remove_item
} // namespace bf

#endif // __BF_ACTION_REMOVE_ITEM_HPP__

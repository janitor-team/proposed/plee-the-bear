/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_set_priority.hpp
 * \brief The action of setting the compilation priority on an item.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_SET_PRIORITY_HPP__
#define __BF_ACTION_SET_PRIORITY_HPP__

#include "bf/history/level_action.hpp"

namespace bf
{
  class item_instance;

  /**
   * \brief The action of setting the compilation priority on an item.
   * \author Julien Jorge
   */
  class action_set_priority:
    public level_action
  {
  public:
    action_set_priority( item_instance* item, unsigned int lay, std::size_t p );

    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    bool is_identity( const gui_level& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The item to remove. */
    item_instance* m_item;

    /** \brief The layer from which the item is removed. */
    unsigned int m_layer;

    /** \brief The priority of the item. */
    std::size_t m_priority;

    /** \brief The previous priority of the item. */
    int m_previous_priority;

  }; // class action_set_priority
} // namespace bf

#endif // __BF_ACTION_SET_PRIORITY_HPP__

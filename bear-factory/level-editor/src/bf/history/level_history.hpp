/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/level_history.hpp
 * \brief A class to manage undo/redo informations on a level.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_HISTORY_HPP__
#define __BF_LEVEL_HISTORY_HPP__

#include "bf/item_selection.hpp"

#include <wx/string.h>
#include <list>

namespace bf
{
  class action_move_selection;
  class gui_level;
  class level_action;

  /**
   * \brief A class to manage undo/redo informations on a level.
   * \author Julien Jorge
   */
  class level_history
  {
  public:
    level_history( gui_level* lvl );
    ~level_history();

    bool can_undo() const;
    bool can_redo() const;

    void undo();
    void redo();

    bool do_action( level_action* action );
    bool do_action( action_move_selection* action );

    wxString get_undo_description() const;
    wxString get_redo_description() const;

    void set_saved();
    bool level_is_saved() const;

    gui_level& get_level();
    const gui_level& get_level() const;

  private:
    void clear_past();
    void clear_future();

    void push_action( level_action* a );

  private:
    /** \brief The level on which we are working. */
    gui_level* m_level;

    /** \brief The actions done in the past. */
    std::list<level_action*> m_past;

    /** \brief The actions in the future. */
    std::list<level_action*> m_future;

    /** \brief Maximum size of the undo history. */
    std::size_t m_max_history;

    /** \brief The last action done before saving. */
    level_action* m_saved_action;

    /** \brief The last action of moving the selection, used to merge successive
        little moves. */
    action_move_selection* m_last_selection_move;

    /** \brief The date of the last movement of the selection. */
    time_t m_last_selection_move_date;

    /** \brief The selection moved by m_last_selection_move. */
    item_selection m_last_selection_move_items;

  }; // class level_history
} // namespace bf

#endif // __BF_LEVEL_HISTORY_HPP__

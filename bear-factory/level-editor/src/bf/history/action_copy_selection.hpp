/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_copy_selection.hpp
 * \brief The action of copying the selected items at a new position.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_COPY_SELECTION_HPP__
#define __BF_ACTION_COPY_SELECTION_HPP__

#include "bf/item_selection.hpp"
#include "bf/history/action_group.hpp"
#include "bf/history/copy_action_common.hpp"

namespace bf
{
  /**
   * \brief The action of copying the selected items at a new position.
   * \author Julien Jorge
   */
  class action_copy_selection:
    public action_group,
    public copy_action_common
  {
  public:
    /**
     * \brief Constructor.
     * \param lvl The level in which we take the selection.
     * \param dx The distance of the copy on the x-axis.
     * \param dy The distance of the copy on the y-axis.
     * \param add Tell if the copy has to be added to the current selection.
     */
    action_copy_selection
    ( const gui_level& lvl, double dx, double dy, bool add );

    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    wxString get_description() const;

  private:
    /** \brief The new items. */
    item_selection m_new_items;

    /** \brief The previous selection. */
    item_selection m_previous_items;

    /** \brief Tell if the new items are added to the current selection or if
        they replace it. */
    const bool m_add_to_selection;

    /** \brief The index of the layer in which the items are added. */
    unsigned int m_layer;

  }; // class action_copy_selection
} // namespace bf

#endif // __BF_ACTION_COPY_SELECTION_HPP__

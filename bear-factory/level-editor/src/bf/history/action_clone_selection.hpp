/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_clone_selection.hpp
 * \brief The action of cloning the selected items.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_CLONE_SELECTION_HPP__
#define __BF_ACTION_CLONE_SELECTION_HPP__

#include "bf/history/action_group.hpp"
#include "bf/history/copy_action_common.hpp"

namespace bf
{
  class item_instance;

  /**
   * \brief The action of deleting the selected items.
   * \author Julien Jorge
   */
  class action_clone_selection:
    public action_group,
    public copy_action_common
  {
  public:
    /**
     * \brief Constructor.
     * \param lvl The level in which we take the selection.
     * \param x_count Count of clones on the x-axis.
     * \param y_count Count of clones on the y-axis.
     * \param x_offset Offset on the x-axis.
     * \param y_offset Offset on the y-axis.
     * \param add Add the clones in the selection.
     */
    action_clone_selection
    ( const gui_level& lvl, unsigned int x_count, unsigned int y_count,
      double x_offset, double y_offset, bool add );

    void execute( gui_level& lvl );
    void undo( gui_level& lvl );

    wxString get_description() const;

  private:
    /**
     * \brief Clone an item.
     * \param item The item to clone.
     * \param x_count Count of clones on the x-axis.
     * \param y_count Count of clones on the y-axis.
     * \param x_offset Offset on the x-axis.
     * \param y_offset Offset on the y-axis.
     */
    void clone_item
    ( const item_instance& item, unsigned int x_count, unsigned int y_count,
      double x_offset, double y_offset );

  private:
    /** \brief The new items, if we have to add them in the selection. */
    item_selection m_new_items;

    /** \brief The index of the layer in which the items are added. */
    unsigned int m_layer;

  }; // class action_clone_selection
} // namespace bf

#endif // __BF_ACTION_CLONE_SELECTION_HPP__

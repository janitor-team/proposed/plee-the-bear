/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/main_frame.hpp
 * \brief The main window of our program.
 * \author Julien Jorge
 */
#ifndef __BF_MAIN_FRAME_HPP__
#define __BF_MAIN_FRAME_HPP__

#include "bf/item_class_pool.hpp"

#include <wx/wx.h>

#include <list>
#include <string>

namespace bf
{
  class class_tree_ctrl;
  class class_selected_event;
  class ingame_view_frame;
  class layer_list_frame;
  class properties_frame;
  class windows_layout;

  /**
   * \brief The main window of our program.
   * \author Julien Jorge
   */
  class main_frame:
    public wxFrame
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDM_ITEM_PROPERTIES,
        IDM_LAYER_LIST,
        IDM_ITEM_CLASS_POOL,
        IDM_SAVE_ALL_LEVELS,
        IDM_UPDATE_IMAGE_POOL,
        IDC_CLASS_LIST
      }; // enum control_id

  public:
    main_frame();
    ~main_frame();

    void load_level( const wxString& path );
    void new_level( const wxString& path );

    void set_active_level( ingame_view_frame* view );

  private:
    /** \brief Enable/disable the controls relative to the levels. */
    void turn_level_entries( bool enable );

    void create_menu();
    void create_controls();

    wxMenu* create_level_menu() const;
    wxMenu* create_window_menu() const;
    wxMenu* create_help_menu() const;

    void save_config();

    void add_level_view( ingame_view_frame* view );

    void on_configuration_menu(wxCommandEvent& event);
    void on_update_image_pool_menu(wxCommandEvent& event);
    void on_item_properties_menu(wxCommandEvent& event);
    void on_layer_list_menu(wxCommandEvent& event);
    void on_item_class_pool_menu(wxCommandEvent& event);
    void on_new_level(wxCommandEvent& event);
    void on_open_level(wxCommandEvent& event);
    void on_save(wxCommandEvent& event);
    void on_save_as(wxCommandEvent& event);
    void on_save_all(wxCommandEvent& event);
    void on_exit(wxCommandEvent& event);
    void on_menu_open(wxMenuEvent& event);
    void on_menu_highlight(wxMenuEvent& event);
    void on_online_doc(wxCommandEvent& event);
    void on_about(wxCommandEvent& event);
    void on_close(wxCloseEvent& event);
    void on_class_selected( class_selected_event& event );

  private:
    /** \brief The window displaying the properties of an item. */
    properties_frame* m_properties_frame;

    /** \brief The list of the layers in the current level. */
    layer_list_frame* m_layer_list;

    /** \brief The windows of the program. */
    windows_layout* m_windows_layout;

    /** \brief The control displaying the item classes. */
    class_tree_ctrl* m_class_tree;

    DECLARE_EVENT_TABLE()

  }; // class main_frame
} // namespace bf

#endif // __BF_MAIN_FRAME_HPP__

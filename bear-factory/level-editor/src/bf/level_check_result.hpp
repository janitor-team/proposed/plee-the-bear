/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level_check_result.hpp
 * \brief This class contains the result of the check of a level.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_CHECK_RESULT_HPP__
#define __BF_LEVEL_CHECK_RESULT_HPP__

#include "bf/layer_check_result.hpp"

#include <map>

namespace bf
{
  class layer;

  /**
   * \brief This class contains the result of the check of a level.
   * \author Julien Jorge
   */
  class level_check_result
  {
  private:
    /** \brief The type of the map in which we store the result of the check of
        the layers. */
    typedef std::map<layer*, layer_check_result> layer_result_map_type;

  public:
    /** \brief The type of the iterators on the results of the check of the
        layers. */
    typedef layer_result_map_type::const_iterator layer_iterator;

  public:
    void check( layer* lay );

    void clear();
    bool is_ok() const;

    bool contains( layer* lay, item_instance* item ) const;

    layer_iterator layer_begin() const;
    layer_iterator layer_end() const;

    const layer_check_result& get_layer_result( layer* lay ) const;

  private:
    /** \brief The result of the check of the layers. */
    layer_result_map_type m_layers;

  }; // class level_check_result
} // namespace bf

#endif // __BF_LEVEL_CHECK_RESULT_HPP__

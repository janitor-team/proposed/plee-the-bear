/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/item_filter.cpp
 * \brief Implementation of the bf::item_filter class.
 * \author Julien Jorge
 */
#include "bf/item_filter/item_filter.hpp"

#include "bf/item_filter/base_item_filter.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bf::item_filter::item_filter()
  : m_filter(NULL)
{

} // item_filter::item_filter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param filter The filter effectively evaluated.
 */
bf::item_filter::item_filter( const base_item_filter& filter )
  : m_filter(filter.clone())
{

} // item_filter::item_filter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of the filter.
 */
bf::item_filter bf::item_filter::copy() const
{
  if ( m_filter == NULL )
    return item_filter();
  else
    return item_filter(*m_filter);
} // item_filter::copy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the filter on an item.
 * \param item The item on which the filter is evaluated.
 */
bool bf::item_filter::evaluate( const item_instance& item ) const
{
  if ( m_filter == NULL )
    return true;
  else
    return m_filter->evaluate(item);
} // item_filter::evaluate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a description of the filter.
 */
std::string bf::item_filter::get_description() const
{
  if ( m_filter == NULL )
    return "NULL filter";
  else
    return m_filter->get_description();
} // item_filter::get_description()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compares with an other filter for equality.
 * \param that The filter with which the comparison is done.
 *
 * \return true if both filters point to the same instance of base_item_filter.
 */
bool bf::item_filter::operator==( const item_filter& that ) const
{
  return m_filter == that.m_filter;
} // item_filter::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compares with an other filter for disequality.
 * \param that The filter with which the comparison is done.
 */
bool bf::item_filter::operator!=( const item_filter& that ) const
{
  return !(*this == that);
} // item_filter::operator!=()

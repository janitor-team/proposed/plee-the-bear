/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/item_filter_by_class_name_glob.cpp
 * \brief Implementation of the bf::item_filter_by_class_name_glob class.
 * \author Julien Jorge
 */
#include "bf/item_filter/item_filter_by_class_name_glob.hpp"

#include "bf/item_instance.hpp"
#include "bf/item_class.hpp"

#include <claw/string_algorithm.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param pattern The pattern that must be matched by the name of the class of
 *        which the item must be an instance.
 */
bf::item_filter_by_class_name_glob::item_filter_by_class_name_glob
( const std::string& pattern )
  : m_class_name_pattern(pattern)
{

} // item_filter_by_class_name_glob::item_filter_by_class_name_glob()

/*----------------------------------------------------------------------------*/
/**
 * \brief Instanciate a copy of this instance.
 */
bf::item_filter_by_class_name_glob*
bf::item_filter_by_class_name_glob::clone() const
{
  return new item_filter_by_class_name_glob(*this);
} // item_filter_by_class_name_glob::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the filter on an item.
 * \param item The item on which the filter is evaluated.
 */
bool
bf::item_filter_by_class_name_glob::evaluate( const item_instance& item ) const
{
  return evaluate_class(item.get_class());
} // item_filter_by_class_name_glob::evaluate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a description of the filter.
 */
std::string bf::item_filter_by_class_name_glob::get_description() const
{
  return "class~" + m_class_name_pattern;
} // item_filter_by_class_name_glob::get_description()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the filter on a class.
 * \param c The class to check.
 */
bool
bf::item_filter_by_class_name_glob::evaluate_class( const item_class& c ) const
{
  bool result =
    claw::text::glob_match( m_class_name_pattern, c.get_class_name() );
  item_class::const_super_class_iterator it;

  for ( it=c.super_class_begin(); !result && (it!=c.super_class_end()); ++it )
    result = evaluate_class(*it);

  return result;
} // item_filter_by_class_name_glob::evaluate_class()

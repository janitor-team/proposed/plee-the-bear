/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/item_filter_by_class_name.cpp
 * \brief Implementation of the bf::item_filter_by_class_name class.
 * \author Julien Jorge
 */
#include "bf/item_filter/item_filter_by_class_name.hpp"

#include "bf/item_instance.hpp"
#include "bf/item_class.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param class_name The name of the class of which the item must be an
 *        instance.
 */
bf::item_filter_by_class_name::item_filter_by_class_name
( const std::string& class_name )
  : m_class_name(class_name)
{

} // item_filter_by_class_name::item_filter_by_class_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Instanciate a copy of this instance.
 */
bf::item_filter_by_class_name* bf::item_filter_by_class_name::clone() const
{
  return new item_filter_by_class_name(*this);
} // item_filter_by_class_name::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the filter on an item.
 * \param item The item on which the filter is evaluated.
 */
bool bf::item_filter_by_class_name::evaluate( const item_instance& item ) const
{
  return (item.get_class().get_class_name() == m_class_name)
  || item.get_class().inherits_from(m_class_name);
} // item_filter_by_class_name::evaluate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a description of the filter.
 */
std::string bf::item_filter_by_class_name::get_description() const
{
  return "class=" + m_class_name;
} // item_filter_by_class_name::get_description()

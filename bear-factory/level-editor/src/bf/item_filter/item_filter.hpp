/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_filter.hpp
 * \brief An object to be passed to layers to filter the items among which the
 *        operations are done.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_FILTER_HPP__
#define __BF_ITEM_FILTER_HPP__

#include "bf/item_filter/base_item_filter.hpp"

#include <claw/smart_ptr.hpp>
#include <string>

namespace bf
{
  class item_instance;

  /**
   * \brief An object to be passed to layers to filter the items among which the
   *        operations are done.
   * \author Julien Jorge
   */
  class item_filter
  {
  private:
    /** \brief The type of the pointer on the effective filter. */
    typedef claw::memory::smart_ptr<base_item_filter> filter_ptr_type;

  public:
    item_filter();
    item_filter( const base_item_filter& filter );

    item_filter copy() const;

    bool evaluate( const item_instance& item ) const;

    std::string get_description() const;

    bool operator==( const item_filter& that ) const;
    bool operator!=( const item_filter& that ) const;

  private:
    /** \brief The filter effectively applied. */
    filter_ptr_type m_filter;

  }; // class item_filter
} // namespace bf

#endif // __BF_ITEM_FILTER_HPP__


/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_filter_by_class_name.hpp
 * \brief A filter on items to check the item's class name.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_FILTER_BY_CLASS_NAME_HPP__
#define __BF_ITEM_FILTER_BY_CLASS_NAME_HPP__

#include "bf/item_filter/base_item_filter.hpp"

#include <string>

namespace bf
{
  /**
   * \brief A filter on items to check the item's class name.
   * \author Julien Jorge
   */
  class item_filter_by_class_name:
    public base_item_filter
  {
  public:
    explicit item_filter_by_class_name( const std::string& class_name );

    virtual item_filter_by_class_name* clone() const;
    virtual bool evaluate( const item_instance& item ) const;
    virtual std::string get_description() const;

  private:
    /** \brief The name of the class of which the item must be an instance. */
    const std::string m_class_name;

  }; // class item_filter_by_class_name
} // namespace bf

#endif // __BF_ITEM_FILTER_BY_CLASS_NAME_HPP__


/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/clone_selection_dialog.hpp
 * \brief A dialog to clone the selection.
 * \author Julien Jorge
 */
#ifndef __BF_CLONE_SELECTION_DIALOG_HPP__
#define __BF_CLONE_SELECTION_DIALOG_HPP__

#include "bf/spin_ctrl.hpp"

#include <wx/dialog.h>
#include <wx/spinctrl.h>
#include <wx/checkbox.h>

namespace bf
{
  /**
   * \brief A dialog to clone the selection.
   * \author Julien Jorge
   */
  class clone_selection_dialog:
    public wxDialog
  {
  public:
    clone_selection_dialog( wxWindow& parent );

    unsigned int get_x_count() const;
    unsigned int get_y_count() const;
    double get_x_offset() const;
    double get_y_offset() const;
    bool get_add_to_selection() const;

  private:
    void create_controls();
    void create_sizers();

  private:
    /** \brief Count on the x-axis. */
    wxSpinCtrl* m_x_count;

    /** \brief Count on the y-axis. */
    wxSpinCtrl* m_y_count;

    /** \brief Offset on the x-axis. */
    spin_ctrl<double>* m_x_offset;

    /** \brief Offset on the y-axis. */
    spin_ctrl<double>* m_y_offset;

    /** \brief Add the clones to the selection. */
    wxCheckBox* m_add_to_selection;

  }; // class clone_selection_dialog
} // namespace bf

#endif // __BF_CLONE_SELECTION_DIALOG_HPP__

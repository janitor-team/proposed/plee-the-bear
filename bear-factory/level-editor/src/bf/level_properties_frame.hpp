/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level_properties_frame.hpp
 * \brief The window showing the properties of a level.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_PROPERTIES_FRAME_HPP__
#define __BF_LEVEL_PROPERTIES_FRAME_HPP__

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <string>

namespace bf
{
  class level;

  /**
   * \brief The window showing the properties of a level.
   * \author Julien Jorge
   */
  class level_properties_frame:
    public wxDialog
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        ID_BROWSE
      }; // enum control_id

  public:
    level_properties_frame( wxWindow* parent );

    unsigned int get_width() const;
    unsigned int get_height() const;
    const std::string& get_music() const;
    const std::string& get_name() const;

    void init_from( const level& lvl );

  private:
    void fill_controls();
    void create_controls();
    void create_sizer_controls();
    wxSizer* create_level_sizer();
    wxSizer* create_music_sizer();
    wxSizer* create_name_sizer();

    void on_ok(wxCommandEvent& event);
    void on_browse(wxCommandEvent& event);

  private:
    /** \brief The width of the level. */
    unsigned int m_width;

    /** \brief The height of the level. */
    unsigned int m_height;

    /** \brief The music to play in this level. */
    std::string m_music;

    /** \brief The name of the level. */
    std::string m_name;

    /** \brief The text control in which we configure the width of the level. */
    wxSpinCtrl* m_width_text;

    /** \brief The text control in which we configure the height of the
        level. */
    wxSpinCtrl* m_height_text;

    /** \brief The control in which we display the music. */
    wxTextCtrl* m_music_text;

    /** \brief The button to select the music. */
    wxButton* m_music_browse;

    /** \brief The control in which we display the name of the level. */
    wxTextCtrl* m_name_text;

    /** \brief Minimum width of the level. */
    static const unsigned int s_min_width;

    /** \brief Minimum height of the level. */
    static const unsigned int s_min_height;

    DECLARE_EVENT_TABLE()

  }; // class level_properties_frame
} // namespace bf

#endif // __BF_LEVEL_PROPERTIES_FRAME_HPP__

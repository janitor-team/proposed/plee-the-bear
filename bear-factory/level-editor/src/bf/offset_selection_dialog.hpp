/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/offset_selection_dialog.hpp
 * \brief A dialog to apply an offset the selection.
 * \author Julien Jorge
 */
#ifndef __BF_OFFSET_SELECTION_DIALOG_HPP__
#define __BF_OFFSET_SELECTION_DIALOG_HPP__

#include "bf/spin_ctrl.hpp"

#include <wx/dialog.h>

namespace bf
{
  /**
   * \brief A dialog to apply offset the selection.
   * \author Julien Jorge
   */
  class offset_selection_dialog:
    public wxDialog
  {
  public:
    offset_selection_dialog( wxWindow& parent );

    double get_x() const;
    double get_y() const;

  private:
    void create_controls();
    void create_sizers();

  private:
    /** \brief Offset on the x-axis. */
    spin_ctrl<double>* m_x;

    /** \brief Offset on the y-axis. */
    spin_ctrl<double>* m_y;

  }; // class offset_selection_dialog
} // namespace bf

#endif // __BF_OFFSET_SELECTION_DIALOG_HPP__

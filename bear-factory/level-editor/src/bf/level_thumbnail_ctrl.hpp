/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level_thumbnail_ctrl.hpp
 * \brief This control display a small global view of the edited level.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_THUMBNAIL_CTRL_HPP__
#define __BF_LEVEL_THUMBNAIL_CTRL_HPP__

#include "bf/sprite_image_cache.hpp"

#include <wx/window.h>

namespace bf
{
  class ingame_view_frame;

  /**
   * \brief This control display a small global view of the edited level.
   * \author Julien Jorge
   */
  class level_thumbnail_ctrl
    : public wxWindow
  {
  public:
    level_thumbnail_ctrl( wxWindow* parent );
    ~level_thumbnail_ctrl();

    void set_view( ingame_view_frame* view );
    void update();

    const ingame_view_frame* get_level() const;

  private:
    void compute_view_box();

    void render();
    void set_view_position(int x, int y);

    void on_paint(wxPaintEvent& event);
    void on_size(wxSizeEvent& event);
    void on_mouse_down(wxMouseEvent& event);
    void on_mouse_move(wxMouseEvent& event);

  private:
    /** \brief The view on the edited level. */
    ingame_view_frame* m_level;

    /** \brief The position of the view in the level view. */
    wxRect m_view_box;

    /** \brief The thumbnail view of the level. */
    wxBitmap m_thumbnail;

    /** \brief A cache of the sprites of the level. */
    sprite_image_cache* m_image_cache;

    DECLARE_EVENT_TABLE()

  }; // class level_thumbnail_ctrl
} // namespace bf

#endif // __BF_LEVEL_THUMBNAIL_CTRL_HPP__

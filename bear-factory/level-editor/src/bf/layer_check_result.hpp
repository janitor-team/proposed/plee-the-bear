/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/layer_check_result.hpp
 * \brief This class contains the result of the check of a layer.
 * \author Julien Jorge
 */
#ifndef __BF_LAYER_CHECK_RESULT_HPP__
#define __BF_LAYER_CHECK_RESULT_HPP__

#include "bf/item_instance.hpp"
#include "bf/item_check_result.hpp"

namespace bf
{
  /**
   * \brief This class contains the result of the check of a layer.
   * \author Julien Jorge
   */
  class layer_check_result
  {
  private:
    /** \brief The type of the map in which we store the result of the check of
        the items. */
    typedef std::map<item_instance*, item_check_result> item_result_map_type;

  public:
    /** \brief The type of the iterators on the results of the check of the
        items. */
    typedef item_result_map_type::const_iterator item_iterator;

  public:
    void check( item_instance* item, const std::set<std::string>& map_id );

    void add( item_instance* item, const check_error& e );

    bool is_ok() const;

    bool contains( item_instance* item ) const;

    item_iterator item_begin() const;
    item_iterator item_end() const;

    const bf::item_check_result& get_item_result( item_instance* item ) const;

  private:
    /** \brief The result of the check of the items. */
    item_result_map_type m_items;

  }; // class layer_check_result
} // namespace bf

#endif // __BF_LAYER_CHECK_RESULT_HPP__

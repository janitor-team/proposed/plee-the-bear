/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level_overview_frame.hpp
 * \brief This control displays a global view of the edited level and allows to
          change the position of the view.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_OVERVIEW_FRAME_HPP__
#define __BF_LEVEL_OVERVIEW_FRAME_HPP__

#include <wx/frame.h>

namespace bf
{
  class level_thumbnail_ctrl;
  class ingame_view_frame;

  /**
   * \brief This control displays a global view of the edited level and allows
   *        to change the position of the view.
   * \author Julien Jorge
   */
  class level_overview_frame
    : public wxFrame
  {
  public:
     /** \brief The identifiers of the controls. */
    enum control_id
      {
        ID_UPDATE_WIDTH,
        ID_UPDATE_HEIGHT
      }; // enum control_id

  public:
    level_overview_frame( ingame_view_frame& parent );
    ~level_overview_frame();

  private:
    void on_close(wxCloseEvent& event);
    void on_refresh(wxCommandEvent& event);
    void on_update_width(wxCommandEvent& event);
    void on_update_height(wxCommandEvent& event);

  private:
    level_thumbnail_ctrl* m_thumbnail;

    DECLARE_EVENT_TABLE()

  }; // class level_overview_frame
} // namespace bf

#endif // __BF_LEVEL_OVERVIEW_FRAME_HPP__

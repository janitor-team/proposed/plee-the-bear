/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/grid_properties_frame.hpp
 * \brief The window showing the properties of the grid.
 * \author Sebastien Angibaud
 */
#ifndef __BF_GRID_PROPERTIES_FRAME_HPP__
#define __BF_GRID_PROPERTIES_FRAME_HPP__

#include "bf/grid.hpp"

#include <wx/wx.h>
#include <wx/spinctrl.h>

namespace bf
{
  /**
   * \brief The grid properties window of our program.
   * \author Sebastien Angibaud
   */
  class grid_properties_frame:
    public wxDialog
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_CHANGE_POSITION_SIZE
      }; // enum control_id

  public:
    grid_properties_frame( wxWindow* parent, const grid& g );

    const grid& get_grid() const;

  private:
    void fill_controls();

    void create_controls();
    void create_member_controls();
    void create_sizer_controls();

    void on_ok(wxCommandEvent& event);
    void on_value_change(wxSpinEvent& event);

  private:
    /** \brief The grid with the current values. */
    grid m_grid;

    /** \brief The text control in which we configure the width of the
        offset. */
    wxSpinCtrl* m_offset_x_text;

    /** \brief The text control in which we configure the height of the
        offset. */
    wxSpinCtrl* m_offset_y_text;

    /** \brief The text control in which we configure the width of the step. */
    wxSpinCtrl* m_step_x_text;

    /** \brief The text control in which we configure the height of the step. */
    wxSpinCtrl* m_step_y_text;

    /** \brief The control for configure the magnetism activation. */
    wxCheckBox* m_magnetism_active_box;

     /** \brief The control for configure
  the magnetism force. */
    wxSlider* m_magnetism_force_slider;

    /** \brief Minimum x_coordinate of step. */
    static const unsigned int s_min_step_x;

    /** \brief Minimum y_coordinate of step. */
    static const unsigned int s_min_step_y;

    DECLARE_EVENT_TABLE()

  }; // class grid_properties_frame
} // namespace bf

#endif // __BF_GRID_PROPERTIES_FRAME_HPP__

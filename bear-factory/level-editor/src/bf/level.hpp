/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/level.hpp
 * \brief A level, with its layers.
 * \author Julien Jorge
 */
#ifndef __BF_LEVEL_HPP__
#define __BF_LEVEL_HPP__

#include "bf/layer.hpp"
#include <vector>
#include <list>

namespace bf
{
  class level_check_result;

  /**
   * \brief A level, with its layers.
   * \author Julien Jorge
   */
  class level
  {
  public:
    level( const std::string& name, unsigned int width, unsigned int height,
           const std::string& mus );
    level( const level& that );
    ~level();

    layer& get_layer( unsigned int index ) const;

    layer* remove_layer( unsigned int index );

    layer& add_layer
    ( const std::string& class_name, bool fit_level = true,
      unsigned int width = 1, unsigned int height = 1 );
    void add_layer( layer* lay, unsigned int index );

    void move_backward( unsigned int layer_index );
    void move_forward( unsigned int layer_index );

    bool empty() const;
    std::size_t layers_count() const;

    unsigned int get_width() const;
    unsigned int get_height() const;
    void set_size( unsigned int width, unsigned int height );

    const std::string& get_music() const;
    void set_music( const std::string& music );

    const std::string& get_name() const;
    void set_name( const std::string& name );

    std::pair<bool, layer::item_iterator>
    find_item_by_id( const std::string& id ) const;
    unsigned int get_layer_by_item( const item_instance& item ) const;

    void generate_valid_id
    ( std::string& id, const std::set<std::string>& avoid ) const;

    level& operator=( const level& that );

    void check( level_check_result& result );

    void compile( compiled_file& f ) const;

    void add_filter( const item_filter& filter );
    void remove_filter( const item_filter& filter );

  private:
    void clear();
    void assign( const level& that );

    void check_unique_identifiers
    ( unsigned int i, std::set<std::string>& identifiers ) const;

  private:
    /** \brief The name of the level. */
    std::string m_name;

    /** \brief The layers. */
    std::vector<layer*> m_layer;

    /** \brief The width of the level. */
    unsigned int m_width;

    /** \brief The height of the level. */
    unsigned int m_height;

    /** \brief The music to play in this level. */
    std::string m_music;

  }; // class level
} // namespace bf

#endif // __BF_LEVEL_HPP__

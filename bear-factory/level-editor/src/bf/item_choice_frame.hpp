/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_choice_frame.hpp
 * \brief The window showing the item choice for selection.
 * \author Sebastien Angibaud
 */
#ifndef __BF_ITEM_CHOICE_FRAME_HPP__
#define __BF_ITEM_CHOICE_FRAME_HPP__

#include <wx/wx.h>
#include <set>

namespace bf
{
  class item_instance;
  class item_selection;

  /**
   * \brief The window showing the item choice for selection.
   * \author Sebastien Angibaud
   */
  class item_choice_frame:
    public wxDialog
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_LAYER_LIST,
        IDC_INVERSE,
        IDC_SELECT_ALL
      }; // enum control_id

  public:
    item_choice_frame
    ( wxWindow* parent, const item_selection& selected_group,
      const std::set<item_instance*>& choice);

    void get_selection(std::set<item_instance*>& selected_items);

  private:
    void create_controls();
    wxSizer* create_list_sizer();

    void select_all();
    void inverse_selection();

    void on_ok(wxCommandEvent& event);
    void on_inverse(wxCommandEvent& event);
    void on_select_all(wxCommandEvent& event);

  private:
    /* \brief The checklistbox of items. */
    wxCheckListBox* m_item_checklistbox;

    /* \brief The list of choice. */
    const std::set<item_instance*>& m_choice;

    /* \brief The selected group. */
    const item_selection& m_selected_group;

    /* \brief The button to inverse the selection. */
    wxButton* m_inverse_button;

    /* \brief The button to select all items. */
    wxButton* m_all_button;

    DECLARE_EVENT_TABLE()

  }; // class item_choice_frame
} // namespace bf

#endif // __BF_ITEM_CHOICE_FRAME_HPP__

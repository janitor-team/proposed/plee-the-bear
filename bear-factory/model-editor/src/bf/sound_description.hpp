/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sound_description.hpp
 * \brief The description of a sound and how to play it.
 * \author Julien Jorge
 */
#ifndef __BF_SOUND_DESCRIPTION_HPP__
#define __BF_SOUND_DESCRIPTION_HPP__

#include <string>

namespace bf
{
  class compiled_file;

  /**
   * \brief The description of a sound and how to play it.
   * \author Julien Jorge
   */
  class sound_description
  {
  public:
    sound_description();

    void set_file(const std::string& file);
    const std::string& get_file() const;

    void set_played_globally( bool b );
    bool is_played_globally() const;

    void compile( compiled_file& f ) const;

  private:
    /** \brief The path to the sound file. */
    std::string m_file;

    /** \brief Tell if the sound is played globally (default is "false"). */
    bool m_played_globally;

  }; // class sound_description
} // namespace bf

#endif // __BF_SOUND_DESCRIPTION_HPP__

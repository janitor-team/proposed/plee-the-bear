/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/error_check_model_dialog.hpp
 * \brief A dialog to check errors in a model.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ERROR_CHECK_MODEL_DIALOG_HPP__
#define __BF_ERROR_CHECK_MODEL_DIALOG_HPP__

#include <wx/dialog.h>
#include <wx/wx.h>
#include <wx/listctrl.h>
#include <list>

namespace bf
{
  class action;

  /**
   * \brief A dialog when the model is not valid.
   * \author Sebastien Angibaud
   */
  class error_check_model_dialog:
    public wxDialog
  {
  public:
    error_check_model_dialog
    ( wxWindow& parent,
      const std::list< const action* >& actions );

    unsigned int get_index_selected() const;

  private:
    void create_controls();
    void create_sizers();
    void adjust_last_column_size();
    void on_size(wxSizeEvent& event);

  private:
    /** \brief The list of bad actions. */
    const std::list< const action* >& m_actions;

    /** \brief The control that displays the list of bad actions. */
    wxListView* m_actions_list;

    DECLARE_EVENT_TABLE()

  }; // class error_check_model_dialog
} // namespace bf

#endif // __BF_ERROR_CHECK_MODEL_DIALOG_HPP__

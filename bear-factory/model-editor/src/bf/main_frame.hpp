/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/main_frame.hpp
 * \brief The main window of our program.
 * \author Julien Jorge
 */
#ifndef __BF_MAIN_FRAME_HPP__
#define __BF_MAIN_FRAME_HPP__

#include "bf/configuration.hpp"
#include <wx/frame.h>

namespace bf
{
  class model_frame;
  class model_properties_ctrl;
  class windows_layout;

  /**
   * \brief The main window of our program.
   * \author Julien Jorge
   */
  class main_frame:
    public wxFrame
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        ID_SAVE_ALL_MODELS,
        ID_UPDATE_IMAGE_POOL
      }; // enum control_id

  public:
    main_frame();
    ~main_frame();

    void new_model( const wxString& path );
    void load_model( const wxString& path );
    void set_active_model( model_frame* m );

    void update_action();
    void update_snapshot();
    void update_mark();

  private:
    void create_menu();
    void create_controls();

    wxMenu* create_model_menu() const;
    wxMenu* create_help_menu() const;

    void save_config();

    void add_model_view( model_frame* m );

    void on_configuration_menu(wxCommandEvent& event);
    void on_update_image_pool_menu(wxCommandEvent& event);
    void on_new_model(wxCommandEvent& event);
    void on_open_model(wxCommandEvent& event);
    void on_save_all(wxCommandEvent& event);
    void on_exit(wxCommandEvent& event);
    void on_online_doc(wxCommandEvent& event);
    void on_about(wxCommandEvent& event);
    void on_close(wxCloseEvent& event);

  private:
    /** \brief The configuration of the program. */
    void turn_model_entries( bool enable );

    /** \brief The properties of the currently edited model. */
    model_properties_ctrl* m_model_properties;

    /** \brief The windows of the program. */
    windows_layout* m_windows_layout;

    DECLARE_EVENT_TABLE()

  }; // class main_frame
} // namespace bf

#endif // __BF_MAIN_FRAME_HPP__

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/mark_placement.hpp
 * \brief The placement of a mark (position, angle, depth).
 * \author Julien Jorge
 */
#ifndef __BF_MARK_PLACEMENT_HPP__
#define __BF_MARK_PLACEMENT_HPP__

#include <string>

#include "easing.hpp"

namespace bf
{
  class compiled_file;
  class mark;

  /**
   * \brief The placement of a mark (position, angle, depth).
   * \author Julien Jorge
   */
  class mark_placement
  {
  public:
    explicit mark_placement( const mark* m = NULL );

    void copy( const mark_placement& that );

    void set_x_position( double x );
    void set_y_position( double y );
    void set_position( double x, double y );
    double get_x_position() const;
    double get_y_position() const;

    void set_width( double w );
    void set_height( double h );
    void set_size( double w, double h );
    double get_width() const;
    double get_height() const;

    void set_collision_function( const std::string& f );
    const std::string& get_collision_function() const;

    void set_visibility( bool v );
    bool is_visible() const;

    void set_depth_position( int p );
    int get_depth_position() const;

    void set_angle( double a );
    double get_angle() const;

    const mark* get_mark() const;

    void set_x_easing( const bear::easing& e );
    const bear::easing& get_x_easing() const;

    void set_y_easing( const bear::easing& e );
    const bear::easing& get_y_easing() const;

    void set_width_easing( const bear::easing& e );
    const bear::easing& get_width_easing() const;

    void set_height_easing( const bear::easing& e );
    const bear::easing& get_height_easing() const;

    void set_angle_easing( const bear::easing& e );
    const bear::easing& get_angle_easing() const;

    void compile( compiled_file& f ) const;

  private:
    /** \brief The mark concerned by this placement. */
    const mark* m_mark;

    /** \brief The X-position of the mark. */
    double m_x;

    /** \brief The Y-position of the mark. */
    double m_y;

    /** \brief The width of the box around the mark. */
    double m_width;

    /** \brief The height of the box around the mark. */
    double m_height;

    /** \brief The depth-position of the mark. */
    int m_depth;

    /** \brief The angle of the mark. */
    double m_angle;

    /** \brief Tell if the mark is displayed or not. */
    bool m_visible;

    /** \brief The function called when a collision occurs with this mark. */
    std::string m_collision_function;

    /** \brief The function to use for the easing of the x-position. */
    bear::easing m_x_easing;

    /** \brief The function to use for the easing of the y-position. */
    bear::easing m_y_easing;

    /** \brief The function to use for the easing of the width. */
    bear::easing m_width_easing;

    /** \brief The function to use for the easing of the height. */
    bear::easing m_height_easing;

    /** \brief The function to use for the easing of the angle. */
    bear::easing m_angle_easing;

  }; // class mark_placement
} // namespace bf

#endif // __BF_MARK_PLACEMENT_HPP__

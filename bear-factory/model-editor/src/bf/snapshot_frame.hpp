/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/snapshot_frame.hpp
 * \brief The window displaying the current snapshot of an action.
 * \author Sébastien Angibaud
 */
#ifndef __BF_SNAPSHOT_FRAME_HPP__
#define __BF_SNAPSHOT_FRAME_HPP__

#include "bf/accordion.hpp"
#include "bf/base_file_edit.hpp"
#include "bf/spin_ctrl.hpp"

#include <wx/combo.h>
#include <wx/spinctrl.h>
#include <wx/wx.h>
#include <string>

namespace bf
{
  class model_frame;
  class snapshot;

  /**
   * \brief The window displaying various informations about a snapshot.
   * \author Sébastien Angibaud
   */
  class snapshot_frame:
    public wxPanel
  {
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_DATE_CTRL,
        IDC_MARK_CHOICE,
        IDC_MARK_PLACEMENT,
        IDC_MARK_SIZE,
        IDC_MARK_FUNCTION,
        IDC_VISIBILITY,
        IDC_FUNCTION,
        IDC_GLOBALLY_SOUND,
        IDC_SOUND_FILE,
        IDC_SOUND_TEXT,
        IDC_ALIGNMENT,
        IDC_SIZE
      }; // enum control_id

  public:
    snapshot_frame( wxWindow* parent);

    void set_model_frame( model_frame* f );
    void update_action();
    void update_snapshot();
    void update_mark();

  private:
    void create_controls();
    void create_member_controls( accordion* a );
    void create_sizer_controls( accordion* a );

    void create_snapshot_controls( wxWindow* parent );
    void create_marks_controls( wxWindow* parent );
    void create_sound_controls( wxWindow* parent );
    void create_item_box_controls( wxWindow* parent );

    wxSizer* create_snapshot_sizer( wxWindow* parent );
    wxSizer* create_marks_sizer( wxWindow* parent );
    wxSizer* create_sound_sizer( wxWindow* parent );
    wxSizer* create_item_box_sizer( wxWindow* parent );

    wxComboCtrl* create_easing_combo( wxWindow* parent );

    void fill();
    void fill_mark();

    void placement_change();
    void alignment_change();
    void size_change();
    void sound_change();

    void enable_controls(bool enable);

    void on_close(wxCloseEvent& event);
    void on_date_change( wxCommandEvent& event );
    void on_mark_change( wxCommandEvent& event );
    void on_alignment_choice_change( wxCommandEvent& event );
    void on_placement_change( spin_event<double>& event );
    void on_mark_size( spin_event<double>& event );
    void on_mark_function( wxCommandEvent& event );
    void on_alignment_change( spin_event<double>& event );
    void on_size_change( spin_event<double>& event );
    void on_depth_change( wxSpinEvent& event );
    void on_visibility_change( wxCommandEvent& event );
    void on_function_change( wxCommandEvent& event );
    void on_globally_sound_change( wxCommandEvent& event );
    void on_sound_file_edit( wxCommandEvent& event );
    void on_sound_text_edit( wxCommandEvent& event );

    void on_x_easing( wxCommandEvent& event );
    void on_y_easing( wxCommandEvent& event );
    void on_width_easing( wxCommandEvent& event );
    void on_height_easing( wxCommandEvent& event );
    void on_angle_easing( wxCommandEvent& event );

  private:
    /** \brief The current model_frame. */
    model_frame* m_model_frame;

    /** \brief The list of marks, presented to the user. */
    snapshot* m_snapshot;

    /** \brief The button control in which we configure the date of
        the snapshot. */
    wxButton* m_date_button;

    /** \brief The text control in which we configure the function to call. */
    wxButton* m_function_button;

    /** \brief The spin control in which we configure the left of the mark.*/
    spin_ctrl<double>* m_mark_left_spin;

    /** \brief The spin control in which we configure the bottom of the mark.*/
    spin_ctrl<double>* m_mark_bottom_spin;

    /** \brief The spin control in which we configure the width of a mark.*/
    spin_ctrl<double>* m_mark_width_spin;

    /** \brief The spin control in which we configure the height of the mark.*/
    spin_ctrl<double>* m_mark_height_spin;

    /** \brief The text control in which we configure the function to call on
        a collision on a mark. */
    wxButton* m_collision_function_button;

    /** \brief The spin control in which we configure the z-coordinate of
        the box. */
    wxSpinCtrl* m_mark_depth_spin;

    /** \brief The spin control in which we configure the angle of the mark.*/
    spin_ctrl<double>* m_mark_angle_spin;

    /** \brief The spin control in which we configure the current mark. */
    wxChoice* m_mark_choice;

    /** \brief The button control to check the visibily of the mark. */
    wxCheckBox* m_mark_visible_box;

    /** \brief The easing function for the x-position of the mark. */
    wxComboCtrl* m_mark_x_easing;

    /** \brief The easing function for the y-position of the mark. */
    wxComboCtrl* m_mark_y_easing;

    /** \brief The easing function for the width of the mark. */
    wxComboCtrl* m_mark_width_easing;

    /** \brief The easing function for the height of the mark. */
    wxComboCtrl* m_mark_height_easing;

    /** \brief The easing function for the angle of the mark. */
    wxComboCtrl* m_mark_angle_easing;

    /** \brief The sound button to edit path. */
    wxButton* m_sound_button;

     /** \brief Text control in which we display the path to the sound. */
    wxTextCtrl* m_sound_path;

    /** \brief The button control to check if the sound is global. */
    wxCheckBox* m_globally_sound_box;

    /** \brief The spin control in which we configure the
        width of the action.*/
    spin_ctrl<double>* m_width_spin;

    /** \brief The spin control in which we configure
        the height of the action.*/
    spin_ctrl<double>* m_height_spin;

    /** \brief The spin control in which we configure
        the value of alignment on x-coordinate.*/
    spin_ctrl<double>* m_x_alignment_value_spin;

    /** \brief The spin control in which we configure
        the value of alignment on y-coordinate.*/
    spin_ctrl<double>* m_y_alignment_value_spin;

    /** \brief The spin control in which we choose
        the type of alignment on x-coordinate.*/
    wxChoice* m_x_alignment_choice;

    /** \brief The spin control in which we choose
        the type of alignment on y-coordinate.*/
    wxChoice* m_y_alignment_choice;

    DECLARE_EVENT_TABLE()

  }; // class snapshot_frame
} // namespace bf

#endif // __BF_SNAPSHOT_FRAME_HPP__

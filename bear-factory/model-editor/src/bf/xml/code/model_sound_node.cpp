/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/code/model_sound_node.cpp
 * \brief Implementation of the bf::xml::model_sound_node class.
 * \author Julien Jorge
 */
#include "bf/xml/model_sound_node.hpp"

#include "bf/sound_description.hpp"
#include "bf/xml/reader_tool.hpp"
#include "bf/xml/util.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Read an xml node "sound".
 * \param s (out) The sound read in the node.
 * \param node The item node.
 */
void bf::xml::model_sound_node::read
( sound_description& s, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node!=NULL );
  CLAW_PRECOND( node->GetName() == wxT("sound") );

  s.set_file( reader_tool::read_string(node, wxT("file")) );
  s.set_played_globally
    ( reader_tool::read_bool_opt(node, wxT("global"), false) );
} // model_sound_node::read()

/*----------------------------------------------------------------------------*/
/**
 * \brief Write an xml node "sound".
 * \param s The sound to write.
 * \param os The stream in which we write.
 */
void bf::xml::model_sound_node::write
( const sound_description& s, std::ostream& os ) const
{
  if ( !s.get_file().empty() )
    os << "<sound file=\""
       << xml::util::replace_special_characters(s.get_file()) << "\" global=\""
       << s.is_played_globally() << "\"/>\n";
} // model_sound_node::write()

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/model_properties_ctrl.hpp
 * \brief This window display the properties of the model currently edited.
 * \author Julien Jorge
 */
#ifndef __BF_MODEL_PROPERTIES_CTRL_HPP__
#define __BF_MODEL_PROPERTIES_CTRL_HPP__

#include <wx/notebook.h>

namespace bf
{
  class model;
  class model_frame;
  class fields_frame;
  class action_list_frame;
  class mark_list_frame;
  class snapshot_frame;
  class windows_layout;

  /**
   * \brief This window display the properties of the model currently edited.
   * \author Julien Jorge
   */
  class model_properties_ctrl:
    public wxNotebook
  {
  public:
    model_properties_ctrl( wxWindow* parent );

    void set_model_frame( model_frame* m );
    void update_action();
    void update_snapshot();
    void update_mark();

  private:
    void create_controls();

  private:
    /** \brief The frame that displays the actions of the current model. */
    action_list_frame* m_actions_frame;

    /** \brief The frame that displays the marks of the current action. */
    mark_list_frame* m_marks_frame;

    /** \brief The frame that displays the current snapshot. */
    snapshot_frame* m_snapshot_frame;

  }; // class model_properties_ctrl
} // namespace

#endif // __BF_MODEL_PROPERTIES_CTRL_HPP__

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/model_selection.hpp
 * \brief A selection of marks.
 * \author Julien Jorge
 */
#ifndef __BF_MODEL_SELECTION_HPP__
#define __BF_MODEL_SELECTION_HPP__

#include <set>

namespace bf
{
  class mark;

  /**
   * \brief A selection of marks.
   * \author Julien Jorge
   */
  class model_selection
  {
  public:
    /** \brief Iterator on the selected marks. */
    typedef std::set<mark*>::const_iterator const_iterator;

  public:
    model_selection();

    void insert( mark* mark, bool main_selection = false );
    void insert( const model_selection& s );

    void remove( mark* mark );
    void remove( const model_selection& s );

    bool is_selected( mark* mark ) const;
    bool is_main_selection( mark* mark ) const;

    void set_bounding_box_selection( bool s );
    bool bounding_box_is_selected() const;
    bool bounding_box_is_main_selection() const;

    void clear();

    bool empty() const;
    bool mark_empty() const;

    mark* get_main_mark_selection() const;

    const_iterator begin() const;
    const_iterator end() const;

  private:
    /** \brief The main selection. */
    mark* m_mark;

    /** \brief All the selected marks. */
    std::set<mark*> m_group;

    /** \brief Tell if the bounding box is selected. */
    bool m_bounding_box_is_selected;

  }; // class model_selection
} // namespace bf

#endif // __BF_MODEL_SELECTION_HPP__

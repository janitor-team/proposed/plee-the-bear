/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/sound_description.cpp
 * \brief Implementation of the bf::sound_description class.
 * \author Julien Jorge
 */
#include "bf/sound_description.hpp"

#include "bf/compiled_file.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bf::sound_description::sound_description()
  : m_played_globally(false)
{

} // sound_description::sound_description()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the path to the file to play.
 * \param file The path to the file to play.
 */
void bf::sound_description::set_file(const std::string& file)
{
  m_file = file;
} // sound_description::set_file()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the path to the file to play.
 */
const std::string& bf::sound_description::get_file() const
{
  return m_file;
} // sound_description::get_file()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set if the sound is played globally.
 * \param b Global or not.
 */
void bf::sound_description::set_played_globally( bool b )
{
  m_played_globally = b;
} // sound_description::set_played_globally()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the sound is played globally.
 */
bool bf::sound_description::is_played_globally() const
{
  return m_played_globally;
} // sound_description::is_played_globally()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compile the sound.
 * \param f The file in which we compile.
 */
void bf::sound_description::compile( compiled_file& f ) const
{
  f << m_file << m_played_globally;
} // sound_description::compile()

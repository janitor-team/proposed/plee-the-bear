/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/model_properties_ctrl.cpp
 * \brief Implementation of the bf::model_properties_ctrl class.
 * \author Julien Jorge
 */
#include "bf/model_properties_ctrl.hpp"

#include "bf/action_list_frame.hpp"
#include "bf/mark_list_frame.hpp"
#include "bf/snapshot_frame.hpp"
#include "bf/model.hpp"
#include "bf/model_frame.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Default constructor.
 * \param parent The parent window.
 */
bf::model_properties_ctrl::model_properties_ctrl( wxWindow* parent )
  : wxNotebook(parent, wxID_ANY)
{
  create_controls();
} // model_properties_ctrl::model_properties_ctrl()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the model frame for which the properties are edited.
 */
void bf::model_properties_ctrl::set_model_frame( model_frame* m )
{
  m_actions_frame->set_model_frame(m);
  m_marks_frame->set_model_frame(m);
  m_snapshot_frame->set_model_frame(m);
} // model_properties_ctrl::set_model_frame()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update windows when the current action is modified.
 */
void bf::model_properties_ctrl::update_action()
{
  m_marks_frame->update_action();
  m_snapshot_frame->update_action();
} // model_properties_ctrl::update_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update windows when the current snapshot is modified.
 */
void bf::model_properties_ctrl::update_snapshot()
{
  m_snapshot_frame->update_snapshot();
} // model_properties_ctrl::update_snapshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update windows when the current mark is modified.
 */
void bf::model_properties_ctrl::update_mark()
{
  m_marks_frame->update_mark();
  m_snapshot_frame->update_mark();
} // model_properties_ctrl::update_mark()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the controls.
 */
void bf::model_properties_ctrl::create_controls()
{
  m_actions_frame = new action_list_frame( this );
  AddPage( m_actions_frame, _("Actions") );

  m_marks_frame = new mark_list_frame( this );
  AddPage( m_marks_frame, _("Marks") );

  m_snapshot_frame = new snapshot_frame( this );
  AddPage( m_snapshot_frame, _("Snapshot") );
} // model_properties_ctrl::create_controls()

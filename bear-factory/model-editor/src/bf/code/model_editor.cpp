/*
    Bear Engine - model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/model_editor.cpp"
 * \brief Implementation of the bf::model_editor class.
 * \author Julien Jorge.
 */
#include "bf/model_editor.hpp"

#include "bf/compiled_file.hpp"
#include "bf/config_frame.hpp"
#include "bf/gui_model.hpp"
#include "bf/image_pool.hpp"
#include "bf/main_frame.hpp"
#include "bf/path_configuration.hpp"
#include "bf/version.hpp"
#include "bf/wx_facilities.hpp"
#include "bf/xml/model_file.hpp"

#include <wx/tooltip.h>
#include <claw/logger.hpp>
#include <claw/exception.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bf::model_editor::model_editor()
  : m_main_frame(NULL)
{

} // model_editor::model_editor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the configuration of the program.
 */
bf::configuration& bf::model_editor::get_config()
{
  return m_config;
} // model_editor::get_config()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the configuration of the program.
 */
const bf::configuration& bf::model_editor::get_config() const
{
  return m_config;
} // model_editor::get_config()

/*----------------------------------------------------------------------------*/
/**
 * \brief Configure the program.
 */
void bf::model_editor::configure()
{
  config_frame dlg(NULL);

  if ( dlg.ShowModal() == wxID_OK )
    update_image_pool();
} // model_editor::configure()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the image pool.
 */
void bf::model_editor::update_image_pool() const
{
  image_pool::get_instance().clear();

  std::list<std::string>::const_iterator it;

  for ( it=path_configuration::get_instance().data_path.begin();
        it!=path_configuration::get_instance().data_path.end(); ++it )
    image_pool::get_instance().scan_directory(*it);
} // model_editor::update_image_pool()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compile a model.
 * \param path The path to the model file.
 */
void bf::model_editor::compile( const wxString& path ) const
{
  model* mdl(NULL);

  try
    {
      xml::model_file reader;
      mdl = reader.load(path);

      if ( check_model(*mdl) )
        {
          compile_model(*mdl, path);
          delete mdl;
        }
      else
        {
          delete mdl;
          claw::exception("Invalid model.");
        }
    }
  catch(...)
    {
      delete mdl;
      throw;
    }
} // model_editor::compile()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update a model.
 * \param path The path to the model file.
 */
void bf::model_editor::update( const wxString& path ) const
{
  gui_model* mdl(NULL);

  try
    {
      xml::model_file mf;
      mdl = mf.load(path);

      std::ofstream f( wx_to_std_string(path).c_str() );
      mf.save(*mdl, f);

      delete mdl;
    }
  catch(...)
    {
      delete mdl;
      throw;
    }
} // model_editor::update()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compile a model.
 * \param mdl The model to compile.
 * \param path The path to the model file.
 * \return true if the compilation went ok.
 */
bool
bf::model_editor::compile_model( const model& mdl, const wxString& path ) const
{
  bool result(true);

  try
    {
      std::string std_path( wx_to_std_string(path) );
      std::size_t pos = std_path.rfind(".mdl");

      if ( pos != std::string::npos )
        std_path = std_path.substr(0, pos);

      std_path += ".cm";

      std::ofstream f( std_path.c_str() );
      if (f)
        {
          compiled_file cf(f);
          mdl.compile(cf);
        }
    }
  catch(...)
    {
      result = false;
    }

  return result;
} // model_editor::compile_model()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the application.
 */
bool bf::model_editor::do_init_app()
{
  init_config();
  update_image_pool();

  m_main_frame = new main_frame;
  m_main_frame->Show();

  load_models();

  return true;
} // model_editor::do_init_app()

/*----------------------------------------------------------------------------*/
/**
 * \brief Minimal initialisation for command line usage.
 */
bool bf::model_editor::do_command_line_init()
{
  init_config();

  return true;
} // model_editor::do_command_line_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load and apply the configuration.
 */
void bf::model_editor::init_config()
{
  m_config.load();

  if ( path_configuration::get_instance().data_path.empty() )
    configure();
} // model_editor::init_config()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if a model is valid.
 * \param mdl The model to check.
 */
bool bf::model_editor::check_model( const model& mdl ) const
{
  bool result(true);
  model::const_action_iterator it;

  for ( it=mdl.action_begin(); it!=mdl.action_end(); ++it )
    if ( !it->get_auto_next().empty() )
      if ( !mdl.has_action( it->get_auto_next() )  )
        {
          result = false;
          claw::logger << claw::log_error << "Unknown action '"
                       << it->get_auto_next() << '\'' << std::endl;
        }

  return result;
} // model_editor::check_model()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the models passed on the command line.
 */
void bf::model_editor::load_models()
{
  for (int i=1; i<argc; ++i)
    if ( wxString(argv[i]) != wxT("--") )
      try
        {
          m_main_frame->load_model( argv[i] );
        }
      catch( std::ios_base::failure& e )
        {
          claw::logger << claw::log_error << e.what()
                       << "\nCreating a new model." << std::endl;
          m_main_frame->new_model( argv[i] );
        }
      catch( std::exception& e )
        {
          claw::logger << claw::log_error << e.what() << std::endl;
        }
} // level_editor::load_levels()

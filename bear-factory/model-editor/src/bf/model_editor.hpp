/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/model_editor.hpp
 * \brief The class representing the application.
 * \author Julien Jorge
 */
#ifndef __BF_MODEL_EDITOR_HPP__
#define __BF_MODEL_EDITOR_HPP__

#include "bf/base_editor_application.hpp"
#include "bf/configuration.hpp"

namespace bf
{
  class main_frame;
  class model;

  /**
   * \brief The class representing the application.
   * \author Julien Jorge
   */
  class model_editor:
    public base_editor_application
  {
  public:
    model_editor();

    configuration& get_config();
    const configuration& get_config() const;

    void configure();
    void update_image_pool() const;

    void compile( const wxString& path ) const;
    void update( const wxString& path ) const;
    bool compile_model( const model& mdl, const wxString& path ) const;

  private:
    bool do_init_app();
    bool do_command_line_init();
    void init_config();

    bool check_model( const model& mdl ) const;

    void load_models();

  private:
    /** \brief The main window of our program. */
    main_frame* m_main_frame;

    /** \brief The configuration of the program. */
    configuration m_config;

  }; // class model_editor
} // namespace bf

#endif // __BF_MODEL_EDITOR_HPP__

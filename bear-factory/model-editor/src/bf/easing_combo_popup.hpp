/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/easing_combo_popup.hpp
 * \brief A combobox containing all the easing functions.
 * \author Julien Jorge
 */
#ifndef __BF_EASING_COMBO_POPUP_HPP__
#define __BF_EASING_COMBO_POPUP_HPP__

#include <wx/combo.h>
#include <wx/listctrl.h>

namespace bf
{
  /**
   * \brief A combobox containing all the easing functions.
   * \author Julien Jorge
   */
  class easing_combo_popup:
    public wxListView,
    public wxComboPopup
  {
  public:
    virtual bool Create(wxWindow* parent);
    virtual wxWindow* GetControl();

    virtual void SetStringValue( const wxString& s );
    virtual wxString GetStringValue() const;

  private:
    void connect_list_events();
    void disconnect_list_events();

    void append_item( const wxString& label, const wxBitmap& img );
    void fill_list();

    void on_item_activated( wxListEvent& event );

  private:

    DECLARE_EVENT_TABLE()

  }; // class easing_combo_popup
} // namespace bf

#endif // __BF_EASING_COMBO_POPUP_HPP__

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/windows_layout.hpp
 * \brief A class that groups all windows common to all models.
 * \author Julien Jorge
 */
#ifndef __BF_WINDOWS_LAYOUT_HPP__
#define __BF_WINDOWS_LAYOUT_HPP__

#include "bf/model.hpp"

#include <set>

class wxWindow;

namespace bf
{
  class main_frame;
  class model_frame;

  /**
   * \brief A class that groups all windows common to all models.
   * \author Julien Jorge
   */
  class windows_layout
  {
  public:
    typedef claw::wrapped_iterator
    < model_frame,
      std::set<model_frame*>::iterator,
      claw::dereference<model_frame>
    >::iterator_type iterator;

  public:
    windows_layout( main_frame& mf );

    wxWindow* get_root_window() const;

    model_frame* get_current_model_frame();
    bool set_current_model_frame( model_frame& m );
    void add_model_frame( model_frame& m );
    void remove_model_frame( model_frame& m );

    void update_action();
    void update_snapshot();
    void update_mark();

    iterator begin();
    iterator end();

  private:
    /** \brief The main window of the program. */
    main_frame& m_main_frame;

    /** \brief All model windows. */
    std::set<model_frame*> m_model_frame;

    /** \brief Active model window. */
    model_frame* m_current_model_frame;

  }; // class windows_layout
} // namespace bf

#endif // __BF_WINDOWS_LAYOUT_HPP__

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/snapshot_date_frame.hpp
 * \brief The window showing the date of a snapshot.
 * \author Sébastien Angibaud
 */
#ifndef __BF_SNAPSHOT_DATE_FRAME_HPP__
#define __BF_SNAPSHOT_DATE_FRAME_HPP__

#include <wx/wx.h>
#include <wx/textctrl.h>

#include "bf/spin_ctrl.hpp"

namespace bf
{
  class action;
  class snapshot;

  /**
   * \brief The snapshot_date window of our program.
   * \author Sébastien Angibaud
   */
  class snapshot_date_frame:
    public wxDialog
  {
  public:
    snapshot_date_frame( wxWindow* parent, const action* a );

    double get_date() const;

    void fill_from( const snapshot* a );

  private:
    void fill_controls();

    void create_controls();
    void create_member_controls();
    void create_sizer_controls();

    void on_ok(wxCommandEvent& event);

  private:
    /** \brief The current action. */
    const action* m_action;

    /** \brief The date of the snapshot. */
    double m_date;

    /** \brief The spin control in which we configure the date of
        the snapshot. */
    spin_ctrl<double>* m_date_spin;

    DECLARE_EVENT_TABLE()

  }; // class snapshot_date_frame
} // namespace bf

#endif // __BF_SNAPSHOT_DATE_FRAME_HPP__

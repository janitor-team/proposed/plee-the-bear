/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/action_properties_frame.hpp
 * \brief The window showing the properties of a action.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_PROPERTIES_FRAME_HPP__
#define __BF_ACTION_PROPERTIES_FRAME_HPP__

#include <wx/wx.h>
#include <wx/textctrl.h>
#include <wx/spinctrl.h>
#include <wx/choice.h>
#include "bf/action.hpp"
#include "bf/spin_ctrl.hpp"
#include "bf/base_file_edit.hpp"

namespace bf
{
  class gui_model;

  /**
   * \brief The action_properties window of our program.
   * \author Sébastien Angibaud
   */
  class action_properties_frame:
    public wxDialog
  {
  public:
    enum control_id
      {
        ID_DURATION_SPIN,
        ID_ALIGNMENT
      }; // enum control_id

  public:
    action_properties_frame( wxWindow* parent, const gui_model* m );

    double get_action_duration() const;
    const std::string& get_action_name() const;
    const sound_description& get_sound() const;
    const std::string& get_auto_next() const;

    void fill_from( const action& a );

  private:
    void fill_controls();

    void create_controls();
    void create_member_controls();
    void create_sizer_controls();

    void on_ok(wxCommandEvent& event);

  private:
    /** \brief The current model. */
    const gui_model* m_model;

    /** \brief The duration of the action. */
    double m_duration;

    /** \brief The name of the action. */
    std::string m_action_name;

    /** \brief The sound played during the action. */
    sound_description m_sound;

    /** \brief The name of the action to start when this one is finished. */
    std::string m_auto_next;

    /** \brief The text control in which we configure the duration of the
        action. */
    spin_ctrl<double>* m_duration_spin;

    /** \brief The text control in which we configure the name of
        the action. */
    wxTextCtrl* m_action_name_box;

    /** \brief The text control in which we configure the sound file. */
    base_file_edit< custom_type< std::string > >* m_sound_file_box;

    /** \brief The control in which we configure the sound. */
    wxCheckBox* m_globally_sound_box;

    /** \brief The text control in which we configure the next action. */
    wxTextCtrl* m_auto_next_text;

    DECLARE_EVENT_TABLE()

  }; // class action_properties_frame
} // namespace bf

#endif // __BF_ACTION_PROPERTIES_FRAME_HPP__

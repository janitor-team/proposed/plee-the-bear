/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/model.hpp
 * \brief A model is a set of actions, and gives default values for some fields.
 * \author Julien Jorge
 */
#ifndef __BF_MODEL_HPP__
#define __BF_MODEL_HPP__

#include "bf/action.hpp"

#include <claw/functional.hpp>
#include <claw/non_copyable.hpp>
#include <claw/iterator.hpp>
#include <list>
#include <string>

namespace bf
{
  class compiled_file;

  /**
   * \brief A model is a set of actions, and gives default values for some
   *        fields.
   * \author Julien Jorge
   */
  class model:
    public claw::pattern::non_copyable
  {
  private:
    /** \brief The structure in which we store the actions. */
    typedef std::list<action*> action_list;

  public:
    typedef claw::wrapped_iterator
    < const action,
      action_list::const_iterator,
      claw::const_dereference<action> >::iterator_type const_action_iterator;

  public:
    model();
    ~model();

    bool empty() const;
    bool has_action( const std::string& name ) const;

    std::string create_unique_action_name( const std::string& prefix ) const;

    action& add_action
    ( const std::string& action_name, double duration,
      const sound_description& sound,
      const std::string& next_action);
    void add_action( action* a );
    action* remove_action( const std::string& name );

    action& get_action( const std::string& name );
    const action& get_action( const std::string& name ) const;

    void compile( compiled_file& f ) const;

    const_action_iterator action_begin() const;
    const_action_iterator action_end() const;

  private:
    /** \brief The actions this model can do. */
    action_list m_actions;

  }; // class model
} // namespace bf

#endif // __BF_MODEL_HPP__

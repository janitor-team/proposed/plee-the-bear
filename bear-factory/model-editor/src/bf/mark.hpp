/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/mark.hpp
 * \brief A mark in the action.
 * \author Julien Jorge
 */
#ifndef __BF_MARK_HPP__
#define __BF_MARK_HPP__

#include <string>
#include <map>

#include "bf/custom_type.hpp"
#include "bf/animation.hpp"
#include "bf/any_animation.hpp"

namespace bf
{
  class compiled_file;

  /**
   * \brief A mark in the model.
   *
   * A mark is present in all actions of a model.
   *
   * \author Julien Jorge
   */
  class mark
  {
  public:
    mark();

    void set_label( const std::string& label );
    const std::string& get_label() const;

    void set_animation( const any_animation& anim );
    const any_animation& get_animation() const;

    bool has_animation() const;
    animation get_animation_data() const;

    void apply_angle_to_animation( bool b );
    bool apply_angle_to_animation() const;

    void pause_animation_when_hidden( bool b );
    bool pause_animation_when_hidden() const;

    void compile
    ( compiled_file& f,
      const std::map<any_animation, std::size_t>& anim_ref ) const;

  private:
    /** \brief The label of the mark. */
    std::string m_label;

    /** \brief The animation centered on this mark. */
    any_animation m_animation;

    /** \brief Tell if the angle applied to the mark is also applied to the
        animation. */
    bool m_apply_angle_to_animation;

    /** \brief Tell if the animation must be paused when the mark is hidden. */
    bool m_pause_animation_when_hidden;

  }; // class mark
} // namespace bf

#endif // __BF_MARK_HPP__

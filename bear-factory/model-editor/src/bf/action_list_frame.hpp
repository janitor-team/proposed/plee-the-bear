/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/action_list_frame.hpp
 * \brief The window displaying the list of actions in the model.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_LIST_FRAME_HPP__
#define __BF_ACTION_LIST_FRAME_HPP__

#include <wx/wx.h>
#include <string>

namespace bf
{
  class ingame_view;
  class model_frame;

  /**
   * \brief The window displaying the list of actions in the model.
   * \author Sébastien Angibaud
   */
  class action_list_frame:
    public wxPanel
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_ACTION_LIST
      }; // enum control_id

  public:
    action_list_frame( wxWindow* parent );

    void set_model_frame( model_frame* f );
    void select_action( const std::string& action_name );

  private:
    void create_controls();
    void create_member_controls();
    void create_sizer_controls();
    void update_controls();

    void clear();
    void fill();

    void on_new_action(wxCommandEvent& event);
    void on_delete(wxCommandEvent& event);
    void on_copy(wxCommandEvent& event);

    void on_show_properties(wxCommandEvent& event);
    void on_select_action(wxCommandEvent& event);

    void on_close(wxCloseEvent& event);

  private:
    /** \brief The model we are working on. */
    model_frame* m_model_frame;

    /** \brief The list of actions, presented to the user. */
    wxListBox* m_action_list;

    /** \brief The button to create a new action. */
    wxButton* m_new_action_button;

    /** \brief The button to delete an action. */
    wxButton* m_delete_action_button;

    /** \brief The button to copy an action. */
    wxButton* m_copy_action_button;

    /** \brief The button to edit properties of an action. */
    wxButton* m_properties_action_button;

    DECLARE_EVENT_TABLE()

  }; // class action_list_frame
} // namespace bf

#endif // __BF_ACTION_LIST_FRAME_HPP__

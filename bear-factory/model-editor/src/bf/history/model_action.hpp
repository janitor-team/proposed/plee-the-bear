/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/model_action.hpp
 * \brief Base class for the actions on a model, that can be undone.
 * \author Sébastien Angibaud
 */
#ifndef __BF_MODEL_ACTION_HPP__
#define __BF_MODEL_ACTION_HPP__

#include <claw/non_copyable.hpp>
#include <wx/string.h>

namespace bf
{
  class gui_model;

  /**
   * \brief Base class for the actions on a model, that can be undone.
   * \author Sébastien Angibaud
   */
  class model_action
    : public claw::pattern::non_copyable
  {
  public:
    /** \brief Destructor. */
    virtual ~model_action() { }

    /**
     * \brief Apply the action to a model.
     * \param mdl The model to which the action is applied.
     */
    virtual void execute( gui_model& mdl ) = 0;

    /**
     * \brief Undo the action.
     * \param mdl The model in which the action is undone.
     */
    virtual void undo( gui_model& mdl ) = 0;

    /** \brief Tell if the action does nothing. */
    virtual bool is_identity( const gui_model& gui ) const = 0;

    /** \brief Get a short description of the action. */
    virtual wxString get_description() const = 0;

  }; // class model_action
} // namespace bf

#endif // __BF_MODEL_ACTION_HPP__

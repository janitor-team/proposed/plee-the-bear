/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_modify_action.hpp
 * \brief The action of modify an action.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_MODIFY_ACTION_HPP__
#define __BF_ACTION_MODIFY_ACTION_HPP__

#include "bf/history/model_action.hpp"
#include "bf/snapshot.hpp"

#include <string>

namespace bf
{
  class action;
  class sound_description;

  /**
   * \brief The action of modify an action.
   * \author Sébastien Angibaud
   */
  class action_modify_action:
    public model_action
  {
  public:
    action_modify_action
    ( action* a, const std::string& name,
      double duration, const sound_description& sound,
      const std::string& auto_next);

    ~action_modify_action();

    /**
     * \remark Calling execute() two times will restore the initial size.
     */
    void execute( gui_model& mdl );
    void undo( gui_model& mdl );

    bool is_identity( const gui_model& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The action to modify. */
    action* m_action;

    /** \brief The new name of the action. */
    std::string m_name;

    /** \brief The new duration of the action. */
    double m_duration;

    /** \brief The sound played during the action. */
    sound_description m_sound;

    /** \brief The name of the action to start when this one is finished. */
    std::string m_auto_next;

    /** \brief List of snapshots that are delete by changing the duration. */
    std::list<snapshot*> m_snapshots;
  }; // class action_modify_action
} // namespace bf

#endif // __BF_ACTION_MODIFY_ACTION_HPP__

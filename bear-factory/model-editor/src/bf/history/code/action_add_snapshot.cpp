/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_add_snapshot.cpp
 * \brief Implementation of the bf::action_add_snapshot class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_add_snapshot.hpp"

#include "bf/gui_model.hpp"

#include <claw/assert.hpp>
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_add_snapshot::action_add_snapshot
(  const std::string& action_name, snapshot* s )
  : m_action_name(action_name), m_snapshot(s), m_done(false)
{

} // action_add_snapshot::action_add_snapshot()

/*----------------------------------------------------------------------------*/
bf::action_add_snapshot::~action_add_snapshot()
{
  /* If the action has not been done, the snapshot must be deleted here.
     Otherwise, it will be deleted by the action. */
  if ( !m_done )
    delete m_snapshot;
} // action_add_snapshot::~action_add_snapshot()

/*----------------------------------------------------------------------------*/
void bf::action_add_snapshot::execute( gui_model& mdl )
{
  CLAW_PRECOND( !m_done );
  CLAW_PRECOND( mdl.has_action(m_action_name) );
  CLAW_PRECOND( !mdl.get_action(m_action_name).has_snapshot(m_snapshot) );

  mdl.add_snapshot( m_action_name, m_snapshot );
  m_done = true;
} // action_add_snapshot::execute()

/*----------------------------------------------------------------------------*/
void bf::action_add_snapshot::undo( gui_model& mdl )
{
  CLAW_PRECOND( m_done );
  CLAW_PRECOND( mdl.has_action(m_action_name) );
  CLAW_PRECOND( mdl.get_action(m_action_name).has_snapshot(m_snapshot) );

  mdl.remove_snapshot( m_action_name, m_snapshot );
  m_done = false;
} // action_add_snapshot::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_add_snapshot::is_identity( const gui_model& mdl ) const
{
  return false;
} // action_add_snapshot::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_add_snapshot::get_description() const
{
  return _("Add snapshot");
} // action_add_snapshot::get_description()

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_modify_snapshot_alignment.cpp
 * \brief Implementation of the bf::action_modify_snapshot_alignment class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_modify_snapshot_alignment.hpp"

#include "bf/gui_model.hpp"
#include "bf/action.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The snapshot.
 * \param x_alignment The type of alignment on x-coordinate.
 * \param x_value The value of alignment on x-coordinate.
 * \param y_alignment The type of alignment on y-coordinate.
 * \param y_value The value of alignment on y-coordinate.
 */
bf::action_modify_snapshot_alignment::action_modify_snapshot_alignment
( snapshot* s, const std::string& x_alignment, double x_value,
  const std::string& y_alignment, double y_value)
  : m_snapshot(s),
    m_x_alignment(x_alignment), m_x_alignment_value(x_value),
    m_y_alignment(y_alignment), m_y_alignment_value(y_value)
{
} // action_modify_snapshot_alignment::action_modify_snapshot_alignment()

/*----------------------------------------------------------------------------*/
void bf::action_modify_snapshot_alignment::execute( gui_model& mdl )
{
  const std::string x_alignment(m_snapshot->get_x_alignment());
  double x_alignment_value(m_snapshot->get_x_alignment_value());
  const std::string y_alignment(m_snapshot->get_y_alignment());
  double y_alignment_value(m_snapshot->get_y_alignment_value());

  m_snapshot->set_x_alignment(m_x_alignment);
  m_snapshot->set_x_alignment_value(m_x_alignment_value);
  m_snapshot->set_y_alignment(m_y_alignment);
  m_snapshot->set_y_alignment_value(m_y_alignment_value);

  m_x_alignment = x_alignment;
  m_x_alignment_value = x_alignment_value;
  m_y_alignment = y_alignment;
  m_y_alignment_value = y_alignment_value;
} // action_modify_snapshot_alignment::execute()

/*----------------------------------------------------------------------------*/
void bf::action_modify_snapshot_alignment::undo( gui_model& mdl )
{
    const std::string x_alignment(m_snapshot->get_x_alignment());
  double x_alignment_value(m_snapshot->get_x_alignment_value());
  const std::string y_alignment(m_snapshot->get_y_alignment());
  double y_alignment_value(m_snapshot->get_y_alignment_value());

  m_snapshot->set_x_alignment(m_x_alignment);
  m_snapshot->set_x_alignment_value(m_x_alignment_value);
  m_snapshot->set_y_alignment(m_y_alignment);
  m_snapshot->set_y_alignment_value(m_y_alignment_value);

  m_x_alignment = x_alignment;
  m_x_alignment_value = x_alignment_value;
  m_y_alignment = y_alignment;
  m_y_alignment_value = y_alignment_value;
} // action_modify_snapshot_alignment::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_modify_snapshot_alignment::is_identity
( const gui_model& mdl ) const
{
  return (m_x_alignment == m_snapshot->get_x_alignment()) &&
    (m_x_alignment_value == m_snapshot->get_x_alignment_value()) &&
    (m_y_alignment == m_snapshot->get_y_alignment()) &&
    (m_y_alignment_value == m_snapshot->get_y_alignment_value());
} // action_modify_snapshot_alignment::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_modify_snapshot_alignment::get_description() const
{
  return _("Set bounding box alignment in snapshot");
} // action_modify_snapshot_alignment::get_description()

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_remove_action.cpp
 * \brief Implementation of the bf::action_remove_action class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_remove_action.hpp"

#include "bf/gui_model.hpp"

#include <claw/assert.hpp>
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_remove_action::action_remove_action
( const std::string& action_name )
  : m_action(NULL), m_action_name(action_name)
{

} // action_remove_action::action_remove_action()

/*----------------------------------------------------------------------------*/
bf::action_remove_action::~action_remove_action()
{
  /* If the action has not been done, the action must be deleted here.
     Otherwise, it will be deleted by the model. */
  if ( m_action != NULL )
    delete m_action;
} // action_remove_action::~action_remove_action()

/*----------------------------------------------------------------------------*/
void bf::action_remove_action::execute( gui_model& mdl )
{
  CLAW_PRECOND( m_action == NULL );
  CLAW_PRECOND( mdl.has_action(m_action_name) );

  m_action = mdl.remove_action( m_action_name );
} // action_remove_action::execute()

/*----------------------------------------------------------------------------*/
void bf::action_remove_action::undo( gui_model& mdl )
{
  CLAW_PRECOND( m_action != NULL );
  CLAW_PRECOND( !mdl.has_action(m_action_name) );

  mdl.add_action( m_action );

  m_action = NULL;
} // action_remove_action::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_remove_action::is_identity( const gui_model& mdl ) const
{
  return false;
} // action_remove_action::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_remove_action::get_description() const
{
  return _("Remove action");
} // action_remove_action::get_description()

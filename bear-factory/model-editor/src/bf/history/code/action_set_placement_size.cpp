/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_set_placement_size.cpp
 * \brief Implementation of the bf::action_set_placement_size class.
 * \author Julien Jorge
 */
#include "bf/history/action_set_placement_size.hpp"

#include "bf/gui_model.hpp"
#include "bf/snapshot.hpp"
#include "bf/mark.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The considered snapshot.
 * \param m The considered mark.
 * \param width The new left position of the mark.
 * \param height The new bottom position of the mark.
 */
bf::action_set_placement_size::action_set_placement_size
( snapshot* s, const mark* m, double width, double height )
  : m_snapshot(s), m_mark(m), m_width(width), m_height(height)
{

} // action_set_placement_size::action_set_placement_size()

/*----------------------------------------------------------------------------*/
void bf::action_set_placement_size::execute( gui_model& mdl )
{
  CLAW_PRECOND( m_mark != NULL );
  CLAW_PRECOND( m_snapshot != NULL );
  CLAW_PRECOND( m_snapshot->has_mark(m_mark) );

  mark_placement& p = m_snapshot->get_placement(m_mark);

  const double width(p.get_width());
  const double height(p.get_height());

  p.set_size( m_width, m_height );

  m_width = width;
  m_height = height;
} // action_set_placement_size::execute()

/*----------------------------------------------------------------------------*/
void bf::action_set_placement_size::undo( gui_model& mdl )
{
  /* the first call to execute saved the initial label and animation. */
  execute(mdl);
} // action_set_placement_size::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_set_placement_size::is_identity
( const gui_model& mdl ) const
{
  const mark_placement& p = m_snapshot->get_placement(m_mark);

  return (p.get_width() == m_width) && (p.get_height() == m_height);
} // action_set_placement_size::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_set_placement_size::get_description() const
{
  return _("Set mark size");
} // action_set_placement_size::get_description()

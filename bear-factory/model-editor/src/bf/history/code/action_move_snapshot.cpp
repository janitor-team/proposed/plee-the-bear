/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_move_snapshot.cpp
 * \brief Implementation of the bf::action_move_snapshot class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_move_snapshot.hpp"

#include "bf/gui_model.hpp"
#include "bf/snapshot.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The snapshot.
 * \param date The new date of the snapshot.
 */
bf::action_move_snapshot::action_move_snapshot
( snapshot* s, double date )
  : m_snapshot(s), m_date(date)
{

} // action_move_snapshot::action_move_snapshot()

/*----------------------------------------------------------------------------*/
void bf::action_move_snapshot::execute( gui_model& mdl )
{
  double d(m_snapshot->get_date());

  m_snapshot->set_date( m_date );

  m_date = d;
} // action_move_snapshot::execute()

/*----------------------------------------------------------------------------*/
void bf::action_move_snapshot::undo( gui_model& mdl )
{
  /* the first call to execute saved the initial position. */
  execute(mdl);
} // action_move_snapshot::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_move_snapshot::is_identity( const gui_model& mdl ) const
{
  return ( m_snapshot->get_date() == m_date );
} // action_move_snapshot::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_move_snapshot::get_description() const
{
  return _("Move snapshot");
} // action_move_snapshot::get_description()

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_set_placement_function.cpp
 * \brief Implementation of the bf::action_set_placement_function class.
 * \author Julien Jorge
 */
#include "bf/history/action_set_placement_function.hpp"

#include "bf/gui_model.hpp"
#include "bf/snapshot.hpp"
#include "bf/mark.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The considered snapshot.
 * \param m The considered mark.
 * \param f The new collision function of the mark.
 */
bf::action_set_placement_function::action_set_placement_function
( snapshot* s, const mark* m, const std::string& f )
  : m_snapshot(s), m_mark(m), m_function(f)
{

} // action_set_placement_function::action_set_placement_function()

/*----------------------------------------------------------------------------*/
void bf::action_set_placement_function::execute( gui_model& mdl )
{
  CLAW_PRECOND( m_mark != NULL );
  CLAW_PRECOND( m_snapshot != NULL );
  CLAW_PRECOND( m_snapshot->has_mark(m_mark) );

  mark_placement& p = m_snapshot->get_placement(m_mark);

  std::string f(p.get_collision_function());
  p.set_collision_function( m_function );
  m_function.swap(f);
} // action_set_placement_function::execute()

/*----------------------------------------------------------------------------*/
void bf::action_set_placement_function::undo( gui_model& mdl )
{
  /* the first call to execute saved the initial label and animation. */
  execute(mdl);
} // action_set_placement_function::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_set_placement_function::is_identity
( const gui_model& mdl ) const
{
  const mark_placement& p = m_snapshot->get_placement(m_mark);

  return p.get_collision_function() == m_function;
} // action_set_placement_function::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_set_placement_function::get_description() const
{
  return _("Set collision function");
} // action_set_placement_function::get_description()

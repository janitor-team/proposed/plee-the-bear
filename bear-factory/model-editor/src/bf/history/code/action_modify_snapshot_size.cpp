/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_modify_snapshot_size.cpp
 * \brief Implementation of the bf::action_modify_snapshot_size class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_modify_snapshot_size.hpp"

#include "bf/gui_model.hpp"
#include "bf/action.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The snapshot.
 * \param width The new width of the snapshot.
 * \param height The new height of the snapshot.
 */
bf::action_modify_snapshot_size::action_modify_snapshot_size
( snapshot* s, const double width, const double height)
  : m_snapshot(s), m_width(width), m_height(height)
{

} // action_modify_snapshot_size::action_modify_snapshot_size()

/*----------------------------------------------------------------------------*/
void bf::action_modify_snapshot_size::execute( gui_model& mdl )
{
  const double width(m_snapshot->get_width());
  const double height(m_snapshot->get_height());

  m_snapshot->set_size(m_width, m_height);

  m_width = width;
  m_height = height;
} // action_modify_snapshot_size::execute()

/*----------------------------------------------------------------------------*/
void bf::action_modify_snapshot_size::undo( gui_model& mdl )
{
  const double width(m_snapshot->get_width());
  const double height(m_snapshot->get_height());

  m_snapshot->set_size(m_width, m_height);

  m_width = width;
  m_height = height;
} // action_modify_snapshot_size::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_modify_snapshot_size::is_identity
( const gui_model& mdl ) const
{
  return (m_snapshot->get_height() == m_height) &&
    (m_snapshot->get_width() == m_width );
} // action_modify_snapshot_size::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_modify_snapshot_size::get_description() const
{
  return _("Set bounding box size in snapshot");
} // action_modify_snapshot_size::get_description()

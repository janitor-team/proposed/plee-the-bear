/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_modify_snapshot_sound.cpp
 * \brief Implementation of the bf::action_modify_snapshot_sound class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_modify_snapshot_sound.hpp"

#include "bf/gui_model.hpp"
#include "bf/action.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The snapshot.
 * \param sound The new sound of the snapshot.
 */
bf::action_modify_snapshot_sound::action_modify_snapshot_sound
( snapshot* s, const sound_description& sound)
  : m_snapshot(s), m_sound(sound)
{

} // action_modify_snapshot_sound::action_modify_snapshot_sound()

/*----------------------------------------------------------------------------*/
bf::action_modify_snapshot_sound::~action_modify_snapshot_sound()
{
} // action_modify_snapshot_sound::~action_modify_snapshot_sound()

/*----------------------------------------------------------------------------*/
void bf::action_modify_snapshot_sound::execute( gui_model& mdl )
{
  const sound_description sound(m_snapshot->get_sound());

  m_snapshot->set_sound(m_sound);

  m_sound = sound;
} // action_modify_snapshot_sound::execute()

/*----------------------------------------------------------------------------*/
void bf::action_modify_snapshot_sound::undo( gui_model& mdl )
{
  const sound_description sound(m_snapshot->get_sound());

  m_snapshot->set_sound(m_sound);

  m_sound = sound;
} // action_modify_snapshot_sound::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_modify_snapshot_sound::is_identity
( const gui_model& mdl ) const
{
  return (m_snapshot->get_sound().get_file() == m_sound.get_file()) &&
    (m_snapshot->get_sound().is_played_globally() ==
     m_sound.is_played_globally() );
} // action_modify_snapshot_sound::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_modify_snapshot_sound::get_description() const
{
  return _("Set snapshot sound");
} // action_modify_snapshot_sound::get_description()

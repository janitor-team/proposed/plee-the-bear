/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_add_mark.cpp
 * \brief Implementation of the bf::action_add_mark class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_add_mark.hpp"

#include "bf/gui_model.hpp"

#include <claw/assert.hpp>
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
bf::action_add_mark::action_add_mark
(  const std::string& action_name, mark* m )
  : m_action_name(action_name), m_mark(m), m_done(false)
{

} // action_add_mark::action_add_mark()

/*----------------------------------------------------------------------------*/
bf::action_add_mark::~action_add_mark()
{
  /* If the action has not been done, the mark must be deleted here. Otherwise,
     it will be deleted by the action. */
  if ( !m_done )
    delete m_mark;
} // action_add_mark::~action_add_mark()

/*----------------------------------------------------------------------------*/
void bf::action_add_mark::execute( gui_model& mdl )
{
  CLAW_PRECOND( !m_done );
  CLAW_PRECOND( mdl.has_action(m_action_name) );
  CLAW_PRECOND( !mdl.get_action(m_action_name).has_mark(m_mark) );

  mdl.add_mark( m_action_name, m_mark );
  m_done = true;
} // action_add_mark::execute()

/*----------------------------------------------------------------------------*/
void bf::action_add_mark::undo( gui_model& mdl )
{
  CLAW_PRECOND( m_done );
  CLAW_PRECOND( mdl.has_action(m_action_name) );
  CLAW_PRECOND( mdl.get_action(m_action_name).has_mark(m_mark) );

  mdl.remove_mark( m_action_name, m_mark );
  m_done = false;
} // action_add_mark::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_add_mark::is_identity( const gui_model& mdl ) const
{
  return false;
} // action_add_mark::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_add_mark::get_description() const
{
  return _("Add mark");
} // action_add_mark::get_description()

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_modify_action.cpp
 * \brief Implementation of the bf::action_modify_action class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_modify_function.hpp"

#include "bf/gui_model.hpp"
#include "bf/snapshot.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The snapshot.
 * \param function The new function of the snapshot.
 */
bf::action_modify_function::action_modify_function
( snapshot* s, const std::string& function )
  : m_snapshot(s), m_function(function)
{

} // action_modify_function::action_modify_function()

/*----------------------------------------------------------------------------*/
/**
 * \remark Calling execute() two times will restore the initial size.
 */
void bf::action_modify_function::execute( gui_model& mdl )
{
  const std::string f(m_snapshot->get_function());

  m_snapshot->set_function( m_function );

  m_function = f;
} // action_modify_function::execute()

/*----------------------------------------------------------------------------*/
void bf::action_modify_function::undo( gui_model& mdl )
{
  /* the first call to execute saved the initial label and animation. */
  execute(mdl);
} // action_modify_function::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_modify_function::is_identity( const gui_model& mdl ) const
{
  return ( m_snapshot->get_function() == m_function );
} // action_modify_function::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_modify_function::get_description() const
{
  return _("Set function called in a snapshot");
} // action_modify_function::get_description()

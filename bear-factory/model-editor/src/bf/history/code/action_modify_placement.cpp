/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_modify_placement.cpp
 * \brief Implementation of the bf::action_modify_placement class.
 * \author Sébastien Angibaud
 */
#include "bf/history/action_modify_placement.hpp"

#include "bf/gui_model.hpp"
#include "bf/snapshot.hpp"
#include "bf/mark.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param s The considered snapshot.
 * \param m The considered mark.
 * \param left The new left position of the mark.
 * \param bottom The new bottom position of the mark.
 * \param depth The new z-ccordinate of the mark.
 * \param angle The new angle of the mark.
 * \param visible The new visibility statut of the mark.
 */
bf::action_modify_placement::action_modify_placement
( snapshot* s, mark* m, double left, double bottom,
  int depth, double angle, bool visible )
  : m_snapshot(s), m_mark(m), m_left(left),
    m_bottom(bottom), m_depth(depth), m_angle(angle), m_visible(visible)
{

} // action_modify_placement::action_modify_placement()

/*----------------------------------------------------------------------------*/
void bf::action_modify_placement::execute( gui_model& mdl )
{
  CLAW_PRECOND( m_mark != NULL );
  CLAW_PRECOND( m_snapshot != NULL );
  CLAW_PRECOND( m_snapshot->has_mark(m_mark) );

  mark_placement& p = m_snapshot->get_placement(m_mark);

  double left(p.get_x_position());
  double bottom(p.get_y_position());
  int depth(p.get_depth_position());
  double angle(p.get_angle());
  bool visible(p.is_visible());

  p.set_position( m_left, m_bottom );
  p.set_depth_position( m_depth );
  p.set_angle( m_angle );
  p.set_visibility( m_visible );

  m_left = left;
  m_bottom = bottom;
  m_depth = depth;
  m_angle = angle;
  m_visible = visible;
} // action_modify_placement::execute()

/*----------------------------------------------------------------------------*/
void bf::action_modify_placement::undo( gui_model& mdl )
{
  /* the first call to execute saved the initial label and animation. */
  execute(mdl);
} // action_modify_placement::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_modify_placement::is_identity
( const gui_model& mdl ) const
{
  mark_placement& p = m_snapshot->get_placement(m_mark);

  return ( (p.get_x_position() == m_left) &&
           (p.get_y_position() == m_bottom) &&
           (p.get_depth_position() == m_depth) &&
           (p.get_angle() == m_angle) &&
           (p.is_visible() == m_visible) );
} // action_modify_placement::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_modify_placement::get_description() const
{
  return _("Set mark placement");
} // action_modify_placement::get_description()

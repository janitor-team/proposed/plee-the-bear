/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/code/action_group.cpp
 * \brief Implementation of the bf::action_group class.
 * \author Julien Jorge
 */
#include "bf/history/action_group.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::action_group::action_group()
  : m_description(_("Group of actions"))
{

} // action_group::action_group()


/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::action_group::~action_group()
{
  action_collection::iterator it;

  for (it=m_actions.begin(); it!=m_actions.end(); ++it)
    delete *it;
} // action_group::~action_group()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the description.
 * \param description The new description.
 */
void bf::action_group::set_description( const wxString& description )
{
  m_description = description;
} // action_group::set_description()


/*----------------------------------------------------------------------------*/
/**
 * \brief Insert an action at the end.
 * \param a The action to add.
 */
void bf::action_group::add_action( model_action* a )
{
  m_actions.push_back(a);
} // action_group::add_action()

/*----------------------------------------------------------------------------*/
void bf::action_group::execute( gui_model& lvl )
{
  action_collection::iterator it;

  for (it=m_actions.begin(); it!=m_actions.end(); ++it)
    (*it)->execute(lvl);
} // action_group::execute()

/*----------------------------------------------------------------------------*/
void bf::action_group::undo( gui_model& lvl )
{
  action_collection::reverse_iterator it;

  for (it=m_actions.rbegin(); it!=m_actions.rend(); ++it)
    (*it)->undo(lvl);
} // action_group::undo()

/*----------------------------------------------------------------------------*/
bool bf::action_group::is_identity( const gui_model& lvl ) const
{
  bool result(true);
  action_collection::const_reverse_iterator it;

  for (it=m_actions.rbegin(); result && (it!=m_actions.rend()); ++it)
    result = (*it)->is_identity(lvl);

  return result;
} // action_group::is_identity()

/*----------------------------------------------------------------------------*/
wxString bf::action_group::get_description() const
{
  return m_description;
} // action_group::get_description()

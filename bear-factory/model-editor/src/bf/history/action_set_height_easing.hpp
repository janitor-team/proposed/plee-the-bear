/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_set_height_easing.hpp
 * \brief The action of modifying the easing function for the height.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_SET_HEIGHT_EASING_HPP__
#define __BF_ACTION_SET_HEIGHT_EASING_HPP__

#include "bf/history/model_action.hpp"

#include "easing.hpp"

namespace bf
{
  class snapshot;
  class mark;

  /**
   * \brief The action of modifying the easing function for the height.
   * \author Julien Jorge
   */
  class action_set_height_easing:
    public model_action
  {
  public:
    action_set_height_easing( snapshot* s, const mark* m, const bear::easing& e );

    void execute( gui_model& mdl );
    void undo( gui_model& mdl );

    bool is_identity( const gui_model& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The snapshot containing the mark. */
    snapshot* m_snapshot;

    /** \brief The considered mark. */
    const mark* m_mark;

    /** \brief The new easing. */
    bear::easing m_easing;

  }; // class action_set_height_easing
} // namespace bf

#endif // __BF_ACTION_SET_HEIGHT_EASING_HPP__

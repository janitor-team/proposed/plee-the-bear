/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_modify_snapshot_size.hpp
 * \brief The action of modify the size of the snpashot.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_MODIFY_SNAPSHOT_SIZE_HPP__
#define __BF_ACTION_MODIFY_SNAPSHOT_SIZE_HPP__

#include "bf/history/model_action.hpp"
#include "bf/snapshot.hpp"

#include <string>

namespace bf
{
  class snapshot;

  /**
   * \brief The action of modify the size of a snapshot.
   * \author Sébastien Angibaud
   */
  class action_modify_snapshot_size:
    public model_action
  {
  public:
    action_modify_snapshot_size
    ( snapshot* s, const double width, const double heigth );

    /**
     * \remark Calling execute() two times will restore the initial size.
     */
    void execute( gui_model& mdl );
    void undo( gui_model& mdl );

    bool is_identity( const gui_model& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The snapshot to modify. */
    snapshot* m_snapshot;

    /** \brief The width of the snapshot. */
    double m_width;

    /** \brief The height of the snapshot. */
    double m_height;
  }; // class action_modify_snapshot_size
} // namespace bf

#endif // __BF_ACTION_MODIFY_SNAPSHOT_SIZE_HPP__

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_modify_mark.hpp
 * \brief The action of modify a mark.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_MODIFY_MARK_HPP__
#define __BF_ACTION_MODIFY_MARK_HPP__

#include "bf/history/model_action.hpp"
#include "bf/any_animation_edit.hpp"

#include <string>

namespace bf
{
  class mark;

  /**
   * \brief The action of modify an mark.
   * \author Sébastien Angibaud
   */
  class action_modify_mark:
    public model_action
  {
  public:
    action_modify_mark
    ( mark* m, const std::string& label, const bf::any_animation& animation,
      bool apply_angle_to_animation, bool pause_hidden );

    void execute( gui_model& mdl );
    void undo( gui_model& mdl );

    bool is_identity( const gui_model& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The mark to modify. */
    mark* m_mark;

    /** \brief The new label of the mark. */
    std::string m_label;

    /** \brief The new animation of the mark. */
    bf::any_animation m_animation;

    /** \brief Tell if the angle applied to the mark is also applied to the
        animation. */
    bool m_apply_angle_to_animation;

    /** \brief Tell if the animation must be paused when the mark is hidden. */
    bool m_pause_animation_when_hidden;

  }; // class action_modify_mark
} // namespace bf

#endif // __BF_ACTION_MODIFY_MARK_HPP__

/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/model_history.hpp
 * \brief A class to manage undo/redo informations on a model.
 * \author Sébastien Angibaud
 */
#ifndef __BF_MODEL_HISTORY_HPP__
#define __BF_MODEL_HISTORY_HPP__

#include <wx/string.h>
#include <list>

namespace bf
{
  class gui_model;
  class model_action;

  /**
   * \brief A class to manage undo/redo informations on a model.
   * \author Sébastien Angibaud
   */
  class model_history
  {
  public:
    model_history( gui_model* mdl );
    ~model_history();

    bool can_undo() const;
    bool can_redo() const;

    void undo();
    void redo();

    bool do_action( model_action* action );

    wxString get_undo_description() const;
    wxString get_redo_description() const;

    void set_saved();
    bool model_is_saved() const;

    gui_model& get_model();
    const gui_model& get_model() const;

  private:
    void clear_past();
    void clear_future();

  private:
    /** \brief The model on which we are working. */
    gui_model* m_model;

    /** \brief The actions done in the past. */
    std::list<model_action*> m_past;

    /** \brief The actions in the future. */
    std::list<model_action*> m_future;

    /** \brief Maximum size of the undo history. */
    std::size_t m_max_history;

    /** \brief The last action done before saving. */
    model_action* m_saved_action;

  }; // class model_history
} // namespace bf

#endif // __BF_MODEL_HISTORY_HPP__

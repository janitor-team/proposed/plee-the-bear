/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_group.hpp
 * \brief An action made of sub actions.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_GROUP_HPP__
#define __BF_ACTION_GROUP_HPP__

#include "bf/history/model_action.hpp"

#include <list>

namespace bf
{
  /**
   * \brief The action made of sub actions.
   * \author Julien Jorge
   */
  class action_group:
    public model_action
  {
  private:
    /** \brief The type of the collection of sub actions. */
    typedef std::list<model_action*> action_collection;

  public:
    action_group();
    ~action_group();

    void set_description( const wxString& description );

    void add_action( model_action* a );

    void execute( gui_model& lvl );
    void undo( gui_model& lvl );

    bool is_identity( const gui_model& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The sub actions. */
    action_collection m_actions;

    /** \brief The descriptions of the group. */
    wxString m_description;

  }; // class action_group
} // namespace bf

#endif // __BF_ACTION_GROUP_HPP__

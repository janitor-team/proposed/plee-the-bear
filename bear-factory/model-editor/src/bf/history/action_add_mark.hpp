/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_add_mark.hpp
 * \brief The action of adding mark in an action.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_ADD_MARK_HPP__
#define __BF_ACTION_ADD_MARK_HPP__

#include "bf/history/model_action.hpp"

#include "bf/mark.hpp"

namespace bf
{
  class action;

  /**
   * \brief The action of adding a mark in an action.
   * \author Sébastien Angibaud
   */
  class action_add_mark:
    public model_action
  {
  public:
    /**
     * \brief Constructor.
     * \param action_name The current action.
     * \param m The new mark.
     */
    action_add_mark( const std::string& action_name, mark* m);

    /** \brief Destructor. */
    ~action_add_mark();

    void execute( gui_model& mdl );
    void undo( gui_model& mdl );

    bool is_identity( const gui_model& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The current action. */
    const std::string m_action_name;

    /** \brief The lmark to add. */
    mark* m_mark;

    /** \brief Tell if the action has been done or not. */
    bool m_done;
  }; // class action_add_mark
} // namespace bf

#endif // __BF_ACTION_ADD_MARK_HPP__

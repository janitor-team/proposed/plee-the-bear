/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/history/action_add_action.hpp
 * \brief The action of adding action in a model.
 * \author Sébastien Angibaud
 */
#ifndef __BF_ACTION_ADD_ACTION_HPP__
#define __BF_ACTION_ADD_ACTION_HPP__

#include "bf/history/model_action.hpp"
#include <string>

namespace bf
{
  class action;

  /**
   * \brief The action of adding a action in a model.
   * \author Sébastien Angibaud
   */
  class action_add_action:
    public model_action
  {
  public:
    /**
     * \brief Constructor.
     * \param a The action to add.
     */
    action_add_action( action* a );

    /** \brief Destructor. */
    ~action_add_action();

    void execute( gui_model& mdl );
    void undo( gui_model& mdl );

    bool is_identity( const gui_model& gui ) const;
    wxString get_description() const;

  private:
    /** \brief The action to add. */
    action* m_action;

    /** \brief The name of the action. */
    std::string m_action_name;

  }; // class action_add_action
} // namespace bf

#endif // __BF_ACTION_ADD_ACTION_HPP__

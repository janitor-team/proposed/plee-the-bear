/*
    Bear Engine - Model editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/action_player.hpp
 * \brief This class helps getting the sprites of an action at a given date and
 *        moving through an action when playing it.
 * \author Julien Jorge
 */
#ifndef __BF_ACTION_PLAYER_HPP__
#define __BF_ACTION_PLAYER_HPP__

#include "bf/animation_player.hpp"

#include <map>

namespace bf
{
  class action;
  class mark;

  /**
   * \brief This class helps getting the sprites of an action at a given date
   *        and moving through an action when playing it.
   * \author Julien Jorge
   */
  class action_player
  {
  private:
    /** \brief A player for each mark. */
    typedef std::map<const mark*, animation_player> player_map;

    /** \brief The sprites displayed at a given date. */
    typedef std::map<const mark*, sprite> sprite_map;

  public:
    action_player( const action* a = NULL );

    void set_action( const action* a );

    void reset();
    void next();
    void next( double d );
    double get_date() const;
    void set_date( double d );
    double get_duration_until_next() const;

    sprite get_current_sprite_for( const mark* m ) const;

    bool is_finished() const;

  private:
    void fill_sprites();

  private:
    /** \brief The action played. */
    const action* m_action;

    /** \brief The players for the action. */
    player_map m_player;

    /** \brief The current date. */
    double m_date;

    /** \brief The sprite of the marks at the current date. */
    sprite_map m_sprite;

  }; // class action_player
} // namespace bf

#endif // __BF_ACTION_PLAYER_HPP__

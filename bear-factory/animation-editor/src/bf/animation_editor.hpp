/*
    Bear Engine - Animation editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/animation_editor.hpp
 * \brief The class representing the application.
 * \author Julien Jorge
 */
#ifndef __BF_ANIMATION_EDITOR_HPP__
#define __BF_ANIMATION_EDITOR_HPP__

#include "bf/animation.hpp"
#include "bf/base_editor_application.hpp"
#include "bf/configuration.hpp"

namespace bf
{
  /**
   * \brief The class representing the application.
   * \author Julien Jorge
   */
  class animation_editor:
    public base_editor_application
  {
  public:
    void configure();
    void update_image_pool() const;
    void set_main_rect( const wxRect& r );

    void compile( const wxString& path ) const;
    void update( const wxString& path ) const;
    void compile_animation( const animation& anim, const wxString& path ) const;

  private:
    bool do_init_app();
    void init_config();

  private:
    /** \brief The configuration of the program. */
    configuration m_config;

  }; // class animation_editor
} // namespace bf

#endif // __BF_ANIMATION_EDITOR_HPP__

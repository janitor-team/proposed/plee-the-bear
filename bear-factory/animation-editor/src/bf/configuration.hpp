/*
    Bear Engine - Animation editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/configuration.hpp
 * \brief The configuration of our program.
 * \author Julien Jorge
 */
#ifndef __BF_CONFIGURATION_HPP__
#define __BF_CONFIGURATION_HPP__

#include <string>
#include <list>
#include <claw/configuration_file.hpp>
#include <wx/gdicmn.h>

namespace bf
{
  /**
   * \brief The configuration of our program.
   * \author Julien Jorge
   */
  class configuration
  {
  public:
    configuration();

    void load();
    void save() const;

  private:
    bool create_config_file() const;

    wxRect read_rect( const claw::configuration_file& config,
                      const std::string& section ) const;
    void write_rect( std::ostream& f, const wxRect& r ) const;

  public:
    /** \brief Position and size of the main window. */
    wxRect main_rect;

  private:
    /** \brief The name of the configuration file. */
    static const std::string s_config_file_name;

    /** \brief Character written on the left of a section name in the
        configuration file. */
    static const char s_section_left;

    /** \brief Character written on the right of a section name in the
        configuration file. */
    static const char s_section_right;

    /** \brief Character used for comments in the configuration file. */
    static const char s_comment;

    /** \brief Character used for assigning a value to a field. */
    static const char s_field_assign;

    /** \brief Section of the main frame. */
    static const std::string s_main_frame_section;

    /** \brief Label of the x-position fields. */
    static const std::string s_x_field;

    /** \brief Label of the y-position fields. */
    static const std::string s_y_field;

    /** \brief Label of the width fields. */
    static const std::string s_width_field;

    /** \brief Label of the height fields. */
    static const std::string s_height_field;

  }; // class configuration
} // namespace bf

#endif // __BF_CONFIGURATION_HPP__

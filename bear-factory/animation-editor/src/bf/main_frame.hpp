/*
    Bear Engine - Animation editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/main_frame.hpp
 * \brief The main window of our program.
 * \author Julien Jorge
 */
#ifndef __BF_MAIN_FRAME_HPP__
#define __BF_MAIN_FRAME_HPP__

#include <wx/wx.h>

#include "bf/animation_edit.hpp"

namespace bf
{
  /**
   * \brief The main window of our program.
   * \author Julien Jorge
   */
  class main_frame:
    public wxFrame
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        ID_UPDATE_IMAGE_POOL,
        ID_COMPILE,
        ID_SCALE_DURATION,
        ID_CHANGE_DURATION
      }; // enum control_id

  public:
    main_frame();

    void load_animation( const wxString& path );
    void update_image_pool() const;

  private:
    void make_title();
    bool is_changed() const;

    bool save();
    bool save_as();
    bool effective_save();

    void turn_animation_menu_entries( bool b );

    void create_menu();
    void create_toolbar();
    void create_controls();

    wxMenu* create_animation_menu() const;
    wxMenu* create_edit_menu() const;
    wxMenu* create_help_menu() const;

    void save_config();

    void compile_animation();
    void compile_animation_no_check();

    void on_configuration_menu(wxCommandEvent& event);
    void on_update_image_pool_menu(wxCommandEvent& event);
    void on_new_animation(wxCommandEvent& event);
    void on_open_animation(wxCommandEvent& event);
    void on_save(wxCommandEvent& event);
    void on_save_as(wxCommandEvent& event);
    void on_compile(wxCommandEvent& event);
    void on_exit(wxCommandEvent& event);

    void on_change_duration(wxCommandEvent& event);

    void on_online_doc(wxCommandEvent& event);
    void on_about(wxCommandEvent& event);
    void on_close(wxCloseEvent& event);

  private:
    /** \brief The animation editor. */
    animation_edit* m_animation_edit;

    /** \brief The last saved animation. */
    animation m_last_saved_animation;

    /** \brief The path to the animation file. */
    wxString m_animation_file;

    DECLARE_EVENT_TABLE()

  }; // class main_frame
} // namespace bf

#endif // __BF_MAIN_FRAME_HPP__

/*
    Bear Engine - Animation editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/frame_duration_dialog.cpp
 * \brief Implementation of the bf::frame_duration_dialog class.
 * \author Julien Jorge
 */
#include "bf/frame_duration_dialog.hpp"

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param parent Parent window.
 */
bf::frame_duration_dialog::frame_duration_dialog( wxWindow* parent )
  : wxDialog(parent, wxID_ANY, wxT("Change frame durations"), wxDefaultPosition,
             wxDefaultSize)
{
  create_controls();
  Fit();
} // frame_duration_dialog::frame_duration_dialog()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the value of the operand applied to the frames.
 */
double bf::frame_duration_dialog::get_operand() const
{
  return m_operand->GetValue();
} // frame_duration_dialog::get_operand()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell how to apply the operand to the durations.
 */
bf::frame_duration_dialog::duration_application_mode
bf::frame_duration_dialog::get_mode() const
{
  duration_application_mode result(duration_replace);

  switch ( m_mode->GetSelection() )
    {
    case 0: result = duration_replace; break;
    case 1: result = duration_add; break;
    case 2: result = duration_multiply; break;
    }

  return result;
} // frame_duration_dialog::get_mode()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the controls of the dialog.
 */
void bf::frame_duration_dialog::create_controls()
{
  wxBoxSizer* sizer = new wxBoxSizer( wxVERTICAL );

  wxArrayString choices;
  choices.Add(_("Replace previous duration."));
  choices.Add(_("Add to previous duration."));
  choices.Add(_("Multiply previous duration."));

  m_mode = new wxRadioBox
    (this, wxID_ANY, _("Application mode"), wxDefaultPosition, wxDefaultSize,
     choices);
  m_mode->SetSelection(0);

  m_operand = new spin_ctrl<double>(this);
  m_operand->SetRange(0, std::numeric_limits<double>::max());
  m_operand->SetValue(1);
  m_operand->SetStep(0.1);

  sizer->Add( m_mode, 1, wxEXPAND | wxALL, 5 );
  sizer->Add( m_operand, 0, wxEXPAND | wxALL, 5 );

  sizer->Add
    ( CreateStdDialogButtonSizer(wxOK | wxCANCEL), 0, wxALL | wxCENTER, 5 );

  SetSizer(sizer);
} // frame_duration_dialog::create_controls()

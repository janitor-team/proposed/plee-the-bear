/*
    Bear Engine - Animation editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/about_dialog.cpp
 * \brief Implementation of the bf::about_dialog class.
 * \author Julien Jorge
 */
#include "bf/about_dialog.hpp"

#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>

#include "bf/version.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param parent The parent window.
 */
bf::about_dialog::about_dialog( wxWindow& parent )
  : wxDialog( &parent, wxID_ANY, _("About"), wxDefaultPosition,
              wxSize(300, 200), wxDEFAULT_DIALOG_STYLE )
{
  create_controls();
} // about_dialog::about_dialog()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the controls.
 */
void bf::about_dialog::create_controls()
{
  wxBoxSizer* sizer = new wxBoxSizer( wxVERTICAL );
  wxStaticText* text =
    new wxStaticText( this, wxID_ANY, _("Bear Factory"), wxDefaultPosition,
                      wxDefaultSize, wxALIGN_CENTRE );
  wxFont fnt;
  fnt.SetPointSize(20);
  fnt.SetWeight(wxFONTWEIGHT_BOLD);
  text->SetFont(fnt);

  sizer->Add( text, 0, wxEXPAND | wxALL, 5 );

  text = new wxStaticText
    ( this, wxID_ANY,
      wxString::Format(_("Release %d.%d.%d, Animation editor"),
                       BF_MAJOR_VERSION, BF_MINOR_VERSION, BF_RELEASE_NUMBER),
      wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );

  sizer->Add( text, 0, wxEXPAND | wxALL, 5 );

  const wxString contrib_text =
    wxT("Copyright\n")
    wxT("\tS\u00E9bastien Angibaud\n")
    wxT("\tJulien Jorge\n");

  wxTextCtrl* contrib =
    new wxTextCtrl( this, wxID_ANY, contrib_text, wxDefaultPosition,
                    wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY );

  sizer->Add( contrib, 1, wxEXPAND | wxALL, 5 );
  sizer->Add( CreateStdDialogButtonSizer(wxOK), 0, wxALL | wxCENTER, 5 );

  SetSizer(sizer);
} // about_dialog::create_controls()

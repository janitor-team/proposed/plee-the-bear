/*
    Bear Engine - Animation editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/frame_duration_dialog.hpp
 * \brief A dialog shown to change the duration of all frames at once.
 * \author Julien Jorge
 */
#ifndef __BF_FRAME_DURATION_DIALOG_HPP__
#define __BF_FRAME_DURATION_DIALOG_HPP__

#include "bf/spin_ctrl.hpp"

namespace bf
{
  /**
   * \brief A dialog shown to change the duration of all frames at once.
   * \author Julien Jorge
   */
  class frame_duration_dialog:
    public wxDialog
  {
  public:
    /** \brief How the value must be applied to the frames. */
    enum duration_application_mode
      {
        duration_replace,
        duration_add,
        duration_multiply
      }; // enum duration_application_mode

  public:
    frame_duration_dialog( wxWindow* parent );

    double get_operand() const;
    duration_application_mode get_mode() const;

  private:
    void create_controls();

  private:
    /** \brief How to apply the operand to the durations. */
    wxRadioBox* m_mode;

    /** \brief The operand applied to the frames. */
    spin_ctrl<double>* m_operand;

  }; // class frame_duration_dialog
} // namespace bf

#endif // __BF_FRAME_DURATION_DIALOG_HPP__

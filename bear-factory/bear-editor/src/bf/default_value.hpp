/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/default_value.hpp
 * \brief Default values for the types used in a level file.
 * \author Julien Jorge
 */
#ifndef __BF_DEFAULT_VALUE_HPP__
#define __BF_DEFAULT_VALUE_HPP__

#include "bf/custom_type.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief Default values for the types used in a level file.
   * \author Julien Jorge
   */
  template<typename Type>
  class default_value
  {
  public:
    static Type get() { return Type(); }
  }; // class default_value

  /**
   * \brief Default values for the types used in a level file. Specialisation
   *        for string_type.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT default_value<string_type>
  {
  public:
    static string_type get() { return string_type(); }
  }; // class default_value [string_type]

  /**
   * \brief Default values for the types used in a level file. Specialisation
   *        others custom types.
   * \author Julien Jorge
   */
  template<typename T>
  class default_value< custom_type<T> >
  {
  public:
    static custom_type<T> get() { return custom_type<T>(0); }
  }; // class default_value [custom_type]

} // namespace bf

#endif // __BF_DEFAULT_VALUE_HPP__

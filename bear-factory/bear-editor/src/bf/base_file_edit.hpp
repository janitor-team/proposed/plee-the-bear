/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/base_file_edit.hpp
 * \brief Base control for editing a field whose value is a path to a file.
 * \author Julien Jorge
 */
#ifndef __BF_BASE_FILE_EDIT_HPP__
#define __BF_BASE_FILE_EDIT_HPP__

#include "bf/simple_edit.hpp"

#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/window.h>

namespace bf
{
  /**
   * \brief Base control for editing a field whose value is a path to a file.
   * \author Julien Jorge
   */
  template<typename T>
  class base_file_edit:
    public simple_edit<T>,
    public wxPanel
  {
  public:
    /** \brief The type of the content of the file. */
    typedef T file_type;

    /** \brief The type of the current class. */
    typedef base_file_edit<T> self_type;

    /** \brief The type of the parent edit. */
    typedef simple_edit<T> super;

    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_BROWSE
      }; // enum control_id

  public:
    base_file_edit
    ( wxWindow& parent, const wxString& filter, const file_type& v );

    bool validate();

  private:
    void value_updated();

    void create_controls();

    void on_browse(wxCommandEvent& event);

  private:
    /** \brief The filter of the file selection dialog. */
    wxString m_filter;

    /** \brief Text control in which we display the path to the file. */
    wxTextCtrl* m_path;

    /** \brief A button that opens the file selection dialog. */
    wxButton* m_browse;

  }; // class base_file_edit
} // namespace bf

#include "bf/impl/base_file_edit.tpp"

#endif // __BF_BASE_FILE_EDIT_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sprite_view_ctrl.hpp
 * \brief This control displays a sprite and some buttons to adjust its size.
 * \author Julien Jorge
 */
#ifndef __BF_SPRITE_VIEW_CTRL_HPP__
#define __BF_SPRITE_VIEW_CTRL_HPP__

#include "bf/sprite.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/combobox.h>
#include <wx/panel.h>
#include <wx/scrolbar.h>

namespace bf
{
  class sprite_view;

  /**
   * \brief This control displays a sprite and some buttons to adjust its size.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT sprite_view_ctrl:
    public wxPanel
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        ID_ZOOM_100,
        ID_ZOOM_FIT,
        ID_ZOOM_IN,
        ID_ZOOM_OUT,
        ID_COMBO_ZOOM
      }; // enum control_id

  public:
    sprite_view_ctrl( wxWindow& parent, const sprite& spr = sprite() );

    void set_sprite( const sprite& spr );

  private:
    void create_controls();
    void create_sizers();

    void set_zoom_from_combo();

    void adjust_scrollbars();

    void on_zoom_100( wxCommandEvent& event );
    void on_zoom_fit( wxCommandEvent& event );
    void on_zoom_in( wxCommandEvent& event );
    void on_zoom_out( wxCommandEvent& event );
    void on_zoom_selection( wxCommandEvent& event );
    void on_scroll(wxScrollEvent& event);
    void on_size(wxSizeEvent& event);

  private:
    /** \brief The control that displays the sprite. */
    sprite_view* m_sprite_view;

    /** \brief The combo box with the zoom ratio. */
    wxComboBox* m_combo_zoom;

    /** \brief Horizontal scrollbar to scroll the sprite. */
    wxScrollBar* m_h_scrollbar;

    /** \brief Vertical scrollbar to scroll the sprite. */
    wxScrollBar* m_v_scrollbar;

    DECLARE_EVENT_TABLE()

  }; // class sprite_view_ctrl
} // namespace bf

#endif // __BF_SPRITE_VIEW_CTRL_HPP__

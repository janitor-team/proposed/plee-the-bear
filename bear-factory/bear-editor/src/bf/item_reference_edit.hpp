/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_reference_edit.hpp
 * \brief A control for choosing an item reference.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_REFERENCE_EDIT_HPP__
#define __BF_ITEM_REFERENCE_EDIT_HPP__

#include "bf/simple_edit.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/combobox.h>

namespace bf
{
  /**
   * \brief A control for choosing an item reference.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT item_reference_edit:
    public simple_edit<item_reference_type>,
    public wxComboBox
  {
 public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_PATTERN_TEXT
      }; // enum control_id

    /** The type of the current class. */
    typedef item_reference_edit self_type;

    /** \brief The type of the value of the edited field. */
    typedef simple_edit<item_reference_type>::value_type value_type;

  public:
    item_reference_edit
    ( wxWindow& parent, const wxArrayString& choices, const value_type& v );

    bool validate();

  private:
    void value_updated();
    void fill_id_list();
    void on_pattern_change( wxCommandEvent& event );

  private:
    /* \brief The choices of identifiers. */
    const wxArrayString m_choices;

    DECLARE_EVENT_TABLE()

  }; // class item_reference_edit
} // namespace bf

#endif // __BF_ITEM_REFERENCE_EDIT_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/interval_edit.hpp
 * \brief A control for editing a field that takes its value in an interval.
 * \author Julien Jorge
 */
#ifndef __BF_INTERVAL_EDIT_HPP__
#define __BF_INTERVAL_EDIT_HPP__

#include "bf/simple_edit.hpp"
#include "bf/spin_ctrl.hpp"
#include "bf/default_value.hpp"

namespace bf
{
  /**
   * \brief A control for editing a field that can takes ites value in an
   *        interval.
   * \author Julien Jorge
   */
  template<typename Type>
  class interval_edit:
    public simple_edit<Type>,
    public spin_ctrl<typename Type::value_type>
  {
  public:
    /** The type of the current class. */
    typedef interval_edit<Type> self_type;

    /** \brief The type of the value of the edited field. */
    typedef typename simple_edit<Type>::value_type value_type;

    /** \brief The type of the parent edit. */
    typedef simple_edit<Type> super_edit;

    /** \brief The type of the parent control. */
    typedef spin_ctrl<typename Type::value_type> super_ctrl;

  public:
    interval_edit
    ( wxWindow& parent, const value_type& min, const value_type& max,
      const value_type& v = default_value<Type>::get() );

    bool validate();

  private:
    void value_updated();

  }; // class interval_edit
} // namespace bf

#include "bf/impl/interval_edit.tpp"

#endif // __BF_INTERVAL_EDIT_HPP__

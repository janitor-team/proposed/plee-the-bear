/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/exception.hpp
 * \brief Various exceptions thrown when parsing an XML file.
 */
#ifndef __BF_XML_EXCEPTION_HPP__
#define __BF_XML_EXCEPTION_HPP__

#include "bf/libeditor_export.hpp"

#include <exception>
#include <string>

namespace bf
{
  namespace xml
  {
    /**
     * \brief Exception thrown when the parser find an unexpected node.
     * \author Julien Jorge
     */
    class BEAR_EDITOR_EXPORT bad_node:
      public std::exception
    {
    public:
      bad_node( const std::string& node_name );
      virtual ~bad_node() throw();

      virtual const char* what() const throw();

    private:
      /** \brief A short explanation of the problem. */
      const std::string m_msg;

    }; // class bad_node

    /**
     * \brief Exception thrown when the parser does not find an needed node.
     * \author Julien Jorge
     */
    class BEAR_EDITOR_EXPORT missing_node:
      public std::exception
    {
    public:
      missing_node( const std::string& property_name );
      virtual ~missing_node() throw();

      virtual const char* what() const throw();

    private:
      /** \brief A short explanation of the problem. */
      const std::string m_msg;

    }; // class missing_node

    /**
     * \brief Exception thrown when the parser does not find an needed property.
     * \author Julien Jorge
     */
    class BEAR_EDITOR_EXPORT missing_property:
      public std::exception
    {
    public:
      missing_property( const std::string& property_name );
      virtual ~missing_property() throw();

      virtual const char* what() const throw();

    private:
      /** \brief A short explanation of the problem. */
      const std::string m_msg;

    }; // class missing_property

    /**
     * \brief Exception thrown when the parser find a not supported value.
     * \author Julien Jorge
     */
    class BEAR_EDITOR_EXPORT bad_value:
      public std::exception
    {
    public:
      bad_value( const std::string& value );
      bad_value( const std::string& type, const std::string& value );
      virtual ~bad_value() throw();

      virtual const char* what() const throw();

    private:
      /** \brief A short explanation of the problem. */
      const std::string m_msg;

    }; // class bad_value
  } // namespace xml
} // namespace bf

#endif // __BF_XML_EXCEPTION_HPP__


/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/impl/xml_to_value.tpp
 * \brief Implementation of the template methods of the bf::xml::xml_to_value
 *        class.
 * \author Julien Jorge
 */

#include "bf/stream_conv.hpp"
#include "bf/xml/exception.hpp"
#include "bf/wx_facilities.hpp"

#include <claw/assert.hpp>
#include <sstream>

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a xml value node.
 * \param v (out) The value we have read.
 * \param node The node from which we read the value.
 */
template<typename Type>
void bf::xml::xml_to_value<Type>::operator()
  ( Type& v, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node != NULL );
  wxString val;

  if ( !node->GetPropVal( wxT("value"), &val ) )
    throw missing_property( "value" );

  const std::string std_val( wx_to_std_string(val) );
  std::istringstream iss( std_val );

  if ( !stream_conv<Type>::read(iss, v) )
    throw bad_value( wx_to_std_string(node->GetName()), std_val );
} // xml_to_value::operator()()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/impl/value_to_xml.tpp
 * \brief Implementation of the template methods of the bf::xml::value_to_xml
 *        class.
 * \author Julien Jorge
 */
#include "bf/xml/util.hpp"
#include <sstream>

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a node for a simple value.
 * \param os The stream in which we save the value.
 * \param node_name The name of the xml node.
 * \param v The value to write.
 */
template<typename Type>
void bf::xml::value_to_xml<Type>::write
( std::ostream& os, const std::string& node_name, const Type& v )
{
  std::ostringstream oss;
  oss << v.get_value();

  os << "<" << util::replace_special_characters(node_name) << " value='"
     << util::replace_special_characters(oss.str()) << "'/>\n";
} // value_to_xml::write()

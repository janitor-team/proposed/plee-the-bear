/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/code/item_class_inherit_node.cpp
 * \brief Implementation of the bf::xml::item_class_inherit_node class.
 * \author Julien Jorge
 */
#include "bf/xml/item_class_inherit_node.hpp"

#include "bf/item_class.hpp"
#include "bf/item_class_pool.hpp"
#include "bf/wx_facilities.hpp"
#include "bf/xml/reader_tool.hpp"

#include <claw/assert.hpp>
#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Read an xml node "inherit".
 * \param pool The pool of item classes in which we take the parent classes.
 * \param item (out) The item class for we read the hierarchy.
 * \param node The node.
 */
void bf::xml::item_class_inherit_node::read
( const item_class_pool& pool, item_class& item, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node!=NULL );
  CLAW_PRECOND( node->GetName() == wxT("inherit") );

  node = reader_tool::skip_comments(node->GetChildren());

  while ( node!=NULL )
    {
      if ( node->GetName() == wxT("class") )
        item.add_super_class
          ( pool.get_item_class_ptr(wx_to_std_string(node->GetNodeContent())) );
      else
        claw::logger << claw::log_warning << "Ignored node '"
                     << wx_to_std_string(node->GetName()) << "'" << std::endl;

      node = reader_tool::skip_comments(node->GetNext());
    }
} // item_class_inherit_node::read()

/*----------------------------------------------------------------------------*/
/**
 * \brief Write an xml node "inherit".
 * \param item The item class to write.
 * \param os The stream in which we write.
 */
void bf::xml::item_class_inherit_node::write
( const item_class& item, std::ostream& os ) const
{
  item_class::const_super_class_iterator it;
  const item_class::const_super_class_iterator eit( item.super_class_end() );

  os << "<inherit>\n";

  for ( it=item.super_class_begin(); it!=eit; ++it)
    os << "<class>" << it->get_class_name() << "</class>\n";

  os << "</inherit>\n";
} // item_class_inherit_node::write()

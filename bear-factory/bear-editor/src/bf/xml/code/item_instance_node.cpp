/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/code/item_instance_node.cpp
 * \brief Implementation of the bf::xml::item_instance_node class.
 * \author Julien Jorge
 */
#include "bf/xml/item_instance_node.hpp"

#include "bf/item_class_pool.hpp"
#include "bf/item_instance.hpp"
#include "bf/xml/exception.hpp"
#include "bf/xml/item_instance_fields_node.hpp"
#include "bf/xml/reader_tool.hpp"
#include "bf/wx_facilities.hpp"

#include <claw/assert.hpp>
#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Read an xml node "item".
 * \param node The node.
 * \return A dynamically allocated item_instance as described by the XML node.
 */
bf::item_instance* bf::xml::item_instance_node::read
( const item_class_pool& pool, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node!=NULL );
  CLAW_PRECOND( node->GetName() == wxT("item") );

  wxString val;

  if ( !node->GetPropVal( wxT("class_name"), &val ) )
    throw xml::missing_property( "class_name" );

  std::string class_name( wx_to_std_string(val) );
  item_instance* item(NULL);

  try
    {
      item = new item_instance( pool.get_item_class_ptr(class_name) );
      item->set_fixed
        ( xml::reader_tool::read_bool_opt(node, wxT("fixed"), false) );
      item->set_id
        ( wx_to_std_string(node->GetPropVal( wxT("id"), wxEmptyString )) );

      load_fields( *item, node->GetChildren() );
    }
  catch( std::exception& e )
    {
      claw::logger << claw::log_error << e.what() << std::endl;
      delete item;
      item = NULL;
    }

  return item;
} // item_instance_node::read()

/*----------------------------------------------------------------------------*/
/**
 * \brief Write an xml node "item".
 * \param item The item instance to write.
 * \param os The stream in which we write.
 */
void bf::xml::item_instance_node::write
( const item_instance& item, std::ostream& os ) const
{
  os << "    <item class_name='" << item.get_class().get_class_name()
     << "' fixed='";

  if ( item.get_fixed() )
    os << "true'";
  else
    os << "false'";

  if ( !item.get_id().empty() )
    os << " id='" << item.get_id() << "'";

  os << ">\n";

  item_instance_fields_node field_node;
  field_node.write(item, os);

  os << "    </item><!-- " << item.get_class().get_class_name() << " -->\n\n";
} // item_instance_node::write()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the fields of an item.
 * \param item The item in which we set the fields.
 * \param node The node to parse.
 */
void bf::xml::item_instance_node::load_fields
( item_instance& item, const wxXmlNode* node ) const
{
  xml::item_instance_fields_node reader;
  node = xml::reader_tool::skip_comments(node);

  if ( node->GetName() == wxT("fields") )
    reader.read(item, node);
  else
    claw::logger << claw::log_warning << "Ignored node '"
                 << wx_to_std_string(node->GetName()) << "'" << std::endl;
} // item_instance_fields_node::load_fields()

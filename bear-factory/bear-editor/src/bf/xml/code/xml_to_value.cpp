/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/code/xml_to_value.cpp
 * \brief Implementation of the bf::xml::xml_to_value class.
 * \author Julien Jorge
 */
#include "bf/xml/xml_to_value.hpp"

#include "bf/image_pool.hpp"
#include "bf/wx_facilities.hpp"
#include "bf/xml/reader_tool.hpp"

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the bitmap_rendering_attributes of a node.
 * \param att (out) The attributes we have read.
 * \param node The node from which we read the value.
 */
void
bf::xml::bitmap_rendering_attributes_xml_to_value::load_rendering_attributes
( bitmap_rendering_attributes& att, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node != NULL );

  att.set_auto_size
    ( xml::reader_tool::read_bool_opt
      (node, wxT("auto_size"), att.get_auto_size()) );
  att.set_width
    ( xml::reader_tool::read_uint_opt(node, wxT("width"), att.width()) );
  att.set_height
    ( xml::reader_tool::read_uint_opt(node, wxT("height"), att.height()) );
  att.mirror
    ( xml::reader_tool::read_bool_opt(node, wxT("mirror"), att.is_mirrored()));
  att.flip
    ( xml::reader_tool::read_bool_opt(node, wxT("flip"), att.is_flipped()));
  att.set_opacity
    ( xml::reader_tool::read_real_opt
      (node, wxT("opacity"), att.get_opacity()) );
  att.set_angle
    ( xml::reader_tool::read_real_opt( node, wxT("angle"), att.get_angle()) );

  double r = xml::reader_tool::read_real_opt
    ( node, wxT("red_intensity"), att.get_red_intensity() );
  double g = xml::reader_tool::read_real_opt
    ( node, wxT("green_intensity"), att.get_green_intensity() );
  double b = xml::reader_tool::read_real_opt
    ( node, wxT("blue_intensity"), att.get_blue_intensity() );

  att.set_intensity( r, g, b );
} // bitmap_rendering_attributes_xml_to_value::load_rendering_attributes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a xml value node.
 * \param spr (out) The sprite we have read.
 * \param node The node from which we read the value.
 */
void bf::xml::xml_to_value<bf::sprite>::operator()
  ( sprite& spr, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node != NULL );

  spr.set_image_name( xml::reader_tool::read_string(node, wxT("image")) );
  const std::string spritepos =
    xml::reader_tool::read_string_opt(node, wxT("spritepos"), std::string());

  if ( spritepos.empty() )
    {
      spr.set_left( xml::reader_tool::read_uint(node, wxT("x")) );
      spr.set_top( xml::reader_tool::read_uint(node, wxT("y")) );
      spr.set_clip_width
        ( xml::reader_tool::read_uint(node, wxT("clip_width")) );
      spr.set_clip_height
        ( xml::reader_tool::read_uint(node, wxT("clip_height")) );
      spr.set_spritepos_entry
        ( wx_to_std_string
          (image_pool::get_instance().find_spritepos_name_from_size
           ( std_to_wx_string(spr.get_image_name()),
             spr.get_clip_rectangle() )) );
    }
  else
    {
      spr.set_clip_rectangle
        ( image_pool::get_instance().get_spritepos_rectangle
          ( std_to_wx_string(spr.get_image_name()),
            std_to_wx_string(spritepos) ) );
      spr.set_spritepos_entry( spritepos );
    }

  load_rendering_attributes(spr, node);

  if ( spr.get_auto_size() )
    {
      spr.set_width( spr.get_clip_width() );
      spr.set_height( spr.get_clip_height() );
    }
  else if ( (spr.width() == spr.get_clip_width())
            && (spr.height() == spr.get_clip_height()) )
    spr.set_auto_size(true);
} // xml_to_value::operator()() [sprite]

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the reader can process a node with a given name.
 * \param node_name The name of the node.
 */
bool bf::xml::xml_to_value<bf::animation>::supported_node
( const wxString& node_name )
{
  return node_name == wxT("animation");
} // xml_to_value::supported_node() [animation]

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a xml value node.
 * \param anim (out) The animation we have read.
 * \param node The node from which we read the value.
 */
void bf::xml::xml_to_value<bf::animation>::operator()
  ( animation& anim, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node != NULL );

  anim.set_loops( xml::reader_tool::read_uint(node, wxT("loops")) );
  anim.set_first_index
    ( xml::reader_tool::read_uint(node, wxT("first_index")) );
  anim.set_last_index
  ( xml::reader_tool::read_uint(node, wxT("last_index")) );
  anim.set_loop_back
  ( xml::reader_tool::read_bool_opt(node, wxT("loop_back"), false));

  load_frames(anim, node->GetChildren());

  load_rendering_attributes(anim, node);

  if ( anim.get_auto_size() )
    {
      anim.set_width( anim.get_max_size().x );
      anim.set_height( anim.get_max_size().y );
    }
  else if ( anim.get_size() == anim.get_max_size() )
    anim.set_auto_size(true);
} // xml_to_value::operator()() [animation]

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the frames of an animation.
 * \param anim The animation in which we set a list of frames.
 * \param node The node to parse.
 */
void bf::xml::xml_to_value<bf::animation>::load_frames
( animation& anim, const wxXmlNode* node ) const
{
  for ( ; node!=NULL; node=node->GetNext() )
    if ( node->GetName() == wxT("frame") )
      load_frame(anim, node);
    else if ( node->GetName() != wxT("comment") )
      claw::logger << claw::log_warning << "Ignored node '"
                   << wx_to_std_string(node->GetName()) << "'" << std::endl;
} // bf::xml::xml_to_value<bf::animation>::load_frames()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a frame of an animation.
 * \param anim The animation in which we add the frame.
 * \param node The node to parse.
 */
void bf::xml::xml_to_value<bf::animation>::load_frame
( animation& anim, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node->GetName() == wxT("frame") );

  wxString val;
  animation_frame frame;
  sprite spr;

  if ( !node->GetPropVal( wxT("duration"), &val ) )
    throw missing_property( "duration" );

  frame.set_duration
    ( xml::reader_tool::read_real_opt(node, wxT("duration"), 40) );

  wxXmlNode* children = node->GetChildren();
  if ( children != NULL )
    {
      if ( children->GetName() == wxT("sprite") )
        {
          xml::xml_to_value<sprite> xml_conv;
          xml_conv(spr, children);
          frame.set_sprite(spr);
          anim.add_frame() = frame;
        }
      else if ( node->GetName() != wxT("comment") )
        claw::logger << claw::log_warning << "Ignored node '"
                     << wx_to_std_string(children->GetName())
                     << "'" << std::endl;
    }
  else
    throw missing_node("sprite");
} // bf::xml::xml_to_value<bf::animation>::load_frame()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the reader can process a node with a given name.
 * \param node_name The name of the node.
 */
bool bf::xml::xml_to_value<bf::animation_file_type>::supported_node
( const wxString& node_name )
{
  return node_name == wxT("animation_file");
} // xml_to_value::supported_node() [animation_file_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a xml value node.
 * \param anim (out) The animation we have read.
 * \param node The node from which we read the value.
 */
void bf::xml::xml_to_value<bf::animation_file_type>::operator()
  ( animation_file_type& anim, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node != NULL );

  wxString path;

  if ( !node->GetPropVal( wxT("path"), &path ) )
    throw missing_property("path");

  anim.set_path( wx_to_std_string(path) );

  load_rendering_attributes(anim, node);
} // xml_to_value::operator()() [animation_file_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the reader can process a node with a given name.
 * \param node_name The name of the node.
 */
bool bf::xml::xml_to_value<bf::any_animation>::supported_node
( const wxString& node_name )
{
  return xml_to_value<animation_file_type>::supported_node(node_name)
    || xml_to_value<animation>::supported_node(node_name);
} // xml_to_value::supported_node() [any_animation]

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a xml value node.
 * \param anim (out) The animation we have read.
 * \param node The node from which we read the value.
 */
void bf::xml::xml_to_value<bf::any_animation>::operator()
  ( any_animation& anim, const wxXmlNode* node ) const
{
  wxString name = node->GetName();

  if ( xml_to_value<animation_file_type>::supported_node(name) )
    {
      animation_file_type data;
      xml_to_value<animation_file_type> reader;
      reader(data, node);
      anim.set_animation_file(data);
    }
  else if ( xml_to_value<animation>::supported_node(name) )
    {
      animation data;
      xml_to_value<animation> reader;
      reader(data, node);
      anim.set_animation(data);
    }
  else
    throw bad_node( wx_to_std_string(name) );
} // xml_to_value::operator()() [any_animation]

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a xml value node.
 * \param v (out) The sample we have read.
 * \param node The node from which we read the value.
 */
void bf::xml::xml_to_value<bf::sample>::operator()
  ( sample& v, const wxXmlNode* node ) const
{
  CLAW_PRECOND( node != NULL );

  wxString path;

  if ( !node->GetPropVal( wxT("path"), &path ) )
    throw missing_property("path");

  v.set_path( wx_to_std_string(path) );

  v.set_loops( xml::reader_tool::read_uint_opt(node, wxT("loops"), 1) );
  v.set_volume( xml::reader_tool::read_real_opt(node, wxT("volume"), 1) );
} // xml_to_value::operator()() [sample]

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/code/exception.cpp
 * \brief Implementation of the bf::xml exceptions classes.
 */
#include "bf/xml/exception.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param node_name The name of the unexpected node.
 */
bf::xml::bad_node::bad_node( const std::string& node_name )
  : m_msg( "Unexpected node name '" + node_name + "'")
{

} // bad_node::bad_node()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::xml::bad_node::~bad_node() throw()
{

} // bad_node::bad_node()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an explanation of the problem.
 */
const char* bf::xml::bad_node::what() const throw()
{
  return m_msg.c_str();
} // bad_node::what()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param node_name The name of the needed node.
 */
bf::xml::missing_node::missing_node( const std::string& node_name )
  : m_msg( "Missing node '" + node_name + "'")
{

} // missing_node::missing_node()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::xml::missing_node::~missing_node() throw()
{

} // missing_node::missing_node()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an explanation of the problem.
 */
const char* bf::xml::missing_node::what() const throw()
{
  return m_msg.c_str();
} // missing_node::what()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param property_name The name of the needed property.
 */
bf::xml::missing_property::missing_property
( const std::string& property_name )
  : m_msg( "Missing property '" + property_name + "'")
{

} // missing_property::missing_property()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::xml::missing_property::~missing_property() throw()
{

} // missing_property::missing_property()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an explanation of the problem.
 */
const char* bf::xml::missing_property::what() const throw()
{
  return m_msg.c_str();
} // missing_property::what()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param value The problematic value.
 */
bf::xml::bad_value::bad_value( const std::string& value )
  : m_msg( "Invalid value '" + value + "'")
{

} // bad_value::bad_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param type The expected type.
 * \param value The problematic value.
 */
bf::xml::bad_value::bad_value
( const std::string& type, const std::string& value )
  : m_msg( "Invalid value '" + value + "' (" + type + ")" )
{

} // bad_value::bad_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::xml::bad_value::~bad_value() throw()
{

} // bad_value::bad_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an explanation of the problem.
 */
const char* bf::xml::bad_value::what() const throw()
{
  return m_msg.c_str();
} // bad_value::what()

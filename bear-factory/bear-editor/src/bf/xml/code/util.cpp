/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file util.cpp
 * \brief Implementation of the bf::xml::util class.
 * \author Julien Jorge
 */
#include "bf/xml/util.hpp"

#include "boost/algorithm/string/replace.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Replace special XML characters with adequate entities.
 * \param str The string to process.
 */
std::string bf::xml::util::replace_special_characters( const std::string& str )
{
  std::string result( str );

  // replace & first to ensure proper conversion.
  boost::algorithm::replace_all( result, "&", "&amp;" );
  boost::algorithm::replace_all( result, "<", "&lt;" );
  boost::algorithm::replace_all( result, ">", "&gt;" );
  boost::algorithm::replace_all( result, "'", "&apos;" );
  boost::algorithm::replace_all( result, "\"", "&quot;" );

  return result;
} // util::replace_special_characters()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/item_instance_field_node.hpp
 * \brief Process an xml "field" node of an item instance.
 * \author Julien Jorge
 */
#ifndef __BF_XML_ITEM_INSTANCE_FIELD_NODE_HPP__
#define __BF_XML_ITEM_INSTANCE_FIELD_NODE_HPP__

#include <wx/xml/xml.h>
#include <iostream>

#include "bf/custom_type.hpp"

namespace bf
{
  class item_instance;
  class type_field;

  namespace xml
  {
    /**
     * \brief Process an xml "field" node of an item instance.
     * \author Julien Jorge
     */
    class item_instance_field_node
    {
    public:
      void read( item_instance& item, const wxXmlNode* node ) const;
      void write
      ( const item_instance& item, const type_field& f,
        std::ostream& os ) const;

    private:
      void load_field
      ( item_instance& item, const type_field& f, const wxXmlNode* node ) const;
      void save_field
      ( const item_instance& item, const type_field& f,
        std::ostream& os ) const;

      void save_sprite
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;
      void save_animation
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;
      void save_font
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;
      void save_sample
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;
      void save_sprite_list
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;
      void save_animation_list
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;
      void save_font_list
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;
      void save_sample_list
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item ) const;

      template<typename Type>
      void load_value
      ( item_instance& item, const std::string& field_name,
        const wxXmlNode* node ) const;

      template<typename Type>
      void load_value_list
      ( item_instance& item, const std::string& field_name,
        const wxXmlNode* node ) const;

      template<typename Type>
      void save_value
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item, const std::string& node_name ) const;

      template<typename Type>
      void save_value_list
      ( std::ostream& os, const std::string& field_name,
        const item_instance& item, const std::string& node_name ) const;

    }; // class item_instance_field_node
  } // namespace xml
} // namespace bf

#endif // __BF_XML_ITEM_INSTANCE_FIELD_NODE_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/xml_to_value.hpp
 * \brief A class for reading source xml files.
 * \author Julien Jorge
 */
#ifndef __BF_XML_XML_TO_VALUE_HPP__
#define __BF_XML_XML_TO_VALUE_HPP__

#include "bf/any_animation.hpp"
#include "bf/sample.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/xml/xml.h>

namespace bf
{
  namespace xml
  {
    /**
     * \brief A class to convert an XML node into real data.
     * \author Julien Jorge
     */
    template<typename Type>
    class xml_to_value
    {
    public:
      void operator()( Type& v, const wxXmlNode* node ) const;

    }; // class xml_to_value

    /**
     * \brief A class for common code in
     *        xml_to_value<something with bitmap_rendering_attributes>.
     * \author Julien Jorge
     */
    class BEAR_EDITOR_EXPORT bitmap_rendering_attributes_xml_to_value
    {
    protected:
      void load_rendering_attributes
        ( bitmap_rendering_attributes& att, const wxXmlNode* node  ) const;

    }; // class bitmap_rendering_attributes_xml_to_value

    /**
     * \brief A class to convert an XML node into real data. Specialisation for
     *        the bf::sprite class.
     */
    template<>
    class BEAR_EDITOR_EXPORT xml_to_value<sprite>:
      public bitmap_rendering_attributes_xml_to_value
    {
    public:
      void operator()( sprite& v, const wxXmlNode* node ) const;

    }; // class xml_to_value [sprite]

    /**
     * \brief A class to convert an XML node into real data. Specialisation for
     *        the bf::animation class.
     */
    template<>
    class BEAR_EDITOR_EXPORT xml_to_value<animation>:
      public bitmap_rendering_attributes_xml_to_value
    {
    public:
      static bool supported_node( const wxString& node_name );
      void operator()( animation& v, const wxXmlNode* node ) const;

    private:
      void load_frames( animation& anim, const wxXmlNode* node ) const;
      void load_frame( animation& anim, const wxXmlNode* node ) const;

    }; // class xml_to_value [animation]

    /**
     * \brief A class to convert an XML node into real data. Specialisation for
     *        the bf::animation_file_type class.
     */
    template<>
    class BEAR_EDITOR_EXPORT xml_to_value<animation_file_type>:
      public bitmap_rendering_attributes_xml_to_value
    {
    public:
      static bool supported_node( const wxString& node_name );

      void operator()( animation_file_type& v, const wxXmlNode* node ) const;

    }; // class xml_to_value [animation_file_type]

    /**
     * \brief A class to convert an XML node into real data. Specialisation for
     *        the bf::any_animation class.
     */
    template<>
    class BEAR_EDITOR_EXPORT xml_to_value<any_animation>
    {
    public:
      static bool supported_node( const wxString& node_name );

      void operator()( any_animation& v, const wxXmlNode* node ) const;

    }; // class xml_to_value [any_animation]

    /**
     * \brief A class to convert an XML node into real data. Specialisation for
     *        the bf::sample class.
     */
    template<>
    class BEAR_EDITOR_EXPORT xml_to_value<sample>
    {
    public:
      void operator()( sample& v, const wxXmlNode* node ) const;

    }; // class xml_to_value [sample]

  } // namespace xml
} // namespace bf

#include "bf/xml/impl/xml_to_value.tpp"

#endif // __BF_XML_XML_TO_VALUE_HPP__

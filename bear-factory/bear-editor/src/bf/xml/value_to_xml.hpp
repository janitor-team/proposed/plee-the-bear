/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/value_to_xml.hpp
 * \brief A class for writing source level files.
 * \author Julien Jorge
 */
#ifndef __BF_XML_VALUE_TO_XML_HPP__
#define __BF_XML_VALUE_TO_XML_HPP__

#include "bf/any_animation.hpp"
#include "bf/sample.hpp"
#include "bf/libeditor_export.hpp"

#include <iostream>
#include <wx/xml/xml.h>

namespace bf
{
  namespace xml
  {
    /**
     * \brief A class to convert a value into an XML representation.
     * \author Julien Jorge
     */
    template<typename Type>
    class value_to_xml
    {
    public:
      static void write
      ( std::ostream& os, const std::string& node_name, const Type& v );
    }; // class value_to_xml

    /**
     * \brief A class for common code in value_to_xml<animation> and
     *        value_to_xml<sprite>.
     * \author Julien Jorge
     */
    class BEAR_EDITOR_EXPORT base_sprite_animation_value_to_xml
    {
    protected:
      static void bitmap_rendering_attributes_xml
      ( std::ostream& os, const bitmap_rendering_attributes& att );

    }; // class base_sprite_animation_value_to_xml

    /**
     * \brief A class to convert a value into an XML representation.
     *        Specialisation for the bf::sprite class.
     * \author Julien Jorge
     */
    template<>
    class BEAR_EDITOR_EXPORT value_to_xml<sprite>:
      public base_sprite_animation_value_to_xml
    {
    public:
      static void write( std::ostream& os, const sprite& spr );
    }; // class value_to_xml [sprite]

    /**
     * \brief A class to convert a value into an XML representation.
     *        Specialisation for the bf::animation class.
     * \author Julien Jorge
     */
    template<>
    class BEAR_EDITOR_EXPORT value_to_xml<animation>:
      public base_sprite_animation_value_to_xml
    {
    public:
      static void write( std::ostream& os, const animation& anim );
    }; // class value_to_xml [animation]

    /**
     * \brief A class to convert a value into an XML representation.
     *        Specialisation for the bf::animation_file_type class.
     * \author Julien Jorge
     */
    template<>
    class BEAR_EDITOR_EXPORT value_to_xml<animation_file_type>:
      public base_sprite_animation_value_to_xml
    {
    public:
      static void write( std::ostream& os, const animation_file_type& anim );
    }; // class value_to_xml [animation_file_type]

    /**
     * \brief A class to convert a value into an XML representation.
     *        Specialisation for the bf::any_animation class.
     * \author Julien Jorge
     */
    template<>
    class BEAR_EDITOR_EXPORT value_to_xml<any_animation>
    {
    public:
      static void write( std::ostream& os, const any_animation& anim );
    }; // class value_to_xml [any_animation]

    /**
     * \brief A class to convert a value into an XML representation.
     *        Specialisation for the bf::sample class.
     * \author Julien Jorge
     */
    template<>
    class BEAR_EDITOR_EXPORT value_to_xml<sample>
    {
    public:
      static void write( std::ostream& os, const sample& s );
    }; // class value_to_xml [sample]

  } // namespace xml
} // namespace bf

#include "bf/xml/impl/value_to_xml.tpp"

#endif // __BF_XML_VALUE_TO_XML_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/reader_tool.hpp
 * \brief Some tool functions for the xml readers.
 * \author Julien Jorge
 */
#ifndef __BF_XML_READER_TOOL_HPP__
#define __BF_XML_READER_TOOL_HPP__

#include <wx/xml/xml.h>
#include <string>

namespace bf
{
  namespace xml
  {
    /**
     * \brief Some tool functions for the xml readers.
     * \author Julien Jorge
     */
    class reader_tool
    {
    public:
      static const wxXmlNode* skip_comments( const wxXmlNode* node );

      static int read_int( const wxXmlNode* node, const wxString& prop );
      static unsigned int
      read_uint( const wxXmlNode* node, const wxString& prop );
      static std::string
      read_string( const wxXmlNode* node, const wxString& prop );
      static double read_real( const wxXmlNode* node, const wxString& prop );

      static int read_int_opt
      ( const wxXmlNode* node, const wxString& prop, int def );
      static unsigned int read_uint_opt
      ( const wxXmlNode* node, const wxString& prop, unsigned int def );
      static std::string read_string_opt
      ( const wxXmlNode* node, const wxString& prop, const std::string& def );
      static double
      read_real_opt( const wxXmlNode* node, const wxString& prop, double def );
      static bool
      read_bool_opt( const wxXmlNode* node, const wxString& prop, bool def );

    }; // class reader_tool
  } // namespace xml
} // namespace bf

#endif // __BF_XML_READER_TOOL_HPP__

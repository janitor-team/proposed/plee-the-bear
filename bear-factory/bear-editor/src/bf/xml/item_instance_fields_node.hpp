/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/xml/item_instance_fields_node.hpp
 * \brief Process an xml "fields" node of an item instance.
 * \author Julien Jorge
 */
#ifndef __BF_XML_ITEM_INSTANCE_FIELDS_NODE_HPP__
#define __BF_XML_ITEM_INSTANCE_FIELDS_NODE_HPP__

#include <wx/xml/xml.h>
#include <iostream>

namespace bf
{
  class item_class;
  class item_instance;

  namespace xml
  {
    /**
     * \brief Process an xml "fields" node of an item instance.
     * \author Julien Jorge
     */
    class item_instance_fields_node
    {
    public:
      void read( item_instance& item, const wxXmlNode* node ) const;
      void write
      ( const item_instance& item, std::ostream& os ) const;

    private:
      void save_item_by_class
      ( const item_instance& item, const item_class& the_class,
        std::ostream& os ) const;

    }; // class item_instance_fields_node
  } // namespace xml
} // namespace bf

#endif // __BF_XML_ITEM_INSTANCE_FIELDS_NODE_HPP__

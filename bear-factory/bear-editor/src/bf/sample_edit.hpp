/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sample_edit.hpp
 * \brief The window showing the properties of a sample.
 * \author Julien Jorge
 */
#ifndef __BF_SAMPLE_EDIT_HPP__
#define __BF_SAMPLE_EDIT_HPP__

#include "bf/default_value.hpp"
#include "bf/libeditor_export.hpp"

#include "bf/base_edit.hpp"
#include "bf/sample.hpp"
#include "bf/spin_ctrl.hpp"

#include <wx/wx.h>
#include <wx/spinctrl.h>

#include <vector>
#include <claw/rectangle.hpp>

namespace bf
{
  /**
   * \brief The window showing the properties of a sample.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT sample_edit:
    public wxPanel,
    public base_edit<sample>
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_FILE_SELECT
      }; // enum control_id

  public:
    sample_edit
    ( wxWindow& parent, const sample& s = default_value<sample>::get() );

    bool validate();

  private:
    void value_updated();
    void fill_controls();

    void create_controls();
    void create_sizer_controls();

    void on_file_select(wxCommandEvent& event);

  private:
    /** \brief How many times the sample is repeated. */
    wxSpinCtrl* m_loops;

    /** \brief The valume at which the sample is played. */
    spin_ctrl<double>* m_volume;

    /** \brief The path to the sample file. */
    wxTextCtrl* m_sound_file;

    DECLARE_EVENT_TABLE()

  }; // class sample_edit
} // namespace bf

#endif // __BF_SAMPLE_EDIT_HPP__

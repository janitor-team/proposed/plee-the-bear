/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/type_field_set.hpp
 * \brief A field taking its value from a set.
 * \author Julien Jorge
 */
#ifndef __BF_TYPE_FIELD_SET_HPP__
#define __BF_TYPE_FIELD_SET_HPP__

#include "bf/type_field.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief A field taking its value from a set.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT type_field_set:
    public type_field
  {
  public:
    type_field_set( const std::string& name, field_type ft,
                    const std::list<std::string>& values );

    virtual type_field* clone() const;
    virtual range_type get_range_type() const;
    virtual void get_set( std::list<std::string>& values ) const;

  private:
    /** \brief The values this field can take. */
    std::list<std::string> m_values;

  }; // class type_field_set
} // namespace bf

#endif // __BF_TYPE_FIELD_SET_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/base_editor_application.hpp
 * \brief The class representing the application.
 * \author Julien Jorge
 */
#ifndef __BF_BASE_EDITOR_APPLICATION_HPP__
#define __BF_BASE_EDITOR_APPLICATION_HPP__

#include <wx/wx.h>

namespace bf
{
  /**
   * \brief The class representing the application.
   * \author Julien Jorge
   */
  class base_editor_application:
    public wxApp
  {
  public:
    base_editor_application();
    ~base_editor_application();

  protected:
    virtual void compile( const wxString& path ) const;
    virtual void update( const wxString& path ) const;
    virtual bool do_init_app();
    virtual bool do_command_line_init();

  private:
    bool OnInit();
    bool init_app();
    bool command_line_init();

    bool compile_arguments() const;
    bool update_arguments() const;
    bool show_help();
    bool show_version();
    bool find_and_erase_option
    ( const wxString& long_name, const wxString& short_name );

  private:
    /** \brief The locale, for internationalization. */
    wxLocale m_locale;

  }; // class base_editor_application
} // namespace bf

#endif // __BF_BASE_EDITOR_APPLICATION_HPP__

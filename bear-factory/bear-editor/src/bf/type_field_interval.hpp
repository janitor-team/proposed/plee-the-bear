/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/type_field_interval.hpp
 * \brief A field taking its value in an interval.
 * \author Julien Jorge
 */
#ifndef __BF_TYPE_FIELD_INTERVAL_HPP__
#define __BF_TYPE_FIELD_INTERVAL_HPP__

#include "bf/type_field.hpp"

namespace bf
{
  /**
   * \brief A field taking its value in an interval.
   * \author Julien Jorge
   */
  template<typename T>
  class type_field_interval:
    public type_field
  {
  public:
    type_field_interval( const std::string& name, T min, T max );

    virtual type_field* clone() const;
    virtual range_type get_range_type() const;
    virtual void get_interval( T& min, T& max ) const;

  private:
    /** \brief The minimum valid value. */
    const T m_min;

    /** \brief The maximum valid value. */
    const T m_max;

  }; // class type_field_interval
} // namespace bf

#include "bf/impl/type_field_interval.tpp"

#endif // __BF_TYPE_FIELD_INTERVAL_HPP__

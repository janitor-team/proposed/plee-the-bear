/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/human_readable.hpp
 * \brief A class to transform a value into a human readable text.
 * \author Julien Jorge
 */
#ifndef __BF_HUMAN_READABLE_HPP__
#define __BF_HUMAN_READABLE_HPP__

#include "bf/custom_type.hpp"
#include "bf/any_animation.hpp"
#include "bf/sample.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/string.h>
#include <list>

namespace bf
{
  /**
   * \brief A class to transform a value into a human readable text.
   * \author Julien Jorge
   */
  template<typename Type>
  class human_readable
  {
  public:
    /** \brief The type of the value. */
    typedef Type value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable

  /**
   * \brief Specialisation for list of values.
   * \author Julien Jorge
   */
  template<typename T>
  class human_readable< std::list<T> >
  {
  public:
    /** \brief The type of the value. */
    typedef std::list<T> value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [list]

  /**
   * \brief Specialisation for bool_type "true"/"false".
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT human_readable<bool_type>
  {
  public:
    /** \brief The type of the value. */
    typedef bool_type value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [bool_type]

  /**
   * \brief Specialisation for string_type: add quotes.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT human_readable<string_type>
  {
  public:
    /** \brief The type of the value. */
    typedef string_type value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [string_type]

  /**
   * \brief Specialisation for sprite.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT human_readable<sprite>
  {
  public:
    /** \brief The type of the value. */
    typedef sprite value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [sprite]

  /**
   * \brief Specialisation for animation.
   * \author Sebastien Angibaud
   */
  template<>
  class BEAR_EDITOR_EXPORT human_readable<animation>
  {
  public:
    /** \brief The type of the value. */
    typedef animation value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [animation]

  /**
   * \brief Specialisation for animation_file_type.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT human_readable<animation_file_type>
  {
  public:
    /** \brief The type of the value. */
    typedef animation_file_type value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [animation_file_type]

  /**
   * \brief Specialisation for any_animation.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT human_readable<any_animation>
  {
  public:
    /** \brief The type of the value. */
    typedef any_animation value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [any_animation]

  /**
   * \brief Specialisation for sample.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT human_readable<sample>
  {
  public:
    /** \brief The type of the value. */
    typedef sample value_type;

  public:
    static wxString convert( const value_type& v );

  }; // class human_readable [sample]

} // namespace bf

#include "bf/impl/human_readable.tpp"

#endif // __BF_HUMAN_READABLE_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/splash_screen.hpp
 * \brief A splash screen for the applications.
 * \author Julien Jorge
 */
#ifndef __BF_SPLASH_SCREEN_HPP__
#define __BF_SPLASH_SCREEN_HPP__

#include <wx/splash.h>
#include <wx/stattext.h>

namespace bf
{
  /**
   * \brief A splash screen for the applications.
   * \author Julien Jorge
   */
  class splash_screen:
    public wxSplashScreen
  {
  public:
    static splash_screen* create
    ( const wxString& img_name,
      long status_style = wxALIGN_CENTRE | wxST_NO_AUTORESIZE );

    splash_screen( const wxBitmap& img, long status_style );

    wxStaticText& get_status_label();
    wxStaticText& get_version_label();

  private:
    /** \brief The static text where the status of the application is
        displayed. */
    wxStaticText* m_status_label;

    /** \brief The static text where the version of the application is
        displayed. */
    wxStaticText* m_version_label;

  }; // class splash_screen
} // namespace bf

#endif // __BF_SPLASH_SCREEN_HPP__

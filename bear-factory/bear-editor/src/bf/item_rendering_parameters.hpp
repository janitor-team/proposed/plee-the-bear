/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_rendering_parameters.hpp
 * \brief Rendering parameters of an instance of an item class.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_RENDERING_PARAMETERS_HPP__
#define __BF_ITEM_RENDERING_PARAMETERS_HPP__

#include "bf/animation.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  class item_instance;
  class item_class;

  /**
   * \brief An instance of an item class displayed in the level.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT item_rendering_parameters
  {
  public:
    item_rendering_parameters( item_instance& item );
    item_rendering_parameters( const item_rendering_parameters& that );
    ~item_rendering_parameters();

    void field_changed( const std::string& field_name );
    void reset();
    void init();

    double get_left() const;
    double get_right() const;
    double get_bottom() const;
    double get_top() const;
    double get_horizontal_middle() const;
    double get_vertical_middle() const;

    int get_pos_z() const;
    double get_width() const;
    double get_height() const;
    int get_gap_x() const;
    int get_gap_y() const;

    bool is_mirrored() const;
    bool is_flipped() const;

    bool has_user_defined_width() const;
    bool has_user_defined_height() const;

    bool has_sprite() const;
    const sprite& get_sprite() const;

    void set_position( double x, double y );
    void set_left( double p );
    void set_bottom( double p );
    void set_size( double x, double y );

  private:
    sprite get_sprite_from_item() const;
    sprite get_sprite_from_sprites() const;
    sprite search_sprite_in_class( const item_class& c ) const;
    sprite get_sprite_from_animations() const;
    animation search_animation_in_class( const item_class& c ) const;
    animation load_animation( const std::string& path ) const;

    double get_field_real( const std::string& field_name, double v = 0 ) const;
    int get_field_int( const std::string& field_name, int v = 0 ) const;
    bool get_field_bool( const std::string& field_name, bool v = false ) const;

    void set_field_real( const std::string& field_name, double v );

  public:
    /** \brief Default field name for the left position. */
    static const std::string s_field_name_left;

    /** \brief Default field name for the bottom positino. */
    static const std::string s_field_name_bottom;

    /** \brief Default field name for the width. */
    static const std::string s_field_name_width;

    /** \brief Default field name for the height. */
    static const std::string s_field_name_height;

    /** \brief Default field name for the depth. */
    static const std::string s_field_name_depth;

    /** \brief Default field name for the gap on the x-axis. */
    static const std::string s_field_name_gap_x;

    /** \brief Default field name for the gap on the y-axis. */
    static const std::string s_field_name_gap_y;

    /** \brief Default field name for the mirror state. */
    static const std::string s_field_name_mirror;

    /** \brief Default field name for the flipped state. */
    static const std::string s_field_name_flip;

  private:
    /** \brief The instance of the item. */
    item_instance& m_item;

    /**
     * \brief The sprite used to display the item.
     *
     * We use a pointer because the pointed value updated in const member
     * functions.
     */
    sprite* m_sprite;

    /** \brief Left position of the item. */
    double m_left;

    /** \brief Bottom position of the item. */
    double m_bottom;

    /** \brief Width of the item. */
    double m_width;

    /** \brief Height position of the item. */
    double m_height;

    /** \brief Z-axis position of the item. */
    int m_pos_z;

    /** \brief Gap of the sprite on the X-axis. */
    int m_gap_x;

    /** \brief Gap of the sprite on the Y-axis. */
    int m_gap_y;

    /** \brief Indicates if the item is mirrorred. */
    bool m_mirror;

    /** \brief Indicates if the item is flipped. */
    bool m_flip;

  }; // class item_rendering_parameters
} // namespace bf

#endif // __BF_ITEM_RENDERING_PARAMETERS_HPP__

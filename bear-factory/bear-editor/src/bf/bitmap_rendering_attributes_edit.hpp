/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/bitmap_rendering_attributes_edit.hpp
 * \brief The window showing the properties of a bitmap_rendering_attributes.
 * \author Julien Jorge
 */
#ifndef __BF_BITMAP_RENDERING_ATTRIBUTES_EDIT_HPP__
#define __BF_BITMAP_RENDERING_ATTRIBUTES_EDIT_HPP__

#include "bf/base_edit.hpp"
#include "bf/bitmap_rendering_attributes.hpp"
#include "bf/spin_ctrl.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/wx.h>
#include <wx/spinctrl.h>

namespace bf
{
  /**
   * \brief The window showing the properties of a bitmap_rendering_attributes.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT bitmap_rendering_attributes_edit:
    public wxPanel,
    public base_edit<bitmap_rendering_attributes>
  {
  private:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        ID_AUTO_SIZE
      }; // enum control_id

  public:
    bitmap_rendering_attributes_edit
    ( wxWindow& parent, const bitmap_rendering_attributes& spr );

    bool validate();

  private:
    void value_updated();
    void fill_controls();

    void create_controls();
    wxSizer* create_size_and_placement_sizer();
    wxSizer* create_color_sizer();

    void on_auto_size( wxCommandEvent& event );

  private:
    /** \brief Tell if the size is automatically computed. */
    wxCheckBox* m_auto_size;

    /** \brief The text control in which we configure the width. */
    wxSpinCtrl* m_width_spin;

    /** \brief The text control in which we configure the height. */
    wxSpinCtrl* m_height_spin;

    /** \brief The control for configuring the angle. */
    bf::spin_ctrl<double>* m_angle_spin;

    /** \brief The control for configuring the flip_x option. */
    wxCheckBox* m_flip_x_box;

    /** \brief The control for configuring the flip_y option. */
    wxCheckBox* m_flip_y_box;

    /** \brief The control for configuring the intensity on the red channel. */
    bf::spin_ctrl<double>* m_red_spin;

    /** \brief The control for configuring the intensity on the green
        channel. */
    bf::spin_ctrl<double>* m_green_spin;

    /** \brief The control for configuring the intensity on the blue channel. */
    bf::spin_ctrl<double>* m_blue_spin;

    /** \brief The control for configure the transparency. */
    bf::spin_ctrl<double>* m_alpha_spin;

    DECLARE_EVENT_TABLE()

  }; // class bitmap_rendering_attributes_edit
} // namespace bf

#endif // __BF_BITMAP_RENDERING_ATTRIBUTES_EDIT_HPP__

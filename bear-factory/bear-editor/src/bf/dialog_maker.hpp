/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/dialog_maker.hpp
 * \brief A class that create the adequate dialog for editing a field.
 * \author Julien Jorge
 */
#ifndef __BF_DIALOG_MAKER_HPP__
#define __BF_DIALOG_MAKER_HPP__

#include "bf/value_editor_dialog.hpp"
#include "bf/set_edit.hpp"
#include "bf/interval_edit.hpp"

namespace bf
{
  /**
   * \brief A class that create the adequate dialog for editing a field.
   *
   * \b Template \b parameters:
   * - \a Control The control used for selecting the value (free_edit, set_edit,
   *   sprite_edit and so on),
   * - \a Type The type of the edited value (integer_type,
   *   std::list<integer_type> and so on).
   *
   * \author Julien Jorge
   */
  template<typename Control, typename Type>
  class dialog_maker
  {
  public:
    typedef value_editor_dialog<Control, Type> dialog_type;
    typedef Type value_type;

  public:
    static dialog_type*
    create( wxWindow& parent, const wxString& type, const type_field& f,
            const value_type& v );

  }; // class dialog_maker

  /**
   * \brief Specialisation of the bf::dialog_maket class for bf::set_edit<> type
   *        controls.
   *
   * \b Template \b parameters:
   * - \a T The type of the value edited with the set_edit (among the base
   *   types: integer_type, real_type and so on),
   * - \a Type The type of the edited value (integer_type,
   *   std::list<integer_type> and so on).
   *
   * \author Julien Jorge
   */
  template<typename T, typename Type>
  class dialog_maker<set_edit<T>, Type>
  {
  public:
    typedef value_editor_dialog<set_edit<T>, Type> dialog_type;
    typedef Type value_type;

  public:
    static dialog_type*
    create( wxWindow& parent, const wxString& type, const type_field& f,
            const value_type& v );

  }; // class dialog_maker [set_edit]

  /**
   * \brief Specialisation of the bf::dialog_maket class for
   *        bf::interval_edit<> type controls.
   *
   * \b Template \b parameters:
   * - \a T The type of the value edited with the interval_edit (among the base
   *   types: integer_type, real_type and so on),
   * - \a Type The type of the edited value (integer_type,
   *   std::list<integer_type> and so on).
   *
   * \author Julien Jorge
   */
  template<typename T, typename Type>
  class dialog_maker<interval_edit<T>, Type>
  {
  public:
    typedef value_editor_dialog<interval_edit<T>, Type> dialog_type;
    typedef Type value_type;

  public:
    static dialog_type*
    create( wxWindow& parent, const wxString& type, const type_field& f,
            const value_type& v );

  }; // class dialog_maker [interval_edit]
} // namespace bf

#include "bf/impl/dialog_maker.tpp"

#endif // __BF_DIALOG_MAKER_HPP__

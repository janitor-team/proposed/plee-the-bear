/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/*
 * \file bf/item_event.hpp
 * \brief An event sent when something happens to an item.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_EVENT_HPP__
#define __BF_ITEM_EVENT_HPP__

#include <wx/event.h>

#define item_event_handler(func)                                       \
  (wxObjectEventFunction)(wxEventFunction)                             \
    wxStaticCastEvent(item_event::function_type, &func)

#define EVT_ITEM_SELECTION(id, func)                          \
  wx__DECLARE_EVT1( bf::item_event::selection_event_type,     \
                    id, item_event_handler(func) )

namespace bf
{
  class item_instance;

  /**
   * \brief An event sent when something happens to an item.
   * \author Julien Jorge
   */
  class item_event:
    public wxNotifyEvent
  {
  public:
    typedef void (wxEvtHandler::*function_type)(item_event&);

  public:
    explicit item_event( wxEventType t = wxEVT_NULL, wxWindowID id = wxID_ANY );
    explicit item_event
    ( item_instance* item, wxEventType t = wxEVT_NULL,
      wxWindowID id = wxID_ANY );
    item_event( const item_event& that );

    wxEvent* Clone() const;

    item_instance* get_item() const;

  public:
    static const wxEventType selection_event_type;

  private:
    /** The item concerned by the event. */
    item_instance* const m_item;

  }; // class item_event

} // namespace bf

#endif // __BF_ITEM_EVENT_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/image_selection_dialog.hpp
 * \brief A dialog to select one of the images in bf::image_pool.
 * \author Julien Jorge
 */
#ifndef __BF_IMAGE_SELECTION_DIALOG_HPP__
#define __BF_IMAGE_SELECTION_DIALOG_HPP__

#include "bf/libeditor_export.hpp"

#include <wx/dialog.h>
#include <wx/textctrl.h>

namespace bf
{
  class image_list_ctrl;

  /**
   * \brief A dialog to select one of the images in bf::image_pool.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT image_selection_dialog:
    public wxDialog
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_PATTERN_TEXT
      }; // enum control_id

  public:
    image_selection_dialog
    ( wxWindow& parent, const wxString& val = wxEmptyString );

    wxString get_image_name() const;

  private:
    void create_controls();

    void fill_image_list();

    void on_pattern_change( wxCommandEvent& event );

  private:
    /** \brief The list of images. */
    image_list_ctrl* m_image_list;

    /** \brief A text in which the user can enter a part of an image name. */
    wxTextCtrl* m_pattern;

    DECLARE_EVENT_TABLE()

  }; // class image_selection_dialog
} // namespace bf

#endif // __BF_IMAGE_SELECTION_DIALOG_HPP__

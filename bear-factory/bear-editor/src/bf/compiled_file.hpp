/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file compiled_file.hpp
 * \brief This class outputs data in a format understood by the game.
 * \author Julien Jorge
 */
#ifndef __BF_COMPILED_FILE_HPP__
#define __BF_COMPILED_FILE_HPP__

#include <iostream>
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief This class outputs data in a format understood by the game.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT compiled_file
  {
  public:
    compiled_file( std::ostream& f );

    compiled_file& operator<<( const std::string& s );
    compiled_file& operator<<( unsigned long i );
    compiled_file& operator<<( long i );
    compiled_file& operator<<( unsigned int i );
    compiled_file& operator<<( int i );
    compiled_file& operator<<( double i );

  private:
    void output_string_as_text( const std::string& s );
    void output_long_as_text( long i );
    void output_unsigned_long_as_text( unsigned long i );
    void output_integer_as_text( int i );
    void output_unsigned_integer_as_text( unsigned int i );
    void output_real_as_text( double r );

  private:
    /** \brief The file we are writing in. */
    std::ostream& m_file;

  }; // compiled_file
} // namespace bf

#endif // __BF_COMPILED_FILE_HPP__

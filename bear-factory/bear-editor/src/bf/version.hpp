/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file version.hpp
 * \brief Version of the editor.
 * \author Julien Jorge
 */
#ifndef __BF_VERSION_HPP__
#define __BF_VERSION_HPP__

#define BF_TO_STR_BIS(v) # v
#define BF_TO_STR(v) BF_TO_STR_BIS(v)

#define BF_MAJOR_VERSION 0
#define BF_MINOR_VERSION 6
#define BF_RELEASE_NUMBER 0
#define BF_VERSION_STRING "Bear Factory, " BF_TO_STR(BF_MAJOR_VERSION) "." \
  BF_TO_STR(BF_MINOR_VERSION) "." BF_TO_STR(BF_RELEASE_NUMBER)

#endif // __BF_VERSION_HPP__

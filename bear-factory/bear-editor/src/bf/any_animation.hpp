/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/any_animation.hpp
 * \brief A class that contains an animation or an animation_file_type, only one
 *        at once.
 * \author Julien Jorge
 */
#ifndef __BF_ANY_ANIMATION_HPP__
#define __BF_ANY_ANIMATION_HPP__

#include "bf/animation_file_type.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief A class that contains an animation or an animation_file_type, only
   *        one at once.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT any_animation
  {
  public:
    /** \brief The type of the animation effectively stored. */
    enum content_type
      {
        /** \brief A completely detailed animation. */
        content_animation,

        /** \brief An animation file.*/
        content_file

      }; // enum content_type

  public:
    static std::string content_to_string( content_type c );
    static content_type string_to_content( const std::string& c );

    any_animation( content_type c = content_animation );

    void set_animation_file( const animation_file_type& a );
    void set_animation( const animation& a );

    const animation_file_type& get_animation_file() const;
    const animation& get_animation() const;

    animation get_current_animation() const;

    void switch_to( content_type c );
    content_type get_content_type() const;

    void compile( compiled_file& f ) const;

    bool operator==( const any_animation& that ) const;
    bool operator!=( const any_animation& that ) const;
    bool operator<( const any_animation& that ) const;

  private:
    /** \brief The type of the animation on which we are working. */
    content_type m_content_type;

    /** \brief The path of the animation file. */
    animation_file_type m_animation_file;

    /** \brief The animation returned by original_animation(). */
    animation m_animation;

  }; // class any_animation
} // namespace bf

#endif // __BF_ANY_ANIMATION_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/base_edit.hpp
 * \brief A base class owning the edited value for the controls that edit an
 *        item property.
 * \author Julien Jorge
 */
#ifndef __BF_BASE_EDIT_HPP__
#define __BF_BASE_EDIT_HPP__

namespace bf
{
  /**
   * \brief A base class owning the edited value for the controls that edit an
   *        item property.
   * \author Julien Jorge
   */
  template<typename Type>
  class base_edit
  {
  public:
    /** \brief The type of the value of the edited field. */
    typedef Type value_type;

  public:
    base_edit( const value_type& v );
    virtual ~base_edit();

    /** \brief Check if the displayed value is correct and, if it is, set the
        value according to the display. */
    virtual bool validate() = 0;

    const value_type& get_value() const;
    void set_value( const value_type& v );

  protected:
    /** \brief Method called after changing the value by set_value(). */
    virtual void value_updated() = 0;

  private:
    /** \brief The value. */
    value_type m_value;

  }; // class base_edit
} // namespace bf

#include "bf/impl/base_edit.tpp"

#endif // __BF_BASE_EDIT_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/animation_player.hpp
 * \brief This class stores the status of an animation when reading it.
 * \author Julien Jorge
 */
#ifndef __BF_ANIMATION_PLAYER_HPP__
#define __BF_ANIMATION_PLAYER_HPP__

#include "bf/animation.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief This class stores the status of an animation when reading it.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT animation_player
  {
  public:
    animation_player( const animation& anim = animation() );

    void set_animation( const animation& anim );
    const animation& get_animation() const;

    void reset();
    void next();
    void next( double d );

    double get_duration_until_next() const;
    double get_time() const;

    sprite get_sprite() const;

    void set_current_index( std::size_t index );
    std::size_t get_current_index() const;

    bool is_finished() const;

  private:
    bool sequence_is_finished() const;

    void next_index();
    void next_forward();
    void next_backward();

  private:
    /** \brief The displayed animation. */
    animation m_animation;

    /** \brief Current frame. */
    std::size_t m_index;

    /** \brief Are we playing in the normal order ? */
    bool m_forward;

    /** \brief How many full playing have we done ? */
    unsigned int m_play_count;

    /** \brief Time spent on the current frame. */
    double m_time;

  }; // class animation_player
} // namespace bf

#endif // __BF_ANIMATION_PLAYER_HPP__

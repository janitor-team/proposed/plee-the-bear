/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/class_tree_ctrl.hpp
 * \brief A wxTreeCtrl of the available item classes.
 * \author Julien Jorge
 */
#ifndef __BF_CLASS_TREE_CTRL_HPP__
#define __BF_CLASS_TREE_CTRL_HPP__

#include <wx/treectrl.h>
#include <wx/stattext.h>

#include <string>

#include "bf/libeditor_export.hpp"

#define class_selected_event_handler(func)                              \
  (wxObjectEventFunction)(wxEventFunction)                              \
  wxStaticCastEvent(bf::class_selected_event::function_type, &func)

#define EVT_CLASS_SELECTED(id, func)                                    \
  wx__DECLARE_EVT1( bf::class_selected_event::class_selected_event_type, \
                    id, class_selected_event_handler(func) )

namespace bf
{
  class item_class_pool;
  class tree_builder;

  /**
   * \brief Event sent when an item class is selected.
   * \author Julien Jorge
   */
  class class_selected_event:
    public wxNotifyEvent
  {
  public:
    typedef void (wxEvtHandler::*function_type)(class_selected_event&);

  public:
    explicit class_selected_event
    ( const std::string& class_name, wxEventType t = wxEVT_NULL,
      wxWindowID id = wxID_ANY );

    wxEvent* Clone() const;

    const std::string& get_class_name() const;

  public:
    static const wxEventType class_selected_event_type;

  private:
    /** \brief The name of the selected class. */
    const std::string m_class_name;

  }; // class class_selected_event

  /**
   * \brief A wxTreeCtrl of the available item classes.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT class_tree_ctrl:
    public wxPanel
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_PATTERN,
        IDC_TREE
      }; // enum control_id

  public:
    class_tree_ctrl
    ( const item_class_pool& pool, wxWindow* parent, int id = wxID_ANY );

    wxTreeItemId GetSelection() const;
    bool ItemHasChildren( const wxTreeItemId& item ) const;
    wxString GetItemText( const wxTreeItemId& item ) const;

  private:
    void create_controls();

    void fill_tree();
    void create_categories_tree( tree_builder& tb ) const;

    wxString make_pattern() const;

    void select_class(bool shift_down);
    void show_class_description();

    void on_double_click(wxMouseEvent& event);
    void on_class_change(wxTreeEvent& event);
    void on_key_up(wxKeyEvent& event);
    void on_mouse_move(wxMouseEvent& event);
    void on_pattern_change(wxCommandEvent& event);

  private:
    /** \brief The tree in which the class are stored. */
    wxTreeCtrl* m_tree;

    /** \brief The control in which we can type a search pattern. */
    wxTextCtrl* m_pattern;

    /** \brief The control in which we show the description of current class. */
    wxStaticText* m_class_description;

    /** \brief The classes for which we build the tree. */
    const item_class_pool& m_pool;

    DECLARE_EVENT_TABLE()

  }; // class class_tree_ctrl
} // namespace bf

#endif // __BF_CLASS_TREE_CTRL_HPP__

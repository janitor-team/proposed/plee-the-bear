/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/bitmap_rendering_attributes.hpp
 * \brief A class to represent rendering attributes for bitmaps (sprite,
 *        animation).
 * \author Julien Jorge
 */
#ifndef __BF_BITMAP_RENDERING_ATTRIBUTES_HPP__
#define __BF_BITMAP_RENDERING_ATTRIBUTES_HPP__

#include <claw/coordinate_2d.hpp>

#include "bf/libeditor_export.hpp"

namespace bf
{
  class compiled_file;

  /**
   * \brief A class to represent the rendering attributes of a picture.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT bitmap_rendering_attributes
  {
  public:
    bitmap_rendering_attributes();

    bool operator==( const bitmap_rendering_attributes& that ) const;
    bool operator!=( const bitmap_rendering_attributes& that ) const;
    bool operator<( const bitmap_rendering_attributes& that ) const;

    double get_opacity() const;
    void set_opacity( double opacity );

    double get_red_intensity() const;
    double get_green_intensity() const;
    double get_blue_intensity() const;
    void set_intensity( double r, double g, double b );

    const claw::math::coordinate_2d<unsigned int>& get_size() const;

    void set_auto_size( bool a );
    bool get_auto_size() const;

    void set_size( const claw::math::coordinate_2d<unsigned int>& s );
    void set_size( unsigned int w, unsigned int h );
    void set_width( unsigned int w );
    void set_height( unsigned int h );

    unsigned int width() const;
    unsigned int height() const;

    void set_angle( double angle );
    double get_angle() const;

    void flip( bool b );
    void mirror( bool b );

    bool is_flipped() const;
    bool is_mirrored() const;

    void compile( compiled_file& f ) const;

    bitmap_rendering_attributes&
    assign( const bitmap_rendering_attributes& that );

    void combine( const bitmap_rendering_attributes& that );

  private:
    /** \brief The size of the bitmap on the screen. */
    claw::math::coordinate_2d<unsigned int> m_size;

    /** \brief Is the picture reversed along Y axis ? */
    bool m_flip;

    /** \brief Is the picture reversed along X axis ? */
    bool m_mirror;

    /** \brief Pixel intensity. */
    double m_opacity;

    /** \brief Intensity of the red channel. */
    double m_red_intensity;

    /** \brief Intensity of the green channel. */
    double m_green_intensity;

    /** \brief Intensity of the blue channel. */
    double m_blue_intensity;

    /** \brief The angle on the screen. */
    double m_angle;

    /** \brief Tell if the size is automatically computed. */
    bool m_auto_size;

  }; // class bitmap_rendering_attributes

} // namespace bf

#endif // __BF_BITMAP_RENDERING_ATTRIBUTES_HPP__

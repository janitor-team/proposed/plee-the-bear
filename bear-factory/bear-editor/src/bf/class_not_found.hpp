/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/class_not_found.hpp
 * \brief Exception thrown when a class is required but not found.
 * \author Julien Jorge
 */
#ifndef __BF_CLASS_NOT_FOUND_HPP__
#define __BF_CLASS_NOT_FOUND_HPP__

#include "bf/libeditor_export.hpp"

#include <exception>
#include <string>

namespace bf
{
  /**
   * \brief Exception thrown when a class is required but not found.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT class_not_found:
    public std::exception
  {
  public:
    class_not_found( const std::string& class_name );
    virtual ~class_not_found() throw();

    virtual const char* what() const throw();
    const std::string& class_name() const throw();

  private:
    /** \brief A short explanation of the problem. */
    const std::string m_msg;

    /** \brief The name of the class. */
    const std::string m_class_name;

  }; // class class_not_found
} // namespace bf

#endif // __BF_CLASS_NOT_FOUND_HPP__

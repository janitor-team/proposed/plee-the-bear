/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_check_result.hpp
 * \brief This class contains the result of the check of an item instance.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_CHECK_RESULT_HPP__
#define __BF_ITEM_CHECK_RESULT_HPP__

#include "bf/check_error.hpp"

#include <list>

namespace bf
{
  /**
   * \brief This class contains the result of the check of an item instance.
   * \author Julien Jorge
   */
  class item_check_result
  {
  private:
    /** \brief The type of the list in which the problems are stored. */
    typedef std::list<check_error> error_list_type;

  public:
    /** \brief The type of teh iterator on the errors. */
    typedef error_list_type::const_iterator const_iterator;

  public:
    void add( const check_error& e );

    bool is_ok() const;

    const_iterator begin() const;
    const_iterator end() const;

  private:
    /** \brief The errors in the item. */
    error_list_type m_errors;

  }; // class item_check_result
} // namespace bf

#endif // __BF_ITEM_CHECK_RESULT_HPP__

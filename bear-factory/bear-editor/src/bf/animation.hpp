/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/animation.hpp
 * \brief The value of a field of type "animation".
 * \author Julien Jorge
 */
#ifndef __BF_ANIMATION__
#define __BF_ANIMATION__

#include <list>

#include "bf/animation_frame.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief Animation type.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT animation:
    public bitmap_rendering_attributes,
    public std::list<animation_frame>
  {
  public:
    typedef std::list<animation_frame> frame_list;

  public:
    animation();

    bool operator==(const animation& that) const;
    bool operator!=(const animation& that) const;
    bool operator<(const animation& that) const;

    void set_frames( const frame_list& frames );
    void set_loops( unsigned int loops );
    void set_loop_back( bool b );
    void set_first_index( unsigned int index );
    void set_last_index( unsigned int index );

    unsigned int get_loops() const;
    bool get_loop_back() const;
    unsigned int get_first_index() const;
    unsigned int get_last_index() const;

    animation_frame& add_frame();
    animation_frame& get_frame(unsigned int index);
    const animation_frame& get_frame(unsigned int index) const;
    void delete_frame(unsigned int index);
    void move_backward(unsigned int index);
    void move_forward(unsigned int index);

    sprite get_sprite(unsigned int index) const;

    claw::math::coordinate_2d<unsigned int> get_max_size() const;

    std::size_t frames_count() const;

    void compile( compiled_file& f ) const;

  private:
    /** \brief Number of loops. */
    unsigned int m_loops;

    /** \brief Play the animation backward. */
    bool m_loop_back;

    /** \brief The index of the first frame of the loops. */
    unsigned int m_first_index;

    /** \brief The index of the last frame of the loops. */
    unsigned int m_last_index;

  }; // class animation
} // namespace bf

#endif // __BF_ANIMATION__

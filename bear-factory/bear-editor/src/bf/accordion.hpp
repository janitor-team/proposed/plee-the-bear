/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/accordion.hpp
 * \brief An accordion of controls.
 * \author Julien Jorge
 */
#ifndef __BF_ACCORDION_HPP__
#define __BF_ACCORDION_HPP__

#include <wx/panel.h>
#include <wx/tglbtn.h>
#include <map>

namespace bf
{
  /**
   * \brief An accordion of controls.
   *
   * Each added control is associated with a button that can be used to show or
   * Hide the control. At most one control can be visible at once.
   *
   * \author Julien Jorge
   */
  class accordion:
    public wxPanel
  {
  public:
    accordion( wxWindow* parent, int orient );

    void add( wxWindow* c, const wxString& label );
    void add( wxSizer* s, const wxString& label );

  private:
    void on_button( wxCommandEvent& event );

  private:
    /** \brief The sizer in which the parts of the accordion are added. */
    wxSizer* m_content;

    /** \brief The button of the currently visible sizer. */
    wxToggleButton* m_current_button;

    /** \brief The sizers associated with each button. */
    std::map<wxToggleButton*, wxSizer*> m_sizers;

  }; // accordion
} // namespace bf

#endif // __BF_ACCORDION_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/animation_edit.hpp
 * \brief The window showing the properties of a animation.
 * \author Sebastien Angibaud
 */
#ifndef __BF_ANIMATION_EDIT_HPP__
#define __BF_ANIMATION_EDIT_HPP__

#include "bf/animation.hpp"
#include "bf/base_edit.hpp"
#include "bf/default_value.hpp"
#include "bf/spin_ctrl.hpp"

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/listctrl.h>

namespace bf
{
  class animation_view_ctrl;
  class bitmap_rendering_attributes_edit;

  /**
   * \brief The window showing the properties of a animation.
   * \author Sebastien Angibaud
   */
  class animation_edit:
    public wxPanel,
    public base_edit<animation>
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_BOUND_INDEX_CHANGE,
        IDC_FRAME
      }; // enum control_id

  public:
    animation_edit
    ( wxWindow& parent,
      const animation& anim = default_value<animation>::get() );

    bool validate();

  private:
    animation make_animation() const;

    void value_updated();
    void update_spin_ctrl();
    void adjust_last_column_size();
    void fill_controls();

    void create_controls();
    void create_sizer_controls();
    wxSizer* create_loops_sizer();

    void edit_frame( long index );

    void on_up( wxCommandEvent& event );
    void on_down( wxCommandEvent& event );
    void on_new( wxCommandEvent& event );
    void on_copy( wxCommandEvent& event );
    void on_frame_edit(wxListEvent& event);
    void on_frame_button_edit( wxCommandEvent& event );
    void on_delete( wxCommandEvent& event );
    void on_size(wxSizeEvent& event);
    void on_column_begin_drag(wxListEvent& event);
    void on_column_end_drag(wxListEvent& event);
    void on_bound_index_change( wxSpinEvent& event );
    void on_refresh(wxCommandEvent& event);

  private:
    /** \brief The text control in which we configure the number of loops. */
    bf::spin_ctrl<unsigned int>* m_loops_spin;

    /** \brief The text control in which we configure the index of first frame
     * for loops. */
    wxSpinCtrl* m_first_index_spin;

    /** \brief The text control in which we configure the index of last frame
     * for loops. */
    wxSpinCtrl* m_last_index_spin;

    /** \brief The text control in which we configure the loop_back option. */
    wxCheckBox* m_loop_back_box;

    /** \brief The control in which we configure the rendering attributes. */
    bitmap_rendering_attributes_edit* m_rendering_attributes;

    /** \brief The control that displays the list of frame. */
    wxListView* m_frame_list;

    /** \brief The control in which we display the animation. */
    animation_view_ctrl* m_animation_view;

    DECLARE_EVENT_TABLE()

  }; // class animation_edit
} // namespace bf

#endif // __BF_ANIMATION_EDIT_HPP__

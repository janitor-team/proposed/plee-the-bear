/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_comparator.hpp
 * \brief A set of classes for comparing instances of item_instance.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_COMPARATOR_HPP__
#define __BF_ITEM_COMPARATOR_HPP__

#include "bf/libeditor_export.hpp"

namespace bf
{
  class item_instance;

  /**
   * \brief A set of classes for comparing instances of item_instance.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT item_comparator
  {
  public:
    /**
     * \brief Do a lexicographic comparison of two items according to their
     *        position and their size.
     * \author Julien Jorge
     */
    class BEAR_EDITOR_EXPORT by_place
    {
    public:
      /**
       * \brief Do a lexicographic comparison of two items according to their
       *        position and their size.
       * \param a The left operand.
       * \param b The right operand.
       * \return a < b : position(a) lex< position(b) && size(a) lex< size(b)
       */
      bool operator()( const item_instance& a, const item_instance& b ) const;

    }; // class by_place

  }; // class item_comparator

} // namespace bf

#endif // __BF_ITEM_COMPARATOR_HPP__

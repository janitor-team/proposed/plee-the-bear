/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sprite.hpp
 * \brief The value of a field of type "sprite".
 * \author Julien Jorge
 */
#ifndef __BF_SPRITE_HPP__
#define __BF_SPRITE_HPP__

#include "bf/bitmap_rendering_attributes.hpp"
#include "bf/libeditor_export.hpp"

#include <claw/rectangle.hpp>
#include <string>

namespace bf
{
  /**
   * \brief Sprite type.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT sprite:
    public bitmap_rendering_attributes
  {
  public:
    sprite();

    void set_image_name( const std::string& name );
    void set_spritepos_entry( const std::string& name );
    void set_top( const unsigned int top );
    void set_left( const unsigned int left );
    void set_clip_width( const unsigned int width );
    void set_clip_height( const unsigned int height );
    void set_clip_rectangle( const claw::math::rectangle<unsigned int>& r );

    unsigned int get_left() const;
    unsigned int get_top() const;
    unsigned int get_clip_width() const;
    unsigned int get_clip_height() const;
    claw::math::rectangle<unsigned int> get_clip_rectangle() const;
    const std::string& get_image_name() const;
    const std::string& get_spritepos_entry() const;

    void compile( compiled_file& f ) const;

    bool operator==( const sprite& that ) const;
    bool operator!=( const sprite& that ) const;
    bool operator<( const sprite& that ) const;

  private:
    /** \brief The name of the image resource to use. */
    std::string m_image_name;

    /** \brief The name of the entry in the spritepos file. */
    std::string m_spritepos_entry;

    /** \brief Y-coordinate. */
    unsigned int m_top;

    /** \brief X-coordinate. */
    unsigned int m_left;

    /** \brief Width in the source image. */
    unsigned int m_clip_width;

    /** \brief Height in the source image. */
    unsigned int m_clip_height;

  }; // class sprite
} // namespace bf

#endif // __BF_SPRITE_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/config_frame.hpp
 * \brief The dialog to configure the paths to the datas.
 * \author Julien Jorge
 */
#ifndef __BF_CONFIG_FRAME_HPP__
#define __BF_CONFIG_FRAME_HPP__

#include <wx/wx.h>

#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief The dialog to configure the paths to the datas.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT config_frame:
    public wxDialog
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_BROWSE_ITEM_CLASSES_BUTTON,
        IDC_ERASE_ITEM_CLASSES_BUTTON,
        IDC_BROWSE_DATA_PATH_BUTTON,
        IDC_ERASE_DATA_PATH_BUTTON
      }; // enum control_id

  public:
    config_frame( wxWindow* parent );

  private:

    void fill_controls();

    void create_controls();
    void create_member_controls();
    void create_sizer_controls();

    void on_ok(wxCommandEvent& event);
    void on_cancel(wxCommandEvent& event);
    void on_browse_item_classes(wxCommandEvent& event);
    void on_erase_item_classes(wxCommandEvent& event);
    void on_browse_data_path(wxCommandEvent& event);
    void on_erase_data_path(wxCommandEvent& event);

  private:
    /** \brief The list of paths to the item classes files. */
    wxListBox* m_item_classes_list;

    /** \brief The list of paths to the data of the game. */
    wxListBox* m_data_path_list;

    DECLARE_EVENT_TABLE()

  }; // class config_frame
} // namespace bf

#endif // __BF_CONFIG_FRAME_HPP__

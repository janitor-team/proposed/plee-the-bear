/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/bool_edit.hpp
 * \brief A control for editing a field that takes its value in a set.
 * \author Julien Jorge
 */
#ifndef __BF_BOOL_EDIT_HPP__
#define __BF_BOOL_EDIT_HPP__

#include "bf/base_edit.hpp"
#include "bf/default_value.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/checkbox.h>

namespace bf
{
  /**
   * \brief A control for editing a field that can takes ites value in a set.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT bool_edit:
    public base_edit<bool_type>,
    public wxCheckBox
  {
  public:
    /** \brief The type of the value of the edited field. */
    typedef bool_type value_type;

  public:
    bool_edit
    ( wxWindow& parent, const value_type& v = default_value<bool_type>::get() );

    bool validate();

  private:
    void init();
    void value_updated();

    void on_change( wxCommandEvent& event );

  }; // class bool_edit
} // namespace bf

#endif // __BF_BOOL_EDIT_HPP__

/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/check_error.hpp
 * \brief This class carries a description of a problem found during the
 *        check of the level.
 * \author Julien Jorge
 */
#ifndef __BF_CHECK_ERROR_HPP__
#define __BF_CHECK_ERROR_HPP__

#include <string>

namespace bf
{
  /**
   * \brief This class carries a description of a problem found during the
   *        check of the level.
   * \author Julien Jorge
   */
  class check_error
  {
  public:
    explicit check_error( const std::string& msg );
    check_error( const std::string& prefix, const std::string& msg );

    const std::string& get_cause() const;
    const std::string& get_message() const;

  private:
    /** \brief The cause of the message describes the element on which the
        error occured. */
    std::string m_cause;

    /** \brief The message describes the problem. */
    std::string m_message;

  }; // class check_error
} // namespace bf

#endif // __BF_CHECK_ERROR_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/simple_edit.hpp
 * \brief A base class providing methods for simple types (integer_type,
 *        string_type and so on).
 * \author Julien Jorge
 */
#ifndef __BF_SIMPLE_EDIT_HPP__
#define __BF_SIMPLE_EDIT_HPP__

#include "bf/base_edit.hpp"

#include <wx/string.h>

namespace bf
{
  /**
   * \brief A base class providing methods for simple types (integer_type,
   *        string_type and so on).
   * \author Julien Jorge
   */
  template<typename Type>
  class simple_edit:
    public base_edit<Type>
  {
  public:
    /** \brief The type of the parent class. */
    typedef base_edit<Type> super;

    /** \brief The type of the value of the edited field. */
    typedef typename base_edit<Type>::value_type value_type;

  public:
    simple_edit( const value_type& v );

  protected:
    wxString value_to_string() const;
    bool value_from_string( const wxString& str );

  }; // class simple_edit
} // namespace bf

#include "bf/impl/simple_edit.tpp"

#endif // __BF_SIMPLE_EDIT_HPP__

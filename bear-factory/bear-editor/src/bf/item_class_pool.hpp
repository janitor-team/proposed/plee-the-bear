/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_class_pool.hpp
 * \brief A singleton containing all item classes.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_CLASS_POOL_HPP__
#define __BF_ITEM_CLASS_POOL_HPP__

#include <boost/filesystem/convenience.hpp>
#include <map>

#include "bf/item_class.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief A class containing all item classes.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT item_class_pool
  {
  private:
    /** \brief The map associating item classes with their name. */
    typedef std::map<std::string, item_class*> item_class_map;

    /** \brief Function object passed to scan_dir to get the item class
        files. */
    struct open_item_class_file
    {
      open_item_class_file( std::map<std::string, std::string>& c );

      void operator()( const std::string& path );

      /** \brief The files found in the directory. */
      std::map<std::string, std::string>& class_files;

    }; // struct open_item_class_file

  public:
    /** \brief Iterator on all the classes. */
    typedef claw::wrapped_iterator
    < const item_class,
      item_class_map::const_iterator,
      claw::unary_compose
      < claw::const_dereference<item_class>,
        claw::const_pair_second<item_class_map::value_type> > >
    ::iterator_type const_iterator;

  public:
    ~item_class_pool();

    void scan_directory( const std::list<std::string>& dir_path );

    bool has_item_class( const std::string& class_name ) const;
    const item_class& get_item_class( const std::string& class_name ) const;
    const item_class* get_item_class_ptr( const std::string& class_name ) const;

    const_iterator begin() const;
    const_iterator end() const;

  private:
    void load_class
    ( const std::string& name, std::map<std::string, std::string>& files );

    void field_unicity_test();

  private:
    /** \brief The item classes. */
    item_class_map m_item_class;

  }; // class item_class_pool
} // namespace bf

#endif // __BF_ITEM_CLASS_POOL_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/stream_conv.hpp
 * \brief Conversion between a value and a text in a stream.
 * \author Julien Jorge
 */
#ifndef __BF_STREAM_CONV_HPP__
#define __BF_STREAM_CONV_HPP__

#include "bf/custom_type.hpp"
#include "bf/libeditor_export.hpp"

#include <iostream>

namespace bf
{
  /**
   * \brief Conversion between a value and a text in a stream. Not defined for
   *        types that are not based on custom_type.
   * \author Julien Jorge
   */
  template<typename Type>
  class stream_conv
  {
  public:
    typedef Type value_type;

  public:
    static std::istream& read( std::istream& is, value_type& v );

  }; // class stream_conv

  /**
   * \brief Conversion between a value and a text in a stream. Specialisation
   *        for string_type.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT stream_conv<string_type>
  {
  public:
    typedef string_type value_type;

  public:
    static std::istream& read( std::istream& is, value_type& v );

  }; // class stream_conv [string_type]

  /**
   * \brief Conversion between a value and a text in a stream. Specialisation
   *        for custom_type<bool>.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT stream_conv< custom_type<bool> >
  {
  public:
    typedef custom_type<bool> value_type;

  public:
    static std::istream& read( std::istream& is, value_type& v );

  }; // class stream_conv [custom_type<bool>]

  /**
   * \brief Conversion between a value and a text in a stream. Specialisation
   *        for others custom types.
   * \author Julien Jorge
   */
  template<typename T>
  class stream_conv< custom_type<T> >
  {
  public:
    typedef custom_type<T> value_type;

  public:
    static std::istream& read( std::istream& is, value_type& v );

  }; // class stream_conv [custom_type]

  /**
   * \brief Conversion between a value and a text in a stream. Specialisation
   *        for item_reference_type.
   * \author Julien Jorge
   */
  template<>
  class BEAR_EDITOR_EXPORT stream_conv<item_reference_type>:
    public stream_conv< custom_type<std::string> >
  {

  }; // class stream_conv [item_reference_type]

  /**
   * \brief Conversion between a value and a text in a stream. Specialisation
   *        for font_file_type.
   * \author Sébastien Angibaud
   */
  template<>
  class BEAR_EDITOR_EXPORT stream_conv<font_file_type>:
    public stream_conv< custom_type<std::string> >
  {

  }; // class stream_conv [font_file_type]

} // namespace bf

#include "bf/impl/stream_conv.tpp"

#endif // __BF_STREAM_CONV_HPP__

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/animation_file_xml_reader.hpp
 * \brief A class for reading source animation files.
 * \author Julien Jorge
 */
#ifndef __BF_ANIMATION_FILE_XML_READER_HPP__
#define __BF_ANIMATION_FILE_XML_READER_HPP__

#include "bf/animation.hpp"
#include "bf/libeditor_export.hpp"

#include <iostream>
#include <wx/xml/xml.h>

namespace bf
{
  /**
   * \brief A class for reading source animation files.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT animation_file_xml_reader
  {
  public:
    animation load( const wxString& file_path ) const;
    animation load( wxXmlNode* node ) const;

  }; // class animation_file_xml_reader
} // namespace bf

#endif // __BF_ANIMATION_FILE_XML_READER_HPP__

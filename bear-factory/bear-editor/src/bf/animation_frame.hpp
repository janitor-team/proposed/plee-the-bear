/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/animation_frame.hpp
 * \brief A frame in an animation.
 * \author Julien Jorge
 */
#ifndef __BF_ANIMATION_FRAME_HPP__
#define __BF_ANIMATION_FRAME_HPP__

#include "bf/sprite.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief A frame is a element of an animation (a sprite and a time).
   */
  class BEAR_EDITOR_EXPORT animation_frame
  {
  public:
    animation_frame();

    void set_sprite(const sprite& spr);
    void set_duration(double time);

    const bf::sprite& get_sprite() const;
    double get_duration() const;

    bool operator==( const animation_frame& that ) const;
    bool operator<( const animation_frame& that ) const;

  private:
    /** \brief The sprite. */
    sprite m_sprite;

    /** \brief The time during which the sprite is display. */
    double m_duration;

  }; // class animation_frame
} // namespace bf

#endif // __BF_ANIMATION_FRAME_HPP__

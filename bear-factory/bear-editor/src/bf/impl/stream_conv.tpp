/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/stream_conv.tpp
 * \brief Implementation the the bf::stream_conv class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a stream.
 * \param iss The stream in which we read.
 * \param v The value we have read.
 */
template<typename Type>
std::istream& bf::stream_conv<Type>::read( std::istream& is, value_type& v )
{
  return is;
} // read()




/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a stream.
 * \param iss The stream in which we read.
 * \param v The value we have read.
 */
template<typename T>
std::istream&
bf::stream_conv< bf::custom_type<T> >::read( std::istream& is, value_type& v )
{
  typename value_type::value_type real_v;

  if ( is >> real_v )
    v.set_value(real_v);

  return is;
} // read()

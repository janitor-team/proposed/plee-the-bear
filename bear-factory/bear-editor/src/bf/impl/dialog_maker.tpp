/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/dialog_maker.tpp
 * \brief Implementation of the bf::dialog_maker class.
 * \author Julien Jorge
 */

#include "bf/wx_facilities.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the dialog.
 * \param parent The parent window.
 * \param type The name of the edited type.
 * \param f The edited field.
 * \param v The initial value.
 */
template<typename Control, typename Type>
typename bf::dialog_maker<Control, Type>::dialog_type*
bf::dialog_maker<Control, Type>::create
( wxWindow& parent, const wxString& type, const type_field& f,
  const value_type& v )
{
  return new dialog_type
    ( parent, std_to_wx_string(f.get_name()) + wxT(" (") + type + wxT(")"), v );
} // dialog_maker::create()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the dialog.
 * \param parent The parent window.
 * \param type The name of the edited type.
 * \param f The edited field.
 * \param v The initial value.
 */
template<typename T, typename Type>
typename bf::dialog_maker<bf::set_edit<T>, Type>::dialog_type*
bf::dialog_maker<bf::set_edit<T>, Type>::create
( wxWindow& parent, const wxString& type, const type_field& f,
  const value_type& v )
{
  wxArrayString values;
  std::list<std::string> raw_values;
  typename std::list<std::string>::const_iterator it;

  f.get_set(raw_values);
  raw_values.sort();

  for (it=raw_values.begin(); it!=raw_values.end(); ++it)
    values.Add( std_to_wx_string(*it) );

  return new dialog_type
    ( parent,
      std_to_wx_string(f.get_name()) + wxT(" (") + type + wxT(")"), values, v );
} // dialog_maker::create() [set_edit]

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the dialog.
 * \param parent The parent window.
 * \param type The name of the edited type.
 * \param f The edited field.
 * \param v The initial value.
 */
template<typename T, typename Type>
typename bf::dialog_maker<bf::interval_edit<T>, Type>::dialog_type*
bf::dialog_maker<bf::interval_edit<T>, Type>::create
( wxWindow& parent, const wxString& type, const type_field& f,
  const value_type& v )
{
  typename T::value_type min, max;

  f.get_interval(min, max);

  return new dialog_type
    ( parent, std_to_wx_string(f.get_name()) + wxT(" (") + type + wxT(")"),
      T(min), T(max), v );
} // dialog_maker::create() [interval_edit]


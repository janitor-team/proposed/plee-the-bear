/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/scan_dir.tpp
 * \brief Implementation of the bf::scan_dir class.
 * \author Julien Jorge
 */

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/convenience.hpp>
#include <queue>

/*----------------------------------------------------------------------------*/
/**
 * \brief Read all item files from a given directory and in its subdirectories.
 * \param dir The path where the files are searched.
 * \param f A copyable function object called on each file found in the
 *        directory.
 * \param first_ext Iterator on the first valid extension.
 * \param last_ext Iterator just after the last valid extension.
 */
template<typename Func>
template<typename Iterator>
void bf::scan_dir<Func>::operator()
( const std::string& dir, Func& f, Iterator first_ext, Iterator last_ext )
{
  std::queue<boost::filesystem::path> pending;
  boost::filesystem::path path(dir, boost::filesystem::native);

  if ( !boost::filesystem::exists(path) )
    return;

  pending.push(path);

  while ( !pending.empty() )
    {
      path = pending.front();
      pending.pop();

      boost::filesystem::directory_iterator it(path);
      boost::filesystem::directory_iterator eit;

      for ( ; it!=eit; ++it)
        if ( boost::filesystem::is_directory(*it) )
          pending.push(*it);
        else if (supported_extension( it->string(), first_ext, last_ext ))
          f(it->string());
    }
} // scan_dir::operator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if a path correspond to a supported extension.
 * \param path The path to check.
 * \param first_ext Iterator on the first valid extension.
 * \param last_ext Iterator just after the last valid extension.
 */
template<typename Func>
template<typename Iterator>
bool bf::scan_dir<Func>::supported_extension
( const std::string& path, Iterator first_ext, Iterator last_ext )
{
  // return true if no extension is given
  bool result( first_ext == last_ext );

  for ( ; !result && (first_ext!=last_ext); ++first_ext )
    if ( path.size() >= first_ext->size() )
      result = path.rfind(*first_ext) == path.size() - first_ext->size();

  return result;
} // scan_dir::supported_extension()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/type_field_interval.tpp
 * \brief Implementation of the bf::type_field_interval class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a copy of this instance.
 */
template<typename T>
bf::type_field* bf::type_field_interval<T>::clone() const
{
  return new type_field_interval(*this);
} // type_field_interval::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the type of the range of valid values.
 */
template<typename T>
typename bf::type_field::range_type
bf::type_field_interval<T>::get_range_type() const
{
  return field_range_interval;
} // type_field_interval::get_range_type()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the interval of valid values for this field.
 * \param min Minimum valid value.
 * \param max Maximum valid value.
 */
template<typename T>
void bf::type_field_interval<T>::get_interval( T& min, T& max ) const
{
  min = m_min;
  max = m_max;
} // type_field::get_interval()

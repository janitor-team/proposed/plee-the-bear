/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/simple_edit.tpp
 * \brief Implementation of the bf::simple_edit class.
 * \author Julien Jorge
 */

#include "bf/stream_conv.hpp"
#include "bf/wx_facilities.hpp"

#include <sstream>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor from a value.
 * \param v The value.
 */
template<typename Type>
bf::simple_edit<Type>::simple_edit( const value_type& v )
  : super(v)
{

} // simple_edit::simple_edit()

/*----------------------------------------------------------------------------*/
/**
 * \brief Transform the value in a string.
 */
template<typename Type>
wxString bf::simple_edit<Type>::value_to_string() const
{
  std::ostringstream oss;

  oss << this->get_value();

  return std_to_wx_string( oss.str() );
} // simple_edit::value_to_string()

/*----------------------------------------------------------------------------*/
/**
 * \brief Transform a string in a value.
 * \param str The string in which we read the value.
 * \return true If the string has been converted.
 */
template<typename Type>
bool bf::simple_edit<Type>::value_from_string( const wxString& str )
{
  std::istringstream iss( wx_to_std_string(str) );

  value_type v;
  bool result = true;

  if ( stream_conv<Type>::read(iss, v) )
    this->set_value(v);
  else
    result = false;

  return result;
} // simple_edit::value_from_string()

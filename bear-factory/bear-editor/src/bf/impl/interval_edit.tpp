/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/interval_edit.tpp
 * \brief Implementation of the bf::interval_edit class.
 * \author Julien Jorge
 */

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param parent The windows owning this one.
 * \param min The minimum valid value.
 * \param max The maximum valid value.
 * \param v The initial value.
 */
template<typename Type>
bf::interval_edit<Type>::interval_edit
( wxWindow& parent, const value_type& min, const value_type& max,
  const value_type& v )
  : super_edit(v),
    super_ctrl( &parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0,
                min.get_value(), max.get_value() )
{
  value_updated();
} // interval_edit::interval_edit()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the displayed value is correct and, if it is, set the
 *        value according to the display.
 */
template<typename Type>
bool bf::interval_edit<Type>::validate()
{
  bool result = false;

  if ( this->value_from_string( this->GetValueText() ) )
    result = (this->get_value().get_value() == this->GetValue() );

  return result;
} // interval_edit::validate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when the value has been changed, to update the display.
 */
template<typename Type>
void bf::interval_edit<Type>::value_updated()
{
  this->SetValue( this->get_value().get_value() );
} // interval_edit::value_updated()

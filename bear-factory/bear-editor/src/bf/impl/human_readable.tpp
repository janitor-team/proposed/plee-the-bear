/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/human_readable.tpp
 * \brief Implementation of the bf::human_readable class.
 * \author Julien Jorge
 */

#include "bf/wx_facilities.hpp"

#include <sstream>

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
template<typename Type>
wxString bf::human_readable<Type>::convert( const value_type& v )
{
  std::ostringstream oss;

  oss << v;

  return std_to_wx_string(oss.str());
} // human_readable::convert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
template<typename T>
wxString bf::human_readable< std::list<T> >::convert( const value_type& v )
{
  wxString result;

  result = wxT("[");

  if ( !v.empty() )
    {
      typename value_type::const_iterator it = v.begin();
      result += human_readable<T>::convert(*it);

      for (++it; it!=v.end(); ++it)
        result += wxT(", ") + human_readable<T>::convert(*it);
    }

  result += wxT("]");

  return result;
} // human_readable::convert() [list]

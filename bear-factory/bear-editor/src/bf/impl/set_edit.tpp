/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/set_edit.tpp
 * \brief Implementation of the bf::set_edit class.
 * \author Julien Jorge
 */

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param parent The windows owning this one.
 * \param choices The valid choices of the value.
 * \param v The initial value.
 */
template<typename Type>
bf::set_edit<Type>::set_edit
( wxWindow& parent, const wxArrayString& choices, const value_type& v )
  : simple_edit<Type>(v),
    wxChoice( &parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, choices )
{
  CLAW_PRECOND( !choices.IsEmpty() );

  value_updated();
} // set_edit::set_edit()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the displayed value is correct and, if it is, set the
 *        value according to the display.
 */
template<typename Type>
bool bf::set_edit<Type>::validate()
{
  return this->value_from_string( this->GetStringSelection() );
} // set_edit::validate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when the value has been changed, to update the display.
 */
template<typename Type>
void bf::set_edit<Type>::value_updated()
{
  bool found = false;
  wxString val = this->value_to_string();
  unsigned int i=0;

  while ( (i!=this->GetCount()) && !found )
    if ( val == this->GetString(i) )
      found = true;
    else
      ++i;

  if ( found )
    this->SetSelection(i);
  else
    {
      this->SetSelection(0);
      validate();
    }
} // set_edit::value_updated()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/impl/base_edit.tpp
 * \brief Implementation of the bf::base_edit class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor from a value.
 * \param v The value.
 */
template<typename Type>
bf::base_edit<Type>::base_edit( const value_type& v )
  : m_value(v)
{

} // base_edit::base_edit()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
template<typename Type>
bf::base_edit<Type>::~base_edit()
{
  // nothing to do
} // base_edit::~base_edit()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the value.
 */
template<typename Type>
const typename bf::base_edit<Type>::value_type&
bf::base_edit<Type>::get_value() const
{
  return m_value;
} // base_edit::get_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value.
 * \param v The new value.
 */
template<typename Type>
void bf::base_edit<Type>::set_value( const value_type& v )
{
  m_value = v;
  value_updated();
} // base_edit::set_value()

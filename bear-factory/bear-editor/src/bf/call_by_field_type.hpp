/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/call_by_field_type.hpp
 * \brief A function object that calls an other function object according to the
 *        type of a field.
 * \author Julien Jorge
 */
#ifndef __BF_CALL_BY_FIELD_TYPE_HPP__
#define __BF_CALL_BY_FIELD_TYPE_HPP__

namespace bf
{
  class type_field;

  /**
   * \brief A function object that calls an other function object according to
   *        the type of a field.
   *
   * \b Template \b parameters:
   * - \param F A template function object taking a field type as its argument.
   *
   * \author Julien Jorge
   */
  template<template<typename T> class F, typename R>
  class call_by_field_type
  {
  public:
    template<typename A1>
    R operator()( const type_field& f, A1& a1 ) const;

    template<typename A1, typename A2>
    R operator()( const type_field& f, A1& a1, A2& a2 ) const;

  private:
    template<typename Type, typename A1>
    R do_call( A1& a1 ) const;

    template<typename Type, typename A1, typename A2>
    R do_call( A1& a1, A2& a2 ) const;

  }; // class call_by_field_type
} // namespace bf

#include "bf/impl/call_by_field_type.tpp"

#endif // __BF_CALL_BY_FIELD_TYPE_HPP__

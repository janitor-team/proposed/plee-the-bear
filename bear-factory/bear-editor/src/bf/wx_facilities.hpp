/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/wx_facilities.hpp
 * \brief Some facilities for using the wxWidget library with other libraries
 *        (namely standard library).
 * \author Julien Jorge
 */
#ifndef __BF_WX_FACILITIES_HPP__
#define __BF_WX_FACILITIES_HPP__

#include <wx/bmpbuttn.h>
#include <wx/string.h>
#include <wx/menu.h>
#include <string>

namespace bf
{
  /**
   * \brief Create a std::string from a wxString.
   * \param str The reference string.
   */
  inline std::string wx_to_std_string( const wxString& str )
  {
    return std::string(str.char_str());
  } // wx_to_std_string()

  /**
   * \brief Create a std::string from a wxString.
   * \param str The reference string.
   * \param conv The type of the convertion.
   */
  inline wxString std_to_wx_string
  ( const std::string& str, wxMBConv& conv = wxConvLocal )
  {
    return wxString( str.c_str(), conv );
  } // std_to_wx_string()

  /**
   * \brief Append a menu item with an icon.
   * \param menu The menu in which the item is added.
   * \param id The identifier of the menu entry.
   * \param label The string to appear on the menu item.
   * \param help An help string associated with the item.
   * \param icon An icon displayed next to the item.
   */
  inline void wx_menu_append_item
  ( wxMenu* menu, int id, const wxString& label, const wxString& help,
    const wxBitmap& icon = wxBitmap() )
  {
    wxMenuItem* item = new wxMenuItem(menu, id, label, help);
    item->SetBitmap(icon);
    menu->Append(item);
  } // wx_menu_append_item()

  /**
   * \brief Create a wxBitmapButton.
   * \param parent The owner of the button.
   * \param id The identifier of the button.
   * \param bmp The bitmap displayed in the button.
   * \param tip The message displayed in the tool tip.
   */
  inline wxBitmapButton* wx_new_bitmap_button
  ( wxWindow* parent, int id, const wxBitmap& bmp, const wxString& tip )
  {
    wxBitmapButton* result = new wxBitmapButton(parent, id, bmp);
    result->SetToolTip(tip);
    return result;
  } // wx_new_bitmap_button()

} // namespace bf

#endif // __BF_WX_FACILITIES_HPP__

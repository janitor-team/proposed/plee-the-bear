/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sample.hpp
 * \brief The value of a field of type "sample".
 * \author Julien Jorge
 */
#ifndef __BF_SAMPLE_HPP__
#define __BF_SAMPLE_HPP__

#include "bf/bitmap_rendering_attributes.hpp"
#include "bf/libeditor_export.hpp"

#include <string>

namespace bf
{
  /**
   * \brief The value of a field of type "sample".
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT sample:
    public bitmap_rendering_attributes
  {
  public:
    sample();

    void set_path( const std::string& name );
    void set_loops( const unsigned int loops );
    void set_volume( const double v );

    unsigned int get_loops() const;
    double get_volume() const;
    const std::string& get_path() const;

    bool operator==( const sample& that ) const;
    bool operator!=( const sample& that ) const;

    void compile( compiled_file& f ) const;

  private:
    /** \brief The path to the resource to use. */
    std::string m_path;

    /** \brief How many times the sample will be played. */
    unsigned int m_loops;

    /** \brief The volume at which the sample is played. */
    double m_volume;

  }; // class sample
} // namespace bf

#endif // __BF_SAMPLE_HPP__

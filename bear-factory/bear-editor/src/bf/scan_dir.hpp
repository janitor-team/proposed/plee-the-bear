/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/scan_dir.hpp
 * \brief Search a given directory for files with specific extensions.
 * \author Julien Jorge
 */
#ifndef __BF_SCAN_DIR_HPP__
#define __BF_SCAN_DIR_HPP__

namespace bf
{
  /**
   * \brief Search a given directory for files with specific extensions.
   * \param Func The type of a copyable function object used to process the
   *        files.
   *
   * This class scan a given directory and call Func( file_name ) on each
   * file_name found in the directory, ending with one of the given extensions.
   *
   * \author Julien Jorge
   */
  template<typename Func>
  class scan_dir
  {
  public:
    template<typename Iterator>
    void operator()
    ( const std::string& dir, Func& f, Iterator first_ext, Iterator last_ext );

  private:
    template<typename Iterator>
    bool supported_extension
    ( const std::string& dir, Iterator first_ext, Iterator last_ext );

  }; // class scan_dir
} // namespace bf

#include "bf/impl/scan_dir.tpp"

#endif // __BF_SCAN_DIR_HPP__

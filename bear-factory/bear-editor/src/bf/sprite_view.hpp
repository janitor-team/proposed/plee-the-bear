/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sprite_view.hpp
 * \brief This control displays a sprite.
 * \author Julien Jorge
 */
#ifndef __BF_SPRITE_VIEW_HPP__
#define __BF_SPRITE_VIEW_HPP__

#include <wx/wx.h>

#include "bf/sprite_image_cache.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief This control displays a sprite.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT sprite_view:
    public wxWindow
  {
  public:
    sprite_view( wxWindow& parent, const sprite& spr = sprite() );

    void set_sprite( const sprite& spr );
    void set_zoom( unsigned int z );
    unsigned int get_zoom() const;
    void auto_zoom();

    wxPoint get_view_position() const;
    wxSize get_view_size() const;
    void set_view_delta( int x, int y );

  private:
    void render();

    void fill_background( wxDC& dc ) const;
    void draw_sprite( wxDC& dc ) const;
    void draw_box( wxDC& dc ) const;

    void make_sprite_image();

    void on_size(wxSizeEvent& event);
    void on_paint(wxPaintEvent& event);

  private:
    /** \brief The pattern of the background. */
    wxBitmap m_background_pattern;

    /** \brief The sprite displayed. */
    sprite m_sprite;

    /** \brief The sprite scaled at the window's size. */
    wxBitmap m_sprite_image;

    /** \brief The position of the sprite in the view. */
    wxPoint m_sprite_position;

    /** \brief A delta applied to the sprite when rendering. */
    wxPoint m_sprite_delta;

    /** \brief A cache of the sprites. */
    sprite_image_cache m_image_cache;

    /** \brief The zoom ratio. */
    unsigned int m_zoom;

    DECLARE_EVENT_TABLE()

  }; // class sprite_view
} // namespace bf

#endif // __BF_SPRITE_VIEW_HPP__

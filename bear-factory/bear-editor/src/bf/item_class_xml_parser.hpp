/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_class_xml_parser.hpp
 * \brief A class that reads an XML file describing an item class and creates
 *        a corresponding item_class instance.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_CLASS_XML_PARSER_HPP__
#define __BF_ITEM_CLASS_XML_PARSER_HPP__

#include "bf/item_class.hpp"
#include "bf/type_field.hpp"

#include <wx/xml/xml.h>
#include <string>
#include <list>
#include <utility>

namespace bf
{
  class item_class_pool;

  /**
   * \brief A class that reads an XML file describing an item class and creates
   *        a corresponding item_class instance.
   * \author Julien Jorge
   */
  class item_class_xml_parser
  {
  public:
    static std::string get_item_class_name( const std::string& file_path );

    item_class* read
    ( const item_class_pool& pool, const std::string& file_path ) const;

  private:
    void parse_item_node
    ( item_class& item, const item_class_pool& pool,
      const wxXmlNode* node ) const;
    void read_item_properties( item_class& item, const wxXmlNode* node ) const;

    void read_item_fields( item_class& item, const wxXmlNode* node ) const;
    void read_new_default_value
    ( item_class& item, const wxXmlNode* node ) const;
    void read_removed_field( item_class& item, const wxXmlNode* node ) const;
    void read_description
    ( item_class& item, const wxXmlNode* node ) const;
    void read_field_type( item_class& item, const wxXmlNode* node ) const;

    type_field*
    add_integer_field( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_u_integer_field( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_real_field( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_string_field( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_boolean_field( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_sprite_field( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_animation_field( const std::string& name, const wxXmlNode* node ) const;
    type_field* add_item_reference_field
    ( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_font_field( const std::string& name, const wxXmlNode* node ) const;
    type_field*
    add_sample_field( const std::string& name, const wxXmlNode* node ) const;

    std::string read_after( const wxXmlNode* node ) const;

    void read_set( const wxXmlNode* node, std::list<std::string>& set ) const;

    template<typename T, bool SetAvailable, bool RangeAvailable>
    type_field*
    create_field( const std::string& name, type_field::field_type ft,
                  const wxXmlNode* node ) const;

    template<typename T>
    std::pair<T, T> read_interval( const wxXmlNode* node ) const;

  }; // class item_class_xml_parser
} // namespace bf

#endif // __BF_ITEM_CLASS_XML_PARSER_HPP__


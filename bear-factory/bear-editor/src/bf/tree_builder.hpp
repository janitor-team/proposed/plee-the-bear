/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/tree_builder.hpp
 * \brief A class to help building trees (wxTreeCtrl).
 * \author Julien Jorge
 */
#ifndef __BF_TREE_BUILDER_HPP__
#define __BF_TREE_BUILDER_HPP__

#include "bf/libeditor_export.hpp"

#include <wx/treectrl.h>
#include <claw/tree.hpp>

#include <string>

namespace bf
{
  /**
   * \brief A class to help building trees (wxTreeCtrl).
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT tree_builder
  {
  private:
    typedef claw::tree<std::string> tree_type;

  public:
    void add_entries( const std::string& entries, char sep );
    void create_wxTree( wxTreeCtrl& result ) const;

  private:
    void create_wxTree
    ( wxTreeCtrl& result, wxTreeItemId parent, const tree_type& t ) const;

    void
    insert_entries( tree_type& node, std::list<std::string>& entries ) const;

  private:
    /** \brief the tree currently built. */
    tree_type m_tree;

  }; // class tree_builder
} // namespace bf

#endif // __BF_TREE_BUILDER_HPP__

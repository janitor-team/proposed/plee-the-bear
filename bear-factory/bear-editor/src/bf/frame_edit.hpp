/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/frame_edit.hpp
 * \brief The window showing the properties of a frame.
 * \author Sebastien Angibaud
 */
#ifndef __BF_FRAME_EDIT_HPP__
#define __BF_FRAME_EDIT_HPP__

#include "bf/free_edit.hpp"
#include "bf/animation_frame.hpp"
#include "bf/custom_type.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/dialog.h>

namespace bf
{
  class sprite_edit;

  /**
   * \brief The window showing the properties of a frame.
   * \author Sebastien Angibaud
   */
  class BEAR_EDITOR_EXPORT frame_edit:
    public wxDialog
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_DURATION,
  IDC_SPRITE
      }; // enum control_id

  public:
    frame_edit
    ( wxWindow& parent,
      const animation_frame& frame = default_value<animation_frame>::get());

    const animation_frame& get_frame() const;

  private:
    void validate();
    void create_controls();
    void create_sizer_controls();

    void fill();
    void edit_sprite();
    void edit_duration();

    void on_ok(wxCommandEvent& event);

  private:
   /** \brief The edited frame. */
    animation_frame m_frame;

    /** \brief The control for duration. */
    free_edit<real_type>* m_duration;

    /** \brief The control in which we display the sprite. */
    sprite_edit* m_sprite;

    // DECLARE_EVENT_TABLE()

  }; // class frame_edit
} // namespace bf

#endif // __BF_FRAME_EDIT_HPP__

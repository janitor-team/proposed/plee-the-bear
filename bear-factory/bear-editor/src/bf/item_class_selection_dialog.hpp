/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/item_class_selection_dialog.hpp
 * \brief A dialog to select an item class.
 * \author Julien Jorge
 */
#ifndef __BF_ITEM_CLASS_SELECTION_DIALOG_HPP__
#define __BF_ITEM_CLASS_SELECTION_DIALOG_HPP__

#include <string>
#include <wx/dialog.h>

namespace bf
{
  class class_tree_ctrl;
  class class_selected_event;
  class item_class_pool;

  /**
   * \brief A dialog to select an item class.
   * \author Julien Jorge
   */
  class item_class_selection_dialog:
    public wxDialog
  {
  public:
    item_class_selection_dialog
    ( const item_class_pool& pool, wxWindow* parent,
      const std::string& class_name = std::string() );

    const std::string& get_class_name() const;

  private:
    void on_ok(wxCommandEvent& event);
    void on_class_selected( class_selected_event& event );

  private:
    /** \brief The name of the selected class. */
    std::string m_class_name;

    /** \brief The tree of classes. */
    class_tree_ctrl* m_tree;

    DECLARE_EVENT_TABLE()

  }; // class item_class_selection_dialog
} // namespace bf

#endif // __BF_ITEM_CLASS_SELECTION_DIALOG_HPP__

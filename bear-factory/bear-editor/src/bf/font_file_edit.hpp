/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/font_file_edit.hpp
 * \brief Control for editing a field whose value is a path to a font.
 * \author Sébastien Angibaud
 */
#ifndef __BF_FONT_FILE_EDIT_HPP__
#define __BF_FONT_FILE_EDIT_HPP__

#include "bf/base_file_edit.hpp"
#include "bf/custom_type.hpp"
#include "bf/default_value.hpp"
#include "bf/libeditor_export.hpp"

namespace bf
{
  /**
   * \brief Control for editing a field whose value is a path to a font.
   * \author Sébastien Angibaud
   */
  class BEAR_EDITOR_EXPORT font_file_edit:
    public base_file_edit<font_file_type>
  {
  private:
    typedef base_file_edit<font_file_type> super;

  public:
    font_file_edit
    ( wxWindow& parent,
      const font_file_type& v =
      default_value<font_file_type>::get() );

  }; // class font_file_edit
} // namespace bf

#endif // __BF_FONT_FILE_EDIT_HPP__

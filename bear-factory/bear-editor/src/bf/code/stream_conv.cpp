/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/stream_conv.cpp
 * \brief Implementation the the specialisations of the bf::stream_conv class.
 * \author Julien Jorge
 */
#include "bf/stream_conv.hpp"

#include "bf/wx_facilities.hpp"
#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a stream.
 * \param is The stream in which we read.
 * \param v The value we have read.
 */
std::istream&
bf::stream_conv<bf::string_type>::read( std::istream& is, value_type& v )
{
  value_type::value_type real_v;

  // an empty string is ok
  if ( is.rdbuf()->in_avail() == 0 )
    v.set_value(real_v);
  else if ( std::getline(is, real_v) )
    v.set_value(real_v);

  return is;
} // read()

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the value from a stream.
 * \param is The stream in which we read.
 * \param v The value we have read.
 */
std::istream&
bf::stream_conv< bf::custom_type<bool> >::read
( std::istream& is, value_type& v )
{
  std::string t;
  bool result = false;

  // an empty string is ok
  if ( std::getline(is, t) )
    if ( (t == "1") || (t == "true")
         || (std_to_wx_string(t) == _("true")) )
      result = true;

  v.set_value(result);

  return is;
} // read()

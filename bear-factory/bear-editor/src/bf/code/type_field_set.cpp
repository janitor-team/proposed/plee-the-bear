/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/type_field_set.cpp
 * \brief Implementation of the bf::type_field_set class.
 * \author Julien Jorge
 */
#include "bf/type_field_set.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param name The name of the field.
 * \param ft The type of the field.
 * \param values The valid values for this field.
 */
bf::type_field_set::type_field_set
( const std::string& name, field_type ft, const std::list<std::string>& values )
  : type_field( name, ft ), m_values(values)
{

} // type_field_set::type_field_set()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a copy of this instance.
 */
bf::type_field* bf::type_field_set::clone() const
{
  return new type_field_set(*this);
} // type_field_set::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the type of the range of valid values.
 */
bf::type_field::range_type bf::type_field_set::get_range_type() const
{
  return field_range_set;
} // type_field_set::get_range_type()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the set of valid values for this field.
 * \param values (out) The valid values.
 */
void bf::type_field_set::get_set( std::list<std::string>& values ) const
{
  values = m_values;
} // type_field_set::get_set()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/*
 * \file bf/code/item_event.cpp
 * \brief Implementation of the bf::item_event class.
 * \author Julien Jorge
 */
#include "bf/item_event.hpp"

/*----------------------------------------------------------------------------*/
const wxEventType bf::item_event::selection_event_type = wxNewEventType();

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param t The type of the event.
 * \param id the id of the window that generates the event.
 */
bf::item_event::item_event( wxEventType t, wxWindowID id )
  : wxNotifyEvent(t, id), m_item(NULL)
{

} // item_event::item_event()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param t The type of the event.
 * \param id the id of the window that generates the event.
 */
bf::item_event::item_event( item_instance* item, wxEventType t, wxWindowID id )
  : wxNotifyEvent(t, id), m_item(item)
{

} // item_event::item_event()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 * \param that The instance to copy from.
 */
bf::item_event::item_event( const item_event& that )
  : wxNotifyEvent(that), m_item(that.m_item)
{

} // item_event::item_event()

/*----------------------------------------------------------------------------*/
/**
 * \brief Allocate a copy of this instance.
 */
wxEvent* bf::item_event::Clone() const
{
  return new item_event(*this);
} // item_event::Clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the item concerned by this event.
 */
bf::item_instance* bf::item_event::get_item() const
{
  return m_item;
} // item_event::get_item()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/sample.cpp
 * \brief Implementation of the bf::sample class.
 * \author Julien Jorge
 */
#include "bf/sample.hpp"

#include "bf/compiled_file.hpp"
#include "bf/path_configuration.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Default constructor.
 */
bf::sample::sample()
  : m_loops(1), m_volume(1)
{

} // sample::sample()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the path to the resource to use.
 * \param name The new value.
 */
void bf::sample::set_path( const std::string& name )
{
  m_path = name;
} // sample::set_path()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the number of times the sample will be played.
 * \param loops The new value.
 */
void bf::sample::set_loops( const unsigned int loops )
{
  m_loops = loops;
} // sample::set_loops()

/*----------------------------------------------------------------------------*/
/**
 * \brief The volume at which the sample is played.
 * \param v The new value, in the interval (0, 1).
 */
void bf::sample::set_volume( const double v )
{
  if (v > 1)
    m_volume = 1;
  else if ( v < 0 )
    m_volume = 0;
  else
    m_volume = v;
} // sample::set_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get how many times the sample will be played.
 */
unsigned int bf::sample::get_loops() const
{
  return m_loops;
} // sample::get_loops()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the volume at which the sample is played.
 */
double bf::sample::get_volume() const
{
  return m_volume;
} // sample::get_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the path to the resource to use.
 */
const std::string& bf::sample::get_path() const
{
  return m_path;
} // sample::get_path()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if two samples are the same.
 * \param that The instance to compare to.
 */
bool bf::sample::operator==( const sample& that ) const
{
  return (m_path == that.m_path)
    && (m_loops == that.m_loops)
    && (m_volume == that.m_volume);
} // sample::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if two samples are different.
 * \param that The instance to compare to.
 */
bool bf::sample::operator!=( const sample& that ) const
{
  return !(*this == that);
} // sample::operator!=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compile the sample.
 * \param f The stream in which we write the compiled sample.
 */
void bf::sample::compile( compiled_file& f ) const
{
  std::string path(m_path);

  if ( path_configuration::get_instance().expand_file_name(path) )
    path_configuration::get_instance().get_relative_path(path);

  f << path << m_loops << m_volume;
} // sample::compile()

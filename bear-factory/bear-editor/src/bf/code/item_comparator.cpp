/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/item_comparator.cpp
 * \brief Implementation of the bf::item_comparator class.
 * \author Julien Jorge
 */
#include "bf/item_comparator.hpp"

#include "bf/item_instance.hpp"

/*----------------------------------------------------------------------------*/
bool bf::item_comparator::by_place::operator()
( const item_instance& a, const item_instance& b ) const
{
  bool result = false;

  if ( a.get_rendering_parameters().get_left()
       < b.get_rendering_parameters().get_left() )
    result = true;
  else if ( a.get_rendering_parameters().get_left()
            == b.get_rendering_parameters().get_left() )
    {
      if ( a.get_rendering_parameters().get_bottom()
           < b.get_rendering_parameters().get_bottom() )
        result = true;
      else if ( a.get_rendering_parameters().get_bottom()
                == b.get_rendering_parameters().get_bottom() )
        {
          if ( a.get_rendering_parameters().get_width()
               < b.get_rendering_parameters().get_width() )
            result = true;
          else if ( a.get_rendering_parameters().get_width()
                    == b.get_rendering_parameters().get_width() )
            result = ( a.get_rendering_parameters().get_height()
                       < b.get_rendering_parameters().get_height() );
        }
    }

  return result;
} // item_comparator::by_place::operator()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file animation_frame.cpp
 * \brief Implementation of the bf::animation_frame class.
 * \author Julien Jorge
 */
#include "bf/animation_frame.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Default constructor.
 */
bf::animation_frame::animation_frame()
  : m_sprite(), m_duration(1)
{

} // animation_frame::animation_frame()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the sprite.
 * \param spr The new sprite.
 */
void bf::animation_frame::set_sprite(const sprite& spr)
{
  m_sprite = spr;
} // animation_frame::set_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the duration.
 * \param duration The new duration.
 */
void bf::animation_frame::set_duration(double duration)
{
  m_duration = duration;
} // animation_frame::set_duration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sprite.
 */
const bf::sprite& bf::animation_frame::get_sprite() const
{
  return m_sprite;
} // animation_frame::get_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the duration.
 */
double bf::animation_frame::get_duration() const
{
  return m_duration;
} // animation_frame::get_duration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if two frames are the same.
 * \param that The other frame.
 */
bool bf::animation_frame::operator==( const animation_frame& that ) const
{
  return (m_sprite == that.m_sprite) && (m_duration == that.m_duration);
} // animation_frame::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compare with an other animation frame in lexicographic order of their
 *        attributes.
 * \param that The other animation frame.
 */
bool bf::animation_frame::operator<(const animation_frame& that) const
{
  if ( m_sprite != that.m_sprite )
    return m_sprite < that.m_sprite;

  return m_duration < that.m_duration;
} // animation_frame::operator<()

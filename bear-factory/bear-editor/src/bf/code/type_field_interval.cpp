/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/type_field_interval.cpp
 * \brief Implementation of the specialized methods of the
 *        bf::type_field_interval class.
 * \author Julien Jorge
 */
#include "bf/type_field_interval.hpp"

namespace bf
{
  /*--------------------------------------------------------------------------*/
  /**
   * \brief Constructor.
   * \param name The name of the field.
   * \param min The minimum valid value of the interval.
   * \param max The maximum valid value of the interval.
   */
  template<>
  type_field_interval<int>::type_field_interval
  ( const std::string& name, int min, int max )
    : type_field( name, type_field::integer_field_type ), m_min(min), m_max(max)
  {

  } // type_field_interval::type_field_interval()
}

namespace bf
{
  /*--------------------------------------------------------------------------*/
  /**
   * \brief Constructor.
   * \param name The name of the field.
   * \param min The minimum valid value of the interval.
   * \param max The maximum valid value of the interval.
   */
  template<>
  type_field_interval<unsigned int>::type_field_interval
  ( const std::string& name, unsigned int min, unsigned int max )
    : type_field( name, type_field::u_integer_field_type ), m_min(min),
      m_max(max)
  {

  } // type_field_interval::type_field_interval()
}

namespace bf
{
  /*--------------------------------------------------------------------------*/
  /**
   * \brief Constructor.
   * \param name The name of the field.
   * \param min The minimum valid value of the interval.
   * \param max The maximum valid value of the interval.
   */
  template<>
  type_field_interval<double>::type_field_interval
  ( const std::string& name, double min, double max )
    : type_field(name, type_field::real_field_type), m_min(min), m_max(max)
  {

  } // type_field_interval::type_field_interval()
}

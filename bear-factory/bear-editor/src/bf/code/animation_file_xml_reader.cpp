/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/animation_file_xml_reader.cpp
 * \brief Implementation of the bf::animation_file_xml_reader class.
 * \author Julien Jorge
 */
#include "bf/animation_file_xml_reader.hpp"

#include "bf/wx_facilities.hpp"
#include "bf/xml/exception.hpp"
#include "bf/xml/xml_to_value.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a animation.
 * \param file_path The path to the animation file.
 */
bf::animation
bf::animation_file_xml_reader::load( const wxString& file_path ) const
{
  wxXmlDocument doc;

  if ( !doc.Load(file_path) )
    throw std::ios_base::failure
      ( "Cannot load the XML file '" + wx_to_std_string(file_path) + "'" );

  wxXmlNode* node = doc.GetRoot();

  if ( node == NULL )
    throw xml::missing_node("animation");

  return load( node );
} // animation_file_xml_reader::load()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a node of type "animation".
 * \param node The node to parse.
 */
bf::animation bf::animation_file_xml_reader::load( wxXmlNode* node ) const
{
  CLAW_PRECOND( node != NULL );

  if ( node->GetName() != wxT("animation") )
    throw xml::bad_node( wx_to_std_string(node->GetName()) );

  animation anim;
  xml::xml_to_value<animation> xml_conv;

  xml_conv( anim, node );

  return anim;
} // animation_file_xml_reader::load_animation()

/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file human_readable.cpp
 * \brief Implementation of the bf::human_readable class.
 * \author Julien Jorge
 */
#include "bf/human_readable.hpp"

#include <wx/intl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
wxString bf::human_readable<bf::bool_type>::convert( const value_type& v )
{
  if ( v.get_value() )
    return _("true");
  else
    return _("false");
} // human_readable::convert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
wxString bf::human_readable<bf::string_type>::convert( const value_type& v )
{
  std::ostringstream oss;

  oss << v;

  return wxT("\"") + std_to_wx_string(oss.str()) + wxT("\"");
} // human_readable::convert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
wxString bf::human_readable<bf::sprite>::convert( const value_type& v )
{
  std::ostringstream oss;

  oss << "'" << v.get_image_name() << "', x=" << v.get_left()
      << ", y=" << v.get_top() << ", clip_w=" << v.get_clip_width()
      << ", clip_h=" << v.get_clip_height() << ", w=" << v.width()
      << ", h=" << v.height() << ", a=" << v.get_opacity();

  if ( v.is_mirrored() )
    oss << ", mirror";

  if ( v.is_flipped() )
    oss << ", flip";

  return _("sprite:") + std_to_wx_string(oss.str());
} // human_readable::convert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
wxString bf::human_readable<bf::animation>::convert( const value_type& v )
{
  std::ostringstream oss;

  oss << "'a=" << v.get_opacity() << ", loops=" << v.get_loops();

  if ( v.is_mirrored() )
    oss << ", mirror";

  if ( v.is_flipped() )
    oss << ", flip";

  if ( v.get_loop_back() )
    oss << ", loop_back";

  oss << ", first_index=" << v.get_first_index() <<
    ", last_index=" << v.get_last_index();

  return _("animation:") + std_to_wx_string(oss.str());
} // human_readable::convert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
wxString
bf::human_readable<bf::animation_file_type>::convert( const value_type& v )
{
  return _("animation file:") + std_to_wx_string(v.get_path());
} // human_readable::convert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
wxString
bf::human_readable<bf::any_animation>::convert( const value_type& v )
{
  if ( v.get_content_type() == any_animation::content_animation )
    return human_readable<animation>::convert( v.get_animation() );
  else
    return human_readable<animation_file_type>::convert
      ( v.get_animation_file() );
} // human_readable::convert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a value in a string representation.
 * \param v The value to transform.
 */
wxString bf::human_readable<bf::sample>::convert( const value_type& v )
{
  std::ostringstream oss;

  oss << "'" << v.get_path() << "', loops=" << v.get_loops()
      << ", volume=" << v.get_volume();

  return _("sample:") + std_to_wx_string(oss.str());
} // human_readable::convert()

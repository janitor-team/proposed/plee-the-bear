/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/item_check_result.cpp
 * \brief Implementation of the bf::item_check_result class.
 * \author Julien Jorge
 */
#include "bf/item_check_result.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Add an error in the result.
 * \param e The error to add.
 */
void bf::item_check_result::add( const check_error& e )
{
  m_errors.push_back(e);
} // item_check_result::add()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is no error in the result.
 */
bool bf::item_check_result::is_ok() const
{
  return m_errors.empty();
} // item_check_result::is_ok()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an iterator on the beginning of the errors.
 */
bf::item_check_result::const_iterator bf::item_check_result::begin() const
{
  return m_errors.begin();
} // item_check_result::begin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an iterator on the end of the errors.
 */
bf::item_check_result::const_iterator bf::item_check_result::end() const
{
  return m_errors.end();
} // item_check_result::end()

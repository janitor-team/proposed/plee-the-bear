/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/animation_file_xml_writer.cpp
 * \brief Implementation of the bf::animation_file_xml_writer class.
 * \author Julien Jorge
 */
#include "bf/animation_file_xml_writer.hpp"

#include "bf/xml/value_to_xml.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Save a animation.
 * \param os The stream in which we write.
 * \param anim The animation to save.
 */
void bf::animation_file_xml_writer::save
( std::ostream& os, const animation& anim ) const
{
  os << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  xml::value_to_xml<animation>::write(os, anim);
} // animation_file_xml_writer::save()

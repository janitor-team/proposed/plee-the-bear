/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/class_not_found.cpp
 * \brief Implementation of the bf::xml* exceptions classes.
 */
#include "bf/class_not_found.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param class_name The name of the class.
 */
bf::class_not_found::class_not_found( const std::string& class_name )
  : m_msg( "Can't find item class '" + class_name + "'"),
    m_class_name(class_name)
{

} // class_not_found::class_not_found()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bf::class_not_found::~class_not_found() throw()
{

} // class_not_found::class_not_found()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an explanation of the problem.
 */
const char* bf::class_not_found::what() const throw()
{
  return m_msg.c_str();
} // class_not_found::what()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the class.
 */
const std::string& bf::class_not_found::class_name() const throw()
{
  return m_class_name;
} // class_not_found::class_name()

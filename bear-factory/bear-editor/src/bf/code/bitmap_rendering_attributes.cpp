/*
  Bear Engine - Editor library

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bitmap_rendering_attributes.cpp
 * \brief Implementation of the  bitmap_rendering_attributes class.
 * \author Julien Jorge
 */
#include "bf/bitmap_rendering_attributes.hpp"

#include "bf/compiled_file.hpp"

#include <claw/assert.hpp>
#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bf::bitmap_rendering_attributes::bitmap_rendering_attributes()
  : m_size(0, 0), m_flip(false), m_mirror(false), m_opacity(1),
    m_red_intensity(1), m_green_intensity(1), m_blue_intensity(1), m_angle(0),
    m_auto_size(true)
{

} // bitmap_rendering_attributes::bitmap_rendering_attributes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if two bitmap_rendering_attributes are the same.
 * \param that The instance to compare to.
 */
bool bf::bitmap_rendering_attributes::operator==
( const bitmap_rendering_attributes& that ) const
{
  return (m_size == that.m_size)
    && (m_flip == that.m_flip)
    && (m_mirror == that.m_mirror)
    && (m_opacity == that.m_opacity)
    && (m_red_intensity == that.m_red_intensity)
    && (m_green_intensity == that.m_green_intensity)
    && (m_blue_intensity == that.m_blue_intensity)
    && (m_angle == that.m_angle)
    && (m_auto_size == that.m_auto_size);
} // bitmap_rendering_attributes::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if two bitmap_rendering_attributes are different.
 * \param that The instance to compare to.
 */
bool bf::bitmap_rendering_attributes::operator!=
( const bitmap_rendering_attributes& that ) const
{
  return !(*this == that);
} // bitmap_rendering_attributes::operator!=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compare two bitmap_rendering_attributes in lexicographic order.
 * \param that The instance to compare to.
 */
bool bf::bitmap_rendering_attributes::operator<
( const bitmap_rendering_attributes& that ) const
{
  if (m_size.x != that.m_size.x)
    return m_size.x < that.m_size.x;

  if (m_size.y != that.m_size.y)
    return m_size.y < that.m_size.y;

  if (m_flip != that.m_flip)
    return m_flip < that.m_flip;

  if (m_mirror != that.m_mirror)
    return m_mirror < that.m_mirror;

  if (m_opacity != that.m_opacity)
    return m_opacity < that.m_opacity;

  if (m_red_intensity != that.m_red_intensity)
    return m_red_intensity < that.m_red_intensity;

  if (m_green_intensity != that.m_green_intensity)
    return m_green_intensity < that.m_blue_intensity;

  if (m_blue_intensity != that.m_blue_intensity)
    return m_blue_intensity < that.m_blue_intensity;

  if ( m_angle != that.m_angle )
    return m_angle < that.m_angle;

  return m_auto_size < that.m_auto_size;
} // bitmap_rendering_attributes::operator<()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the opacity translucy.
 */
double bf::bitmap_rendering_attributes::get_opacity() const
{
  return m_opacity;
} // bitmap_rendering_attributes::get_opacity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the opacity translucy.
 */
void
bf::bitmap_rendering_attributes::set_opacity( double opacity )
{
  m_opacity = opacity;
} // bitmap_rendering_attributes::set_opacity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the intensity of the red channel.
 */
double bf::bitmap_rendering_attributes::get_red_intensity() const
{
  return m_red_intensity;
} // bitmap_rendering_attributes::get_red_intensity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the intensity of the green channel.
 */
double bf::bitmap_rendering_attributes::get_green_intensity() const
{
  return m_green_intensity;
} // bitmap_rendering_attributes::get_green_intensity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the intensity of the blue channel.
 */
double bf::bitmap_rendering_attributes::get_blue_intensity() const
{
  return m_blue_intensity;
} // bitmap_rendering_attributes::get_blue_intensity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the intensity of the channels.
 * \param r Value on the r component.
 * \param g Value on the r component.
 * \param b Value on the r component.
 */
void bf::bitmap_rendering_attributes::set_intensity
( double r, double g, double b )
{
  if (r > 1)
    m_red_intensity = 1;
  else if (r < 0)
    m_red_intensity = 0;
  else
    m_red_intensity = r;

  if (g > 1)
    m_green_intensity = 1;
  else if (g < 0)
    m_green_intensity = 0;
  else
    m_green_intensity = g;

  if (b > 1)
    m_blue_intensity = 1;
  else if (b < 0)
    m_blue_intensity = 0;
  else
    m_blue_intensity = b;
} // bitmap_rendering_attributes::set_color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the size is automatically computed.
 * \param a Tell if the size is automatically computed.
 */
void bf::bitmap_rendering_attributes::set_auto_size( bool a )
{
  m_auto_size = a;
} // bitmap_rendering_attributes::set_auto_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the size is automatically computed.
 */
bool bf::bitmap_rendering_attributes::get_auto_size() const
{
  return m_auto_size;
} // bitmap_rendering_attributes::get_auto_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size.
 */
const claw::math::coordinate_2d<unsigned int>&
bf::bitmap_rendering_attributes::get_size() const
{
  return m_size;
} // bitmap_rendering_attributes::get_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the size.
 * \param s The new size.
 */
void bf::bitmap_rendering_attributes::set_size
( const claw::math::coordinate_2d<unsigned int>& s )
{
  m_size = s;
} // bitmap_rendering_attributes::set_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the size.
 * \param w The new width.
 * \param h The new height.
 */
void bf::bitmap_rendering_attributes::set_size
( unsigned int w, unsigned int h )
{
  m_size.set(w, h);
} // bitmap_rendering_attributes::set_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the width.
 * \param w The new width.
 */
void bf::bitmap_rendering_attributes::set_width( unsigned int w )
{
  m_size.x = w;
} // bitmap_rendering_attributes::set_width()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the height.
 * \param h The new height.
 */
void bf::bitmap_rendering_attributes::set_height( unsigned int h )
{
  m_size.y = h;
} // bitmap_rendering_attributes::set_height()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the width.
 */
unsigned int bf::bitmap_rendering_attributes::width() const
{
  return m_size.x;
} // bitmap_rendering_attributes::width()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the height.
 */
unsigned int bf::bitmap_rendering_attributes::height() const
{
  return m_size.y;
} // bitmap_rendering_attributes::height()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the angle.
 * \param a The new angle.
 */
void bf::bitmap_rendering_attributes::set_angle( double a )
{
  m_angle = a;
} // bitmap_rendering_attributes::set_angle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the angle.
 */
double bf::bitmap_rendering_attributes::get_angle() const
{
  return m_angle;
} // bitmap_rendering_attributes::get_angle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set vertical inversion.
 * \param b The new value for the inversion.
 */
void bf::bitmap_rendering_attributes::flip( bool b )
{
  m_flip = b;
} // bitmap_rendering_attributes::flip()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set horizontal inversion.
 * \param b The new value for the inversion.
 */
void bf::bitmap_rendering_attributes::mirror( bool b )
{
  m_mirror = b;
} // bitmap_rendering_attributes::mirror()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get vertical inversion.
 */
bool bf::bitmap_rendering_attributes::is_flipped() const
{
  return m_flip;
} // bitmap_rendering_attributes::is_flipped()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get horizontal inversion.
 */
bool bf::bitmap_rendering_attributes::is_mirrored() const
{
  return m_mirror;
} // bitmap_rendering_attributes::is_mirrored()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compile the attributes.
 * \param f The stream in which we write the compilation.
 */
void bf::bitmap_rendering_attributes::compile( compiled_file& f ) const
{
  f << m_size.x << m_size.y << m_mirror << m_flip << m_opacity
    << m_red_intensity << m_green_intensity << m_blue_intensity << m_angle;
} // bitmap_rendering_attributes::compile()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy the values of an other bitmap_rendering_attributes in this one.
 * \param that The bitmap_rendering_attributes from which we copy.
 */
bf::bitmap_rendering_attributes& bf::bitmap_rendering_attributes::assign
( const bitmap_rendering_attributes& that )
{
  return *this = that;
} // bitmap_rendering_attributes::assign()

/*----------------------------------------------------------------------------*/
/**
 * \brief Combine with an other set of attributes.
 * \param that The attributes to combine with.
 *
 * The attributes changed by this method are : is_flipped(), is_mirrored(), the
 * intensities, the opacity and the angle. The size is not changed.
 */
void bf::bitmap_rendering_attributes::combine
( const bitmap_rendering_attributes& that )
{
  flip( that.is_flipped() ^ is_flipped() );
  mirror( that.is_mirrored() ^ is_mirrored() );
  set_intensity
    ( that.get_red_intensity() * get_red_intensity(),
      that.get_green_intensity() * get_green_intensity(),
      that.get_blue_intensity() * get_blue_intensity()
      );
  set_opacity( that.get_opacity() * get_opacity() );
  set_angle( that.get_angle() + get_angle() );
} // bitmap_rendering_attributes::combine()

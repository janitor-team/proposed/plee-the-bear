/*
    Bear Engine - Level editor

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/code/check_error.cpp
 * \brief Implementation of the bf::check_error class.
 * \author Julien Jorge
 */
#include "bf/check_error.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param msg A description of the error.
 */
bf::check_error::check_error( const std::string& msg )
  : m_message(msg)
{

} // check_error::check_error()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param cause A description of the element on which the error occured.
 * \param msg A description of the error.
 */
bf::check_error::check_error( const std::string& cause, const std::string& msg )
  : m_cause(cause), m_message(msg)
{

} // check_error::check_error()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the description of the element on which the error occured.
 */
const std::string& bf::check_error::get_cause() const
{
  return m_cause;
} // check_error::get_cause()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the description of the error.
 */
const std::string& bf::check_error::get_message() const
{
  return m_message;
} // check_error::get_message()

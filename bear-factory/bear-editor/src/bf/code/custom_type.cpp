/*
    Bear Engine - Level compiler

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file custom_type.cpp
 * \brief Implementation of the bf::custom_type class.
 * \author Julien Jorge
 */
#include "bf/custom_type.hpp"

#include "bf/compiled_file.hpp"
#include "bf/path_configuration.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Output the value in the compiled file.
 * \param cf The file to write in.
 */
void bf::base_file_type::compile( compiled_file& cf ) const
{
  std::string p(get_value());

  if ( path_configuration::get_instance().expand_file_name(p) )
    path_configuration::get_instance().get_relative_path(p);

  cf << p;
} // base_file_type::compile()




/*----------------------------------------------------------------------------*/
/**
 * \brief Compare two item_reference_type.
 * \param that The value to compare to.
 */
bool bf::item_reference_type::operator==( const item_reference_type& that) const
{
  return this->get_value() == that.get_value();
} // item_reference_type::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compare two item_reference_type.
 * \param that The value to compare to.
 */
bool bf::item_reference_type::operator!=( const item_reference_type& that) const
{
  return this->get_value() != that.get_value();
} // item_reference_type::operator!=()




/*----------------------------------------------------------------------------*/
/**
 * \brief Compare two font_file_type.
 * \param that The value to compare to.
 */
bool bf::font_file_type::operator==( const font_file_type& that) const
{
  return this->get_value() == that.get_value();
} // font_file_type::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compare two font_file_type.
 * \param that The value to compare to.
 */
bool bf::font_file_type::operator!=( const font_file_type& that) const
{
  return this->get_value() != that.get_value();
} // font_file_type::operator!=()

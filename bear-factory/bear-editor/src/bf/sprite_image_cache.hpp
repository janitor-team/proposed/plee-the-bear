/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/sprite_image_cache.hpp
 * \brief A cache of resized or rotated sprites.
 * \author Julien Jorge
 */
#ifndef __BF_SPRITE_IMAGE_CACHE_HPP__
#define __BF_SPRITE_IMAGE_CACHE_HPP__

#include "bf/sprite.hpp"
#include "bf/libeditor_export.hpp"

#include <map>
#include <wx/bitmap.h>

namespace bf
{
  /**
   * \brief A cache of resized or rotated sprites.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT sprite_image_cache
  {
  private:
    /**
     * \brief The key of the sprite map.
     */
    class key_type
    {
    public:
      key_type( const sprite& s, unsigned int w, unsigned int h );

      bool operator<( const key_type& k ) const;

      const sprite& get_sprite() const;
      unsigned int get_width() const;
      unsigned int get_height() const;

    private:
      /** \brief The sprite. */
      sprite m_sprite;

      /** \brief The width of the resulting image. */
      unsigned int m_width;

      /** \brief The height of the resulting image. */
      unsigned int m_height;

      /** \brief String representation of the string. */
      std::string m_key_string;

    }; // class key_type

    /**
     * \brief The values stored in the cache.
     */
    struct value_type
    {
      /** \brief The resulting image and the distance between the top left
          corner of the original image and the one of the rotated image. */
      std::pair<wxBitmap, wxPoint> resulting_image;

      /** \brief Evaluation of the usefulness of this cache entry. */
      int usage_count;

    }; // struct value_type

  public:
    std::pair<wxBitmap, wxPoint> get_image
    ( const sprite& s, unsigned int w, unsigned int h );

    void clear();

  private:
    std::pair<wxBitmap, wxPoint> add_image( const key_type& k );
    std::pair<wxBitmap, wxPoint>
    apply_effects( const key_type& k, wxBitmap bmp ) const;

  private:
    /** \brief The cache of images. */
    std::map<key_type, value_type> m_cache;

  }; // class sprite_image_cache
} // namespace bf

#endif // __BF_SPRITE_IMAGE_CACHE_HPP__

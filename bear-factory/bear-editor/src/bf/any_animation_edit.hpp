/*
    Bear Engine - Editor library

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bf/any_animation_edit.hpp
 * \brief Control for editing a field whose value is any animation.
 * \author Julien Jorge
 */
#ifndef __BF_ANY_ANIMATION_EDIT_HPP__
#define __BF_ANY_ANIMATION_EDIT_HPP__

#include "bf/any_animation.hpp"
#include "bf/base_edit.hpp"
#include "bf/default_value.hpp"
#include "bf/libeditor_export.hpp"

#include <wx/wx.h>

namespace bf
{
  class animation_edit;
  class animation_file_edit;

  /**
   * \brief Control for editing a field whose value is any animation.
   * \author Julien Jorge
   */
  class BEAR_EDITOR_EXPORT any_animation_edit:
    public wxPanel,
    public base_edit<any_animation>
  {
  public:
    /** \brief The identifiers of the controls. */
    enum control_id
      {
        IDC_CONTENT_TYPE
      }; // enum control_id

  private:
    typedef base_edit<any_animation> super;

  public:
    any_animation_edit
    ( wxWindow& parent,
      const any_animation& v =
      default_value<any_animation>::get() );

    bool validate();

  private:
    void value_updated();
    void fill_controls();

    any_animation::content_type get_visible_content_type() const;

    void create_controls();
    void create_sizer_controls();

    void on_switch_content_type( wxCommandEvent& event );

  private:
    /** \brief The control in which we display the type of the content currently
        edited. */
    wxChoice* m_content_type;

    /** \brief The control used to edit the animation. */
    animation_edit* m_animation_edit;

    /** \brief The control used to edit the animation file. */
    animation_file_edit* m_animation_file_edit;

    DECLARE_EVENT_TABLE()

  }; // class any_animation_edit
} // namespace bf

#endif // __BF_ANY_ANIMATION_EDIT_HPP__

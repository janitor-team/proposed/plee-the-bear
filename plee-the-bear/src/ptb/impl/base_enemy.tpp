/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file base_enemy.tpp
 * \brief Implementation of the ptb::base_enemy class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/headstone.hpp"

#include "ptb/base_enemy.hpp"
#include "ptb/item/floating_score.hpp"
#include "ptb/level_variables.hpp"

#include "engine/base_item.hpp"
#include "visual/scene_line.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
template<class Base>
ptb::base_enemy<Base>::base_enemy()
  : m_score(1000)
{

} // base_enemy::base_enemy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
template<class Base>
void ptb::base_enemy<Base>::build()
{
  super::build();

  level_variables::set_object_count
        ( this->get_level(), "enemy",
          level_variables::get_object_count( this->get_level(), "enemy" ) + 1);
} // base_enemy::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c unsigned \c integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool ptb::base_enemy<Base>::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result = true;

  if ( name == "base_enemy.score" )
    m_score = value;
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // base_enemy::set_u_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sprite representing the item.
 * \param visuals (out) The sprites of the item, and their positions.
 */
template<class Base>
void
ptb::base_enemy<Base>::get_visual
( std::list<bear::engine::scene_visual>& visuals ) const
{
  super::get_visual(visuals);

  if ( monster::is_injured() )
    {
      std::vector<bear::visual::position_type> p;
      p.resize(2);

      p[0].x = this->get_horizontal_middle()-25;
      p[0].y = this->get_top() + 20;
      p[1].x = p[0].x + 50;
      p[1].y = p[0].y;

      bear::engine::scene_visual scene
        ( bear::visual::scene_line(0, 0, claw::graphic::red_pixel, p, 2),
          this->get_z_position()-1 );

      visuals.push_back( scene );

      p[0].x = this->get_horizontal_middle()-25;
      p[0].y = this->get_top() + 20;
      p[1].x = p[0].x +
        50 * monster::get_energy() / monster::get_max_energy();
      p[1].y = p[0].y;

      bear::engine::scene_visual scene2
        ( bear::visual::scene_line(0, 0, claw::graphic::green_pixel, p, 2),
          this->get_z_position() );

      visuals.push_back( scene2 );
    }
} // base_enemy::get_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create an headstone.
 */
template<class Base>
void ptb::base_enemy<Base>::create_headstone
( const bear::universe::position_type& bottom_middle_pos,
  const bear::visual::animation& soul_anim,
  unsigned int soul_energy,
  unsigned int pos_z)
{
  headstone* new_headstone = new headstone;

  new_headstone->set_soul_animation(soul_anim);
  new_headstone->set_bottom_middle( bottom_middle_pos );
  new_headstone->set_soul_energy(soul_energy);
  new_headstone->set_z_position(pos_z );

  CLAW_ASSERT( new_headstone->is_valid(),
               "The headstone of wasp isn't correctly initialized" );

  super::new_item( *new_headstone );
} // base_enemy::create_headstone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a floating score.
 * \param index The index of the player.
 */
template<class Base>
void ptb::base_enemy<Base>::create_floating_score
( unsigned int index ) const
{
  floating_score* s = new floating_score;

  super::new_item(*s);

  s->set_z_position( super::get_z_position() + 10 );
  s->set_center_of_mass( super::get_center_of_mass() );

  s->add_points( index, m_score );
} // base_enemy::create_floating_score()

/*----------------------------------------------------------------------------*/
/**
 * \brief The enemy dies.
 * \param attacker The attacker monster.
 */
template<class Base>
void ptb::base_enemy<Base>::die(const monster& attacker)
{
  super::set_mass(0.1);
  super::set_offensive_phase(false);

  if ( ( ( attacker.get_monster_type() == monster::player_monster ) ||
         ( attacker.get_monster_type() == monster::stone_monster ) ) &&
       ( ( attacker.get_monster_index() == 1 ) ||
         ( attacker.get_monster_index() == 2 ) ) )
    create_floating_score(attacker.get_monster_index());
  else
     {
       create_floating_score(1);
       create_floating_score(2);
     }

  level_variables::set_killed_object_count
        ( this->get_level(), "enemy",
          level_variables::get_killed_object_count
        ( this->get_level(), "enemy" ) + 1);
} // base_enemy::die()

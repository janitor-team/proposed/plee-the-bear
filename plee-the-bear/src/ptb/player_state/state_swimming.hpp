/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_swimming.hpp
 * \brief The class describing the state where the player swimming.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_STATE_SWIMMING_HPP__
#define __PTB_STATE_SWIMMING_HPP__

#include "ptb/player_state/state_player.hpp"

namespace ptb
{
  /**
   * \brief The class describing a state where the player swim.
   * \author Sebastien Angibaud
   */
  class state_swimming : public ptb::state_player
  {
  public:
    /** \brief The type of the parent class. */
    typedef ptb::state_player super;

  public:
    state_swimming(const player_proxy& player);

    virtual std::string get_name() const;

    virtual void do_look_upward();
    virtual void do_crouch();
    virtual void do_continue_look_upward();
    virtual void do_continue_crouch();
    virtual void do_jump();
  }; // class state_swimming
} // namespace ptb

#endif // __PTB_STATE_SWIMMING_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_paralyze.cpp
 * \brief Implementation of the ptb::state_paralyze class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_paralyze.hpp"

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_paralyze::state_paralyze( const player_proxy& player_instance )
  : state_player(player_instance)
{

} // state_paralyze::state_paralyze()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_paralyze::get_name() const
{
  return "paralyze";
} // state_paralyze::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialization of this state.
 */
void ptb::state_paralyze::start()
{
 m_player_instance.set_marionette(true);
} // state_paralyze::start()

/*----------------------------------------------------------------------------*/
/**
 * \brief The end of the action.
 */
void ptb::state_paralyze::stop()
{
  m_player_instance.set_marionette(false);
} // state_paralyze::stop()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_paralyze::do_move_left()
{
  // we can't move in this state
} // state_paralyze::do_move_left()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_paralyze::do_move_right()
{
   // we can't move in this state
} // state_paralyze::do_move_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_paralyze::do_jump()
{
   // we can't jump in this state
} // state_paralyze::do_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player doing slap.
 */
void ptb::state_paralyze::do_slap()
{
   // we can't slap in this state
} // state_paralyze::do_slap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player throw a stone
 */
void ptb::state_paralyze::do_start_throw()
{
 // we can't throw in this state
} // state_paralyze::start_throw

/*----------------------------------------------------------------------------*/
/**
 * \brief Player drop a power.
 */
void ptb::state_paralyze::do_start_drop()
{
 // we can't drop in this state
} // state_paralyze::do_start_drop()

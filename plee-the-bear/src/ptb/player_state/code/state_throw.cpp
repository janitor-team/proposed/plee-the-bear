/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_throw.cpp
 * \brief Implementation of the ptb::state_throw class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_throw.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_throw::state_throw( const player_proxy& player_instance  )
  : state_player(player_instance)
{

} // state_throw::state_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_throw::get_name() const
{
  return "throw";
} // state_throw::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialization of this state.
 */
void ptb::state_throw::start()
{
  m_player_instance.update_throw_time_ratio();
} // state_throw::start()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player want start throw.
 */
void ptb::state_throw::do_start_throw()
{
  // Player can't throw an other stone.
} // state_throw::do_start_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player look upward.
 */
void ptb::state_throw::do_look_upward()
{
  m_player_instance.set_throw_up(true);
} // state_throw::do_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to look upward.
 */
void ptb::state_throw::do_continue_look_upward()
{
  m_player_instance.set_throw_up(true);
} // state_throw::do_continue_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player crouch.
 */
void ptb::state_throw::do_crouch()
{
  m_player_instance.set_throw_down(true);
} // state_throw::do_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to crouch.
 */
void ptb::state_throw::do_continue_crouch()
{
  m_player_instance.set_throw_down(true);
} // state_throw::do_continue_crouch()


/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_game_over.cpp
 * \brief Implementation of the ptb::state_game_over class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_game_over.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_game_over::state_game_over( const player_proxy& player_instance )
  : state_player(player_instance)
{
} // state_game_over::state_game_over()


/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_game_over::get_name() const
{
  return "game_over";
} // state_game_over::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_game_over::do_move_left()
{
  // do nothing
} // state_game_over::do_move_left()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_game_over::do_move_right()
{
  // do nothing
} // state_game_over::do_move_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_game_over::do_jump()
{
  // do nothing
} // state_game_over::do_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player doing slap.
 */
void ptb::state_game_over::do_slap()
{
  // do nothing
} // state_game_over::do_slap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player throw.
 */
void ptb::state_game_over::do_start_throw()
{
  // do nothing
} // state_game_over::do_start_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player drop a power.
 */
void ptb::state_game_over::do_start_drop()
{
  // do nothing
} // state_game_over::do_start_drop()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player is injured.
 */
void ptb::state_game_over::do_injured()
{
  // do nothing
} // state_game_over::do_injured()

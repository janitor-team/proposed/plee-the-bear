/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_start_cling.cpp
 * \brief Implementation of the ptb::state_start_cling class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_start_cling.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_start_cling::state_start_cling( const player_proxy& player_instance)
  : state_player(player_instance)
{
} // state_start_cling::state_start_cling()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialization of this state.
 */
void ptb::state_start_cling::start()
{
  m_player_instance.set_offensive_phase(true);
  m_initial_normal_defensive_power =
    m_player_instance.get_defensive_power(monster::normal_attack);
  m_player_instance.set_defensive_power(monster::normal_attack, true);
} // state_start_cling::start()

/*----------------------------------------------------------------------------*/
/**
 * \brief The end of the action.
 */
void ptb::state_start_cling::stop()
{
  m_player_instance.set_offensive_phase(false);
  m_player_instance.set_defensive_power
    (monster::normal_attack, m_initial_normal_defensive_power);
} // state_start_cling::stop()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_start_cling::get_name() const
{
  return "start_cling";
} // state_start_cling::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player doing slap.
 */
void ptb::state_start_cling::do_slap()
{
} // state_start_cling::do_slap()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_look_upward.cpp
 * \brief Implementation of the ptb::state_look_upward class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_look_upward.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_look_upward::state_look_upward( const player_proxy& player_instance )
  : state_player(player_instance)
{

} // state_look_upward::state_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_look_upward::get_name() const
{
  return "look_upward";
} // state_look_upward::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_look_upward::do_move_left()
{
  super::do_move_left();
  m_player_instance.choose_walk_state();
} // state_look_upward::do_move_left()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_look_upward::do_move_right()
{
  super::do_move_right();
  m_player_instance.choose_walk_state();
} // state_look_upward::do_move_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_look_upward::do_jump()
{
  if ( m_player_instance.has_bottom_contact() )
    m_player_instance.start_action_model("start_jump");
} // state_look_upward::do_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop to look up.
 */
void ptb::state_look_upward::do_stop_look_upward()
{
   if ( m_player_instance.get_status_crouch() )
     m_player_instance.start_action_model("crouch");
   else
     m_player_instance.choose_idle_state();
} // state_look_upward::do_stop_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player crouch.
 */
void ptb::state_look_upward::do_crouch()
{
  m_player_instance.start_action_model("crouch");
} // state_look_upward::do_crouch()

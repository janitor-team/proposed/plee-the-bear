/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_float.cpp
 * \brief Implementation of the ptb::state_float class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_float.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_float::state_float( const player_proxy& player_instance  )
  : state_player(player_instance)
{

} // state_float::state_float()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_float::get_name() const
{
  return "float";
} // state_float::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialization of this state.
 */
void ptb::state_float::start()
{
  m_density = m_player_instance.get_density();
  m_player_instance.set_density(0.85);
  m_player_instance.set_system_angle(0);
} // state_float::start()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop this state.
 */
void ptb::state_float::stop()
{
  m_player_instance.set_density(m_density);
} // state_float::stop()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_float::do_jump()
{
  // the bottom_contact is not required
  if ( m_player_instance.get_current_action_name() == "float" )
    m_player_instance.apply_impulse_jump();
} // state_float::do_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player crouch.
 */
void ptb::state_float::do_crouch()
{
  m_player_instance.apply_dive();
} // state_float::do_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to crouch.
 */
void ptb::state_float::do_continue_crouch()
{
  m_player_instance.apply_dive();
} // state_float::do_continue_crouch()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_slap.cpp
 * \brief Implementation of the ptb::state_slap class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_slap.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_slap::state_slap( const player_proxy& player_instance )
  : state_player(player_instance)
{

} // state_slap::state_slap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_slap::get_name() const
{
  return "slap";
} // state_slap::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start the action.
 */
void ptb::state_slap::start()
{
  m_initial_normal_defensive_power =
    m_player_instance.get_defensive_power( monster::normal_attack );
} // state_slap::stop()

/*----------------------------------------------------------------------------*/
/**
 * \brief The end of the action.
 */
void ptb::state_slap::stop()
{
  m_player_instance.set_offensive_phase(false);
  m_player_instance.set_defensive_power
    (monster::normal_attack, m_initial_normal_defensive_power);
} // state_slap::stop()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player doing slap.
 */
void ptb::state_slap::do_slap()
{
  // Player can't do a new slap.
} // state_slap::do_slap()

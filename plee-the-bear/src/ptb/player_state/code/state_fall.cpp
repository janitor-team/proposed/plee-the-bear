/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_fall.cpp
 * \brief Implementation of the ptb::state_fall class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_fall.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_fall::state_fall( const player_proxy& player_instance )
  : state_player(player_instance)
{

} // state_fall::state_fall()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_fall::get_name() const
{
  return "fall";
} // state_fall::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player throw.
 */
void ptb::state_fall::do_start_throw()
{
  m_player_instance.start_action_model("maintain_and_fall");
} // state_fall::do_start_throw()

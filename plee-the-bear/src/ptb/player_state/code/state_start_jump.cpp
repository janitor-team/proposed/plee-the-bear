/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_start_jump.cpp
 * \brief Implementation of the ptb::state_start_jump class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_start_jump.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_start_jump::state_start_jump( const player_proxy& player_instance)
  : state_player(player_instance)
{
} // state_start_jump::state_start_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_start_jump::get_name() const
{
  return "start_jump";
} // state_start_jump::get_name()

/*---------------------------------------------------------------------------*/
/**
 * \brief Progress the spot.
 */
void ptb::state_start_jump::progress_spot()
{
  claw::math::coordinate_2d<int> gap(0, -5);
  m_player_instance.add_spot_gap(gap);
} // state_start_jump::progress_spot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player want an vertical jump.
 */
void ptb::state_start_jump::do_stop_vertical_jump()
{
  m_player_instance.set_air_float(false);

  if ( m_player_instance.has_bottom_contact() )
    m_player_instance.start_action_model("vertical_jump");
} // state_start_jump::do_stop_vertical_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_start_jump::do_move_left()
{
  // Player can't move on the left in this state
} // state_start_jump::do_move_left()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_start_jump::do_move_right()
{
  // Player can't move on the right in this state
} // state_start_jump::do_move_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop an vertical jump.
 */
void ptb::state_start_jump::do_stop_look_upward()
{
  m_player_instance.choose_idle_state();
} // state_start_jump::do_stop_look_upward()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_maintain.cpp
 * \brief Implementation of the ptb::state_maintain class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_maintain.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_maintain::state_maintain( const player_proxy& player_instance  )
  : state_player(player_instance)
{

} // state_maintain::state_maintain()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_maintain::get_name() const
{
  return "maintain";
} // state_maintain::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialization of this state.
 */
void ptb::state_maintain::start()
{
  m_player_instance.set_throw_up(false);
  m_player_instance.set_throw_down(false);
} // state_maintain::start()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player want start throw.
 */
void ptb::state_maintain::do_start_throw()
{
  // Player can't maintain an other stone.
} // state_maintain::do_start_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player want maintain.
 */
void ptb::state_maintain::do_stop_throw()
{
  if ( m_player_instance.get_current_action_name() == "maintain_and_fall" )
    m_player_instance.start_action_model("throw_and_fall");
  else if ( m_player_instance.get_current_action_name() == "maintain_and_walk" )
    m_player_instance.start_action_model("throw_and_walk");
  else
    m_player_instance.start_action_model("throw");
} // state_maintain::do_stop_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player look upward.
 */
void ptb::state_maintain::do_look_upward()
{
  m_player_instance.set_throw_up(true);
} // state_maintain::do_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to look upward.
 */
void ptb::state_maintain::do_continue_look_upward()
{
  m_player_instance.set_throw_up(true);
} // state_maintain::do_continue_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player crouch.
 */
void ptb::state_maintain::do_crouch()
{
  m_player_instance.set_throw_down(true);
} // state_maintain::do_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to crouch.
 */
void ptb::state_maintain::do_continue_crouch()
{
  m_player_instance.set_throw_down(true);
} // state_maintain::do_continue_crouch()


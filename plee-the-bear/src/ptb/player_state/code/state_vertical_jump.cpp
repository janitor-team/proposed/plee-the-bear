/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_vertical_jump.cpp
 * \brief Implementation of the ptb::state_vertical_jump class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_vertical_jump.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_vertical_jump::state_vertical_jump( const player_proxy& player_instance  )
  : state_player(player_instance)
{

} // state_vertical_jump::state_vertical_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_vertical_jump::get_name() const
{
  return "vertical_jump";
} // state_vertical_jump::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialization of this state.
 */
void ptb::state_vertical_jump::start()
{
  double force = m_player_instance.get_vertical_jump_force() *
    m_player_instance.get_jump_time_ratio();

  m_player_instance.add_external_force( bear::universe::force_type(0, force));
  m_player_instance.add_internal_force( bear::universe::force_type(0, force));
} // state_vertical_jump::start()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_vertical_jump::do_jump()
{
  // Player can't jump
} // state_vertical_jump::do_jump()

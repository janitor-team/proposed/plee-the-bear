/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_sink.cpp
 * \brief Implementation of the ptb::state_sink class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_sink.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_sink::state_sink( const player_proxy& player_instance )
  : state_player(player_instance)
{

} // state_sink::state_sink()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_sink::get_name() const
{
  return "sink";
} // state_sink::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player look upward.
 */
void ptb::state_sink::do_look_upward()
{
  m_player_instance.apply_swim_up();
} // state_sink::do_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to look_upward.
 */
void ptb::state_sink::do_continue_look_upward()
{
  m_player_instance.apply_swim_up();
} // state_sink::do_continue_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player crouch.
 */
void ptb::state_sink::do_crouch()
{
  m_player_instance.apply_swim_down();
} // state_sink::do_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to crouch.
 */
void ptb::state_sink::do_continue_crouch()
{
  m_player_instance.apply_swim_down();
} // state_sink::do_continue_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_sink::do_jump()
{
  m_player_instance.start_action_model("swimming");
} // state_sink::do_jump()


/*----------------------------------------------------------------------------*/
/**
 * \brief Do continue a jump.
 */
void ptb::state_sink::do_continue_jump()
{
  m_player_instance.start_action_model("swimming");
} // state_sink::do_continuejump()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_walk.cpp
 * \brief Implementation of the ptb::state_walk class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_walk.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_walk::state_walk( const player_proxy& player_instance )
  : state_player(player_instance)
{

} // state_walk::state_walk()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_walk::get_name() const
{
  return "walk";
} // state_walk::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player doing slap.
 */
void ptb::state_walk::do_slap()
{
  m_player_instance.start_action_model("slap_and_walk");
} // state_walk::do_slap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player throw.
 */
void ptb::state_walk::do_start_throw()
{
  m_player_instance.start_action_model("maintain_and_walk");
} // state_walk::do_start_throw()

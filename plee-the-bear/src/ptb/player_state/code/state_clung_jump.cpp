/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_clung_jump.cpp
 * \brief Implementation of the ptb::state_clung_jump class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_clung_jump.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_clung_jump::state_clung_jump( const player_proxy& player_instance  )
  : state_player(player_instance)
{

} // state_clung_jump::state_clung_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_clung_jump::get_name() const
{
  return "clung_jump";
} // state_clung_jump::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a clung_jump.
 */
void ptb::state_clung_jump::do_jump()
{
  // we can't jump in this state.
} // state_clung_jump::do_jump()

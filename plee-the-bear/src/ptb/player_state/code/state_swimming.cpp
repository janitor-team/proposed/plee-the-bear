/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_swimming.cpp
 * \brief Implementation of the ptb::state_swimming class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_swimming.hpp"


/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_swimming::state_swimming( const player_proxy& player_instance  )
  : state_player(player_instance)
{

} // state_swimming::state_swimming()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_swimming::get_name() const
{
  return "swimming";
} // state_swimming::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player look up.
 */
void ptb::state_swimming::do_look_upward()
{
  m_player_instance.apply_swim_up();
} // state_swimming::do_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to look_upward.
 */
void ptb::state_swimming::do_continue_look_upward()
{
  m_player_instance.apply_swim_up();
} // state_swimming::do_continue_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player crouch.
 */
void ptb::state_swimming::do_crouch()
{
  m_player_instance.apply_swim_down();
} // state_swimming::do_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player continue to crouch.
 */
void ptb::state_swimming::do_continue_crouch()
{

  m_player_instance.apply_swim_down();
} // state_swimming::do_continue_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_swimming::do_jump()
{
  // Do nothing
} // state_swimming::do_jump()

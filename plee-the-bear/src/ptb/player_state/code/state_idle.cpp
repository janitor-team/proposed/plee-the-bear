/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_idle.cpp
 * \brief Implementation of the ptb::state_idle class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_idle.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_idle::state_idle( const player_proxy& player_instance )
  : state_player(player_instance)
{

} // state_idle::state_idle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_idle::get_name() const
{
  return "idle";
} // state_idle::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_idle::do_move_left()
{
  super::do_move_left();
  m_player_instance.choose_walk_state();
} // state_idle::do_move_left()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_idle::do_move_right()
{
  super::do_move_right();
  m_player_instance.choose_walk_state();
} // state_idle::do_move_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player look up.
 */
void ptb::state_idle::do_look_upward()
{
  m_player_instance.start_action_model("look_upward");
} // state_idle::do_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Continue to look up.
 */
void ptb::state_idle::do_continue_look_upward()
{
  m_player_instance.start_action_model("look_upward");
} // state_idle::do_continue_look_upward()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player crouch.
 */
void ptb::state_idle::do_crouch()
{
  m_player_instance.start_action_model("crouch");
} // state_idle::do_crouch()

/*----------------------------------------------------------------------------*/
/**
 * \brief Continue to crouch.
 */
void ptb::state_idle::do_continue_crouch()
{
  m_player_instance.start_action_model("crouch");
} // state_idle::do_continue_crouch()

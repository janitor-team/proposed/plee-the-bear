/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_cling.cpp
 * \brief Implementation of the ptb::state_cling class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_cling.hpp"

#include "universe/world.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_cling::state_cling( const player_proxy& player_instance)
  : state_player(player_instance)
{
} // state_cling::state_cling()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_cling::get_name() const
{
  return "cling";
} // state_cling::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player doing slap.
 */
void ptb::state_cling::do_slap()
{
  m_player_instance.start_action_model("fall");
} // state_cling::do_slap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_cling::do_jump()
{
  m_player_instance.apply_clung_jump();
  m_player_instance.start_action_model("fall");
} // state_cling::do_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_cling::do_move_left()
{
  if ( !m_player_instance.get_rendering_attributes().is_mirrored() )
    m_player_instance.set_want_clung_jump(true);
} // state_cling::do_move_left()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_cling::do_move_right()
{
  if ( m_player_instance.get_rendering_attributes().is_mirrored() )
    m_player_instance.set_want_clung_jump(true);
} // state_cling::do_move_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player want start throw.
 */
void ptb::state_cling::do_start_throw()
{
} // state_cling::do_start_throw()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_roar.cpp
 * \brief Implementation of the ptb::state_roar class.
 * \author Sebastien Angibaud
 */
#include "ptb/player_state/state_roar.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::state_roar::state_roar( const player_proxy& player_instance )
  : state_player(player_instance)
{
} // state_roar::state_roar()


/*----------------------------------------------------------------------------*/
/**
 * \brief Return the name of the state.
 */
std::string ptb::state_roar::get_name() const
{
  return "roar";
} // state_roar::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_roar::do_move_left()
{
  // we can't move in this state
} // state_roar::do_move_left()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the player to the left.
 */
void ptb::state_roar::do_move_right()
{
   // we can't move in this state
} // state_roar::do_move_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a jump.
 */
void ptb::state_roar::do_jump()
{
   // we can't jump in this state
} // state_roar::do_jump()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the player doing slap.
 */
void ptb::state_roar::do_slap()
{
   // we can't slap in this state
} // state_roar::do_slap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Player throw a stone
 */
void ptb::state_roar::do_start_throw()
{
 // we can't throw in this state
} // state_roar::start_throw

/*----------------------------------------------------------------------------*/
/**
 * \brief Player drop a power.
 */
void ptb::state_roar::do_start_drop()
{
  // we can't drop in this state
} // state_roar::do_start_drop()


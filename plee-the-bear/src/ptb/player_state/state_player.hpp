/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file state_plee.cpp
 * \brief Definition of the class describing a state of Plee.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_STATE_PLAYER_HPP__
#define __PTB_STATE_PLAYER_HPP__

#include "ptb/player_proxy.hpp"

#include "engine/scene_visual.hpp"

#include <string>

namespace ptb
{
  /**
   * \brief The class describing a state of the player (Player).
   * \author Julien Jorge
   */
  class state_player
  {
  public:
    state_player(const player_proxy& player_instance);
    virtual ~state_player() {};

  public:
    virtual std::string get_name() const { return "undefined state"; };
    virtual void start() {};
    virtual void stop() {};

    virtual void do_move_left();
    virtual void do_move_right();
    virtual void do_jump();
    virtual void do_continue_jump();
    virtual void do_slap();
    virtual void do_stop_vertical_jump();
    virtual void do_look_upward();
    virtual void do_continue_look_upward();
    virtual void do_stop_look_upward();
    virtual void do_continue_crouch();
    virtual void do_crouch();
    virtual void do_stop_crouch();
    virtual void unchain();
    virtual void chain();
    virtual void do_start_throw();
    virtual void do_start_change_object();
    virtual void do_stop_throw();
    virtual void do_injured();
    virtual void do_finish_injured();

  protected:
    /** \brief The instance of Player. */
    const player_proxy m_player_instance;

  }; // class state_player
} // namespace ptb

#endif // __PTB_STATE_PLAYER_HPP__






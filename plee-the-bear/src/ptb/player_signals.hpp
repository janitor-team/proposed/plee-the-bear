/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player.hpp
 * \brief The class containing all player's signal.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_PLAYER_SIGNALS_HPP__
#define __PTB_PLAYER_SIGNALS_HPP__

#include <boost/signal.hpp>

namespace ptb
{
  /**
   * \brief The class containing all player's signals.
   * \author Sebastien Angibaud
   */
  class player_signals
  {
  public:
    /** \brief The signal when the player enters in the water. */
    boost::signal<void ()> enters_water_zone;

    /** \brief The signal when the player leaves the water. */
    boost::signal<void ()> leaves_water_zone;

    /** \brief The signal when the the oxygen gauge changes. */
    boost::signal<void (double)> oxygen_gauge_changed;

    /** \brief The signal when the player enters in a cold zone. */
    boost::signal<void ()> enters_cold_zone;

    /** \brief The signal when the player gos in a cold zone. */
    boost::signal<void ()> leaves_cold_zone;

    /** \brief The signal when the the ice gauge changes. */
    boost::signal<void (double)> cold_gauge_changed;

    /** \brief The signal when the player enters in a heat zone. */
    boost::signal<void ()> enters_heat_zone;

    /** \brief The signal when the player leaves in a heat zone. */
    boost::signal<void ()> leaves_heat_zone;

    /** \brief The signal when the the heat gauge changes. */
    boost::signal<void (double)> heat_gauge_changed;

    /** \brief The signal when the player wins energy. */
    boost::signal<void (double)> energy_added;

    /** \brief The signal when the player loses energy. */
    boost::signal<void (double)> energy_removed;
  }; // class player_signals
} // namespace ptb

#endif // __PTB_PLAYER_SIGNALS_HPP__

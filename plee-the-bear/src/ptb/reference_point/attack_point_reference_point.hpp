/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file attack_point_reference_point.hpp
 * \brief Instances of attack_point_reference_point compute the reference point
 *        of in forced movements as the attack point of an item.
 * \author Julien Jorge
 */
#ifndef __PTB_ATTACK_POINT_REFERENCE_POINT_HPP__
#define __PTB_ATTACK_POINT_REFERENCE_POINT_HPP__

#include "universe/forced_movement/base_reference_point.hpp"
#include "universe/derived_item_handle.hpp"

namespace ptb
{
  class with_attack_point;

  /**
   * \brief Instances of attack_point_reference_point compute the reference
   *        point of in forced movements as the attack point of an item.
   *
   * \author Julien Jorge
   */
  class attack_point_reference_point:
    public bear::universe::base_reference_point
  {
  private:
    /** The type of the handle on the item. */
    typedef bear::universe::derived_item_handle<with_attack_point>
    item_handle_type;

  public:
    explicit attack_point_reference_point
    ( bear::universe::physical_item& item );

    virtual bear::universe::base_reference_point* clone() const;

    virtual bool is_valid() const;
    virtual bear::universe::position_type get_point() const;

    virtual bool has_item() const;
    virtual bear::universe::physical_item& get_item() const;

  private:
    /** The item on which we take the center of mass. */
    item_handle_type m_item;

  }; // class attack_point_reference_point
} // namespace ptb

#endif // __PTB_ATTACK_POINT_REFERENCE_POINT_HPP__

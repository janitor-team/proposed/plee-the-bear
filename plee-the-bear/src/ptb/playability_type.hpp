/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file playability_type.hpp
 * \brief Type of level playability : number of required players.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_PLAYABILITY_TYPE_HPP__
#define __PTB_PLAYABILITY_TYPE_HPP__

#include <string>

namespace ptb
{
  /**
   * \brief Type of level playability : number of required players.
   * \author Sébastien Angibaud
   */
  struct playability_type
  {
  public:
    /** \brief The type of playability_type. */
    typedef unsigned int value_type;

  public:
    static std::string to_string( value_type a );
    static value_type from_string( const std::string& s );

  public:
    /** \brief Playable with one or two players. */
    static const value_type one_or_two_players = 0;

    /** \brief Playable with one player only. */
    static const value_type one_player_only = one_or_two_players + 1;

    /** \brief Playable with two players only. */
    static const value_type two_players_only = one_player_only + 1;

    /** \brief Playable without player. */
    static const value_type no_player = two_players_only + 1;
  }; // struct playability_type
} // namespace ptb

#endif // __PTB_PLAYABILITY_TYPE_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file boolean_player_function.hpp
 * \brief An expression returning the result of a call of a function on a player
 *        instance.
 * \author Julien Jorge
 */
#ifndef __PTB_BOOLEAN_PLAYER_FUNCTION_HPP__
#define __PTB_BOOLEAN_PLAYER_FUNCTION_HPP__

#include "ptb/player_proxy.hpp"

#include "expr/base_boolean_expression.hpp"

namespace ptb
{
  /**
   * \brief An expression returning the result of a call of a function on a
   *        player instance.
   *
   * The evaluation of this expression is m(*inst), where \a m is the function
   * passed to the constructor, and \a inst the pointer on a pointer.
   *
   * \author Julien Jorge
   */
  template<typename FunctionType>
  class boolean_player_function:
    public bear::expr::base_boolean_expression
  {
  public:
    explicit boolean_player_function
    ( unsigned int player_index = 0,
      const bear::engine::level_globals* g = NULL );
    explicit boolean_player_function
    ( FunctionType m, unsigned int player_index = 0,
      const bear::engine::level_globals* g = NULL );

    base_boolean_expression* clone() const;
    bool evaluate() const;

    void set_function( FunctionType m );
    void set_player_index( unsigned int i );
    void set_level_globals( const bear::engine::level_globals* g );

  private:
    void search_player() const;

  private:
    /** \brief The index of the player on which we work. */
    unsigned int m_player_index;

    /** \brief The instance on which we call the function. */
    mutable player_proxy m_player;

    /** \brief The function to call. */
    FunctionType m_function;

    /** \brief The level_globals of the level in which the player is
        searched. */
    const bear::engine::level_globals* m_level_globals;

  }; // class boolean_player_function

  template<typename FunctionType>
  boolean_player_function<FunctionType>
  boolean_player_function_maker
  ( FunctionType m, unsigned int player_index,
    const bear::engine::level_globals& g );

} // namespace ptb

#include "ptb/expr/impl/boolean_player_function.tpp"

#endif // __PTB_BOOLEAN_PLAYER_FUNCTION_HPP__

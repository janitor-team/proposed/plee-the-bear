/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file level_util.hpp
 * \brief Utility functions about the levels.
 * \author Julien Jorge
 */
#ifndef __PTB_LEVEL_UTIL_HPP__
#define __PTB_LEVEL_UTIL_HPP__

#include <string>

namespace ptb
{
  namespace util
  {
    std::string get_thumbnail( const std::string& level_path );

  } // namespace util
} // namespace ptb

#endif // __PTB_LEVEL_UTIL_HPP__

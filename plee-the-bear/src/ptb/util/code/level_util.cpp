/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file level_util.cpp
 * \brief Implementation of the level utility functions.
 * \author Julien Jorge
 */
#include "ptb/util/level_util.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the default thumbnail path for a given level path.
 * \param level_path The path to the .cl level file.
 */
std::string ptb::util::get_thumbnail( const std::string& level_path )
{
  std::string::size_type first = level_path.find_last_of('/');

  if ( first == std::string::npos )
    first = 0;
  else
    ++first;

  std::string::size_type dot = level_path.find_last_of('.');

  if ( (dot == std::string::npos) || (dot < first) )
    dot = level_path.length();

  std::string::size_type last =
    level_path.find_last_not_of("0123456789", dot-1);

  if ( (last == std::string::npos) || (last < first) )
    last = dot;
  else if ( level_path[last] == '-' )
    --last;

  return "gfx/thumb/" + level_path.substr(first, last - first + 1) + ".png";
} // get_thumbnail()

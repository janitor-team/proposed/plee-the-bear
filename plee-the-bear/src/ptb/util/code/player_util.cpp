/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_util.cpp
 * \brief Utility functions about the players.
 * \author Julien Jorge
 */
#include "ptb/util/player_util.hpp"

#include "ptb/message/get_player_instance.hpp"

#include <sstream>

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of a player.
 * \param player_index The index of the player for which we want the name.
 */
std::string ptb::util::get_player_name( unsigned int player_index )
{
  std::ostringstream oss;
  oss << "player_" << player_index;

  return oss.str();
} // get_player_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Find a player in a level.
 * \param glob The level_globals of the level in which the player is
 *        searched.
 * \param player_index The index of the player we are searching for.
 */
ptb::player_proxy ptb::util::find_player
( const bear::engine::level_globals& glob, unsigned int player_index )
{
  get_player_instance msg;
  glob.send_message( get_player_name(player_index), msg );
  return player_proxy(msg.get_instance());
} // find_player()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file throwable_items_container.hpp
 * \brief The set of throwable_items that Plee can throw.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_THROWABLE_ITEMS_CONTAINER_HPP__
#define __PTB_THROWABLE_ITEMS_CONTAINER_HPP__

#include "ptb/throwable_item/throwable_item.hpp"
#include "universe/types.hpp"

#include <boost/signal.hpp>
#include <map>

namespace ptb
{
  class player_proxy;

  /**
   * \brief The set of throwable_items that Plee can throw.
   * \author Sebastien Angibaud
   */
  class throwable_items_container
  {
  public:
    explicit throwable_items_container();
    ~throwable_items_container();

    void progress( bear::universe::time_type elapsed_time );

    bool has_next() const;
    void next();

    void control_current_throwable_item();
    void select( const::std::string& name );
    throwable_item* get_current_throwable_item() const;

    void add( throwable_item* t );
    throwable_item* remove( const std::string& name );
    bool contains( const std::string& name ) const;

  private:
    std::size_t find_next() const;

  private:
    /* \brief The current throwable_item. */
    std::size_t m_current_throwable_item;

    /* \brief The map of throwable_items. */
    std::vector<throwable_item*> m_throwable_items;

    /* \brief The last stock of the current item. */
    unsigned int m_last_stock;

    /* \brief The last animation of the current item. */
    std::string m_last_animation;

  public:
    /** \brief The signal when the selected throwable item changes. */
    boost::signal<void (const std::string&)> throwable_item_changed;

    /** \brief The signal when stock of current throwable item changes. */
    boost::signal<void (unsigned int)> throwable_item_stock_changed;
  }; // class throwable_items_container
} // namespace ptb

#endif // __PTB_THROWABLE_ITEMS_CONTAINER_HPP__

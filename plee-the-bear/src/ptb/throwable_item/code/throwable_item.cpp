/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file throwable_item.cpp
 * \brief Implementation of the ptb::throwable_item class.
 * \author Sebastien Angibaud
 */
#include "ptb/throwable_item/throwable_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 * \param name The name of this item.
 * \param always_visible Tell if the item is visible even with no stock.
 */
ptb::throwable_item::throwable_item
( const std::string& name, bool always_visible )
  : m_name(name), m_always_visible(always_visible)
{

} // throwable_item::throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::throwable_item::~throwable_item()
{

} // throwable_item::~throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if Plee can throw this throwable_item.
 */
bool ptb::throwable_item::can_throw() const
{
  return get_stock() > 0;
}// throwable_item::can_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if this throwable_item has not must be hidden.
 */
bool ptb::throwable_item::is_empty() const
{
  return !can_throw() && !m_always_visible;
}// throwable_item::is_empty()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the throwable item.
 */
const std::string& ptb::throwable_item::get_name() const
{
  return m_name;
}// throwable_item::get_name()

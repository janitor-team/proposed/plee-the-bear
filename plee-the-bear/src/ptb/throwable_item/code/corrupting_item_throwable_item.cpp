/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file corrupting_item_throwable_item.cpp
 * \brief Implementation of the ptb::corrupting_item_throwable_item class.
 * \author Julien Jorge
 */
#include "ptb/throwable_item/corrupting_item_throwable_item.hpp"

#include "ptb/game_variables.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 * \param anim The animation of the throwable item.
 * \param ref An example of the item to throw.
 * \param ratio The amount of corrupting bonus used per each unit of the amount
 *        of ref.
 */
ptb::corrupting_item_throwable_item::corrupting_item_throwable_item
( const std::string& anim, const corrupting_item& ref, double ratio )
  : throwable_item("corrupting_item", true), m_animation(anim),
    m_reference(ref), m_ratio(ratio)
{

} // corrupting_item_throwable_item::corrupting_item_throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the throwable_item.
 */
bear::engine::base_item*
ptb::corrupting_item_throwable_item::create_throwable_item() const
{
  return m_reference.clone();
} // throwable_item::create_throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Decrease the stock.
 */
void ptb::corrupting_item_throwable_item::decrease_stock() const
{
  const unsigned int c = game_variables::get_corrupting_bonus_count();
  const double s = m_reference.get_amount() * m_ratio;

  if ( c >= s )
    game_variables::set_corrupting_bonus_count( c - s );
  else
    game_variables::set_corrupting_bonus_count(0);
} // throwable_item::decrease_stock()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the stock.
 */
unsigned int ptb::corrupting_item_throwable_item::get_stock() const
{
  return game_variables::get_corrupting_bonus_count();
} // corrupting_item_throwable_item::get_stock()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if Plee can throw this throwable_item.
 */
bool ptb::corrupting_item_throwable_item::can_throw() const
{
  return get_stock() >= m_reference.get_amount();;
}// corrupting_item_throwable_item::can_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the animation of the throwable_item.
 */
std::string ptb::corrupting_item_throwable_item::get_animation() const
{
  return m_animation;
} // corrupting_item_throwable_item::get_animation()

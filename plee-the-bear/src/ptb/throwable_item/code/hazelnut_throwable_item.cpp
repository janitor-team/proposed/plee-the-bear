/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file hazelnut_throwable_item.cpp
 * \brief Implementation of the ptb::hazelnut_throwable_item class.
 * \author Sebastien Angibaud
 */
#include "ptb/throwable_item/hazelnut_throwable_item.hpp"

#include "ptb/level_variables.hpp"
#include "ptb/item/hazelnut.hpp"

const std::string
ptb::hazelnut_throwable_item::s_name = "hazelnut";

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 * \param p The pointer on the player.
 */
ptb::hazelnut_throwable_item::hazelnut_throwable_item(const player_proxy& p)
  : throwable_item("hazelnut"), m_player(p)
{

} // hazelnut_throwable_item::hazelnut_throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the throwable_item.
 */
bear::engine::base_item*
ptb::hazelnut_throwable_item::create_throwable_item() const
{
  return new hazelnut();
} // hazelnut_throwable_item::create_throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Decrease the stock.
 */
void ptb::hazelnut_throwable_item::decrease_stock() const
{
  level_variables::set_current_hazelnut(m_player.get_level(), false);
} // hazelnut::throwable_item::decrease_stock()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the stock.
 */
unsigned int ptb::hazelnut_throwable_item::get_stock() const
{
  if (level_variables::get_current_hazelnut(m_player.get_level()) )
    return 1;
  else
    return 0;
} // hazelnut_throwable_item::get_stock()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the animation of the throwable_item.
 */
std::string ptb::hazelnut_throwable_item::get_animation() const
{
  return "animation/owl/hazelnut.canim";
} // hazelnut_throwable_item::get_animation()

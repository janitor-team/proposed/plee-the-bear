/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file honeypot_throwable_item.cpp
 * \brief Implementation of the ptb::honeypot_throwable_item class.
 * \author Sebastien Angibaud
 */
#include "ptb/throwable_item/air_honeypot_throwable_item.hpp"

#include "ptb/game_variables.hpp"
#include "ptb/item/small_honeypot.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 * \param p The pointer on the player.
 */
ptb::air_honeypot_throwable_item::air_honeypot_throwable_item
( const player_proxy& p )
  : throwable_item("air_power_honeypot"), m_player(p)
{

} // air_honeypot_throwable_item::air_honeypot_throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the throwable_item.
 */
bear::engine::base_item*
ptb::air_honeypot_throwable_item::create_throwable_item() const
{
  small_honeypot* new_small_honeypot = new small_honeypot();
  new_small_honeypot->set_type(base_bonus::air_power);

  return new_small_honeypot;
} // air_honeypot_throwable_item::create_throwable_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if Plee can throw this throwable_item.
 */
bool ptb::air_honeypot_throwable_item::can_throw() const
{
  return (get_stock() > 0) && m_player.can_throw_power(monster::air_attack);
} // air_honeypot_throwable_item::can_throw()

/*----------------------------------------------------------------------------*/
/**
 * \brief Decrease the stock.
 */
void ptb::air_honeypot_throwable_item::decrease_stock() const
{
  game_variables::set_air_power(m_player.get_index(), false);
} // air_honeypot_throwable_item::decrease_stock()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the stock.
 */
unsigned int ptb::air_honeypot_throwable_item::get_stock() const
{
  if ( game_variables::get_air_power(m_player.get_index()) )
    return 1;
  else
    return 0;
} // air_honeypot_throwable_item::get_stock()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the animation of the throwable_item.
 */
std::string ptb::air_honeypot_throwable_item::get_animation() const
{
  return "animation/powerup/small_air.canim";
} // air_honeypot_throwable_item::get_animation()

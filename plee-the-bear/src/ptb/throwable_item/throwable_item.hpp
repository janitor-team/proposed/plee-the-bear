/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file throwable_item.hpp
 * \brief A item that Plee can throw.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_THROWABLE_ITEM_HPP__
#define __PTB_THROWABLE_ITEM_HPP__

#include "engine/base_item.hpp"

namespace ptb
{
  /**
   * \brief An item that player can throw.
   *
   * \author Sebastien Angibaud
   */
  class throwable_item
  {
  public:
    explicit throwable_item
    ( const std::string& name, bool always_visible = false );
    virtual ~throwable_item();

    virtual bool can_throw() const;
    bool is_empty() const;
    const std::string& get_name() const;

    virtual bear::engine::base_item* create_throwable_item() const = 0;
    virtual void decrease_stock() const = 0;
    virtual unsigned int get_stock() const = 0;
    virtual std::string get_animation() const = 0;

  private:
    /** \brief The name of the throwable_item. */
    std::string m_name;

    /** \brief Indicates if the throwable_item appears with no stock. */
    bool m_always_visible;

  }; // class throwable_item
} // namespace ptb

#endif // __PTB_THROWABLE_ITEM_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file corrupting_item_throwable_item.hpp
 * \brief Information about corrupting items that Plee can throw.
 * \author Julien Jorge
 */
#ifndef __PTB_CORRUPTING_ITEM_THROWABLE_ITEM_HPP__
#define __PTB_CORRUPTING_ITEM_THROWABLE_ITEM_HPP__

#include "ptb/item/corrupting_item.hpp"
#include "ptb/throwable_item/throwable_item.hpp"

namespace ptb
{
  /**
   * \brief Informations about corrupting_items that Plee can throw.
   * \author Julien Jorge
   */
  class corrupting_item_throwable_item:
    public throwable_item
  {
  public:
    typedef throwable_item super;

  public:
    corrupting_item_throwable_item
    ( const std::string& anim, const corrupting_item& ref, double ratio );

    bear::engine::base_item* create_throwable_item() const;
    void decrease_stock() const;
    unsigned int get_stock() const;
    bool can_throw() const;
    std::string get_animation() const;

  private:
    /** \brief The name of the animation of the throwable. */
    const std::string m_animation;

    /** \brief The item to throw. */
    const corrupting_item m_reference;

    /** \brief The amount of corrupting bonus used per each unit of the amount
        of ref. */
    const double m_ratio;

  }; // class corrupting_item_throwable_item
} // namespace ptb

#endif // __PTB_CORRUPTING_ITEM_THROWABLE_ITEM_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file hazelnut_throwable_item.hpp
 * \brief Information about hazelnuts that Plee can throw.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_AZELNUT_THROWABLE_ITEM_HPP__
#define __PTB_AZELNUT_THROWABLE_ITEM_HPP__

#include "ptb/player_proxy.hpp"

#include "ptb/throwable_item/throwable_item.hpp"

namespace ptb
{
  /**
   * \brief Informations about hazelnut that Plee can throw.
   *
   * \author Sebastien Angibaud
   */
  class hazelnut_throwable_item
    : public throwable_item
  {
  public:
    typedef throwable_item super;

  public:
    /** \brief The name of hazelnut_throwable_item. */
    static const std::string s_name;

  public:
    hazelnut_throwable_item(const player_proxy& p);

    bear::engine::base_item* create_throwable_item() const;
    void decrease_stock() const;
    unsigned int get_stock() const;
    std::string get_animation() const;

  private:
    /** \brief The pointer on the player. */
    const player_proxy m_player;

  }; // class hazelnut_throwable_item
} // namespace ptb

#endif // __PTB_AZELNUT_THROWABLE_ITEM_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file balloon.hpp
 * \brief A balloon in which we display what a speaker_item says.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_BALLOON_HPP__
#define __PTB_BALLOON_HPP__

#include "engine/level_globals.hpp"
#include "gui/static_text.hpp"
#include "universe/types.hpp"

#include <string>
#include <list>
#include <vector>

namespace ptb
{
  /**
   * \brief A balloon in which we display what a speaker_item says.
   * \author Sebastien Angibaud
   */
  class balloon
  {
  public:
    balloon();

    void progress( bear::universe::time_type elapsed_time );
    void render( std::list<bear::visual::scene_element>& e );
    void build(bear::engine::level_globals& glob);

    bool is_finished() const;
    bear::visual::size_box_type get_size() const;
    bear::visual::size_box_type get_final_size() const;
    void set_position
    ( const bear::visual::position_type& pos, bool on_top, bool on_right );
    bear::visual::position_type get_position() const;

    bool is_on_top() const;
    bool is_on_right() const;

    void set_speeches( const std::list<std::string>& speeches );
    void close();

  private:
    void render_bottom_left_corner
    ( std::list<bear::visual::scene_element>& e,
      bear::visual::sprite s ) const;
    void render_bottom_right_corner
    ( std::list<bear::visual::scene_element>& e,
      bear::visual::sprite s ) const;
    void render_top_left_corner
    ( std::list<bear::visual::scene_element>& e,
      bear::visual::sprite s ) const;
    void render_top_right_corner
    ( std::list<bear::visual::scene_element>& e,
      bear::visual::sprite s ) const;

    void increase();
    void decrease();
    void set_content_size( const bear::visual::size_box_type& s );

    void write_text();

  private:
    /** \brief The speech. */
    std::list<std::string> m_speeches;

    /** \brief The spike, going out of the mouth of the speaker. */
    bear::visual::sprite m_spike;

    /** \brief The corner of the balloon. */
    bear::visual::sprite m_corner;

    /** \brief The horizontal border of the balloon. */
    bear::visual::sprite m_horizontal_border;

    /** \brief The vertical border of the balloon. */
    bear::visual::sprite m_vertical_border;

    /** \brief The size of the frame. */
    bear::gui::size_box_type m_size_frame;

    /** \brief Indicates if the balloon is at its maximum size. */
    bool m_has_started;

    /** \brief The component in which we show the text. */
    bear::gui::static_text m_text;

    /** \brief Elapsed time since the creation. */
    bear::universe::time_type m_time;

    /** \brief Indicates how long the text is visible. */
    bear::universe::time_type m_play_time;

    /** \brief Indicates if the balloon is on the top of the item. */
    bool m_on_top;

    /** \brief Indicates if the balloon is on the right of the item. */
    bool m_on_right;

    /** \brief Indicates if the balloon is currently showing something, or about
        to show something. */
    bool m_active;

  }; // class balloon
} // namespace ptb

#endif // __PTB_BALLOON_HPP__

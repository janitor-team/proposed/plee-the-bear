/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file level_variables.hpp
 * \brief The class that helps accessing to the game variables.
 * \author Julien Jorge
 */
#ifndef __PTB_LEVEL_VARIABLES_HPP__
#define __PTB_LEVEL_VARIABLES_HPP__

#include "engine/level.hpp"

namespace ptb
{
  /**
   * \brief The class that helps accessing to the game variables.
   * \author Julien Jorge
   */
  class level_variables
  {
  public:
    static unsigned int get_players_on_exit( const bear::engine::level& lvl );
    static void set_players_on_exit
    ( bear::engine::level& lvl, unsigned int c );

    static unsigned int get_object_count
    ( const bear::engine::level& lvl, const std::string& object_type );
    static void set_object_count
    ( bear::engine::level& lvl, const std::string& object_type,
      unsigned int nb );

    static unsigned int get_killed_object_count
    ( const bear::engine::level& lvl, const std::string& object_type );
    static void set_killed_object_count
    ( bear::engine::level& lvl, const std::string& object_type,
      unsigned int nb );

    static bool get_current_hazelnut( const bear::engine::level& lvl);
    static void set_current_hazelnut( bear::engine::level& lvl, bool v );

    static bool get_hazelnut_found( const bear::engine::level& lvl);
    static void set_hazelnut_found( bear::engine::level& lvl, bool v );

    static bool get_secret_level_found( const bear::engine::level& lvl);
    static void set_secret_level_found( bear::engine::level& lvl, bool v );

    static unsigned int get_honeypots_found( const bear::engine::level& lvl );
    static void set_honeypots_found
    ( bear::engine::level& lvl, unsigned int nb );
    static bool get_honeypot_found
    ( const bear::engine::level& lvl, unsigned int id );
    static void set_honeypot_found
    ( bear::engine::level& lvl, unsigned int id, bool b );

    static bool is_exiting( const bear::engine::level& lvl );
    static void set_exiting( bear::engine::level& lvl );

    static bool owl_is_met( const bear::engine::level& lvl );
    static void meet_the_owl( bear::engine::level& lvl );

    static void all_hideouts_found( bear::engine::level& lvl );

    static bool get_friendly_fire(bear::engine::level& lvl);
    static void set_friendly_fire( bear::engine::level& lvl, bool v );

  }; // class level_variables
} // namespace ptb

#endif // __PTB_LEVEL_VARIABLES_HPP__

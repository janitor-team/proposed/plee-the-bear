/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file boss.hpp
 * \brief Base class for the bosses.
 * \author Julien Jorge
 */
#ifndef __PTB_BOSS_HPP__
#define __PTB_BOSS_HPP__

#include "ptb/monster_item.hpp"

#include "engine/item_brick/with_toggle.hpp"
#include "engine/model.hpp"
#include "engine/model/model_mark_item.hpp"
#include "engine/base_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief Base class for the bosses.
   *
   * The valid fields for this item are
   * - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class boss:
    public monster_item< bear::engine::model< bear::engine::base_item > >
  {
  public:
    /** \brief The type of the parent class. */
    typedef monster_item<
    bear::engine::model < bear::engine::base_item > > super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  private:
    /** \brief The type of the functions used to do the progression of the
        boss. */
    typedef void
    (boss::*progress_function_type)(bear::universe::time_type);

    /** \brief The type of the handle on the toggle activated when the boss is
        dead. */
    typedef
    bear::universe::derived_item_handle<bear::engine::with_toggle>
    toggle_handle;

  public:
    boss();

    void pre_cache();
    void progress( bear::universe::time_type elapsed_time );
    void build();

    bool set_item_field
    ( const std::string& name, bear::engine::base_item* value );

    void corrupt( double amount );

  protected:
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void inform_dead();

    void create_floating_score
    ( const std::string& p1_mark, const std::string& p2_mark ) const;
    void create_floating_score
    ( unsigned int index, const bear::universe::position_type& pos ) const;

    virtual std::string get_bonus_picture_filename() const = 0;
    virtual std::string get_bonus_picture_name() const = 0;

  private:
    void progress_energy( bear::universe::time_type elapsed_time );

    void godify( bear::universe::time_type d, bear::visual::size_type size );

    void show_energy( bear::universe::time_type d );

    static void init_exported_methods();

  private:
    /** \brief Current progress function. */
    progress_function_type m_progress;

    /** \brief The duration of the current action. */
    bear::universe::time_type m_action_duration;

    /** \brief The remaining duration of the god effect. */
    bear::universe::time_type m_godify_duration;

    /** \brief The increment of the energy, in units per seconds, at the
        beginning of the fight. */
    double m_energy_increment;

    /** \brief The item toggled when the boss is dead. */
    toggle_handle m_toggle;

    /** \brief The identifier of the music of the boss. */
    std::size_t m_music_id;

  }; // class boss
} // namespace ptb

#endif // __PTB_BOSS_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file base_enemy.hpp
 * \brief The class describing an enemy.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_BASE_ENEMY_HPP__
#define __PTB_BASE_ENEMY_HPP__

#include "ptb/monster_item.hpp"

namespace ptb
{
  /**
   * \brief The class describing an enemy. This class automatically inherit from
   *        ptb::monster_item.
   *
   * \b Template \b arguments :
   *  - \a score: the score given when the enemy dies.
   *  - \a Base the base class of the enemy.
   *
   * \author Sébastien Angibaud
   */
  template<class Base>
  class base_enemy:
    public monster_item<Base>
  {
  public:
    /** \brief The type of the parent class. */
    typedef monster_item<Base> super;

  public:
    base_enemy();

    void build();
    virtual bool set_u_integer_field
    ( const std::string& name, unsigned int value );
    virtual void get_visual
    ( std::list<bear::engine::scene_visual>& visuals ) const;

    void create_headstone
    ( const bear::universe::position_type& bottom_middle_pos,
      const bear::visual::animation& soul_anim,
      unsigned int soul_energy, unsigned int pos_z);

    void create_floating_score( unsigned int index ) const;

    void die(const monster& attacker);

  private:
    /** \brief The score to give when this enemy dies. */
    unsigned int m_score;

  }; // class base_enemy
} // namespace ptb

#include "ptb/impl/base_enemy.tpp"

#endif // __PTB_BASE_ENEMY_HPP__

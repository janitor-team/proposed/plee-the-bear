/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file controller_config.hpp
 * \brief The class that helps storing the input configuration.
 * \author Julien Jorge
 */
#ifndef __PTB_CONTROLLER_CONFIG_HPP__
#define __PTB_CONTROLLER_CONFIG_HPP__

#include "ptb/controller_layout.hpp"

#include <string>

namespace ptb
{
  /**
   * \brief The class that helps storing the input configuration.
   * \author Julien Jorge
   */
  class controller_config
  {
  public:
    controller_config();

    void load();
    void save() const;

    void save_controller_layout( unsigned int i ) const;
    void load_controller_layout( unsigned int i );

    void set_layout( unsigned int i, const controller_layout& lay );
    controller_layout& get_layout( unsigned int i );
    const controller_layout& get_layout( unsigned int i ) const;

  private:
    void default_controls_for_player_1();
    void default_controls_for_player_2();

  private:
    /** \brief The base name of the controller files. */
    const std::string m_base_layout_file_name;

    /** \brief The controller layout for the players. */
    static controller_layout s_controller_layout[2];

  }; // class controller_config
} // namespace ptb

#endif // __PTB_CONTROLLER_CONFIG_HPP__

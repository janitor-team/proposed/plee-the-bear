/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file level_information.hpp
 * \brief The class containing informations on a level.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_LEVEL_INFORMATION_HPP__
#define __PTB_LEVEL_INFORMATION_HPP__

#include <string>
#include <list>

namespace ptb
{
  /**
   * \brief The class containing informations on a level.
   * \author Sebastien Angibaud
   */
  class level_information
  {
  public:
    typedef std::list<std::string> bonus_list;

  public:
    level_information();
    explicit level_information
    (const std::string& filename, const std::string& name);

    bool is_valid() const;

    const std::string& get_id() const;
    const std::string& get_name() const;
    const std::string& get_filename() const;
    const bonus_list& get_bonus() const;

    void set_thumb_filename(const std::string& f);
    const std::string& get_thumb_filename() const;
    std::string get_medal_name() const;

    void load_bonus_list();

  private:
    /** \brief The identifier of the level. */
    std::string m_id;

    /** \brief The filename of the level. */
    std::string m_filename;

    /** \brief The filename of thumbnail. */
    std::string m_thumb_filename;

    /** \brief List of bonus to found. */
    bonus_list m_bonus;
   }; // class level_information
} // namespace ptb

#endif // __PTB_LEVEL_INFORMATION_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file vertical_progress_bar.hpp
 * \brief A vertical progress bar.
 * \author Julien Jorge
 */
#ifndef __PTB_VERTICAL_GAUGE_HPP__
#define __PTB_VERTICAL_GAUGE_HPP__

#include "engine/level_globals.hpp"
#include "universe/types.hpp"
#include "visual/scene_element.hpp"
#include "visual/sprite.hpp"
#include "gui/visual_component.hpp"

namespace ptb
{
  /**
   * \brief A vertical progress bar.
   * \author Julien Jorge
   */
  class vertical_gauge :
    public bear::gui::visual_component
  {
  public:
    typedef std::list<bear::visual::scene_element> scene_element_list;

  public:
    vertical_gauge
    ( bear::engine::level_globals& glob, unsigned int length,
      const std::string& level_sprite, const std::string& icon_sprite = "" );

    unsigned int width() const;
    unsigned int height() const;

    void set_level( double lev );
    void progress( bear::universe::time_type elapsed_time );
    void render
    ( scene_element_list& e, const bear::visual::position_type& pos ) const;
    void display( std::list<bear::visual::scene_element>& e ) const;

  private:
    /** \brief The icon displayed on the bar. */
    bear::visual::sprite m_icon;

    /** \brief The sprite displaying the level of the bar. */
    bear::visual::sprite m_level;

    /** \brief Sprite displayed over the ends of the bars. */
    bear::visual::sprite m_tube_clamp;

    /** \brief Sprite displayed over the bar. */
    bear::visual::sprite m_glass_tube;

    /** \brief The current level of the bar. */
    double m_level_value;

    /** \brief The current loss of the bar. */
    double m_loss_value;

  }; // class vertical_bar
} // namespace ptb

#endif // __PTB_VERTICAL_GAUGE_HPP__

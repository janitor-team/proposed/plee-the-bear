/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file horizontal_gauge.hpp
 * \brief A horizontal progress bar.
 * \author Julien Jorge
 */
#ifndef __PTB_HORIZONTAL_GAUGE_HPP__
#define __PTB_HORIZONTAL_GAUGE_HPP__

#include "engine/level_globals.hpp"
#include "gui/visual_component.hpp"
#include "universe/types.hpp"
#include "visual/scene_element.hpp"
#include "visual/sprite.hpp"

namespace ptb
{
  /**
   * \brief A horizontal progress bar.
   * \author Julien Jorge
   */
  class horizontal_gauge :
    public bear::gui::visual_component
  {
  public:
    typedef std::list<bear::visual::scene_element> scene_element_list;

  public:
    horizontal_gauge
    ( bear::engine::level_globals& glob, unsigned int length,
      const std::string& level_sprite, const std::string& loss_sprite = "",
      const std::string& icon_name = "", bool clamp_flashing = false );

    void set_length(unsigned int length);
    unsigned int length() const;
    unsigned int width() const;
    unsigned int height() const;

    void set_level_sprite
    ( bear::engine::level_globals& glob, const std::string& level_sprite );

    void set_level( double lev );
    double get_level() const;
    void set_max_level( double lev );

    void progress( bear::universe::time_type elapsed_time );
    void render
    ( scene_element_list& e, const bear::visual::position_type& pos ) const;
    void update_bar();

  private:
    void display( std::list<bear::visual::scene_element>& e ) const;

  private:
    /** \brief The icon displayed on the bar. */
    bear::visual::sprite m_icon;

    /** \brief The sprite displaying the level of the bar. */
    bear::visual::sprite m_level;

    /** \brief The sprite displaying the amount of loss. */
    bear::visual::sprite m_loss;

    /** \brief Sprite displayed over the ends of the bars. */
    bear::visual::sprite m_tube_clamp;

    /** \brief Sprite displayed over the bar. */
    bear::visual::sprite m_glass_tube;

    /** \brief The maximum value of the bar. */
    double m_max_value;

    /** \brief The current value of the bar. */
    double m_level_value;

    /** \brief The green intensity of clamp. */
    double m_clamp_green_intensity;

    /** \brief Indicates how the clamp intensity increases. */
    double m_clamp_increment_direction;

  }; // class horizontal_gauge
} // namespace ptb

#endif // __PTB_HORIZONTAL_GAUGE_HPP__

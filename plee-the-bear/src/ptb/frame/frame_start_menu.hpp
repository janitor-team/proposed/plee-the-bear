/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_start_menu.hpp
 * \brief This frame is the start menu (where we choose the mode of the game).
 * \author Julien Jorge
 */
#include "ptb/frame/menu_frame.hpp"
#include "ptb/playability_type.hpp"

namespace ptb
{
  /**
   * \brief This frame is the start menu (where we choose the mode of the game).
   * \author Julien Jorge
   */
  class frame_start_menu:
    public menu_frame
  {
  public:
    frame_start_menu
    ( windows_layer* owning_layer, bool use_loading,
      playability_type::value_type playability );

  private:
    void create_controls();

    void start_game( unsigned int p ) const;

  private:
    /** \brief Tell if we use the "loading" level. */
    bool m_use_loading;

    /** \brief The playability of the level : number of players. */
    playability_type::value_type m_playability;

  }; // class frame_start_menu
} // namespace ptb

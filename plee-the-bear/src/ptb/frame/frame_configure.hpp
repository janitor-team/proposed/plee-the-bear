/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_configure.hpp
 * \brief This frame is the configuration menu.
 * \author Julien Jorge
 */
#ifndef __PTB_FRAME_CONFIGURE_HPP__
#define __PTB_FRAME_CONFIGURE_HPP__

#include "ptb/frame/menu_frame.hpp"

namespace ptb
{
  /**
   * \brief This frame is the configuration menu.
   * \author Julien Jorge
   */
  class frame_configure:
    public menu_frame
  {
  public:
    frame_configure( windows_layer* in_layer );

  private:
    void create_controls();

    void on_first_player_controls();
    void on_second_player_controls();
    void on_game_options();
    void on_screen();
    void on_audio();
    void on_password();
    void on_back();

  }; // class frame_configure
} // namespace ptb

#endif // __PTB_FRAME_CONFIGURE_HPP__

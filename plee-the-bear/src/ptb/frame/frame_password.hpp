/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_password.hpp
 * \brief This frame is shown to enter the passwords.
 * \author Julien Jorge.
 */
#ifndef __PTB_FRAME_PASSWORD_HPP__
#define __PTB_FRAME_PASSWORD_HPP__

#include "ptb/frame/frame.hpp"
#include "gui/text_input.hpp"

namespace ptb
{
  /**
   * \brief This frame is shown to choose enter the passwords.
   * \author Julien Jorge.
   */
  class frame_password:
    public frame
  {
  public:
    frame_password( windows_layer* in_layer );

  private:
    void validate();

    void create_controls();
    bear::gui::visual_component* create_input();
    bear::gui::visual_component* create_back();

    void execute_command( const std::vector<std::string>& command ) const;

    void command_load_level( const std::vector<std::string>& command ) const;
    void command_unlock( const std::vector<std::string>& command ) const;
    void command_give
    ( const std::vector<std::string>& command, unsigned int index ) const;
    void command_game_variable( const std::vector<std::string>& command ) const;

    template<typename T>
    void command_game_variable( const std::string& var_value ) const;

  private:
    /** \brief The text control receiving the passwords. */
    bear::gui::text_input* m_password;

  }; // class frame_password
} // namespace ptb

#endif // __PTB_FRAME_PASSWORD_HPP__

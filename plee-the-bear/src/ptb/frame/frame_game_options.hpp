/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_game_options.hpp
 * \brief This frame is shown to choose the game_options parameters.
 * \author Julien Jorge
 */
#ifndef __PTB_FRAME_GAME_OPTIONS_HPP__
#define __PTB_FRAME_GAME_OPTIONS_HPP__

#include "ptb/frame/frame.hpp"

namespace ptb
{
  /**
   * \brief This frame is shown to configure miscellaneous game options.
   * \author Julien Jorge
   */
  class frame_game_options:
    public frame
  {
  public:
    frame_game_options( windows_layer* in_layer );

  private:
    void create_controls();

    bear::gui::visual_component* create_checkbox( bear::visual::font f );
    bear::gui::visual_component* create_ok_button( bear::visual::font f );
    bear::gui::visual_component* create_cancel_button( bear::visual::font f );

    void save();

    void on_ok();
    void on_cancel();
    void on_friendly_fire_check();
    void on_friendly_fire_uncheck();

  }; // class frame_game_options
} // namespace ptb

#endif // __PTB_FRAME_GAME_OPTIONS_HPP__

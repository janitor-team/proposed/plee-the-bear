/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame.hpp
 * \brief The base frame, with default borders and background.
 * \author Julien Jorge
 */
#ifndef __PTB_FRAME_HPP__
#define __PTB_FRAME_HPP__

#include "gui/frame.hpp"
#include "ptb/layer/windows_layer.hpp"

namespace ptb
{
  /**
   * \brief The base frame, with default borders and background.
   * \author Julien Jorge
   */
  class frame:
    public bear::gui::frame
  {
  private:
    /** \brief The type of the list of controls that can be activated with the
        cursor. */
    typedef std::vector<bear::gui::visual_component*> control_group;

  public:
    explicit frame( windows_layer* owning_layer );
    frame( windows_layer* owning_layer, const std::string& title );

    bear::visual::sprite get_arrow() const;
    bear::visual::sprite get_cursor() const;
    bear::visual::sprite get_checkbox_off() const;
    bear::visual::sprite get_checkbox_on() const;
    bear::visual::sprite get_radio_off() const;
    bear::visual::sprite get_radio_on() const;
    bear::visual::sprite get_slider_bar() const;
    bear::visual::sprite get_slider() const;
    bear::visual::font get_font() const;
    bear::visual::size_type get_margin() const;

    windows_layer& get_layer() const;

    static void set_borders_up( bear::gui::visual_component& c );
    static void set_borders_down( bear::gui::visual_component& c );

    virtual void on_focus();

  protected:
    void show_window( frame* wnd ) const;
    void replace_with( frame* wnd );
    void close_window() const;

    void insert_control( bear::gui::visual_component& c );

    bool process_key_press( const bear::input::key_info& key );
    bool process_button_press( bear::input::joystick::joy_code button,
                               unsigned int joy_index );
    bool process_mouse_move
    ( const claw::math::coordinate_2d<unsigned int>& pos );

    bool on_key_press( const bear::input::key_info& key );
    bool on_button_press( bear::input::joystick::joy_code button,
                          unsigned int joy_index );
    bool on_mouse_move( const claw::math::coordinate_2d<unsigned int>& pos );

  private:
    bear::visual::sprite get_frame_sprite( const std::string& name ) const;

    void common_init();

    void move_cursor_up();
    void move_cursor_down();
    void move_cursor_left();
    void move_cursor_right();

    template< typename CurrentValue1, typename OtherValue1,
              typename CurrentValue2, typename OtherValue2 >
    std::size_t find_nearest_control
    ( const CurrentValue1& current_value_1, const OtherValue1& other_value_1,
      const CurrentValue2& current_value_2,
      const OtherValue2& other_value_2,
      bool reversed = false) const;

    bool highlight_control_at
    ( const claw::math::coordinate_2d<unsigned int>& pos );

    void switch_to_control( std::size_t i );

    void on_focused();

  private:
    /** \brief The layer owning this window. */
    windows_layer* const m_owning_layer;

    /** \brief A group of controls that can be activated with the cursor. */
    control_group m_controls;

    /** \brief The control currently highlighted. */
    std::size_t m_current_control;
  }; // class frame
} // namespace ptb

#endif // __PTB_FRAME_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file message_box.hpp
 * \brief This frame displays a message.
 * \author Julien Jorge
 */
#ifndef __PTB_MESSAGE_BOX_HPP__
#define __PTB_MESSAGE_BOX_HPP__

#include "ptb/frame/frame.hpp"

namespace ptb
{
  /**
   * \brief This frame displays a message.
   * \author Julien Jorge
   */
  class message_box:
    public frame
  {
  public:
    typedef unsigned int flags;

  public:
    message_box( windows_layer* in_layer, const std::string& msg,
                 flags* buttons = NULL );

  private:
    void on_ok();
    void on_cancel();

    void create_controls( const std::string& msg );
    bear::gui::visual_component* create_text( const std::string& msg );
    bear::gui::visual_component* create_ok_button();
    bear::gui::visual_component* create_cancel_button();

  public:
    /** \brief Flag of the 'ok' button. */
    static const flags s_ok;

    /** \brief Flag of the 'cancel' button. */
    static const flags s_cancel;

  private:
    /** \brief The flags of the selected buttons. Serves also as result. */
    flags* m_flags;

  }; // class message_box
} // namespace ptb

#endif // __PTB_MESSAGE_BOX_HPP__

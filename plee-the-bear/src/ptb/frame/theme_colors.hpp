/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file theme_colors.hpp
 * \brief The colors used in the frames.
 * \author Julien Jorge
 */
#ifndef __PTB_THEME_COLORS_HPP__
#define __PTB_THEME_COLORS_HPP__

/** \brief The color of the selected items in the menus. */
#define PTB_THEME_SELECTION bear::visual::color_type(255, 255, 255, 64)

/** \brief The color of the bright sides of the controls. */
#define PTB_THEME_LIGHT bear::visual::color_type("bfaa90")

/** \brief The color of the shadows of the controls. */
#define PTB_THEME_DARK bear::visual::color_type("1a1614")

/** \brief The color of the faces of the controls. */
#define PTB_THEME_FACE bear::visual::color_type("736656")

/** \brief A transparent color. */
#define PTB_THEME_TRANSPARENT bear::visual::color_type(0, 0, 0, 0)

#endif // __PTB_THEME_COLORS_HPP__

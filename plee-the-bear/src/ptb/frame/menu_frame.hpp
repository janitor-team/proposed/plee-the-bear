/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file menu_frame.hpp
 * \brief Inherit from this class if you need a frame containing just a menu.
 * \author Julien Jorge
 */
#ifndef __PTB_MENU_FRAME_HPP__
#define __PTB_MENU_FRAME_HPP__

#include "ptb/frame/frame.hpp"
#include "gui/callback.hpp"

namespace ptb
{
  /**
   * \brief Inherit from this class if you need a frame containing just a menu.
   * \author Julien Jorge
   */
  class menu_frame:
    public frame
  {
  public:
    menu_frame( windows_layer* owning_layer, const std::string& title );

  protected:
    void push( const std::string& text, const bear::gui::callback& c );

  private:
    /** \brief The top entry of the menu. */
    bear::gui::visual_component* m_top;

  }; // class menu_frame
} // namespace ptb

#endif // __PTB_MENU_FRAME_HPP__

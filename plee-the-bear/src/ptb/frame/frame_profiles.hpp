/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_profiles.hpp
 * \brief This frame is shown to choose a profile.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_FRAME_PROFILES_HPP__
#define __PTB_FRAME_PROFILES_HPP__

#include "ptb/frame/frame.hpp"
#include "gui/radio_group.hpp"
#include "ptb/frame/message_box.hpp"

namespace ptb
{
  /**
   * \brief This frame is shown to choose a profile.
   * \author Sebastien Angibaud
   */
  class frame_profiles:
    public frame
  {
  public:
    frame_profiles( windows_layer* in_layer );
    void on_focus();

  private:
    void create_controls();
    bear::gui::radio_group*
    create_profiles_radio_buttons( bear::visual::font f );
    bear::gui::visual_component* create_ok_button( bear::visual::font f );
    bear::gui::visual_component* create_back_button( bear::visual::font f );
    bear::gui::visual_component* create_remove_button( bear::visual::font f );

    bool on_ok();
    bool on_back();
    bool on_remove();

    void update_controls();
    void select_current_profile();

  private:
    /** \brief Radio group to select a uniq profile. */
    bear::gui::radio_group* m_profiles;

    /** \brief Radio buttons to select a profile. */
    std::vector<bear::gui::radio_button*> m_profile_radio_buttons;

    /** \brief The resulting value of a displayed message box. */
    message_box::flags m_msg_result;
  }; // class frame_profiles
} // namespace ptb

#endif // __PTB_FRAME_PROFILES_HPP__

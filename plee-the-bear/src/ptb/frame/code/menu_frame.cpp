/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file menu_frame.cpp
 * \brief Implementation of the ptb::menu_frame class.
 * \author Julien Jorge
 */
#include "ptb/frame/menu_frame.hpp"

#include "gui/button.hpp"

#include <libintl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param owning_layer The layer onwing the window.
 * \param title The title of the frame.
 */
ptb::menu_frame::menu_frame
( windows_layer* owning_layer, const std::string& title )
  : frame(owning_layer, title), m_top(NULL)
{

} // menu_frame::menu_frame()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add an entry at the top of the other entries.
 * \param text The label of the entry.
 * \param c The callback executed when the entry is clicked.
 */
void
ptb::menu_frame::push( const std::string& text, const bear::gui::callback& c )
{
  bear::gui::button* b = new bear::gui::button( get_font(), text, c );
  b->set_margin(3);

  if ( m_top != NULL )
    b->set_bottom( m_top->top() );

  m_top = b;

  insert_control(*b);
  b->set_focus();
} // menu_frame::push()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_choose_player_mode.cpp
 * \brief Implementation of the ptb::frame_choose_player_mode class.
 * \author Angibaud Sebastien
 */
#include "ptb/frame/frame_choose_player_mode.hpp"
#include "ptb/frame/frame_play_story.hpp"
#include "ptb/game_variables.hpp"
#include "ptb/defines.hpp"

#include "engine/game.hpp"
#include "engine/variable/variable_list_reader.hpp"
#include "engine/variable/var_map.hpp"

#include "gui/callback_function.hpp"

#include <boost/bind.hpp>
#include <libintl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param owning_layer The layer onwing the window.
 */
ptb::frame_choose_player_mode::frame_choose_player_mode
( windows_layer* owning_layer )
  : menu_frame(owning_layer, gettext("Start game"))
{
  create_controls();
} // frame_choose_player_mode::frame_choose_player_mode()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the static text components.
 */
void ptb::frame_choose_player_mode::create_controls()
{
  push
    ( gettext("Back"),
      bear::gui::callback_function_maker
      ( boost::bind( &frame_choose_player_mode::close_window, this ) ) );

  push
    ( gettext("Two players (local)"),
      bear::gui::callback_function_maker
      ( boost::bind( &frame_choose_player_mode::start_game, this, 2 ) ) );

  push
    ( gettext("One player"),
      bear::gui::callback_function_maker
      ( boost::bind( &frame_choose_player_mode::start_game, this, 1 ) ) );

  fit();
} // frame_choose_player_mode::create_controls()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start the game.
 * \param p The number of players.
 */
void ptb::frame_choose_player_mode::start_game( unsigned int p ) const
{
  game_variables::set_players_count(p);

  std::string pattern(PTB_PERSISTENT_PREFIX);
  pattern += ".*";
  bear::engine::game::get_instance().erase_game_variables( pattern );

  load_game_variables();
  bear::engine::var_map vars;
  bear::engine::game::get_instance().get_game_variables
    ( vars, ".*continues_with" );

  if ( vars.begin<std::string>() != vars.end<std::string>() )
    show_window( new frame_play_story(&get_layer()) );
  else
    {
      game_variables::set_next_level_name( "level/intro.cl" );

      bear::engine::game::get_instance().set_waiting_level
        ( PTB_LOADING_LEVEL_NAME );
    }
} // frame_choose_player_mode::start_game()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load game variables.
 )*/
void ptb::frame_choose_player_mode::load_game_variables() const
{
  std::string filename(bear::engine::game::get_instance().get_custom_game_file
      (PTB_PROFILES_FOLDER) + game_variables::get_profile_name() + "/");
  if ( game_variables::get_players_count() == 1 )
    filename += PTB_SAVE_ONE_PLAYER_FILENAME;
  else
    filename += PTB_SAVE_TWO_PLAYERS_FILENAME;

  std::ifstream f( filename.c_str() );
  bear::engine::var_map vars;
  bear::engine::variable_list_reader reader;

  reader(f, vars);

  bear::engine::game::get_instance().set_game_variables(vars);
} // frame_choose_player_mode::load_game_variables()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_audio.hpp
 * \brief This frame is shown to choose the audio parameters.
 * \author Julien Jorge
 */
#ifndef __PTB_FRAME_AUDIO_HPP__
#define __PTB_FRAME_AUDIO_HPP__

#include "ptb/frame/frame.hpp"
#include "gui/slider.hpp"

namespace ptb
{
  /**
   * \brief This frame is shown to choose the audio parameters.
   * \author Julien Jorge
   */
  class frame_audio:
    public frame
  {
  public:
    typedef frame super;

  public:
    frame_audio( windows_layer* in_layer );

    void set_volume();

  private:
    void create_controls();
    bear::gui::visual_component* create_music_checkbox( bear::visual::font f );
    bear::gui::visual_component* create_sound_checkbox( bear::visual::font f );
    bear::gui::visual_component* create_music_slider();
    bear::gui::visual_component* create_sound_slider();
    bear::gui::visual_component* create_ok_button( bear::visual::font f );
    bear::gui::visual_component* create_cancel_button( bear::visual::font f );

    void save();
    void cancel() const;

    bool on_ok();
    bool on_cancel();
    bool on_close();

  private:
    /* \brief The sound 'mute' status when the frame was shown. */
    const bool m_saved_sound_muted;

    /* \brief The music 'mute' status when the frame was shown. */
    const bool m_saved_music_muted;

    /* \brief The sound volume when the frame was shown. */
    const bool m_saved_sound_volume;

    /* \brief The music volume when the frame was shown. */
    const double m_saved_music_volume;

    /** \brief Slider to control the volume of the sounds. */
    bear::gui::slider<double>* m_sound_volume;

    /** \brief Slider to control the volume of the music. */
    bear::gui::slider<double>* m_music_volume;

    /** \brief Indicates if ok button has been pressed. */
    bool m_ok_is_pressed;

  }; // class frame_audio
} // namespace ptb

#endif // __PTB_FRAME_AUDIO_HPP__

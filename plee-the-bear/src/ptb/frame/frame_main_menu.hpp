/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_main_menu.hpp
 * \brief This frame is the main menu of the game.
 * \author Julien Jorge
 */
#ifndef __PTB_FRAME_MAIN_MENU_HPP__
#define __PTB_FRAME_MAIN_MENU_HPP__

#include "ptb/frame/menu_frame.hpp"

namespace ptb
{
  /**
   * \brief This frame is the main menu of the game.
   * \author Julien Jorge
   */
  class frame_main_menu:
    public menu_frame
  {
  public:
    frame_main_menu( windows_layer* in_layer );

  private:
    bool on_key_press( const bear::input::key_info& key );
    bool on_button_press( bear::input::joystick::joy_code button,
                          unsigned int joy_index );

    void create_controls();

    bool on_close();
    void on_focus();
    void on_configuration();
    void on_game();
    void on_tutorial();
    void on_mini_game();
    void on_quit();

  private:
    /** \brief The index of the next key to read in the cheat code sequence. */
    unsigned int m_cheat_index;

    /** \brief The sequence to type on the keyboard to turn on the passwords. */
    static const bear::input::key_code s_keyboard_cheat[];

    /** \brief The sequence to type on the joystick to turn on the passwords. */
    static const bear::input::joystick::joy_code s_joystick_cheat[];

  }; // class frame_main_menu
} // namespace ptb

#endif // __PTB_FRAME_MAIN_MENU_HPP__

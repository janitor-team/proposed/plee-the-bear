/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_pause.hpp
 * \brief This frame is shown when the game is paused.
 * \author Julien Jorge
 */
#ifndef __PTB_FRAME_PAUSE_HPP__
#define __PTB_FRAME_PAUSE_HPP__

#include "ptb/frame/menu_frame.hpp"
#include "ptb/frame/message_box.hpp"

namespace ptb
{
  /**
   * \brief This frame is shown when the game is paused.
   * \author Julien Jorge
   */
  class frame_pause:
    public menu_frame
  {
  public:
    frame_pause( windows_layer* in_layer );

  private:
    bool on_close();
    void on_focus();

    void on_resume() const;
    void on_restart_level();
    void on_title_screen();
    void on_configuration();

    void create_controls();

  private:
    /** \brief The resulting value of a displayed message box. */
    message_box::flags m_msg_result;

  }; // class frame_pause
} // namespace ptb

#endif // __PTB_FRAME_PAUSE_HPP__

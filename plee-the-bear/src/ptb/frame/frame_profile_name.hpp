/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frame_profile_name.hpp
 * \brief This frame is shown to choose a profile name.
 * \author Sebastien Angibaud.
 */
#ifndef __PTB_FRAME_PROFILE_NAME_HPP__
#define __PTB_FRAME_PROFILE_NAME_HPP__

#include "ptb/frame/frame.hpp"
#include "gui/text_input.hpp"

namespace ptb
{
  /**
   * \brief This frame is shown to choose a profile name.
   * \author Sebastien Angibaud.
   */
  class frame_profile_name:
    public frame
  {
  public:
    frame_profile_name( windows_layer* in_layer );

  private:
    void validate();

    void create_controls();
    bear::gui::visual_component* create_input();
    bear::gui::visual_component* create_back();
    bear::gui::visual_component* create_create();

  private:
    /** \brief The text control receiving the profile name. */
    bear::gui::text_input* m_profile_name;

  }; // class frame_profile_name
} // namespace ptb

#endif // __PTB_FRAME_PROFILE_NAME_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file gui_command.hpp
 * \brief Codes for user's requests requiring something from the GUI.
 * \author Julien Jorge
 */
#ifndef __PTB_GUI_COMMAND_HPP__
#define __PTB_GUI_COMMAND_HPP__

#include <string>

namespace ptb
{
  /**
   * \brief Codes for user's requests requiring something from the GUI.
   * \author Julien Jorge
   */
  struct gui_command
  {
  public:
    /** \brief The type of the codes. */
    typedef unsigned int value_type;

  public:
    static std::string to_string( value_type a );
    static value_type from_string( const std::string& s );

  public:
    /** \brief Bad action code. */
    static const value_type null_command = 0;

    /** \brief Request to pause the game. */
    static const value_type pause = null_command + 1;

    /** \brief Minimal value of the valid actions. */
    static const value_type min_value = pause;

    /** \brief Make the player to say something. */
    static const value_type talk = pause + 1;

    /** \brief Minimal value of the valid actions. */
    static const value_type max_value = talk;

  }; // struct gui_command
} // namespace ptb

#endif // __PTB_GUI_COMMAND_HPP__

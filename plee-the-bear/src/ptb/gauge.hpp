/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file gauge.cpp
 * \brief Definition of the class describing a gauge.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_GAUGE_HPP__
#define __PTB_GAUGE_HPP__

namespace ptb
{
  class plee;

  /**
   * \brief The class describing a gauge.
   * \author Sebastien Angibaud
   */
  class gauge
  {
  public:
    gauge();
    gauge(double max_value);

    void set_value(double value);
    void set_activated(bool value);
    void add_value(double value);
    void remove_value(double value);

    void fill();

    double get_value() const;
    double get_max_value() const;
    bool is_activated() const;


  private:
    /* \brief The value of the gauge. */
    double m_value;

    /* \brief The maximum value. */
    double m_max_value;

    /* \brief Indicates if the gauges is activated. */
    bool m_activated;
  }; // class gauge
} // namespace ptb

#endif // __PTB_GAUGE_HPP__






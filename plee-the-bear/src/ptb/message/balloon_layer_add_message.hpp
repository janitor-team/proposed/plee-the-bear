/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file balloon_layer_add_message.hpp
 * \brief A message that adds a speaker in the layer.
 * \author Angibaud Sébastien
 */
#ifndef __PTB_BALLOON_LAYER_ADD_MESSAGE_HPP__
#define __PTB_BALLOON_LAYER_ADD_MESSAGE_HPP__

#include "communication/typed_message.hpp"

namespace ptb
{
  class balloon_layer;
  class speaker_item;

  /**
   * \brief A message that adds a speaker in the layer.
   * \author Sébastien Angibaud
   */
  class balloon_layer_add_message:
    public bear::communication::typed_message<balloon_layer>
  {
  public:
    balloon_layer_add_message();
    void set_speaker( speaker_item* speaker );
    bool apply_to( balloon_layer& that );

  private:
    /** \brief Pointer to the speaker. */
    speaker_item* m_speaker;

  }; // class balloon_layer_add_message
} // namespace ptb

#endif // __PTB_BALLOON_LAYER_ADD_MESSAGE_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file get_player_instance.hpp
 * \brief A message sent to get the instance of a player.
 * \author Julien Jorge
 */
#ifndef __PTB_GET_PLAYER_INSTANCE_HPP__
#define __PTB_GET_PLAYER_INSTANCE_HPP__

#include "communication/typed_message.hpp"

#include "ptb/player.hpp" // must be included, not predeclared.

namespace ptb
{
  /**
   * \brief A message sent to get the instance of a player.
   * \author Julien Jorge
   */
  class get_player_instance:
    public bear::communication::typed_message<player>
  {
  public:
    get_player_instance();

    bool apply_to( player& that );

    player* get_instance() const;

  private:
    /** \brief The instance of the player. */
    player* m_player;

  }; // class get_player_instance
} // namespace ptb

#endif // __PTB_GET_PLAYER_INSTANCE_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file status_layer_boss_message.hpp
 * \brief A message that adds the boss in the status layer.
 * \author Julien Jorge
 */
#ifndef __PTB_STATUS_LAYER_BOSS_MESSAGE_HPP__
#define __PTB_STATUS_LAYER_BOSS_MESSAGE_HPP__

#include "communication/typed_message.hpp"

namespace ptb
{
  class monster;
  class status_layer;

  /**
   * \brief A message that adds the boss in the status layer.
   * \author Sebastien Angibaud
   */
  class status_layer_boss_message:
    public bear::communication::typed_message<status_layer>
  {
  public:
    status_layer_boss_message();

    void set_boss( monster* b );
    bool apply_to( status_layer& that );

  private:
    /** \brief Pointer to the boss. */
    monster* m_boss;

  }; // class status_layer_boss_message
} // namespace ptb

#endif // __PTB_STATUS_LAYER_BOSS_MESSAGE_HPP__

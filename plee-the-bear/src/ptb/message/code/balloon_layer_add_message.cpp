/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file balloon_layer_add_message.cpp
 * \brief Implementation of the ptb::balloon_layer_add_message class.
 * \author Sebastien Angibaud
 */
#include "ptb/message/balloon_layer_add_message.hpp"

#include "ptb/layer/balloon_layer.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::balloon_layer_add_message::balloon_layer_add_message()
  : m_speaker(NULL)
{

} // balloon_layer_add_message::balloon_layer_add_message()

/*----------------------------------------------------------------------------*/
/*
 * \brief Set the speaker.
 * \param speaker The speaker.
 */
void ptb::balloon_layer_add_message::set_speaker( speaker_item* speaker)
{
  m_speaker = speaker;
} // balloon_layer_add_message::set_speaker()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the message to the balloon layer.
 * \param that The balloon layer to apply the message to.
 */
bool ptb::balloon_layer_add_message::apply_to( balloon_layer& that )
{
  if ( m_speaker == NULL )
    return false;

  that.add_speaker(m_speaker);
  return true;
} // balloon_layer_add_message::apply_to()

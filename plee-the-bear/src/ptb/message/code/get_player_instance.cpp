/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file get_player_instance.cpp
 * \brief Implementation of the ptb::get_player_instance class.
 * \author Julien Jorge
 */
#include "ptb/message/get_player_instance.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::get_player_instance::get_player_instance()
  : m_player(NULL)
{

} // get_player_instance::get_player_instance()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the message to a player.
 * \param that The player to which the message is applied.
 */
bool ptb::get_player_instance::apply_to( player& that )
{
  m_player = &that;
  return true;
} // get_player_instance::apply_to()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the instance of the player.
 */
ptb::player* ptb::get_player_instance::get_instance() const
{
  return m_player;
} // get_player_instance::get_instance()

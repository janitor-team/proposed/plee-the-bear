/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file status_layer_boss_message.cpp
 * \brief Implementation of the ptb::status_layer_boss_message class.
 * \author Sebastien Angibaud
 */
#include "ptb/message/status_layer_boss_message.hpp"

#include "ptb/layer/status_layer.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::status_layer_boss_message::status_layer_boss_message()
  : m_boss(NULL)
{

} // status_layer_boss_message::status_layer_boss_message()

/*----------------------------------------------------------------------------*/
/*
 * \brief Set the boss.
 * \param b The boss.
 */
void ptb::status_layer_boss_message::set_boss( monster* b )
{
  m_boss = b;
} // status_layer_boss_message::set_boss()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the message to the status layer.
 * \param that The status layer to apply the message to.
 */
bool ptb::status_layer_boss_message::apply_to( status_layer& that )
{
  that.set_boss(m_boss);
  return true;
} // status_layer_boss_message::apply_to()

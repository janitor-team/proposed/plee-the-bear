/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file speaker_item.hpp
 * \brief The class describing a speaker_item.hpp.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_SPEAKER_ITEM_HPP__
#define __PTB_SPEAKER_ITEM_HPP__

#include "ptb/balloon.hpp"

#include "engine/base_item.hpp"

#include <string>
#include <list>
#include <vector>

namespace ptb
{
  /**
   * \brief The class describing a speaker.
   * \author Sébastien Angibaud
   */
  class speaker_item
  {
  public:
    speaker_item();
    speaker_item( const speaker_item& that );
    virtual ~speaker_item();

    void build( bear::engine::level_globals& glob );
    void progress( bear::universe::time_type elapsed_time );

    bool has_finished_to_chat() const;
    bool has_more_things_to_say() const;

    void stop_to_speak();
    void speak( const std::vector<std::string>& speech );
    void speak( const std::string& s );

    balloon& get_balloon();

    void set_persistent_balloon( bool b );
    bool get_persistent_balloon() const;

  private:
    /** \brief The balloon. */
    balloon m_balloon;

    /** \brief The list of speeches. */
    std::list< std::list<std::string> > m_speeches;

    /** \brief Tell if the balloon must stay visible even if the item is off
        screen. */
    bool m_persistent_balloon;

  }; // class speaker_item
} // namespace ptb

#endif // __PTB_SPEAKER_ITEM_HPP__

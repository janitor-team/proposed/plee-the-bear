/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file action_file_recorder.hpp
 * \brief An item that records the actions sent to a player.
 * \author Julien Jorge
 */
#ifndef __PTB_ACTION_FILE_RECORDER_HPP__
#define __PTB_ACTION_FILE_RECORDER_HPP__

#include "ptb/item_brick/item_with_single_player_control_reader.hpp"
#include "engine/base_item.hpp"

#include "engine/export.hpp"

#include <map>
#include <fstream>

namespace ptb
{
  /**
   * \brief An item that records the actions sent to a player.
   *
   * This item saves the actions in a file that can be given to a
   * player_commander item to control a player.
   *
   * The valid fields for this item are
   *  - \a file_path: (string) [required] The path to the file in which the
   *    actions are saved,
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class action_file_recorder:
    public item_with_single_player_control_reader<bear::engine::base_item>
  {
    DECLARE_BASE_ITEM(action_file_recorder);

  private:
    /** \brief The type of the parent class. */
    typedef
    item_with_single_player_control_reader<bear::engine::base_item> super;

  private:
    /** \brief A class to store action informations. */
    class action_information
    {
    public:
      bool operator<( const action_information& other) const;

    public:
      /** \brief The date of the action. */
      bear::universe::time_type date;

      /** \brief The name of the action. */
      player_action::value_type action;

      /** \brief The duration of the action. */
      bear::universe::time_type duration;
    }; // class  action_information

    /** \brief All actions of a given player. */
    typedef std::vector< action_information > actions_vector;

    /** \brief The current actions of a given player. */
    typedef std::map
    < player_action::value_type,
      action_information > current_actions_map;


  public:
    action_file_recorder();
    action_file_recorder( const action_file_recorder& that );
    ~action_file_recorder();

    bool set_string_field( const std::string& name, const std::string& value );

    void build();
    void progress( bear::universe::time_type elapsed_time );

  private:
    void start_action( player_action::value_type a );

    void do_action
    ( bear::universe::time_type elapsed_time, player_action::value_type a );

    void stop_action( player_action::value_type a );

  private:
    /** \brief The file in which we save the actions. */
    std::ofstream m_file;

    /** \brief The current date. */
    bear::universe::time_type m_date;

    /** \brief The current actions of a given player. */
    current_actions_map m_current_actions;

    /** \brief All actions of a given player. */
    actions_vector m_actions;
  }; // class action_file_recorder
} // namespace ptb

#endif // __PTB_ACTION_FILE_RECORDER_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file stone_tracer.hpp
 * \brief The Objects of this class create a bear::path_trace for all stones
 *        that collided with them.
 * \author Julien Jorge
 */
#ifndef __PTB_STONE_TRACER_HPP__
#define __PTB_STONE_TRACER_HPP__

#include "generic_items/path_tracer.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The Objects of this class create a bear::path_trace for all stones
   *        that collided with them.
   *
   * \author Julien Jorge
   */
  class stone_tracer:
    public bear::path_tracer
  {
    DECLARE_BASE_ITEM(stone_tracer);

  public:
    /** \brief The type of the parent class. */
    typedef bear::path_tracer super;

  public:
    stone_tracer();
    stone_tracer( const stone_tracer& that );

  }; // class stone_tracer
} // namespace ptb

#endif // __PTB_STONE_TRACER_HPP__

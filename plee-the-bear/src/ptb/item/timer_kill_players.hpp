/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file timer_kill_players.hpp
 * \brief A timer object that kills the players when the time is over.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_TIMER_KILL_PLAYERS_HPP__
#define __PTB_TIMER_KILL_PLAYERS_HPP__

#include "generic_items/timer.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A timer object that kills the players when the time is over.
   * \author Sébastien Angibaud
   */
  class timer_kill_players:
    public bear::timer
  {
    DECLARE_BASE_ITEM(timer_kill_players);

  public:
    /** \brief The type of the parent class. */
    typedef bear::timer super;

  public:
    void build();

  private:
    void create_trigger();
    void create_toggles();

    bear::engine::base_item* create_kill_toggle( unsigned int i ) const;

  }; // class timer_kill_players
} // namespace bear

#endif // __PTB_TIMER_KILL_PLAYERS_HPP__

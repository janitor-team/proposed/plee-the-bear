/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file projectile_enemy.hpp
 * \brief The class describing a projectile enemy.
 * The valid fields for this item are
   *  - \a projectile_model: (string) \b the model of the projectile,
   *  - any field supported by the parent classes.
   *
 * \author Sébastien Angibaud
 */
#ifndef __PTB_PROJECTILE_ENEMY_HPP__
#define __PTB_PROJECTILE_ENEMY_HPP__

#include "ptb/monster_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a projectile enemy.
   * \author Sébastien Angibaud
   */
  class projectile_enemy:
    public monster_item
      < bear::engine::model< bear::engine::base_item > >
  {
    DECLARE_BASE_ITEM(projectile_enemy);

  public:
    /** \brief The type of the parent class. */
    typedef monster_item
    < bear::engine::model< bear::engine::base_item > > super;

  public:
    projectile_enemy();

    void build();

    bool set_string_field
    ( const std::string& name, const std::string& value );
    void set_projectile_model(const std::string& value);
    bool is_valid() const;

    void progress( bear::universe::time_type elapsed_time );
    void leaves_active_region();

  protected:
    virtual void has_attacked(const monster& other);

  private:
    /** \brief The model of the projectile. */
    std::string m_projectile_model;

    /** \brief Indicates if the sting is dead. */
    bool m_is_dead;
  }; // class projectile_enemy
} // namespace ptb

#endif // __PTB_PROJECTILE_ENEMY_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_settings.hpp
 * \brief An item to set each parameter of a player.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_PLAYER_SETTINGS_HPP__
#define __PTB_PLAYER_SETTINGS_HPP__

#include "engine/base_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item to set each parameter of a player.
   *
   * The valid fields for this item are
   *  - \a index:
   *  \c (unsigned integer) (required) The index of the player,
   *  - \a power.air:
   *  \c (bool) Indicates if the player has the air power,
   *  - \a power.fire:
   *  \c (unsigned integer) Indicates if the player has the fire power,
   *  - \a power.water:
   *  \c (unsigned integer) Indicates if the player has the water power,
   *  - \a stones:
   *  \c (unsigned integer) The number of stones,
   *  - \a lives:
   *  \c (unsigned integer) The number of lives,
   *  - \a score:
   *  \c (unsigned integer) The score of the player,
   *  - \a max_energy:
   *  \c (real) The size of the energy's gauge,
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class player_settings:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(player_settings);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    player_settings();

    void build();

    virtual bool set_u_integer_field
    ( const std::string& name, unsigned int value );
    virtual bool set_bool_field( const std::string& name, bool value );
    virtual bool set_real_field( const std::string& name, double value );

  private:
    /** \brief The index of the player. */
    unsigned int m_player_index;
  }; // class player_settings
} // namespace ptb

#endif // __PTB_PLAYER_SETTINGS_HPP__

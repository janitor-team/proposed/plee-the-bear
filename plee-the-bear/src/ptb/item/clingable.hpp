/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file clingable.hpp
 * \brief A clingable.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_CLINGABLE_HPP__
#define __PTB_CLINGABLE_HPP__

#include "engine/base_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A bonus box.
   *
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class clingable:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(clingable);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

    /** \brief The type of the list in which we store the items depending on the
        train. */
    typedef std::list<bear::universe::item_handle> item_list;

  public:
    clingable();

    void build();
    void progress( bear::universe::time_type elapsed_time );

  protected:
    void move( bear::universe::time_type elapsed_time );

  private:
    void collision_check_and_apply
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void get_dependent_items( std::list<physical_item*>& d ) const;

  private:
    /** \brief The list of items that are clung. */
    item_list m_list_items;

    /** \brief The list of the items that were clung. */
    item_list m_old_items;

    /** \brief The last position of the platform. */
    bear::universe::position_type m_last_position;
  }; // class clingable
} // namespace ptb

#endif // __PTB_CLINGABLE_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file spring.hpp
 * \brief A spring gives a force to the items colliding with him.
 * \author Julien Jorge
 */
#ifndef __PTB_SPRING_HPP__
#define __PTB_SPRING_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"
#include "engine/item_brick/item_with_activable_sides.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A spring gives a force to the items colliding with him.
   *
   * The valid fields for this item are
   *  - \a applied_force_x: (real) the force applied on the x-axis
   *    (default = 0),
   *  - \a applied_force_y: (real) the force applied on the y-axis
   *    (default = 0),
   *  - any field supported by the parent classes.
   *
   * When the spring is not active, the last frame of the animation is
   * displayed.
   *
   * \author Julien Jorge
   */
  class spring:
    public bear::engine::item_with_activable_sides
    < bear::engine::item_with_decoration
      < bear::engine::basic_renderable_item<bear::engine::base_item> >
    >
  {
    DECLARE_BASE_ITEM(spring);

  public:
    /** \brief The type of the parent class. */
    typedef
    bear::engine::item_with_activable_sides
    < bear::engine::item_with_decoration
      < bear::engine::basic_renderable_item<bear::engine::base_item> > > super;

  public:
    spring();

    bool set_real_field( const std::string& name, double value );

    void build();

  protected:
    void collision_check_and_bounce
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    /** \brief The force applied to the item. */
    bear::universe::force_type m_applied_force;

  }; // class spring
} // namespace ptb

#endif // __PTB_SPRING_HPP__

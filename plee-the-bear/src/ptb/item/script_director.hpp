/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file script_director.hpp
 * \brief An item that plays a script with black stripes.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_SCRIPT_DIRECTOR_HPP__
#define __PTB_SCRIPT_DIRECTOR_HPP__

#include "generic_items/script/script_director.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that plays a script with black stripes.
   * \author Sébastien
   *
   * The custom fields of this class are:
   * - any field supported by the parent class.
   */
  class script_director:
    public bear::script_director
  {
    DECLARE_BASE_ITEM(script_director);

  public:
    /** \brief The type of the parent class. */
    typedef bear::script_director super;

  private:
    void begin();
    void end();

  }; // class script_director
} // namespace ptb

#endif // __PTB_SCRIPT_DIRECTOR_HPP__

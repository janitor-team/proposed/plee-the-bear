/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file stone_target.hpp
 * \brief A target for stone in jet stone mini-game.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_STONE_TARGET_HPP__
#define __PTB_STONE_TARGET_HPP__

#include "ptb/monster_item.hpp"
#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A target for stone in jet stone mini-game.
   *
   * The valid fields for this item are
   *
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class stone_target:
    public monster_item
    < bear::engine::basic_renderable_item<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(stone_target);

  public:
    /** \brief The type of the parent class. */
    typedef
    monster_item
    < bear::engine::basic_renderable_item<bear::engine::base_item> > super;

  public:
    stone_target();

    void pre_cache();
    bool set_animation_field
    ( const std::string& name, const bear::visual::animation& value );
    void progress( bear::universe::time_type elapsed_time );
    void build();
    void get_visual( std::list<bear::engine::scene_visual>& visuals ) const;
    virtual void kill();

  private:
     void collision_check_and_apply
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    virtual void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void create_floating_score();
    unsigned int get_stone_target(bool hit) const;
    void create_decoration();

  protected:
    bool get_defensive_power
    ( unsigned int index, const monster& attacker,
        bear::universe::zone::position side ) const;

  private:
    /** \brief Indicates if the target has been hit. */
    bool m_hit;

    /** \brief The animation to display when the target is not hit. */
    bear::visual::animation m_starting_animation;

    /** \brief The animation to display when the target is hit. */
    bear::visual::animation m_hit_animation;

    /** \brief The current animation. */
    bear::visual::animation* m_current_animation;

    /** \brief Indicates if the target has been visible. */
    bool m_started;
  }; // class stone_target
} // namespace ptb

#endif // __PTB_STONE_TARGET_HPP__

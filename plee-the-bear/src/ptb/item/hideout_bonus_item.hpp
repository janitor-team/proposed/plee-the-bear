/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file hideout_bonus_item.hpp
 * \brief A hideout to discover in order to have hideout bonus.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_HIDEOUT_BONUS_ITEM_HPP__
#define __PTB_HIDEOUT_BONUS_ITEM_HPP__

#include "ptb/item_brick/item_waiting_for_players.hpp"
#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
 /**
   * \brief A hideout to discover in order to have hideout bonus.
   *
   * The custom fields of this class are:
   * - any field supported by the parent classes.
   *
   * \author Sébastien Angibaud
   */
  class hideout_bonus_item:
    public item_waiting_for_players<bear::engine::base_item>
  {
    DECLARE_BASE_ITEM(hideout_bonus_item);

  public:
    /** \brief The type of the parent class. */
    typedef item_waiting_for_players< bear::engine::base_item> super;

  public:
    hideout_bonus_item();
    void build();

  protected:
    void on_one_player( const player_proxy& p );
    void on_all_players( const player_proxy& p1, const player_proxy& p2 );

  private:
    void discover();
    void create_level_bonus();

  private:
    /** \brief The identifier of the hideout_bonus item. */
    std::string m_id;
  }; // class hideout_bonus_item
} // namespace ptb

#endif // __PTB_HIDEOUT_BONUS_ITEM_HPP__

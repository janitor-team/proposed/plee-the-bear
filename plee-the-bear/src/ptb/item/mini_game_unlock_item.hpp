/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file mini_game_unlock_item.hpp
 * \brief An item that unlocks a mini-game.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_MINI_GAME_UNLOCK_ITEM_HPP__
#define __PTB_MINI_GAME_UNLOCK_ITEM_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/export.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/with_text.hpp"
#include "engine/scene_visual.hpp"

namespace ptb
{
  /**
   * \brief A item that unlocks a mini-game.
   *
   * The valid fields for this item are
   *  - \a minigame_name: (string) the name of the minigame,
   *  - \a unlocked_sentence: (string) the sentence displayed when the minigame
   *    is unlocked,
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class mini_game_unlock_item:
    public bear::engine::basic_renderable_item
    < bear::engine::item_with_toggle<bear::engine::base_item> >,
    public bear::engine::with_text
  {
    DECLARE_BASE_ITEM(mini_game_unlock_item);

  public:
    typedef bear::engine::basic_renderable_item
    < bear::engine::item_with_toggle<bear::engine::base_item > > super;

  public:
    bool is_valid() const;
    bool set_string_field( const std::string& name, const std::string& value );

    void on_toggle_on(base_item* activator);
    void get_visual( std::list<bear::engine::scene_visual>& visuals ) const;

  private:
    void create_text_decoration();

  private:
    /** \brief The name of the mini-game. */
    std::string m_mini_game_name;

    /** \brief The sentence that is written when the mini-game is unlocked. */
    std::string m_unlocked_sentence;

  }; // class mini_game_unlock_item
} // namespace ptb

#endif // __PTB_MINI_GAME_UNLOCK_ITEM_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_killer_toggle.hpp
 * \brief This toggle kill a player when activated.
 * \author Julien Jorge
 */
#ifndef __PTB_PLAYER_KILLER_TOGGLE_HPP__
#define __PTB_PLAYER_KILLER_TOGGLE_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief This toggle kill a player when activated.
   *
   * The custom fields of this class are:
   * - player_index (unsigned integer): the index of the player to kill,
   * - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class player_killer_toggle:
    public bear::engine::item_with_toggle<bear::engine::base_item>
  {
    DECLARE_BASE_ITEM(player_killer_toggle);

  public:
    typedef bear::engine::item_with_toggle<bear::engine::base_item> super;

  public:
    player_killer_toggle( unsigned int player_index = 0 );

    bool set_u_integer_field( const std::string& name, unsigned int value );

  private:
    void on_toggle_on( bear::engine::base_item* activator );

  private:
    /** \brief The index of the player to kill. */
    unsigned int m_player_index;

  }; // class player_killer_toggle
} // namespace ptb

#endif // __PTB_PLAYER_KILLER_TOGGLE_HPP__

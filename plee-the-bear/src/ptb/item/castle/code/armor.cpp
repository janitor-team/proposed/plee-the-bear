/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file armor.cpp
 * \brief Implementation of the ptb::armor class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/castle/armor.hpp"
#include "ptb/item/castle/axe.hpp"
#include "ptb/util/player_util.hpp"

#include "engine/world.hpp"
#include "engine/export.hpp"

#include "generic_items/decorative_item.hpp"

#include <claw/assert.hpp>

BASE_ITEM_EXPORT( armor, ptb )

const unsigned int ptb::armor::s_soul_energy = 30;
const unsigned int ptb::armor::s_scan_distance = 350;

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::armor::armor()
  : m_progress(NULL), m_max_distance(200), m_has_axe(true), m_has_head(true)
{
  set_z_fixed(false);
  set_mass(100);
  set_density(2);
  set_stone_vulnerability(false);

  m_monster_type = monster::enemy_monster;
  m_energy = 15;
  m_offensive_phase = true;
  m_offensive_force = 15;
  m_offensive_coefficients[normal_attack] = 1;
  m_defensive_powers[air_attack] = true;
  m_defensive_powers[fire_attack] = true;
  m_defensive_powers[water_attack] = true;
get_rendering_attributes().mirror(false);
} // armor::armor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::armor::~armor()
{
} // armor::~armor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::armor::pre_cache()
{
  get_level_globals().load_image("gfx/castle/armor/armor-parts.png");
  get_level_globals().load_animation
    ("animation/forest/gorilla/gorilla_soul.canim");
  get_level_globals().load_model("model/castle/armor.cm");
  get_level_globals().load_model("model/headstone.cm");
  get_level_globals().load_model("model/castle/axe.cm");
} // armor::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
void ptb::armor::build()
{
  super::build();

  set_model_actor( get_level_globals().get_model("model/castle/armor.cm") );

  start_model_action("idle");
  m_progress = &armor::progress_idle;
} // armor::build()

/*---------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::armor::progress( bear::universe::time_type elapsed_time )
{
  super::progress( elapsed_time );

  if ( m_progress != NULL )
    (this->*m_progress)(elapsed_time);
} // armor::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::armor::set_real_field
( const std::string& name, double value )
{
  bool ok = true;

  if (name == "armor.max_distance")
    m_max_distance = value;
  else
    ok = super::set_real_field(name, value);

  return ok;
} // armor::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the item that he have no energy now.
 * \param attacker The attacker monster.
 */
void ptb::armor::inform_no_energy(const monster& attacker)
{
  m_is_injured = false;

  if ( get_current_action_name() != "dead" )
    {
      set_speed( bear::universe::speed_type(0, 0) );
      set_acceleration( bear::universe::force_type(0, 0) );
      set_internal_force(bear::universe::force_type(0, 0));
      set_external_force(bear::universe::force_type(0, 0));
      start_dead();
      die(attacker);
    }
} // armor::inform_no_energy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the item tat he left the active region.
 */
void ptb::armor::leaves_active_region()
{
  super::leaves_active_region();

  if ( get_current_action_name() == "dead" )
    kill();
} // armor::leaves_active_region()

/*----------------------------------------------------------------------------*/
/**
 * \brief The monster is injure.
 * \param attacker The monster attacking me.
 * \param side The side on which I am attacked.
 * \param duration The duration of injure state.
 */
void ptb::armor::injure
( const monster& attacker, bear::universe::zone::position side,
  double duration )
{
  super::injure(attacker, side, duration);
  
  if ( m_has_head ) 
    create_head(side == bear::universe::zone::middle_right_zone);
} // ptb::armor::injure()

/*----------------------------------------------------------------------------*/
/**
 * \brief Progress in the state idle.
 */
void ptb::armor::progress_idle( bear::universe::time_type elapsed_time )
{
  
} // armor::progress_idle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Progress in the state attack.
 */
void ptb::armor::progress_attack( bear::universe::time_type elapsed_time )
{
  
} // armor::progress_attack()

/*----------------------------------------------------------------------------*/
/**
 * \brief Progress in the state angry.
 */
void ptb::armor::progress_walk( bear::universe::time_type elapsed_time )
{
  if (  m_has_axe && scan() )
    {
      set_speed( bear::universe::speed_type(0, 0) );
      set_internal_force(bear::universe::force_type(0, 0));
      set_external_force(bear::universe::force_type(0, 0));
      start_model_action("attack");
    }
  else
    {
      if ( get_center_of_mass().distance(m_origin_position) > m_max_distance )
	get_rendering_attributes().mirror
	  (get_center_of_mass().x > m_origin_position.x);
      
      if ( get_rendering_attributes().is_mirrored() )
	add_internal_force( bear::universe::force_type(-40000, 0) );
      else
	add_internal_force( bear::universe::force_type(40000, 0) );
    }
} // armor::progress_walk()

/*----------------------------------------------------------------------------*/
/**
 * \brief Progress in the state dead.
 */
void ptb::armor::progress_dead( bear::universe::time_type elapsed_time )
{
  if (  has_bottom_contact() )
    {
      bear::visual::animation soul_anim
        ( get_level_globals().get_animation
          ("animation/forest/gorilla/gorilla_soul.canim") );
      create_headstone( get_bottom_middle(), soul_anim, s_soul_energy,
                        get_z_position() - 2 );
      kill();
    }
  else
    get_rendering_attributes().set_angle
      ( get_rendering_attributes().get_angle() - 0.1 );
} // armor::progress_dead()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start idle state.
 */
void ptb::armor::start_idle()
{
  m_progress = &armor::progress_idle;
  m_origin_position = get_center_of_mass();
} // armor::start_idle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start to attack.
 */
void ptb::armor::start_attack()
{
  m_progress = &armor::progress_attack;
} // armor::start_attack()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start dead state.
 */
void ptb::armor::start_dead()
{
  if ( get_current_action_name() != "dead" )
    {
      start_model_action("dead");
      m_progress = &armor::progress_dead;
    }
} // armor::start_dead()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start to walk.
 */
void ptb::armor::start_walk()
{
  m_progress = &armor::progress_walk;
} // armor::start_walk()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create and throw the axe.
 */
void ptb::armor::create_axe()
{
  if ( m_has_axe )
    {
      bear::engine::model_mark_placement m;
      
      if ( get_mark_placement("axe", m) )
	{
	  m_has_axe = false;
	  const bear::universe::position_type pos(m.get_position());
	  bear::universe::force_type force;
	  
	  force.x = 700000;
	  if ( get_rendering_attributes().is_mirrored() ) 
	    force.x *= -1;
	  force.y = 0;
	  
	  axe* new_axe = new axe();
	  
	  CLAW_ASSERT(new_axe->is_valid(),
		      "The axe isn't correctly initialized" );
	  
	  new_item( *new_axe );
	  
	  new_axe->add_external_force(force);
	  new_axe->set_center_of_mass(pos);
	  new_axe->set_z_position( m.get_depth_position() );
	  new_axe->get_rendering_attributes().set_angle(m.get_angle());
	  
	  set_global_substitute("axe", new bear::visual::animation() );
	}  
    }
} // armor::create_axe()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create and throw the head.
 * \param right_orientation Indicates if the head must be throw on the right.
 */
void ptb::armor::create_head(bool right_orientation)
{
  if ( m_has_head )
    {
      bear::engine::model_mark_placement m;
      
      if ( get_mark_placement("head", m) )
	{
	  bear::visual::animation anim
	    (get_level_globals().auto_sprite
	     ("gfx/castle/armor/armor-parts.png", "profile - head" ));
	  m_has_head = false;
	  const bear::universe::position_type pos(m.get_position());
	  bear::universe::force_type force;
	  
	  force.x = 20000;
	  if ( right_orientation ) 
	    force.x *= -1;
	  force.y = 10000;
	  
	  bear::decorative_item* new_head = new bear::decorative_item;

	  CLAW_ASSERT(new_head->is_valid(),
		      "The head isn't correctly initialized" );
	  
	  new_head->set_animation(anim);
	  new_item( *new_head );
	  
	  new_head->set_phantom(false);
	  new_head->set_artificial(false);
	  new_head->set_can_move_items(false);
	  new_head->set_kill_on_contact(false);
	  new_head->set_mass(1);
	  new_head->set_elasticity(0.8);
	  new_head->add_external_force(force);
	  new_head->set_center_of_mass(pos);
	  new_head->set_z_position( m.get_depth_position() );
	  new_head->get_rendering_attributes().set_angle(m.get_angle());
	  
	  set_global_substitute("head", new bear::visual::animation() );
	}  
    }
} // armor::create_head()

/*----------------------------------------------------------------------------*/
/**
 * \brief Scan if there is a player in armor's direction.
 */
bool ptb::armor::scan() const
{
  bool result = false;

  player_proxy p1 = util::find_player( get_level_globals(), 1 );
  player_proxy p2 = util::find_player( get_level_globals(), 2 );

  if( p1 != NULL )
    result =
      scan_for_player(p1, get_rendering_attributes().is_mirrored());

  if ( !result && (p2 != NULL) )
    result =
      scan_for_player(p2, get_rendering_attributes().is_mirrored());

  return result;
} // armor::scan()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if a player is visible for the armor.
 * \param p The player we are looking for.
 * \param left_orientation True if the orientation is toward the left.
 */
bool ptb::armor::scan_for_player
( const player_proxy& p, bool left_orientation) const
{
  bool result = false;
  const bool player_on_the_left
    ( p.get_horizontal_middle() <= get_horizontal_middle() );

  if ( !(player_on_the_left ^ left_orientation) )
    {
      bear::engine::model_mark_placement m;
      m.set_position( get_center_of_mass() );
      get_mark_placement("eyes", m);
      bear::universe::position_type pos(p.get_bottom_middle());
      pos.y += 1;

      result = scan_no_wall_in_direction
        ( m.get_position(), pos - m.get_position(), s_scan_distance );
    }

  return result;
} // armor::scan_in_direction()

/*----------------------------------------------------------------------------*/
/**
 * \brief Scan if there is a player in a given direction.
 * \param origin The position from which we start watching.
 * \param left_orientation True if the orientation is toward the left.
 * \param distance The distance of scan.
 */
bool ptb::armor::scan_no_wall_in_direction
( const bear::universe::position_type& origin,
  const bear::universe::vector_type& dir,
  bear::universe::coordinate_type distance ) const
{
  bear::universe::item_picking_filter filter;
  filter.set_can_move_items_value(true);

  if ( ( dir.length() <= distance ) && get_layer().has_world() )
    return
      get_layer().get_world().pick_item_in_direction
      (origin, dir, filter) == NULL;
  else
    return false;
} // armor::scan_no_wall_in_direction()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give a string representation of the item.
 * \param str (out) The result of the convertion.
 */
void ptb::armor::to_string( std::string& str ) const
{
  std::ostringstream oss;

  super::to_string(str);

  oss << "state: " << get_current_action_name() << "\n";
  oss << "origin_position: " << m_origin_position.x << " " <<
    m_origin_position.y << "\n";

  str += oss.str();
} // armor::to_string()

/*----------------------------------------------------------------------------*/
/**
 * \brief Export the methods of the class.
 */
void ptb::armor::init_exported_methods()
{
  TEXT_INTERFACE_CONNECT_METHOD_0( ptb::armor, start_attack, void );
  TEXT_INTERFACE_CONNECT_METHOD_0( ptb::armor, start_idle, void );
  TEXT_INTERFACE_CONNECT_METHOD_0( ptb::armor, start_walk, void );
  TEXT_INTERFACE_CONNECT_METHOD_0( ptb::armor, start_dead, void );
  TEXT_INTERFACE_CONNECT_METHOD_0( ptb::armor, create_axe, void );
} // armor::init_exported_methods()

/*----------------------------------------------------------------------------*/
TEXT_INTERFACE_IMPLEMENT_METHOD_LIST( ptb::armor )

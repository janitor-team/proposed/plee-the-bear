/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file armor.hpp
 * \brief The class describing an armor.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_ARMOR_HPP__
#define __PTB_ARMOR_HPP__

#include "ptb/base_enemy.hpp"
#include "ptb/monster.hpp"
#include "ptb/player_proxy.hpp"
#include "engine/model.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing an armor.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class armor:
    public base_enemy< bear::engine::model<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(armor);

  public:
    /** \brief The type of the parent class. */
    typedef base_enemy< bear::engine::model<bear::engine::base_item> > super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  private:
    typedef void (armor::*progress_function_type)(bear::universe::time_type);

  public:
    armor();
    ~armor();

    void pre_cache();
    void build();
    void progress( bear::universe::time_type elapsed_time );
    bool set_real_field( const std::string& name, double value );

  protected:
    void inform_no_energy(const monster& attacker);
    void leaves_active_region();
    void injure
    ( const monster& attacker, bear::universe::zone::position side,
      double duration);
    
  private:
    void progress_idle( bear::universe::time_type elapsed_time );
    void progress_attack( bear::universe::time_type elapsed_time );
    void progress_walk( bear::universe::time_type elapsed_time );
    void progress_dead( bear::universe::time_type elapsed_time );

    void start_idle();
    void start_attack();
    void start_walk();
    void start_dead();
    void create_axe();
    void create_head(bool right_orientation);

    bool scan() const;
    bool scan_for_player( const player_proxy& p, bool left_orientation) const;
    bool scan_no_wall_in_direction
    ( const bear::universe::position_type& origin,
      const bear::universe::vector_type& dir,
      bear::universe::coordinate_type distance ) const;

  protected:
    void to_string( std::string& str ) const;

  private:
    static void init_exported_methods();

  private:
    /** \brief The energy of the soul of an armor.*/
    static const unsigned int s_soul_energy;

    /** \brief The distance under which the armor scans.*/
    static const unsigned int s_scan_distance;

    /** \brief Current progress function. */
    progress_function_type m_progress;

    /** \brief The position of the origin. */
    bear::universe::position_type m_origin_position;

    /** \brief The maximum possible distance with origin position. */
    bear::universe::coordinate_type m_max_distance;

    /** \brief Indicates if armor has its axe. */
    bool m_has_axe;

    /** \brief Indicates if armor has its head. */
    bool m_has_head;
  }; // class armor
} // namespace ptb

#endif // __PTB_ARMOR_HPP__

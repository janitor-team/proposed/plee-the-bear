/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file two_players_only.hpp
 * \brief This class implements an item taht kills items according to the player
 *        mode.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_TWO_PLAYERS_ONLY_HPP__
#define __PTB_TWO_PLAYERS_ONLY_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief This class implements an item taht kills items according to the
   *        player mode.
   *
   * The custom fields of this class are:
   * - killing_item_with_one_player: \c (list of items) The items to kill when
   * there is only one player.
   * - killing_item_with_two_players: \c (list of items) The items to kill
   * when there is two players.
   *  - any field supported by the parent classes.
   * \author Sebastien Angibaud
   */
  class two_players_only:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(two_players_only);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    two_players_only();

    virtual bool set_item_list_field
      ( const std::string& name, const std::vector<base_item*>& value );

    void build();

  private:
    /** \brief The list of items to kill when there is only one player. */
    std::vector<bear::universe::item_handle> m_items_one_player;

    /** \brief The list of items to kill when there is two players. */
    std::vector<bear::universe::item_handle> m_items_two_players;
  }; // class two_players_only
} // namespace ptb

#endif // __PTB_TWO_PLAYERS_ONLY_HPP__

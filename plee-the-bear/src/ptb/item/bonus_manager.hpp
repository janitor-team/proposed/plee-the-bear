/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_manager.hpp
 * \brief A toggle that updates level bonus objects.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_BONUS_MANAGER_HPP__
#define __PTB_BONUS_MANAGER_HPP__

#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/base_item.hpp"
#include "universe/derived_item_handle.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A toggle that updates level bonus objects.
   *
   * The custom fields of this class are:
   * - any field supported by the parent class.
   *
   * \author Sebastien Angibaud
   */
  class bonus_manager:
    public bear::engine::item_with_toggle
    < bear::engine::base_item >
  {
    DECLARE_BASE_ITEM(bonus_manager);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::item_with_toggle
    < bear::engine::base_item > super;

  public:
    bonus_manager();
    void manage();

  private:
    void on_toggle_on(bear::engine::base_item *activator);
    void save_game_variables() const;
  }; // class bonus_manager
} // namespace ptb

#endif // __PTB_BONUS_MANAGER_HPP__

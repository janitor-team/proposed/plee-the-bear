/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file checkpoint.hpp
 * \brief Save the position of the player.
 * \author Julien Jorge
 */
#ifndef __PTB_CHECKPOINT_HPP__
#define __PTB_CHECKPOINT_HPP__

#include "engine/item_brick/basic_renderable_item.hpp"
#include "ptb/item/save_player_position.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief Tell the player to save its position. The position given to the
   *        player, as a reference position, is the center of mass of the
   *        checkpoint.
   *
   * This item has no field.
   *
   * \author Julien Jorge
   */
  class checkpoint:
    public bear::engine::basic_renderable_item<save_player_position>
  {
    DECLARE_BASE_ITEM(checkpoint);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::basic_renderable_item<save_player_position> super;

  public:
    checkpoint();

    void pre_cache();
    void build();

    void progress( bear::universe::time_type elapsed_time );
    void get_visual( std::list<bear::engine::scene_visual>& visuals ) const;

  private:
    void activate();
    void reactivate();

  private:
    /** \brief The animation of the checkpoint. */
    bear::visual::animation m_animation;

  }; // class checkpoint
} // namespace ptb

#endif // __PTB_CHECKPOINT_HPP__

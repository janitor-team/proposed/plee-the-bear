/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file power_drop_restriction_zone.hpp
 * \brief A zone in which Plee cannot drop a given power.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_POWER_DROP_RESTRICTION_ZONE_HPP__
#define __PTB_POWER_DROP_RESTRICTION_ZONE_HPP__

#include "engine/base_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief  A zone in which Plee cannot drop a given power.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *  - \a air: (bool) Indicates if the air power is restricted,
   *    (default = false),
   *  - \a fire: (bool) Indicates if the fire power is restricted,
   *    (default = false),
   *  - \a water: (bool) Indicates if the water power is restricted,
   *    (default = false),
   *
   * \author Sebastien Angibaud
   */
  class power_drop_restriction_zone:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(power_drop_restriction_zone);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    power_drop_restriction_zone();

    void set_powers_restriction(bool air, bool fire, bool water);
    bool set_bool_field( const std::string& name, bool value );

  private:
     void collision_check_and_apply
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    virtual void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    /** \brief Indicates if a player can drop the air power. */
    bool m_air;

    /** \brief Indicates if a player can drop the fire power. */
    bool m_fire;

    /** \brief Indicates if a player can drop the water power. */
    bool m_water;
  }; // class power_drop_restriction_zone
} // namespace ptb

#endif // __PTB_POWER_DROP_RESTRICTION_ZONE_HPP__

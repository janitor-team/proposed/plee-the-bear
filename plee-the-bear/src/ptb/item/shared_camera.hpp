/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file shared_camera.hpp
 * \brief A camera shared among the players.
 * \author Julien Jorge
 */
#ifndef __PTB_SHARED_CAMERA_HPP__
#define __PTB_SHARED_CAMERA_HPP__

#include "ptb/player_proxy.hpp"

#include "communication/typed_message.hpp"
#include "engine/messageable_item.hpp"
#include "generic_items/camera.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A camera shared among the players.
   * \author Julien Jorge
   */
  class shared_camera:
    public bear::engine::messageable_item<bear::camera>
  {
    DECLARE_BASE_ITEM(shared_camera);

  public:
    typedef bear::engine::messageable_item<bear::camera> super;

    /*----------------------------------------------------------------------*/
    /**
     * \brief The different placement modes for the camera.
     */
    enum placement_mode
      {
        /** \brief Centered on the first player. */
        lock_first_player,

        /** \brief Centered on the second player. */
        lock_second_player,

        /** \brief Centered between the two players. */
        shared,

        /** \brief Don't move. */
        do_nothing
      }; // enum mode

    /*----------------------------------------------------------------------*/
    /**
     * \brief A message that set the placement mode of the camera.
     * \author Julien Jorge
     */
    class set_placement_message:
      public bear::communication::typed_message<shared_camera>
    {
    public:
      set_placement_message( placement_mode mode );

      bool apply_to( shared_camera& that );

    private:
      /** \brief The mode to set. */
      const placement_mode m_mode;

    }; // class set_placement_message

  private:
    /** \brief The type of the progress function. */
    typedef
    void (shared_camera::*progress_type)( bear::universe::time_type );

    /*----------------------------------------------------------------------*/
  public:
    shared_camera();

    void build();
    void progress( bear::universe::time_type elapsed_time );

    void set_mode( placement_mode mode );

  private:
    void auto_position( bear::universe::time_type elapsed_time );

    void set_first_player( bear::universe::time_type elapsed_time );
    void set_second_player( bear::universe::time_type elapsed_time );
    void set_shared( bear::universe::time_type elapsed_time );
    void search_players();

    void progress_no_players( bear::universe::time_type elapsed_time );
    void progress_with_players( bear::universe::time_type elapsed_time );

  private:
    /** \brief Current placement mode. */
    placement_mode m_placement;

    /** \brief The first player. */
    player_proxy m_first_player;

    /** \brief The second player. */
    player_proxy m_second_player;

    /** \brief The current progress method. */
    progress_type m_progress;

  }; // class shared_camera
} // namespace ptb

#endif // __PTB_SHARED_CAMERA_HPP__

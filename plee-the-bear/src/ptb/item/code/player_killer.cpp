/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_killer.cpp
 * \brief Implementation of the bear::player_killer class.
 * \author Julien Jorge
 */
#include "ptb/item/player_killer.hpp"

#include "universe/collision_info.hpp"
#include "engine/export.hpp"
#include "ptb/player_proxy.hpp"
#include "ptb/player_action.hpp"

BASE_ITEM_EXPORT( player_killer, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Kill \a that if it is a player.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::player_killer::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  player_proxy p(&that);

  if ( p != NULL )
    p.start_action( player_action::die );
} // player_killer::collision()

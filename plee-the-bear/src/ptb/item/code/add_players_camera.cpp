/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file add_players_camera.cpp
 * \brief Implementation of the ptb::add_players_camera class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/add_players_camera.hpp"
#include "universe/derived_item_handle.hpp"

#include "ptb/player.hpp"
#include "ptb/util/player_util.hpp"

BASE_ITEM_EXPORT( add_players_camera, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::add_players_camera::add_players_camera()
  : m_first_player(false), m_second_player(false), m_camera(NULL)
{

} // add_players_camera::add_players_camera()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::add_players_camera::progress
( bear::universe::time_type elapsed_time )
{
  if ( m_camera )
    {
      player_proxy p = util::find_player( get_level_globals(), 1 );

      if ( p!=NULL )
        m_camera->add_item(p.get_player_instance());

      p = util::find_player( get_level_globals(), 2 );

      if ( p!=NULL )
        m_camera->add_item(p.get_player_instance());
    }

  kill();
} // add_players_camera::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::add_players_camera::set_bool_field
( const std::string& name, bool value )
{
  bool ok = true;

  if ( name == "add_players_camera.first_item" )
    m_first_player = value;
  else if ( name == "add_players_camera.second_item" )
       m_second_player = value;
  else
    ok = super::set_bool_field(name, value);

  return ok;
} // add_players_camera::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c base_item.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::add_players_camera::set_item_field
( const std::string& name, base_item* value )
{
  bool result = true;

  if ( name == "add_players_camera.camera" )
    m_camera = (bear::camera_on_object*)value;
  else
    result = super::set_item_field(name, value);

  return result;
} // ptb::add_players_camera::set_item_field()

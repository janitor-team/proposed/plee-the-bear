/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_box.cpp
 * \brief Implementation of the ptb::bonus_box class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/bonus_box.hpp"

#include "ptb/item/bonus_points/bonus_all_dead.hpp"
#include "ptb/level_variables.hpp"
#include "ptb/player_proxy.hpp"

#include "universe/collision_info.hpp"

#include "generic_items/decorative_item.hpp"
#include "generic_items/decorative_effect.hpp"

#include "engine/game.hpp"
#include "engine/world.hpp"
#include "engine/layer/layer.hpp"
#include "engine/export.hpp"

BASE_ITEM_EXPORT( bonus_box, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::bonus_box::pre_cache()
{
  super::pre_cache();

  // The little Plee
  get_level_globals().load_model("model/little_plee.cm");

  get_level_globals().load_animation("animation/powerup/fire.canim");
  get_level_globals().load_animation("animation/powerup/air.canim");
  get_level_globals().load_animation("animation/powerup/water.canim");
  get_level_globals().load_animation("animation/powerup/invincibility.canim");
  get_level_globals().load_animation("animation/powerup/stones_stock.canim");
  get_level_globals().load_animation
    ("animation/powerup/stones_big_stock.canim");
  get_level_globals().load_animation("animation/powerup/one_more_life.canim");
  get_level_globals().load_animation
    ("animation/powerup/increase_max_energy.canim");
  get_level_globals().load_animation
    ("animation/powerup/increase_max_energy-alt.canim");
  //get_level_globals().load_animation
  //  ("animation/powerup/switch_players.canim");

  get_level_globals().load_animation("animation/powerup/small_air.canim");
  get_level_globals().load_animation("animation/powerup/small_fire.canim");
  get_level_globals().load_animation("animation/powerup/small_water.canim");
  get_level_globals().load_animation( "animation/stones/stone.canim" );

  get_level_globals().load_image("gfx/bonus-box-2.png");
} // bonus_box::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::bonus_box::build()
{
  super::build();

  level_variables::set_object_count
        ( this->get_level(), "bonus_box",
          level_variables::get_object_count
          ( this->get_level(), "bonus_box" ) + 1);

  if ( level_variables::get_object_count
       ( this->get_level(), "bonus_box" ) == 1 )
    create_level_bonus();

  set_size( 50, 80 );
  set_gap_x(-5);
} // bonus_box::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the collision is with a player.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::bonus_box::collision_check_and_apply
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  player_proxy p(&that);

  if ( p != NULL )
    {
      if ( !get_bonus_given()
           && ( ( info.get_collision_side()
                  == bear::universe::zone::middle_left_zone )
                ||
                ( info.get_collision_side()
                  == bear::universe::zone::middle_right_zone ) )
           && p.is_in_offensive_phase()
           && can_give_bonus(p) )
        {
          give_bonus(p);
          create_broken_bonus_box();
          create_broken_glass(p.get_rendering_attributes().is_mirrored());
          create_honeypot_decoration();
        }
      else
        default_collision(info);
    }
  else
    default_collision(info);
} // bonus_box::collision_check_and_apply()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_apply().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::bonus_box::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  collision_check_and_apply(that, info);
} // bonus_box::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a decoration of a broken bonus_box.
 */
void ptb::bonus_box::create_broken_bonus_box()
{
   bear::decorative_item* item = new bear::decorative_item;

   item->set_sprite
     ( get_level_globals().auto_sprite
       ( "gfx/bonus-box-2.png", "broken box bottom" ) );
   item->set_z_position(get_z_position());
   item->set_mass(get_mass());
   item->set_density(get_density());
   item->set_phantom(false);
   item->set_artificial(false);
   new_item( *item );

   item->set_bottom_middle(get_bottom_middle());

   CLAW_ASSERT
     ( item->is_valid(),
       "The decoration of broken bonus_box isn't correctly initialized" );
} // bonus_box::create_broken_bonus_box()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a decoration of a broken glass.
 * \param left_orientation Indicates if the glass go on the left.
 */
void ptb::bonus_box::create_broken_glass(bool left_orientation)
{
  bear::decorative_item* item = new bear::decorative_item;

  item->set_sprite
     ( get_level_globals().auto_sprite
       ( "gfx/bonus-box-2.png", "broken box top" ) );
  item->get_rendering_attributes().set_angle(get_visual_angle());
  item->set_z_position(get_z_position());
  item->set_mass(5);
  item->set_density(2);
  item->set_phantom(false);
  item->set_artificial(false);
  item->set_size( 50, 48 );

  bear::universe::force_type force(93750, 18750);
  if ( left_orientation )
    force.x *= -1;

  double a = (1.0 * rand() / RAND_MAX)+0.5;
  item->add_internal_force(force*a);

  new_item( *item );

  item->set_top_middle(get_top_middle());

  CLAW_ASSERT( item->is_valid(),
         "The decoration of broken glass isn't correctly initialized" );
} // bonus_box::create_broken_bonus_box()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do the actions related to a change in the bonus type.
 * \param t The new type of the bonus.
 */
void ptb::bonus_box::do_set_type(base_bonus_type t)
{
  switch(t)
    {
    case fire_power:
      set_animation
        ( get_level_globals().get_animation("animation/powerup/fire.canim") );
      break;
    case air_power:
      set_animation
        ( get_level_globals().get_animation("animation/powerup/air.canim") );
      break;
    case water_power:
      set_animation
        ( get_level_globals().get_animation("animation/powerup/water.canim") );
      break;
    case invincibility_power:
      set_animation( get_level_globals().get_animation
                     ("animation/powerup/invincibility.canim") );
      break;
    case stones_stock:
      set_animation( get_level_globals().get_animation
                     ("animation/powerup/stones_stock.canim") );
      set_stock(20);
      break;
    case stones_big_stock:
      set_animation( get_level_globals().get_animation
                     ("animation/powerup/stones_big_stock.canim") );
      set_stock(50);
      break;
    case one_more_life:
      set_animation( get_level_globals().get_animation
                     ("animation/powerup/one_more_life.canim") );
      set_stock(1);
      break;
    case increase_max_energy:
      if ( get_player_index() == 2 )
        set_animation( get_level_globals().get_animation
                       ("animation/powerup/increase_max_energy-alt.canim") );
      else
        set_animation( get_level_globals().get_animation
                       ("animation/powerup/increase_max_energy.canim") );
      set_stock(20);
      break;
    case switch_players:
      set_animation( get_level_globals().get_animation
                     ("animation/powerup/switch_players.canim") );
    default:
      {
        CLAW_FAIL( "Not a valid power." );
      }
    }
} // bonus_box::do_set_type()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a decoration of honeypot.
 */
void ptb::bonus_box::create_honeypot_decoration()
{
  bear::decorative_item* item = new bear::decorative_item;

  bool deco = true;

  switch(get_type())
    {
    case fire_power:
      item->set_animation
        ( get_level_globals().get_animation
          ("animation/powerup/small_fire.canim") );
      break;
    case air_power:
      item->set_animation
        ( get_level_globals().get_animation
          ("animation/powerup/small_air.canim") );
      break;
    case water_power:
      item->set_animation
        ( get_level_globals().get_animation
          ("animation/powerup/small_water.canim") );
      break;
    case stones_stock:
    case stones_big_stock:
      item->set_animation
        (get_level_globals().get_animation
         ("animation/stones/stone.canim" ) );
      break;

    default:
      deco = false;
    }

  if ( deco )
    {
      item->set_z_position(get_z_position()+1);
      item->set_mass(1);
      item->set_density
        (0.8 * get_layer().get_world().get_average_density
         (get_bounding_box()));
      item->set_kill_when_leaving(true);

      new_item( *item );

      item->set_top_middle(get_top_middle());

      CLAW_ASSERT( item->is_valid(),
                   "In bonus_box class: the decoration of honeypot isn't "
                   "correctly initialized" );

      bear::decorative_effect* decoration_effect = new bear::decorative_effect;

      decoration_effect->set_duration(1.5);
      decoration_effect->set_opacity_factor_init(0.6);
      decoration_effect->set_opacity_factor_end(0);
      decoration_effect->set_item(item, true);

      new_item(*decoration_effect);
    }
  else
    delete item;
} // bonus_box::create_honeypot_decoration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create level bonus objects.
 */
void ptb::bonus_box::create_level_bonus()
{
  bonus_all_dead* new_bonus = new bonus_all_dead( "All bonus boxes", 5000 );

  new_bonus->set_picture_filename("gfx/ui/bonus-icons/bonus-icons.png");
  new_bonus->set_picture_name("bonus boxes");

  std::list<std::string> classes;
  classes.push_back( get_class_name() );
  new_bonus->set_class_names(classes);

  new_item( *new_bonus );
  CLAW_ASSERT(new_bonus->is_valid(),
              "The bonus isn't correctly initialized" );
  new_bonus->set_center_of_mass(get_center_of_mass());
} // bonus_box::create_level_bonus()

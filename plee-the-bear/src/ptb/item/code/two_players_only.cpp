/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file two_players_only.hpp
 * \brief Implementation of the ptb::two_players_only class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/two_players_only.hpp"

#include "ptb/game_variables.hpp"

BASE_ITEM_EXPORT( two_players_only, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::two_players_only::two_players_only()
{
  m_items_two_players.clear();
  m_items_one_player.clear();

  set_global(true);
} // two_players_only::two_players_only()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type list of <*base_item>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::two_players_only::set_item_list_field
( const std::string& name, const std::vector<base_item*>& value )
{
  bool result = false;

  if ( name == "two_players_only.items_killed_with_two_players" )
    {
      m_items_two_players.clear();
      for ( unsigned int index = 0; index != value.size(); ++index )
        m_items_two_players.push_back(value[index]);
      result = true;
    }
  else if ( name == "two_players_only.items_killed_with_one_player" )
    {
      m_items_one_player.clear();
      for ( unsigned int index = 0; index != value.size(); ++index )
        m_items_one_player.push_back(value[index]);
      result = true;
    }
  else
    result = super::set_item_list_field( name, value );

  return result;
} // two_players_only::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::two_players_only::build()
{
  std::vector<bear::universe::item_handle>::iterator it;

  if ( game_variables::get_players_count() == 1 )
    {
       for (it = m_items_one_player.begin();
           it != m_items_one_player.end(); ++it)
        if ( *it != NULL )
          {
            base_item*
              item = dynamic_cast<base_item*>(it->get());
            if ( item != NULL )
              item->kill();
          }
    }
  else
    {
      for (it = m_items_two_players.begin();
           it != m_items_two_players.end(); ++it)
        if ( *it != NULL )
          {
            base_item*
              item = dynamic_cast<base_item*>(it->get());
            if ( item != NULL )
              item->kill();
          }
    }

  kill();
} // two_players_only::build()

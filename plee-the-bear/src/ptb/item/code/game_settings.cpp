/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file game_settings.cpp
 * \brief Implementation of the ptb::game_settings class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/game_settings.hpp"
#include "ptb/game_variables.hpp"

BASE_ITEM_EXPORT( game_settings, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::game_settings::game_settings()
{
} // game_settings::game_settings()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::game_settings::build()
{
  super::build();

  kill();
} // game_settings::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c unsigned \c integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::game_settings::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result(true);

  if ( name == "game_settings.corrupting_bonus_count" )
    game_variables::set_corrupting_bonus_count(value);
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // game_settings::set_u_integer_field()

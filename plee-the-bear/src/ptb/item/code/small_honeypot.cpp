/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file small_honeypot.cpp
 * \brief Implementation of the ptb::small_honeypot class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/small_honeypot.hpp"

#include "universe/collision_info.hpp"
#include "generic_items/decorative_item.hpp"
#include "ptb/game_variables.hpp"
#include "ptb/player_proxy.hpp"
#include "engine/game.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( small_honeypot, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::small_honeypot::small_honeypot()
{
  set_mass(1);
  set_width(15);
  set_height(15);
  set_elasticity(0.2);
} // small_honeypot::small_honeypot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::small_honeypot::pre_cache()
{
  super::pre_cache();

  get_level_globals().load_animation("animation/powerup/small_air.canim");
  get_level_globals().load_animation("animation/powerup/small_fire.canim");
  get_level_globals().load_animation("animation/powerup/small_water.canim");
} // small_honeypot::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the collision is with a player.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::small_honeypot::collision_check_and_apply
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  player_proxy p(&that);

  if ( p != NULL )
    {
      if ( !get_bonus_given()
           && ( info.get_collision_side() != bear::universe::zone::middle_zone )
           && ( p.get_index() <= 2 )
           && ( ( get_type() != base_bonus::air_power )
                || !game_variables::get_air_power(p.get_index()) )
           && ( ( get_type() != base_bonus::fire_power )
                || !game_variables::get_fire_power(p.get_index()) )
           && ( ( get_type() != base_bonus::water_power )
                || !game_variables::get_water_power(p.get_index()) ) )
        give_bonus(p);
    }
} // small_honeypot::collision_check_and_apply()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_apply().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::small_honeypot::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  collision_check_and_apply(that, info);
} // small_honeypot::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do the actions related to a change in the bonus type.
 * \param t The new type of the bonus.
 */
void ptb::small_honeypot::do_set_type(base_bonus_type t)
{
  switch(t)
    {
    case fire_power:
      set_animation
        ( get_level_globals().get_animation
          ("animation/powerup/small_fire.canim") );
      break;
    case air_power:
      set_animation
        ( get_level_globals().get_animation
          ("animation/powerup/small_air.canim") );
      break;
    case water_power:
      set_animation
        ( get_level_globals().get_animation
          ("animation/powerup/small_water.canim") );
      break;
    default:
      {
        CLAW_FAIL( "Not a valid power for this class." );
      }
    }
} // small_honeypot::do_set_type()

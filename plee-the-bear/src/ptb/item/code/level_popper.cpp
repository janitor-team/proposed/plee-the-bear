/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item/code/level_popper.cpp
 * \brief Implementation of the ptb::level_popper class.
 * \author Julien Jorge
 */
#include "ptb/item/level_popper.hpp"

#include "ptb/game_variables.hpp"
#include "ptb/player_proxy.hpp"

#include "engine/game.hpp"

BASE_ITEM_EXPORT( level_popper, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::level_popper::level_popper()
  : m_players_count(0), m_applied(false)
{
  set_phantom(true);
  set_can_move_items(false);
} // level_popper::level_popper()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 */
void ptb::level_popper::progress
( bear::universe::time_type elapsed_time )
{
  if ( m_players_count == game_variables::get_players_count() )
    {
      m_applied = true;
      bear::engine::game::get_instance().pop_level();
    }

  m_players_count = 0;
} // level_popper::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the collision is with a player.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::level_popper::collision_check_and_apply
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  player_proxy p(&that);

  if ( p != NULL )
    ++m_players_count;
} // level_popper::collision_check_and_apply()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_apply().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::level_popper::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  if ( !m_applied )
    collision_check_and_apply(that, info);
} // level_popper::collision()

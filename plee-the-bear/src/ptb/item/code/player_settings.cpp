/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_settings.cpp
 * \brief Implementation of the ptb::player_settings class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/player_settings.hpp"
#include "ptb/game_variables.hpp"

BASE_ITEM_EXPORT( player_settings, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::player_settings::player_settings()
  : m_player_index(1)
{
} // player_settings::player_settings()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::player_settings::build()
{
  super::build();
  kill();
} // player_settings::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c unsigned \c integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::player_settings::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result(true);

  if ( name == "player_settings.player_index" )
    m_player_index = value;
  else if ( name == "player_settings.stones" )
    game_variables::set_stones_count(m_player_index, value);
  else if ( name == "player_settings.lives" )
    game_variables::set_lives_count(m_player_index, value);
  else if ( name == "player_settings.score" )
    game_variables::set_score(m_player_index, value);
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // player_settings::set_u_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
ptb::player_settings::set_bool_field( const std::string& name, bool value )
{
  bool result = true;

  if (name == "player_settings.power.air")
    game_variables::set_air_power(m_player_index, value);
  else if (name == "player_settings.power.fire")
    game_variables::set_fire_power(m_player_index, value);
  else if (name == "player_settings.power.water")
    game_variables::set_water_power(m_player_index, value);
  else
    result = super::set_bool_field(name, value);

  return result;
} // player_settings::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c real.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
ptb::player_settings::set_real_field( const std::string& name, double value )
{
  bool result = true;

  if (name == "player_settings.max_energy")
    game_variables::set_max_energy(m_player_index, value);
  else
    result = super::set_real_field(name, value);

  return result;
} // player_settings::set_real_field()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file passive_enemy.cpp
 * \brief Implementation of the ptb::passive_enemy class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/passive_enemy.hpp"
#include "engine/export.hpp"

BASE_ITEM_EXPORT( passive_enemy, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::passive_enemy::passive_enemy()
  : m_right_side_type(inactive_type), m_left_side_type(inactive_type),
    m_top_side_type(inactive_type), m_bottom_side_type(inactive_type)
{
  set_can_move_items(true);
  m_monster_type = monster::nature_monster;
  m_offensive_coefficients[indefensible_attack] = 1;
} // passive_enemy::passive_enemy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_align().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::passive_enemy::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  side_type t = inactive_type;
  double f;

  switch( info.get_collision_side() )
    {
    case bear::universe::zone::top_zone:
      t = m_top_side_type; f = get_top_friction(); break;
    case bear::universe::zone::bottom_zone:
      t = m_bottom_side_type; f = get_bottom_friction();break;
    case bear::universe::zone::middle_left_zone:
      t = m_left_side_type; f = get_left_friction();break;
    case bear::universe::zone::middle_right_zone:
      t = m_right_side_type; f = get_right_friction();break;
    default:
      { }
    }

  if ( t != inactive_type )
    if ( default_collision(info) )
      {
        that.set_contact_friction(f);
        that.set_system_angle(0);
        if ( t == attack_type )
          if ( collision_and_attack(that, info) )
            ;
      }
} // passive_enemy::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::passive_enemy::set_string_field
( const std::string& name, const std::string& value )
{
  bool result = true;

  if ( name == "passive_enemy.type_right_side" )
    result = set_side_type(m_right_side_type, value);
  else if ( name == "passive_enemy.type_left_side" )
    result = set_side_type(m_left_side_type, value);
  else if ( name == "passive_enemy.type_top_side" )
    result = set_side_type(m_top_side_type, value);
  else if ( name == "passive_enemy.type_bottom_side" )
    result = set_side_type(m_bottom_side_type, value);
  else
    result = super::set_string_field(name, value);

  return result;
} // passive_enemy::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the type of a given side.
 * \param side The considered side.
 * \param value The new type of the side.
 * \return false if the value is unknow, true otherwise.
 */
bool ptb::passive_enemy::set_side_type
(side_type& side, const std::string& value)
{
  bool result = true;

  if ( value == "inactive" )
    side = inactive_type;
  else if ( value == "solid" )
    side = solid_type;
  else if ( value == "attack" )
    side = attack_type;
  else
    result = false;

  return result;
} // passive_enemy::set_side()

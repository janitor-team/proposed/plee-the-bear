/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file script_director.cpp
 * \brief Implementation of the bear::script_director class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/script_director.hpp"

#include "ptb/defines.hpp"

#include "engine/transition_effect/strip_effect.hpp"
#include "engine/message/transition_effect_message.hpp"
#include "engine/level_globals.hpp"

BASE_ITEM_EXPORT( script_director, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform each actor that the script begins.
 */
void ptb::script_director::begin()
{
  bear::engine::transition_effect_message<bear::engine::strip_effect> msg;
  msg.get_effect().set_duration(0.25, get_script_duration(), 0.75);
  msg.get_effect().set_color( 0, 0, 0 );
  msg.get_effect().set_strip_height(60);
  get_level_globals().send_message
    ( PTB_TRANSITION_EFFECT_DEFAULT_TARGET_NAME, msg );
} // script_director::begin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform each actor that the script ends.
 */
void ptb::script_director::end()
{

} // script_director::end()

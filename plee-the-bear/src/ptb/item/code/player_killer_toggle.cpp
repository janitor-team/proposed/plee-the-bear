/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_killer_toggle.cpp
 * \brief Implementation of the ptb::player_killer_toggle class.
 * \author Julien Jorge
 */
#include "ptb/item/player_killer_toggle.hpp"

#include "ptb/util/player_util.hpp"

BASE_ITEM_EXPORT( player_killer_toggle, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param player_index The index of the player to kill.
 */
ptb::player_killer_toggle::player_killer_toggle( unsigned int player_index  )
  : m_player_index(player_index)
{

} // player_killer_toggle::player_killer_toggle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of a field of type unsigned int.
 * \param name The name of the field to set.
 * \param value The value of the field.
 */
bool ptb::player_killer_toggle::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result(true);

  if ( name == "player_killer_toggle.player_index" )
    m_player_index = value;
  else
    result = super::set_u_integer_field( name, value );

  return result;
} // player_killer_toggle::set_u_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief The toggle is activated, kill the player.
 * \param activator The item who activated this toggle.
 */
void ptb::player_killer_toggle::on_toggle_on
( bear::engine::base_item* activator )
{
  super::on_toggle_on(activator);

  player_proxy p = util::find_player( get_level_globals(), m_player_index );

  if ( p != NULL )
    p.start_action( player_action::die );
} // player_killer_toggle::on_toggle_on()

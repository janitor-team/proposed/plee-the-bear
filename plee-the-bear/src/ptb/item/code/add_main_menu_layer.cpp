/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file add_main_menu_layer.cpp
 * \brief Implementation of the ptb::add_main_menu_layer class.
 * \author Julien Jorge
 */
#include "ptb/item/add_main_menu_layer.hpp"

#include "engine/level.hpp"
#include "engine/level_globals.hpp"
#include "ptb/layer/main_menu_layer.hpp"
#include "ptb/layer/misc_layer.hpp"

BASE_ITEM_EXPORT( add_main_menu_layer, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::add_main_menu_layer::add_main_menu_layer()
{

} // add_main_menu_layer::add_main_menu_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::add_main_menu_layer::pre_cache()
{
  get_level_globals().load_font("font/fixed_white-7x12.fnt");
  get_level_globals().load_font("font/fixed_yellow-10x20.fnt");
  get_level_globals().load_image("gfx/ui/frame.png");
} // add_main_menu_layer::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Show the menu.
 */
void ptb::add_main_menu_layer::build()
{
  get_level().push_layer( new main_menu_layer( get_bottom_left() ) );
  get_level().push_layer( new misc_layer() );
} // add_main_menu_layer::build()

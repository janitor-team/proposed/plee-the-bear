/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file code/players_detector.cpp
 * \brief Implementation of the ptb::players_detector class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/players_detector.hpp"

BASE_ITEM_EXPORT( players_detector, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::players_detector::players_detector()
{
  set_weak_collisions(true);
} // players_detector::players_detector()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <std::string>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::players_detector::set_string_field
( const std::string& name, const std::string& value )
{
  bool result(true);

  if ( name == "players_detector.category" )
    m_category = value;
  else
    result = super::set_string_field( name, value );

  return result;
} // players_detector::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the category of the item.
 */
const std::string& ptb::players_detector::get_category() const
{
  return m_category;
} // players_detector::get_category()

/*----------------------------------------------------------------------------*/
/**
 * \brief Activation by one player among the two players.
 * \param p The player.
 */
void ptb::players_detector::on_one_player( const player_proxy& p )
{
  kill();
} // players_detector::on_one_player()

/*----------------------------------------------------------------------------*/
/**
 * \brief Activation by all players.
 * \param p1 The first player.
 * \param p2 The second player. NULL in a single player game.
 */
void ptb::players_detector::on_all_players
( const player_proxy& p1, const player_proxy& p2 )
{
  kill();
} // players_detector::on_all_players()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file add_ingame_layers.cpp
 * \brief Implementation of the ptb::add_ingame_layers class.
 * \author Julien Jorge
 */
#include "ptb/item/add_ingame_layers.hpp"

#include "engine/export.hpp"
#include "engine/level.hpp"
#include "engine/level_globals.hpp"
#include "engine/layer/transition_layer.hpp"

#include "ptb/layer/balloon_layer.hpp"
#include "ptb/layer/ingame_menu_layer.hpp"
#include "ptb/layer/misc_layer.hpp"
#include "ptb/layer/physics_layer.hpp"
#include "ptb/layer/player_arrows_layer.hpp"
#include "ptb/layer/status_layer.hpp"
#include "ptb/transition_effect/level_starting_effect.hpp"

#ifndef NDEBUG
#include "ptb/layer/item_information_layer.hpp"
#include "ptb/layer/link_layer.hpp"
#include "ptb/layer/recent_path_layer.hpp"
#include "ptb/layer/wireframe_layer.hpp"
#endif

#include "ptb/defines.hpp"

BASE_ITEM_EXPORT( add_ingame_layers, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::add_ingame_layers::add_ingame_layers()
: m_add_starting_effect(true), m_timer(NULL)
{

} // add_ingame_layers::add_ingame_layers()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::add_ingame_layers::pre_cache()
{
  get_level_globals().load_font("font/bouncy.fnt");
  get_level_globals().load_font("font/fixed_white-7x12.fnt");
  get_level_globals().load_font("font/fixed_yellow-10x20.fnt");
  get_level_globals().load_font("font/level_name-42x50.fnt");
  get_level_globals().load_image("gfx/ui/frame.png");
  get_level_globals().load_image("gfx/ui/status/status.png");
} // add_ingame_layers::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::add_ingame_layers::build()
{
  bear::engine::transition_layer* transition
    ( new bear::engine::transition_layer
      (PTB_TRANSITION_EFFECT_DEFAULT_TARGET_NAME) );

  status_layer* status
    ( new status_layer(PTB_STATUS_LAYER_DEFAULT_TARGET_NAME) );

  if ( m_timer != NULL )
    status->set_timer(m_timer);

  status->set_corrupting_bonus(m_corrupting_animation);

  get_level().push_layer( status );
  get_level().push_layer( new player_arrows_layer );
  get_level().push_layer
    ( new balloon_layer(PTB_BALLOON_LAYER_DEFAULT_TARGET_NAME) );
  get_level().push_layer
    ( new ingame_menu_layer(PTB_WINDOWS_LAYER_DEFAULT_TARGET_NAME) );
  // must be after ingame_menu_layer, so we can prevent the user from pausing
  // the game
  get_level().push_layer( transition );
  get_level().push_layer( new misc_layer );

#ifndef NDEBUG
  get_level().push_layer( new link_layer );
  get_level().push_layer( new physics_layer );
  get_level().push_layer( new wireframe_layer );
  get_level().push_layer( new item_information_layer );
  get_level().push_layer( new recent_path_layer );
#endif

  if (m_add_starting_effect)
    transition->push_effect( new level_starting_effect, 1000 );

  kill();
} // add_ingame_layers::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c boolean.
 * \param name The name of the field.
 * \param v The value of the field.
 */
bool
ptb::add_ingame_layers::set_bool_field( const std::string& name, bool value )
{
  bool result(true);

  if ( name == "add_ingame_layers.show_introduction" )
    m_add_starting_effect = value;
  else
    result = super::set_bool_field(name, value);

  return result;
} // add_ingame_layers::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c animation.
 * \param name The name of the field.
 * \param v The value of the field.
 */
bool ptb::add_ingame_layers::set_animation_field
( const std::string& name, const bear::visual::animation& value )
{
  bool result(true);

  if ( name == "add_ingame_layers.corrupting_bonus_animation" )
    m_corrupting_animation = value;
  else
    result = super::set_animation_field(name, value);

  return result;
} // add_ingame_layers::set_animation_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c item.
 * \param name The name of the field.
 * \param v The value of the field.
 */
bool ptb::add_ingame_layers::set_item_field
( const std::string& name, bear::engine::base_item* value )
{
  bool result(true);

  if ( name == "add_ingame_layers.status_layer.timer" )
    {
      m_timer = dynamic_cast<bear::timer*>(value);

      if ( m_timer == NULL )
        claw::logger << claw::log_error << "add_ingame_layers::set_item_field:"
                     << " item is not an instance of 'bear::timer'."
                     << std::endl;
    }
  else
    result = super::set_item_field(name, value);

  return result;
} // add_ingame_layers::set_item_field()

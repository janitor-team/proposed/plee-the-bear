/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_stop_block.cpp
 * \brief Implementation of the bear::player_stop_block class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/player_stop_block.hpp"

#include "universe/collision_info.hpp"

#include "ptb/player_proxy.hpp"

BASE_ITEM_EXPORT( player_stop_block, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if \a that is a player align the other item.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::player_stop_block::collision_check_player_and_align
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  player_proxy p(&that);

  if (p != NULL)
    collision_check_and_align(that, info);
} // player_stop_block::collision_check_player_and_align()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_align().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::player_stop_block::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  collision_check_player_and_align(that, info);
} // player_stop_block::collision()

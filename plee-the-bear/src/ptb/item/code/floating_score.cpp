/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item/code/floating_score.cpp
 * \brief Implementation of the ptb::floating_score class.
 * \author Julien Jorge
 */
#include "ptb/item/floating_score.hpp"

#include "generic_items/decorative_effect.hpp"

#include "ptb/defines.hpp"
#include "ptb/game_variables.hpp"
#include "ptb/util/player_util.hpp"

#include <sstream>
#include <limits>

BASE_ITEM_EXPORT( floating_score, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::floating_score::floating_score()
{
  set_mass(1);
  set_density(9.0/10000);
  set_kill_when_leaving(true);
} // floating_score::floating_score()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::floating_score::pre_cache()
{
  super::pre_cache();

  get_level_globals().load_font("font/bouncy.fnt");
} // floating_score::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
void ptb::floating_score::build()
{
  super::build();

  set_font( get_level_globals().get_font("font/bouncy.fnt") );
} // floating_score::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the score given by the item.
 * \param player_index The index of the player to which the points are given.
 * \param points The points to give to the player.
 */
void ptb::floating_score::add_points
( unsigned int player_index, unsigned int points )
{
  const unsigned int old_score( game_variables::get_score(player_index) );
  const unsigned int new_score( old_score + points );

  game_variables::set_score( player_index, new_score );

  player_proxy p = util::find_player( get_level_globals(), player_index );

  // check if the player wins a new try

  if ( (old_score / PTB_ONE_UP_POINTS_COUNT)
       < (new_score / PTB_ONE_UP_POINTS_COUNT) )
    give_one_up_to(player_index);

  const double intensity( (double)points / (double)PTB_MAX_POINTS_AT_ONCE );

  if ( player_index == 1 )
    get_rendering_attributes().set_intensity(intensity, 1, intensity);
  else
    get_rendering_attributes().set_intensity(intensity, intensity, 1);

  std::ostringstream oss;
  oss << points;
  set_text( oss.str() );

  create_effect();
} // floating_score::add_points()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give one more try to a player.
 * \param player_index The index of the player to which the points are given.
 */
void ptb::floating_score::one_up( unsigned int player_index )
{
  game_variables::set_lives_count
    ( player_index, game_variables::get_lives_count(player_index) + 1);

  if ( player_index == 1 )
    get_rendering_attributes().set_intensity(1, 0, 0);
  else
    get_rendering_attributes().set_intensity(0, 1, 1);

  set_text( "1up" );

  create_effect();

  get_level_globals().play_music("music/1-up.ogg", 1);
} // floating_score::one_up()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the displayed text.
 * \param text The text to display.
 */
void ptb::floating_score::set_score( const std::string& text )
{
  set_text( text );

  create_effect();
} // floating_score::set_score()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the effects on the score.
 */
void ptb::floating_score::create_effect()
{
  const bear::universe::position_type pos(get_center_of_mass());
  fit_to_text();
  set_center_of_mass(pos);
  get_rendering_attributes().set_size( get_size() / 2 );

  bear::decorative_effect* decoration_effect = new bear::decorative_effect;

  decoration_effect->set_duration(1);
  decoration_effect->set_opacity_factor_init(1);
  decoration_effect->set_opacity_factor_end(0);
  decoration_effect->set_item(this, true);

  new_item(*decoration_effect);
} // floating_score::create_effect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give a one up to a player, creating a new floating_score on him.
 * \param player_index The index of the player to which the points are given.
 */
void ptb::floating_score::give_one_up_to( unsigned int player_index ) const
{
  player_proxy p = util::find_player( get_level_globals(), player_index );

  if ( p != NULL )
    p.give_one_up();
} // floating_score::give_one_up_to()

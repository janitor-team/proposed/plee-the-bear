/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file ptb/item/code/stone_tracer.cpp
 * \brief Implementation of the ptb::stone_tracer class.
 * \author Julien Jorge
 */
#include "ptb/item/stone_tracer.hpp"

#include "engine/expr/check_item_class_hierarchy.hpp"
#include "ptb/item/stone/stone.hpp"

BASE_ITEM_EXPORT(stone_tracer, ptb)

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::stone_tracer::stone_tracer()
{
  bear::engine::check_item_class_hierarchy<stone> e;
  e.set_collision_data( get_collision_in_expression() );
  set_condition( e );
} // stone_tracer::stone_tracer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 * \param that The instance to copy from.
 */
ptb::stone_tracer::stone_tracer( const stone_tracer& that )
  : super(that)
{
  bear::engine::check_item_class_hierarchy<stone> e;
  e.set_collision_data( get_collision_in_expression() );
  set_condition( e );
} // stone_tracer::stone_tracer()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_manager.cpp
 * \brief Implementation of the ptb::bonus_manager class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/bonus_manager.hpp"
#include "ptb/game_variables.hpp"
#include "ptb/defines.hpp"
#include "ptb/item/bonus_points/bonus_points.hpp"

#include "engine/game.hpp"
#include "engine/world.hpp"

BASE_ITEM_EXPORT( bonus_manager, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::bonus_manager::bonus_manager()
{
} // bonus_manager::bonus_manager()

/*----------------------------------------------------------------------------*/
/**
 * \brief Manage bonus.
 */
void ptb::bonus_manager::manage()
{
  bear::engine::world::const_item_iterator it;
  for ( it=get_world().living_items_begin();
        it!=get_world().living_items_end(); ++it)
    {
      const bonus_points* pts = dynamic_cast<const bonus_points*>(&(*it));

      if ( pts != NULL )
        pts->update_bonus_state();
    }

  save_game_variables();
} // bonus_manager::manage()

/*----------------------------------------------------------------------------*/
/**
 * \brief Actions done when the state of the toggle changes from off to on.
 * \param activator The item that changed the state.
 */
void ptb::bonus_manager::on_toggle_on
(bear::engine::base_item *activator)
{
  manage();
} // bonus_manager::on_toggle_on()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save all game variables.
 */
void ptb::bonus_manager::save_game_variables() const
{
  std::string filename(bear::engine::game::get_instance().get_custom_game_file
      (PTB_PROFILES_FOLDER) + game_variables::get_profile_name() + "/");
  if ( game_variables::get_players_count() == 1 )
    filename += PTB_SAVE_ONE_PLAYER_FILENAME;
  else
    filename += PTB_SAVE_TWO_PLAYERS_FILENAME;

  std::ofstream f( filename.c_str() );

  bear::engine::game::get_instance().save_game_variables(f, "persistent/.*");
} // bonus_manager::save_game_variables()

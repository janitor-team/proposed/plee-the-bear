/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file corrupting_bonus_attractor.cpp
 * \brief Implementation of the ptb::corrupting_bonus_attractor class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/corrupting_bonus_attractor.hpp"

#include "ptb/item/corrupting_bonus.hpp"

BASE_ITEM_EXPORT( corrupting_bonus_attractor, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::corrupting_bonus_attractor::corrupting_bonus_attractor()
{
  set_phantom(true);
  set_can_move_items(false);
  set_size(600, 600);
} // corrupting_bonus_attractor::corrupting_bonus_attractor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_apply().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::corrupting_bonus_attractor::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  corrupting_bonus* item = dynamic_cast<corrupting_bonus*>(&that);

  if ( item != NULL )
    item->attract(*this);
} // corrupting_bonus_attractor::collision()

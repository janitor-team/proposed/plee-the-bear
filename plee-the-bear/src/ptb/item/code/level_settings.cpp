/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file level_settings.cpp
 * \brief Implementation of the ptb::level_settings class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/level_settings.hpp"
#include "ptb/level_variables.hpp"
#include "ptb/game_variables.hpp"

BASE_ITEM_EXPORT( level_settings, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::level_settings::level_settings()
  : m_is_main_level(true)
{
} // level_settings::level_settings()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::level_settings::build()
{
  super::build();

  if ( m_is_main_level )
    game_variables::set_main_level_name(get_level().get_filename());

  kill();
} // level_settings::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
ptb::level_settings::set_bool_field( const std::string& name, bool value )
{
  bool result = true;

  if (name == "level_settings.friendly_fire")
    level_variables::set_friendly_fire(get_level(), value);
  else if (name == "level_settings.is_main_level")
    m_is_main_level  = value;
  else
    result = super::set_bool_field(name, value);

  return result;
} // level_settings::set_bool_field()


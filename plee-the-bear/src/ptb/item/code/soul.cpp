/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file soul.cpp
 * \brief Implementation of the ptb::soul class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/soul.hpp"
#include "ptb/player_proxy.hpp"
#include "engine/export.hpp"

#include "universe/forced_movement/forced_translation.hpp"
#include "universe/physical_item_state.hpp"
#include "universe/world.hpp"

BASE_ITEM_EXPORT( soul, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::soul::soul()
{
  m_energy = 0;

  set_phantom(true);
  set_can_move_items(false);
  set_mass(1);
  set_density(0.0009);
  set_friction(1);
} // soul::soul()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::soul::~soul()
{
} // soul::soul()

/*----------------------------------------------------------------------------*/
/**
 * \brief Process a collision.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::soul::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  player_proxy other(&that);

  if (other != NULL)
    {
      other.receive_energy(m_energy);
      m_energy = 0;
      kill();
    }
} // soul::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the energy of the soul.
 * \param energy The new energy of the soul.
 */
void ptb::soul::set_energy( double energy )
{
  m_energy = energy;
} // soul::set_energy()

/*---------------------------------------------------------------------------*/
/**
 * \brief Inform the item tat he left the active region.
 */
void ptb::soul::leaves_active_region()
{
  super::leaves_active_region();

  kill();
} // ptb::soul::leaves_active_region()

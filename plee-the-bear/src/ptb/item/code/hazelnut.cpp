/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file hazelnut.cpp
 * \brief Implementation of the ptb::hazelnut class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/hazelnut.hpp"

#include "ptb/player_proxy.hpp"
#include "ptb/level_variables.hpp"
#include "ptb/item/bonus_points/bonus_points.hpp"

#include "engine/level_globals.hpp"
#include "engine/function/bool_level_variable_getter.hpp"

BASE_ITEM_EXPORT( hazelnut, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::hazelnut::hazelnut()
{
  set_can_move_items(false);
  set_elasticity(1);
  set_friction(0.98);
  set_mass(20);
  set_density(2);
} // hazelnut::hazelnut()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::hazelnut::pre_cache()
{
  super::pre_cache();

  get_level_globals().load_animation("animation/owl/hazelnut.canim");
} // hazelnut::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::hazelnut::build()
{
  super::build();

  level_variables::set_object_count
    ( this->get_level(), "hazelnut",
      level_variables::get_object_count( this->get_level(), "hazelnut" ) + 1);

  if ( level_variables::get_object_count( this->get_level(), "hazelnut" ) == 1 )
    create_level_bonus();

  set_animation
    ( get_level_globals().get_animation("animation/owl/hazelnut.canim") );

  set_size( get_animation().get_size() );
} // hazelnut::build()

/*---------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::hazelnut::progress( bear::universe::time_type elapsed_time )
{
  super::progress(elapsed_time);

  if ( has_bottom_contact() )
    add_internal_force( bear::universe::force_type(0, 200000) );
} // hazelnut::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the collision is with a player.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::hazelnut::collision_check_and_apply
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  const player_proxy p(&that);

  if ( p != NULL )
    {
      if ( !level_variables::get_current_hazelnut(get_level()) )
        {
          if ( info.get_collision_side() != bear::universe::zone::middle_zone)
            {
              level_variables::set_current_hazelnut(get_level(), true);
              level_variables::set_hazelnut_found(get_level(), true);
              kill();
            }
          else
            default_collision(info);
        }
      else
        default_collision(info);
    }
  else
    default_collision(info);
} // hazelnut::collision_check_and_apply()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_apply().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::hazelnut::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  collision_check_and_apply(that, info);
} // hazelnut::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create level bonus objects.
 */
void ptb::hazelnut::create_level_bonus()
{
  bonus_points* new_bonus_hazelnut = new bonus_points( "Hazelnut", 5000 );

  new_bonus_hazelnut->set_picture_filename
    ("gfx/ui/bonus-icons/bonus-icons.png");
  new_bonus_hazelnut->set_picture_name("hazelnut");
  new_bonus_hazelnut->set_condition
    ( bear::engine::bool_level_variable_getter
      ( &get_level(), "hazelnut_found" ) );

  new_item( *new_bonus_hazelnut );
  CLAW_ASSERT(new_bonus_hazelnut->is_valid(),
              "The bonus_points isn't correctly initialized" );
  new_bonus_hazelnut->set_center_of_mass(get_center_of_mass());
} // owl::create_level_bonus()

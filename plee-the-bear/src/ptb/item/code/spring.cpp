/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file spring.cpp
 * \brief Implementation of the ptb::spring class.
 * \author Julien Jorge
 */
#include "ptb/item/spring.hpp"

#include "universe/collision_info.hpp"
#include "engine/layer/layer.hpp"
#include "engine/world.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( spring, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
ptb::spring::spring()
  : m_applied_force(0, 0)
{

} // spring::spring()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::spring::set_real_field( const std::string& name, double value )
{
  bool result = true;

  if ( name == "spring.applied_force.x" )
    m_applied_force.x = value;
  else if ( name == "spring.applied_force.y" )
    m_applied_force.y = value;
  else
    result = super::set_real_field( name, value );

  return result;
} // spring::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::spring::build()
{
  if ( get_layer().has_world() )
    {
      m_applied_force.x =
        get_layer().get_world().to_world_unit( m_applied_force.x );
      m_applied_force.y =
        get_layer().get_world().to_world_unit( m_applied_force.y );
    }

  get_animation().set_current_index(get_animation().get_max_index());
} // forced_join_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the collision is on an active side and bounce the other item.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::spring::collision_check_and_bounce
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  bool bounce(false);
  bool top_contact(false);

  switch( info.get_collision_side() )
    {
    case bear::universe::zone::bottom_zone:
      bounce = bottom_side_is_active();
      break;
    case bear::universe::zone::top_zone:
      bounce = top_side_is_active();
      top_contact = true;
      break;
    case bear::universe::zone::middle_left_zone:
      bounce = left_side_is_active();
      break;
    case bear::universe::zone::middle_right_zone:
      bounce = right_side_is_active();
      break;
    case bear::universe::zone::middle_zone:
      break;
    default: { CLAW_ASSERT( false, "Invalid collision side." ); }
    }

  if ( bounce )
    {
      if ( m_applied_force.x > 0 )
        bounce = collision_align_right( info );
      else if ( m_applied_force.x < 0 )
        bounce = collision_align_left( info );

      if ( m_applied_force.y > 0 )
        bounce = collision_align_top( info );
      else if ( m_applied_force.y < 0 )
        bounce = collision_align_bottom( info );

      if (bounce)
        {
          that.add_external_force(m_applied_force);
          if ( top_contact )
            that.set_bottom_contact(false);
          get_animation().reset();
        }
    }
  else
    default_collision(info);
} // spring::collision_check_and_bounce()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_check_and_align().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void ptb::spring::collision
( bear::engine::base_item& that, bear::universe::collision_info& info )
{
  collision_check_and_bounce(that, info);
} // spring::collision()

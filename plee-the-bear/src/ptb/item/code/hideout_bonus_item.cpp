/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file code/hideout_bonus_item.cpp
 * \brief Implementation of the ptb::hideout_bonus_item class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/hideout_bonus_item.hpp"

#include "ptb/defines.hpp"
#include "ptb/level_variables.hpp"
#include "ptb/item/bonus_points/bonus_points.hpp"
#include "engine/function/bool_level_variable_getter.hpp"
#include "engine/game.hpp"

BASE_ITEM_EXPORT( hideout_bonus_item, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::hideout_bonus_item::hideout_bonus_item()
{
  set_weak_collisions(true);
} // hideout_bonus_item::hideout_bonus_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::hideout_bonus_item::build()
{
  super::build();

  level_variables::set_object_count
    ( this->get_level(), "hideout",
      level_variables::get_object_count
      ( this->get_level(), "hideout" ) + 1);

  std::ostringstream s;
  s << "hideout "
    << level_variables::get_object_count( this->get_level(), "hideout" );;
  m_id = s.str();

  game_variables::set_hideout_state
    ( m_id, game_variables::get_hideout_state( m_id ) );

  if ( level_variables::get_object_count( this->get_level(), "hideout" ) == 1 )
    create_level_bonus();

  if ( game_variables::get_hideout_state( m_id ) )
    kill();
} // hideout_bonus_item::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Activation by one player among the two players.
 * \param p The player.
 */
void ptb::hideout_bonus_item::on_one_player( const player_proxy& p )
{
  discover();
} // hideout_bonus_item::on_one_player()

/*----------------------------------------------------------------------------*/
/**
 * \brief Activation by all players.
 * \param p1 The first player.
 * \param p2 The second player. NULL in a single player game.
 */
void ptb::hideout_bonus_item::on_all_players
( const player_proxy& p1, const player_proxy& p2 )
{
  discover();
} // hideout_bonus_item::on_all_players()

/*----------------------------------------------------------------------------*/
/**
 * \brief The hideout is discovered.
 */
void ptb::hideout_bonus_item::discover()
{
  if ( !game_variables::get_hideout_state(m_id) )
    {
      game_variables::set_hideout_state( m_id, true);

      bear::engine::var_map vars;
      bear::engine::game::get_instance().get_game_variables
        ( vars, PTB_PERSISTENT_PREFIX +
          game_variables::get_main_level_name() + "/hideout/.*");

      bool bonus(true);
      bear::engine::var_map::iterator<bool>::type it;
      std::string prefix
        (PTB_PERSISTENT_PREFIX + game_variables::get_main_level_name()
         + "/hideout/");

      for ( it=vars.begin<bool>();
            (it!=vars.end<bool>()) && bonus; ++it )
        {
          std::string var(it->first);
          std::string c1;
          c1 = var.erase(0, prefix.size());

          if ( !game_variables::get_hideout_state(c1) )
            bonus = false;
        }

      if ( bonus )
        level_variables::all_hideouts_found(get_level());
    }

  kill();
} // hideout_bonus_item::discover()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create level bonus object.
 */
void ptb::hideout_bonus_item::create_level_bonus()
{
  bonus_points* new_bonus = new bonus_points( "Great explorer", 10000 );

  new_bonus->set_picture_filename("gfx/ui/bonus-icons/bonus-icons.png");
  new_bonus->set_picture_name("hideouts");
  new_bonus->set_condition
   ( bear::engine::bool_level_variable_getter
     (&get_level(), "all_hideouts_found"));

  new_item( *new_bonus );
  CLAW_ASSERT(new_bonus->is_valid(),
              "The bonus isn't correctly initialized" );
  new_bonus->set_center_of_mass(get_center_of_mass());
} // hideout_bonus_item::create_level_bonus()

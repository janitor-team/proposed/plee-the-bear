/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file water_fire_stone.hpp
 * \brief The class describing a stone with water and fire powers.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_WATER_FIRE_STONE_HPP__
#define __PTB_WATER_FIRE_STONE_HPP__

#include "ptb/item/stone/fire_stone.hpp"

namespace ptb
{
  /**
   * \brief The class describing a stone with water and fire powers.
   * \author Sébastien Angibaud
   */
  class water_fire_stone:
    public fire_stone
  {
    DECLARE_BASE_ITEM(water_fire_stone);

  public:
    /** \brief The type of the parent class. */
    typedef fire_stone super;

  public:
    water_fire_stone();

    void build();
    void progress(bear::universe::time_type elapsed_time);
    virtual bool has_water_power() const;

  private:
    /* \brief The last position of the item. */
    bear::universe::position_type m_last_position;

    /* \brief The number of iterations without move. */
    unsigned int m_iteration_without_move;
  }; // class water_fire_stone
} // namespace ptb

#endif // __PTB_WATER_FIRE_STONE_HPP__

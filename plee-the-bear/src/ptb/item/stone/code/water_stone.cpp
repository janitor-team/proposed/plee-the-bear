/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file water_stone.cpp
 * \brief Implementation of the ptb::water_stone class.
 * \author Sébastien Angibaud
 */
#include "ptb/item/stone/water_stone.hpp"

BASE_ITEM_IMPLEMENT( water_stone, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
  ptb::water_stone::water_stone()
{
  set_elasticity(1);
  set_density(0.7);
  m_offensive_coefficients[water_attack] = 1;
  m_iteration_without_move = 0;
} // water_stone::water_stone()

/*---------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
void ptb::water_stone::build()
{
  super::build();
  m_last_position = get_bottom_left();
  set_model_actor
    (get_level_globals().get_model("model/stones/water_stone.cm"));
  start_model_action("attack");
} // water_stone::build()

/*---------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::water_stone::progress( bear::universe::time_type elapsed_time )
{
  if ( ( has_contact() && ( m_iteration_without_move >= 8 ) ) ||
       has_middle_contact() ||
       ( is_in_environment(bear::universe::water_environment) &&
         !is_only_in_environment(bear::universe::water_environment) ) )
    kill();
  else
    {
      if ( ( std::abs(m_last_position.x - get_left()) <= 1 ) &&
           ( std::abs(m_last_position.y - get_bottom()) <= 1 ) )
        ++m_iteration_without_move;
      else
        m_iteration_without_move = 0;

      m_last_position = get_bottom_left();
      default_progress( elapsed_time );
    }
} // water_stone::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the stone has the water power.
 */
bool ptb::water_stone::has_water_power() const
{
  return true;
} // water_stone::has_water_power()

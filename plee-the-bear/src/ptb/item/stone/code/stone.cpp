/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file stone.cpp
 * \brief Implementation of the ptb::stone class.
 * \author S�bastien Angibaud
 */
#include "ptb/item/stone/stone.hpp"
#include "ptb/item/air_bubble.hpp"
#include "engine/world.hpp"

BASE_ITEM_EXPORT( stone, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::stone::stone()
{
  set_friction(0.98);
  set_mass(1);
  set_density(2);
  set_z_fixed(false);
  set_size(16, 16);
  set_weak_collisions(false);

  m_monster_type = monster::stone_monster;
  m_energy = 1;
  m_offensive_force = 3;
  m_offensive_coefficients[normal_attack] = 1;

  set_system_angle_as_visual_angle(true);
} // stone::stone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
void ptb::stone::build()
{
  super::build();

  set_model_actor(get_level_globals().get_model("model/stones/stone.cm"));
  start_model_action("attack");
} // stone::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::stone::progress( bear::universe::time_type elapsed_time )
{
  if ( has_contact() )
    kill();
  else
    {
      rotate();
      default_progress(elapsed_time);
    }
} // stone::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief The item has attacked.
 * \param other The monster that is attacked.
 */
void ptb::stone::has_attacked(const monster& other)
{
  kill();
} // stone::has_attacked()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the item tat he left the active region.
 */
void ptb::stone::leaves_active_region()
{
  super::leaves_active_region();

  kill();
} // stone::leaves_active_region()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the item that he have no energy now.
 */
void ptb::stone::inform_no_energy(const monster& attacker)
{
  kill();
} // stone::inform_no_energy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Kill the item.
 * \pre m_owner != NULL
 */
void ptb::stone::kill()
{
  if ( is_only_in_environment(bear::universe::water_environment) )
    create_bubble();

  super::kill();
} // stone::kill()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform that a new stone is create.
 */
void ptb::stone::inform_new_stone()
{

} // stone::inform_new_stone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the stone has the air power.
 */
bool ptb::stone::has_air_power() const
{
  return false;
} // stone::has_air_power()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the stone has the fire power.
 */
bool ptb::stone::has_fire_power() const
{
  return false;
} // stone::has_fire_power()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the stone has the water power.
 */
bool ptb::stone::has_water_power() const
{
  return false;
} // stone::has_water_power()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a star to show a hit.
 * \param that The item being attacked by this.
 */
void ptb::stone::create_hit_star( const bear::engine::base_item& that ) const
{
  super::create_hit_star
    ( get_center_of_mass(), get_rendering_attributes().get_angle() );
} // stone::create_hit_star()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a bubble.
 */
void ptb::stone::create_bubble()
{
  air_bubble* new_bubble;

  new_bubble= new air_bubble();
  new_bubble->set_z_position(get_z_position() + 1);
  new_bubble->set_center_of_mass(get_center_of_mass());
  new_bubble->set_oxygen(2000);
  //new_bubble->give_max_size();
  new_item( *new_bubble );
} // stone::create_bubble()

/*----------------------------------------------------------------------------*/
/**
 * \brief Rotate the stone.
 */
void ptb::stone::rotate()
{
  double delta = 0.05;

  if ( is_in_environment(bear::universe::water_environment) )
    delta = 0.01;

  if ( get_speed().x >= 0 )
    delta *= -1;

  get_rendering_attributes().set_angle
    ( get_rendering_attributes().get_angle() + delta );
} // stone::rotate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do the default progress of a stone.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::stone::default_progress( bear::universe::time_type elapsed_time )
{
  super::progress( elapsed_time );
} // stone::default_progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Export the methods of the class.
 */
void ptb::stone::init_exported_methods()
{
  TEXT_INTERFACE_CONNECT_METHOD_0( ptb::stone, kill, void );
} // stone::init_exported_methods()

/*----------------------------------------------------------------------------*/
TEXT_INTERFACE_IMPLEMENT_METHOD_LIST( ptb::stone )

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file fire_stone.hpp
 * \brief The class describing a stone with fire power.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_FIRE_STONE_HPP__
#define __PTB_FIRE_STONE_HPP__

#include "ptb/item/stone/stone.hpp"

namespace ptb
{
  /**
   * \brief The class describing a stone with the fire power.
   * \author Sébastien Angibaud
   */
  class fire_stone:
    public stone
  {
    DECLARE_BASE_ITEM(fire_stone);

  public:
    /** \brief The type of the parent class. */
    typedef stone super;

  public:
    fire_stone();

    virtual void progress( bear::universe::time_type elapsed_time );
    virtual void build();
    void has_attacked(const monster& other);
    void inform_no_energy(const monster& attacker);
    virtual bool has_fire_power() const;

  private:
    void create_smoke(const bear::visual::animation& anim);

  protected:
    void progress_fire_stone( bear::universe::time_type elapsed_time );

  private:
    /* \brief The time since the last smoke. */
    bear::universe::time_type m_last_smoke;

    /* \brief Indicates if the stone is extinct. */
    bool m_is_extinct;
  }; // class fire_stone
} // namespace ptb

#endif // __PTB_FIRE_STONE_HPP__

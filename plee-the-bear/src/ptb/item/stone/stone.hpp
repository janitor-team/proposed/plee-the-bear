/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file stone.hpp
 * \brief The class describing a stone.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_STONE_HPP__
#define __PTB_STONE_HPP__

#include "ptb/monster_item.hpp"
#include "engine/model.hpp"
#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a stone.
   * \author Sébastien Angibaud
   */
  class stone:
    public monster_item< bear::engine::model<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(stone);

  public:
    /** \brief The type of the parent class. */
    typedef monster_item< bear::engine::model<bear::engine::base_item> > super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  public:
    stone();

    void build();
    void progress( bear::universe::time_type elapsed_time );
    virtual void has_attacked(const monster& other);
    void leaves_active_region();
    virtual void inform_no_energy(const monster& attacker);
    virtual void kill();
    virtual void inform_new_stone();

    virtual bool has_air_power() const;
    virtual bool has_fire_power() const;
    virtual bool has_water_power() const;

  private:
    void create_hit_star( const bear::engine::base_item& that ) const;
    void create_bubble();

  protected:
    void rotate();
    void default_progress( bear::universe::time_type elapsed_time );

  private:
    static void init_exported_methods();

  }; // class stone
} // namespace ptb

#endif // __PTB_STONE_HPP__

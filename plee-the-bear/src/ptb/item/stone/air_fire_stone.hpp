/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file air_stone.hpp
 * \brief The class describing a stone with the air and fire powers.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_AIR_FIRE_STONE_HPP__
#define __PTB_AIR_FIRE_STONE_HPP__

#include "ptb/player_proxy.hpp"
#include "ptb/item/stone/fire_stone.hpp"
#include "generic_items/reference_item.hpp"

namespace ptb
{
  /**
   * \brief The class describing a stone with the air and fire powers.
   * \author Sébastien Angibaud
   */
  class air_fire_stone:
    public fire_stone
  {
     DECLARE_BASE_ITEM(air_fire_stone);

  public:
    /** \brief The type of the parent class. */
    typedef fire_stone super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  public:
    air_fire_stone();
    ~air_fire_stone();

    virtual void progress( bear::universe::time_type elapsed_time);
    void build();
    virtual void inform_new_stone();
    virtual void kill();
    virtual void inform_no_energy(const monster& attacker);
    virtual bool has_air_power() const;

  private:
    void blast();
    void create_decorations();
    void create_decorative_blast
    (const std::string& action, const bear::universe::speed_type& speed);
    void create_movement();

    static void init_exported_methods();

  private:
    /** \brief Pointer to the player. */
    player_proxy m_player;

    /** \brief Indicates if the stone blast. */
    bool m_blast;

    /** \brief The reference item for the movement. */
    bear::reference_item* m_reference_item;

  }; // class air_fire_stone
} // namespace ptb

#endif // __PTB_AIR_FIRE_STONE_HPP__

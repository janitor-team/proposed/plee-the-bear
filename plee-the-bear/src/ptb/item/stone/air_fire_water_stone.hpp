/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file air_fire_water_stone.hpp
 * \brief The class describing a stone with air, fire and water powers..
 * \author Sébastien Angibaud
 */
#ifndef __PTB_AIR_FIRE_WATER_STONE_HPP__
#define __PTB_AIR_FIRE_WATER_STONE_HPP__

#include "ptb/item/stone/stone.hpp"

namespace ptb
{
  /**
   * \brief The class describing a stone with air, fire and water powers.
   * \author Sébastien Angibaud
   */
  class air_fire_water_stone:
    public stone
  {
    DECLARE_BASE_ITEM(air_fire_water_stone);

  public:
    /** \brief The type of the parent class. */
    typedef stone super;

  public:
    air_fire_water_stone();

    void build();
    void progress( bear::universe::time_type elapsed_time );
    virtual void kill();

    virtual bool has_air_power() const;
    virtual bool has_fire_power() const;
    virtual bool has_water_power() const;

  private:
    void search_enemy();
    void blast();
    void create_stone
    ( monster::attack_type power, bear::universe::speed_type& speed );

  private:
    /* \brief Indicates if an enemy is found. */
    bool m_enemy_found;

    /* \brief The last position. */
    bear::universe::position_type m_last_position;

    /* \brief The number of iterations without movement. */
    unsigned int m_idle_iterations;

    /** \brief The distance under which the stone detect an enemy. */
    static const double s_view_distance;
  }; // class air_fire_water_stone
} // namespace ptb

#endif // __PTB_AIR_FIRE_WATER_STONE_HPP__

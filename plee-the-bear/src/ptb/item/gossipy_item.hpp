/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file gossipy_item.hpp
 * \brief The class describing a item that speaks from the beginning.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_GOSSIPY_ITEM_HPP__
#define __PTB_GOSSIPY_ITEM_HPP__

#include "ptb/item_brick/item_that_speaks.hpp"
#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a item that speaks from the beginning.
   * \author Sébastien Angibaud
   */
  class gossipy_item:
    public item_that_speaks<bear::engine::base_item>
  {
    DECLARE_BASE_ITEM(gossipy_item);

  }; // class gossipy_item
} // namespace ptb

#endif // __PTB_GOSSIPY_ITEM_HPP__

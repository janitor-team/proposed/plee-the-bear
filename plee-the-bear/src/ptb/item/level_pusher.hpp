/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item/level_pusher.hpp
 * \brief An item that starts a level when all players are colliding with it.
 * \author Julien Jorge
 */
#ifndef __PTB_LEVEL_PUSHER_HPP__
#define __PTB_LEVEL_PUSHER_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that starts a level when all players are colliding with it.
   *
   * The current level is saved by the engine and must be restored when the new
   * one is finished.
   *
   * The custom fields of this class are :
   * - level: (string) The level to load.
   *
   * \sa level_popper
   * \author Julien Jorge
   */
  class level_pusher:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(level_pusher);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    level_pusher();

    void progress( bear::universe::time_type elapsed_time );
    bool is_valid() const;
    bool set_string_field( const std::string& name, const std::string& value );

  private:
    void collision_check_and_apply
    ( bear::engine::base_item& that, bear::universe::collision_info& info );
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    /** \brief The name of the level to load. */
    std::string m_level_name;

    /** \brief Remember that the level has been pushed and wait until both
        players are outside before allowing to push the level again. */
    bool m_pushed;

    /** \brief How many players are colliding with this item. */
    std::size_t m_players_count;

  }; // class level_pusher
} // namespace ptb

#endif // __PTB_LEVEL_PUSHER_HPP__

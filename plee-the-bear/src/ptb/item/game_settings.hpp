/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file game_settings.hpp
 * \brief An item to set parameters of game.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_GAME_SETTINGS_HPP__
#define __PTB_GAME_SETTINGS_HPP__

#include "engine/base_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item to set parameters of game.
   *
   * The valid fields for this item are
   *  - \a corrupting_bonus_count:
   *  \c (unsigned integer) The number of corrupting bonus.
   *  - \a corrupting_bonus_animation_name:
   *  \c (string) The path of corrupting bonus animation.
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class game_settings:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(game_settings);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    game_settings();

    void build();

    virtual bool set_u_integer_field
    ( const std::string& name, unsigned int value );

  }; // class game_settings
} // namespace ptb

#endif // __PTB_GAME_SETTINGS_HPP__

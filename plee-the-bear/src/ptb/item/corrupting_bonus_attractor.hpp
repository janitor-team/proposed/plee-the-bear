/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file corrupting_bonus_attractor.hpp
 * \brief An item that attracts corrupting_bonus items.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_CORRUPTING_BONUS_ATTRACTOR_HPP__
#define __PTB_CORRUPTING_BONUS_ATTRACTOR_HPP__

#include "engine/base_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that attracts corrupting_bonus items.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class corrupting_bonus_attractor:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(corrupting_bonus_attractor);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    corrupting_bonus_attractor();

  private:
    virtual void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  }; // class corrupting_bonus_attractor
} // namespace ptb

#endif // __PTB_CORRUPTING_BONUS_ATTRACTOR_HPP__

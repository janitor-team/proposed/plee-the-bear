/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file sting.hpp
 * \brief The class describing a sting.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_STING_HPP__
#define __PTB_STING_HPP__

#include "engine/base_item.hpp"
#include "ptb/monster_item.hpp"
#include "engine/model.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a sting.
   * \author Sébastien Angibaud
   */
  class sting:
    public monster_item< bear::engine::model<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(sting);

  public:
    /** \brief The type of the parent class. */
    typedef monster_item< bear::engine::model<bear::engine::base_item> > super;

    /** \brief The energy of a sting.*/
    static const unsigned int s_sting_energy = 1;

    /** \brief The offensive_force of a sting.*/
    static const unsigned int s_sting_offensive_force = 20;

  public:
    sting();

    void build();
    void progress( bear::universe::time_type elapsed_time );
    void leaves_active_region();

  protected:
    virtual void has_attacked(const monster& other);

  private:
    /** \brief Indicates if the sting is dead. */
    bool m_is_dead;

  }; // class sting
} // namespace ptb

#endif // __PTB_STING_HPP__

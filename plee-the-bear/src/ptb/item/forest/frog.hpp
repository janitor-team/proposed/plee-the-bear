/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file frog.hpp
 * \brief The class describing a frog.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_FROG_HPP__
#define __PTB_FROG_HPP__

#include "engine/model.hpp"
#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a frog.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Sébastien Angibaud
   */
  class frog:
    public bear::engine::model<bear::engine::base_item>
  {
    DECLARE_BASE_ITEM(frog);

  public:
    /** \brief The type of the parent class. */
    typedef  bear::engine::model<bear::engine::base_item> super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  private:
    typedef void (frog::*progress_function_type)(bear::universe::time_type);

  public:
    frog();
    ~frog();

    void pre_cache();
    void build();
    void progress( bear::universe::time_type elapsed_time );
    bool set_real_field( const std::string& name, double value );

  protected:
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    void progress_idle( bear::universe::time_type elapsed_time );
    void progress_jump( bear::universe::time_type elapsed_time );
    void progress_fall( bear::universe::time_type elapsed_time );
    void progress_explose( bear::universe::time_type elapsed_time );

    void start_idle();
    void start_jump();
    void start_fall();
    void start_explose();

    void apply_jump();
    void choose_idle_action();

    bool scan_no_wall_in_direction
    ( const bear::universe::position_type& origin,
      const bear::universe::vector_type& dir ) const;

    bool test_in_sky();
    bool test_bottom_contact();
    void test_explose();
    void try_to_jump();
    bool can_jump() const;
    void create_floating_score() const;

    static void init_exported_methods();

  private:
    /** \brief Current progress function. */
    progress_function_type m_progress;

    /** \brief Initial position. */
    bear::universe::position_type m_initial_position;

    /** \brief The maximum distance of the initial position. */
    bear::universe::coordinate_type m_max_distance;

    /** \brief The last index of player on the frog. */
    unsigned int m_last_player_index;
  }; // class frog
} // namespace ptb

#endif // __PTB_FROG_HPP__

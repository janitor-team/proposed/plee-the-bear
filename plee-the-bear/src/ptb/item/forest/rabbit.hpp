/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file rabbit.hpp
 * \brief The class describing a rabbit.
 * \author Sébastien Angibaud
 * \author Julien Jorge
 */
#ifndef __PTB_RABBIT_HPP__
#define __PTB_RABBIT_HPP__

#include "ptb/player_proxy.hpp"
#include "ptb/item_brick/item_with_attack_point.hpp"

#include "engine/model.hpp"
#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a rabbit.
   *
   * The valid fields for this item are
   * - max_distance (real): The maximum horizontal distance from its initial
   *   position where the rabbit is allowed to go (default = 500),
   * - any field supported by the parent classes.
   *
   * \author Sébastien Angibaud
   * \author Julien Jorge
   */
  class rabbit:
    public  item_with_attack_point
    < bear::engine::model<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(rabbit);

  public:
    /** \brief The type of the parent class. */
    typedef item_with_attack_point
    < bear::engine::model<bear::engine::base_item> > super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  private:
    /** \brief The type of the functions used to do the progression of the
        rabbit. */
    typedef void (rabbit::*progress_function_type)(bear::universe::time_type);

  public:
    rabbit();

    void pre_cache();
    void build();
    void progress( bear::universe::time_type elapsed_time );
    bool set_real_field( const std::string& name, double value );
    bool set_bool_field( const std::string& name, bool value );

    void enters_active_region();

  protected:
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    bool is_attacked(const player_proxy& p);
    void progress_idle( bear::universe::time_type elapsed_time );
    void progress_jump( bear::universe::time_type elapsed_time );
    void progress_fall( bear::universe::time_type elapsed_time );
    void progress_eat( bear::universe::time_type elapsed_time );
    void progress_dig( bear::universe::time_type elapsed_time );
    void progress_in_burrow( bear::universe::time_type elapsed_time );
    void progress_injured( bear::universe::time_type elapsed_time );
    void progress_walk( bear::universe::time_type elapsed_time );

    void start_idle();
    void start_fall();
    void start_eat();
    void start_dig();
    void start_in_burrow();
    void start_injured();
    void start_walk();

    void pre_jump();
    void walk( bear::universe::time_type duration );

    void apply_walk();
    void apply_jump();
    void choose_idle_action();

    bool scan_no_wall_in_direction
    ( const bear::universe::position_type& origin,
      const bear::universe::vector_type& dir ) const;

    bool test_in_sky();
    void try_to_move();
    bool can_move_forward() const;

    void create_floating_score( unsigned int index ) const;

    static void init_exported_methods();

  private:
    /** \brief Current progress function. */
    progress_function_type m_progress;

    /** \brief Initial position. */
    bear::universe::position_type m_initial_position;

    /** \brief The maximum distance of the initial position. */
    bear::universe::coordinate_type m_max_distance;

    /** \brief Indicates if the rabbit is injured. */
    bool m_injured;

    /** \brief How long the opacity effect has been applied. */
    bear::universe::time_type m_opacity_effect_duration;

    /** \brief The opacity applied to the sprites when the rabbit is injured. */
    double m_opacity_injured;

    /** \brief The increment added at each iteration to m_opacity_injured. */
    double m_opacity_inc;

    /** \brief Indicates if the rabbit has a carrot. */
    bool m_has_carrot;

    /** \brief The remaining time to spend in the current action. */
    bear::universe::time_type m_remaining_action_time;

    /** \brief Tell if the rabbit is a marionette, in which case he won't decide
        is actions himself. */
    bool m_marionette;

  }; // class rabbit
} // namespace ptb

#endif // __PTB_RABBIT_HPP__

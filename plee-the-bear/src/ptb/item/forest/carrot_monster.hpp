/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file carrot_monster.hpp
 * \brief The class describing a carrot monster.
 * \author S�bastien Angibaud
 */
#ifndef __PTB_CARROT_MONSTER_HPP__
#define __PTB_CARROT_MONSTER_HPP__

#include "engine/base_item.hpp"
#include "ptb/monster_item.hpp"
#include "engine/model.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a carrot monster.
   * \author S�bastien Angibaud
   */
  class carrot_monster:
    public monster_item< bear::engine::model<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(carrot_monster);

  public:
    /** \brief The type of the parent class. */
    typedef monster_item< bear::engine::model<bear::engine::base_item> > super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  private:
    /** \brief The type of the functions used to do the progression of the
        item. */
    typedef void
    (carrot_monster::*progress_function_type)(bear::universe::time_type);

  public:
    carrot_monster();

    void build();
    void progress( bear::universe::time_type elapsed_time );

    void leaves_active_region();
    bool has_attack_point() const;

  private:
    void start_attack();
    void start_crush();
    void progress_attack( bear::universe::time_type elapsed_time );

    static void init_exported_methods();

  private:
    /** \brief Current progress function. */
    progress_function_type m_progress;

  }; // class carrot_monster
} // namespace ptb

#endif // __PTB_CARROT_MONSTER_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_box.hpp
 * \brief A bonus that Plee can take by slaping.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_BONUS_BOX_HPP__
#define __PTB_BONUS_BOX_HPP__

#include "ptb/base_bonus.hpp"

namespace ptb
{
  /**
   * \brief A bonus box.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class bonus_box:
    public base_bonus
  {
    DECLARE_BASE_ITEM(bonus_box);

  public:
    /** \brief The type of the parent class. */
    typedef ptb::base_bonus super;

  public:
    void pre_cache();
    void build();

  private:
    void collision_check_and_apply
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void create_broken_bonus_box();
    void create_broken_glass(bool left_orientation);

    void do_set_type(base_bonus_type t);
    void create_honeypot_decoration();
    void create_level_bonus();
  }; // class bonus_box
} // namespace ptb

#endif // __PTB_BONUS_BOX_HPP__

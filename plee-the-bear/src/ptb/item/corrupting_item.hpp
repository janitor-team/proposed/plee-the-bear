/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file corrupting_item.hpp
 * \brief This item corrupts a boss.
 * \author Julien Jorge
 */
#ifndef __PTB_CORRUPTING_ITEM_HPP__
#define __PTB_CORRUPTING_ITEM_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief This item corrupts a boss.
   *
   * The custom fields of this item are:
   * - any field supported by the parent class.
   *
   * \author Julien Jorge
   */
  class corrupting_item:
    public bear::engine::item_with_decoration
  < bear::engine::basic_renderable_item<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(corrupting_item);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::item_with_decoration
    < bear::engine::basic_renderable_item<bear::engine::base_item> > super;

  public:
    corrupting_item( double amount = 0 );

    double get_amount() const;
    void set_amount( double a );

    void progress( bear::universe::time_type elapsed_time );

  private:
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    /** \brief The amount of curruption passed to the boss. */
    double m_amount;

  }; // class corrupting_item
} // namespace ptb

#endif // __PTB_CORRUPTING_ITEM_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item/floating_score.hpp
 * \brief An item that display a score given to a player.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_FLOATING_SCORE_HPP__
#define __PTB_FLOATING_SCORE_HPP__

#include "generic_items/decorative_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that display a score given to a player.
   * \author Julien Jorge
   */
  class floating_score:
    public bear::decorative_item
  {
    DECLARE_BASE_ITEM(floating_score);

  public:
    /** \brief The type of the parent class. */
    typedef bear::decorative_item super;

  public:
    floating_score();

    void pre_cache();

    void build();

    void add_points( unsigned int player_index, unsigned int score );
    void one_up( unsigned int player_index );
    void set_score( const std::string& text);

  private:
    void create_effect();
    void give_one_up_to( unsigned int player_index ) const;

  }; // class floating_score
} // namespace ptb

#endif // __PTB_FLOATING_SCORE_HPP__

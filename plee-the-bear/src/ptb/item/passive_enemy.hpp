/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file passive_enemy.hpp
 * \brief The class describing a passive enemy.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_PASSIVE_ENEMY_HPP__
#define __PTB_PASSIVE_ENEMY_HPP__

#include "ptb/monster_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"
#include "engine/item_brick/item_with_friction.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a passive enemy.
   *
   * The valid fields for this item are
   *  - \a type_right_side: \c (string) The type of right side
   * possible values: inactive, solid, attack.
   *  - \a type_left_side: \c (string) The type of left side
   * possible values: inactive, solid, attack.
   *  - \a type_bottom_side: \c (string) The type of bottom side
   * possible values: inactive, solid, attack.
   *  - \a type_top_side: \c (string) The type of top side
   * possible values: inactive, solid, attack.
   *
   * \author Sébastien Angibaud
   */
  class passive_enemy:
    public monster_item
    < bear::engine::item_with_friction
      < bear::engine::item_with_decoration
        < bear::engine::basic_renderable_item<bear::engine::base_item> >
      >
    >
  {
    DECLARE_BASE_ITEM(passive_enemy);

  public:
    /** \brief The type of the parent class. */
    typedef monster_item
    < bear::engine::item_with_friction
      < bear::engine::item_with_decoration
      < bear::engine::basic_renderable_item <bear::engine::base_item> >
      >
    > super;

  public:
     enum side_type
      {
        inactive_type = 0,
        solid_type,
        attack_type
      }; // enum side_type

  public:
    passive_enemy();

    void collision( bear::engine::base_item& that,
        bear::universe::collision_info& info );

    bool set_string_field( const std::string& name, const std::string& value );

  private:
    bool set_side_type(side_type& side, const std::string& value);

  private:
    /* \brief The type of the right side. */
    side_type m_right_side_type;

    /* \brief The type of the left side. */
    side_type m_left_side_type;

    /* \brief The type of the top side. */
    side_type m_top_side_type;

    /* \brief The type of the bottom side. */
    side_type m_bottom_side_type;
  }; // class passive_enemy
} // namespace ptb

#endif // __PTB_PASSIVE_ENEMY_HPP__

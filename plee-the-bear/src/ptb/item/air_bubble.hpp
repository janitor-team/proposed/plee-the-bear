/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file air_bubble.hpp
 * \brief The class describing a air bubble.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_AIR_BUBBLE_HPP__
#define __PTB_AIR_BUBBLE_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing a air bubble.
   * \author Sébastien Angibaud
   */
  class air_bubble:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(air_bubble);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    air_bubble();

    void progress( bear::universe::time_type elapsed_time );
    void get_visual( std::list<bear::engine::scene_visual>& visuals ) const;
    void pre_cache();
    void build();
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );
    void set_oxygen(double oxygen);
    void give_max_size();
    void leaves_active_region();
    void update_size( bear::universe::time_type elapsed_time );

  private:
    /** \brief The oxygen quantity of the air_bubble. */
    double m_oxygen;

    /** \brief The max_size according its oxygen capacity. */
    double m_max_size;

    /** \brief The animation of the halo. */
    bear::visual::sprite m_sprite;

    /** \brief The maximum size of the bubble. */
    static const bear::universe::size_type s_max_size;

    /** \brief The minimu size of the bubble. */
    static const bear::universe::size_type s_min_size;

    /** \brief The oxygen in a maximal bubble. */
    static const double s_oxygen_in_max_size;
  }; // class air_bubble
} // namespace ptb

#endif // __PTB_AIR_BUBBLE_HPP__

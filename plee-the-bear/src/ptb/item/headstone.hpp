/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file headstone.hpp
 * \brief A headstone appears when a monster is killed.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_HEADSTONE_HPP__
#define __PTB_HEADSTONE_HPP__

#include "engine/base_item.hpp"
#include "engine/model.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A headstone appears when a monster is killed.
   * \author Sébastien Angibaud
   */
  class headstone:
    public bear::engine::model<bear::engine::base_item>
  {
    DECLARE_BASE_ITEM(headstone);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::model<bear::engine::base_item> super;

  public:
    headstone();

    void build();
    void progress( bear::universe::time_type elapsed_time );
    bool is_valid() const;

    void set_soul_energy(double soul_energy);
    void set_soul_animation( const bear::visual::animation& value );

  private:
    void create_soul() const;

  private:
    /** \brief Indicates if the soul has appeared. */
    bool m_soul_has_appeared;

    /** \brief The energy of the soul. */
    double m_soul_energy;

    /** \brief The soul animation. */
    bear::visual::animation m_soul_animation;

    /** \brief Indicates the time when headstone appears. */
    bear::universe::time_type m_time_start;

    /** \brief The time over which the soul appears.*/
    static const bear::universe::time_type s_time_to_appear;

  }; // class headstone
} // namespace ptb

#endif // __PTB_HEADSTONE_HPP__

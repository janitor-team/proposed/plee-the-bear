/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_stop_block.hpp
 * \brief An block that blocks only the players.
 * \author Julien Jorge
 */
#ifndef __PTB_PLAYER_STOP_BLOCK_HPP__
#define __PTB_PLAYER_STOP_BLOCK_HPP__

#include "generic_items/block.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An block that blocks only the players.
   * \author Julien Jorge
   */
  class player_stop_block:
    public bear::block
  {
    DECLARE_BASE_ITEM(player_stop_block);

    typedef bear::block super;

  protected:
    void collision_check_player_and_align
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    /** Tell if the player is aligned without setting contacts. */
    bool m_clear_contact;

  }; // class player_stop_block
} // namespace ptb

#endif // __PTB_PLAYER_STOP_BLOCK_HPP__

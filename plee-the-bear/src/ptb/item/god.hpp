/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file god.hpp
 * \brief The class describing God.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_GOD_HPP__
#define __PTB_GOD_HPP__

#include "ptb/monster_item.hpp"
#include "ptb/item_brick/item_that_speaks.hpp"
#include "engine/model.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The class describing God.
   * \author Sebastien Angibaud
   */
  class god :
    public item_that_speaks
      < monster_item
        < bear::engine::model
          < bear::engine::base_item> > >
  {
    DECLARE_BASE_ITEM(god);

  public:
    /** \brief The type of the parent class. */
    typedef
    item_that_speaks
      < monster_item
        < bear::engine::model
          < bear::engine::base_item > > > super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  private:
    typedef void (god::*progress_function_type)(bear::universe::time_type);

  public:
    god();

    void progress( bear::universe::time_type elapsed_time );
    void pre_cache();
    void build();

    void launch_ball
    ( bear::universe::time_type d, bear::universe::time_type t );

  private:
    void progress_idle( bear::universe::time_type elapsed_time );
    void progress_talk( bear::universe::time_type elapsed_time );

    void start_idle();
    void start_talk();

    void do_action(const std::string& a);
    void talk(const std::vector<std::string>& speech);

    static void init_exported_methods();

  private:
    /** \brief Current progress function. */
    progress_function_type m_progress;
   }; // class god
} // namespace ptb

#endif // __PTB_GOD_HPP__

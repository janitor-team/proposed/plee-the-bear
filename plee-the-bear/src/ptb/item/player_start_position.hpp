/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file player_start_position.hpp
 * \brief This class represent the position of a player when a level starts.
 * \author Julien Jorge
 */
#ifndef __PTB_PLAYER_START_POSITION_HPP__
#define __PTB_PLAYER_START_POSITION_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief This class represent the position of a player when a level starts.
   *
   * The custom fields of this class are:
   * - player_index: (unsigned int) the index of the player to create,
   * - exit_name: (string) the name of the exit associated with this entry
   *   point.
   *
   * The player is created if the last exit name saved in
   * game_variables::get_next_level_name() is equal to the field
   * \a player_start_position.exit_name OR if this field is empty.
   *
   * \author Julien Jorge
   */
  class player_start_position:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(player_start_position);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    player_start_position();

    bool set_u_integer_field( const std::string& name, unsigned int value );
    bool set_string_field( const std::string& name, const std::string& value );
    bool is_valid() const;

    void build();

  private:
    /** \brief The index of the player to create. */
    unsigned int m_player_index;

    /** \brief The name of the exit in the previous level, associated with this
        entry point. */
    std::string m_exit_name;

    /** \brief The type of the player : plee or ray. */
    std::string m_character;

  }; // class player_start_position
} // namespace ptb

#endif // __PTB_PLAYER_START_POSITION_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file small_honeypot.hpp
 * \brief A small_honeypot.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_SMALL_HONEYPOT_HPP__
#define __PTB_SMALL_HONEYPOT_HPP__

#include "ptb/base_bonus.hpp"

namespace ptb
{
  /**
   * \brief A small honeypot.
   *
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class small_honeypot:
    public base_bonus
  {
    DECLARE_BASE_ITEM(small_honeypot);

  public:
    /** \brief The type of the parent class. */
    typedef ptb::base_bonus super;

  public:
    small_honeypot();

    void pre_cache();

  private:
    void collision_check_and_apply
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void do_set_type(base_bonus_type t);

  }; // class small_honeypot
} // namespace ptb

#endif // __PTB_SMALL_HONEYPOT_HPP__

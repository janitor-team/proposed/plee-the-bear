/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_all_dead_by_category.hpp
 * \brief An item that adds points if all players_detector of given category
 * are dead.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_BONUS_ALL_DEAD_BY_CATEGORY_HPP__
#define __PTB_BONUS_ALL_DEAD_BY_CATEGORY_HPP__

#include "ptb/item/bonus_points/bonus_points.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that adds points if all players_detector of given category
   * are dead.
   * The valid fields for this item are
   *  - \a category:
   *  \c (string) (required) The category of players_detector to consider,
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class bonus_all_dead_by_category:
    public bonus_points
  {
    DECLARE_BASE_ITEM(bonus_all_dead_by_category);

  private:
    /** \brief The type of the parent class. */
    typedef bonus_points super;

  public:
    bonus_all_dead_by_category();
    explicit bonus_all_dead_by_category
    ( const std::string& name, unsigned int points = 0 );

    void build();

    bool set_string_field
    ( const std::string& name, const std::string& value );
    void set_category(const std::string& category);
    bool check_all_dead();

  private:
    /** \brief The category of players_detector to consider. */
    std::string m_category;

  }; // class bonus_all_dead_by_category
} // namespace ptb

#endif // __PTB_BONUS_ALL_DEAD_BY_CATEGORY_HPP__

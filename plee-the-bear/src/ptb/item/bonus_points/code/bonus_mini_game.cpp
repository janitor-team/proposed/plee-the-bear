/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_mini_game.cpp
 * \brief Implementation of the ptb::bonus_mini_game class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/bonus_points/bonus_mini_game.hpp"

#include "ptb/game_variables.hpp"

#include "expr/boolean_function.hpp"

#include <libintl.h>

BASE_ITEM_EXPORT( bonus_mini_game, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::bonus_mini_game::bonus_mini_game()
  : super("Mini-game unlocked", 10000)
{

} // bonus_mini_game::bonus_mini_game()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::bonus_mini_game::build()
{
  super::build();

  m_already_unlocked = game_variables::get_level_object_state(get_bonus_id());

  set_condition
    ( bear::expr::boolean_function_maker
      ( this, std::mem_fun_ref( &ptb::bonus_mini_game::check ) ) );
} // bonus_mini_game::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the bonus must be given.
 */
bool ptb::bonus_mini_game::check() const
{
  return ( !m_already_unlocked ) &&
    game_variables::get_current_level_mini_game_state();
} // bonus_mini_game::check()

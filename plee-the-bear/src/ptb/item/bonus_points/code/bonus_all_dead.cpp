/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_all_dead.cpp
 * \brief Implementation of the ptb::bonus_all_dead class.
 * \author Julien Jorge
 */
#include "ptb/item/bonus_points/bonus_all_dead.hpp"

#include "engine/expr/count_items_by_class_name.hpp"
#include "expr/linear_constant.hpp"

#include <libintl.h>

BASE_ITEM_EXPORT( bonus_all_dead, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::bonus_all_dead::bonus_all_dead()
: super()
{

} // bonus_all_dead::bonus_all_dead()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param name The name of the bonus.
 * \param points The number of points given for this bonus.
 */
ptb::bonus_all_dead::bonus_all_dead
( const std::string& name, unsigned int points )
  : super(name, points)
{

} // bonus_all_dead::bonus_all_dead()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::bonus_all_dead::build()
{
  super::build();

  bear::expr::linear_expression count( bear::expr::linear_constant(0) );
  std::list<std::string>::const_iterator it;

  for ( it=m_class_names.begin(); it!=m_class_names.end(); ++it )
    count += bear::engine::count_items_by_class_name( *this, *it );

  set_condition( count == 0 );
} // bonus_all_dead::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <list of items>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::bonus_all_dead::set_item_list_field
( const std::string& name, const std::vector<bear::engine::base_item*>& value )
{
  bool result(true);

  if ( name == "bonus_all_dead.class_instances" )
    for ( std::size_t i=0; i!=value.size(); ++i )
      m_class_names.push_back( value[i]->get_class_name() );
  else
    result = super::set_item_list_field( name, value );

  return result;
} // bonus_all_dead::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <list of items>.
 * \param class_names The names of classes to check.
 */
void ptb::bonus_all_dead::set_class_names
(const std::list<std::string>& class_names)
{
  m_class_names = class_names;
} // bonus_all_dead::set_class_names()

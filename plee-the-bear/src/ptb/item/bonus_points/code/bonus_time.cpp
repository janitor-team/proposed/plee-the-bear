/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_time.cpp
 * \brief Implementation of the ptb::bonus_time class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/bonus_points/bonus_time.hpp"
#include "ptb/level_variables.hpp"
#include "expr/linear_function.hpp"

#include <libintl.h>

BASE_ITEM_EXPORT( bonus_time, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::bonus_time::bonus_time()
: super("Time bonus"), m_timer(NULL), m_points_per_second(10)
{

} // bonus_time::bonus_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is valid.
 */
bool ptb::bonus_time::is_valid() const
{
  return (m_timer != (bear::timer*)NULL) && super::is_valid();
} // bonus_time::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::bonus_time::build()
{
  super::build();

  set_condition
    ( bear::expr::linear_function_maker
      ( m_timer, std::mem_fun_ref(&bear::timer::get_loops) ) == 0 );
  set_points
    ( bear::expr::linear_function_maker
      ( m_timer, std::mem_fun_ref(&bear::timer::get_time) )
      * m_points_per_second );
} // bonus_time::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::bonus_time::set_item_field
( const std::string& name, bear::engine::base_item* value )
{
  bool result(true);

  if ( name == "bonus_time.level_timer" )
    m_timer = value;
  else
    result = super::set_item_field( name, value );

  return result;
} // bonus_time::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c unsigned int.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::bonus_time::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result = true;

  if (name == "bonus_time.points_per_second")
    m_points_per_second = value;
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // bonus_time::set_u_intger_field()

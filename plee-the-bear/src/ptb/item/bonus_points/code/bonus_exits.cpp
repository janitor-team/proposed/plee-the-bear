/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_exits.cpp
 * \brief Implementation of the ptb::bonus_exits class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/bonus_points/bonus_exits.hpp"
#include "ptb/game_variables.hpp"
#include "expr/boolean_function.hpp"

#include <libintl.h>

BASE_ITEM_EXPORT( bonus_exits, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::bonus_exits::bonus_exits()
: super("Different exits", 10000)
{

} // bonus_exits::bonus_exits()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::bonus_exits::build()
{
  if ( game_variables::get_players_count() > 1 )
    {
      set_picture_filename("gfx/ui/bonus-icons/bonus-icons.png");
      set_picture_name("exits");

      set_condition
        ( bear::expr::boolean_function_maker
          ( this, std::mem_fun_ref( &ptb::bonus_exits::different_exits ) ) );

      super::build();
    }
  else
    kill();
} // bonus_exits::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if players used different exits.
 */
bool ptb::bonus_exits::different_exits()
{
  return game_variables::get_last_level_exit
    ( game_variables::get_next_level_name(), 1 ) !=
    game_variables::get_last_level_exit
    ( game_variables::get_next_level_name(), 2 ) ;
} // bonus_exits::different_exits()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_time_record.cpp
 * \brief Implementation of the ptb::bonus_time_record class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/bonus_points/bonus_time_record.hpp"

#include "ptb/level_variables.hpp"
#include "expr/linear_function.hpp"
#include <libintl.h>

BASE_ITEM_EXPORT( bonus_time_record, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::bonus_time_record::bonus_time_record()
  : super("Rocket bonus", 10000), m_timer(NULL), m_time_record(0)
{
  set_picture_filename("gfx/ui/bonus-icons/bonus-icons.png");
  set_picture_name("time");
} // bonus_time_record::bonus_time_record()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is valid.
 */
bool ptb::bonus_time_record::is_valid() const
{
  return (m_timer != (bear::timer*)NULL) && super::is_valid();
} // bonus_time_record::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::bonus_time_record::build()
{
  super::build();

  set_condition
    ( ( bear::expr::linear_function_maker
        ( m_timer, std::mem_fun_ref(&bear::timer::get_loops) ) == 0 )
      && ( bear::expr::linear_function_maker
           ( m_timer, std::mem_fun_ref(&bear::timer::get_elapsed_time) )
           <= m_time_record ) );
} // bonus_time_record::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::bonus_time_record::set_item_field
( const std::string& name, bear::engine::base_item* value )
{
  bool result(true);

  if ( name == "bonus_time_record.level_timer" )
    m_timer = value;
  else
    result = super::set_item_field( name, value );

  return result;
} // bonus_time_record::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c real.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool ptb::bonus_time_record::set_real_field
( const std::string& name, double value )
{
  bool result = true;

  if (name == "bonus_time_record.time_record")
    m_time_record = value;
  else
    result = super::set_real_field(name, value);

  return result;
} // bonus_time_record::set_real_field()

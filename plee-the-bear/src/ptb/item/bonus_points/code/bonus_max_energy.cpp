/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_max_energy.cpp
 * \brief Implementation of the ptb::bonus_max_energy class.
 * \author Sebastien Angibaud
 */
#include "ptb/item/bonus_points/bonus_max_energy.hpp"

#include "ptb/game_variables.hpp"

#include "expr/boolean_function.hpp"

#include <libintl.h>

BASE_ITEM_EXPORT( bonus_max_energy, ptb )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::bonus_max_energy::bonus_max_energy()
  : super("Energy bonus found", 10000)
{

} // bonus_max_energy::bonus_max_energy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void ptb::bonus_max_energy::build()
{
  set_picture_filename("gfx/ui/bonus-icons/bonus-icons.png");
  set_picture_name("energy box");

  super::build();

  m_already_found = game_variables::get_current_level_max_energy_state();

  set_condition
    ( bear::expr::boolean_function_maker
      ( this, std::mem_fun_ref( &ptb::bonus_max_energy::check ) ) );
} // bonus_max_energy::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the bonus must be given.
 */
bool ptb::bonus_max_energy::check() const
{
  return ( !m_already_found ) &&
    game_variables::get_current_level_max_energy_state();
} // bonus_max_energy::check()

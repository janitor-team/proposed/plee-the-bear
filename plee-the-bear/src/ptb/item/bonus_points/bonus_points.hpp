/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_points.hpp
 * \brief A class describing an item that adds point at the end of the level.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_BONUS_POINTS_HPP__
#define __PTB_BONUS_POINTS_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/with_expression_assignment.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A class describing an item that adds point at the end of the level.
   *
   * The valid fields for this item are:
   *  - \a name: (string) the name of given points,
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class bonus_points:
    public bear::engine::base_item,
    public bear::engine::with_boolean_expression_assignment,
    public bear::engine::with_linear_expression_assignment
  {
    DECLARE_BASE_ITEM(bonus_points);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    bonus_points();
    explicit bonus_points( const std::string& name, unsigned int points = 0 );

    void build();
    bool set_string_field(const std::string& name, const std::string& value);
    bool set_u_integer_field( const std::string& name, unsigned int value );

    const std::string& get_name() const;
    unsigned int get_points() const;
    const std::string& get_bonus_id() const;
    void update_bonus_state() const;

    void set_points( const bear::expr::linear_expression& e );
    void set_condition( const bear::expr::boolean_expression& e );
    bool is_level_bonus() const;
    void set_picture_filename( const std::string& filename );
    const std::string& get_picture_filename() const;
    void set_picture_name( const std::string& name );
    const std::string& get_picture_name() const;

  private:
    void do_set_expression( const bear::expr::boolean_expression& e );
    void do_set_expression( const bear::expr::linear_expression& e );
    void update_bonus() const;

  private:
    /* \brief The identifier of the bonus. */
    std::string m_bonus_id;

    /* \brief The name of the bonus. */
    std::string m_bonus_name;

    /* \brief Number of points to give. */
    bear::expr::linear_expression m_bonus_points;

    /* \brief The expression to check. */
    bear::expr::boolean_expression m_condition;

    /* \brief The filename of the bonus picture. */
    std::string m_picture_filename;

    /* \brief The name of the bonus picture in spritepos file. */
    std::string m_picture_name;

  }; // class bonus_points
} // namespace ptb

#endif // __PTB_BONUS_POINTS_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_time_record.hpp
 * \brief An item that adds point if the level is
 * finished under a record time.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_BONUS_TIME_RECORD_HPP__
#define __PTB_BONUS_TIME_RECORD_HPP__

#include "ptb/item/bonus_points/bonus_points.hpp"
#include "generic_items/timer.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that adds point if the level is
   * finished under a record time.
   *
   * The valid fields for this item are
   *  - time_record:
   *   \c (real) The time record for the level.
   *  - level_timer:
   *   \c (item) The timer to use.
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class bonus_time_record:
    public bonus_points
  {
    DECLARE_BASE_ITEM(bonus_time_record);

  private:
    /** \brief The type of the parent class. */
    typedef bonus_points super;

    /** \brief The type of a handle on a timer. */
    typedef bear::universe::const_derived_item_handle<bear::timer> timer_handle;

  public:
    bonus_time_record();

    bool is_valid() const;
    void build();

    bool
    set_item_field( const std::string& name, bear::engine::base_item* value );
    bool set_real_field( const std::string& name, double value );

  private:
    /** \brief The timer used to compute the time bonuses. */
    timer_handle m_timer;

    /** \brief The time under which points are given. */
    bear::universe::time_type m_time_record;

  }; // class bonus_time_record
} // namespace ptb

#endif // __PTB_BONUS_TIME_RECORD_HPP__

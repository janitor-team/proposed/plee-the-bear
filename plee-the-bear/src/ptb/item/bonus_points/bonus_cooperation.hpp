/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_cooperation.hpp
 * \brief An item that gives bonus if players discover cooperation area.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_BONUS_COOPERATION_HPP__
#define __PTB_BONUS_COOPERATION_HPP__

#include "ptb/item/bonus_points/bonus_all_dead_by_category.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that gives bonus if players discover cooperation area.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class bonus_cooperation:
    public bonus_all_dead_by_category
  {
    DECLARE_BASE_ITEM(bonus_cooperation);

  public:
    /** \brief The type of the parent class. */
    typedef bonus_all_dead_by_category super;

  public:
    bonus_cooperation();

    void build();
  }; // class bonus_cooperation
} // namespace ptb

#endif // __PTB_BONUS_COOPERATION_HPP__

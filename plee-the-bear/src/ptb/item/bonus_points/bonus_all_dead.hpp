/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_all_dead.hpp
 * \brief An item that adds points if all items of given classes are dead.
 * \author Julien Jorge
 */
#ifndef __PTB_BONUS_ALL_DEAD_HPP__
#define __PTB_BONUS_ALL_DEAD_HPP__

#include "ptb/item/bonus_points/bonus_points.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that adds points if all items of given classes are dead.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class bonus_all_dead:
    public bonus_points
  {
    DECLARE_BASE_ITEM(bonus_all_dead);

  private:
    /** \brief The type of the parent class. */
    typedef bonus_points super;

  public:
    bonus_all_dead();
    explicit bonus_all_dead
    ( const std::string& name, unsigned int points = 0 );

    void build();

    bool set_item_list_field
    ( const std::string& name,
      const std::vector<bear::engine::base_item*>& value );
    void set_class_names(const std::list<std::string>& class_names);

  private:
    /** \brief The class names of all the items that have to be dead. */
    std::list<std::string> m_class_names;

  }; // class bonus_all_dead
} // namespace ptb

#endif // __PTB_BONUS_ALL_DEAD_HPP__

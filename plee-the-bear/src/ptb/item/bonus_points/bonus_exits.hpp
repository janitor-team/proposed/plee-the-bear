/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_exits.hpp
 * \brief An item that gives bonus if players use different exits.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_BONUS_EXITS_HPP__
#define __PTB_BONUS_EXITS_HPP__

#include "ptb/item/bonus_points/bonus_points.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that gives bonus if players use different exits.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class bonus_exits:
    public bonus_points
  {
    DECLARE_BASE_ITEM(bonus_exits);

  public:
    /** \brief The type of the parent class. */
    typedef bonus_points super;

  public:
    bonus_exits();

    void build();
    bool different_exits();
  }; // class bonus_exits
} // namespace ptb

#endif // __PTB_BONUS_EXITS_HPP__

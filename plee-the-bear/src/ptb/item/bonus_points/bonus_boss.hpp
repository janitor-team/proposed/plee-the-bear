/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file bonus_boss.hpp
 * \brief An item that adds points if the boss has been killed.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_BONUS_BOSS_HPP__
#define __PTB_BONUS_BOSS_HPP__

#include "ptb/item/bonus_points/bonus_points.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that adds points if the boss has been killed.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class bonus_boss:
    public bonus_points
  {
    DECLARE_BASE_ITEM(bonus_boss);

  public:
    /** \brief The type of the parent class. */
    typedef bonus_points super;

  public:
    bonus_boss();

    void build();

  private:
    bool check() const;

  private:
    /* \brief Indicates if the boss has already been killed. */
    bool m_already_killed;
  }; // class bonus_boss
} // namespace ptb

#endif // __PTB_BONUS_BOSS_HPP__

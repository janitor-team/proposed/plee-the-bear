/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item/level_popper.hpp
 * \brief An item that restore the last level pushed in the engine.
 * \author Julien Jorge
 */
#ifndef __PTB_LEVEL_POPPER_HPP__
#define __PTB_LEVEL_POPPER_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item that restore the last level pushed in the engine.
   *
   * This item has no custom fields.
   *
   * \sa level_pusher
   * \author Julien Jorge
   */
  class level_popper:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(level_popper);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    level_popper();

    void progress( bear::universe::time_type elapsed_time );

  private:
    void collision_check_and_apply
    ( bear::engine::base_item& that, bear::universe::collision_info& info );
    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    /** \brief How many players are colliding with this item. */
    std::size_t m_players_count;

    /** \brief Tell if the level has been popped. */
    bool m_applied;

  }; // class level_popper
} // namespace ptb

#endif // __PTB_LEVEL_POPPER_HPP__

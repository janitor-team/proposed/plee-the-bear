/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file soul.hpp
 * \brief The soul appears out of a headstone. The player can take it to get
 *        more energy.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_SOUL_HPP__
#define __PTB_SOUL_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief The soul appears out of a headstone. The player can take it to get
   *        more energy.
   * \author Sébastien Angibaud
   */
  class soul:
    public bear::engine::item_with_decoration
    < bear::engine::basic_renderable_item<bear::engine::base_item> >
  {
    DECLARE_BASE_ITEM(soul);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::item_with_decoration
    < bear::engine::basic_renderable_item<bear::engine::base_item> > super;

  public:
    soul();
    ~soul();

    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

    void set_energy(double value);

    void leaves_active_region();

  private:
    /** \brief The energy of the soul. */
    double m_energy;

  }; // class soul
} // namespace ptb

#endif // __PTB_SOUL_HPP__

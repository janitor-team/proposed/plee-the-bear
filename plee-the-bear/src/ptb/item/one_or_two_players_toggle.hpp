/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file one_or_two_players_toggle.hpp
 * \brief A toggle that applies its status to a given toggle if we are playing a
 *        single player game, and to an other toggle otherwise.
 * \author Julien Jorge
 */
#ifndef __PTB_ONE_OR_TWO_PLAYERS_TOGGLE_HPP__
#define __PTB_ONE_OR_TWO_PLAYERS_TOGGLE_HPP__

#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/base_item.hpp"
#include "universe/derived_item_handle.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A toggle that applies its status to a given toggle if we are playing
   *        a single player game, and to an other toggle otherwise.
   *
   * The custom fields of this class are:
   * - single_player_game (item), the item toggled in a single player game
   *   (default=none),
   * - two_players_game (item), the item toggled in a two players game
   *   (default=none),
   * - any field supported by the parent class.
   *
   * \author Julien Jorge
   */
  class one_or_two_players_toggle:
    public bear::engine::item_with_toggle
  < bear::engine::base_item >
  {
    DECLARE_BASE_ITEM(one_or_two_players_toggle);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::item_with_toggle
    < bear::engine::base_item > super;

  private:
    /** \brief The type of the handles on the toggles to activate. */
    typedef bear::universe::derived_item_handle<with_toggle> toggle_handle;

  public:
    one_or_two_players_toggle();

    bool set_item_field
    ( const std::string& name, bear::engine::base_item* value );

  private:
    void on_toggle_on(bear::engine::base_item *activator);
    void on_toggle_off(bear::engine::base_item *activator);

  private:
    /** \brief The item toggled in a single player game. */
    toggle_handle m_toggle_single;

    /** \brief The item toggled in a two players game. */
    toggle_handle m_toggle_coop;

  }; // class one_or_two_players_toggle
} // namespace ptb

#endif // __PTB_ONE_OR_TWO_PLAYERS_TOGGLE_HPP__

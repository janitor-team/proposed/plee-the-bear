/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file authorize_action_toggle.hpp
 * \brief A toggle that authorize or refuse an action to both players.
 * \author Julien Jorge
 */
#ifndef __PTB_AUTHORIZE_ACTION_TOGGLE_HPP__
#define __PTB_AUTHORIZE_ACTION_TOGGLE_HPP__

#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/base_item.hpp"
#include "ptb/player_action.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A toggle that authorize or refuse an action to both players.
   *
   * The custom fields of this class are:
   * - action (string, required), the name of the action ,
   * - value (bool), the state of the action (default=true),
   * - any field supported by the parent class.
   *
   * \author Sébastien Angibaud
   */
  class authorize_action_toggle:
    public bear::engine::item_with_toggle
  < bear::engine::base_item >
  {
    DECLARE_BASE_ITEM(authorize_action_toggle);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::item_with_toggle
    < bear::engine::base_item > super;

  public:
    authorize_action_toggle();

    bool set_string_field( const std::string& name, const std::string& value );
    bool set_bool_field( const std::string& name, bool value );

  private:
    void on_toggle_on(bear::engine::base_item *activator);

  private:
    /** \brief Indicates if the action is authorized. */
    bool m_value;

    /** \brief The action to set. */
    player_action::value_type m_action;
  }; // class authorize_action_toggle
} // namespace ptb

#endif // __PTB_AUTHORIZE_ACTION_TOGGLE_HPP__

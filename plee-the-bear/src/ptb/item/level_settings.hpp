/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file level_settings.hpp
 * \brief An item to set parameters of level.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_LEVEL_SETTINGS_HPP__
#define __PTB_LEVEL_SETTINGS_HPP__

#include "engine/base_item.hpp"
#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief An item to set parameters of level.
   *
   * The valid fields for this item are
   *  - \a is_main_level:
   *  \c (bool) Indicates if the level is a main level.
   *  - \a friendly_fire:
   *  \c (bool) Indicates if a player can harm each other.
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angibaud
   */
  class level_settings:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(level_settings);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    level_settings();

    void build();

    virtual bool set_bool_field( const std::string& name, bool value );

  private:
    /** \brief Indicates if the level is a main level. */
    bool m_is_main_level;
  }; // class level_settings
} // namespace ptb

#endif // __PTB_LEVEL_SETTINGS_HPP__

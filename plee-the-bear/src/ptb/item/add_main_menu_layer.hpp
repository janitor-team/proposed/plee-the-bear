/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file add_main_menu_layer.hpp
 * \brief This class adds the main menu in the graphical user interface in the
 *        current level.
 * \author Julien Jorge
 */
#ifndef __PTB_ADD_MAIN_MENU_LAYER_HPP__
#define __PTB_ADD_MAIN_MENU_LAYER_HPP__

#include "engine/base_item.hpp"
#include "engine/layer/gui_layer.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief This class adds the main menu in the graphical user interface in the
   *        current level.
   *
   * \author Julien Jorge
   */
  class add_main_menu_layer:
    public bear::engine::base_item
  {
    DECLARE_BASE_ITEM(add_main_menu_layer);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    add_main_menu_layer();

    void pre_cache();
    void build();

  }; // class add_main_menu_layer
} // namespace ptb

#endif // __PTB_ADD_MAIN_MENU_LAYER_HPP__

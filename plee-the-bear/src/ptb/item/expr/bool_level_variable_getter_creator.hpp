/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file bool_level_variable_getter_creator.hpp
 * \brief A boolean expression that returns the value of a persistent level
 *        variable
 * \author Julien Jorge
 */
#ifndef __PTB_BOOL_LEVEL_VARIABLE_GETTER_CREATOR_HPP__
#define __PTB_BOOL_LEVEL_VARIABLE_GETTER_CREATOR_HPP__

#include "engine/base_item.hpp"
#include "engine/function/bool_game_variable_getter.hpp"
#include "engine/item_brick/with_boolean_expression_creation.hpp"

#include "engine/export.hpp"

namespace ptb
{
  /**
   * \brief A boolean expression that returns the value of a persistent level
   *        variable
   *
   * The valid fields for this item are
   *  - name: The name of the variable to get,
   *  - default_value: The default value of the variable, if not set,
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class bool_level_variable_getter_creator:
    public bear::engine::base_item,
    public bear::engine::with_boolean_expression_creation
  {
    DECLARE_BASE_ITEM(bool_level_variable_getter_creator);

  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::base_item super;

  public:
    bool_level_variable_getter_creator();

    void build();
    bool is_valid() const;

    bool set_string_field
    ( const std::string& name, const std::string& value );
    bool set_bool_field( const std::string& name, bool value );

  private:
    virtual bear::expr::boolean_expression do_get_expression() const;

  private:
    /** \brief The expression created by this item. */
    bear::engine::bool_game_variable_getter m_expr;

  }; // class bool_level_variable_getter_creator
} // namespace ptb

#endif // __PTB_BOOL_LEVEL_VARIABLE_GETTER_CREATOR_HPP__

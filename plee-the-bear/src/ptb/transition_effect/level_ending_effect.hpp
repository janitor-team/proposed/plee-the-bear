/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file level_ending_effect.hpp
 * \brief The effect displayed at the introduction of the levels.
 * \author Julien Jorge
 */
#ifndef __PTB_LEVEL_ENDING_EFFECT_HPP__
#define __PTB_LEVEL_ENDING_EFFECT_HPP__

#include "engine/transition_effect/transition_effect.hpp"
#include "engine/world.hpp"
#include "visual/writing.hpp"

namespace ptb
{
  class horizontal_gauge;

  /**
   * \brief The effect displayed at the introduction of the levels.
   * \author Julien Jorge
   */
  class level_ending_effect:
    public bear::engine::transition_effect
  {
  public:
    /** \brief The type of a list of scene elements retrieved from the layer.*/
    typedef std::list<bear::visual::scene_element> scene_element_list;

  private:
    /** \brief A line of points displayed on the screen. */
    class score_line
    {
    public:
      score_line
      ( bear::engine::level_globals& glob, const bear::visual::font& f,
        const std::string& text, unsigned int points,
        const std::string& picture_filename, const std::string& picture_name );

      void render
      ( scene_element_list& e, bear::visual::coordinate_type left,
        bear::visual::coordinate_type right ) const;

      unsigned int get_score() const;
      unsigned int decrease_score( unsigned int delta );
      bear::visual::coordinate_type get_height() const;

      double get_y_position() const;
      void set_y_position( double y );

      bear::universe::time_type get_time() const;
      void add_time( bear::universe::time_type t );

    private:
      /** \brief The font used for the writings. */
      bear::visual::font m_font;

      /** \brief Explain the reason of the points. */
      bear::visual::writing m_label;

      /** \brief The remaining points, as a text. */
      bear::visual::writing m_points_text;

      /** \brief The sprite of the bonus picture. */
      bear::visual::sprite m_bonus_sprite;

      /** \brief The remaining points. */
      unsigned int m_points;

      /** \brief The y-position of the score. */
      bear::visual::coordinate_type m_y;

      /** \brief How long the line has been displayed. */
      bear::universe::time_type m_time;

      /** \brief A delta applied to the text to create shadows. */
      static const bear::visual::coordinate_type s_shadow_delta;

      /** \brief Margin on the right of bonus picture. */
      static const bear::visual::coordinate_type s_bonus_picture_margin;

      /** \brief The scale factor applied to the text. */
      static const double s_scale_factor;

    }; // class score_line

  public:
    level_ending_effect();
    level_ending_effect( const level_ending_effect& that );
    ~level_ending_effect();

    void set_world( const bear::engine::world* w );

    bool is_finished() const;
    void build();
    bear::universe::time_type
    progress( bear::universe::time_type elapsed_time );
    void render( scene_element_list& e ) const;

  private:
    void create_controls();
    void fill_controls();

    bool key_pressed( const bear::input::key_info& key );
    bool key_maintained( const bear::input::key_info& key );
    bool button_pressed
    ( bear::input::joystick::joy_code button, unsigned int joy_index );
    bool button_maintained
    ( bear::input::joystick::joy_code button, unsigned int joy_index );
    void check_new_try(unsigned int s);

    void fill_points();
    void update_bonus_state();

    bool update_lines( bear::universe::time_type elapsed_time );
    void update_score_bar( bear::universe::time_type elapsed_time );
    void update_tick( bear::universe::time_type elapsed_time );

    // not implemented
    level_ending_effect& operator=( const level_ending_effect& that );

  private:
    /** \brief The remaining points to give. */
    std::list<score_line> m_points;

    /** \brief A factor applied to s_points_per_second. */
    double m_speed_factor;

    /** \brief The date at which we emit the next tick sound. */
    bear::universe::time_type m_next_tick;

    /** \brief The bar displaying the sum of the players' points. */
    horizontal_gauge* m_score_bar;

    /** \brief The world where we search for the bonus points. */
    const bear::engine::world* m_world;

    /** \brief How many points are given per second. */
    static const unsigned int s_points_per_second;

    /** \brief The default margin around the score lines. */
    static const bear::visual::coordinate_type s_screen_margin;

    /** \brief The margin between two score lines. */
    static const bear::visual::coordinate_type s_margin;

    /** \brief The speed in screen unit/sec. of the movement of a score line. */
    static const double s_score_line_speed;

  }; // class level_ending_effect
} // namespace ptb

#endif // __PTB_LEVEL_ENDING_EFFECT_HPP__

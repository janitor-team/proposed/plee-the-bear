/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file invincibility_effect.cpp
 * \brief Implementation of the ptb::invincibility_effect class.
 * \author Julien Jorge
 */
#include "ptb/transition_effect/invincibility_effect.hpp"

#include "engine/level_globals.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::invincibility_effect::invincibility_effect()
  : m_music_id(0)
{

} // invincibility_effect::invincibility_effect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 * \param that The instance to copy from.
 */
ptb::invincibility_effect::invincibility_effect
( const invincibility_effect& that )
  : fade_effect(that), m_music_id(0)
{

} // invincibility_effect::invincibility_effect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::invincibility_effect::~invincibility_effect()
{
  if (m_music_id!=0)
    get_level_globals().stop_music(m_music_id);
} // invincibility_effect::~invincibility_effect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assignment.
 * \param that The instance to copy from.
 */
ptb::invincibility_effect&
ptb::invincibility_effect::operator=( const invincibility_effect& that )
{
  super::operator=(that);
  return *this;
} // invincibility_effect::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the effect.
 */
void ptb::invincibility_effect::build()
{
  super::build();
  set_color(1, 0.5, 0);
  set_opacity(0.25);
  set_duration(1, get_total_duration() - 2, 1);

  m_music_id = get_level_globals().play_music("music/invincibility.ogg", 1);
} // invincibility_effect::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the default duration of the invincibility.
 * \remark The duration must match the duration of the music.
 */
bear::universe::time_type ptb::invincibility_effect::get_total_duration()
{
  return 20; // 20 seconds
} // invincibility_effect::get_total_duration()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file game_over_effect.cpp
 * \brief Implementation of the ptb::game_over_effect class.
 * \author Julien Jorge
 */
#include "ptb/transition_effect/game_over_effect.hpp"

#include "engine/game.hpp"
#include "engine/level.hpp"
#include "visual/scene_writing.hpp"

#include "ptb/util/player_util.hpp"

#include <libintl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the effect.
 */
void ptb::game_over_effect::build()
{
  super::build();
  set_color(0, 0, 0);
  set_duration(1, 10, 0);

  bear::visual::font fnt
    ( get_level_globals().get_font("font/level_name-42x50.fnt") );
  m_game_over.create(fnt, gettext("game over"));

  m_first_player = util::find_player( get_level_globals(), 1 );
  m_second_player = util::find_player( get_level_globals(), 2 );

  get_level_globals().stop_all_musics();
  get_level_globals().play_music("music/game-over.ogg", 1);
} // game_over_effect::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Adjust the components of the effect.
 * \param elapsed_time Elapsed time since the last call.
 */
bear::universe::time_type
ptb::game_over_effect::progress( bear::universe::time_type elapsed_time )
{
  const bear::universe::time_type result( super::progress(elapsed_time) );

  if ( is_finished() )
    bear::engine::game::get_instance().set_waiting_level
      ("level/title_screen.cl");

  return result;
} // game_over_effect::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the components of the effect.
 * \param e (out) The scene elements.
 */
void ptb::game_over_effect::render( scene_element_list& e ) const
{
  super::render(e);

  e.push_back
    ( bear::visual::scene_writing
      ( (get_layer().get_size().x - m_game_over.get_width()) / 2,
        m_game_over.get_height(),
        m_game_over ) );

  std::list<bear::engine::scene_visual> tmp;

  if ( m_first_player != NULL )
    m_first_player.get_visual(tmp);
  if ( m_second_player != NULL )
    m_second_player.get_visual(tmp);

  tmp.sort( bear::engine::scene_visual::z_position_compare() );

  for ( ; !tmp.empty(); tmp.pop_front() )
    e.push_back
      ( get_level().element_to_screen_coordinates(tmp.front().scene_element) );
} // game_over_effect::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the effect that a key had been pressed.
 * \param key The value of the pressed key.
 * \remark This effect pretends to treat all keys, so we can maintain the pause
 *         mode.
 */
bool ptb::game_over_effect::key_pressed( const bear::input::key_info& key )
{
  // prevent pausing the game
  return true;
} // game_over_effect::key_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the effect that a joystick button had been pressed.
 * \param button The value of the pressed button.
 * \param joy_index The index of the joystick.
 * \remark This effect pretends to treat all keys, so we can maintain the pause
 *         mode.
 */
bool ptb::game_over_effect::button_pressed
( bear::input::joystick::joy_code button, unsigned int joy_index )
{
  // prevent pausing the game
  return true;
} // game_over_effect::button_pressed()

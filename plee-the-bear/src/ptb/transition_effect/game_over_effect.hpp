/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file game_over_effect.hpp
 * \brief An effect that displays a text "game over" and start the title
 *        screen.
 * \author Julien Jorge
 */
#ifndef __PTB_GAME_OVER_EFFECT_HPP__
#define __PTB_GAME_OVER_EFFECT_HPP__

#include "ptb/player_proxy.hpp"

#include "visual/writing.hpp"
#include "engine/transition_effect/fade_effect.hpp"

namespace ptb
{
  /**
   * \brief An effect that displays a text "game over" and start the title
   *        screen.
   * \author Julien Jorge
   */
  class game_over_effect:
    public bear::engine::fade_effect
  {
  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::fade_effect super;

  public:
    void build();
    bear::universe::time_type
    progress( bear::universe::time_type elapsed_time );
    void render( scene_element_list& e ) const;

    bool key_pressed( const bear::input::key_info& key );
    bool button_pressed
    ( bear::input::joystick::joy_code button, unsigned int joy_index );

  private:
    /** \brief The text. */
    bear::visual::writing m_game_over;

    /** \brief Pointer on the first player. */
    player_proxy m_first_player;

    /** \brief Pointer on the second player. */
    player_proxy m_second_player;

  }; // class game_over_effect
} // namespace ptb

#endif // __PTB_GAME_OVER_EFFECT_HPP__

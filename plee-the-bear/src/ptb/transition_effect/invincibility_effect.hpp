/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file invincibility_effect.hpp
 * \brief The effect shown when the player is invincible.
 * \author Julien Jorge
 */
#ifndef __PTB_INVINCIBILITY_EFFECT_HPP__
#define __PTB_INVINCIBILITY_EFFECT_HPP__

#include "engine/transition_effect/fade_effect.hpp"

namespace ptb
{
  /**
   * \brief The effect shown when the player is invincible.
   * \author Julien Jorge
   */
  class invincibility_effect:
    public bear::engine::fade_effect
  {
  public:
    /** \brief The type of the parent class. */
    typedef bear::engine::fade_effect super;

  public:
    invincibility_effect();
    invincibility_effect( const invincibility_effect& that );
    ~invincibility_effect();

    invincibility_effect& operator=( const invincibility_effect& that );

    void build();

    static bear::universe::time_type get_total_duration();

  private:
    /** \brief The identifier of the music played. */
    std::size_t m_music_id;

  }; // class invincibility_effect
} // namespace ptb

#endif // __PTB_INVINCIBILITY_EFFECT_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file balloon_layer.hpp
 * \brief The balloon layer contains all balloons.
 * \author Angibaud Sebastien
 */
#ifndef __PTB_BALLOON_LAYER_HPP__
#define __PTB_BALLOON_LAYER_HPP__

#include "communication/messageable.hpp"
#include "engine/layer/gui_layer.hpp"
#include "universe/derived_item_handle.hpp"

#include <string>
#include <list>

namespace ptb
{
  class speaker_item;

  /**
   * \brief The balloon layer contains the balloons of the text said by the
   *        items.
   */
  class balloon_layer:
    public bear::communication::messageable,
    public bear::engine::gui_layer
  {
  public:
    /** \brief The type of a list of scene elements retrieved from the layer. */
    typedef bear::engine::gui_layer::scene_element_list scene_element_list;

    /** \brief The type of the pointers on the speaker items. */
    typedef bear::universe::derived_item_handle<speaker_item> handle_type;

    /** \brief The type of the list of all speakers. */
    typedef std::list<handle_type> speaker_list;

  public:
    balloon_layer( const std::string& name );

    void build();
    void progress( bear::universe::time_type elapsed_time );
    void render( scene_element_list& e ) const;

    void add_speaker( speaker_item* speaker );

  private:
    bear::universe::rectangle_type
    get_bounding_box_on_screen(handle_type& speaker) const;

  private:
    /** \brief List of speakers. */
    speaker_list m_speakers;

    /** \brief  The size of the border. */
    static const unsigned int s_border;

  }; // class balloon_layer
} // namespace ptb

#endif // __PTB_BALLOON_LAYER_HPP__

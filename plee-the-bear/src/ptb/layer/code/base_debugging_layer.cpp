/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file base_debugging_layer.cpp
 * \brief Implementation of the ptb::base_debugging_layer class.
 * \author Julien Jorge
 */
#include "ptb/layer/base_debugging_layer.hpp"

#include "engine/layer/layer.hpp"
#include "engine/level.hpp"
#include "engine/level_globals.hpp"
#include "engine/world.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param toggle_key The value of the key that changes the visibility of the
 *        layer.
 */
ptb::base_debugging_layer::base_debugging_layer
( bear::input::key_code toggle_key )
  : m_visible(false), m_toggle_key(toggle_key)
{

} // base_debugging_layer::base_debugging_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of the layer.
 * \param elapsed_time Elapsed time since the last call.
 */
void
ptb::base_debugging_layer::progress( bear::universe::time_type elapsed_time )
{
  item_list items;

  find_items(items);

  if ( !items.empty() )
    progress(items, elapsed_time);
 } // base_debugging_layer::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the layer on a screen.
 * \param e (out) The scene elements.
 */
void ptb::base_debugging_layer::render( scene_element_list& e ) const
{
  if (m_visible)
    render(e, get_level().get_camera_focus().bottom_left());
} // base_debugging_layer::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a keyboard key has been pressed.
 * \param key The value of the pressed key.
 */
bool ptb::base_debugging_layer::key_pressed
( const bear::input::key_info& key )
{
  bool result = true;

  if ( key.get_code() == m_toggle_key )
    {
      m_visible = !m_visible;
      if ( m_visible )
        on_show();
    }
  else
    result = false;

  return result;
} // base_debugging_layer::key_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the visible items.
 * \param items (out) The items found.
 */
void ptb::base_debugging_layer::find_items( item_list& items ) const
{
  bear::engine::level_globals& glob = get_level_globals();

  bear::engine::world::msg_pick_items_in_region msg_w
    ( get_level().get_camera_focus() );
  glob.send_message( "world", msg_w );

  items = msg_w.items;
} // base_debugging_layer::find_items()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the subclasses that the layer is displayed.
 */
void ptb::base_debugging_layer::on_show()
{
  // nothing to do
} // base_debugging_layer::on_show()

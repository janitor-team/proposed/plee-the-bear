/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file balloon_layer.cpp
 * \brief Implementation of the ptb::balloon_layer class.
 * \author Sebastien Angibaud
 */
#include "ptb/layer/balloon_layer.hpp"

#include "ptb/balloon.hpp"
#include "ptb/speaker_item.hpp"
#include "ptb/layer/balloon_placement/balloon_placement.hpp"

#include "engine/level.hpp"
#include "engine/level_globals.hpp"

/*----------------------------------------------------------------------------*/
const unsigned int ptb::balloon_layer::s_border = 20;

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param name The name of the layer.
 */
ptb::balloon_layer::balloon_layer( const std::string& name )
  : bear::communication::messageable(name)
{

} // balloon_layer::balloon_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the layer.
 */
void ptb::balloon_layer::build()
{
  get_level_globals().register_item(*this);
} // balloon_layer::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Adjust the postion of the balloons.
 * \param elapsed_time The time elapsed since the last call.
 */
void ptb::balloon_layer::progress( bear::universe::time_type elapsed_time )
{
  if ( get_level().is_paused() )
    return;

  speaker_list::iterator it;
  balloon_placement placement(get_size().x, get_size().y);

  for ( it = m_speakers.begin(); it != m_speakers.end(); )
    if ( (*it) == (speaker_item*)(NULL) )
      {
        speaker_list::iterator it2 = it;
        ++it;
        m_speakers.erase(it2);
      }
    else
      {
        placement.add_speaker( **it, get_bounding_box_on_screen(*it) );
        ++it;
      }

  placement.place_balloons();
} // balloon_layer::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the scene elements of the layer.
 * \param e (out) The scene elements.
 */
void ptb::balloon_layer::render( scene_element_list& e ) const
{
  speaker_list::const_iterator it;

  for ( it = m_speakers.begin(); it != m_speakers.end(); ++it)
    if ( !(*it)->has_finished_to_chat() )
      (*it)->get_balloon().render(e);
} // balloon_layer::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a speaker.
 * \param speaker The speaker.
 * \pre The speaker is unknown by this layer.
 */
void ptb::balloon_layer::add_speaker( speaker_item* speaker )
{
#ifndef NDEBUG
  bool known = false;
  speaker_list::iterator it;
  for ( it=m_speakers.begin(); (it!=m_speakers.end()) && !known; ++it)
    if ( *it == speaker )
      known = true;

  if (known)
    CLAW_FAIL("The item is already in the balloon layer.");
#endif

  m_speakers.push_back( bear::universe::item_handle_from(speaker) );
} // balloon_layer::add_speaker()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the bounding box of a speaker of the screen.
 * \param speaker The speaker.
 */
bear::universe::rectangle_type
ptb::balloon_layer::get_bounding_box_on_screen(handle_type& speaker) const
{
  const bear::universe::rectangle_type cam( get_level().get_camera_focus() );
  const double x_ratio = get_size().x / cam.size().x;
  const double y_ratio = get_size().y / cam.size().y;

  const bear::universe::coordinate_type left =
    x_ratio * (speaker.get_item()->get_left() - cam.left());
  const bear::universe::coordinate_type right =
    x_ratio * (speaker.get_item()->get_right() - cam.left());
  const bear::universe::coordinate_type top =
    y_ratio * (speaker.get_item()->get_top() - cam.bottom());
  const bear::universe::coordinate_type bottom =
    y_ratio * (speaker.get_item()->get_bottom() - cam.bottom());

  return bear::universe::rectangle_type( left, bottom, right, top );
} // balloon_layer::get_bounding_box_on_screen()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file misc_layer.cpp
 * \brief Implementation of the ptb::misc_layer class.
 * \author Julien Jorge
 */
#include "ptb/layer/misc_layer.hpp"

#include "engine/game.hpp"
#include "engine/level_globals.hpp"
#include "input/keyboard.hpp"
#include "visual/font.hpp"
#include "visual/scene_sprite.hpp"

#include "ptb/game_variables.hpp"

#include <sstream>
#include <iomanip>
#include <claw/bitmap.hpp>
#include <claw/logger.hpp>
#include <claw/png.hpp>

#include <boost/thread.hpp>

#ifdef PTB_TRACE_FPS
typedef std::list<unsigned int> fps_list;
static fps_list g_fps;
#endif

/*----------------------------------------------------------------------------*/
/**
 * \brief Execute the function object to save the image.
 */
template<typename ImageWriter>
void ptb::misc_layer::screenshot_file_save<ImageWriter>::operator()() const
{
  try
    {
      std::ofstream f( file_path.c_str() );
      ImageWriter writer(*image, f);
    }
  catch(std::exception& e)
    {
      claw::logger << claw::log_error << "Screenshot: " << e.what()
                   << std::endl;
    }
} // misc_layer::screenshot_file_save::operator()()




/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::misc_layer::misc_layer()
  : m_fps_text(NULL), m_fps_count(0), m_its_count(0),
#ifdef PTB_TRACE_FPS
    m_show_fps(true),
#else
    m_show_fps(false),
#endif
    m_cursor_position(-1, -1), m_mouse_idle(0), m_first_screenshot(0),
    m_fps_key(bear::input::keyboard::kc_F2),
    m_screenshot_key(bear::input::keyboard::kc_F5),
    m_fullscreen_key(bear::input::keyboard::kc_F12),
    m_screenshot_sequence_key(bear::input::keyboard::kc_F11),
    m_levelshot_key(bear::input::keyboard::kc_F10)
{

} // misc_layer::misc_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::misc_layer::~misc_layer()
{
  delete m_fps_text;

  if (m_first_screenshot != 0)
    stop_screenshot_sequence();

#ifdef PTB_TRACE_FPS
  std::ofstream f("fps.txt");
  std::size_t i=0;
  for ( fps_list::const_iterator it=g_fps.begin(); it!=g_fps.end(); ++it, ++i )
    f << i << '\t' << *it << '\n';
#endif
} // misc_layer::~misc_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the layer.
 */
void ptb::misc_layer::build()
{
  m_fps_text =
    new bear::gui::static_text
    ( get_level_globals().get_font("font/fixed_white-7x12.fnt") );

  m_fps_text->set_auto_size(true);
  m_fps_text->set_text("0");
  m_fps_text->set_position( m_fps_text->height(), m_fps_text->height() );

  m_last_fps_check = bear::systime::get_date_ms();

  m_cursor =
    get_level_globals().auto_sprite( "gfx/ui/frame.png", "mouse cursor" );

  if ( game_variables::record_game() )
    start_screenshot_sequence();
} // misc_layer::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of the layer.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::misc_layer::progress( bear::universe::time_type elapsed_time )
{
  ++m_its_count;
  m_mouse_idle += elapsed_time;

  if ( m_first_screenshot != 0 )
    sequence_screenshot();
} // misc_layer::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the visibles components of the layer on a screen.
 * \param e (out) The scene elements.
 */
void ptb::misc_layer::render( scene_element_list& e ) const
{
  ++m_fps_count;
  render_fps( e );

  if ( m_mouse_idle <= 2 )
    e.push_back
      ( bear::visual::scene_sprite
        ( m_cursor_position.x, m_cursor_position.y - m_cursor.height(),
          m_cursor) );
} // misc_layer::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a key had been pressed.
 * \param key The value of the pressed key.
 */
bool ptb::misc_layer::key_pressed( const bear::input::key_info& key )
{
  bool result = true;

  if ( key.get_code() == m_fps_key )
    m_show_fps = !m_show_fps;
  else if ( key.get_code() == m_screenshot_key )
    screenshot();
  else if ( key.get_code() == m_levelshot_key )
    levelshot();
  else if ( key.get_code() == m_fullscreen_key )
    bear::engine::game::get_instance().toggle_fullscreen();
  else if ( key.get_code() == m_screenshot_sequence_key )
    {
      if (m_first_screenshot != 0)
        stop_screenshot_sequence();
      else
        start_screenshot_sequence();
    }
  else
    result = false;

  return result;
} // misc_layer::key_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that the mouse had been moved.
 * \param pos The new position of the mouse.
 */
bool ptb::misc_layer::mouse_move
( const claw::math::coordinate_2d<unsigned int>& pos )
{
  m_cursor_position = pos;
  m_mouse_idle = 0;

  // let the sub components know the movement
  return false;
} // misc_layer::mouse_move()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a picture of the whole level.
 */
void ptb::misc_layer::levelshot() const
{
  std::ostringstream name;
  name << "level-" << bear::systime::get_date_ms() << ".bmp";

  screenshot_file_save<claw::graphic::bitmap::writer> writer;
  writer.image = new claw::graphic::image;

  bear::engine::game::get_instance().levelshot(*writer.image);
  writer.file_path =
    bear::engine::game::get_instance().get_custom_game_file(name.str());

  boost::thread t(writer);
} // misc_layer::levelshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a picture of the screen.
 */
void ptb::misc_layer::screenshot() const
{
  std::ostringstream name;
  name << bear::systime::get_date_ms() << ".png";

  screenshot<claw::graphic::png::writer>(name.str());
} // misc_layer::screenshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a picture of the screen.
 * \param name The name of the file to save the image into.
 */
template<typename ImageWriter>
void ptb::misc_layer::screenshot( const std::string& name ) const
{
  screenshot_file_save<ImageWriter> writer;
  writer.image = new claw::graphic::image;

  bear::engine::game::get_instance().screenshot(*writer.image);
  writer.file_path =
    bear::engine::game::get_instance().get_custom_game_file(name);

  boost::thread t(writer);
} // misc_layer::screenshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a picture of the screen for the screenshot sequence.
 */
void ptb::misc_layer::sequence_screenshot()
{
  if ( bear::systime::get_date_ms() >= m_last_screenshot + 40 ) // 25 fps
    {
      std::ostringstream name;
      name << m_screenshot_prefix << '-' << std::setw(8)
           << std::setfill('0') << m_screenshots_count << ".bmp";
      ++m_screenshots_count;

      screenshot<claw::graphic::bitmap::writer>(name.str());
      m_last_screenshot = bear::systime::get_date_ms();
    }
} // misc_layer::sequence_screenshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save the images in the sequence.
 */
void ptb::misc_layer::start_screenshot_sequence()
{
  claw::logger << claw::log_verbose << "Starting screenshot sequence."
               << std::endl;

  m_first_screenshot = bear::systime::get_date_ms();
  m_last_screenshot = m_first_screenshot;
  m_screenshots_count = 0;

  std::ostringstream prefix;
  prefix << "s-" << bear::systime::get_date_ms();
  m_screenshot_prefix = prefix.str();
} // misc_layer::start_screenshot_sequence()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save the data associated with the screenshots of the sequence.
 */
void ptb::misc_layer::stop_screenshot_sequence()
{
  std::string file_name
    ( bear::engine::game::get_instance().get_custom_game_file
      ( m_screenshot_prefix + ".seq" ) );
  std::ofstream f( file_name.c_str() );

  const bear::systime::milliseconds_type d = bear::systime::get_date_ms();
  const unsigned int fps =
    (unsigned int)
    ( (double)m_screenshots_count
      / ((double)(d - m_first_screenshot) / 1000.0)
      + 0.5 );

  f << fps << " # fps" << std::endl;

  claw::logger << claw::log_verbose << "Screenshot sequence stopped. " << fps
               << " fps during " << ((double)(d - m_first_screenshot) / 1000.0)
               << " seconds." << std::endl;

  m_first_screenshot = 0;
} // misc_layer::stop_screenshot_sequence()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the fps text component.
 * \param e (out) The scene elements.
 */
void ptb::misc_layer::render_fps( scene_element_list& e ) const
{
  if ( m_show_fps )
    {
      const bear::systime::milliseconds_type current_time =
        bear::systime::get_date_ms();

      if ( current_time - m_last_fps_check >= 1000 )
        {
          std::ostringstream oss;
          oss << m_fps_count << " fps - " << m_its_count << " its";

#ifdef PTB_TRACE_FPS
          g_fps.push_back(m_fps_count);
#endif

          m_fps_text->set_text( oss.str() );
          m_fps_count = 0;
          m_its_count = 0;
          m_last_fps_check = bear::systime::get_date_ms();
        }

      m_fps_text->render( e );
    }
} // misc_layer::render_fps()

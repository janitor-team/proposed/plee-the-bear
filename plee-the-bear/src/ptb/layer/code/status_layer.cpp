/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file status_layer.cpp
 * \brief Implementation of the ptb::status_layer class.
 * \author Julien Jorge
 */
#include "ptb/layer/status_layer.hpp"

#include "engine/game.hpp"
#include "visual/bitmap_writing.hpp"
#include "visual/scene_sprite.hpp"
#include "visual/scene_writing.hpp"
#include "visual/scene_line.hpp"

#include "ptb/defines.hpp"
#include "ptb/game_variables.hpp"
#include "ptb/gauge.hpp"
#include "ptb/level_variables.hpp"
#include "ptb/monster.hpp"
#include "ptb/util/player_util.hpp"

#include <libintl.h>
#include <boost/bind.hpp>

#include <claw/tween/tweener_sequence.hpp>
#include <claw/tween/easing/easing_elastic.hpp>
#include <claw/tween/easing/easing_linear.hpp>
#include <claw/tween/easing/easing_back.hpp>

/*----------------------------------------------------------------------------*/
const double ptb::status_layer::player_status::s_bar_length = 100;

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param glob Level globals in which we take the resources.
 * \param f The font to use to display the texts.
 * \param p The player from which we take the data.
 */
ptb::status_layer::player_status::player_status
( bear::engine::level_globals& glob, const bear::visual::font& f,
  const player_proxy& p )
  : m_level_globals(glob),
    energy( glob, s_bar_length, "bar (green)", "bar (red)", "heart", true ),
    oxygen( glob, s_bar_length, "bar (blue)", "bubble" ),
    heat_gauge( glob, s_bar_length, "bar (yellow)", "sun" ),
    cold_gauge( glob, s_bar_length, "bar (white)", "snowflake" ),
    oxygen_active(false), oxygen_offset_x(oxygen.width() + 5),
    heat_gauge_active(false), heat_gauge_offset_x(heat_gauge.width() + 5),
    cold_gauge_active(false), cold_gauge_offset_x(cold_gauge.width() + 5),
    lives_scale(1), m_player(p)
{
  m_font = f;

  std::ostringstream oss;
  oss << game_variables::get_lives_count(m_player.get_index());
  lives.create(m_font, oss.str());
  lives->set_intensity(1, 0.8, 0);

  std::ostringstream oss2;
  oss2 << game_variables::get_score(m_player.get_index());
  score.create(m_font, oss2.str());
  score->set_intensity(1, 0.8, 0);

  if ( p.get_index() == 2 )
    energy.set_level_sprite( m_level_globals, "bar (light blue)" );

  init_signals();

  energy.set_length
    (game_variables::get_max_energy(m_player.get_index()));
  energy.set_max_level
    (game_variables::get_max_energy(m_player.get_index()));
  energy.set_level(p.get_energy());

  std::ostringstream oss3;
  oss3 <<
    m_player.get_throwable_items().get_current_throwable_item()->get_stock();
  throwable_items.create(m_font, oss3.str());
  throwable_items->set_intensity(1, 0.8, 0);

  throwable_item_animation =
    m_level_globals.get_animation
    ( m_player.get_throwable_items().get_current_throwable_item()
      ->get_animation() );
} // status_layer::player_status::player_status()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::status_layer::player_status::~player_status()
{
  for ( ; !m_signals.empty(); m_signals.pop_front() )
    m_signals.front().disconnect();
} // status_layer::player_status::~player_status()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the player concerned by this instance.
 */
const ptb::player_proxy& ptb::status_layer::player_status::get_player() const
{
  return m_player;
} // status_layer::player_status::get_player()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the values.
 * \param glob The level globals in which we load the media.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::status_layer::player_status::progress
( bear::engine::level_globals& glob, bear::universe::time_type elapsed_time )
{
  energy.progress(elapsed_time);
  oxygen.progress(elapsed_time);
  heat_gauge.progress(elapsed_time);
  cold_gauge.progress(elapsed_time);

  if (m_player != NULL)
    {
      m_tweeners.update(elapsed_time);
      throwable_item_animation.next(elapsed_time);
    }
} // status_layer::player_status::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set all signals to listen.
 */
void ptb::status_layer::player_status::init_signals()
{
  m_signals.push_back
    ( m_player.get_signals().enters_water_zone.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_enters_water_zone, this) ) );

  m_signals.push_back
    ( m_player.get_signals().leaves_water_zone.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_leaves_water_zone, this) ) );

  m_signals.push_back
    ( m_player.get_signals().oxygen_gauge_changed.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_oxygen_gauge_changed, this,
         _1) ) );

  m_signals.push_back
    ( m_player.get_signals().enters_cold_zone.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_enters_cold_zone, this) ) );

  m_signals.push_back
    ( m_player.get_signals().leaves_cold_zone.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_leaves_cold_zone, this) ) );

  m_signals.push_back
    ( m_player.get_signals().cold_gauge_changed.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_cold_gauge_changed, this,
         _1) ) );

  m_signals.push_back
    ( m_player.get_signals().enters_heat_zone.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_enters_heat_zone, this) ) );

  m_signals.push_back
    ( m_player.get_signals().leaves_heat_zone.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_leaves_heat_zone, this) ) );

  m_signals.push_back
    ( m_player.get_signals().heat_gauge_changed.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_heat_gauge_changed, this,
         _1) ) );

  m_signals.push_back
    ( m_player.get_signals().energy_added.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_energy_added, this, _1) ) );

  m_signals.push_back
    ( m_player.get_signals().energy_removed.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_energy_removed, this, _1) ) );

  m_signals.push_back
    ( m_player.get_throwable_items().throwable_item_changed.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_throwable_item_changed,
         this, _1) ) );

  m_signals.push_back
    ( m_player.get_throwable_items().throwable_item_stock_changed.connect
      ( boost::bind
        (&ptb::status_layer::player_status::on_throwable_item_stock_changed,
         this, _1) ) );

  m_signals.push_back
    ( bear::engine::game::get_instance().listen_uint_variable_change
      ( game_variables::get_score_variable_name(m_player.get_index()),
        boost::bind
        (&ptb::status_layer::player_status::on_score_changed,
         this, _1) ) );

  m_signals.push_back
    ( bear::engine::game::get_instance().listen_uint_variable_change
      ( game_variables::get_lives_count_variable_name(m_player.get_index()),
        boost::bind
        (&ptb::status_layer::player_status::on_lives_changed,
         this, _1) ) );

  m_signals.push_back
    ( bear::engine::game::get_instance().listen_double_variable_change
      ( game_variables::get_max_energy_variable_name(m_player.get_index()),
        boost::bind
        (&ptb::status_layer::player_status::on_max_energy_added,
         this, _1) ) );
} // status_layer::player_status::init_signals()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the player enters in the water.*/
void ptb::status_layer::player_status::on_enters_water_zone()
{
  m_tweeners.insert
  ( claw::tween::single_tweener
    (oxygen_offset_x, 0, 1,
     boost::bind
     ( &ptb::status_layer::player_status::on_oxygen_gauge_position_update,
       this, _1 ), &claw::tween::easing_elastic::ease_out ) );
} // status_layer::player_status::on_enters_water_zone()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the player leaves the water.*/
void ptb::status_layer::player_status::on_leaves_water_zone()
{
  m_tweeners.insert
  ( claw::tween::single_tweener
    (oxygen_offset_x, oxygen.width() + 5, 1,
     boost::bind
     ( &ptb::status_layer::player_status::on_oxygen_gauge_position_update,
       this, _1 ), &claw::tween::easing_linear::ease_in_out ) );
} // status_layer::player_status::on_leaves_water_zone()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the the oxygen gauge changes.
 * \param level The level of the gauge.
 */
void ptb::status_layer::player_status::on_oxygen_gauge_changed(double level)
{
  if ( m_player != NULL )
    oxygen.set_level( s_bar_length * level /
                      m_player.get_oxygen_gauge().get_max_value() );
} // status_layer::player_status::on_oxygen_gauge_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the player enters in a cold zone.*/
void ptb::status_layer::player_status::on_enters_cold_zone()
{
  m_tweeners.insert
  ( claw::tween::single_tweener
    (cold_gauge_offset_x, 0, 1,
     boost::bind
     ( &ptb::status_layer::player_status::on_cold_gauge_position_update,
       this, _1 ), &claw::tween::easing_elastic::ease_out ) );
} // status_layer::player_status::on_enters_cold_zone()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the player leaves a cold zone.*/
void ptb::status_layer::player_status::on_leaves_cold_zone()
{
  m_tweeners.insert
  ( claw::tween::single_tweener
    (cold_gauge_offset_x, cold_gauge.width() + 5, 1,
     boost::bind
     ( &ptb::status_layer::player_status::on_cold_gauge_position_update,
       this, _1 ), &claw::tween::easing_linear::ease_in_out ) );
} // status_layer::player_status::on_leaves_cold_zone()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the the cold gauge changes.
 * \param level The level of the gauge.
 */
void ptb::status_layer::player_status::on_cold_gauge_changed(double level)
{
  if ( m_player != NULL )
    cold_gauge.set_level
      ( s_bar_length * level /
        m_player.get_cold_gauge().get_max_value() );
} // status_layer::player_status::on_cold_gauge_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the player enters in a heat zone.*/
void ptb::status_layer::player_status::on_enters_heat_zone()
{
  m_tweeners.insert
    ( claw::tween::single_tweener
      (heat_gauge_offset_x, 0, 1,
       boost::bind
       ( &ptb::status_layer::player_status::on_heat_gauge_position_update,
         this, _1 ), &claw::tween::easing_elastic::ease_out ) );
} // status_layer::player_status::on_enters_heat_zone()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the player leaves a heat zone.*/
void ptb::status_layer::player_status::on_leaves_heat_zone()
{
  m_tweeners.insert
    ( claw::tween::single_tweener
      (heat_gauge_offset_x, heat_gauge.width() + 5, 1,
       boost::bind
       ( &ptb::status_layer::player_status::on_heat_gauge_position_update,
         this, _1 ), &claw::tween::easing_linear::ease_in_out ) );
} // status_layer::player_status::on_leaves_heat_zone()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the the heat gauge changes.
 * \param level The level of the gauge.
 */
void ptb::status_layer::player_status::on_heat_gauge_changed(double level)
{
  if ( m_player != NULL )
    heat_gauge.set_level
      ( s_bar_length * level /
        m_player.get_heat_gauge().get_max_value() );
} // status_layer::player_status::on_heat_gauge_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when .
 * \param animation The new animation */
void ptb::status_layer::player_status::on_throwable_item_changed
( const std::string& animation )
{
  throwable_item_animation = m_level_globals.get_animation( animation );
} // status_layer::player_status::on_throwable_item_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when .
 * \param stock The new stock.
 */
void ptb::status_layer::player_status::on_throwable_item_stock_changed
( unsigned int stock)
{
  std::ostringstream oss;
  oss << stock;
  throwable_items.create(m_font, oss.str());
  throwable_items->set_intensity(1, 0.8, 0);
} // status_layer::player_status::on_throwable_item_stock_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when score changes.
 * \param s The new score.
 */
void ptb::status_layer::player_status::on_score_changed(unsigned int s)
{
  std::ostringstream oss;
  oss << s;
  score.create(m_font, oss.str());
  score->set_intensity(1, 0.8, 0);
} // status_layer::player_status::on_score_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when lives count changes.
 * \param s The new lives count.
 */
void ptb::status_layer::player_status::on_lives_changed(unsigned int s)
{
  std::ostringstream oss;
  oss << s;
  lives.create(m_font, oss.str());
  lives->set_intensity(1, 0.8, 0);

  claw::tween::tweener_sequence tween;
  tween.insert
    ( claw::tween::single_tweener
      (1, 1.5, 0.5,
       boost::bind
       ( &ptb::status_layer::player_status::on_lives_scale_update,
         this, _1 ), &claw::tween::easing_back::ease_out ) );
  tween.insert
    ( claw::tween::single_tweener
      (1.5, 1, 0.5,
       boost::bind
       ( &ptb::status_layer::player_status::on_lives_scale_update,
         this, _1 ), &claw::tween::easing_back::ease_in ) );

  m_tweeners.insert( tween );
} // status_layer::player_status::on_lives_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when some energy is added.
 * \param e The new level of energy.
 */
void ptb::status_layer::player_status::on_energy_added(double e)
{
  energy.set_level( e );
} // status_layer::player_status::on_energy_added()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when some energy is removed.
 * \param e The new level of energy.
 */
void ptb::status_layer::player_status::on_energy_removed(double e)
{
  energy.set_level( e );
} // status_layer::player_status::on_energy_removed()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the maximum energy increases.
 * \param e The new maximum of energy.
 */
void ptb::status_layer::player_status::on_max_energy_added(double e)
{
  m_tweeners.insert
    ( claw::tween::single_tweener
      (energy.length(), e, 1,
       boost::bind
       ( &ptb::status_layer::player_status::on_max_energy_length_update,
         this, _1 ), &claw::tween::easing_elastic::ease_out ) );
} // status_layer::player_status::on_max_energy_added()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the position of oxygen gauge changes.
 * \param The new x position of the gauge.
 */
void ptb::status_layer::player_status::on_oxygen_gauge_position_update
(double position)
{
  oxygen_offset_x = position;

  oxygen_active = ( oxygen_offset_x != ( oxygen.width() + 5 ) );
} // status_layer::player_status::on_oxygen_gauge_position_update()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the position of cold gauge changes.
 * \param The new x position of the gauge.
 */
void ptb::status_layer::player_status::on_cold_gauge_position_update
(double position)
{
  cold_gauge_offset_x = position;

  cold_gauge_active = ( cold_gauge_offset_x != ( cold_gauge.width() + 5 ) );
} // status_layer::player_status::on_cold_gauge_position_update()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the position of heat gauge changes.
 * \param The new x position of the gauge.
 */
void ptb::status_layer::player_status::on_heat_gauge_position_update
(double position)
{
  heat_gauge_offset_x = position;

  heat_gauge_active = ( heat_gauge_offset_x != ( heat_gauge.width() + 5 ) );
} // status_layer::player_status::on_heat_gauge_position_update()


/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the length of max energy length changes.
 * \param length The new length of max energy bar.
 */
void ptb::status_layer::player_status::on_max_energy_length_update
(double length)
{
  if ( m_player != NULL )
    {
      energy.set_length(length);
      energy.set_max_level(length);
      energy.set_level(length);
    }
} // status_layer::player_status::on_max_energy_length_update

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the scale of lives writing changes.
 * \param scale The new scale.
 */
void ptb::status_layer::player_status::on_lives_scale_update
(double scale)
{
  lives_scale = scale;

  double red(1/lives_scale);
  double green(red-0.5);
  green = green * 8 / 5;
  lives->set_intensity(red, green, 0);
} // status_layer::player_status::on_lives_scale_update()

/*----------------------------------------------------------------------------*/
const unsigned int ptb::status_layer::s_margin = 10;

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param name The name of the layer.
 */
ptb::status_layer::status_layer( const std::string& name )
  : bear::communication::messageable(name), m_data_1(NULL), m_data_2(NULL),
    m_boss(NULL), m_boss_energy(NULL), m_timer(NULL),
    m_corrupting_bonus_scale(1)
{

} // status_layer::status_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::status_layer::~status_layer()
{
  for ( ; !m_signals.empty(); m_signals.pop_front() )
    m_signals.front().disconnect();

  delete m_data_1;
  delete m_data_2;
  delete m_boss_energy;
} // status_layer::~status_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void ptb::status_layer::pre_cache()
{
  super::pre_cache();

  get_level_globals().load_image("gfx/ui/status/tube.png");
  get_level_globals().load_image("gfx/ui/status/tube-vertical.png");
} // status_layer::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the layer.
 */
void ptb::status_layer::build()
{
  super::build();

  init_signals();
  bear::engine::level_globals& glob = get_level_globals();
  glob.register_item(*this);

  bear::visual::font f = glob.get_font("font/bouncy.fnt");

  m_text_time.create(f, "00:00");
  m_time_on = true;

  m_small_player =
    bear::visual::sprite
    ( glob.auto_sprite("gfx/ui/status/status.png", "plee"));

  std::ostringstream oss;
  oss << game_variables::get_corrupting_bonus_count();
  m_text_corrupting_bonus.create(f, oss.str());
  m_text_corrupting_bonus->set_intensity(1, 0.8, 0);
  m_last_corrupting_bonus_count = game_variables::get_corrupting_bonus_count();

  m_hazelnut = new bear::visual::animation
    (glob.get_animation("animation/owl/hazelnut.canim"));

  m_honeypot = glob.auto_sprite("gfx/ui/status/status.png", "shiny jar");
} // status_layer::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the players status and update local values.
 */
void ptb::status_layer::progress( bear::universe::time_type elapsed_time )
{
  search_players();

  progress_time(elapsed_time);
  progress_boss(elapsed_time);
  m_tweeners.update(elapsed_time);

  if ( m_data_1 != NULL )
    m_data_1->progress(get_level_globals(), elapsed_time);

  if ( m_data_2 != NULL )
    m_data_2->progress(get_level_globals(), elapsed_time);
} // status_layer::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the players status.
 * \param e (out) The scene elements.
 */
void ptb::status_layer::render( scene_element_list& e ) const
{
  // render first player status
  if ( (m_data_1 != NULL) && !m_data_1->get_player().is_a_marionette() )
    render_player( e, m_data_1, false );

  // render second player status (if any)
  if ( (m_data_2 != NULL) && !m_data_2->get_player().is_a_marionette() )
    render_player( e, m_data_2, true );

  if ( m_time_on )
    e.push_back
      ( bear::visual::scene_writing
        ( (get_size().x - m_text_time.get_width()) / 2,
          get_size().y - s_margin - m_text_time.get_height(), m_text_time ) );

  render_corrupting_bonus( e );
  render_hazelnut( e );
  render_honeypots( e );
  render_boss( e );
} // status_layer::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the timer to use for the time.
 */
void ptb::status_layer::set_timer( const timer_handle& t )
{
  m_timer = t;
} // status_layer::set_timer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the boss for which we display the energy.
 * \param b The boss.
 */
void ptb::status_layer::set_boss( const monster* b )
{
  delete m_boss_energy;
  m_boss_energy = NULL;

  m_boss = dynamic_cast<const bear::universe::physical_item*>(b);

  if ( m_boss != (monster*)NULL )
    {
      m_boss_energy =
        new horizontal_gauge
        ( get_level_globals(), m_boss->get_max_energy(), "bar (god yellow)",
          "bar (red)" );
    }
} // status_layer::set_boss()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the animation representing the corrupting bonus.
 * \param anim The animation.
 */
void ptb::status_layer::set_corrupting_bonus
( const bear::visual::animation& anim )
{
  m_corrupting_bonus = anim;
} // status_layer::set_corrupting_bonus()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set all signals to listen.
 */
void ptb::status_layer::init_signals()
{
  m_signals.push_back
    ( bear::engine::game::get_instance().listen_uint_variable_change
      ( game_variables::get_corrupting_bonus_count_variable_name(),
        boost::bind
        (&ptb::status_layer::on_corrupting_bonus_updated,
         this, _1) ) );
} // status_layer::init_signals()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the time text.
 */
void ptb::status_layer::progress_time( bear::universe::time_type elapsed_time )
{
  if ( m_timer != (bear::timer*)NULL )
    {
      m_time_on = true;

      const bear::universe::time_type time = m_timer->get_time();
      const std::string t
        ( bear::systime::format_time_s( time,  gettext("%M:%S") ) );

      if ( m_timer->is_countdown() && (time <= 30) &&
           ((time - (int)time) < 0.5) )
        m_text_time->set_intensity(1, 0, 0);
      else
        m_text_time->set_intensity(1, 0.8, 0);

      m_text_time.create( get_level_globals().get_font("font/bouncy.fnt"), t );
    }
  else
    m_time_on = false;
} // status_layer::progress_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the state of the boss.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::status_layer::progress_boss( bear::universe::time_type elapsed_time )
{
  if ( (m_boss == (monster*)NULL) || (m_boss_energy == NULL) )
    return;

  m_boss_energy->set_length
    ( std::min(m_boss->get_max_energy(), get_size().x * 0.9 ) );
  m_boss_energy->set_max_level( m_boss->get_max_energy() );
  m_boss_energy->set_level( m_boss->get_energy() );

  m_boss_energy->progress( elapsed_time );
} // progress_boss()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the corrupting_bonus state.
 */
void ptb::status_layer::render_corrupting_bonus( scene_element_list& e) const
{
  if ( m_corrupting_bonus.empty() )
    return;

  bear::universe::coordinate_type width =
    m_corrupting_bonus.width() +
    m_text_corrupting_bonus.get_width() + s_margin;

  bear::universe::position_type pos( (get_size().x - width)/2, s_margin);

  if ( (m_boss_energy == NULL) || (m_boss_energy->get_level() == 0) )
    pos.y += m_honeypot.height() + s_margin;

  bear::universe::coordinate_type height =
    std::max(m_corrupting_bonus.height(), m_text_corrupting_bonus.get_height());

  bear::visual::scene_sprite sp( 0, 0, m_corrupting_bonus.get_sprite() );

  sp.set_scale_factor(m_corrupting_bonus_scale, m_corrupting_bonus_scale);
  sp.set_position
    ( pos.x + (m_corrupting_bonus.width() - sp.get_bounding_box().width()) / 2,
      pos.y + (height - sp.get_bounding_box().height()) / 2 );
  e.push_back( sp );

  pos.x += m_corrupting_bonus.width() + s_margin;

  bear::visual::scene_writing s( 0, 0, m_text_corrupting_bonus );
  s.set_scale_factor(m_corrupting_bonus_scale, m_corrupting_bonus_scale);
  s.set_position
    ( pos.x
      + ( m_text_corrupting_bonus.get_width()
          - s.get_bounding_box().width() ) / 2,
      pos.y + (height - s.get_bounding_box().height()) / 2 );

  e.push_back( s );
} // status_layer::render_corrupting_bonus()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the hazelnut.
 */
void ptb::status_layer::render_hazelnut
( scene_element_list& e) const
{
  if ( level_variables::get_current_hazelnut(get_level()) )
    {
      e.push_back
        ( bear::visual::scene_sprite
          ( (get_size().x-m_hazelnut->width())/2,
            get_size().y - 2*s_margin - m_text_time.get_height()
            - m_hazelnut->get_sprite().height(),
            m_hazelnut->get_sprite()) );
    }
} // status_layer::render_hazelnut()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render honeypots.
 */
void ptb::status_layer::render_honeypots
( scene_element_list& e) const
{
  if ( (m_boss_energy == NULL) || (m_boss_energy->get_level() == 0) )
    {
      unsigned int nb = level_variables::get_honeypots_found(get_level());
      bear::universe::coordinate_type width =
        nb * m_honeypot.width() + (nb-1)*s_margin;

      for ( unsigned int i = 0; i != nb; ++i )
        e.push_back
          ( bear::visual::scene_sprite
            ( ( get_size().x - width )/ 2 +
              i * (s_margin + m_honeypot.width()),
              s_margin,
              m_honeypot) );
    }
} // status_layer::render_honeypots()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the energy bar of the boss.
 * \param e The list in which the visual components of the bar are added.
 */
void ptb::status_layer::render_boss( scene_element_list& e ) const
{
  if ( (m_boss_energy == NULL) || (m_boss_energy->get_level() == 0) )
    return;

  m_boss_energy->render
    ( e,
      bear::visual::position_type
      ( (get_size().x - m_boss_energy->width()) / 2, 80 ) );
} // status_layer::render_boss()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw the status of a given player.
 * \param e (out) The scene elements.
 * \param data The player status to render.
 * \param right_alignment Indicates if we align on the right.
 */
void ptb::status_layer::render_player
( scene_element_list& e, const player_status* data,
  bool right_alignment ) const
{
  double take_width(0);
  double orientation(1);

  if ( right_alignment )
    {
      take_width = 1;
      orientation = -1;
    }

  double pos_y;

  pos_y = render_energy(e, data,get_size().y,take_width,orientation);
  pos_y = render_score(e, data,pos_y,take_width,orientation);
  pos_y = render_throwable_item(e, data,pos_y,take_width,orientation);
  pos_y = render_gauges(e, data,pos_y,take_width,orientation);
  pos_y = render_lifes(e, data,pos_y,take_width,orientation);
} // status_layer::render_player()


/*----------------------------------------------------------------------------*/
/**
 * \brief Draw the energy of a given player.
 * \param e (out) The scene elements.
 * \param data The player status to render.
 * \param y_init The y-coordinate reference.
 * \param take_width Indicates if we take into account object's width.
 * \param orientation The orientation of objects.
 */
double ptb::status_layer::render_energy
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width,
      double orientation ) const
{
  bear::visual::position_type pos
    ( (take_width ? get_size().x : 1)
      - take_width * data->energy.width() + orientation * s_margin,
      y_init - s_margin - data->energy.height() );
  data->energy.render(e, pos);

  return pos.y;
} // status_layer::render_energy

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw the score of a given player.
 * \param e (out) The scene elements.
 * \param data The player status to render.
 * \param y_init The y-coordinate reference.
 * \param take_width Indicates if we take into account object's width.
 * \param orientation The orientation of objects.
 */
double ptb::status_layer::render_score
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width, double orientation ) const
{
  bear::visual::position_type pos
    ( (take_width ? get_size().x : 1) +
      orientation*s_margin - take_width*data->score.get_size().x,
      y_init - s_margin - data->score.get_height() );
  bear::visual::scene_writing s1( pos.x, pos.y, data->score );
  e.push_back( s1 );

  return pos.y;
} // status_layer::render_score

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw throwable_item of a given player.
 * \param e (out) The scene elements.
 * \param data The player status to render.
 * \param y_init The y-coordinate reference.
 * \param take_width Indicates if we take into account object's width.
 * \param orientation The orientation of objects.
 */
double ptb::status_layer::render_throwable_item
( scene_element_list& e, const player_status* data,
  double y_init, double take_width, double orientation ) const
{
  bear::visual::position_type pos
    ( (take_width ? get_size().x : 1) + orientation*s_margin -
      take_width*data->throwable_item_animation.get_sprite().width(),
      y_init - s_margin -
      data->throwable_item_animation.get_sprite().height() );

  bear::visual::scene_sprite s_item
    (pos.x, pos.y, data->throwable_item_animation.get_sprite());

  pos.set( (take_width ? get_size().x : 1) + orientation *
           (2*s_margin + data->throwable_item_animation.get_sprite().width()) -
           take_width*data->throwable_items.get_size().x,
           pos.y
           + ( data->throwable_item_animation.get_sprite().height()
               - data->throwable_items.get_height() ) / 2 );

  bear::visual::scene_writing s2( pos.x, pos.y, data->throwable_items );

  if ( !data->get_player().get_throwable_items().
       get_current_throwable_item()->can_throw() )
    {
      s_item.get_rendering_attributes().set_opacity(0.5);
      s2.get_rendering_attributes().set_opacity(0.5);
      s2.get_rendering_attributes().set_intensity(1, 0.4, 0.4);
    }
  e.push_back( s_item );
  e.push_back( s2 );

  return pos.y;
} // status_layer::render_throwable_item

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw gauges of a given player.
 * \param e (out) The scene elements.
 * \param data The player status to render.
 * \param y_init The y-coordinate reference.
 * \param take_width Indicates if we take into account object's width.
 * \param orientation The orientation of objects.
 */
double ptb::status_layer::render_gauges
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width, double orientation ) const
{
  bear::visual::position_type pos
    ((take_width ? get_size().x : 1) +
     orientation*s_margin - take_width*data->oxygen.width(),
     y_init - s_margin - data->oxygen.height());

  if ( data->oxygen_active )
    {
      pos.x -= orientation*data->oxygen_offset_x;
      data->oxygen.render(e, pos);
    }

  pos.set( (take_width ? get_size().x : 1) + orientation*s_margin -
           take_width*data->heat_gauge.width(),
           pos.y - data->heat_gauge.height() - s_margin );

  if ( data->heat_gauge_active )
    {
      pos.x -= orientation*data->heat_gauge_offset_x;
      data->heat_gauge.render(e, pos);
    }
  pos.set( (take_width ? get_size().x : 1) + orientation*s_margin -
           take_width*data->cold_gauge.width(),
           pos.y - data->cold_gauge.height() - s_margin );

  if ( data->cold_gauge_active )
    {
      pos.x -= orientation*data->cold_gauge_offset_x;
      data->cold_gauge.render(e, pos);
    }

  return pos.y;
} // status_layer::render_gauges

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw lifes of a given player.
 * \param e (out) The scene elements.
 * \param data The player status to render.
 * \param y_init The y-coordinate reference.
 * \param take_width Indicates if we take into account object's width.
 * \param orientation The orientation of objects.
 */
double ptb::status_layer::render_lifes
( scene_element_list& e, const player_status* data,
  double y_init, double take_width, double orientation ) const
{
  bear::visual::position_type pos
    ( (take_width ? get_size().x : 1) + orientation*s_margin
      - take_width*m_small_player.width(), s_margin );
  e.push_back( bear::visual::scene_sprite(pos.x, pos.y, m_small_player) );

  pos.set( (take_width ? get_size().x : 1) +
           orientation*(2*s_margin + m_small_player.width())
           - take_width*data->lives.get_width()*data->lives_scale,
           s_margin
           + ( m_small_player.height()
               - data->lives.get_height() *data->lives_scale ) / 2 );

  bear::visual::scene_writing s3( pos.x, pos.y, data->lives );
  s3.set_scale_factor(data->lives_scale, data->lives_scale);
  e.push_back( s3 );

  return pos.y;
} // status_layer::render_lifes

/*----------------------------------------------------------------------------*/
/**
 * \brief Search players.
 */
void ptb::status_layer::search_players( )
{
  if ( m_data_1 == NULL )
    {
      player_proxy p = util::find_player( get_level_globals(), 1 );

      if ( p != NULL )
        m_data_1 = new player_status
          ( get_level_globals(),
            get_level_globals().get_font("font/bouncy.fnt"), p );
    }

  if ( (game_variables::get_players_count() == 2) && (m_data_2 == NULL) )
    {
      player_proxy p = util::find_player( get_level_globals(), 2 );

      if ( p != NULL )
        m_data_2 = new player_status
          ( get_level_globals(),
            get_level_globals().get_font("font/bouncy.fnt"), p );
    }
} // status_layer::search_players( )

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when a corrupting_bonus is updated.
 * \param value The new number of corrupting_bonus.
 */
void ptb::status_layer::on_corrupting_bonus_updated
(unsigned int value)
{
  bear::engine::level_globals& glob = get_level_globals();
  bear::visual::font f = glob.get_font("font/bouncy.fnt");

  std::ostringstream oss;
  oss << value;
  m_text_corrupting_bonus.create(f, oss.str());
  m_text_corrupting_bonus->set_intensity(1, 0.8, 0);

  if ( value < m_last_corrupting_bonus_count )
    {
      claw::tween::tweener_sequence tween;
      tween.insert
        ( claw::tween::single_tweener
          (1, 1.5, 0.3,
           boost::bind
           ( &ptb::status_layer::on_corrupting_bonus_scale_update,
             this, _1 ), &claw::tween::easing_back::ease_out ) );
      tween.insert
        ( claw::tween::single_tweener
          (1.5, 1, 0.3,
           boost::bind
           ( &ptb::status_layer::on_corrupting_bonus_scale_update,
             this, _1 ), &claw::tween::easing_back::ease_in ) );

      m_tweeners.insert( tween );
    }

  m_last_corrupting_bonus_count = value;
} // status_layer::on_corrupting_bonus_updated()

/*----------------------------------------------------------------------------*/
/**
 * \brief The fonction called when the scale of corrupting bonus
 * writing changes.
 * \param scale The new scale.
 */
void ptb::status_layer::on_corrupting_bonus_scale_update
(double scale)
{
  m_corrupting_bonus_scale = scale;

  double red(1/m_corrupting_bonus_scale);
  double green(red-0.5);
  green = green * 8 / 5;
  m_text_corrupting_bonus->set_intensity(red, green, 0);
} // status_layer::on_corrupting_bonus_scale_update()

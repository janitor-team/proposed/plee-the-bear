/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file windows_layer.cpp
 * \brief Implementation of the ptb::windows_layer class.
 * \author Julien Jorge
 */
#include "ptb/layer/windows_layer.hpp"

#include "ptb/frame/frame.hpp"
#include "engine/level_globals.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::windows_layer::windows_layer()
{

} // windows_layer::windows_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param name The name of the layer in the post_office.
 */
ptb::windows_layer::windows_layer( const std::string& name )
  : bear::communication::messageable(name)
{

} // windows_layer::windows_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::windows_layer::~windows_layer()
{
  clear();
} // windows_layer::~windows_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the layer.
 */
void ptb::windows_layer::build()
{
  get_level_globals().register_item(*this);
} // windows_layer::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of the layer.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::windows_layer::progress( bear::universe::time_type elapsed_time )
{
  super::progress(elapsed_time);

  for ( ; !m_dying_windows.empty(); m_dying_windows.pop_front() )
    delete m_dying_windows.front();
} // windows_layer::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a key has been pressed.
 * \param key The value of the pressed key.
 */
bool ptb::windows_layer::key_pressed( const bear::input::key_info& key )
{
  bool result = false;

  if ( !m_windows.empty() )
    {
      result = m_windows.top()->key_pressed(key);

      if ( !result && key.is_escape() )
        result = close_window();
    }

  return result;
} // windows_layer::key_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a character has been entered.
 * \param key The value of the pressed key.
 */
bool ptb::windows_layer::char_pressed( const bear::input::key_info& key )
{
  bool result = false;

  if ( !m_windows.empty() )
    result = m_windows.top()->char_pressed(key);

  return result;
} // windows_layer::char_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a joystick button had been pressed.
 * \param button The value of the pressed button.
 * \param joy_index The index of the joystick.
 */
bool ptb::windows_layer::button_pressed
( bear::input::joystick::joy_code button, unsigned int joy_index )
{
  if ( !m_windows.empty() )
    return m_windows.top()->button_pressed( button, joy_index );
  else
    return false;
} // windows_layer::button_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a mouse button has been pressed.
 * \param pos The current position of the cursor.
 * \param key The value of the pressed button.
 */
bool ptb::windows_layer::mouse_pressed
( bear::input::mouse::mouse_code key,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  bool result = false;

  if ( !m_windows.empty() )
    {
      claw::math::rectangle<unsigned int> bounding_box
        ( m_windows.top()->get_position(), m_windows.top()->get_size() );

      if ( bounding_box.includes( pos ) )
        result = m_windows.top()->mouse_pressed
          ( key, pos - m_windows.top()->get_position() );
    }

  return result;
} // windows_layer::mouse_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a mouse button has been released.
 * \param pos The current position of the cursor.
 * \param key The value of the pressed button.
 */
bool ptb::windows_layer::mouse_released
( bear::input::mouse::mouse_code key,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  bool result = false;

  if ( !m_windows.empty() )
    {
      claw::math::rectangle<unsigned int> bounding_box
        ( m_windows.top()->get_position(), m_windows.top()->get_size() );

      if ( bounding_box.includes( pos ) )
        result = m_windows.top()->mouse_released
          ( key, pos - m_windows.top()->get_position() );
    }

  return result;
} // windows_layer::mouse_released()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that a mouse button has been maintained.
 * \param pos The current position of the cursor.
 * \param key The value of the pressed button.
 */
bool ptb::windows_layer::mouse_maintained
( bear::input::mouse::mouse_code key,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  bool result = false;

  if ( !m_windows.empty() )
    {
      claw::math::rectangle<unsigned int> bounding_box
        ( m_windows.top()->get_position(), m_windows.top()->get_size() );

      if ( bounding_box.includes( pos ) )
        result = m_windows.top()->mouse_maintained
          ( key, pos - m_windows.top()->get_position() );
    }

  return result;
} // windows_layer::mouse_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform the layer that the mouse has been moved.
 * \param pos The new position of the mouse.
 */
bool ptb::windows_layer::mouse_move
( const claw::math::coordinate_2d<unsigned int>& pos )
{
  bool result = false;

  if ( !m_windows.empty() )
    {
      claw::math::rectangle<unsigned int> bounding_box
        ( m_windows.top()->get_position(), m_windows.top()->get_size() );

      if ( bounding_box.includes( pos ) )
        result =
          m_windows.top()->mouse_move( pos - m_windows.top()->get_position() );
    }

  return result;
} // windows_layer::mouse_move()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the visible components of the layer on a screen.
 * \param e (out) The scene elements.
 */
void ptb::windows_layer::render( scene_element_list& e ) const
{
  if ( !m_windows.empty() )
    m_windows.top()->render(e);
} // windows_layer::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Remove all windows.
 */
void ptb::windows_layer::clear()
{
  for ( ; !m_windows.empty(); m_windows.pop() )
    delete m_windows.top();
} // windows_layer::clear()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is no window displayed.
 */
bool ptb::windows_layer::empty() const
{
  return m_windows.empty();
} // windows_layer::empty()

/*----------------------------------------------------------------------------*/
/**
 * \brief Show a window, centered in the screen.
 * \param wnd The window to show.
 * \post !m_window.empty() && m_window.top() is centered in the screen.
 */
void ptb::windows_layer::show_centered_window( window_item wnd )
{
  wnd->set_position( (get_size() - wnd->get_size()) / 2 );
  m_windows.push(wnd);
  wnd->on_focus();
} // windows_layer::show_window()

/*----------------------------------------------------------------------------*/
/**
 * \brief Replace the top window.
 * \param old_wnd The window to replace.
 * \param new_wnd The new window.
 * \post !m_window.empty() && m_window.top() is centered in the screen.
 */
void ptb::windows_layer::replace_window
( window_item old_wnd, window_item new_wnd )
{
  CLAW_PRECOND(old_wnd == m_windows.top());
  CLAW_PRECOND(old_wnd != new_wnd);

  m_windows.top()->close();
  pop_window();

  new_wnd->set_position( (get_size() - new_wnd->get_size()) / 2 );
  m_windows.push(new_wnd);
  new_wnd->on_focus();
} // windows_layer::replace_window()

/*----------------------------------------------------------------------------*/
/**
 * \brief Show a window.
 * \param wnd The window to show.
 * \post !m_window.empty()
 */
void ptb::windows_layer::show_window( window_item wnd )
{
  m_windows.push( wnd );
  wnd->on_focus();
} // windows_layer::show_window()

/*----------------------------------------------------------------------------*/
/**
 * \brief Close the current window.
 */
bool ptb::windows_layer::close_window()
{
  bool result = false;

  if ( !m_windows.empty() )
    if ( m_windows.top()->close() )
      {
        pop_window();

        if ( !m_windows.empty() )
          m_windows.top()->on_focus();

        result = true;
      }

  return result;
} // windows_layer::close_window()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do the stuff related to a score message.
 * \param msg The message to process.
 */
bool ptb::windows_layer::process_score_message( const score_message& msg )
{
  // nothing to do
  return false;
} // windows_layer::process_score_message()

/*----------------------------------------------------------------------------*/
/**
 * \brief Pop the current window.
 */
void ptb::windows_layer::pop_window()
{
  m_dying_windows.push_back(m_windows.top());
  m_windows.pop();
} // windows_layer::pop_window()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file main_menu_layer.cpp
 * \brief Implementation of the ptb::main_menu_layer class.
 * \author Julien Jorge
 */
#include "ptb/layer/main_menu_layer.hpp"

#include "engine/level_globals.hpp"
#include "ptb/frame/frame_main_menu.hpp"

#include "engine/version.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param pos The position of the main window.
 */
ptb::main_menu_layer::main_menu_layer
( const claw::math::coordinate_2d<unsigned int>& pos )
  : m_engine_version(NULL), m_position(pos)
{

} // main_menu_layer::main_menu_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::main_menu_layer::~main_menu_layer()
{
  delete m_engine_version;
} // main_menu_layer::~main_menu_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the layer.
 */
void ptb::main_menu_layer::build()
{
  super::build();

  m_engine_version =
    new bear::gui::static_text
    ( get_level_globals().get_font("font/fixed_white-7x12.fnt") );

  m_engine_version->set_auto_size(true);
  m_engine_version->set_text(BEAR_VERSION_STRING);
  m_engine_version->set_position
    ( get_size().x - m_engine_version->width() - m_engine_version->height(),
      m_engine_version->height() );

  frame_main_menu* wnd = new frame_main_menu(this);

  wnd->set_position(m_position);
  show_window( wnd );
} // main_menu_layer::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the layer on a screen.
 * \param e (out) The scene elements.
 */
void ptb::main_menu_layer::render( scene_element_list& e ) const
{
  m_engine_version->render(e);
  super::render(e);
} // main_menu_layer::render()

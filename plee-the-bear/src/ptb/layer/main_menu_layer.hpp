/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file main_menu_layer.hpp
 * \brief The main_menu_layer is a layer containing the windows of the title
 *        screen.
 * \author Julien Jorge
 */
#ifndef __PTB_MAIN_MENU_LAYER_HPP__
#define __PTB_MAIN_MENU_LAYER_HPP__

#include "ptb/layer/windows_layer.hpp"
#include "gui/static_text.hpp"

namespace ptb
{
  /**
   * \brief The main_menu_layer is a layer containing the windows of the title
   *        screen.
   * \author Julien Jorge
   */
  class main_menu_layer:
    public windows_layer
  {
  private:
    /** \brief The type of the parent class. */
    typedef windows_layer super;

  public:
    main_menu_layer( const claw::math::coordinate_2d<unsigned int>& pos );
    ~main_menu_layer();

    void build();
    void render( scene_element_list& e ) const;

  private:
    /** \brief The component in which we show the version of the engine. */
    bear::gui::static_text* m_engine_version;

    /** \brief The position of the menu. */
    const claw::math::coordinate_2d<unsigned int> m_position;

  }; // class main_menu_layer
} // namespace ptb

#endif // __PTB_MAIN_MENU_LAYER_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file balloon_placement.hpp
 * \brief A class that tries to place the balloons such that they don't overlap.
 * \author Julien Jorge
 */
#ifndef __PTB_BALLOON_PLACEMENT_HPP__
#define __PTB_BALLOON_PLACEMENT_HPP__

#include "universe/types.hpp"

#include <list>

namespace ptb
{
  class speaker_item;

  /**
   * \brief A class that tries to place the balloons such that they don't
   *        overlap.
   * \author Julien Jorge
   */
  class balloon_placement
  {
  private:
    /** \brief A character speaking in the scene. */
    struct scene_character
    {
    public:
      scene_character
      ( speaker_item& i, const bear::universe::rectangle_type& r, bool v );

      bear::universe::size_box_type get_balloon_size() const;

      /** \brief The bounding box of the character. */
      const bear::universe::rectangle_type box;

      /** \brief The speaking item. */
      speaker_item& item;

      /** \brief Tell if the speaker is visible. */
      bool visible;

    }; // struct scene_character

    class candidate;
    typedef std::list<candidate*> candidate_group;

    /** \brief A candidate position for a balloon. */
    class candidate
    {
    public:
      /** \brief This function object compares two candidates in decreasing
          order of their quality. */
      struct increasing_conflicts
      {
        bool operator()( const candidate* a, const candidate* b ) const;
      }; // struct increasing_conflicts

    public:
      candidate
      ( const bear::universe::rectangle_type& r, const scene_character& s,
        int score );

      void add_covered_area( double percent );
      void set_in_conflict_with( candidate* c );
      std::size_t get_conclicts_count() const;

      /** \brief Get the candidates conflicting with this one. */
      const candidate_group& get_conflicts() const { return m_conflicts; }

      bool is_valid() const;
      void invalidate();

      int eval() const;

    public:
      /** \brief The rectangle where the balloon would be. */
      const bear::universe::rectangle_type rect;

      /** \brief The character owning the balloon. */
      const scene_character& speaker;

    private:
      /** \brief The candidates in conflicts with this one. */
      candidate_group m_conflicts;

      /** \brief The size of m_conflicts. */
      int m_conflicts_count;

      /** \brief Tell if this candidate is still usable. */
      bool m_is_valid;

      /** \brief The score of this candidate. */
      int m_score;

      /** \brief Percentage of the area of the balloon covered by something
          else. */
      double m_covered_area;

    }; // class candidate

    /** \brief The list of candidates, where each group concerns the same
        speaker. */
    typedef std::list<candidate_group> candidate_group_list;

    /** \brief This function object compares two groups in decreasing order of
        the quality of their first candidate. */
    struct group_ordering
    {
      bool operator()
      ( const candidate_group& a, const candidate_group& b ) const;
    }; // struct group_ordering

    /** \brief The type of the list in which we store the speakers. */
    typedef std::list<scene_character> character_list_type;

  public:
    balloon_placement
    ( bear::universe::size_type w, bear::universe::size_type h );

    void add_speaker
    ( speaker_item& c, const bear::universe::rectangle_type& rect );

    void place_balloons() const;

  private:
    void sort_candidates( candidate_group_list& c ) const;

    void create_candidates( candidate_group_list& c ) const;
    void
    check_conflicts( candidate_group& g, candidate_group_list& result ) const;

    void new_candidate
    ( const scene_character& c, candidate_group& result,
      bear::universe::coordinate_type left,
      bear::universe::coordinate_type bottom, int score ) const;
    void create_candidate_visible
    ( const scene_character& c, candidate_group& result ) const;
    void create_candidate_not_visible
    ( const scene_character& c, candidate_group& result ) const;

    void repeat_candidate_horizontally
    ( const scene_character& c, candidate_group& result,
      bear::universe::coordinate_type first,
      bear::universe::coordinate_type last,
      bear::universe::coordinate_type y ) const;
    void repeat_candidate_vertically
    ( const scene_character& c, candidate_group& result,
      bear::universe::coordinate_type first,
      bear::universe::coordinate_type last,
      bear::universe::coordinate_type x ) const;
    void repeat_candidate_placed_horizontally
    ( const scene_character& c, candidate_group& result,
      bear::universe::coordinate_type y ) const;
    void repeat_candidate_placed_vertically
    ( const scene_character& c, candidate_group& result,
      bear::universe::coordinate_type x ) const;

    bool check_on_top( const candidate& c ) const;
    bool check_on_right( const candidate& c ) const;

  private:
    /** \brief The size of the view where the speakers are visible. */
    bear::universe::rectangle_type m_view;

    /** \brief The list of all speakers. */
    character_list_type m_characters;

  }; // class balloon_placement
} // namespace ptb

#endif // __PTB_BALLOON_PLACEMENT_HPP__

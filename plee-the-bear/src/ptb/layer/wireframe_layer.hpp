/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file wireframe_layer.hpp
 * \brief This layer displays the bounding boxes of the items.
 * \author Julien Jorge
 */
#ifndef __PTB_WIREFRAME_LAYER_HPP__
#define __PTB_WIREFRAME_LAYER_HPP__

#include "ptb/layer/base_debugging_layer.hpp"

namespace ptb
{
  /**
   * \brief This layer displays the bounding boxes if the items.
   * \author Julien Jorge
   */
  class wireframe_layer:
    public base_debugging_layer
  {
  public:
    wireframe_layer();

  private:
    void render
    ( scene_element_list& e, const bear::visual::position_type& delta ) const;

    void progress
    ( const item_list& items, bear::universe::time_type elapsed_time );

  private:
    void draw_box
    ( scene_element_list& e, const bear::visual::position_type& delta,
      const bear::universe::physical_item& item,
      const bear::visual::color_type& color ) const;
    void draw_system
    ( scene_element_list& e, const bear::visual::position_type& delta,
      const bear::universe::physical_item& item,
      const bear::visual::color_type& color ) const;
    void draw_slope
    ( scene_element_list& e, const bear::visual::position_type& delta,
      const bear::universe::physical_item& item,
      const bear::visual::color_type& color ) const;

  private:
    /** \brief The items to render. */
    item_list m_items;

  }; // class wireframe_layer
} // namespace ptb

#endif // __PTB_WIREFRAME_LAYER_HPP__

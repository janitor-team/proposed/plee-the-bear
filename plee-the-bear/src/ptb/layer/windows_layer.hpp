/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file windows_layer.hpp
 * \brief The windows_layer is a layer containing the windows.
 * \author Julien Jorge
 */
#ifndef __PTB_WINDOWS_LAYER_HPP__
#define __PTB_WINDOWS_LAYER_HPP__

#include "communication/messageable.hpp"
#include "engine/layer/gui_layer.hpp"
#include "input/keyboard.hpp"
#include "input/joystick.hpp"

#include <stack>

namespace ptb
{
  class frame;
  class score_message;

  /**
   * \brief The windows_layer is a layer containing windows.
   * \author Julien Jorge
   */
  class windows_layer:
    public bear::communication::messageable,
    public bear::engine::gui_layer
  {
  public:
    /** \brief The type of a list of scene elements retrieved from the layer. */
    typedef bear::engine::gui_layer::scene_element_list scene_element_list;

  private:
    /** \brief Informations on a window in the stack. */
    typedef frame* window_item;

    /** \brief The type of the main super class. */
    typedef bear::engine::gui_layer super;

  public:
    windows_layer();
    explicit windows_layer( const std::string& name );

    ~windows_layer();

    void build();
    void progress( bear::universe::time_type elapsed_time );

    bool key_pressed( const bear::input::key_info& key );
    bool char_pressed( const bear::input::key_info& key );
    bool button_pressed( bear::input::joystick::joy_code button,
                         unsigned int joy_index );
    bool mouse_pressed( bear::input::mouse::mouse_code key,
                        const claw::math::coordinate_2d<unsigned int>& pos );
    bool mouse_released( bear::input::mouse::mouse_code key,
                        const claw::math::coordinate_2d<unsigned int>& pos );
    bool mouse_maintained( bear::input::mouse::mouse_code key,
                        const claw::math::coordinate_2d<unsigned int>& pos );
    bool mouse_move( const claw::math::coordinate_2d<unsigned int>& pos );

    void render( scene_element_list& e ) const;

    void clear();
    bool empty() const;
    void show_centered_window( window_item wnd );
    void replace_window( window_item old_wnd, window_item new_wnd );
    void show_window( window_item wnd );

    bool close_window();

    virtual bool process_score_message( const score_message& msg );

  private:
    void pop_window();

  private:
    /** \brief The pending windows. */
    std::stack<window_item> m_windows;

    /** \brief A window waiting to be deleted. */
    std::list<window_item> m_dying_windows;

  }; // class windows_layer
} // namespace ptb

#endif // __PTB_WINDOWS_LAYER_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file status_layer.hpp
 * \brief The status layer contains the area where are printed the score,
 *        energy, remaining lifes of the players.
 * \author Julien Jorge
 */
#ifndef __PTB_STATUS_LAYER_HPP__
#define __PTB_STATUS_LAYER_HPP__

#include "ptb/gui/horizontal_gauge.hpp"
#include "ptb/gui/vertical_gauge.hpp"
#include "ptb/player_proxy.hpp"
#include "ptb/throwable_item/throwable_item.hpp"

#include "generic_items/timer.hpp"

#include "communication/messageable.hpp"
#include "engine/layer/gui_layer.hpp"
#include "visual/animation.hpp"
#include "visual/writing.hpp"
#include "universe/derived_item_handle.hpp"

#include <claw/tween/tweener_group.hpp>

namespace ptb
{
  class monster;

  /**
   * \brief The status layer contains the area where are printed the score,
   *        energy, remaining lifes of the players.
   */
  class status_layer:
    public bear::communication::messageable,
    public bear::engine::gui_layer
  {
  public:
    /** \brief The type of a list of scene elements retrieved from the layer. */
    typedef bear::engine::gui_layer::scene_element_list scene_element_list;

    typedef bear::engine::gui_layer super;

  private:
    /** \brief An handle on the timer of the level. */
    typedef
    bear::universe::const_derived_item_handle<bear::timer> timer_handle;

    /** \brief An handle on a monster. */
    typedef bear::universe::const_derived_item_handle<monster> monster_handle;

  private:
    /**
     * \brief All the informations we want to show about a player.
     */
    class player_status
    {
    public:
      player_status
      ( bear::engine::level_globals& glob, const bear::visual::font& f,
        const player_proxy& p);

      ~player_status();

      const player_proxy& get_player() const;

      void progress( bear::engine::level_globals& glob,
                     bear::universe::time_type elapsed_time );

    private:
      void init_signals();

      void on_enters_water_zone();
      void on_leaves_water_zone();
      void on_oxygen_gauge_changed(double level);
      void on_enters_cold_zone();
      void on_leaves_cold_zone();
      void on_cold_gauge_changed(double level);
      void on_enters_heat_zone();
      void on_leaves_heat_zone();
      void on_heat_gauge_changed(double level);
      void on_throwable_item_changed(const std::string& animation);
      void on_throwable_item_stock_changed(unsigned stock);
      void on_score_changed(unsigned int s);
      void on_lives_changed(unsigned int s);
      void on_energy_added(double e);
      void on_energy_removed(double e);
      void on_max_energy_added(double e);

      void on_oxygen_gauge_position_update(double position);
      void on_cold_gauge_position_update(double position);
      void on_heat_gauge_position_update(double position);
      void on_max_energy_length_update(double length);
      void on_lives_scale_update(double scale);

    public:
      /** \brief Level globals. */
      bear::engine::level_globals& m_level_globals;

      /** \brief Level of energy. */
      horizontal_gauge energy;

      /** \brief Level of oxygen. */
      vertical_gauge oxygen;

      /** \brief The heat gauge. */
      vertical_gauge heat_gauge;

      /** \brief The cold gauge. */
      vertical_gauge cold_gauge;

      /** \brief The score. */
      bear::visual::writing score;

      /** \brief Number of throwable items. */
      bear::visual::writing throwable_items;

      /** \brief Number of remaining lives. */
      bear::visual::writing lives;

      /** \brief Indicates if the oxygen gauge is activated. */
      bool oxygen_active;

      /** \brief The offset of the oxygen gauge. */
      bear::visual::coordinate_type oxygen_offset_x;

      /** \brief Indicates if the heat gauge is activated. */
      bool heat_gauge_active;

      /** \brief The offset of the heat gauge. */
      bear::visual::coordinate_type heat_gauge_offset_x;

      /** \brief Indicates if the cold gauge is activated. */
      bool cold_gauge_active;

      /** \brief The offset of the cold gauge. */
      bear::visual::coordinate_type cold_gauge_offset_x;

      /** \brief The current animation of throwable_item. */
      bear::visual::animation throwable_item_animation;

      /** \brief The scale of lives writing. */
      double lives_scale;

    private:
      /** \brief The font for text. */
      bear::visual::font m_font;

      /** \brief The set of current tweeners. */
      claw::tween::tweener_group m_tweeners;

      /** \brief Last number of remaining lives. */
      unsigned int m_last_lives;

      /** \brief Pointer to the player. */
      player_proxy m_player;

      /** \brief The length of the bars. */
      const static double s_bar_length;

      /** \brief The connections to various signals. */
      std::list<boost::signals::connection> m_signals;

    }; // class player_status

  public:
    status_layer( const std::string& name );
    virtual ~status_layer();

    void pre_cache();
    void build();
    void progress( bear::universe::time_type elapsed_time );
    void render( scene_element_list& e ) const;

    void set_timer( const timer_handle& t );
    void set_boss( const monster* b );
    void set_corrupting_bonus( const bear::visual::animation& anim );

  private:
    void init_signals();
    void progress_time( bear::universe::time_type elapsed_time );
    void progress_corrupting_bonus( bear::universe::time_type elapsed_time );
    void progress_boss( bear::universe::time_type elapsed_time );

    void render_corrupting_bonus( scene_element_list& e ) const;
    void render_hazelnut( scene_element_list& e ) const;
    void render_honeypots( scene_element_list& e ) const;
    void render_boss( scene_element_list& e ) const;

    void render_player
    ( scene_element_list& e, const player_status* data,
      bool right_alignment ) const;
    double render_energy
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width, double orientation ) const;
    double render_score
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width, double orientation ) const;
    double render_throwable_item
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width, double orientation ) const;
    double render_gauges
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width, double orientation ) const;
    double render_lifes
    ( scene_element_list& e, const player_status* data,
      double y_init, double take_width, double orientation ) const;

    void search_players();

    void on_corrupting_bonus_updated(unsigned int value);
    void on_corrupting_bonus_scale_update(double scale);

  private:
    /** \brief Data of the first player. */
    player_status* m_data_1;

    /** \brief Data of the second player. */
    player_status* m_data_2;

    /** \brief The set of current tweeners. */
    claw::tween::tweener_group m_tweeners;

    /** \brief The boss whose energy is displayed. */
    monster_handle m_boss;

    /** \brief Level of energy for the boss. */
    horizontal_gauge* m_boss_energy;

    /** \brief The component in which we show the time. */
    bear::visual::writing m_text_time;

    /** \brief Indicate if the time must be displayed. */
    bool m_time_on;

    /** \brief The timer of the level. */
    timer_handle m_timer;

    /** \brief A small Plee next to the remaining lives. */
    bear::visual::sprite m_small_player;

    /** \brief Animation for corrupting bonus. */
    bear::visual::animation m_corrupting_bonus;

    /** \brief The component in which we show the corrupting bonus count. */
    bear::visual::writing m_text_corrupting_bonus;

    /** \brief The last number of corrupting bonus. */
    unsigned int m_last_corrupting_bonus_count;

    /** \brief Animation for hazelnut. */
    bear::visual::animation* m_hazelnut;

    /** \brief Animation for honeypots. */
    bear::visual::sprite m_honeypot;

    /** \brief Scale of the corrupting bonus writing. */
    double m_corrupting_bonus_scale;

    /** \brief Distance between the elements of the layer. */
    const static unsigned int s_margin;

    /** \brief The connections to various signals. */
    std::list<boost::signals::connection> m_signals;

  }; // class status_layer
} // namespace ptb

#endif // __PTB_STATUS_LAYER_HPP__

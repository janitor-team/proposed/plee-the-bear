/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file ingame_menu_layer.hpp
 * \brief The ingame_menu_layer is a layer containing the windows shown during
 *        the game (levels).
 * \author Julien Jorge
 */
#ifndef __PTB_INGAME_MENU_LAYER_HPP__
#define __PTB_INGAME_MENU_LAYER_HPP__

#include "ptb/layer/windows_layer.hpp"

namespace ptb
{
  /**
   * \brief The ingame_menu_layer is a layer containing the windows shown during
   *        the game (levels).
   * \author Julien Jorge
   */
  class ingame_menu_layer:
    public windows_layer
  {
  public:
    /** \brief The type of the parent class. */
    typedef windows_layer super;

  public:
    ingame_menu_layer( const std::string& name );

    bool key_pressed( const bear::input::key_info& key );
    bool button_pressed
    ( bear::input::joystick::joy_code button, unsigned int joy_index );
    bool mouse_pressed
    ( bear::input::mouse::mouse_code button,
      const claw::math::coordinate_2d<unsigned int>& pos );

    virtual bool process_score_message( const score_message& msg );

  private:
    bool pause( unsigned int player_index );
    bool talk( unsigned int player_index );

  }; // class ingame_menu_layer
} // namespace ptb

#endif // __PTB_INGAME_MENU_LAYER_HPP__

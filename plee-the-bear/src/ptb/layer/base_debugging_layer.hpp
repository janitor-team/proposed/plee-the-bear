/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file base_debugging_layer.hpp
 * \brief Base layer for the ones that give debugging information about the
 *        visible items.
 * \author Julien Jorge
 */
#ifndef __PTB_BASE_DEBUGGING_LAYER_HPP__
#define __PTB_BASE_DEBUGGING_LAYER_HPP__

#include "engine/layer/gui_layer.hpp"
#include "engine/base_item.hpp"

#include "input/key_info.hpp"

namespace ptb
{
  /**
   * \brief Base layer for the ones that give debugging information about the
   *        visible items.
   * \author Julien Jorge
   */
  class base_debugging_layer:
    public bear::engine::gui_layer
  {
  public:
    /** \brief The type of a list of scene elements retrieved from the layer. */
    typedef bear::engine::gui_layer::scene_element_list scene_element_list;

  protected:
    /** \brief The type of a list of items. */
    typedef std::list<bear::universe::physical_item*> item_list;

  public:
    base_debugging_layer( bear::input::key_code toggle_key );

    void progress( bear::universe::time_type elapsed_time );
    void render( scene_element_list& e ) const;

    bool key_pressed( const bear::input::key_info& key );

  protected:
    virtual void render( scene_element_list& e,
                         const bear::visual::position_type& delta ) const = 0;

    virtual void progress
    ( const item_list& items, bear::universe::time_type elapsed_time ) = 0;

  private:
    void find_items( item_list& items ) const;

    virtual void on_show();

  private:
    /** \brief Tell if the bow must be drawn. */
    bool m_visible;

    /** \brief The value of the key that changes the visibility of the layer. */
    const bear::input::key_code m_toggle_key;

  }; // class base_debugging_layer
} // namespace ptb

#endif // __PTB_BASE_DEBUGGING_LAYER_HPP__

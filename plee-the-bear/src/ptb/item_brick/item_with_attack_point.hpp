/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item_with_attack_point.hpp
 * \brief The class describing an item that has an attack point.
 * \author Sébastien Angibaud
 */
#ifndef __PTB_ITEM_WITH_ATTACK_POINT_HPP__
#define __PTB_ITEM_WITH_ATTACK_POINT_HPP__

#include "ptb/item_brick/with_attack_point.hpp"

namespace ptb
{
  /**
   * \brief The class describing an item that has an attack point.
   *
   * \b Template \b arguments :
   *  - \a Base the base class of the model, should end by
   *    bear::engine::base_item.
   *
   * \author Sébastien Angibaud
   */
  template<class Base>
  class item_with_attack_point :
    public Base,
    public with_attack_point
  {
  public:
    /** \brief The type of the parent class. */
    typedef Base super;

  public:
    virtual bear::universe::position_type get_attack_point() const;
    virtual bool has_attack_point() const;
  }; // class item_with_attack_point
} // namespace ptb

#include "ptb/item_brick/impl/item_with_attack_point.tpp"

#endif // __PTB_ITEM_WITH_ATTACK_POINT_HPP__

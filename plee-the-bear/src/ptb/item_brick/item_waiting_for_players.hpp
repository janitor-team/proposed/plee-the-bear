/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item_waiting_for_players.hpp
 * \brief This item is activated when all players are colliding with it.
 * \author Julien Jorge
 */
#ifndef __PTB_ITEM_WAITING_FOR_PLAYERS_HPP__
#define __PTB_ITEM_WAITING_FOR_PLAYERS_HPP__

#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/base_item.hpp"

#include "ptb/player_proxy.hpp"

namespace ptb
{
  /**
   * \brief This item is activated when all players are colliding with it.
   * \author Julien Jorge
   */
  template<class Base>
  class item_waiting_for_players:
    public bear::engine::basic_renderable_item<Base>
  {
    /** \brief The type of the parent class. */
    typedef bear::engine::basic_renderable_item <Base> super;

  public:
    item_waiting_for_players();
    item_waiting_for_players(const item_waiting_for_players<Base>& that );

    void progress( bear::universe::time_type elapsed_time );
    void build();
    void get_visual( std::list<bear::engine::scene_visual>& visuals ) const;

    bool set_animation_field
    ( const std::string& name, const bear::visual::animation& value );

    bool all_players_present() const;
    bool one_player_present() const;

  private:
    void one_player( const player_proxy& p );
    void all_players( const player_proxy& p1, const player_proxy& p2 );
    void no_player();

    void progress_one_player
    ( bear::universe::time_type elapsed_time, const player_proxy& p );
    void progress_all_players
    ( bear::universe::time_type elapsed_time, const player_proxy& p1,
      const player_proxy& p2 );
    void progress_no_player( bear::universe::time_type elapsed_time );

    virtual void on_one_player( const player_proxy& p );
    virtual void on_all_players
    ( const player_proxy& p1, const player_proxy& p2 );
    virtual void on_no_player();

    virtual void do_progress_one_player
    ( bear::universe::time_type elapsed_time, const player_proxy& p );
    virtual void do_progress_all_players
    ( bear::universe::time_type elapsed_time, const player_proxy& p1,
      const player_proxy& p2 );
    virtual void
    do_progress_no_player( bear::universe::time_type elapsed_time );

    void collision
    ( bear::engine::base_item& that, bear::universe::collision_info& info );

  private:
    /** \brief The first player, if colliding with this item. */
    player_proxy m_first_player;

    /** \brief The second player, if colliding with this item. */
    player_proxy m_second_player;

    /** \brief The pointer to the first player from the previous progress. */
    player_proxy m_previous_first_player;

    /** \brief The pointer to the second player from the previous progress. */
    player_proxy m_previous_second_player;

    /** \brief The animation to display when no player is there. */
    bear::visual::animation m_off;

    /** \brief The animation to display when only the first player is there. */
    bear::visual::animation m_single_one;

    /** \brief The animation to display when only the second player is there.*/
    bear::visual::animation m_single_two;

    /** \brief The animation to display when all players are there. */
    bear::visual::animation m_all;

    /** \brief The animation currently displayed. */
    bear::visual::animation* m_current_animation;

  }; // class item_waiting_for_players
} // namespace ptb

#include "ptb/item_brick/impl/item_waiting_for_players.tpp"

#endif // __PTB_ITEM_WAITING_FOR_PLAYERS_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item_that_speaks.hpp
 * \brief An item with a single sprite.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_ITEM_THAT_SPEAKS_HPP__
#define __PTB_ITEM_THAT_SPEAKS_HPP__

#include "ptb/speaker_item.hpp"

namespace ptb
{
  /**
   * \brief An item that speaks.
   *
   * \b template \b parameters :
   * - \a Base : the base class for this item. Must inherit from
   *    bear::engine::base_item,
   * The valid fields for this item are
   *  - \a speeches: \c The speeches
   *  - any field supported by the parent classes.*
   * \author Sebastien Angibaud
   */
  template<class Base>
  class item_that_speaks:
    public Base,
    public speaker_item
  {
    typedef Base super;

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  public:
    void build();

    void pre_cache();
    void progress( bear::universe::time_type elapsed_time );

    virtual bool set_string_list_field
    ( const std::string& name, const std::vector<std::string>& value );
    virtual bool set_bool_field( const std::string& name, bool value );

  private:
    static void init_exported_methods();

  private:
    /** \brief Tell if this item has been registered in a balloon layer. */
    bool m_registered_in_layer;

  }; // class item_that_speaks
} // namespace ptb

#include "ptb/item_brick/impl/item_that_speaks.tpp"

#endif // __PTB_ITEM_THAT_SPEAKS_HPP__

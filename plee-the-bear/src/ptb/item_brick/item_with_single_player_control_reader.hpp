/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item_with_single_player_control_reader.hpp
 * \brief Inherit from this class to allow your item to read the inputs for a
 *        given player.
 * \author Julien Jorge
 */
#ifndef __PTB_ITEM_WITH_SINGLE_PLAYER_CONTROL_READER_HPP__
#define __PTB_ITEM_WITH_SINGLE_PLAYER_CONTROL_READER_HPP__

#include "ptb/item_brick/item_with_player_control_reader.hpp"

namespace ptb
{
  /**
   * \brief Inherit from this class to allow your item to read the inputs for a
   *        given player.
   *
   * \b template \b parameters :
   * - \a Base: the base class for this item. Must inherit from
   *    engine::base_item.
   *
   * The valid fields for this item are
   *  - \a player_index: (unsigned int) The index of the player controlling this
   *    item (default = 1),
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  template<class Base>
  class item_with_single_player_control_reader:
    public item_with_player_control_reader<Base>
  {
  private:
    /** \brief The type of the parent class. */
    typedef item_with_player_control_reader<Base> super;

  public:
    item_with_single_player_control_reader();

    bool set_u_integer_field( const std::string& name, unsigned int value );

    unsigned int get_player_index() const;

  protected:
    virtual void start_action( player_action::value_type a );
    virtual void do_action
    ( bear::universe::time_type elapsed_time, player_action::value_type a );
    virtual void stop_action( player_action::value_type a );

    void set_player_index( unsigned int i );

  private:
    void start_action( unsigned int player_index, player_action::value_type a );

    void do_action
    ( bear::universe::time_type elapsed_time, unsigned int player_index,
      player_action::value_type a );

    void stop_action( unsigned int player_index, player_action::value_type a );

  private:
    /** \brief The index of the player to listen to. */
    unsigned int m_player_index;

  }; // class item_with_single_player_control_reader
} // namespace ptb

#include "ptb/item_brick/impl/item_with_single_player_control_reader.tpp"

#endif // __PTB_ITEM_WITH_SINGLE_PLAYER_CONTROL_READER_HPP__

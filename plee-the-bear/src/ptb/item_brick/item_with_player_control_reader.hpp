/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item_with_player_control_reader.hpp
 * \brief Inherit from this class to allow your item to read the player's
 *        inputs.
 * \author Julien Jorge
 */
#ifndef __PTB_ITEM_WITH_PLAYER_CONTROL_READER_HPP__
#define __PTB_ITEM_WITH_PLAYER_CONTROL_READER_HPP__

#include "ptb/player_action.hpp"
#include "engine/item_brick/item_with_input_listener.hpp"

namespace ptb
{
  /**
   * \brief Inherit from this class to allow your item to read the player's
   *        inputs.
   *
   * \b template \b parameters :
   * - \a Base: the base class for this item. Must inherit from
   *    engine::base_item.
   *
   * There is no custom field of this class.
   *
   * \author Julien Jorge
   */
  template<class Base>
  class item_with_player_control_reader:
    public bear::engine::item_with_input_listener<Base>
  {
  protected:
    /**
     * \brief Start an action asked by the human player.
     * \param player_index The index of the player asking the action.
     * \param a The action.
     */
    virtual void start_action
    ( unsigned int player_index, player_action::value_type a );

    /**
     * \brief Continue an action asked by the human player.
     * \param elapsed_time How long the action is done.
     * \param player_index The index of the player asking the action.
     * \param a The action.
     */
    virtual void do_action
    ( bear::universe::time_type elapsed_time, unsigned int player_index,
      player_action::value_type a );

    /**
     * \brief Stop an action asked by the human player.
     * \param player_index The index of the player asking the action.
     * \param a The action.
     */
    virtual void stop_action
    ( unsigned int player_index, player_action::value_type a );

  private:
    bool key_pressed( const bear::input::key_info& key );
    bool key_released( const bear::input::key_info& key );
    bool key_maintained
    ( bear::universe::time_type elapsed_time,
      const bear::input::key_info& key );

    bool button_pressed
    ( bear::input::joystick::joy_code button, unsigned int joy_index );
    bool button_released
    ( bear::input::joystick::joy_code button, unsigned int joy_index );
    bool button_maintained
    ( bear::universe::time_type elapsed_time,
      bear::input::joystick::joy_code button, unsigned int joy_index );

    bool mouse_pressed( bear::input::mouse::mouse_code button,
                        const claw::math::coordinate_2d<unsigned int>& pos );
    bool mouse_released( bear::input::mouse::mouse_code button,
                         const claw::math::coordinate_2d<unsigned int>& pos );
    bool mouse_maintained
    ( bear::universe::time_type elapsed_time,
      bear::input::mouse::mouse_code button,
      const claw::math::coordinate_2d<unsigned int>& pos );

  }; // class item_with_player_control_reader
} // namespace ptb

#include "ptb/item_brick/impl/item_with_player_control_reader.tpp"

#endif // __PTB_ITEM_WITH_PLAYER_CONTROL_READER_HPP__

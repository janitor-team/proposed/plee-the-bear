/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file with_attack_point.hpp
 * \brief The base for items that have an attack point.
 * \author Sebastien Angibaud
 */
#ifndef __PTB_WITH_ATTACK_POINT_HPP__
#define __PTB_WITH_ATTACK_POINT_HPP__

#include "universe/types.hpp"
#include "engine/base_item.hpp"

namespace ptb
{
  /**
   * \brief The base of items that have an attack point.
   */
  class with_attack_point
  {
  public:
    with_attack_point();
    virtual ~with_attack_point();

    /**
     * Return the prefered position where this item can be attacked.
     */
    virtual bear::universe::position_type get_attack_point() const = 0;

    /**
     * Tell if the value returned by get_attack_point() is meaningful.
     */
    virtual bool has_attack_point() const = 0;
  }; // class with_attack_point
} // namespace ptb

#endif // __PTB_WITH_ATTACK_POINT_HPP__

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file with_input_reader.cpp
 * \brief Implementation of the ptb::item_with_player_control_reader class.
 * \author Julien Jorge
 */

#include "ptb/controller_config.hpp"
#include "ptb/game_variables.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Start an action asked by the human player.
 * \param player_index The index of the player asking the action.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_player_control_reader<Base>::start_action
( unsigned int player_index, player_action::value_type a )
{
  // nothing to do
} // item_with_player_control_reader::start_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Continue an action asked by the human player.
 * \param elapsed_time How long the action is done.
 * \param player_index The index of the player asking the action.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_player_control_reader<Base>::do_action
( bear::universe::time_type elapsed_time, unsigned int player_index,
  player_action::value_type a )
{
  // nothing to do
} // item_with_player_control_reader::do_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop an action asked by the human player.
 * \param player_index The index of the player asking the action.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_player_control_reader<Base>::stop_action
 ( unsigned int player_index, player_action::value_type a )
{
  // nothing to do
} // item_with_player_control_reader::stop_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to start the action associated with a keyboard key.
 * \param key The code of the key.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::key_pressed
( const bear::input::key_info& key )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_key(key.get_code());

      if ( action != player_action::action_null )
        {
          result = true;
          start_action( i, action );
        }
    }

  return result;
} // item_with_player_control_reader::key_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to stop the action associated with a keyboard key.
 * \param key The code of the key.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::key_released
( const bear::input::key_info& key )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_key(key.get_code());

      if ( action != player_action::action_null )
        {
          result = true;
          stop_action( i, action );
        }
    }

  return result;
} // item_with_player_control_reader::key_released()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to continue the action associated with a keyboard key.
 * \param elapsed_time How long the action is done.
 * \param key The code of the key.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::key_maintained
( bear::universe::time_type elapsed_time,
  const bear::input::key_info& key )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_key(key.get_code());

      if ( action != player_action::action_null )
        {
          result = true;
          do_action( elapsed_time, i, action );
        }
    }

  return result;
} // item_with_player_control_reader::key_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to start the action associated with a joystick button.
 * \param button The code of the button.
 * \param joy_index The index of the joystick.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::button_pressed
( bear::input::joystick::joy_code button, unsigned int joy_index )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_joystick(joy_index, button);

      if ( action != player_action::action_null )
        {
          result = true;
          start_action( i, action );
        }
    }

  return result;
} // item_with_player_control_reader::button_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to stop the action associated with a joystick button.
 * \param button The code of the button.
 * \param joy_index The index of the joystick.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::button_released
( bear::input::joystick::joy_code button, unsigned int joy_index )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_joystick(joy_index, button);

      if ( action != player_action::action_null )
        {
          result = true;
          stop_action( i, action );
        }
    }

  return result;
} // item_with_player_control_reader::button_released()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to continue the action associated with a joystick
 *        button.
 * \param elapsed_time How long the action is done.
 * \param button The code of the button.
 * \param joy_index The index of the joystick.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::button_maintained
( bear::universe::time_type elapsed_time,
 bear::input::joystick::joy_code button, unsigned int joy_index )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_joystick(joy_index, button);

      if ( action != player_action::action_null )
        {
          result = true;
          do_action( elapsed_time, i, action );
        }
    }

  return result;
} // item_with_player_control_reader::button_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to start the action associated with a mouse button.
 * \param button The code of the button.
 * \param pos The position of the cursor on the screen.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::mouse_pressed
( bear::input::mouse::mouse_code button,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_mouse(button);

      if ( action != player_action::action_null )
        {
          result = true;
          start_action( i, action );
        }
    }

  return result;
} // item_with_player_control_reader::mouse_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to stop the action associated with a mouse button.
 * \param button The code of the button.
 * \param pos The position of the cursor on the screen.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::mouse_released
( bear::input::mouse::mouse_code button,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_mouse(button);

      if ( action != player_action::action_null )
        {
          result = true;
          stop_action( i, action );
        }
    }

  return result;
} // item_with_player_control_reader::mouse_released()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the player to continue the action associated with a mouse button.
 * \param elapsed_time How long the action is done.
 * \param button The code of the button.
 * \param pos The position of the cursor on the screen.
 */
template<class Base>
bool ptb::item_with_player_control_reader<Base>::mouse_maintained
( bear::universe::time_type elapsed_time,
  bear::input::mouse::mouse_code button,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  bool result = false;
  const unsigned int m( game_variables::get_players_count() );
  controller_config cfg;

  for ( unsigned int i=1; i<=m; ++i )
    {
      const player_action::value_type action =
        cfg.get_layout(i).get_action_from_mouse(button);

      if ( action != player_action::action_null )
        {
          result = true;
          do_action( elapsed_time, i, action );
        }
    }

  return result;
} // item_with_player_control_reader::mouse_maintained()

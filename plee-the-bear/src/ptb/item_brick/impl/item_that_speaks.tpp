/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item_that_speaks.tpp
 * \brief Implementation of the bear::engine::item_that_speaks class.
 * \author Sebastien Angibaud
 */

#include "engine/level_globals.hpp"

#include "ptb/defines.hpp"
#include "ptb/message/balloon_layer_add_message.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
template<class Base>
void ptb::item_that_speaks<Base>::build()
{
  super::build();
  speaker_item::build( this->get_level_globals() );
  m_registered_in_layer = false;
} // item_that_speaks::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
template<class Base>
void ptb::item_that_speaks<Base>::pre_cache()
{
  super::pre_cache();

  this->get_level_globals().load_font("font/speech.fnt");
  this->get_level_globals().load_image("gfx/ui/balloon.png");
} // item_that_speaks::pre_cache()

/*---------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
template<class Base>
void ptb::item_that_speaks<Base>::progress
( bear::universe::time_type elapsed_time )
{
  super::progress(elapsed_time);
  speaker_item::progress(elapsed_time);

  if ( !m_registered_in_layer )
    {
      balloon_layer_add_message msg_speaker;
      msg_speaker.set_speaker(this);

      m_registered_in_layer = this->get_level_globals().send_message
              ( PTB_BALLOON_LAYER_DEFAULT_TARGET_NAME, msg_speaker );
    }
} // item_that_speaks::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type list of string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 *
 * Valid values for the \a name and \a value parameters are :
 *  - "speech", string
 *  - anything supported by the parent class
 */
template<class Base>
bool ptb::item_that_speaks<Base>::set_string_list_field
( const std::string& name, const std::vector<std::string>& value )
{
  bool ok = true;

  if (name == "item_that_speaks.speeches")
    speak(value);
  else
    ok = super::set_string_list_field(name, value);

  return ok;
} // item_that_speaks::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool ptb::item_that_speaks<Base>::set_bool_field
( const std::string& name, bool value )
{
  bool ok = true;

  if (name == "item_that_speaks.persistent_balloon")
    set_persistent_balloon(value);
  else
    ok = super::set_bool_field(name, value);

  return ok;
} // item_that_speaks::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Export the methods of the class.
 */
template<class Base>
void ptb::item_that_speaks<Base>::init_exported_methods()
{
  TEXT_INTERFACE_CONNECT_PARENT_METHOD_1
    ( item_that_speaks<Base>, speaker_item, speak, void,
      const std::vector<std::string>& );
} // item_that_speaks::init_exported_methods()

/*----------------------------------------------------------------------------*/
TEXT_INTERFACE_IMPLEMENT_TEMPLATE_METHOD_LIST_1( ptb::item_that_speaks,
                                                 typename, Base )

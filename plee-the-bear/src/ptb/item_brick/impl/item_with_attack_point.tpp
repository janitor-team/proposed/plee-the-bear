/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file item_with_attack_point.tpp
 * \brief Implementation of the ptb::item_with_attack_point class.
 * \author Sébastien Angibaud
 */
#include "engine/base_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * Return the prefered position where this item can be attacked.
 */
template<class Base>
bear::universe::position_type
ptb::item_with_attack_point<Base>::get_attack_point() const
{
  return this->get_center_of_mass();
} // item_with_attack_point::get_attack_point()

/*----------------------------------------------------------------------------*/
/**
 * Tell if the value returned by get_attack_point() is meaningful.
 */
template<class Base>
bool ptb::item_with_attack_point<Base>::has_attack_point() const
{
  return true;
} // item_with_attack_point::has_attack_point()


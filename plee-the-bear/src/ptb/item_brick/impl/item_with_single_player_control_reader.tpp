/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file with_input_reader.cpp
 * \brief Implementation of the ptb::item_with_single_player_control_reader
 *        class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
template<class Base>
ptb::item_with_single_player_control_reader<Base>::
item_with_single_player_control_reader()
  : m_player_index(1)
{

} // item_with_single_player_control_reader::item_with_single_player_control_reader()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c unsigned \c integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool ptb::item_with_single_player_control_reader<Base>::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result = true;

  if ( name == "item_with_single_player_control_reader.player_index" )
    m_player_index = value;
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // item_with_single_player_control_reader::set_u_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the index of the player to listen to.
 */
template<class Base>
unsigned int
ptb::item_with_single_player_control_reader<Base>::get_player_index() const
{
  return m_player_index;
} // item_with_single_player_control_reader::get_player_index()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start an action asked by the human player.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_single_player_control_reader<Base>::start_action
( player_action::value_type a )
{
  // nothing to do
} // item_with_single_player_control_reader::start_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Continue an action asked by the human player.
 * \param elapsed_time How long the action is done.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_single_player_control_reader<Base>::do_action
( bear::universe::time_type elapsed_time, player_action::value_type a )
{
  // nothing to do
} // item_with_single_player_control_reader::do_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop an action asked by the human player.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_single_player_control_reader<Base>::stop_action
( player_action::value_type a )
{
  // nothing to do
} // item_with_single_player_control_reader::stop_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the index of the player to listen to.
 * \param i The index of the player.
 */
template<class Base>
void ptb::item_with_single_player_control_reader<Base>::set_player_index
( unsigned int i )
{
  m_player_index = i;
} // item_with_single_player_control_reader::set_player_index()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start an action asked by the human player.
 * \param player_index The index of the player asking the action.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_single_player_control_reader<Base>::start_action
( unsigned int player_index, player_action::value_type a )
{
  if ( player_index == m_player_index )
    start_action(a);
} // item_with_single_player_control_reader::start_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Continue an action asked by the human player.
 * \param elapsed_time How long the action is done.
 * \param player_index The index of the player asking the action.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_single_player_control_reader<Base>::do_action
( bear::universe::time_type elapsed_time, unsigned int player_index,
  player_action::value_type a )
{
  if ( player_index == m_player_index )
    do_action(elapsed_time, a);
} // item_with_single_player_control_reader::do_action()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop an action asked by the human player.
 * \param player_index The index of the player asking the action.
 * \param a The action.
 */
template<class Base>
void ptb::item_with_single_player_control_reader<Base>::stop_action
( unsigned int player_index, player_action::value_type a )
{
  if ( player_index == m_player_index )
    stop_action(a);
} // item_with_single_player_control_reader::stop_action()

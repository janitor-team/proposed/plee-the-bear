/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file code/activate_on_players.cpp
 * \brief Implementation of the ptb::activate_on_players class.
 * \author Julien Jorge
 */
#include "ptb/item_brick/activate_on_players.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Desctructor.
 */
ptb::activate_on_players::~activate_on_players()
{
  // nothing to do
} // activate_on_players::~activate_on_players()

/*----------------------------------------------------------------------------*/
/**
 * \brief Only one of the two players is present in the event.
 * \param p The player.
 * \remark This method is called only in a two players game.
 */
void ptb::activate_on_players::on_one_player( const player_proxy& p )
{
  // nothing to do
} // activate_on_players::on_one_player()

/*----------------------------------------------------------------------------*/
/**
 * \brief All players are present in the event.
 * \param p1 The first player.
 * \param p2 The second player. NULL in a single player game.
 */
void ptb::activate_on_players::on_all_players
( const player_proxy& p1, const player_proxy& p2 )
{
  // nothing to do
} // activate_on_players::on_all_players()

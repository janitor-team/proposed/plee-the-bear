/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file balloon.cpp
 * \brief Implementation of the ptb::balloon class.
 * \author Sébastien Angibaud
 */
#include "ptb/balloon.hpp"

#include "visual/scene_sprite.hpp"

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor of a balloon.
 */
ptb::balloon::balloon()
  : m_size_frame(0, 0), m_has_started(false), m_time(0), m_on_top(true),
    m_on_right(true), m_active(false)
{
  m_text.set_margin(0, 0);
} // balloon::balloon()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the size of the balloon.
 * \param elapsed_time The time elapsed since the last call.
 */
void ptb::balloon::progress( bear::universe::time_type elapsed_time )
{
  if ( m_has_started )
    {
      m_time += elapsed_time;

      if ( m_time >= m_play_time)
        {
          if ( m_speeches.empty() )
            decrease();
          else
            {
              m_time = 0;
              write_text();
            }
        }
    }
  else
    {
      increase();
      if ( ( m_text.width() == m_size_frame.x ) &&
           ( m_text.height() == m_size_frame.y ) )
        m_has_started = true;
    }
} // balloon::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the visuals of the balloon.
 * \param e (out) The visuals.
 */
void ptb::balloon::render( std::list<bear::visual::scene_element>& e )
{
  if ( (m_text.width() == 0) || (m_text.height() == 0) )
    return;

  m_text.render( e );

  m_horizontal_border.flip(false);
  e.push_back
    ( bear::visual::scene_sprite
      (m_text.left(), m_text.bottom() - m_horizontal_border.height(),
       m_horizontal_border) );

  m_horizontal_border.flip(true);
  e.push_back
    ( bear::visual::scene_sprite
      (m_text.left(), m_text.top(), m_horizontal_border) );

  m_vertical_border.mirror(false);
  e.push_back
    ( bear::visual::scene_sprite
      (m_text.left() - m_vertical_border.width(), m_text.bottom(),
       m_vertical_border) );

  m_vertical_border.mirror(true);
  e.push_back
    ( bear::visual::scene_sprite
      (m_text.right(), m_text.bottom(), m_vertical_border) );

  if ( m_on_right )
    {
      if (m_on_top)
        {
          render_bottom_left_corner(e, m_spike);
          render_bottom_right_corner(e, m_corner);
          render_top_left_corner(e, m_corner);
          render_top_right_corner(e, m_corner);
        }
      else
        {
          render_bottom_left_corner(e, m_corner);
          render_bottom_right_corner(e, m_corner);
          render_top_left_corner(e, m_spike);
          render_top_right_corner(e, m_corner);
        }
    }
  else if (m_on_top)
    {
      render_bottom_left_corner(e, m_corner);
      render_bottom_right_corner(e, m_spike);
      render_top_left_corner(e, m_corner);
      render_top_right_corner(e, m_corner);
    }
  else
    {
      render_bottom_left_corner(e, m_corner);
      render_bottom_right_corner(e, m_corner);
      render_top_left_corner(e, m_corner);
      render_top_right_corner(e, m_spike);
    }
} // balloon::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 * \param glob The level globals in which we load the media.
 */
void ptb::balloon::build( bear::engine::level_globals& glob )
{
  m_spike = glob.auto_sprite("gfx/ui/balloon.png", "spike");
  m_corner = glob.auto_sprite("gfx/ui/balloon.png", "corner");
  m_horizontal_border =
    glob.auto_sprite("gfx/ui/balloon.png", "horizontal border");
  m_vertical_border = glob.auto_sprite("gfx/ui/balloon.png", "vertical border");

  m_text.set_background_color( claw::graphic::white_pixel );
  m_text.set_font( glob.get_font("font/speech.fnt") );

  if ( !m_speeches.empty() )
    write_text();

  m_size_frame = m_text.get_size();

  m_text.set_size( 0, 0 );
} // balloon::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Test if the balloon is finished.
 */
bool ptb::balloon::is_finished() const
{
  return !m_active;
} // balloon::is_finished()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size of the balloon.
 */
bear::visual::size_box_type ptb::balloon::get_size() const
{
  bear::visual::size_box_type result;

  result.x = m_text.width() + m_vertical_border.width() + m_spike.width();
  result.y = m_text.height() + m_horizontal_border.height() + m_spike.height();

  return result;
} // balloon::get_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the final size of the balloon.
 */
bear::visual::size_box_type ptb::balloon::get_final_size() const
{
  bear::visual::size_box_type result(m_size_frame);

  result.x += m_vertical_border.width() + m_spike.width();
  result.y += m_horizontal_border.height() + m_spike.height();

  return result;
} // balloon::get_final_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the position of the balloon.
 * \param pos The position of the balloon.
 * \param on_top Tell that the balloon is on the top of the item.
 * \param on_right Tell that the balloon is on the right of the item.
 */
void ptb::balloon::set_position
( const bear::visual::position_type& pos, bool on_top, bool on_right )
{
  m_on_right = on_right;
  m_on_top = on_top;

  bear::visual::position_type delta;

  if (m_on_right)
    delta.x = m_spike.width();
  else
    delta.x = m_vertical_border.width() + (m_size_frame.x - m_text.width());

  if (m_on_top)
    delta.y = m_spike.height();
  else
    delta.y = m_horizontal_border.height() + (m_size_frame.y - m_text.height());

  m_text.set_position(pos + delta);
} // balloon::set_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the position of the balloon.
 */
bear::visual::position_type ptb::balloon::get_position() const
{
  bear::visual::position_type delta;

  if (m_on_right)
    delta.x = -m_spike.width();
  else
    delta.x = -m_vertical_border.width();

  if (m_on_top)
    delta.y = -m_spike.height();
  else
    delta.y = -m_horizontal_border.height();

  return m_text.bottom_left() + delta;
} // balloon::get_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the balloon is on the top of the item.
 */
bool ptb::balloon::is_on_top() const
{
  return m_on_top;
} // balloon::is_on_top()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the balloon is on the right of the item.
 */
bool ptb::balloon::is_on_right() const
{
  return m_on_right;
} // balloon::is_on_right()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a new list of things to say.
 * \param speeches Speeches said by the speaker.
 */
void ptb::balloon::set_speeches( const std::list<std::string>& speeches )
{
  m_speeches = speeches;
  m_has_started = false;
  m_time = 0;

  if ( !m_speeches.empty() )
    write_text();

  m_size_frame = m_text.get_size();

  m_text.set_size( 0, 0 );
  m_active = true;
} // balloon::set_speeches()

/*----------------------------------------------------------------------------*/
/**
 * \brief Close the balloon. All speeches are removed.
 */
void ptb::balloon::close()
{
  m_speeches.clear();
  m_text.set_size( 0, 0 );
  m_active = false;
} // balloon::close()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render a sprite in the bottom left corner.
 * \param e (out) The visuals.
 * \param s The sprite to render.
 */
void ptb::balloon::render_bottom_left_corner
( std::list<bear::visual::scene_element>& e, bear::visual::sprite s ) const
{
  s.mirror(false);
  s.flip(false);

  e.push_back
    ( bear::visual::scene_sprite
      (m_text.left() - s.width(), m_text.bottom() - s.height(), s) );
} // balloon::render_bottom_left_corner()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render a sprite in the bottom left corner.
 * \param e (out) The visuals.
 * \param s The sprite to render.
 */
void ptb::balloon::render_bottom_right_corner
( std::list<bear::visual::scene_element>& e, bear::visual::sprite s ) const
{
  s.mirror(true);
  s.flip(false);

  e.push_back
    ( bear::visual::scene_sprite
      (m_text.right(), m_text.bottom() - s.height(), s) );
} // balloon::render_bottom_right_corner()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render a sprite in the bottom left corner.
 * \param e (out) The visuals.
 * \param s The sprite to render.
 */
void ptb::balloon::render_top_left_corner
( std::list<bear::visual::scene_element>& e, bear::visual::sprite s ) const
{
  s.mirror(false);
  s.flip(true);

  e.push_back
    ( bear::visual::scene_sprite(m_text.left() - s.width(), m_text.top(), s) );
} // balloon::render_top_left_corner()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render a sprite in the top right corner.
 * \param e (out) The visuals.
 * \param s The sprite to render.
 */
void ptb::balloon::render_top_right_corner
( std::list<bear::visual::scene_element>& e, bear::visual::sprite s ) const
{
  s.mirror(true);
  s.flip(true);

  e.push_back( bear::visual::scene_sprite(m_text.right(), m_text.top(), s) );
} // balloon::render_top_right_corner()

/*----------------------------------------------------------------------------*/
/**
 * \brief Increase the balloon.
 */
void ptb::balloon::increase()
{
  bear::visual::size_box_type size( m_text.get_size() );

  if ( size.x <= m_size_frame.x - 10 )
    size.x += 10;
  else
    size.x = m_size_frame.x;

  if ( size.y <= m_size_frame.y - 10 )
    size.y += 10;
  else
    size.y = m_size_frame.y;

  set_content_size(size);
} // balloon::increase()

/*----------------------------------------------------------------------------*/
/**
 * \brief Decrease the balloon.
 */
void ptb::balloon::decrease()
{
  bear::visual::size_box_type size(m_text.get_size());

  if ( size.x >= 10 )
    size.x -= 10;
  else
    size.x = 0;

  if ( size.y >= 1 )
    size.y -= 1;
  else
    size.y = 0;

  set_content_size(size);

  m_active = (size.x > 0) || (size.y > 0);
} // balloon::decrease()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the size of the content of the balloon.
 */
void ptb::balloon::set_content_size( const bear::visual::size_box_type& s )
{
  m_text.set_size(s);
  m_horizontal_border.set_width(s.x);
  m_vertical_border.set_height(s.y);

  if ( !m_on_top )
    m_text.set_bottom( m_text.top() - m_text.height() );

  if ( !m_on_right )
    m_text.set_left( m_text.right() - m_text.width() );
} // balloon::set_content_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Write the new text.
 */
void ptb::balloon::write_text()
{
  m_text.set_position( 0, 0 );
  m_text.set_auto_size(true);
  m_text.set_text(m_speeches.front());
  m_text.set_auto_size(false);

  if ( m_text.width() > 200 )
    {
      m_text.set_width(200);
      m_text.expand_vertically();
    }

  set_content_size(m_text.get_size());
  m_play_time = m_speeches.front().length()/10;

  if ( m_play_time < 2 )
    m_play_time = 2;
  else if ( m_play_time > 6 )
    m_play_time = 6;

  m_speeches.pop_front();
} // balloon::write_text()

/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file speaker_item.cpp
 * \brief Implementation of the ptb::speaker_item class.
 * \author Sébastien Angibaud
 */
#include "ptb/speaker_item.hpp"

#include <libintl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
ptb::speaker_item::speaker_item()
  : m_persistent_balloon(false)
{
  // nothing to do
} // speaker_item::speaker_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 * \param that The instance to copy from.
 */
ptb::speaker_item::speaker_item( const speaker_item& that )
  : m_speeches(that.m_speeches), m_persistent_balloon(that.m_persistent_balloon)
{

} // speaker_item::speaker_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
ptb::speaker_item::~speaker_item()
{
  // must exist and be virtual for dynamic_casts
} // speaker_item::~speaker_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 * \param glob The level globals in which we take the resources.
 */
void ptb::speaker_item::build( bear::engine::level_globals& glob )
{
  m_balloon.build(glob);
} // speaker_item::build()

/*---------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void ptb::speaker_item::progress( bear::universe::time_type elapsed_time )
{
  m_balloon.progress(elapsed_time);

  if ( has_finished_to_chat() && has_more_things_to_say() )
    {
      m_balloon.set_speeches(m_speeches.front());
      m_speeches.pop_front();
    }
} // speaker_item::create_balloon()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if the speaker has finished to chat.
 */
bool ptb::speaker_item::has_finished_to_chat() const
{
  return m_balloon.is_finished();
} // speaker_item::has_finished_to_chat()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if the speaker has more things to say.
 */
bool ptb::speaker_item::has_more_things_to_say() const
{
  return !m_speeches.empty();
} // speaker_item::has_more_things_to_say()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop to speak.
 */
void ptb::speaker_item::stop_to_speak()
{
  m_balloon.close();
} // speaker_item::stop_to_speak()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start a speech.
 * \param speech The new speech.
 */
void ptb::speaker_item::speak(const std::vector<std::string>& speech)
{
  std::list<std::string> s;
  for ( std::size_t i=0; i!=speech.size(); ++i )
    s.push_back( gettext(speech[i].c_str()) );

  m_speeches.push_back(s);
} // speaker_item::speak()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start a speech.
 * \param s The sentence to say.
 *
 * \a s is added at the end of the pending speeches.
 */
void ptb::speaker_item::speak(const std::string& s)
{
  std::list<std::string> t;
  t.push_back(gettext(s.c_str()));
  m_speeches.push_back(t);
} // speaker_item::speak()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the balloon.
 */
ptb::balloon& ptb::speaker_item::get_balloon()
{
  return m_balloon;
} // speaker_item::get_balloon()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the persistency of the balloon.
 * \param b Persistent or not.
 */
void ptb::speaker_item::set_persistent_balloon( bool b )
{
  m_persistent_balloon = b;
} // speaker_item::set_persistent_balloon()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the balloon is persistent.
 */
bool ptb::speaker_item::get_persistent_balloon() const
{
  return m_persistent_balloon;
} // speaker_item::get_persistent_balloon()

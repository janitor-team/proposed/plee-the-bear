/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file gui_command.cpp
 * \brief Implementation of the ptb::gui_command class.
 * \author Julien Jorge
 */
#include "ptb/gui_command.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert an action into its string representation.
 * \param a The action to convert.
 */
std::string ptb::gui_command::to_string( value_type a )
{
  std::string result;

  switch(a)
    {
    case null_command: result = "null_command" ; break;
    case pause:        result = "pause" ; break;
    case talk:         result = "talk" ; break;
    default:
      result = "not_a_command";
    }

  return result;
} // gui_command::to_string()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a string into an action.
 * \param s The action to convert.
 */
ptb::gui_command::value_type
ptb::gui_command::from_string( const std::string& s )
{
  value_type a(null_command);

  if ( s == "pause" )
    a = pause;
  else if ( s == "talk" )
    a = talk;

  return a;
} // gui_command::from_string()

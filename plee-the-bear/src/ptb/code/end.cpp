/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \brief Implementation of the functions called at the end of
 *        the game.
 * \author Sébastien Angibaud
 */
#include "ptb/end.hpp"
#include "ptb/defines.hpp"
#include "ptb/game_variables.hpp"
#include "engine/game.hpp"
#include "engine/resource_pool.hpp"
#include "engine/variable/variable.hpp"

#include <libintl.h>
#include <claw/logger.hpp>
#include <claw/string_algorithm.hpp>
#include <claw/configuration_file.hpp>
#include <sstream>


/*----------------------------------------------------------------------------*/
/**
 * \brief Save minigames that are unlocked.
 */
void save_mini_game()
{
  std::ofstream f1
    ( bear::engine::game::get_instance().get_custom_game_file
      (PTB_UNLOCKED_MINI_GAME_FILENAME).c_str() );

  if ( !f1 )
    claw::logger << claw::log_warning
                 << "Can't open list of mini-games in file '"
                 << bear::engine::game::get_instance().get_custom_game_file
      (PTB_UNLOCKED_MINI_GAME_FILENAME).c_str()
                 << "'." << std::endl;
  else
    {
      std::stringstream f;
      bear::engine::resource_pool::get_instance().get_file
        (PTB_MINI_GAME_INFORMATIONS, f);

      CLAW_PRECOND( f );

      if (f)
        {
          claw::configuration_file config(f);

          claw::configuration_file::const_file_iterator it;
          for (it=config.file_begin(); it!=config.file_end(); ++it)
            {
              bear::engine::variable<bool> v("mini-game/"+*it);

              if ( bear::engine::game::get_instance().
                   game_variable_exists(v) )
                {
                  bear::engine::game::get_instance().
                    get_game_variable(v);

                  if ( v.get_value() )
                    f1 << *it << std::endl;
                }
            }
        }
      else
        claw::logger << claw::log_error
                     << "can't find file named '"
                     << PTB_MINI_GAME_INFORMATIONS << "'."  << std::endl;
    }
} // save_mini_game()

void end_plee_the_bear()
{
  save_mini_game();
} // end_plee_the_bear()

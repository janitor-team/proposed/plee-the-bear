/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [PTB] in the subject of your mails.
*/
/**
 * \file playability_type.cpp
 * \brief Implementation of the ptb::playability_type class.
 * \author Sébastien Angibaud
 */
#include "ptb/playability_type.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert an playability_type into its string representation.
 * \param a The value to convert.
 */
std::string ptb::playability_type::to_string( value_type a )
{
  std::string result;

  switch(a)
    {
    case one_player_only:   result = "one_player_only" ; break;
    case two_players_only:   result = "two_players_only" ; break;
    default:
      result = "one_or_two_players";
    }

  return result;
} // playability_type::to_string()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a string into an playability_type.
 * \param s The value to convert.
 */
ptb::playability_type::value_type
ptb::playability_type::from_string( const std::string& s )
{
  value_type a(one_or_two_players);

  if ( s == "one_player_only" )
    a = one_player_only;
  else if ( s == "two_players_only" )
    a = two_players_only;

  return a;
} // playability_type::from_string()

#!/bin/sh
#
#  Copyright (C) 2005-2010 Julien Jorge, Sebastien Angibaud
#
#  This program is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the
#  Free Software Foundation; either version 2 of the License, or (at your
#  option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along
#  with this program; if not, write to the Free Software Foundation, Inc.,
#  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
#  contact: plee-the-bear@gamned.org
#

#-------------------------------------------------------------------------------
# Beginning of the variable declaration

# The name of the level file to run
LEVEL_FILE=

# The number of players
PLAYERS_COUNT=1

# The name of the profile
START_LEVEL=

# The name of the last exit used by the first player
PLAYER_1_EXIT=

# The name of the last exit used by the second player
PLAYER_2_EXIT=

# The name of the profile
PROFILE_NAME="$(whoami)"

#-------------------------------------------------------------------------------
# End of the variable declaration

#-------------------------------------------------------------------------------
# This function asks a value to the user and returns his answer.
# \param $1 A description of the value to ask
# \param $2 The default value
ask() {
  local RESULT="$2"
  local ANSWER=

  if [ "$2" = "" ]
  then
    read -p "$1: " ANSWER
  else
    read -p "$1 [$2]: " ANSWER
  fi

  if [ "$ANSWER" != "" ]
  then
    RESULT="$ANSWER"
  fi

  echo "$RESULT"
} # ask()

#-------------------------------------------------------------------------------
# Run the game.
run_game() {

  ./plee-the-bear $GDB \
    --set-game-var-uint=scenario/players_count="$PLAYERS_COUNT" \
    --set-game-var-string=persistent/scenario/profile_name="$PROFILE_NAME" \
    --set-game-var-string=persistent/"$LEVEL_FILE"/player_1/last_exit="$PLAYER_1_EXIT" \
    --set-game-var-string=persistent/"$LEVEL_FILE"/player_2/last_exit="$PLAYER_2_EXIT" \
    --start-level="$LEVEL_FILE" $ARGS
} # run_game()

#-------------------------------------------------------------------------------
# Run the game with the previous configuration.
auto_run_game() {

  echo -n "Auto starting level $LEVEL_FILE with profile $PROFILE_NAME.";
  echo -n " The first player came from"

  if [ "$PLAYER_1_EXIT" = "" ]
  then
    echo -n " an anonymous exit."
  else
    echo -n " exit $PLAYER_1_EXIT"
  fi

  if [ "$PLAYERS_COUNT" != 1 ]
  then
    echo -n " The second player came from"

    if [ "$PLAYER_2_EXIT" = "" ]
    then
      echo -n " an anonymous exit."
    else
      echo -n " exit $PLAYER_2_EXIT"
    fi
  fi

  echo

  run_game;

} # auto_run_game()

#-------------------------------------------------------------------------------
# Print the available exits
print_exits()
{
    echo
    echo "Available exit names for player #$1:"
    grep -A 1 'player_start_position.\(exit_name\|player_index\)' \
        ../data/$LEVEL_FILE \
        | grep -v '^player_start_position.\(exit_name\|player_index\)' \
        | grep -v -- -- \
        | tr '\n' ' ' \
        | sed 's: \([0-9]\): \1\n:g' \
        | awk '{ if ( $2 == "" ) print $1 " "; else print $2 " " $1; }' \
        | grep ^$1 \
        | cut -d' ' -f2
} # print_exits()

AUTO_RUN=
GDB=
ARGS=

for ARG in $@
do
    case "$ARG" in
        --gdb) GDB=gdb;
            ;;
	--again) AUTO_RUN=1;
	    ;;
	--help) echo "$0 [--again] [--help]";
	    echo;
	    echo "This programs fills the arguments required to run Plee the Bear with values asked to the user.";
	    echo;
	    echo "\t--again\tAutomatically run the game with the last configuration.";
	    echo "\t--gdb\tRun the game with gdb.";
	    echo "\t--help\tPrint this message and exit.";
            echo
            ./plee-the-bear --help
	    exit;
	    ;;
        *) ARGS="$ARGS $ARG";
            ;;
    esac
done

# All the parameters will be saved in this file and read again the next time
# this script is read
VARS_FILE=./vars.sh

if [ -f "$VARS_FILE" ]
then
  . "$VARS_FILE"

  if [ "$AUTO_RUN" != "" ]
  then
      auto_run_game;
      exit;
  fi
fi

# Ask for the not determined variables
cd ../data
echo "Available levels:"
find level -name "*.cl" | column
cd - > /dev/null

LEVEL_FILE=$(ask 'Name of the level file' "$LEVEL_FILE")
PLAYERS_COUNT=$(ask 'Number of players' "$PLAYERS_COUNT")

print_exits 1
PLAYER_1_EXIT=$(ask 'Last exit for the first player (* to clear)' "$PLAYER_1_EXIT" | sed 's:^\*$::')

if [ "$PLAYERS_COUNT" != "1" ]
then
    print_exits 2
    PLAYER_2_EXIT=$(ask 'Last exit for the second player (* to clear)' "$PLAYER_2_EXIT" | sed 's:^\*$::')
fi

PROFILE_NAME=$(ask 'Name of the profile to use' "$PROFILE_NAME")

# Save the customisation
(
  echo LEVEL_FILE=\"$LEVEL_FILE\"
  echo PLAYERS_COUNT=\"$PLAYERS_COUNT\"
  echo PLAYER_1_EXIT=\"$PLAYER_1_EXIT\"
  echo PLAYER_2_EXIT=\"$PLAYER_2_EXIT\"
  echo PROFILE_NAME=\"$PROFILE_NAME\"
) > "$VARS_FILE"

run_game;

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file check_item_class_creator.hpp
 * \brief This class is a boolean expression of type "check_item_class".
 * \author Julien Jorge
 */
#ifndef __BEAR_CHECK_ITEM_CLASS_CREATOR_HPP__
#define __BEAR_CHECK_ITEM_CLASS_CREATOR_HPP__

#include "engine/base_item.hpp"
#include "engine/expr/check_item_class.hpp"
#include "engine/item_brick/with_boolean_expression_creation.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class is a boolean expression of type "check_item_class".
   *
   * This item is a parameter for the items of type applied_boolean_expression.
   * He kills himself at the end of his build() method.
   *
   * The fields of this item are
   *  - \a collision_data: (item) \b [required] \c the item from which we take
   *    the collision data. Must inherit from engine::with_trigger,
   *  - \a class_name: (string) \b [required] \c the name of the class to pass
   *    to check_item_class.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT check_item_class_creator:
    public engine::base_item,
    public engine::with_boolean_expression_creation
  {
    DECLARE_BASE_ITEM(check_item_class_creator);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    bool set_string_field( const std::string& name, const std::string& value );
    bool set_item_field( const std::string& name, engine::base_item* value );
    bool is_valid() const;

    void build();

  private:
    virtual expr::boolean_expression do_get_expression() const;

  private:
    /** \brief The expression created by this item. */
    engine::check_item_class m_expr;

  }; // class check_item_class_creator
} // namespace bear

#endif // __BEAR_CHECK_ITEM_CLASS_CREATOR_HPP__

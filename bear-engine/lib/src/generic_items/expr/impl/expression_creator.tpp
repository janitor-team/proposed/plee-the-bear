/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file expression_creator.cpp
 * \brief Implementation of the bear::expression_creator class.
 * \author Julien Jorge
 */

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
template<typename Result, typename LeftOperand, typename RightOperand>
bool bear::binary_expression_creator
<Result, LeftOperand, RightOperand>::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( name == "binary_expression_creator.left_operand" )
    {
      left_creation_class_type* e
        ( dynamic_cast<left_creation_class_type*>(value) );

      if ( e != NULL )
        m_expr.set_left_operand(e->get_expression());
      else
        claw::logger << claw::log_error << name
                     << ": item is not of a valid type." << std::endl;
    }
  else if ( name == "binary_expression_creator.right_operand" )
    {
      right_creation_class_type* e
        ( dynamic_cast<right_creation_class_type*>(value) );

      if ( e != NULL )
        m_expr.set_right_operand(e->get_expression());
      else
        claw::logger << claw::log_error << name
                     << ": item is not of a valid type." << std::endl;
    }
  else
    result = super::set_item_field(name, value);

  return result;
} // binary_expression_creator::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
template<typename Result, typename LeftOperand, typename RightOperand>
void bear::binary_expression_creator<Result, LeftOperand, RightOperand>::build()
{
  kill();
} // binary_expression_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression created by this item.
 */
template<typename Result, typename LeftOperand, typename RightOperand>
typename bear::binary_expression_creator
<Result, LeftOperand, RightOperand>::expression_type
bear::binary_expression_creator
<Result, LeftOperand, RightOperand>::do_get_expression() const
{
  return m_expr;
} // binary_expression_creator::do_get_expression()

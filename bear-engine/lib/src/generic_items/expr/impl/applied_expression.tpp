/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file applied_expression.tpp
 * \brief Implementation of the bear::applied_expression class.
 * \author Julien Jorge
 */

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
template<typename Expression>
bool bear::applied_expression<Expression>::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( name == "applied_expression.expression" )
    {
      creation_class_type* e( dynamic_cast<creation_class_type*>(value) );

      if ( e != NULL )
        m_expression = e->get_expression();
      else
        claw::logger << claw::log_error << name << ": item is not of a type "
                     << "'with_expression_creation'." << std::endl;
    }
  else
    result = super::set_item_field(name, value);

  return result;
} // applied_expression::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <list of item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
template<typename Expression>
bool bear::applied_expression<Expression>::set_item_list_field
( const std::string& name, const std::vector<engine::base_item*>& value )
{
  bool result = true;

  if ( name == "applied_expression.receiver" )
    for ( std::size_t i=0; i!=value.size(); ++i )
      {
        assignment_class_type* a
          ( dynamic_cast<assignment_class_type*>(value[i]) );

        if ( a != NULL )
          m_items.push_back(a);
        else
          claw::logger << claw::log_error << name
                       << ": item #" << i
                       << " is not of type 'with_expression_assignment'."
                       << std::endl;
      }
  else
    result = super::set_item_list_field(name, value);

  return result;
} // applied_expression::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if all required fields are initialized.
 */
template<typename Expression>
bool bear::applied_expression<Expression>::is_valid() const
{
  return !m_items.empty();
} // applied_expression::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
template<typename Expression>
void bear::applied_expression<Expression>::build()
{
  for (unsigned int i=0; i!=m_items.size(); ++i)
    m_items[i]->set_expression(m_expression);

  kill();
} // applied_expression::build()

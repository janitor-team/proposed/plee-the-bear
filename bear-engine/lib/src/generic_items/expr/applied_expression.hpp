/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file applied_expression.hpp
 * \brief This class gives a boolean expression to one engine::with_trigger or
 *        more, then die.
 * \author Julien Jorge
 */
#ifndef __BEAR_APPLIED_EXPRESSION_HPP__
#define __BEAR_APPLIED_EXPRESSION_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/with_expression_assignment.hpp"
#include "engine/item_brick/with_expression_creation.hpp"

#include "generic_items/class_export.hpp"
#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief This class gives a boolean expression to one engine::with_trigger or
   *        more, then die.
   *
   * The fields of this item are
   *  - \a expression: (item) \b [required] The expression. Must inherit from
   *    engine::with_expression_creation,
   *  - \a trigger: (item) \b [required] The triggers to which the condition
   *    is applied. Must inherit from engine::with_trigger.
   *
   * \author Julien Jorge
   */
  template<typename Expression>
  class applied_expression:
    public engine::base_item
  {
  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  private:
    /** \brief The base class of the expression. */
    typedef typename Expression::base_expression_type base_expression_type;

    /** \brief The type of the instances receiving the expression. */
    typedef typename engine::with_expression_assignment
    <
      base_expression_type
    >::assignment_class_type
    assignment_class_type;

    /** \brief The type of the instances from which we get the expression. */
    typedef typename engine::with_expression_creation
    <
      base_expression_type
    >::creation_class_type
    creation_class_type;

  public:
    bool set_item_field( const std::string& name, engine::base_item* value );
    bool set_item_list_field
    ( const std::string& name, const std::vector<engine::base_item*>& value );

    bool is_valid() const;

    void build();

  private:
    /** \brief The items receiving the expression. */
    std::vector<assignment_class_type*> m_items;

    /** \brief The expression. */
    Expression m_expression;

  }; // class applied_expression

  /** \brief Applied expression for boolean expressions. */
  class GENERIC_ITEMS_EXPORT applied_boolean_expression:
    public applied_expression<expr::boolean_expression>
  {
    DECLARE_BASE_ITEM(applied_boolean_expression);
  }; // class applied_boolean_expression

  /** \brief Applied expression for linear expressions. */
  class GENERIC_ITEMS_EXPORT applied_linear_expression:
    public applied_expression<expr::linear_expression>
  {
    DECLARE_BASE_ITEM(applied_linear_expression);
  }; // class applied_linear_expression

} // namespace bear

#include "generic_items/expr/impl/applied_expression.tpp"

#endif // __BEAR_APPLIED_EXPRESSION_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file get_toggle_status_creator.cpp
 * \brief Implementation of the bear::get_toggle_status_creator class.
 * \author Julien Jorge
 */
#include "generic_items/expr/get_toggle_status_creator.hpp"

#include "engine/item_brick/with_toggle.hpp"

#include "engine/export.hpp"

#include <claw/logger.hpp>

BASE_ITEM_EXPORT( get_toggle_status_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::get_toggle_status_creator::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( name == "get_toggle_status_creator.toggle")
    {
      engine::with_toggle* t( dynamic_cast<engine::with_toggle*>(value) );

      if ( t != NULL )
        m_expr.set_toggle( *value );
      else
        claw::logger << claw::log_error << name
                     << ": the item is not of type 'engine::with_toggle'."
                     << std::endl;
    }
  else
    result = super::set_item_field(name, value);

  return result;
} // get_toggle_status_creator::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::get_toggle_status_creator::build()
{
  kill();
} // get_toggle_status_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if all required fields are initialized.
 */
bool bear::get_toggle_status_creator::is_valid() const
{
  return (m_expr.get_toggle() != NULL) && super::is_valid();
} // get_toggle_status_creator::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression created by this item.
 */
bear::expr::boolean_expression
bear::get_toggle_status_creator::do_get_expression() const
{
  return m_expr;
} // get_toggle_status_creator::do_get_expression()

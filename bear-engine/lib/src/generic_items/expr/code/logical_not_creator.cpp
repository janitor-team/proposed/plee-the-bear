/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file logical_not_creator.cpp
 * \brief Implementation of the bear::logical_not_creator class.
 * \author Julien Jorge
 */
#include "generic_items/expr/logical_not_creator.hpp"

#include <claw/logger.hpp>

BASE_ITEM_EXPORT( logical_not_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::logical_not_creator::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( name == "logical_not_creator.operand" )
    {
      engine::with_boolean_expression_creation* e
        ( dynamic_cast<engine::with_boolean_expression_creation*>(value) );

      if ( e != NULL )
        m_expr.set_operand(e->get_expression());
      else
        claw::logger << claw::log_error << name << ": item is not of a type "
                     << "'with_boolean_expression_creation'." << std::endl;
    }
  else
    result = super::set_item_field(name, value);

  return result;
} // logical_not_creator::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::logical_not_creator::build()
{
  kill();
} // logical_not_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression created by this item.
 */
bear::expr::boolean_expression
bear::logical_not_creator::do_get_expression() const
{
  return m_expr;
} // logical_not_creator::do_get_expression()

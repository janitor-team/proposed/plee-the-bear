/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file check_item_class_creator.cpp
 * \brief Implementation of the bear::check_item_class_creator class.
 * \author Julien Jorge
 */
#include "generic_items/expr/check_item_class_creator.hpp"

#include "engine/item_brick/with_trigger.hpp"

#include "engine/export.hpp"

#include <claw/logger.hpp>

BASE_ITEM_EXPORT( check_item_class_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <string>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::check_item_class_creator::set_string_field
( const std::string& name, const std::string& value )
{
  bool result = true;

  if ( name == "check_item_class_creator.class_name" )
    m_expr.set_class_name(value);
  else
    result = super::set_string_field(name, value);

  return result;
} // check_item_class_creator::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::check_item_class_creator::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( name == "check_item_class_creator.collision_data")
    {
      engine::with_trigger* t( dynamic_cast<engine::with_trigger*>(value) );

      if ( t != NULL )
        m_expr.set_collision_data( t->get_collision_in_expression() );
      else
        claw::logger << claw::log_error << name
                     << ": the item is not of type 'engine::with_trigger'."
                     << std::endl;
    }
  else
    result = super::set_item_field(name, value);

  return result;
} // check_item_class_creator::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if all required fields are initialized.
 */
bool bear::check_item_class_creator::is_valid() const
{
  return !m_expr.get_class_name().empty()
    && m_expr.get_collision_data().is_valid() && super::is_valid();
} // check_item_class_creator::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::check_item_class_creator::build()
{
  kill();
} // check_item_class_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression created by this item.
 */
bear::expr::boolean_expression
bear::check_item_class_creator::do_get_expression() const
{
  return m_expr;
} // check_item_class_creator::do_get_expression()

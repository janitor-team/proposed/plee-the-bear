/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file boolean_constant_creator.cpp
 * \brief Implementation of the bear::boolean_constant_creator class.
 * \author Sébastien Angibaud
 */
#include "generic_items/expr/boolean_constant_creator.hpp"

#include <claw/logger.hpp>

BASE_ITEM_EXPORT( boolean_constant_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <boolean>.
 */
bear::boolean_constant_creator::boolean_constant_creator()
: m_expr(false)
{

} // boolean_constant_creator::boolean_constant_creator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <boolean>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::boolean_constant_creator::set_bool_field
( const std::string& name, bool value )
{
  bool result = true;

  if ( name == "boolean_constant_creator.value" )
    m_expr.set_value( value );
  else
    result = super::set_bool_field(name, value);

  return result;
} // boolean_constant_creator::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::boolean_constant_creator::build()
{
  kill();
} // boolean_constant_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression created by this item.
 */
bear::expr::boolean_expression
bear::boolean_constant_creator::do_get_expression() const
{
  return m_expr;
} // boolean_constant_creator::do_get_expression()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file expression_creator.cpp
 * \brief Implementation of the classes inheriting from
 *        bear::expression_creator.
 * \author Julien Jorge
 */
#include "generic_items/expr/expression_creator.hpp"

BASE_ITEM_EXPORT( boolean_equality_creator, bear )
BASE_ITEM_EXPORT( boolean_disequality_creator, bear )
BASE_ITEM_EXPORT( logical_and_creator, bear )
BASE_ITEM_EXPORT( logical_or_creator, bear )

BASE_ITEM_EXPORT( linear_equality_creator, bear )
BASE_ITEM_EXPORT( linear_plus_creator, bear )
BASE_ITEM_EXPORT( linear_minus_creator, bear )
BASE_ITEM_EXPORT( linear_multiplies_creator, bear )
BASE_ITEM_EXPORT( linear_divides_creator, bear )

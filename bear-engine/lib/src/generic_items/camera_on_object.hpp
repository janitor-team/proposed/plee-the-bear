/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file camera_on_object.hpp
 * \brief A camera shared among objects.
 * \author Sébastien Angibaud
 */
#ifndef __BEAR_CAMERA_ON_OBJECT_HPP__
#define __BEAR_CAMERA_ON_OBJECT_HPP__

#include "communication/typed_message.hpp"
#include "engine/messageable_item.hpp"
#include "generic_items/camera.hpp"
#include "universe/derived_item_handle.hpp"

#include "generic_items/class_export.hpp"
#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A camera shared among objects.
   * \author S�bastien Angibaud
   */
  class GENERIC_ITEMS_EXPORT camera_on_object:
    public camera
  {
    DECLARE_BASE_ITEM(camera_on_object);

    TEXT_INTERFACE_DECLARE_METHOD_LIST(super, init_exported_methods)

  public:
    typedef bear::camera super;

  private:
    /** \brief The type of an handle on a toggle. */
    typedef universe::derived_item_handle<base_item> handle_type;

    /** \brief The type of a list of toggles. */
    typedef std::list<handle_type> handle_list;

  public:
    camera_on_object();
    void progress( bear::universe::time_type elapsed_time );

    bool set_item_list_field
    ( const std::string& name, const std::vector<base_item*>& value );
    bool set_bool_field( const std::string& name, bool value );

    void add_item(base_item* item);
    void remove_item(base_item* item);

  private:
    void progress_center( bear::universe::time_type elapsed_time );
    void progress_fit_items( bear::universe::time_type elapsed_time );

    static void init_exported_methods();

  private:
    /** \brief The objects on which camera is focused. */
    handle_list m_objects;

    /** \brief Indicates if camera is fit on items. */
    bool m_fit_items;

  }; // class camera_on_object
} // namespace bear

#endif // __BEAR_CAMERA_ON_OBJECT_HPP__

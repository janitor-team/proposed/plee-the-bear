/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file density_rectangle_creator.hpp
 * \brief This class adds a universe::density_rectangle in universe::world
 *         then dies.
 * \author Sébastien Angibaud
 */
#ifndef __BEAR_DENSITY_RECTANGLE_CREATOR_HPP__
#define __BEAR_DENSITY_RECTANGLE_CREATOR_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class adds a universe::density_rectangle in universe::world
   *         then dies.
   *
   * The fields of this item are
   *  - \a zone_density: (real) \c the density in the rectangle (default = 0).
   *
   * \author Sébastien angibaud
   */
  class GENERIC_ITEMS_EXPORT density_rectangle_creator:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(density_rectangle_creator);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    density_rectangle_creator();

    bool set_real_field( const std::string& name, double value );

    void build();

  private:
    /** \brief Density applied to the items. */
    double m_density;

  }; // class density_rectangle_creator
} // namespace bear

#endif // __BEAR_DENSITY_RECTANGLE_CREATOR_HPP__

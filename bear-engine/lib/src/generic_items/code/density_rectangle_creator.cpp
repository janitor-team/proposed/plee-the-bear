/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file code/density_rectangle_creator.cpp
 * \brief Implementation of the bear::density_rectangle_creator class.
 * \author Sebastien Angibaud
 */
#include "generic_items/density_rectangle_creator.hpp"

#include "engine/layer/layer.hpp"
#include "engine/world.hpp"

BASE_ITEM_EXPORT( density_rectangle_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::density_rectangle_creator::density_rectangle_creator()
  : m_density(0)
{
  set_can_move_items(false);
} // density_rectangle_creator::density_rectangle_creator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::density_rectangle_creator::set_real_field
( const std::string& name, double value )
{
  bool result = true;

  if ( name == "density_rectangle_creator.zone_density" )
    m_density = value;
  else
    result = super::set_real_field(name, value);

  return result;
} // density_rectangle_creator::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::density_rectangle_creator::build()
{
  if ( get_layer().has_world() )
    get_layer().get_world().add_density_rectangle
      (get_bounding_box(), m_density);

  kill();
} // density_rectangle_creator::build()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file decorative_item.cpp
 * \brief Implementation of the bear::decorative_item class.
 * \author Sebastie Angibaud
 */
#include "generic_items/decorative_item.hpp"
#include "engine/export.hpp"

BASE_ITEM_EXPORT( decorative_item, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::decorative_item::decorative_item()
  : m_kill_when_finished(false),  m_kill_on_contact(false),
    m_stop_on_bottom_contact(false), m_kill_when_leaving(false)
{
  set_phantom(true);
  set_can_move_items(false);
  set_artificial(true);
} // decorative_item::decorative_item()

/*---------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 */
void bear::decorative_item::progress( universe::time_type elapsed_time )
{
  super::progress(elapsed_time);

  if ( m_kill_on_contact && has_contact() )
    kill();
  else if ( m_kill_when_finished && get_animation().is_finished() )
    kill();
} // decorative_item::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
bear::decorative_item::set_bool_field( const std::string& name, bool value )
{
  bool ok = true;

  if (name == "decorative_item.kill_when_finished")
    m_kill_when_finished = value;
  else if (name == "decorative_item.kill_on_contact")
    m_kill_on_contact = value;
  else if (name == "decorative_item.kill_when_leaving")
    m_kill_when_leaving = value;
  else
    ok = super::set_bool_field(name, value);

  return ok;
} // decorative_item::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set if the item kills himself when the animation is finished.
 * \param value The new value.
 */
void bear::decorative_item::set_kill_when_finished(bool value)
{
  m_kill_when_finished = value;
} // decorative_item::set_kill_when_finished()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set if the item kills himself when he has a contact.
 * \param value The new value.
 */
void bear::decorative_item::set_kill_on_contact(bool value)
{
  m_kill_on_contact = value;
} // decorative_item::set_kill_on_contact()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set if the item kills himself when he leaves the active region.
 * \param value The new value.
 */
void bear::decorative_item::set_kill_when_leaving(bool value)
{
  m_kill_when_leaving = value;
} // decorative_item::set_kill_when_leaving()

/*----------------------------------------------------------------------------*/
/**
 * \brief The item leaves the active region.
 */
void bear::decorative_item::leaves_active_region()
{
  super::leaves_active_region();

  if (m_kill_when_leaving)
    kill();
} // decorative_item::leaves_active_region()

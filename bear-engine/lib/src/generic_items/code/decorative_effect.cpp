/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file decorative_effect.cpp
 * \brief Implementation of the bear::decorative_effect class.
 * \author Julien Jorge
 */
#include "generic_items/decorative_effect.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( decorative_effect, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::decorative_effect::decorative_effect()
: m_duration(0), m_elapsed_time(0), m_size_factor_init(1), m_size_factor_end(1),
  m_opacity_factor_init(1), m_opacity_factor_end(1), m_angle_offset_init(1),
  m_angle_offset_end(1), m_red_intensity_factor_init(1),
  m_green_intensity_factor_init(1), m_blue_intensity_factor_init(1),
  m_red_intensity_factor_end(1), m_green_intensity_factor_end(1),
  m_blue_intensity_factor_end(1), m_item(NULL), m_same_lifespan(false),
  m_restore_at_end(false)
{
  set_artificial(true);
  set_phantom(true);
  set_can_move_items(false);
} // decorative_effect::decorative_effect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
void bear::decorative_effect::build()
{
  if ( m_item == (engine::with_rendering_attributes*)NULL )
    kill();
  else
    {
      m_rendering_attributes = m_item->get_rendering_attributes();

      apply_effect();

      // follow the item
      set_center_of_mass( m_item.get_item()->get_center_of_mass() );
    }
} // decorative_effect::build()

/*---------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::decorative_effect::progress( universe::time_type elapsed_time )
{
  super::progress(elapsed_time);

  m_elapsed_time += elapsed_time;

  if ( m_item == (engine::with_rendering_attributes*)NULL )
    kill();
  else if ( m_elapsed_time >= m_duration )
    {
      kill();

      if (m_same_lifespan)
        m_item.get_item()->kill();
      else if (m_restore_at_end)
        m_item->set_rendering_attributes( m_rendering_attributes );
    }
  else
    {
      apply_effect();

      // follow the item
      set_center_of_mass( m_item.get_item()->get_center_of_mass() );
    }
} // decorative_effect::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the item on which the effect is applyed.
 * \param d The total duration.
 * \param same_lifespan Tell if \a item must be killed with *this.
 * \param restore Tell if the state of the item must be restored at the end.
 */
void bear::decorative_effect::set_item
( engine::base_item* item, bool same_lifespan, bool restore )
{
  m_item = item;
  m_same_lifespan = same_lifespan;
  m_restore_at_end = restore;

  if ( item != NULL )
    set_center_of_mass( item->get_center_of_mass() );
} // decorative_effect::set_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the duration of the effect.
 * \param d The total duration.
 */
void bear::decorative_effect::set_duration(universe::time_type d)
{
  m_duration = d;
} // decorative_effect::set_duration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the duration of the effect.
 */
bear::universe::time_type bear::decorative_effect::get_duration() const
{
  return m_duration;
} // decorative_effect::get_duration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the initial factor applied to the size.
 * \param f The factor to apply.
 */
void bear::decorative_effect::set_size_factor_init(double f)
{
  m_size_factor_init = f;
} // decorative_effect::set_size_factor_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the initial factor applied to the opacity.
 * \param f The factor to apply.
 */
void bear::decorative_effect::set_opacity_factor_init(double f)
{
  m_opacity_factor_init = f;
} // decorative_effect::set_opacity_factor_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the initial factor applied to the angle.
 * \param f The factor to apply.
 */
void bear::decorative_effect::set_angle_offset_init(double f)
{
  m_angle_offset_init = f;
} // decorative_effect::set_angle_offset_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the initial factors applied to the intensity of the color
 *        components.
 * \param r The initial factor applied to the red component.
 * \param g The initial factor applied to the green component.
 * \param b The initial factor applied to the blue component.
 */
void bear::decorative_effect::set_intensity_factor_init
( double r, double g, double b )
{
  m_red_intensity_factor_init = r;
  m_green_intensity_factor_init = g;
  m_blue_intensity_factor_init = b;
} // decorative_effect::set_intensity_factor_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the final factor applied to the size.
 * \param f The factor to apply.
 */
void bear::decorative_effect::set_size_factor_end(double f)
{
  m_size_factor_end = f;
} // decorative_effect::set_size_factor_end()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the final factor applied to the opacity.
 * \param f The factor to apply.
 */
void bear::decorative_effect::set_opacity_factor_end(double f)
{
  m_opacity_factor_end = f;
} // decorative_effect::set_opacity_factor_end()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the final factor applied to the angle.
 * \param f The factor to apply.
 */
void bear::decorative_effect::set_angle_offset_end(double f)
{
  m_angle_offset_end = f;
} // decorative_effect::set_angle_offset_end()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the final factors applied to the intensity of the color
 *        components.
 * \param r The final factor applied to the red component.
 * \param g The final factor applied to the green component.
 * \param b The final factor applied to the blue component.
 */
void bear::decorative_effect::set_intensity_factor_end
( double r, double g, double b )
{
  m_red_intensity_factor_end = r;
  m_green_intensity_factor_end = g;
  m_blue_intensity_factor_end = b;
} // decorative_effect::set_intensity_factor_end()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the offsets applied to the size.
 * \param init The initial factor to apply.
 * \param end The final factor to apply.
 */
void bear::decorative_effect::set_size_factor(double init, double end)
{
  set_size_factor_init(init);
  set_size_factor_end(end);
} // decorative_effect::set_size_factor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the offsets applied to the opacity.
 * \param init The initial factor to apply.
 * \param end The final factor to apply.
 */
void bear::decorative_effect::set_opacity_factor(double init, double end)
{
  set_opacity_factor_init(init);
  set_opacity_factor_end(end);
} // decorative_effect::set_opacity_factor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the offsets applied to the angle.
 * \param init The initial factor to apply.
 * \param end The final factor to apply.
 */
void bear::decorative_effect::set_angle_offset(double init, double end)
{
  set_angle_offset_init(init);
  set_angle_offset_end(end);
} // decorative_effect::set_angle_offset()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the factors applied to the intensity of the color components.
 * \param init_r The initial factor applied to the red component.
 * \param init_g The initial factor applied to the green component.
 * \param init_b The initial factor applied to the blue component.
 * \param end_r The final factor applied to the red component.
 * \param end_g The final factor applied to the green component.
 * \param end_b The final factor applied to the blue component.
 */
void bear::decorative_effect::set_intensity_factor
( double init_r, double init_g, double init_b,
  double end_r, double end_g, double end_b )
{
  set_intensity_factor_init(init_r, init_g, init_b);
  set_intensity_factor_end(end_r, end_g, end_b);
} // decorative_effect::set_intensity_factor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the effect to the item.
 */
void bear::decorative_effect::apply_effect() const
{
  const double r = m_elapsed_time / m_duration;

  m_item->get_rendering_attributes().set_width
    ( m_rendering_attributes.width()
      * (m_size_factor_init + (m_size_factor_end - m_size_factor_init) * r) );
  m_item->get_rendering_attributes().set_height
    ( m_rendering_attributes.height()
      * (m_size_factor_init + (m_size_factor_end - m_size_factor_init) * r) );
  m_item->get_rendering_attributes().set_opacity
    ( m_rendering_attributes.get_opacity()
      * (m_opacity_factor_init
         + (m_opacity_factor_end - m_opacity_factor_init) * r) );
  m_item->get_rendering_attributes().set_angle
    ( m_rendering_attributes.get_angle()
      + (m_angle_offset_end - m_angle_offset_init) * r );

  m_item->get_rendering_attributes().set_red_intensity
    ( m_rendering_attributes.get_red_intensity()
      * (m_red_intensity_factor_init
         + (m_red_intensity_factor_end - m_red_intensity_factor_init) * r) );
  m_item->get_rendering_attributes().set_green_intensity
    ( m_rendering_attributes.get_green_intensity()
      * (m_green_intensity_factor_init
         + (m_green_intensity_factor_end - m_green_intensity_factor_init)
         * r) );
  m_item->get_rendering_attributes().set_blue_intensity
    ( m_rendering_attributes.get_blue_intensity()
      * (m_blue_intensity_factor_init
         + (m_blue_intensity_factor_end - m_blue_intensity_factor_init) * r) );
} // decorative_effect::apply_effect()

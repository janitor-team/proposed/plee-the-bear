/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file clone_toggle.cpp
 * \brief Implementation of the bear::clone_toggle class.
 * \author Julien Jorge
 */
#include "generic_items/clone_toggle.hpp"

#include "generic_items/delayed_level_loading.hpp"

#include "engine/game.hpp"
#include "engine/resource_pool.hpp"
#include "engine/variable/variable.hpp"

BASE_ITEM_EXPORT( clone_toggle, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::clone_toggle::~clone_toggle()
{
  for ( std::size_t i=0; i!=m_items.size(); ++i )
    delete m_items[i];
} // clone_toggle::~clone_toggle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type list of items.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::clone_toggle::set_item_list_field
( const std::string& name, const std::vector<engine::base_item*>& value )
{
  bool result = true;

  if ( name == "clone_toggle.items" )
    {
      for ( std::size_t i=0; i!=value.size(); ++i )
        if ( value[i] != NULL )
          {
            m_items.push_back( value[i]->clone() );
            value[i]->kill();
          }
    }
  else
    result = super::set_item_list_field( name, value );

  return result;
} // clone_toggle::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Play the sample.
 * \param activator (ignored) The item that activates the toggle, if any.
 */
void bear::clone_toggle::on_toggle_on( engine::base_item* activator )
{
  for ( std::size_t i=0; i!=m_items.size(); ++i )
    new_item( *m_items[i]->clone() );
} // clone_toggle::on_toggle_on()

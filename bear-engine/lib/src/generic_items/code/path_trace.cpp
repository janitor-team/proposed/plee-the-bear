/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file code/path_trace.cpp
 * \brief Implementation of the bear::path_trace class.
 * \author Julien Jorge
 */
#include "generic_items/path_trace.hpp"

#include "visual/scene_polygon.hpp"

BASE_ITEM_EXPORT(path_trace, bear)

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::path_trace::path_trace()
  : m_progress(&path_trace::progress_void),
    m_fill_color(claw::graphic::black_pixel), m_opacity(1), m_fade_out_speed(1)
{
  set_artificial(true);
  set_phantom(true);
  set_can_move_items(false);
} // path_trace::path_trace()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param ref The item to trace.
 */
bear::path_trace::path_trace( const base_item& ref )
  : m_progress(&path_trace::progress_alive), m_item(ref),
    m_fill_color(claw::graphic::black_pixel), m_opacity(1), m_fade_out_speed(1)
{
  set_artificial(true);
  set_phantom(true);
  set_can_move_items(false);

  set_z_position(ref.get_z_position() - 1);
  set_bounding_box( ref.get_bounding_box() );
} // path_trace::path_trace()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the color of the trace.
 * \param c The new color.
 */
void bear::path_trace::set_fill_color( const visual::color_type& c )
{
  m_fill_color = c;
} // path_trace::set_fill_color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the item to trace.
 * \param ref The item to trace.
 */
void bear::path_trace::set_item( const base_item& ref )
{
  set_z_position(ref.get_z_position() - 1);
  set_bounding_box( ref.get_bounding_box() );

  m_previous_top.clear();
  m_previous_bottom.clear();
  m_item=ref;
  m_progress = &path_trace::progress_alive;

  m_previous_top.push_back( ref.get_top_middle() );
  m_previous_bottom.push_back( ref.get_bottom_middle() );
} // path_trace::set_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the speed of the fade out of the trace when the traced item is
 *        dead.
 * \param s Units of opacity lost per second.
 */
void bear::path_trace::set_fade_out_speed( double s )
{
  m_fade_out_speed = (s > 0) ? s : 0;
} // path_trace::set_fade_out_speed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::path_trace::progress( universe::time_type elapsed_time )
{
  super::progress(elapsed_time);

  (this->*m_progress)( elapsed_time );
} // path_trace::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the visuals of this item.
 * \param visuals (out) The visuals.
 */
void
bear::path_trace::get_visual( std::list<engine::scene_visual>& visuals ) const
{
  CLAW_PRECOND( m_previous_bottom.size() == m_previous_top.size() );

  if ( m_previous_top.empty() )
    return;

  std::vector<visual::position_type> p(4);

  position_list::const_iterator bottom_it = m_previous_bottom.begin();
  position_list::const_iterator top_it = m_previous_top.begin();

  position_list::const_iterator next_top(top_it);
  ++next_top;

  while ( next_top != m_previous_top.end() )
    {
      p[0] = *bottom_it;
      ++bottom_it;
      p[1] = *bottom_it;

      p[3] = *top_it;
      ++top_it;
      p[2] = *top_it;

      next_top = top_it;
      ++next_top;

      if ( !p.empty() )
        {
          visual::scene_polygon e(0, 0, m_fill_color, p);
          e.get_rendering_attributes().set_opacity(m_opacity);
          visuals.push_back( engine::scene_visual(e) );
        }
    }
} // path_trace::get_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of this item, when there is no traced
 *        item.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::path_trace::progress_void( universe::time_type elapsed_time )
{
  // nothing to do
} // path_trace::progress_void()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of this item, while the traced item is
 *        alive.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::path_trace::progress_alive( universe::time_type elapsed_time )
{
  if ( m_item == NULL )
    {
      m_progress = &path_trace::progress_dead;
      return;
    }

  m_previous_top.push_back( m_item->get_top_middle() );
  m_previous_bottom.push_back( m_item->get_bottom_middle() );

  set_bounding_box( get_bounding_box().join(m_item->get_bounding_box()) );
} // path_trace::progress_alive()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of this item, once the traced item is
 *        dead.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::path_trace::progress_dead( universe::time_type elapsed_time )
{
  m_opacity -= m_fade_out_speed * elapsed_time;

  if (m_opacity < 0)
    kill();
} // path_trace::progress_dead()

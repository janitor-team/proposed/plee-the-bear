/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_train.cpp
 * \brief Implementation of the bear::base_train class.
 * \author Sebastien Angibaud
 */
#include "generic_items/base_train.hpp"

#include "universe/collision_info.hpp"

#include "engine/export.hpp"

#include <algorithm>

BASE_ITEM_EXPORT( base_train, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::base_train::base_train()
{
  set_global(true);
} // base_train::base_train()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do post creation actions.
 */
void bear::base_train::build()
{
  super::build();

  m_last_position = get_top_left();
} // bear::base_train::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of the item.
 * \param elapsed_time Elasped time since the last progress.
 */
void bear::base_train::progress( universe::time_type elapsed_time )
{
  super::progress( elapsed_time );

  item_list::iterator it;
  std::list<item_list::iterator> dead;

  for (it=m_list_items.begin(); it!=m_list_items.end(); ++it)
    if ( *it == NULL )
      dead.push_front(it);

  for( ; !dead.empty(); dead.pop_front() )
    m_list_items.erase( dead.front() );
} // bear::base_train::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give a string representation of the item.
 * \param str (out) The result of the convertion.
 */
void bear::base_train::to_string( std::string& str ) const
{
  std::ostringstream oss;
  oss << "nb_items: " << m_list_items.size() << "\n";

  super::to_string(str);

  str = str + oss.str();
} // base_train::to_string()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_as_train().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void bear::base_train::collision
( engine::base_item& that, universe::collision_info& info )
{
  collision_as_train(that, info);
} // base_train::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Proceed the collision.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
bool bear::base_train::collision_as_train
( engine::base_item& that, universe::collision_info& info )
{
  bool align(false);
  double f(1);

  switch( info.get_collision_side() )
    {
    case universe::zone::bottom_zone:
      f = get_bottom_friction();
      align = bottom_side_is_active();
      break;
    case universe::zone::top_zone:
      f = get_top_friction();
      align = top_side_is_active();
      break;
    case universe::zone::middle_left_zone:
      f = get_left_friction();
      align = left_side_is_active();
      break;
    case universe::zone::middle_right_zone:
      f = get_right_friction();
      align = right_side_is_active();
      break;
    case universe::zone::middle_zone:
      break;
    default: { CLAW_ASSERT( false, "Invalid collision side." ); }
    }

  if ( align  && satisfy_collision_condition(that) )
    {
      if ( default_collision(info) )
        {
          that.set_contact_friction(f);
          that.set_system_angle(0);
          z_shift(that);

          if ( info.get_collision_side() == universe::zone::top_zone )
            m_list_items.push_front(that);
        }
      else
        align = false;
    }

  return align;
} // base_train::collision_as_train()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the movement of the item.
 * \param elapsed_time Elasped time since the last call.
 */
void bear::base_train::move( universe::time_type elapsed_time )
{
  super::move(elapsed_time);

  item_list::iterator it;
  universe::position_type position = get_top_left();

  for(it=m_list_items.begin(); it!=m_list_items.end(); ++it)
    if ( *it != NULL )
      (*it)->set_left((*it)->get_left() + position.x - m_last_position.x);

  for(it=m_old_items.begin(); it!=m_old_items.end(); ++it)
    if ( *it != NULL )
      if ( std::find(m_list_items.begin(), m_list_items.end(), *it)
           == m_list_items.end() ) // item is not on me anymore
        (*it)->set_speed
          ( (*it)->get_speed() + universe::speed_type(get_speed().x, 0) );

  m_last_position = position;
  std::swap(m_old_items, m_list_items);
  m_list_items.clear();
} // base_train::move()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the items concerned by a progress/move of this one.
 * \param d (out) A list to which are added such items.
 */
void bear::base_train::get_dependent_items( std::list<physical_item*>& d ) const
{
  item_list::const_iterator it;

  for( it=m_list_items.begin(); it!=m_list_items.end(); ++it )
    if ( *it != NULL )
      d.push_front( it->get() );

  for( it=m_old_items.begin(); it!=m_old_items.end(); ++it )
    if ( *it != NULL )
      d.push_front( it->get() );
} // base_train::get_dependent_items()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file code/friction_rectangle_creator.cpp
 * \brief Implementation of the bear::friction_rectangle_creator class.
 * \author Julien Jorge
 */
#include "generic_items/friction_rectangle_creator.hpp"

#include "engine/layer/layer.hpp"
#include "engine/world.hpp"

BASE_ITEM_EXPORT( friction_rectangle_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::friction_rectangle_creator::friction_rectangle_creator()
  : m_friction(1)
{
  set_can_move_items(false);
} // friction_rectangle_creator::friction_rectangle_creator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::friction_rectangle_creator::set_real_field
( const std::string& name, double value )
{
  bool result = true;

  if ( name == "friction_rectangle_creator.friction" )
    m_friction = value;
  else
    result = super::set_real_field(name, value);

  return result;
} // friction_rectangle_creator::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::friction_rectangle_creator::build()
{
  if ( get_layer().has_world() )
    get_layer().get_world().add_friction_rectangle
      (get_bounding_box(), m_friction);

  kill();
} // friction_rectangle_creator::build()

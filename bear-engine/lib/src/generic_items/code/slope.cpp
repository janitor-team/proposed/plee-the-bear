/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file slope.cpp
 * \brief Implementation of the bear::slope class.
 * \author Julien Jorge
 */
#include "generic_items/slope.hpp"

#include "universe/collision_info.hpp"
#include "universe/collision_repair.hpp"
#include "engine/layer/layer.hpp"
#include "engine/world.hpp"

BASE_ITEM_EXPORT( slope, bear )

/*----------------------------------------------------------------------------*/
const bear::universe::coordinate_type bear::slope::s_line_width = 10;

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::slope::slope()
  : m_tangent_friction(0.8), m_opposite_side_is_active(false),
    m_left_side_is_active(false), m_right_side_is_active(false),
    m_apply_angle(true), m_line(0, 0, 0, 0)
{
  set_weak_collisions(false);
} // slope::slope()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c <real>.
 * \param name The name of the field to set.
 * \param value The new value of the field.
 */
bool bear::slope::set_real_field( const std::string& name, double value )
{
  bool result = true;

  if ( name == "slope.steepness" )
    m_line.direction.y = value;
  else if ( name == "slope.margin" )
    m_line.origin.y = -value;
  else if ( name == "slope.tangent_friction" )
    m_tangent_friction = value;
  else
    result = super::set_real_field(name, value);

  return result;
} // slope::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c bool.
 * \param name The name of the field to set.
 * \param value The new value of the field.
 */
bool bear::slope::set_bool_field( const std::string& name, bool value )
{
  bool result = true;

  if ( name == "slope.opposite_side_is_active" )
    m_opposite_side_is_active = value;
  else if ( name == "slope.left_side_is_active" )
    m_left_side_is_active = value;
  else if ( name == "slope.right_side_is_active" )
    m_right_side_is_active = value;
  else if ( name == "slope.apply_angle" )
    m_apply_angle = value;
  else
    result = super::set_bool_field(name, value);

  return result;
} // slope::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is correctly initialized.
 */
bool bear::slope::is_valid() const
{
  return (m_line.direction.y != 0) && super::is_valid();
} // slope::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::slope::build()
{
  super::build();

  m_margin = - m_line.origin.y;
  set_height( get_height() - m_line.origin.y );

  m_line.direction.x = get_width();

  if ( m_line.direction.y > 0 )
    m_line.origin.y -= m_line.direction.y;
} // slope::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the steepness of the slope.
 */
bear::universe::coordinate_type bear::slope::get_steepness() const
{
  return m_line.direction.y;
} // slope::get_steepness()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the margin of the slope.
 */
bear::universe::coordinate_type bear::slope::get_margin() const
{
  return m_margin;
} // slope::get_margin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Align the other item.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void bear::slope::collision_as_slope
( engine::base_item& that, universe::collision_info& info )
{
  double f(1);
  bool align_as_block(false);
  bool align_as_slope(false);

  switch( info.get_collision_side() )
    {
    case universe::zone::bottom_zone:
      f = get_bottom_friction();
      align_as_block = m_opposite_side_is_active;
      break;
    case universe::zone::top_zone:
      f = get_top_friction();
      align_as_slope = true;
      break;
    case universe::zone::middle_left_zone:
      f = get_left_friction();
      if ( check_left_contact_as_slope(that, info) )
        align_as_slope = true;
      else
        align_as_block = m_left_side_is_active;
      break;
    case universe::zone::middle_right_zone:
      f = get_right_friction();
      if ( check_right_contact_as_slope(that, info) )
        align_as_slope = true;
      else
        align_as_block = m_right_side_is_active;
      break;
    case universe::zone::middle_zone:
      f = get_top_friction();
      if ( check_bottom_above_ground(that, info) )
        align_as_slope = true;
      break;
    default: { CLAW_ASSERT( false, "Invalid collision side." ); }
    }

  bool aligned(false);

  if ( satisfy_collision_condition(that) )
    {
      if ( align_as_slope )
        aligned = align_on_ground(that, info);
      else if ( align_as_block )
        aligned = default_collision(info);
      else
        aligned = align_nearest_edge(that, info);

      if (aligned)
        {
          that.set_contact_friction(f);
          z_shift(that);
        }
    }
} // slope::collision_as_slope()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call collision_as_slope().
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void bear::slope::collision
( engine::base_item& that, universe::collision_info& info )
{
  collision_as_slope(that, info);
} // slope::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if an item colliding on the left is aligned on the ground.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
bool bear::slope::check_left_contact_as_slope
( engine::base_item& that, universe::collision_info& info ) const
{
  bool result = false;
  const line_type line(m_line.origin + get_top_left(), m_line.direction);

  // the slope goes from top right to bottom left
  if ( line.direction.y > 0 )
    result = info.get_bottom_left_on_contact().y
      >= line.y_value( get_left() ) - s_line_width;

  return result;
} // slope::check_left_contact_as_slope()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if an item colliding on the right is aligned on the ground.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
bool bear::slope::check_right_contact_as_slope
( engine::base_item& that, universe::collision_info& info ) const
{
  bool result = false;
  const line_type line(m_line.origin + get_top_left(), m_line.direction);

  // the slope goes from top left to bottom right
  if ( line.direction.y < 0 )
    result = info.get_bottom_left_on_contact().y
      >= line.y_value( get_right() ) - s_line_width;

  return result;
} // slope::check_right_contact_as_slope()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the bottom of the other item was above the ground at
 *        collision time.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
bool bear::slope::check_bottom_above_ground
( engine::base_item& that, universe::collision_info& info ) const
{
  const universe::position_type pos
    (info.other_previous_state().get_bottom_middle());
  const line_type line(m_line.origin + get_top_left(), m_line.direction);

  return pos.y >= line.y_value(pos.x) - s_line_width;
} // slope::check_bottom_above_ground()

/*----------------------------------------------------------------------------*/
/**
 * \brief Align \a that on the ground line.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
bool bear::slope::align_on_ground
( engine::base_item& that, universe::collision_info& info )
{
  bool result = false;

  const universe::coordinate_type pos_x
    ( info.get_bottom_left_on_contact().x + that.get_width() / 2 );
  const line_type line(m_line.origin + get_top_left(), m_line.direction);

  if ( (pos_x >= get_left()) && (pos_x <= get_right())
       && item_crossed_up_down(that, info) )
    {
      universe::position_type pos
        ( info.get_bottom_left_on_contact().x, line.y_value(pos_x) );

      if ( std::abs(pos.x - info.other_previous_state().get_left()) +
           std::abs(pos.y - info.other_previous_state().get_bottom()) < 0.6)
        pos = info.other_previous_state().get_bottom_left();

      if ( collision_align_top(info, pos) )
        {
          result = true;

          if ( m_apply_angle )
            apply_angle_to(that, info);
        }
    }

  return result;
} // slope::align_on_ground()

/*----------------------------------------------------------------------------*/
/**
 * \brief Align \a that on the nearest vertical edge.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
bool bear::slope::align_nearest_edge
( engine::base_item& that, universe::collision_info& info )
{
  bool result = false;

  if ( info.reference_previous_state().get_center_of_mass().x
       <= info.other_previous_state().get_center_of_mass().x )
    {
      if (m_right_side_is_active)
        result = collision_align_right(info);
    }
  else if (m_left_side_is_active)
    result = collision_align_left(info);

  return result;
} // slope::align_nearest_edge()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the other item has crossed the surface following a top to down
 *        direction.
 * \param that The other item in the collision.
 * \param info Informations on the collision.
 */
bool bear::slope::item_crossed_up_down
( engine::base_item& that, const universe::collision_info& info ) const
{
  bool result = false;
  const line_type line(m_line.origin + get_top_left(), m_line.direction);

  if ( that.get_bottom()
       <= line.y_value(that.get_center_of_mass().x) + s_line_width )
    {
      const universe::position_type other_prev_bottom
        ( info.other_previous_state().get_bottom_middle() );

      if ( other_prev_bottom.x < get_left() )
        result = other_prev_bottom.y >= line.origin.y - s_line_width;
      else if ( other_prev_bottom.x > get_right() )
        result =
          other_prev_bottom.y >= line.y_value( get_right() ) - s_line_width;
      else
        result = other_prev_bottom.y
          >= line.y_value(other_prev_bottom.x) - s_line_width;
    }

  return result;
} // slope::item_crossed_up_down()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the angle of the slope to a colliding item.
 * \param that The other item in the collision.
 * \param info Informations on the collision.
 */
void bear::slope::apply_angle_to
( engine::base_item& that, const universe::collision_info& info ) const
{
  double angle = std::atan(m_line.direction.y / m_line.direction.x);
  that.set_system_angle( angle);

  universe::coordinate_type g_force(0);

  if ( get_layer().has_world() )
    g_force = std::abs(get_layer().get_world().get_gravity().y
                       * info.other_previous_state().get_mass()
                       + info.other_previous_state().get_force().y);

  const universe::coordinate_type normal_length =
    g_force * std::cos(angle);
  const universe::coordinate_type x_gravity_length =
    g_force * std::sin(angle);
  const universe::coordinate_type friction_length =
    normal_length * m_tangent_friction;

  if ( x_gravity_length > friction_length )
    {
      const universe::coordinate_type d =
        x_gravity_length - friction_length;

      if ( m_line.direction.y > 0 )
        that.add_internal_force( universe::force_type(-d, 0) );
      else
        that.add_internal_force( universe::force_type(d, 0) );
    }
  else
    that.add_internal_force
      ( universe::force_type(x_gravity_length, 0) );

  info.get_collision_repair().set_contact_normal
    (that, that.get_x_axis().get_orthonormal_anticlockwise());
} // slope::apply_angle_to()

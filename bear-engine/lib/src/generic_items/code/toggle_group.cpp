/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file toggle_group.cpp
 * \brief Implementation of the bear::toggle_group class.
 * \author Julien Jorge
 */
#include "generic_items/toggle_group.hpp"

BASE_ITEM_EXPORT( toggle_group, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type list of items.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::toggle_group::set_item_list_field
( const std::string& name, const std::vector<base_item*>& value )
{
  bool ok = true;

  if (name == "toggle_group.toggles")
    {
      for (std::size_t i=0; i!=value.size(); ++i)
        insert(value[i]);
    }
  else
    ok = super::set_item_list_field(name, value);

  return ok;
} // toggle_group::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Insert a toggle in the group.
 * \param t The toggle to insert.
 */
void bear::toggle_group::insert( engine::base_item* t )
{
  m_toggles.push_back(t);
} // toggle_group::insert()

/*----------------------------------------------------------------------------*/
/**
 * \brief Turn on the toggles.
 * \param activator (ignored) The item that activates the toggle, if any.
 */
void bear::toggle_group::on_toggle_on( engine::base_item* activator )
{
  handle_list_type remaining;

  for ( ; !m_toggles.empty(); m_toggles.pop_front() )
    if ( m_toggles.front() != (engine::with_toggle*)NULL )
      {
        remaining.push_back(m_toggles.front());
        m_toggles.front()->toggle_on(activator);
      }

  std::swap(m_toggles, remaining);
} // toggle_group::on_toggle_on()

/*----------------------------------------------------------------------------*/
/**
 * \brief Turn off the toggles.
 * \param activator (ignored) The item that activates the toggle, if any.
 */
void bear::toggle_group::on_toggle_off( engine::base_item* activator )
{
  handle_list_type remaining;

  for ( ; !m_toggles.empty(); m_toggles.pop_front() )
    if ( m_toggles.front() != (engine::with_toggle*)NULL )
      {
        remaining.push_back(m_toggles.front());
        m_toggles.front()->toggle_off(activator);
      }

  std::swap(m_toggles, remaining);
} // toggle_group::on_toggle_off()

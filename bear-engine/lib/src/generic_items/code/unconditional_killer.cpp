/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file unconditional_killer.cpp
 * \brief Implementation of the bear::unconditional_killer class.
 * \author Julien Jorge
 */
#include "generic_items/unconditional_killer.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( unconditional_killer, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::unconditional_killer::unconditional_killer()
{
  set_phantom(true);
} // unconditional_killer::unconditional_killer()

/*----------------------------------------------------------------------------*/
/**
 * \brief An item collides with this. Kill him.
 * \param that The colliding item.
 * \param info Some informations about the collision.
 */
void bear::unconditional_killer::collision
( engine::base_item& that, universe::collision_info& info )
{
  that.kill();
} // unconditional_killer::collision()

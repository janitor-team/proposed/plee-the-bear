/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_movement_toggle.hpp
 * \brief This class applies a forced movement to an item when activated.
 * \author Julien Jorge
 */
#ifndef __BEAR_FORCED_MOVEMENT_TOGGLE_HPP__
#define __BEAR_FORCED_MOVEMENT_TOGGLE_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class applies a forced movement to an item when activated.
   *
   * The fields of this item are
   *  - \a actor: (item list) \b the item to which the movement is applied
   *    (default = none),
   *  - \a movement: (item) \b [required] \c an item having a movement applied
   *    to him, the movement that will be applied to the actor,
   *  - \a apply_to_activator: (bool) \c tell if the movement has to be applied
   *    to the activator too (default = false),
   *  - \a auto_remove: (bool) \c tell if the movement removes himself from the
   *    moving item when finished (default = false).
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT forced_movement_toggle:
    public engine::item_with_toggle<engine::base_item>
  {
    DECLARE_BASE_ITEM(forced_movement_toggle);

  public:
    /** \brief The type of the parent class. */
    typedef engine::item_with_toggle<engine::base_item> super;

  public:
    forced_movement_toggle();

    bool set_bool_field( const std::string& name, bool value );
    bool set_item_field( const std::string& name, engine::base_item* value );
    bool set_item_list_field
    ( const std::string& name, const std::vector<engine::base_item*>& value );

    bool is_valid() const;

    void on_toggle_on( engine::base_item* activator );

  private:
    /** \brief The items to move. */
    std::vector<universe::item_handle> m_actor;

    /** \brief The forced movement to apply to the actor. */
    universe::forced_movement m_movement;

    /** \brief Tell if the movement removes himself from the moving item when
          finished. */
    bool m_auto_remove;

    /** \brief Tell if the movement has to be applied to the activator too. */
    bool m_apply_to_activator;

  }; // class forced_movement_toggle
} // namespace bear

#endif // __BEAR_FORCED_MOVEMENT_TOGGLE_HPP__

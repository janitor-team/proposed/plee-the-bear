/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_aiming_creator.hpp
 * \brief This class creates a forced movement of type "forced_aiming",
 *        applies it to itself then die.
 * \author Julien Jorge
 */
#ifndef __BEAR_FORCED_AIMING_CREATOR_HPP__
#define __BEAR_FORCED_AIMING_CREATOR_HPP__

#include "engine/base_item.hpp"
#include "universe/forced_movement/forced_aiming.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class creates a forced movement of type "forced_aiming",
   *        applies it to itself then die.
   *
   * The fields of this item are
   *  - \a target: (item) \b [required] \c the target to reach,
   *  - \a max_angle: (real) \c the maximum angle applied to the item
   *    (default = inf.),
   *  - \a apply_angle: (bool) \c tell (true) if we apply the \a angle of the
   *    movement to the moving item (default = false),
   *  - \a duration: (real) \c the total time of the movement (default = inf.),
   *  - \a max_speed: (real) \c the maximum speed of the item (default = inf.),
   *  - \a acceleration: (real) \c the acceleration of the movement
   *    (default = inf.).
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT forced_aiming_creator:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(forced_aiming_creator);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    bool set_real_field( const std::string& name, double value );
    bool set_bool_field( const std::string& name, bool value );
    bool set_item_field( const std::string& name, engine::base_item* value );
    bool is_valid() const;

    void build();

  private:
    /** \brief The movement finally applied. */
    universe::forced_aiming m_movement;

  }; // class forced_aiming_creator
} // namespace bear

#endif // __BEAR_FORCED_AIMING_CREATOR_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_goto_creator.hpp
 * \brief This class creates a forced movement of type "forced_goto", applies it
 *        to itself then die.
 * \author Julien Jorge
 */
#ifndef __BEAR_FORCED_GOTO_CREATOR_HPP__
#define __BEAR_FORCED_GOTO_CREATOR_HPP__

#include "engine/base_item.hpp"
#include "universe/forced_movement/forced_goto.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class creates a forced movement of type "forced_goto", applies
   *        it to itself then die.
   *
   * The fields of this item are
   *  - \a target.x: (real) \b [required] \c the X-position to reach,
   *  - \a target.y: (real) \b [required] \c the Y-position to reach,
   *  - \a duration: (real) \c the total time to reach the target (default = 1),
   *  - \a acceleration_time: (real) \c the duration of the acceleration and
   *    deceleration (default = duration / 2).
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT forced_goto_creator:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(forced_goto_creator);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    bool set_real_field( const std::string& name, double value );

    void build();

  private:
    /** \brief The movement finally applied. */
    universe::forced_goto m_movement;

  }; // class forced_goto_creator
} // namespace bear

#endif // __BEAR_FORCED_GOTO_CREATOR_HPP__

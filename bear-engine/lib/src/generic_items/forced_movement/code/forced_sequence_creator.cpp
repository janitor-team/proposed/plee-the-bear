/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_sequence_creator.cpp
 * \brief Implementation of the bear::forced_sequence_creator class.
 * \author Julien Jorge
 */
#include "generic_items/forced_movement/forced_sequence_creator.hpp"

#include "engine/export.hpp"

#include <claw/logger.hpp>

BASE_ITEM_EXPORT( forced_sequence_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <unsigned int>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_sequence_creator::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result = true;

  if ( name == "forced_sequence_creator.loops" )
    m_movement.set_loops(value);
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // forced_sequence_creator::set_u_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <list of item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_sequence_creator::set_item_list_field
( const std::string& name, const std::vector<engine::base_item*>& value )
{
  bool result = true;

  if ( name == "forced_sequence_creator.movements" )
    {
      for (unsigned int i=0; result && (i!=value.size()); ++i)
        if ( value[i] == NULL )
          claw::logger << claw::log_warning << "forced_sequence_creator: item #"
                       << i << " is NULL." << std::endl;
        else if ( value[i]->has_forced_movement() )
          m_movement.push_back( value[i]->get_forced_movement() );
        else
          claw::logger << claw::log_warning << "forced_sequence_creator: item #"
                       << i << " has no movement." << std::endl;
    }
  else
    result = super::set_item_list_field(name, value);

  return result;
} // forced_sequence_creator::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::forced_sequence_creator::build()
{
  set_forced_movement(m_movement);

  kill();
} // forced_sequence_creator::build()

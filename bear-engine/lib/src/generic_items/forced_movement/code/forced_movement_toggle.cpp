/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_movement_toggle.cpp
 * \brief Implementation of the bear::forced_movement_toggle class.
 * \author Julien Jorge
 */
#include "generic_items/forced_movement/forced_movement_toggle.hpp"

#include "universe/forced_movement/forced_movement.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( forced_movement_toggle, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::forced_movement_toggle::forced_movement_toggle()
  : m_auto_remove(false), m_apply_to_activator(false)
{
  set_phantom(true);
  set_can_move_items(false);
  set_artificial(true);
} // forced_movement_toggle::forced_movement_toggle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <bool>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_movement_toggle::set_bool_field
( const std::string& name, bool value )
{
  bool result = true;

  if ( name == "forced_movement_toggle.auto_remove" )
    m_auto_remove = value;
  else if ( name == "forced_movement_toggle.apply_to_activator" )
    m_apply_to_activator = value;
  else
    result = super::set_bool_field(name, value);

  return result;
} // forced_movement_toggle::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_movement_toggle::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( (name == "forced_movement_toggle.movement") && (value != NULL) )
    {
      m_movement = value->get_forced_movement();
      m_movement.set_auto_remove(m_auto_remove);
    }
  else
    result = super::set_item_field(name, value);

  return result;
} // forced_movement_toggle::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <list of item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_movement_toggle::set_item_list_field
( const std::string& name, const std::vector<engine::base_item*>& value )
{
  bool result = true;

  if ( name == "forced_movement_toggle.actor" )
    {
      m_actor.resize(value.size());
      for (std::size_t i=0; i!=value.size(); ++i)
        m_actor[i] = value[i];
    }
  else
    result = super::set_item_list_field(name, value);

  return result;
} // forced_movement_toggle::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if all required fields are initialized.
 */
bool bear::forced_movement_toggle::is_valid() const
{
  bool result = !m_movement.is_null() && super::is_valid();

  for (std::size_t i=0; result && (i!=m_actor.size()); ++i)
    result = m_actor[i] != NULL;

  return result;
} // forced_movement_toggle::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the movement to the actors.
 * \param activator The activator of the toggle.
 */
void bear::forced_movement_toggle::on_toggle_on( engine::base_item* activator )
{
  for (std::size_t i=0; i!=m_actor.size(); ++i)
    if ( m_actor[i] != NULL )
      m_actor[i]->set_forced_movement(m_movement);

  if ( (activator != NULL) && m_apply_to_activator )
    activator->set_forced_movement(m_movement);
} // forced_movement_toggle::on_toggle_on()

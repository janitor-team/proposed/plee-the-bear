/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_join_creator.cpp
 * \brief Implementation of the bear::forced_join_creator class.
 * \author Julien Jorge
 */
#include "generic_items/forced_movement/forced_join_creator.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( forced_join_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_join_creator::set_real_field
( const std::string& name, double value )
{
  bool result = true;

  if ( name == "forced_join_creator.duration" )
    m_movement.set_total_time(value);
  else
    result = super::set_real_field(name, value);

  return result;
} // forced_join_creator::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_join_creator::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( (name == "forced_join_creator.target") && (value != NULL) )
    m_movement.set_reference_point_on_center(*value);
  else
    result = super::set_item_field(name, value);

  return result;
} // forced_join_creator::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if all required fields are initialized.
 */
bool bear::forced_join_creator::is_valid() const
{
  return m_movement.has_reference_item() && super::is_valid();
} // forced_join_creator::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::forced_join_creator::build()
{
  set_forced_movement(m_movement);

  kill();
} // forced_join_creator::build()

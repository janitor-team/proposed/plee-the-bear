/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_goto_creator.cpp
 * \brief Implementation of the bear::forced_goto_creator class.
 * \author Julien Jorge
 */
#include "generic_items/forced_movement/forced_goto_creator.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( forced_goto_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::forced_goto_creator::set_real_field
( const std::string& name, double value )
{
  bool result = true;

  if ( name == "forced_goto_creator.duration" )
    m_movement.set_total_time(value);
  else if ( name == "forced_goto_creator.acceleration_time" )
    m_movement.set_acceleration_time(value);
  else if ( name == "forced_goto_creator.length.x" )
    m_movement.set_x_length(value);
  else if ( name == "forced_goto_creator.length.y" )
    m_movement.set_y_length(value);
  else
    result = super::set_real_field(name, value);

  return result;
} // forced_goto_creator::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::forced_goto_creator::build()
{
  set_forced_movement(m_movement);

  kill();
} // forced_goto_creator::build()

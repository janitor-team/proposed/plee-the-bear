/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file applied_forced_movement.cpp
 * \brief Implementation of the bear::applied_forced_movement class.
 * \author Julien Jorge
 */
#include "generic_items/forced_movement/applied_forced_movement.hpp"

#include "universe/forced_movement/forced_movement.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( applied_forced_movement, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::applied_forced_movement::applied_forced_movement()
  : m_movement(NULL), m_auto_remove(false)
{

} // applied_forced_movement::applied_forced_movement()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <bool>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::applied_forced_movement::set_bool_field
( const std::string& name, bool value )
{
  bool result = true;

  if ( name == "applied_forced_movement.auto_remove" )
    m_auto_remove = value;
  else
    result = super::set_bool_field(name, value);

  return result;
} // applied_forced_movement::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::applied_forced_movement::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( name == "applied_forced_movement.movement" )
    m_movement = value;
  else
    result = super::set_item_field(name, value);

  return result;
} // applied_forced_movement::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <list of item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::applied_forced_movement::set_item_list_field
( const std::string& name, const std::vector<engine::base_item*>& value )
{
  bool result = true;

  if ( name == "applied_forced_movement.actor" )
    m_actor = value;
  else
    result = super::set_item_list_field(name, value);

  return result;
} // applied_forced_movement::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if all required fields are initialized.
 */
bool bear::applied_forced_movement::is_valid() const
{
  bool result = !m_actor.empty() && (m_movement != NULL)
    && m_movement->has_forced_movement() && super::is_valid();

  for (unsigned int i=0; result && (i!=m_actor.size()); ++i)
    result = m_actor[i] != NULL;

  return result;
} // applied_forced_movement::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::applied_forced_movement::build()
{
  for (unsigned int i=0; i!=m_actor.size(); ++i)
    {
      m_actor[i]->set_forced_movement(m_movement->get_forced_movement());
      m_actor[i]->get_forced_movement().set_auto_remove(m_auto_remove);
    }

  kill();
} // applied_forced_movement::build()

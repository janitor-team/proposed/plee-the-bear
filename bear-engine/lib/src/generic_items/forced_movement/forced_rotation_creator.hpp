/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_rotation_creator.hpp
 * \brief This class creates a forced movement of type "forced_rotation",
 *        applies it to itself then die.
 * \author Julien Jorge
 */
#ifndef __BEAR_FORCED_ROTATION_CREATOR_HPP__
#define __BEAR_FORCED_ROTATION_CREATOR_HPP__

#include "engine/base_item.hpp"
#include "universe/forced_movement/forced_rotation.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class creates a forced movement of type "forced_rotation",
   *        applies it to itself then die.
   *
   * The fields of this item are
   *  - \a center: (item) \b [required] \c the center of the rotation,
   *  - \a start_angle: (real) the initial angle (default = -3.1415),
   *  - \a end_angle: (real) the final angle (default = 3.1415),
   *  - \a duration: (real) \c the total time needed to go from \a start_angle
   *    to end_angle (default = 1),
   *  - \a radius: (real) the distance of the moving item from the center of the
   *    rotation (default = auto),
   *  - \a loop_back: (bool) tell to play the rotation backward when the end is
   *    reached (default = false),
   *  - \a loops: (unsigned int) do the rotation this number of times
   *    (default = infinity),
   *  - \a apply_angle: (string) tell how the angle of the rotation is applied
   *    to the rotating item. Valid values are "add", "force" and "keep"
   *    (default = "keep").
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT forced_rotation_creator:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(forced_rotation_creator);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    bool set_real_field( const std::string& name, double value );
    bool set_bool_field( const std::string& name, bool value );
    bool set_string_field( const std::string& name, const std::string& value );
    bool set_u_integer_field( const std::string& name, unsigned int value );
    bool set_item_field( const std::string& name, engine::base_item* value );

    bool is_valid() const;

    void build();

  private:
    /** \brief The movement finally applied. */
    universe::forced_rotation m_movement;

  }; // class forced_rotation_creator
} // namespace bear

#endif // __BEAR_FORCED_ROTATION_CREATOR_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file applied_forced_movement.hpp
 * \brief This class applies a forced movement to an item then die.
 * \author Julien Jorge
 */
#ifndef __BEAR_APPLIED_FORCED_MOVEMENT_HPP__
#define __BEAR_APPLIED_FORCED_MOVEMENT_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class applies a forced movement to an item then die.
   *
   * The fields of this item are
   *  - \a actor: (item) \b [required] \c the item to move,
   *  - \a movement: (item) \b [required] \c an item having a movement applied
   *    to him, the movement that will be applied to the actor,
   *  - \a auto_remove: (bool) \c tell if the movement removes himself from the
   *    moving item when finished (default = false).
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT applied_forced_movement:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(applied_forced_movement);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    applied_forced_movement();

    bool set_bool_field( const std::string& name, bool value );
    bool set_item_field( const std::string& name, engine::base_item* value );
    bool set_item_list_field
    ( const std::string& name, const std::vector<engine::base_item*>& value );

    bool is_valid() const;

    void build();

  private:
    /** \brief The items to move. */
    std::vector<engine::base_item*> m_actor;

    /** \brief The item from which we take the movement. */
    engine::base_item* m_movement;

    /** \brief Tell if the movement removes himself from the moving item when
          finished. */
    bool m_auto_remove;

  }; // class applied_forced_movement
} // namespace bear

#endif // __BEAR_APPLIED_FORCED_MOVEMENT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_loader_progression_item.hpp
 * \brief An item that fills a bar while loading a level.
 * \author Julien Jorge
 */
#ifndef __BEAR_LEVEL_LOADER_PROGRESSION_ITEM_HPP__
#define __BEAR_LEVEL_LOADER_PROGRESSION_ITEM_HPP__

#include "generic_items/level_loader_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"

#include "generic_items/class_export.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief An item that fills a bar while loading a level.
   *
   * The fields of this class are:
   *  - \a sprite: <sprite> the sprite that grows while the level is loaded.
   *    Required.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT level_loader_progression_item:
    public engine::basic_renderable_item<level_loader_item>
  {
    DECLARE_BASE_ITEM(level_loader_progression_item);

  public:
    /** \brief The type of the parent class. */
    typedef engine::basic_renderable_item<level_loader_item> super;

  public:
    void progress( universe::time_type elapsed_time );
    void get_visual( std::list<engine::scene_visual>& visuals ) const;

    bool set_sprite_field
    ( const std::string& name, const visual::sprite& value );

    bool is_valid();

  private:
    /** \brief The progess bar of the items. */
    visual::sprite m_item_bar;

  }; // class level_loader_progression_item
} // namespace bear

#endif // __BEAR_LEVEL_LOADER_PROGRESSION_ITEM_HPP__

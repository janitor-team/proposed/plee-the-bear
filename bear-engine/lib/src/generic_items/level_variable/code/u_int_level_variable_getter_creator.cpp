/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file u_int_level_variable_getter_creator.cpp
 * \brief Implementation of the bear::u_int_level_variable_getter_creator class.
 * \author Julien Jorge
 */
#include "generic_items/level_variable/u_int_level_variable_getter_creator.hpp"

BASE_ITEM_EXPORT( u_int_level_variable_getter_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::u_int_level_variable_getter_creator::u_int_level_variable_getter_creator()
{
  m_expr.set_default_value(0);
} // u_int_level_variable_getter_creator::u_int_level_variable_getter_creator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::u_int_level_variable_getter_creator::build()
{
  m_expr.set_level( &engine::level_object::get_level() );

  kill();
} // u_int_level_variable_getter_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is correctly initialized.
 */
bool bear::u_int_level_variable_getter_creator::is_valid() const
{
  return !m_expr.get_name().empty() && super::is_valid();
} // u_int_level_variable_getter_creator::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::u_int_level_variable_getter_creator::set_string_field
( const std::string& name, const std::string& value )
{
  bool result = true;

  if ( name == "u_int_level_variable_getter_creator.name" )
    m_expr.set_name(value);
  else
    result = super::set_string_field(name, value);

  return result;
} // u_int_level_variable_getter_creator::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c unsigned integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::u_int_level_variable_getter_creator::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result = true;

  if ( name == "u_int_level_variable_getter_creator.default_value" )
    m_expr.set_default_value(value);
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // u_int_level_variable_getter_creator::set_u_int_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression created by this item.
 */
bear::expr::linear_expression
bear::u_int_level_variable_getter_creator::do_get_expression() const
{
  return m_expr;
} // u_level_game_variable_getter_creator::do_get_expression()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file decorative_toggle.hpp
 * \brief A minimal toggle, used only for its animations.
 * \author Julien Jorge
 */
#ifndef __BEAR_DECORATIVE_TOGGLE_HPP__
#define __BEAR_DECORATIVE_TOGGLE_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/decorated_item_with_toggle.hpp"

#include "generic_items/class_export.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A minimal toggle, used only for its animations.
   * \author Julien Jorge
   *
   * The custom fields of this class are the fields of the parent classes.
   */
  class GENERIC_ITEMS_EXPORT decorative_toggle:
    public engine::decorated_item_with_toggle
    < engine::basic_renderable_item<engine::base_item> >
  {
    DECLARE_BASE_ITEM(decorative_toggle);

  public:
    decorative_toggle();

  }; // class decorative_toggle
} // namespace bear

#endif // __BEAR_DECORATIVE_TOGGLE_HPP__

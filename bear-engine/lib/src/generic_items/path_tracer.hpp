/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file path_tracer.hpp
 * \brief An item to create a path_trace on the colliding items under a given
 *        condition.
 * \author Julien Jorge
 */
#ifndef __BEAR_PATH_TRACER_HPP__
#define __BEAR_PATH_TRACER_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_trigger.hpp"

#include "generic_items/class_export.hpp"
#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief An item to create a path_trace on the colliding items under a given
   *        condition.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT path_tracer:
    public engine::item_with_trigger<engine::base_item>
  {
    DECLARE_BASE_ITEM(path_tracer);

  public:
    /** \brief The type of the parent class. */
    typedef engine::item_with_trigger<engine::base_item> super;

  private:
    /** \brief The type of the handle on the traced item. */
    typedef universe::const_item_handle handle_type;

    /** \brief A list of handles. */
    typedef std::list<handle_type> handle_list_type;

  public:
    path_tracer();

    bool set_real_field( const std::string& name, double value );

    void progress( universe::time_type elapsed_time );

    void set_fill_color( const visual::color_type& c );
    void set_fade_out_speed( double s );

  private:
    void on_trigger_on( base_item* activator );

  private:
    /** \brief The color of the trace. */
    visual::color_type m_fill_color;

    /** \brief The speed of the fade out when the item is dead. */
    double m_fade_out_speed;

    /** \brief Handles on the items for which we already have created a
        trace. */
    handle_list_type m_traces;

  }; // class path_tracer
} // namespace bear

#endif // __BEAR_PATH_TRACER_HPP__

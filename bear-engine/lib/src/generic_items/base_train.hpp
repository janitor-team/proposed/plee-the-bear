/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_train.hpp
 * \brief The class describing the base of a train platform.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_BASE_TRAIN_HPP__
#define __BEAR_BASE_TRAIN_HPP__

#include "generic_items/class_export.hpp"
#include "generic_items/block.hpp"
#include "universe/physical_item_state.hpp"

#include <list>
#include <vector>

namespace bear
{
  /**
   * \brief The class describing a train platform.
   * \author Sebastien Angibaud
   */
  class GENERIC_ITEMS_EXPORT base_train:
    public block
  {
    DECLARE_BASE_ITEM(base_train);

  public:
    /** \brief The type of the parent class. */
    typedef block super;

    /** \brief The type of the list in which we store the items depending on the
        train. */
    typedef std::list<universe::item_handle> item_list;

  public:
    base_train();

    void build();
    void progress( universe::time_type elapsed_time );

  protected:
    void to_string( std::string& str ) const;

    void collision( engine::base_item& that, universe::collision_info& info );
    bool collision_as_train
    ( engine::base_item& that, universe::collision_info& info );

    void move( universe::time_type elapsed_time );

  private:
    void get_dependent_items( std::list<physical_item*>& d ) const;

  private:
    /** \brief The list of items on the platform. */
    item_list m_list_items;

    /** \brief The list of the items that were on the platform. */
    item_list m_old_items;

    /** \brief The last position of the platform. */
    universe::position_type m_last_position;

  }; // class base_train
} // namespace bear

#endif // __BEAR_BASE_TRAIN_HPP__

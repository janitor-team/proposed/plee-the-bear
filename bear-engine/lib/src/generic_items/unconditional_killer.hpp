/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file unconditional_killer.hpp
 * \brief An item that kills all colliding items.
 * \author Julien Jorge
 */
#ifndef __BEAR_UNCONDITIONAL_KILLER_HPP__
#define __BEAR_UNCONDITIONAL_KILLER_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief An item that kills all colliding items.
   *
   * The valid fields for this item are
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT unconditional_killer:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(unconditional_killer);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    unconditional_killer();

  protected:
    virtual void collision
      ( engine::base_item& that, universe::collision_info& info );

  }; // class unconditional_killer
} // namespace bear

#endif // __BEAR_UNCONDITIONAL_KILLER_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file camera_toggle.hpp
 * \brief A toggle that changes the current camera.
 * \author Julien Jorge
 */
#ifndef __BEAR_CAMERA_TOGGLE_HPP__
#define __BEAR_CAMERA_TOGGLE_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"
#include "universe/derived_item_handle.hpp"

#include "generic_items/class_export.hpp"
#include "engine/export.hpp"

namespace bear
{
  class camera;

  /**
   * \brief A toggle that changes the current camera.
   *
   * The following fields are supported by this item:
   *  - camera (item) [required], the camera to switch to,
   *  - transition_duration (real), the time passed to the smooth activation
   *    (default = 0.5).
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT camera_toggle:
    public engine::item_with_toggle<engine::base_item>
  {
    DECLARE_BASE_ITEM(camera_toggle);

    /** \brief The type of the parent class. */
    typedef engine::item_with_toggle<engine::base_item> super;

    /** \brief The type of the handle on the item. */
    typedef universe::derived_item_handle<camera> handle_type;

  public:
    camera_toggle();

    bool set_item_field( const std::string& name, engine::base_item* value );
    bool set_real_field( const std::string& name, double value );

    bool is_valid() const;

  private:
    void build_off();
    void on_toggle_on( engine::base_item* activator );
    void on_toggle_off( engine::base_item* activator );

    void switch_camera();

  private:
    /** \brief The time passed to the starting smooth activation. */
    universe::time_type m_starting_smooth_delay;

    /** \brief The time passed to the ending smooth activation. */
    universe::time_type m_ending_smooth_delay;

    /** \brief The camera to activate. */
    handle_type m_camera;

  }; // class camera_toggle
} // namespace bear

#endif // __BEAR_CAMERA_TOGGLE_HPP__

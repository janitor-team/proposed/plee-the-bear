/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file change_camera_size.hpp
 * \brief An item that changes the size of the camera.
 * \author Julien Jorge
 */
#ifndef __BEAR_CHANGE_CAMERA_SIZE_HPP__
#define __BEAR_CHANGE_CAMERA_SIZE_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief A simple change_camera_size.
   *
   * The valid fields for this item are
   *  - \a wanted_width: (real) the width to give to the camera
   *    (default = 320),
   *  - \a wanted_height: (real) the height to give to the camera
   *    (default = 240),
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT change_camera_size:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(change_camera_size);

  public:
    typedef engine::base_item super;

  public:
    change_camera_size();

    bool set_real_field( const std::string& name, double value );
    void progress( universe::time_type elapsed_time );

  private:
    /** \brief The size to attain. */
    universe::size_box_type m_wanted_size;

  }; // class change_camera_size
} // namespace bear

#endif // __BEAR_CHANGE_CAMERA_SIZE_HPP__

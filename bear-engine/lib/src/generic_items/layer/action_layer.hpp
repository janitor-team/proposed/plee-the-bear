/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file action_layer.hpp
 * \brief The layer where all entities live.
 * \author Julien Jorge
 */
#ifndef __BEAR_ACTION_LAYER_HPP__
#define __BEAR_ACTION_LAYER_HPP__

#include "engine/world.hpp"
#include "engine/layer/layer.hpp"
#include "engine/base_item.hpp"

#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief The layer where all entities live.
   */
  class GENERIC_ITEMS_EXPORT action_layer:
    public engine::layer
  {
  public:
    action_layer( const universe::size_box_type& size );

    virtual void start();

    virtual void progress
    ( const region_type& active_area, universe::time_type elapsed_time  );

  private:
    void do_get_visual( std::list<engine::scene_visual>& visuals,
                        const universe::rectangle_type& visible_area ) const;

    void do_add_item( engine::base_item& that );
    void do_remove_item( engine::base_item& item );
    void do_drop_item( engine::base_item& item );

    void static_item( engine::base_item& that );
    void mobile_item( engine::base_item& that );

    engine::world* do_get_world();
    const engine::world* do_get_world() const;

  private:
    /** \brief The world, and physic rules associated. */
    engine::world m_world;

  }; // class action_layer
} // namespace bear

#endif // __BEAR_ACTION_LAYER_HPP__

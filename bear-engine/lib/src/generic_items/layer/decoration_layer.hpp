/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file decoration_layer.hpp
 * \brief A decoration layer contains animation and sprites positioned in the
 *        world.
 * \author Julien Jorge
 */
#ifndef __BEAR_DECORATION_LAYER_HPP__
#define __BEAR_DECORATION_LAYER_HPP__

#include "visual/screen.hpp"
#include "universe/static_map.hpp"
#include "engine/layer/layer.hpp"
#include "engine/base_item.hpp"

#include <claw/math.hpp>

#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief A decoration layer contains animation and sprites positioned in
   *        the world.
   */
  class GENERIC_ITEMS_EXPORT decoration_layer:
    public engine::layer
  {
  public:
    decoration_layer( const universe::size_box_type& size );
    virtual ~decoration_layer();

    void start();

    void progress
    ( const region_type& active_area, universe::time_type elapsed_time  );

    void log_statistics() const;

  private:
    void do_get_visual( std::list<engine::scene_visual>& visuals,
                        const universe::rectangle_type& visible_area ) const;

    void do_add_item( engine::base_item& item );
    void do_remove_item( engine::base_item& item );
    void do_drop_item( engine::base_item& item );

  private:
    /** \brief All the decorations. */
    universe::static_map<engine::base_item*> m_items;

    /** \brief All global items. */
    std::list<engine::base_item*> m_global_items;

  }; // class decoration_layer
} // namespace bear

#endif // __BEAR_DECORATION_LAYER_HPP__

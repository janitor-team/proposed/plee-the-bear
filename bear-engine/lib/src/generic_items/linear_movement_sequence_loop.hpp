/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file linear_movement_sequence_loop.hpp
 * \brief  A linear_movement_sequence_loop is an item that creates some items
 * and sets their movement as a linear movement
 * \author Sébastien Angibaud
 */
#ifndef __BEAR_LINEAR_MOVEMENT_SEQUENCE_LOOP_HPP__
#define __BEAR_LINEAR_MOVEMENT_SEQUENCE_LOOP_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief A linear_movement_sequence_loop is an item that creates some items
   * and sets their movement as a linear movement.
   *
   * The custom fields of this class are:
   * - duration (real) The duration of the movement (default = 1)
   * - length.x (real) The length of the movement on x-axis (default = 0)
   * - length.y (real) The length of the movement on y-axis (default = 0)
   * - positions (list of real) The item positions according the duration
   * - item (item_reference) The item that is copied.
   * - any field supported by the parent class.
   *
   * \author Sébastien Angibaud
   */
  class GENERIC_ITEMS_EXPORT linear_movement_sequence_loop:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(linear_movement_sequence_loop);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    linear_movement_sequence_loop();

    void build();

    bool set_item_field( const std::string& name, base_item* value );
    bool set_real_field( const std::string& name, double value );
    bool set_real_list_field
    ( const std::string& name, const std::vector<double>& value );
    bool is_valid() const;

  private:
    void create_items();

  private:
    /** \brief The item that is copied. */
    engine::base_item* m_item;

    /** \brief The duration of the linear_movement_sequence_loop. */
    universe::time_type m_duration;

    /** \brief The item position at the beginning. */
    std::vector<double> m_positions;

    /** \brief The length of the movement on x-axis. */
    universe::coordinate_type m_x_length;

    /** \brief The length of the movement on y-axis. */
    universe::coordinate_type m_y_length;
  }; // class linear_movement_sequence_loop
} // namespace bear

#endif // __BEAR_LINEAR_MOVEMENT_SEQUENCE_LOOP_HPP__

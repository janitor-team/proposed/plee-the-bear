/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file debug_model.hpp
 * \brief An item used to debug a model. It will just display the model.
 * \author Julien Jorge
 */
#ifndef __BEAR_DEBUG_MODEL_HPP__
#define __BEAR_DEBUG_MODEL_HPP__

#include "engine/base_item.hpp"
#include "engine/model.hpp"
#include "engine/export.hpp"

#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief An item used to debug a model. It will just display the model.
   *
   * The valid fields for this item are
   *  - \a model_file: (string) the path to the model file.
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT debug_model:
    public engine::model<engine::base_item>
  {
    DECLARE_BASE_ITEM(debug_model);

  public:
    typedef engine::model<engine::base_item> super;

  public:
    bool is_valid() const;

    void build();

    bool set_string_field( const std::string& name, const std::string& value );

  private:
    /** \brief The path to the model to load. */
    std::string m_model_path;

  }; // class debug_model
} // namespace bear

#endif // __BEAR_DEBUG_MODEL_HPP__

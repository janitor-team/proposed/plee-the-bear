/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file debug_model.cpp
 * \brief Implementation of the bear::debug_model class.
 * \author Julien Jorge
 */
#include "generic_items/debug/debug_model.hpp"

BASE_ITEM_EXPORT( debug_model, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is well prepared.
 */
bool bear::debug_model::is_valid() const
{
  return !m_model_path.empty() && super::is_valid();
} // debug_model::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::debug_model::build()
{
  super::build();

  set_model_actor( get_level_globals().get_model(m_model_path) );
  start_model_action( "idle" );
} // debug_model::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type string.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::debug_model::set_string_field
( const std::string& name, const std::string& value )
{
  bool result = true;

  if ( name == "debug_model.model_file" )
    m_model_path = value;
  else
    result = super::set_string_field(name, value);

  return result;
} // bear::debug_model::set_string_field()

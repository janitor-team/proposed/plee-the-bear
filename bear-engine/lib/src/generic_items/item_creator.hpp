/*
  Plee the Bear

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file item_creator.hpp
 * \brief  An item that creates some items when it is activated.
 * \author Sébastien Angibaud
 */
#ifndef __BEAR_ITEM_CREATOR_HPP__
#define __BEAR_ITEM_CREATOR_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/export.hpp"

#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief An item that creates some items when it is activated.
   *
   * The custom fields of this class are:
   * - items (list of reference item) The list of item to create (required).
   * - any field supported by the parent class.
   *
   * \author Sébastien Angibaud
   */
  class GENERIC_ITEMS_EXPORT item_creator:
    public engine::item_with_toggle<engine::base_item>
  {
    DECLARE_BASE_ITEM(item_creator);

  public:
    /** \brief The type of the parent class. */
    typedef engine::item_with_toggle<engine::base_item> super;

  public:
    ~item_creator();

    void build();

    bool set_item_list_field
    ( const std::string& name, const std::vector<base_item*>& value );

    bool is_valid() const;

  private:
    void on_toggle_on( base_item* activator );

  private:
    /** \brief Items that are created. */
    std::vector<engine::base_item*> m_items;
  }; // class item_creator
} // namespace bear

#endif // __BEAR_ITEM_CREATOR_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file add_script_actor.hpp
 * \brief An item that add an actor at a script director.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_ADD_SCRIPT_ACTOR_HPP__
#define __BEAR_ADD_SCRIPT_ACTOR_HPP__

#include "engine/base_item.hpp"
#include "universe/derived_item_handle.hpp"
#include "generic_items/script/script_director.hpp"
#include "generic_items/class_export.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief An item that add an actor at a script director.
   * \author Sebastien Angibaud
   *
   * The custom fields of this class are:
   * - \a script_director (reference_item) [required], the script director,
   * - \a actor (reference_item) [required], the new actor,
   * - \a actor_name (string) [required], the name of the actor,
   * - any field supported by the parent class.
   */
  class GENERIC_ITEMS_EXPORT add_script_actor:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(add_script_actor);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  private:
    /** \brief The type of handle on actor. */
    typedef universe::derived_item_handle<engine::base_item> handle_type;

  public:
    add_script_actor();

    void set_actor(script_director& director);
    bool set_item_field( const std::string& name, base_item* value );
    bool set_string_field( const std::string& name, const std::string& value );

    bool is_valid() const;

  private:
    /** \brief The new actor. */
    handle_type m_actor;

    /** \brief The name of the actor. */
    std::string m_actor_name;
  }; // class add_script_actor
} // namespace bear

#endif // __BEAR_ADD_SCRIPT_ACTOR_HPP__

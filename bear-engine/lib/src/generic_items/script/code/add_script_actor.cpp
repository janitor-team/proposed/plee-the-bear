/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file add_script_actor.cpp
 * \brief Implementation of the bear::add_script_actor class.
 * \author Sebastien Angibaud
 */
#include "generic_items/script/add_script_actor.hpp"

BASE_ITEM_EXPORT( add_script_actor, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::add_script_actor::add_script_actor()
  : m_actor(NULL)
{
  set_phantom(true);
  set_can_move_items(false);
  set_artificial(true);
} // add_script_actor::add_script_actor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give the actor at the script director.
 * \param director The script director.
 */
void bear::add_script_actor::set_actor(script_director& director)
{
  if ( m_actor != (base_item*)NULL )
    director.set_actor_item(m_actor_name, m_actor.get());
} // add_script_actor::set_actor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c base_item.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::add_script_actor::set_item_field
( const std::string& name, base_item* value )
{
  bool ok = true;

  if (name == "add_script_actor.actor")
    m_actor = value;
  else
    ok = super::set_item_field(name, value);

  return ok;
} // add_script_actor::set_item_field()
/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c <string>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::add_script_actor::set_string_field
( const std::string& name, const std::string& value )
{
  bool ok = true;

  if (name == "add_script_actor.actor_name")
    m_actor_name = value;
  else
    ok = super::set_string_field(name, value);

  return ok;
} // add_script_actor::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is correctly initialized.
 */
bool bear::add_script_actor::is_valid() const
{
  return ( m_actor != (base_item*)NULL ) &&
    !m_actor_name.empty() && super::is_valid();
} // add_script_actor::is_valid()

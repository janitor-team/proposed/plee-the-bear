/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file script_director.cpp
 * \brief Implementation of the bear::script_director class.
 * \author Julien Jorge
 */
#include "generic_items/script/script_director.hpp"
#include "generic_items/script/add_script_actor.hpp"

BASE_ITEM_EXPORT( script_director, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::script_director::script_director()
{
  set_global(true);
  set_phantom(true);
  set_can_move_items(false);
  set_artificial(true);
} // script_director::script_director()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c <string>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::script_director::set_string_field
( const std::string& name, const std::string& value )
{
  bool ok = true;

  if (name == "script_director.script")
    {
      claw::logger << claw::log_verbose << "Loading script '" << value << '\''
                   << std::endl;
      ok = load_script(value);
    }
  else
    ok = super::set_string_field(name, value);

  return ok;
} // script_director::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of item.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::script_director::set_item_list_field
( const std::string& name, const std::vector<base_item*>& value )
{
  bool ok = true;

  if (name == "script_director.actors")
    {
      m_actors.resize(value.size());

      for ( std::size_t i=0; i!=value.size(); ++i )
        m_actors[i] = value[i];
    }
  else
    ok = super::set_item_list_field(name, value);

  return ok;
} // script_director::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Turn the item on.
 * \param activator The item that activates this item.
 */
void bear::script_director::on_toggle_on(engine::base_item *activator)
{
  reset();

  for ( std::size_t i=0; i!=m_actors.size(); ++i )
    if ( m_actors[i] != (add_script_actor*)NULL )
      m_actors[i]->set_actor(*this);
} // script_director::on_toggle_on()

/*----------------------------------------------------------------------------*/
/**
 * \brief Continue the progression of the item, while active.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::script_director::progress_on(universe::time_type elapsed_time)
{
  play(elapsed_time);
} // script_director::progress_on()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the items concerned by a progress/move of this one.
 * \param d (out) A list to which are added such items.
 */
void bear::script_director::get_dependent_items
( std::list<physical_item*>& d ) const
{
  engine::script_context::actor_item_map_const_iterator_type it;

  for ( it = get_context().get_actors_item_begin();
        it != get_context().get_actors_item_end(); ++it )
    if ( it->second != (base_item*)NULL )
      d.push_front( it->second.get_item() );
} // script_director::get_dependent_items()

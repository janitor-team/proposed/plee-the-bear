/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file script_director.hpp
 * \brief An item that plays a script.
 * \author Julien Jorge
 */
#ifndef __BEAR_SCRIPT_DIRECTOR_HPP__
#define __BEAR_SCRIPT_DIRECTOR_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/script/script_runner.hpp"

#include "generic_items/class_export.hpp"

#include "engine/export.hpp"

namespace bear
{
  class add_script_actor;

  /**
   * \brief An item that plays a script.
   * \author Julien Jorge
   *
   * The custom fields of this class are:
   * - \a script (string) [required], the script to play,
   * - any field supported by the parent class.
   */
  class GENERIC_ITEMS_EXPORT script_director:
    public engine::item_with_toggle<engine::base_item>,
    public engine::script_runner
  {
    DECLARE_BASE_ITEM(script_director);

  public:
    /** \brief The type of the parent class. */
    typedef engine::item_with_toggle<engine::base_item> super;

  private:
    /** \brief The type of the handles on the items to kill. */
    typedef universe::derived_item_handle<add_script_actor> handle_type;

  public:
    script_director();

    bool set_string_field( const std::string& name, const std::string& value );
    bool set_item_list_field
    ( const std::string& name, const std::vector<engine::base_item*>& value );

  private:
    void on_toggle_on(engine::base_item *activator);
    void progress_on(universe::time_type elapsed_time);
    void get_dependent_items( std::list<physical_item*>& d ) const;

  private:
    /** \brief The list of items that add an actor. */
    std::vector<handle_type> m_actors;

  }; // class script_director
} // namespace bear

#endif // __BEAR_SCRIPT_DIRECTOR_HPP__

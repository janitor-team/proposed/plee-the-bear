/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file hidden_block.hpp
 * \brief A class representing a hidden_block.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_HIDDEN_BLOCK_HPP__
#define __BEAR_HIDDEN_BLOCK_HPP__

#include "generic_items/block.hpp"
#include "generic_items/class_export.hpp"
#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A class representing a block that change its opacity
   * when it collides an other item.
   *
   * The custom fields of this class are:
   *  - \a opacity.passive: \c (real) The opacity when no item collides.
   *  - \a opacity.active: \c (real) The opacity when we an item collides.
   *  - \a transition_duration: \c (real) The duration for opacity changing.
   * \author Sebastien Angibaud
   */
  class GENERIC_ITEMS_EXPORT hidden_block:
    public block
  {
    DECLARE_BASE_ITEM(hidden_block);

    typedef block super;

  public:
    hidden_block();
    void progress( universe::time_type elapsed_time );

    bool set_real_field( const std::string& name, double value );

  private:
    void collision( engine::base_item& that, universe::collision_info& info );
    void select_active_opacity();
    void select_passive_opacity();

  protected:
    void to_string( std::string& str ) const;

  private:
    /** \brief Indicates if an item collides. */
    bool m_active_state;

    /** \brief Indicates if there is a new collision during the iteration. */
    bool m_new_collision;

    /** \brief The duration of the change of opacity. */
    bear::universe::time_type m_transition_duration;

    /** \brief The time since the last modification. */
    bear::universe::time_type m_last_modification;

    /** \brief The opacity when no item collides. */
    double m_passive_opacity;

    /** \brief The opacity when an item collides. */
    double m_active_opacity;

  }; // class hidden_block
} // namespace bear

#endif // __BEAR_HIDDEN_BLOCK_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file path_trace.hpp
 * \brief A path_trace shows the path used by an item.
 * \author Julien Jorge
 */
#ifndef __BEAR_PATH_TRACE_HPP__
#define __BEAR_PATH_TRACE_HPP__

#include "engine/base_item.hpp"
#include "universe/const_item_handle.hpp"

#include "generic_items/class_export.hpp"
#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A path_trace shows the path used by an item.
   *
   * A path_trace is a filled polygon made with the previous top and bottom
   * positions of a given item.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT path_trace:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(path_trace);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

    /** \brief The type of the handle on the traced item. */
    typedef universe::const_item_handle handle_type;

  private:
    /** \brief A list of positions. */
    typedef std::list<universe::position_type> position_list;

    /** \brief The type of a function to use to do the progression of this
        instance. */
    typedef void (path_trace::*progress_function_type)
    ( universe::time_type elapsed_time );

  public:
    path_trace();
    explicit path_trace( const base_item& ref );

    void set_fill_color( const visual::color_type& c );
    void set_item( const base_item& ref );
    void set_fade_out_speed( double s );

    void progress( universe::time_type elapsed_time );
    void get_visual( std::list<engine::scene_visual>& visuals ) const;

  private:
    void progress_void( universe::time_type elapsed_time );
    void progress_alive( universe::time_type elapsed_time );
    void progress_dead( universe::time_type elapsed_time );

  private:
    /** \brief The function to use to do the progression of this instance. */
    progress_function_type m_progress;

    /** \brief The item we are following. */
    handle_type m_item;

    /** \brief The previous positions of the top of the item. */
    position_list m_previous_top;

    /** \brief The previous positions of the bottom of the item. */
    position_list m_previous_bottom;

    /** \brief The color of the trace. */
    visual::color_type m_fill_color;

    /** \brief The opacity of the trace. */
    double m_opacity;

    /** \brief The speed of the fade out when the item is dead. */
    double m_fade_out_speed;

  }; // class path_trace
} // namespace bear

#endif // __BEAR_PATH_TRACE_HPP__

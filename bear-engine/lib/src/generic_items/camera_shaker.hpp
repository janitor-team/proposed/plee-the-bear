/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file camera_shaker.hpp
 * \brief An item that shakes the camera.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_CAMERA_SHAKER_HPP__
#define __BEAR_CAMERA_SHAKER_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief An item that shakes the camera.
   *
   * The valid fields for this item are
   *  - \a shaker_force: (real) force of the shaker (default = 10),
   *  - \a check_camera_intersection: (bool) Indicates if the shaker checks
   * camera intersection.
   *  - any field supported by the parent classes.
   *
   * \author Sebastien Angiabud
   */
  class GENERIC_ITEMS_EXPORT camera_shaker:
    public engine::item_with_toggle<engine::base_item>
  {
    DECLARE_BASE_ITEM(camera_shaker);

  public:
    typedef engine::item_with_toggle<engine::base_item> super;

  public:
    camera_shaker();

    static void shake_around
      ( const engine::base_item& item, double force,
        universe::time_type duration, audio::sample* s = NULL );

    bool set_real_field( const std::string& name, double value );
    bool set_bool_field( const std::string& name, bool value );

    void set_shaker_force(double force);
    void check_camera_intersection(bool value);

    void progress_on( universe::time_type elapsed_time );

  private:
    /** \brief The force of the agitation. */
    double m_shaker_force;

    /** \brief Indicates if the shaker checks camera intersection. */
    bool m_camera_intersection;

  }; // class camera_shaker
} // namespace bear

#endif // __BEAR_CAMERA_SHAKER_HPP__

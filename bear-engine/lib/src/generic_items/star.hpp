/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file star.hpp
 * \brief A class to display a star.
 * \author Julien Jorge
 */
#ifndef __BEAR_STAR_HPP__
#define __BEAR_STAR_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "visual/star.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief A class to display a star.
   *
   * The custom fields of this class are:
   * - branches (unsigned int): the number of branches in the star
   *   (default = 5),
   * - ratio (real): the ratio of the inner vertices relatively to the outer
   *   ones (default = 0.5),
   * - border_width (real): the width of the border of the star (default = 1),
   * - border_color.red (real): the red component of the color of the border
   *   (default = 0),
   * - border_color.green (real): the green component of the color of the border
   *   (default = 0),
   * - border_color.blue (real): the blue component of the color of the border
   * - border_color.opacity (real): the opacity of the border (default = 1),
   * - fill_color.red (real): the red component of the color of the inside
   *   (default = 0),
   * - fill_color.green (real): the green component of the color of the inside
   *   (default = 0),
   * - fill_color.blue (real): the blue component of the color of the inside
   * - fill_color.opacity (real): the opacity of the inside (default = 1),
   * - any field supported by the parent class.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT star:
    public engine::basic_renderable_item<engine::base_item>
  {
    DECLARE_BASE_ITEM(star);

  public:
    /** \brief The type of the parent class. */
    typedef engine::basic_renderable_item<engine::base_item> super;

  public:
    star();
    star
      ( std::size_t branches, double ratio, const visual::color_type& color,
        double border_width = 1,
        const visual::color_type& fill_color
        = claw::graphic::transparent_pixel );

    void build();

    bool set_u_integer_field( const std::string& name, unsigned int value );
    bool set_real_field( const std::string& name, double value );

    void get_visual( std::list<engine::scene_visual>& visuals ) const;

  private:
    /** \brief The star to display. */
    visual::star m_star;

    /** \brief The color of the inside. */
    visual::color_type m_fill_color;

    /** \brief The color of the border. */
    visual::color_type m_border_color;

    /** \brief The width of the border. */
    double m_border_width;

  }; // class star
} // namespace bear

#endif // __BEAR_STAR_HPP__

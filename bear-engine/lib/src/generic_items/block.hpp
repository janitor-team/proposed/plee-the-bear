/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file block.hpp
 * \brief A class representing a block.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_BLOCK_HPP__
#define __BEAR_BLOCK_HPP__

#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_activable_sides.hpp"
#include "engine/item_brick/item_with_decoration.hpp"
#include "engine/item_brick/item_with_friction.hpp"
#include "engine/item_brick/item_with_restricted_z_collision.hpp"
#include "engine/item_brick/item_with_z_shift.hpp"
#include "universe/zone.hpp"

#include "generic_items/class_export.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A class representing an block.
   *
   * The custom fields of this class are:
   * - collision_threshold: unsigned integer, a threshold for the collisions in
   *   the corners of the item, to choose if the other item is aligned on the
   *   collided side or the nearest adjacent one (default = 10).
   * - top_sets_contact: boolean, tell if a collision on the top side sets the
   *   contact flag of the colliding item (default = true).
   * - bottom_sets_contact: boolean, tell if a collision on the bottom side sets
   *   the contact flag of the colliding item (default = true).
   * - left_sets_contact: boolean, tell if a collision on the left side sets the
   *   contact flag of the colliding item (default = true).
   * - right_sets_contact: boolean, tell if a collision on the right side sets
   *   the contact flag of the colliding item (default = true).
   * - middle_sets_contact: boolean, tell if a collision on the middle sets the
   *   contact flag of the colliding item (default = true).
   *
   * \author Sebastien Angibaud
   */
  class GENERIC_ITEMS_EXPORT block:
    public engine::item_with_friction
    < engine::item_with_restricted_z_collision
      < engine::item_with_z_shift
        < engine::item_with_activable_sides
          < engine::item_with_decoration
            < engine::basic_renderable_item<engine::base_item>
            >
          >
        >
      >
    >
  {
    DECLARE_BASE_ITEM(block);

    typedef engine::item_with_friction
    < engine::item_with_restricted_z_collision
      < engine::item_with_z_shift
        < engine::item_with_activable_sides
          < engine::item_with_decoration
            < engine::basic_renderable_item<engine::base_item>
            >
          >
        >
      >
    > super;

  public:
    block();

    bool set_bool_field( const std::string& name, bool value );
    bool set_u_integer_field( const std::string& name, unsigned int value );

  protected:
    bool collision_check_and_align
    ( engine::base_item& that, universe::collision_info& info );

  private:
    universe::zone::position choose_alignment_side
    ( const engine::base_item& that,
      const universe::collision_info& info ) const;

    void collision
    ( engine::base_item& that, universe::collision_info& info );

    bool align_bottom
      ( engine::base_item& that, universe::collision_info& info );
    bool align_top( engine::base_item& that, universe::collision_info& info );
    bool align_left( engine::base_item& that, universe::collision_info& info );
    bool align_right( engine::base_item& that, universe::collision_info& info );
    bool align_middle
      ( engine::base_item& that, universe::collision_info& info );

    void post_alignment( engine::base_item& that, double f );

  private:
    /** \brief A threshold for the collisions in the corners of the item, to
        choose if the other item is aligned on the collided side or the nearest
        adjacent one. */
    universe::coordinate_type m_collision_threshold;

    /** \brief Tell if the contact is set when colliding on the top side. */
    bool m_top_sets_contact;

    /** \brief Tell if the contact is set when colliding on the bottom side. */
    bool m_bottom_sets_contact;

    /** \brief Tell if the contact is set when colliding on the left side. */
    bool m_left_sets_contact;

    /** \brief Tell if the contact is set when colliding on the right side. */
    bool m_right_sets_contact;

    /** \brief Tell if the contact is set when colliding on the middle. */
    bool m_middle_sets_contact;

  }; // class block
} // namespace bear

#endif // __BEAR_BLOCK_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file static_decorative_item.hpp
 * \brief An item existing only for a decoration purpose.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_DECORATIVE_ITEM_HPP__
#define __BEAR_DECORATIVE_ITEM_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"
#include "engine/item_brick/item_with_text.hpp"

#include "generic_items/class_export.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief An item existing only for a decoration purpose.
   * \author Sebastien Angibaud
   * The fields of this item are
   *  - \a kill_on_contact: (bool) \c Indicates if the item kills himself when
   * he has a contact (default = false).
   *  - \a kill_when_leaving: (bool) \c Indicates if the item kills himself when
   * he leaves the active region (default = false).
   *  - \a kill_when finished: (bool) \c Indicates if the item kills himself
   * when the animation is finished (default = false).
   *
   */
  class GENERIC_ITEMS_EXPORT decorative_item:
    public engine::item_with_text
    < engine::item_with_decoration
      < engine::basic_renderable_item<engine::base_item> > >
  {
    DECLARE_BASE_ITEM(decorative_item);

    /** \brief The type of the parent class. */
    typedef engine::item_with_text
    < engine::item_with_decoration
      < engine::basic_renderable_item<engine::base_item> > > super;

  public:
    decorative_item();

    void progress( universe::time_type elapsed_time );

    bool set_bool_field( const std::string& name, bool value );

    void set_kill_when_finished(bool value);
    void set_kill_on_contact(bool value);
    void set_kill_when_leaving(bool value);

  private:
    void leaves_active_region();

  private:
    /** \brief Indicates if the item kills himself when the animation is
        finished. */
    bool m_kill_when_finished;

    /** \brief Indicates if the item kills himself when he has a contact. */
    bool m_kill_on_contact;

    /** \brief Indicates if the item stops
        himself when he has a bootom contact. */
    bool m_stop_on_bottom_contact;

    /** \brief Tell if the item kills himself when he leaves the active
        region. */
    bool m_kill_when_leaving;

  }; // class decorative_item
} // namespace bear

#endif // __BEAR_DECORATIVE_ITEM_HPP__

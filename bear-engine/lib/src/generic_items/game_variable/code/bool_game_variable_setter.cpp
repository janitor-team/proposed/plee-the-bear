/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file bool_game_variable_setter.cpp
 * \brief Implementation of the bear::bool_game_variable_setter class.
 * \author Sebastien Angibaud
 */
#include "generic_items/game_variable/bool_game_variable_setter.hpp"

#include "engine/game.hpp"
#include "engine/variable/variable.hpp"

BASE_ITEM_EXPORT( bool_game_variable_setter, bear )
BASE_ITEM_EXPORT( bool_game_variable_setter_suicide, bear )
BASE_ITEM_EXPORT( bool_game_variable_setter_toggle, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::bool_game_variable_setter::bool_game_variable_setter()
  : m_value(false)
{

} // bool_game_variable_setter::bool_game_variable_setter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is correctly initialized.
 */
bool bear::bool_game_variable_setter::is_valid() const
{
  return !m_name.empty() && super::is_valid();
} // bool_game_variable_setter::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <bool>.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::bool_game_variable_setter::set_bool_field
( const std::string& name, bool value )
{
  bool result = true;

  if ( name == "bool_game_variable_setter.value" )
    m_value = value;
  else
    result = super::set_bool_field( name, value );

  return result;
} // bool_game_variable_setter::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::bool_game_variable_setter::set_string_field
( const std::string& name, const std::string& value )
{
  bool result = true;

  if ( name == "bool_game_variable_setter.name" )
    m_name = value;
  else
    result = super::set_string_field(name, value);

  return result;
} // bool_game_variable_setter::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assign the value to the game variable.
 */
void bear::bool_game_variable_setter::assign_game_variable_value() const
{
  engine::game::get_instance().set_game_variable
    ( engine::variable<bool>( m_name, m_value ) );
} // bool_game_variable_setter::assign_game_variable_value()

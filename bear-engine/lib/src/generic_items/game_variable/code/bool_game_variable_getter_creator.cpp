/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file bool_game_variable_getter_creator.cpp
 * \brief Implementation of the bear::bool_game_variable_getter_creator class.
 * \author Sébastien Angibaud
 */
#include "generic_items/game_variable/bool_game_variable_getter_creator.hpp"

BASE_ITEM_EXPORT( bool_game_variable_getter_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::bool_game_variable_getter_creator::bool_game_variable_getter_creator()
{
  m_expr.set_default_value(false);
} // bool_game_variable_getter_creator::bool_game_variable_getter_creator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::bool_game_variable_getter_creator::build()
{
  kill();
} // bool_game_variable_getter_creator::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is correctly initialized.
 */
bool bear::bool_game_variable_getter_creator::is_valid() const
{
  return !m_expr.get_name().empty() && super::is_valid();
} // bool_game_variable_getter_creator::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::bool_game_variable_getter_creator::set_string_field
( const std::string& name, const std::string& value )
{
  bool result = true;

  if ( name == "bool_game_variable_getter_creator.name" )
    m_expr.set_name(value);
  else
    result = super::set_string_field(name, value);

  return result;
} // bool_game_variable_getter_creator::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c boolean.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::bool_game_variable_getter_creator::set_bool_field
( const std::string& name, bool value )
{
  bool result = true;

  if ( name == "bool_game_variable_getter_creator.default_value" )
    m_expr.set_default_value(value);
  else
    result = super::set_bool_field(name, value);

  return result;
} // bool_game_variable_getter_creator::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression created by this item.
 */
bear::expr::boolean_expression
bear::bool_game_variable_getter_creator::do_get_expression() const
{
  return m_expr;
} // bool_game_variable_getter_creator::do_get_expression()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file class_explort.hpp
 * \brief Definition of the GENERIC_ITEMS_EXPORT macro according to the fact
 *        that we create the library or link to it.
 * \author Julien Jorge
 */
#ifndef __BEAR_GENERIC_ITEMS_CLASS_EXPORT_HPP__
#define __BEAR_GENERIC_ITEMS_CLASS_EXPORT_HPP__

#ifndef GENERIC_ITEMS_EXPORT
#  ifdef _WIN32
#    ifdef bear_generic_items_EXPORTS
#      define GENERIC_ITEMS_EXPORT __declspec(dllexport)
#    else
#      define GENERIC_ITEMS_EXPORT __declspec(dllimport)
#    endif // def bear_generic_items_EXPORTS
#  else // def _WIN32
#    define GENERIC_ITEMS_EXPORT
#  endif // def _WIN32
#endif // ndef GENERIC_ITEMS_EXPORT

#endif // __BEAR_GENERIC_ITEMS_CLASS_EXPORT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file ambient_sound.hpp
 * \brief A sound in the level.
 * \author Julien Jorge
 */
#ifndef __BEAR_AMBIENT_SOUND_HPP__
#define __BEAR_AMBIENT_SOUND_HPP__

#include "audio/sample.hpp"
#include "engine/base_item.hpp"
#include "engine/item_brick/item_with_toggle.hpp"

#include "generic_items/class_export.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A sound in the level.
   *
   * The following fields are supported by this item:
   *  - sample (string) [required], the path of the sound to play,
   *  - fadeout (real), fade the sample out during this duration, in seconds,
   *    after the time limit (default = 0).
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT ambient_sound:
    public engine::item_with_toggle<engine::base_item>
  {
    DECLARE_BASE_ITEM(ambient_sound);

    /** \brief The type of the parent class. */
    typedef engine::item_with_toggle<engine::base_item> super;

  public:
    ambient_sound();

  }; // class ambient_sound
} // namespace bear

#endif // __BEAR_AMBIENT_SOUND_HPP__

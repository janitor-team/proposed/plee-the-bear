/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file static_decorative_flow.hpp
 * \brief A decorative sprite.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_DECORATIVE_FLOW_HPP__
#define __BEAR_DECORATIVE_FLOW_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"
#include "engine/item_brick/item_with_activable_sides.hpp"
#include "generic_items/class_export.hpp"
#include "engine/scene_visual.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A rectangle in which decorative sprite move according to a flow.
   * \author Sebastien Angibaud
   * The fields of this item are
   *  - \a min_speed.x : (real) \c The minimum speed on X-axis (default = 0).
   *  - \a min_speed.y : (real) \c The minimum speed on Y-axis (default = 0).
   *  - \a max_speed.x : (real) \c The maximum speed on X-axis (default = 1).
   *  - \a max_speed.y : (real) \c The maximum speed on Y-axis (default = 1).
   *  - \a items_per_second : (real) \c The number of decoration generated per
   *    second (default = 1).
   *  - any field supported by the parent classes.
   *
   */
  class GENERIC_ITEMS_EXPORT decorative_flow:
    public engine::item_with_activable_sides
    < engine::item_with_decoration
      < engine::basic_renderable_item<engine::base_item> >
    >
  {
    DECLARE_BASE_ITEM(decorative_flow);

  private:
    /** \brief The type of the parent class. */
    typedef engine::item_with_activable_sides
    < engine::item_with_decoration
      < engine::basic_renderable_item<engine::base_item> > > super;

  public:
    decorative_flow();

    virtual void progress( universe::time_type elapsed_time );
    void build();
    virtual bool set_real_field( const std::string& name, double value );

    virtual void get_visual( std::list<engine::scene_visual>& visuals ) const;

  private:
    void update_decorations( universe::time_type elapsed_time );
    void create_decoration();
    void initiate_decoration();

  private:
    /* \brief The minimum speed of the flow. */
    universe::speed_type m_min_speed_flow;

    /* \brief The maximum speed of the flow. */
    universe::speed_type m_max_speed_flow;

    /* \brief The number of item per second in average. */
    double m_item_per_second;

    std::list< universe::position_type > m_decorations;

    /* \brief The size of decorations. */
    universe::size_box_type m_decoration_size;

  }; // class decorative_flow
} // namespace bear

#endif // __BEAR_DECORATIVE_FLOW_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file descending_ceiling.hpp
 * \brief A ceiling that is not horizontal.
 * \author Julien Jorge
 */
#ifndef __BEAR_DESCENDING_CEILING_HPP__
#define __BEAR_DESCENDING_CEILING_HPP__

#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"
#include "engine/item_brick/item_with_restricted_z_collision.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief A ceiling that is not horizontal.
   *
   * The parameters accepted by this item are:
   *  - \a steepness: \c real The difference beween the Y-coordinate of the
   *    ceiling's right and left edges,
   *  - \a opposite_side_is_active: \c boolean Turn on/off the alignement on the
   *    top side,
   *  - \a left_side_is_active: \c boolean Turn on/off the alignement on the
   *    left side,
   *  - \a right_side_is_active: \c boolean Turn on/off the alignement on the
   *    right side,
   *  - \a apply_angle: \c boolean Tell if the angle of the ceiling is applied
   *    to the colliding items,
   *  - any field supported by the parent class.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT descending_ceiling:
    public engine::item_with_restricted_z_collision
    < engine::item_with_decoration
      < engine::basic_renderable_item<engine::base_item> >
    >
  {
    DECLARE_BASE_ITEM(descending_ceiling);

  public:
    /** \brief The type of the parent class. */
    typedef engine::item_with_restricted_z_collision
    < engine::item_with_decoration
      < engine::basic_renderable_item<engine::base_item> > > super;

    /** \brief The type of the line describing the surface of the ceiling. */
    typedef claw::math::line_2d<universe::coordinate_type> line_type;

  public:
    descending_ceiling();

    bool set_real_field( const std::string& name, double value );
    bool set_bool_field( const std::string& name, bool value );

    bool is_valid() const;

    void build();

    universe::coordinate_type get_steepness() const;

  protected:
    void collision_as_ceiling
    ( engine::base_item& that, universe::collision_info& info );

  private:
    void collision
    ( engine::base_item& that, universe::collision_info& info );

    bool check_left_contact_as_ceiling
    ( engine::base_item& that, universe::collision_info& info ) const;
    bool check_right_contact_as_ceiling
    ( engine::base_item& that, universe::collision_info& info ) const;
    bool check_top_below_ceiling
    ( engine::base_item& that, universe::collision_info& info ) const;

    bool align_on_ceiling
    ( engine::base_item& that, universe::collision_info& info );
    bool align_nearest_edge
    ( engine::base_item& that, universe::collision_info& info );

    bool item_crossed_down_up
    ( engine::base_item& that, const universe::collision_info& info ) const;

    void apply_angle_to
    ( engine::base_item& that, const universe::collision_info& info ) const;

  private:
    /** \brief The side opposite to the descending_ceiling, is it active ? */
    bool m_opposite_side_is_active;

    /** \brief Tell if the left side of the block is active. */
    bool m_left_side_is_active;

    /** \brief Tell if the right side of the block is active. */
    bool m_right_side_is_active;

    /** \brief Tell if the angle of the ceiling is applied to the colliding
        items. */
    bool m_apply_angle;

    /** \brief The line of the ground. */
    line_type m_line;

  }; // class descending_ceiling
} // namespace bear

#endif // __BEAR_DESCENDING_CEILING_HPP__

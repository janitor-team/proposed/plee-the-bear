/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_loader_toggle.hpp
 * \brief This class starts a level after a given delay.
 * \author Julien Jorge
 */
#ifndef __BEAR_LEVEL_LOADER_TOGGLE_HPP__
#define __BEAR_LEVEL_LOADER_TOGGLE_HPP__

#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/base_item.hpp"
#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This item starts a level when activated.
   *
   * The custom fields of this class are :
   * - \a fade_duration: real, the duration of the fading effect before starting
   *   the level (default = 1),
   * - \a level: string, required. The name of the game variable containing the
   *   name of the level to load or the path of the level itself,
   * - \a transition_layer_name: string, the name of the transition layer
   *   receiving the fade effect (default = none).
   * - any field supported by the parent class.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT level_loader_toggle:
    public engine::item_with_toggle<engine::base_item>
  {
    DECLARE_BASE_ITEM(level_loader_toggle);

  public:
    /** \brief The type of the parent class. */
    typedef engine::item_with_toggle<engine::base_item> super;

  public:
    level_loader_toggle();

    bool set_string_field( const std::string& name, const std::string& value );
    bool set_real_field( const std::string& name, double value );

    bool is_valid() const;

  private:
    void on_toggle_on( engine::base_item* activator );

    std::string get_string_from_vars( const std::string& name ) const;

  private:
    /** \brief The path of the level to load. */
    std::string m_level_path;

    /** \brief The name of the transition layer receiving the fade effect. */
    std::string m_transition_layer_name;

    /** \brief The duration of the fading. */
    universe::time_type m_fade_duration;

  }; // class level_loader_toggle
} // namespace bear

#endif // __BEAR_LEVEL_LOADER_TOGGLE_HPP__

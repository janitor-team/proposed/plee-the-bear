/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file clone_toggle.hpp
 * \brief This toggle creates a clone of an item each time it is turned on.
 * \author Julien Jorge
 */
#ifndef __BEAR_CLONE_TOGGLE_HPP__
#define __BEAR_CLONE_TOGGLE_HPP__

#include "engine/item_brick/item_with_toggle.hpp"
#include "engine/base_item.hpp"
#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This toggle creates a clone of an item each time it is turned on.
   *
   * The custom fields of this class are :
   * - \a items: list of items, the items to clone (default = none),
   * - any field supported by the parent class.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT clone_toggle:
    public engine::item_with_toggle<engine::base_item>
  {
    DECLARE_BASE_ITEM(clone_toggle);

  public:
    /** \brief The type of the parent class. */
    typedef engine::item_with_toggle<engine::base_item> super;

  public:
    ~clone_toggle();

    bool set_item_list_field
      ( const std::string& name, const std::vector<engine::base_item*>& value );

  private:
    void on_toggle_on( engine::base_item* activator );

  private:
    /** \brief The items to clone. */
    std::vector<engine::base_item*> m_items;

  }; // class clone_toggle
} // namespace bear

#endif // __BEAR_CLONE_TOGGLE_HPP__

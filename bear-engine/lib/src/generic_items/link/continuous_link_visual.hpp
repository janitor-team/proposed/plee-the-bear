/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file continuous_link_visual.hpp
 * \brief A class to display a continuous link between two items.
 * \author Julien Jorge
 */
#ifndef __BEAR_CONTINUOUS_LINK_VISUAL_HPP__
#define __BEAR_CONTINUOUS_LINK_VISUAL_HPP__

#include "generic_items/link/base_link_visual.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A class to display a continuous link between two items.
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT continuous_link_visual:
    public engine::item_with_decoration
    < engine::basic_renderable_item<base_link_visual> >
  {
    DECLARE_BASE_ITEM(continuous_link_visual);

  public:
    typedef engine::item_with_decoration
    < engine::basic_renderable_item<base_link_visual> > super;

  public:
    void get_visual( std::list<engine::scene_visual>& visuals ) const;

  }; // class continuous_link_visual
} // namespace bear

#endif // __BEAR_CONTINUOUS_LINK_VISUAL_HPP__

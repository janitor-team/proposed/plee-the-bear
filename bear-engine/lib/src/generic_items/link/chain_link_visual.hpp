/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file chain_link_visual.hpp
 * \brief A class to display a link between two items with a chain.
 * \author Julien Jorge
 */
#ifndef __BEAR_CHAIN_LINK_VISUAL_HPP__
#define __BEAR_CHAIN_LINK_VISUAL_HPP__

#include "generic_items/link/base_link_visual.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "engine/item_brick/item_with_decoration.hpp"

#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A class to display a link between two items as a chain.
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT chain_link_visual:
    public engine::item_with_decoration
    < engine::basic_renderable_item<base_link_visual> >
  {
    DECLARE_BASE_ITEM(chain_link_visual);

  public:
    typedef engine::item_with_decoration
    < engine::basic_renderable_item<base_link_visual> > super;

  public:
    chain_link_visual();

    bool set_real_field( const std::string& name, double value );
    bool set_u_integer_field( const std::string& name, unsigned int value );

    void get_visual( std::list<engine::scene_visual>& visuals ) const;

  private:
    /** \brief The count of links in the chain. */
    std::size_t m_link_count;

    /** \brief How far the the links can fall. */
    universe::coordinate_type m_max_fall;

  }; // class chain_link_visual
} // namespace bear

#endif // __BEAR_CHAIN_LINK_VISUAL_HPP__

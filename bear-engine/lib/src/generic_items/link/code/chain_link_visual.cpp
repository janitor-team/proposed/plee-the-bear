/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file chain_link_visual.cpp
 * \brief Implementation of the bear::chain_link_visual class.
 * \author Julien Jorge
 */
#include "generic_items/link/chain_link_visual.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( chain_link_visual, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::chain_link_visual::chain_link_visual()
  : m_link_count(0), m_max_fall(0)
{

} // chain_link_visual::chain_link_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::chain_link_visual::set_real_field
( const std::string& name, double value )
{
  bool result = true;

  if ( name == "chain_link_visual.max_fall" )
    m_max_fall = value;
  else
    result = super::set_real_field(name, value);

  return result;
} // chain_link_visual::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <unsigned int>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::chain_link_visual::set_u_integer_field
( const std::string& name, unsigned int value )
{
  bool result = true;

  if ( name == "chain_link_visual.links_count" )
    m_link_count = value;
  else
    result = super::set_u_integer_field(name, value);

  return result;
} // chain_link_visual::set_u_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the visuals of this item.
 * \param visuals (out) The visuals.
 */
void bear::chain_link_visual::get_visual
( std::list<engine::scene_visual>& visuals ) const
{
  const std::size_t n(m_link_count+2);
  universe::vector_type dir = get_end_position() - get_start_position();
  const double intensity =
    std::abs(get_end_position().x - get_start_position().x)
    / get_end_position().distance(get_start_position());
  const visual::sprite s(get_sprite());

  universe::vector_type ortho;

  if ( get_end_position().x < get_start_position(). x )
    ortho = dir.get_orthonormal_anticlockwise();
  else
    ortho = dir.get_orthonormal_clockwise();

  ortho.normalize();
  dir /= n-1;
  const visual::position_type origin( get_start_position() - s.get_size() / 2 );

  for (std::size_t i=0; i!=n; ++i)
    {
      visual::position_type p = dir * i;
      double fall_distance = std::sin( (double)i / n * 3.14159 )
        * intensity * m_max_fall;
      p += ortho * fall_distance;

      visuals.push_front
        ( engine::scene_visual( origin + p, s, get_z_position() ) );
    }
} // chain_link_visual::get_visual()

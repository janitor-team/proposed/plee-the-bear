/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file continuous_link_visual.cpp
 * \brief Implementation of the bear::continuous_link_visual class.
 * \author Julien Jorge
 */
#include "generic_items/link/continuous_link_visual.hpp"

#include "engine/export.hpp"

BASE_ITEM_EXPORT( continuous_link_visual, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the visuals of this item.
 * \param visuals (out) The visuals.
 */
void bear::continuous_link_visual::get_visual
( std::list<engine::scene_visual>& visuals ) const
{
  universe::coordinate_type length =
    get_end_position().distance(get_start_position());

  visual::sprite s(get_sprite());
  s.set_width(length);

  universe::position_type pos(get_center_of_mass());

  pos.x -= length / 2;
  pos.y += s.height() / 2;

  s.set_angle( std::atan2( get_end_position().y - get_start_position().y,
                           get_end_position().x - get_start_position().x ) );

  visuals.push_front( engine::scene_visual( pos, s, get_z_position() ) );
} // continuous_link_visual::get_visual()

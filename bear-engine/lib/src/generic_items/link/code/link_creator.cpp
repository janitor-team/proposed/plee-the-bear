/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file link_creator.cpp
 * \brief Implementation of the bear::link_creator class.
 * \author Julien Jorge
 */
#include "generic_items/link/link_creator.hpp"

#include "universe/link/link.hpp"

#include <climits>
#include <limits>

#include "engine/export.hpp"

BASE_ITEM_EXPORT( link_creator, bear )

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::link_creator::link_creator()
  : m_strength(1), m_minimal_length(0),
    m_maximal_length
  (std::numeric_limits<universe::coordinate_type>::infinity()),
    m_first_item(NULL), m_second_item(NULL)
{

} // link_creator::link_creator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <real>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::link_creator::set_real_field
( const std::string& name, double value )
{
  bool result = true;

  if ( name == "link_creator.strength" )
    m_strength = value;
  else if ( name == "link_creator.length.minimal" )
    m_minimal_length = value;
  else if ( name == "link_creator.length.maximal" )
    m_maximal_length = value;
  else
    result = super::set_real_field(name, value);

  return result;
} // link_creator::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type <item>.
 * \param name The name of the field.
 * \param value The value of the field.
 */
bool bear::link_creator::set_item_field
( const std::string& name, engine::base_item* value )
{
  bool result = true;

  if ( (name == "link_creator.first_item") && (value != NULL) )
    m_first_item = value;
  else if ( (name == "link_creator.second_item") && (value != NULL) )
    m_second_item = value;
  else
    result = super::set_item_field(name, value);

  return result;
} // link_creator::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if all required fields are initialized.
 */
bool bear::link_creator::is_valid() const
{
  return (m_first_item!=NULL) && (m_second_item!=NULL) && super::is_valid();
} // link_creator::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
void bear::link_creator::build()
{
  new universe::link( *m_first_item, *m_second_item, m_strength,
                      m_minimal_length, m_maximal_length );

  kill();
} // link_creator::build()

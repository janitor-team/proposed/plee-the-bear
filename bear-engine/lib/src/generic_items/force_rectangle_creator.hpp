/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file force_rectangle_creator.hpp
 * \brief This class adds a universe::force_rectangle in universe::world
 *         then dies.
 * \author Sebastien Angibaud
 */
#ifndef __BEAR_FORCE_RECTANGLE_CREATOR_HPP__
#define __BEAR_FORCE_RECTANGLE_CREATOR_HPP__

#include "engine/base_item.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief This class adds a universe::force_rectangle in universe::world
   *         then dies.
   *
   * The fields of this item are
   *  - \a force_x: (double)
   * \c the force in x axis in the rectangle (default = 0).
   *  - \a force_y: (double)
   * \c the force in y axis in the rectangle (default = 0).
   *
   * \author Sebastien Angibaud
   */
  class GENERIC_ITEMS_EXPORT force_rectangle_creator:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(force_rectangle_creator);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  public:
    force_rectangle_creator();

    bool set_real_field( const std::string& name, double value );

    void build();

  private:
    /** \brief Force applied to the items. */
    universe::force_type m_force;

  }; // class force_rectangle_creator
} // namespace bear

#endif // __BEAR_FORCE_RECTANGLE_CREATOR_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file static_decorative_effect.hpp
 * \brief A temporary decoration with little effects.
 * \author Julien Jorge
 */
#ifndef __BEAR_DECORATIVE_EFFECT_HPP__
#define __BEAR_DECORATIVE_EFFECT_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/basic_renderable_item.hpp"
#include "universe/derived_item_handle.hpp"

#include "generic_items/class_export.hpp"
#include "engine/export.hpp"

namespace bear
{
  /**
   * \brief A temporary decoration with little effects.
   * \author Julien Jorge
   *
   * This item has no custom fields.
   *
   */
  class GENERIC_ITEMS_EXPORT decorative_effect:
    public engine::base_item
  {
    DECLARE_BASE_ITEM(decorative_effect);

  private:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

    /** \brief The type of the handle of the modified item. */
    typedef universe::derived_item_handle
      <engine::with_rendering_attributes, engine::base_item> handle_type;

  public:
    decorative_effect();

    void build();
    void progress( universe::time_type elapsed_time );

    void set_item
      ( engine::base_item* item, bool same_lifespan, bool restore = false );

    void set_duration(universe::time_type d);
    universe::time_type get_duration() const;

    void set_size_factor_init(double f);
    void set_opacity_factor_init(double f);
    void set_angle_offset_init(double f);
    void set_intensity_factor_init(double r, double g, double b);

    void set_size_factor_end(double f);
    void set_opacity_factor_end(double f);
    void set_angle_offset_end(double f);
    void set_intensity_factor_end(double r, double g, double b);

    void set_size_factor(double init, double end);
    void set_opacity_factor(double init, double end);
    void set_angle_offset(double init, double end);
    void set_intensity_factor
      ( double init_r, double init_g, double init_b,
        double end_r, double end_g, double end_b );

  private:
    void apply_effect() const;

  private:
    /** \brief The initial rendering attributes, used for the reference. */
    visual::bitmap_rendering_attributes m_rendering_attributes;

    /** \brief The total duration of the effect. */
    universe::time_type m_duration;

    /** \brief Elapsed time since the begining of the effect. */
    universe::time_type m_elapsed_time;

    /** \brief Initial factor applied to the size. */
    double m_size_factor_init;

    /** \brief Final factor applied to the size. */
    double m_size_factor_end;

    /** \brief Initial factor applied to the opacity. */
    double m_opacity_factor_init;

    /** \brief Final factor applied to the opacity. */
    double m_opacity_factor_end;

    /** \brief Initial offset applied to the angle. */
    double m_angle_offset_init;

    /** \brief Final offset applied to the angle. */
    double m_angle_offset_end;

    /** \brief Initial factor applied to the intensity of the red component. */
    double m_red_intensity_factor_init;

    /** \brief Initial factor applied to the intensity of the green
        component. */
    double m_green_intensity_factor_init;

    /** \brief Initial factor applied to the intensity of the blue component. */
    double m_blue_intensity_factor_init;

    /** \brief Final factor applied to the intensity of the red component. */
    double m_red_intensity_factor_end;

    /** \brief Final factor applied to the intensity of the green
        component. */
    double m_green_intensity_factor_end;

    /** \brief Final factor applied to the intensity of the blue component. */
    double m_blue_intensity_factor_end;

    /** \brief The item to modify. */
    handle_type m_item;

    /** \brief Tell if m_item must be killed with *this. */
    bool m_same_lifespan;

    /** \brief Tell if the aspect of m_item must be restored at the end of the
        effect. */
    bool m_restore_at_end;

  }; // class decorative_effect
} // namespace bear

#endif // __BEAR_DECORATIVE_EFFECT_HPP__

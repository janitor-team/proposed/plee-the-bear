/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file killer.hpp
 * \brief An item that kills other items.
 * \author Julien Jorge
 */
#ifndef __BEAR_KILLER_HPP__
#define __BEAR_KILLER_HPP__

#include "engine/base_item.hpp"
#include "engine/item_brick/with_toggle.hpp"
#include "universe/derived_item_handle.hpp"

#include "engine/export.hpp"
#include "generic_items/class_export.hpp"

namespace bear
{
  /**
   * \brief An item that kills other items.
   *
   * The valid fields for this item are
   *  - \a kill_activator: (bool) Tell if the activator must be killed too
   *    (default = false),
   *  - \a items: \c (list of items) The items to kill,
   *  - any field supported by the parent classes.
   *
   * \author Julien Jorge
   */
  class GENERIC_ITEMS_EXPORT killer:
    public engine::base_item,
    public engine::with_toggle
  {
    DECLARE_BASE_ITEM(killer);

  public:
    /** \brief The type of the parent class. */
    typedef engine::base_item super;

  private:
    /** \brief The type of the handles on the items to kill. */
    typedef universe::derived_item_handle<engine::base_item> handle_type;

  public:
    killer();

    bool set_item_list_field
    ( const std::string& name, const std::vector<base_item*>& value );
    bool set_bool_field( const std::string& name, bool value );

  private:
    void toggle_on( base_item* activator );

  private:
    /** \brief The list of items to kill. */
    std::vector<handle_type> m_items;

    /** \brief Tell if we kill the activator too. */
    bool m_kill_activator;

  }; // class killer
} // namespace bear

#endif // __BEAR_KILLER_HPP__

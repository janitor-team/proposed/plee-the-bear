/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file typed_method_caller.tpp
 * \brief Implementation of the bear::text_interface::typed_method_caller class.
 * \author Julien Jorge.
 */

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Execute a method of a given instance.
 * \param self The instance on which the method is called.
 * \param args The string representation of the value of the arguments passed to
 *        the method.
 * \param c The converter used to convert the arguments.
 */
template<typename SelfClass>
void bear::text_interface::typed_method_caller<SelfClass>::execute
( base_exportable* self, const std::vector<std::string>& args,
  const argument_converter& c )  const
{
  SelfClass* s = dynamic_cast<SelfClass*>(self);

  if ( s!=NULL )
    explicit_execute(*s, args, c);
  else
    claw::logger << claw::log_warning << "Failed to cast base_exportable."
                 << std::endl;
} // typed_method_caller::execute()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file argument_converter.tpp
 * \brief Implementation of the template methods of the
 *        bear::text_interface::argument_converter class.
 * \author Julien Jorge.
 */

#include "text_interface/string_to_arg.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a string representation of a value into a value of a given
 *        type.
 * \param arg The argument to convert.
 */
template<typename T>
typename
bear::text_interface::argument_converter::conversion_result<T>::result_type
bear::text_interface::argument_converter::convert_argument
( const std::string& arg ) const
{
  return string_to_arg<T>::convert_argument(*this, arg);
} // argument_converter::convert_argument()

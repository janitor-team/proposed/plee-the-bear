/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file converted_argument.hpp
 * \brief Convert a string to the equivalent value to pass as an argument for a
 *        character method.
 * \author Julien Jorge.
 */
#ifndef __TEXT_INTERFACE_CONVERTED_ARGUMENT_HPP__
#define __TEXT_INTERFACE_CONVERTED_ARGUMENT_HPP__

namespace bear
{
  namespace text_interface
  {
    /**
     * \brief Helper class to decide whether an argument is converted using the
     *        default procedure or delegated to the implementation of the
     *        subclass.
     */
    class converted_argument
    {
    public:
      converted_argument();
      converted_argument( const converted_argument& that );

      template<typename T>
      converted_argument( T* v );

      template<typename T>
      converted_argument( T& v );

      template<typename T>
      T cast_to() const;

    private:
      /** \brief The converted value. */
      void* m_holder;

    }; // class converted_argument()

  } // namespace text_interface
} // namespace bear

#include "text_interface/impl/converted_argument.tpp"

#endif // __TEXT_INTERFACE_CONVERTED_ARGUMENT_HPP__

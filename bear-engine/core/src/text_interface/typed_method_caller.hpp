/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file typed_method_caller.hpp
 * \brief Base class for calling a method of an instance given the string
 *        representation of the value of the arguments. Contrary to
 *        method_caller, this class cast the instance to a given type.
 * \author Julien Jorge.
 */
#ifndef __TEXT_INTERFACE_TYPED_METHOD_CALLER_HPP__
#define __TEXT_INTERFACE_TYPED_METHOD_CALLER_HPP__

#include <vector>
#include <string>

#include "text_interface/method_caller.hpp"

namespace bear
{
  namespace text_interface
  {
    /**
     * \brief Base class for calling a method of an instance given the string
     *        representation of the value of the arguments. Contrary to
     *        method_caller, this class cast the instance to a given type.
     *
     * \author Julien Jorge.
     */
    template<typename SelfClass>
    class typed_method_caller:
      public method_caller
    {
    public:
      /**
       * \brief Execute a method of a given instance.
       * \param self The instance on which the method is called.
       * \param args The string representation of the value of the arguments
       *        passed to the method.
       * \param c The converter used to convert the arguments.
       */
      virtual void explicit_execute
      ( SelfClass& self, const std::vector<std::string>& args,
        const argument_converter& c ) const = 0;

    private:
      void execute
      ( base_exportable* self, const std::vector<std::string>& args,
        const argument_converter& c ) const;

    }; // class typed_method_caller

  } // namespace text_interface
} // namespace bear

#include "text_interface/impl/typed_method_caller.tpp"

#endif // __TEXT_INTERFACE_TYPED_METHOD_CALLER_HPP__

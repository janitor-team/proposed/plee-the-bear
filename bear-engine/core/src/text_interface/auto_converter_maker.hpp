/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file auto_converter.hpp
 * \brief The base class for all classes for which we want to be able to call
 *        methods from a text_interface file.
 * \author Julien Jorge.
 */
#ifndef __TEXT_AUTO_CONVERTER_MAKER_HPP__
#define __TEXT_AUTO_CONVERTER_MAKER_HPP__

#include "text_interface/auto_converter.hpp"

namespace bear
{
  namespace text_interface
  {
    template<typename A0>
    auto_converter auto_converter_maker( A0 arg0 );

    template<typename A0, typename A1>
    auto_converter auto_converter_maker( A0 arg0, A1 arg1 );

    template<typename A0, typename A1, typename A2>
    auto_converter auto_converter_maker( A0 arg0, A1 arg1, A2 arg2 );

  } // namespace text_interface
} // namespace bear

#include "text_interface/impl/auto_converter_maker.tpp"

#endif // __TEXT_AUTO_CONVERTER_MAKER_HPP__

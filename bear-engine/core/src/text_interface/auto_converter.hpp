/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file auto_converter.hpp
 * \brief This class is an argument converter used to statically associate a
 *        name with each of the elements of a list of arguments.
 * \author Julien Jorge.
 */
#ifndef __TEXT_AUTO_CONVERTER_HPP__
#define __TEXT_AUTO_CONVERTER_HPP__

#include "text_interface/argument_converter.hpp"

#include <map>

namespace bear
{
  namespace text_interface
  {
    /**
     * \brief This class is an argument converter used to statically associate a
     *        name with each of the elements of a list of arguments.
     *
     * You will typicall use this converter when the name of the function to
     * call is unknown but its arguments are known.
     *
     * \author Julien Jorge
     */
    class auto_converter:
      public argument_converter
    {
    private:
      /** \brief The type of the map associating the arguments with their
          names. */
      typedef std::map<std::string, converted_argument> arg_map_type;

    public:
      std::vector<std::string> get_arguments() const;

      converted_argument do_convert_argument
      ( const std::string& arg, const std::type_info& type ) const;

      void push( converted_argument argv );

    private:
      /** \brief The map associating the arguments with their names. */
      arg_map_type m_args;

    }; // class auto_converter

  } // namespace text_interface
} // namespace bear

#endif // __TEXT_AUTO_CONVERTER_HPP__


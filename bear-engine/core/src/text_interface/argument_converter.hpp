/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file argument_converter.hpp
 * \brief A class that converts a string into the adequate value to be passed
 *        to a function of a base_exportable by the method_caller classes.
 * \author Julien Jorge.
 */
#ifndef __TEXT_INTERFACE_ARGUMENT_CONVERTER_HPP__
#define __TEXT_INTERFACE_ARGUMENT_CONVERTER_HPP__

#include <string>
#include <typeinfo>
#include <claw/exception.hpp>

#include "text_interface/converted_argument.hpp"

namespace bear
{
  namespace text_interface
  {
    template<typename T>
    struct string_to_arg;

    /**
     * \brief This exception is thrown when an argument could not be converted
     *        into the desired type.
     * \author Julien Jorge
     */
    class no_converter:
      public claw::exception
    {
    public:
      no_converter( const std::string& arg, const std::type_info& type )
        : claw::exception
          ( "Can't convert arg '" + arg + "' as '" + type.name() + "'" )
      { }
    }; // class no_converter

    /**
     * \brief A class that converts a string into the adequate value to be
     *        passed to a function of a base_exportable by the method_caller
     *        classes.
     * \author Julien Jorge.
     */
    class argument_converter
    {
    public:
      /** The type of the result of the conversion for a given input type. */
      template<typename T>
      struct conversion_result
      {
        typedef typename string_to_arg<T>::result_type result_type;
      }; // struct conversion_result

    public:
      virtual ~argument_converter();

      template<typename T>
      typename conversion_result<T>::result_type
      convert_argument( const std::string& arg ) const;

      virtual converted_argument do_convert_argument
      ( const std::string& arg, const std::type_info& type ) const;

    }; // class argument_converter

  } // namespace text_interface
} // namespace bear

#include "text_interface/impl/argument_converter.tpp"

#endif // __TEXT_INTERFACE_ARGUMENT_CONVERTER_HPP__

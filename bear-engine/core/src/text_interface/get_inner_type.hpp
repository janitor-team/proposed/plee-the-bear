/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file get_inner_type.hpp
 * \brief Get the most inner type in a declaration of a constant and/or a
 *        reference.
 * \author Julien Jorge.
 */
#ifndef __TEXT_INTERFACE_GET_INNER_TYPE_HPP__
#define __TEXT_INTERFACE_GET_INNER_TYPE_HPP__

namespace bear
{
  namespace text_interface
  {
    /**
     * \brief Get the most inner type in a declaration of a constant and/or a
     *        reference.
     * \author Julien Jorge.
     */
    template<typename T>
    class get_inner_type
    {
    public:
      /** \brief The inner type. */
      typedef T type;
    }; // class get_inner_type

    /**
     * \brief Get the most inner type in a declaration of a constant.
     * \author Julien Jorge.
     */
    template<typename T>
    class get_inner_type<const T>:
      public get_inner_type<T>
    { }; // class get_inner_type

    /**
     * \brief Get the most inner type in a declaration of a constant reference.
     * \author Julien Jorge.
     */
    template<typename T>
    class get_inner_type<const T&>:
      public get_inner_type<T>
    { }; // class get_inner_type

  } // namespace text_interface
} // namespace bear

#endif // __TEXT_INTERFACE_GET_INNER_TYPE_HPP__

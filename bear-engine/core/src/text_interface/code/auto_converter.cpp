/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file auto_converter.cpp
 * \brief Implementation of the bear::text_interface::auto_converter class.
 * \author Julien Jorge.
 */
#include "text_interface/auto_converter.hpp"

#include <algorithm>
#include <claw/functional.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the names of the arguments.
 */
std::vector<std::string>
bear::text_interface::auto_converter::get_arguments() const
{
  std::vector<std::string> result( m_args.size() );
  std::transform
    ( m_args.begin(), m_args.end(), result.begin(),
      claw::const_pair_first<arg_map_type::value_type>() );
  return result;
} // auto_converter::get_arguments()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert an argument into its value.
 * \param arg The string representation of the argument.
 * \param type The expected result type.
 */
bear::text_interface::converted_argument
bear::text_interface::auto_converter::do_convert_argument
( const std::string& arg, const std::type_info& type ) const
{
  arg_map_type::const_iterator it = m_args.find(arg);
  if ( it != m_args.end() )
    return it->second;
  else
    throw std::invalid_argument("Can't convert '" + arg + "'");;
} // auto_converter::do_convert_argument()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add an argument value at the end of the argument list.
 * \param arg The value of the argument.
 */
void bear::text_interface::auto_converter::push( converted_argument argv )
{
  std::ostringstream oss;
  oss << "arg" << m_args.size();
  m_args[oss.str()] = argv;
} // auto_converter::push()

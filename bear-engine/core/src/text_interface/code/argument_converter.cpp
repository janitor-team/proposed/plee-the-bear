/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file argument_converter.cpp
 * \brief Implementation of the bear::text_interface::argument_converter class.
 * \author Julien Jorge.
 */
#include "text_interface/argument_converter.hpp"

#include <typeinfo>

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::text_interface::argument_converter::~argument_converter()
{
  // nothing to do
} // argument_converter::~argument_converter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Delegate conversion of an argument, if no adequate conversion has
 *        been found.
 * \param arg The argument to convert.
 * \param type The description of the expected type for the result value.
 */
bear::text_interface::converted_argument
bear::text_interface::argument_converter::do_convert_argument
( const std::string& arg, const std::type_info& type ) const
{
  throw no_converter(arg, type);
} // argument_converter::do_convert_argument()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file string_to_arg.cpp
 * \brief Implementation of the bear::text_interface::string_to_arg class.
 * \author Julien Jorge.
 */
#include "text_interface/string_to_arg.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a string to the equivalent value to pass as an argument for a
 *        character method.
 * \param c The context on which the conversion is done.
 * \param arg The string representation of the value.
 */
std::string bear::text_interface::string_to_arg<std::string>::convert_argument
( const argument_converter& c, const std::string& arg )
{
  return arg;
} // string_to_arg::convert_argument()

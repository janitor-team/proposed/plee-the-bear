/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file converted_argument.cpp
 * \brief Implementation of the bear::text_interface::converted_argument class.
 * \author Julien Jorge.
 */
#include "text_interface/converted_argument.hpp"

#include <cstdlib>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::text_interface::converted_argument::converted_argument()
  : m_holder(NULL)
{

} // converted_argument::converted_argument()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param that The instance to copy from.
 */
bear::text_interface::converted_argument::converted_argument
( const converted_argument& that )
  : m_holder(that.m_holder)
{

} // converted_argument::converted_argument()

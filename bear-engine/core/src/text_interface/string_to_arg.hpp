/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file string_to_arg.hpp
 * \brief Convert a string to the equivalent value to pass as an argument for a
 *        character method.
 * \author Julien Jorge.
 */
#ifndef __TEXT_INTERFACE_STRING_TO_ARG_HPP__
#define __TEXT_INTERFACE_STRING_TO_ARG_HPP__

#include "text_interface/get_inner_type.hpp"

#include <string>
#include <vector>
#include <list>

#include <claw/meta/type_list.hpp>

namespace bear
{
  namespace text_interface
  {
    class argument_converter;

    /**
     * \brief Helper class to decide whether an argument is converted using the
     *        default procedure or delegated to the implementation of the
     *        subclass.
     */
    template<typename T, bool DefaultConversion>
    struct string_to_arg_helper;

    // Do the default conversion of the argument
    template<typename T>
    struct string_to_arg_helper<T, true>
    {
      /** The type of the result value obtained with this converter. */
      typedef typename get_inner_type<T>::type result_type;

      static result_type convert_argument
      ( const argument_converter& c, const std::string& arg );
    }; // struct string_to_arg_helper [true]

    // Let the sub class convert the argument.
    template<typename T>
    struct string_to_arg_helper<T, false>
    {
      /** The type of the result value obtained with this converter. */
      typedef T result_type;

      static T convert_argument
      ( const argument_converter& c, const std::string& arg );
    }; // struct string_to_arg_helper [false]

    // Let the sub class convert the argument.
    template<typename T>
    struct string_to_arg_helper<T&, false>
    {
      /** The type of the result value obtained with this converter. */
      typedef T& result_type;

      static result_type convert_argument
      ( const argument_converter& c, const std::string& arg );
    }; // struct string_to_arg_helper [false]

    // Let the sub class convert the argument.
    template<typename T>
    struct string_to_arg_helper<const T&, false>
    {
      /** The type of the result value obtained with this converter. */
      typedef const T& result_type;

      static result_type convert_argument
      ( const argument_converter& c, const std::string& arg );
    }; // struct string_to_arg_helper [false]

    /**
     * \brief Convert a string to the equivalent value to pass as an argument
     *        for a character method.
     * \author Julien Jorge.
     */
    template<typename T>
    struct string_to_arg:
      public string_to_arg_helper
      <
        T,
        claw::meta::type_list_contains
        <
          T,
          claw::meta::cpp_type_list
        >::result
      >
    { }; // struct string_to_arg

    /**
     * \brief Convert a string to the equivalent value to pass as an argument
     *        for a character method.
     * \author Julien Jorge.
     */
    template<typename T>
    struct string_to_arg<const T&>:
      public string_to_arg_helper
      <
        const T&,
        claw::meta::type_list_contains
        <
          T,
          claw::meta::cpp_type_list
        >::result
      >
    { }; // struct string_to_arg

    /**
     * \brief Specialisation for std::string. There is no conversion to do.
     * \author Sébastien Angibaud
     */
    template<>
    struct string_to_arg<std::string>
    {
      /** The type of the result value obtained with this converter. */
      typedef std::string result_type;

      static std::string convert_argument
      ( const argument_converter& c, const std::string& arg );
    }; // struct string_to_arg [std::string]

    /**
     * \brief Specialisation for std::string&. There is no conversion to do.
     * \author Sébastien Angibaud
     */
    template<>
    struct string_to_arg<std::string&>:
      public string_to_arg<std::string>
    { }; // struct string_to_arg [std::string&]

    /**
     * \brief Specialisation for const std::string&. There is no conversion to
     *        do.
     * \author Sébastien Angibaud
     */
    template<>
    struct string_to_arg<const std::string&>:
      public string_to_arg<std::string>
    { }; // struct string_to_arg [const std::string&]

    /**
     * \brief Convert a string to the equivalent sequence of value to pass as an
     *        argument for a character method.
     * \author Julien Jorge
     */
    template<typename Sequence>
    struct string_to_sequence_arg
    {
    public:
      typedef Sequence result_type;

      static result_type convert_argument
      ( const argument_converter& c, const std::string& arg );
    }; // struct string_to_sequence_arg

    /**
     * \brief Specialisation for std::vector.
     * \author Julien Jorge
     */
    template<typename T>
    struct string_to_arg< const std::vector<T>& >:
      public string_to_sequence_arg< std::vector<T> >
    { };

    /**
     * \brief Specialisation for std::list.
     * \author Julien Jorge
     */
    template<typename T>
    struct string_to_arg< const std::list<T>& >:
      public string_to_sequence_arg< std::list<T> >
    { };

  } // namespace text_interface
} // namespace bear

#include "text_interface/impl/string_to_arg.tpp"

#endif // __TEXT_INTERFACE_STRING_TO_ARG_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sprite_loader.hpp
 * \brief A class to load sprites and animations
 * \author Julien Jorge
 */
#ifndef __ENGINE_SPRITE_LOADER_HPP__
#define __ENGINE_SPRITE_LOADER_HPP__

#include "visual/animation.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class compiled_file;
    class level_globals;

    class ENGINE_EXPORT sprite_loader
    {
    public:
      static visual::sprite
      load_sprite( compiled_file& f, level_globals& glob );

      static visual::animation
      load_animation( compiled_file& f, level_globals& glob );
      static visual::animation
      load_any_animation( compiled_file& f, level_globals& glob );
      static visual::animation
      load_animation_data( compiled_file& f, level_globals& glob );

      static void
      load_bitmap_rendering_attributes
      ( compiled_file& f, visual::bitmap_rendering_attributes& result );

    private:
      static visual::animation
      load_animation_v0_5( compiled_file& f, level_globals& glob );
    }; // class sprite_loader

  } // namespace engine
} // namespace bear

#endif // __ENGINE_SPRITE_LOADER_HPP__

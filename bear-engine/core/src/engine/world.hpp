/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file engine/world.hpp
 * \brief Description of the world.
 * \author Julien Jorge
 */
#ifndef __ENGINE_WORLD_HPP__
#define __ENGINE_WORLD_HPP__

#include "universe/world.hpp"
#include "engine/class_export.hpp"
#include "engine/population.hpp"
#include "engine/scene_visual.hpp"
#include "communication/messageable.hpp"
#include "communication/typed_message.hpp"

#include <list>

namespace bear
{
  namespace engine
  {
    /**
     * \brief Description of the world.
     *
     * The world contains the walls and all the entities.
     */
    class ENGINE_EXPORT world :
      public universe::world,
      public communication::messageable
    {
    public:
      /**
       * \brief This message pick items overlapping a given position.
       */
      class ENGINE_EXPORT msg_pick_items:
        public communication::typed_message<world>
      {
      public:
        msg_pick_items( const universe::position_type& pos );

        bool apply_to( world& that );

      public:
        /** \brief The item found at this position. */
        world::item_list items;

      private:
        /** \brief The position where the item are searched. */
        const universe::position_type m_pos;

      }; // class msg_pick_items

      /**
       * \brief This message pick items in given region.
       */
      class ENGINE_EXPORT msg_pick_items_in_region:
        public communication::typed_message<world>
      {
      public:
        msg_pick_items_in_region( const universe::rectangle_type& r );

        bool apply_to( world& that );

      public:
        /** \brief The items found in this region. */
        world::item_list items;

      private:
        /** \brief The region where the item are searched. */
        const universe::rectangle_type m_region;

      }; // class msg_pick_items_in_region

      /** \brief The type of the iterator on the living items. */
      typedef population::const_iterator const_item_iterator;

    public:
      world( const universe::size_box_type& size );
      ~world();

      void start();

      void progress_entities
      ( const region_type& regions, universe::time_type elapsed_time );

      void get_visual
      ( std::list<scene_visual>& visuals,
        const universe::rectangle_type& camera_box ) const;

      const_item_iterator living_items_begin() const;
      const_item_iterator living_items_end() const;

      void add_static( base_item* const& who );

      void register_item( base_item* const& who );
      void release_item( base_item* const& who );
      void drop_item( base_item* const& who );

      void print_item_stats() const;

    private:
      /** \brief The static items won't be deleted when cleaning the
          population. */
      std::list<base_item*> m_static_items;

      /** \brief All items that can die a any time. */
      population m_population;

    }; // class world
  } // namespace engine
} // namespace bear

#endif // __ENGINE_WORLD_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file libraries_pool
 * \brief A class containing a set of dynamic libraries from which we can get
 *        functions.
 * \author Julien Jorge
 */
#ifndef __ENGINE_LIBRARIES_POOL_HPP__
#define __ENGINE_LIBRARIES_POOL_HPP__

#include <string>
#include <list>
#include <claw/dynamic_library.hpp>

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief The libraries_pool is a class containing a set of dynamic
     *        libraries from which we can get functions.
     *
     * \author Julien Jorge
     */
    class libraries_pool
    {
    private:
      /** \brief The type of the list of libraries. */
      typedef std::list<claw::dynamic_library*> libraries_list;

    public:
      ENGINE_EXPORT ~libraries_pool();

      ENGINE_EXPORT
      void add_library( const std::string& name, bool current_program = false );
      ENGINE_EXPORT bool have_symbol( const std::string& symbol ) const;

      template<typename T>
      T get_symbol( const std::string& name ) const;

    private:
      /** \brief The libraries stored in this pool. */
      libraries_list m_libraries;

    }; // class libraries_pool
  } // namespace engine
} // namespace bear

#include "engine/impl/libraries_pool.tpp"

#endif // __ENGINE_LIBRARIES_POOL_HPP__

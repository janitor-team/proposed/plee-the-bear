/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file variable_eraser.cpp
 * \brief Implementation of the bear::engine::variable_eraser class.
 * \author Julien Jorge
 */
#include "engine/variable/variable_eraser.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param m The map from which the variable is erased.
 * \param pattern A regular expression that must be verified by the variable's
 *        name to be erased.
 */
bear::engine::variable_eraser::variable_eraser
( var_map& m, boost::regex pattern )
  : m_map(m), m_pattern(pattern)
{

} // variable_eraser::variable_eraser()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file variable_copy.cpp
 * \brief Implementation of the bear::engine::variable_copy class.
 * \author Sebastien Angibaud
 */
#include "engine/variable/variable_copy.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param os The stream in which the result will be written.
 * \param pattern A regular expression that must be verified by the variable's
 *        name to be serialised.
 */
bear::engine::variable_copy::variable_copy
( var_map& vars, const boost::regex& pattern )
  : m_vars(vars), m_pattern(pattern)
{

} // variable_copy::variable_copy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Escape value to avoid ambiguity.
 * \param value The value to escape.
 */
std::string
bear::engine::variable_copy::escape( const std::string& value ) const
{
  std::string result;
  result.reserve(value.size());

  for (std::size_t i=0; i!=value.length(); ++i)
    switch( value[i] )
      {
      case '"':
      case '\\':
        result += '\\'; /* no break */
      default:
        result += value[i];
      }

  return result;
} // variable_copy::escape()

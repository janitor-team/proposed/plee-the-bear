/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_variable.cpp
 * \brief Implementation of the bear::engine::base_variable class.
 * \author Julien Jorge
 */
#include "engine/variable/base_variable.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param name The name of the variable.
 */
bear::engine::base_variable::base_variable( const std::string& name )
  : m_name(name)
{

} // base_variable::base_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the variable.
 */
const std::string& bear::engine::base_variable::get_name() const
{
  return m_name;
} // base_variable::get_name()

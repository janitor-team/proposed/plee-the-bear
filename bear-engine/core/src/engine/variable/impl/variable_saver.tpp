/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file variable_saver.tpp
 * \brief Implementation of the template methods of the
 *        bear::engine::variable_saver class.
 * \author Julien Jorge
 */

#include "engine/variable/type_to_string.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Save a variable's value.
 * \param name The name of the variable.
 * \param value The value of the variable.
 */
template<typename T>
void bear::engine::variable_saver::operator()
( const std::string& name, const T& value ) const
{
  if ( boost::regex_match(name, m_pattern) )
    m_output << type_to_string<T>::value << " \"" << escape(name) << "\" = \""
             << escape(value) << "\";" << std::endl;
} // variable_saver::operator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Escape value to avoid ambiguity.
 * \param value The value to escape.
 * \remark There is nothing to do by default.
 */
template<typename T>
const T& bear::engine::variable_saver::escape( const T& value ) const
{
  return value;
} // variable_saver::escape()

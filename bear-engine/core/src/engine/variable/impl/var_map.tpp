/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file var_map.tpp
 * \brief Implementation of the template methods of bear::engine::var_map.
 * \author Julien Jorge
 */

#include <claw/multi_type_map_visitor.hpp>
#include <claw/meta/type_list.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a signal to connect to be informed when the value of a given
 *        variable change.
 * \param name The name of the variable to listen.
 */
template<typename T>
boost::signal<void (T)>&
bear::engine::var_map::variable_changed( const std::string& name )
{
  typedef boost::signal<void (T)> signal_type;

  if ( !m_signals.exists<signal_type*>(name) )
    m_signals.set<signal_type*>( name, new signal_type() );

  return *m_signals.get<signal_type*>(name);
} // var_map::variable_changed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of a given variable.
 * \param k The name of the variable.
 * \param v The new value of the variable.
 */
template<typename T>
void bear::engine::var_map::set( const std::string& k, const T& v )
{
  bool signal = true;

  if ( exists<T>(k) )
    {
      T old_value = get<T>(k);
      super::set<T>(k, v);

      signal = ( old_value != v );
    }
  else
    super::set<T>(k, v);

  typedef boost::signal<void (T)> signal_type;

  if ( signal && m_signals.exists<signal_type*>(k) )
    (*m_signals.get<signal_type*>(k))(v);
} // var_map::set()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply a function on each entry.
 * \param f The template function to call. The first argument must be compatible
 *        with std::string and the second one must be a parameter of the
 *        template.
 */
template<typename Function>
void bear::engine::var_map::for_each( Function f )
{
  claw::multi_type_map_visitor visitor;
  visitor.run( *(super*)this, f );
} // var_map::for_each()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file variable_list_reader.hpp
 * \brief A function object that reads a list of variable assignments as saved
 *        with a variable_saver.
 * \author Julien Jorge
 */
#ifndef __ENGINE_VARIABLE_LIST_READER_HPP__
#define __ENGINE_VARIABLE_LIST_READER_HPP__

#include "engine/variable/var_map.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A function object that reads a list of variable assignments as
     *        saved with a variable_saver.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT variable_list_reader
    {
    public:
      void operator()( std::istream& iss, var_map& output ) const;

    private:
      void apply
      ( var_map& v, const std::string& type, const std::string& name,
        const std::string& value ) const;
      std::string unescape( const std::string& s ) const;

      void add_string_variable
      ( var_map& v, const std::string& name, const std::string& value ) const;

      template<typename T>
      void add_variable
      ( var_map& v, const std::string& name, const std::string& value ) const;

    }; // class variable_list_reader

  } // namespace engine
} // namespace bear

#endif // __ENGINE_VARIABLE_LIST_READER_HPP__

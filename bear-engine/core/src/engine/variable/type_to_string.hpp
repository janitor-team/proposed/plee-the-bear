/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file type_to_string.hpp
 * \brief Converts a type into its string representation as used for the
 *        serialisation of variables.
 * \author Julien Jorge
 */
#ifndef __ENGINE_TYPE_TO_STRING_HPP__
#define __ENGINE_TYPE_TO_STRING_HPP__

#include <string>

namespace bear
{
  namespace engine
  {
    /**
     * \brief Converts a type into its string representation as used for the
     *        serialisation of variables.
     * \author Julien Jorge
     */
    template<typename T>
    class type_to_string;

    /**
     * \brief Specialization for type int.
     * \author Julien Jorge
     */
    template<>
    class type_to_string<int>
    {
    public:
      /** \brief The string representation of the type. */
      static const char* const value;

    }; // class type_to_string [int]

    /**
     * \brief Specialization for type unsigned int.
     * \author Julien Jorge
     */
    template<>
    class type_to_string<unsigned int>
    {
    public:
      /** \brief The string representation of the type. */
      static const char* const value;

    }; // class type_to_string [unsigned int]

    /**
     * \brief Specialization for type bool.
     * \author Julien Jorge
     */
    template<>
    class type_to_string<bool>
    {
    public:
      /** \brief The string representation of the type. */
      static const char* const value;

    }; // class type_to_string [bool]

    /**
     * \brief Specialization for type std::string.
     * \author Julien Jorge
     */
    template<>
    class type_to_string<std::string>
    {
    public:
      /** \brief The string representation of the type. */
      static const char* const value;

    }; // class type_to_string []

    /**
     * \brief Specialization for type double.
     * \author Julien Jorge
     */
    template<>
    class type_to_string<double>
    {
    public:
      /** \brief The string representation of the type. */
      static const char* const value;

    }; // class type_to_string [double]

  } // namespace engine
} // namespace bear

#endif // __ENGINE_TYPE_TO_STRING_HPP__

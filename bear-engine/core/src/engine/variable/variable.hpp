/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file variable.hpp
 * \brief This class represent a level variable or a game variable.
 * \author Julien Jorge
 */
#ifndef __ENGINE_VARIABLE_HPP__
#define __ENGINE_VARIABLE_HPP__

#include "engine/variable/base_variable.hpp"
#include "engine/variable/var_map.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A variable of a known type.
     * \author Julien Jorge
     */
    template<typename T>
    class variable:
      public base_variable
    {
    public:
      explicit variable( const std::string& name );
      variable( const std::string& name, const T& val );

      const T& get_value() const;

      virtual void assign_value_to( var_map& m ) const;
      virtual void get_value_from( const var_map& m );
      virtual bool exists( const var_map& m ) const;

    private:
      /** \brief The value of the variable. */
      T m_value;

    }; // class variable

  } // namespace engine
} // namespace bear

#include "engine/variable/impl/variable.tpp"

#endif // __ENGINE_VARIABLE_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file variable_eraser.hpp
 * \brief A function object that erases the variables whose name match a given
 *        pattern.
 * \author Julien Jorge
 */
#ifndef __ENGINE_VARIABLE_ERASER_HPP__
#define __ENGINE_VARIABLE_ERASER_HPP__

#include "engine/variable/var_map.hpp"

#include <boost/regex.hpp>

namespace bear
{
  namespace engine
  {
    /**
     * \brief A function object that erases the variables whose name match a
     *        given pattern.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT variable_eraser
    {
    public:
      explicit variable_eraser( var_map& m, boost::regex pattern );

      template<typename T>
      void operator()( const std::string& name, const T& value ) const;

    private:
      /** \brief The map from which the variables are erased. */
      var_map& m_map;

      /** \brief A regular expression that must be verified by the variable's
          name to be serialised. */
      const boost::regex m_pattern;

    }; // class variable_eraser

  } // namespace engine
} // namespace bear

#include "engine/variable/impl/variable_eraser.tpp"

#endif // __ENGINE_VARIABLE_ERASER_HPP__

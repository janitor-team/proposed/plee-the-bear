/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file variable_copy.hpp
 * \brief A function object that copies a variable in a var map.
 * \author Sebastien Angibaud
 */
#ifndef __ENGINE_VARIABLE_COPY_HPP__
#define __ENGINE_VARIABLE_COPY_HPP__

#include <boost/regex.hpp>
#include "engine/variable/var_map.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A function object that copies a variable in a var map.
     * \author Sebastien Angibaud
     */
    class ENGINE_EXPORT variable_copy
    {
    public:
      explicit variable_copy( var_map& vars, const boost::regex& pattern );

      template<typename T>
      void operator()( const std::string& name, const T& value ) const;

    private:
      template<typename T>
      const T& escape( const T& value ) const;

      std::string escape( const std::string& value ) const;

    private:
      /** \brief The var map in which we copy varaiable. */
      var_map& m_vars;

      /** \brief A regular expression that must be verified by the variable's
          name to be serialised. */
      const boost::regex& m_pattern;

    }; // class variable_copy

  } // namespace engine
} // namespace bear

#include "engine/variable/impl/variable_copy.tpp"

#endif // __ENGINE_VARIABLE_COPY_HPP__

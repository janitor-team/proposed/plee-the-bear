/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file resource_pool.hpp
 * \brief The resource pool allows to access resource files regardless if they
 *        are in a resource archive or in a folder.
 * \author Julien Jorge
 */
#ifndef __ENGINE_RESOURCE_POOL_HPP__
#define __ENGINE_RESOURCE_POOL_HPP__

#include <list>
#include <string>
#include <iostream>
#include <claw/basic_singleton.hpp>

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief The resource pool stores the resource files.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT resource_pool :
      public claw::pattern::basic_singleton<resource_pool>
    {
    private:
      /** \brief The type of the parent class. */
      typedef claw::pattern::basic_singleton<resource_pool> super;

    public:
      // Must be redefined to work correctly with dynamic libraries.
      // At least under Windows with MinGW.
      static resource_pool& get_instance();

      void add_path( const std::string& path );

      void get_file( const std::string& name, std::ostream& os );
      bool exists( const std::string& name ) const;

    private:
      bool find_file( const std::string& name, std::ifstream& f ) const;
      bool find_file_name_straight( std::string& name ) const;

    private:
      /** \brief Paths for resources. */
      std::list<std::string> m_path;

    }; // class resource_pool
  } // namespace engine
} // namespace bear

#endif // __ENGINE_RESOURCE_POOL_HPP__

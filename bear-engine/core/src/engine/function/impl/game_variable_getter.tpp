/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file game_variable_getter.tpp
 * \brief Implementation of the bear::engine::game_variable_getter class.
 * \author Julien Jorge
 */

#include "engine/game.hpp"
#include "engine/variable/variable.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
template<typename T>
bear::engine::game_variable_getter<T>::game_variable_getter()
{

} // game_variable_getter::game_variable_getter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 */
template<typename T>
bear::engine::game_variable_getter<T>::game_variable_getter
(const game_variable_getter<T>& that)
  : m_name(that.m_name), m_default_value(that.m_default_value)
{
} // game_variable_getter::game_variable_getter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param var_name The name of the game variable.
 * \param default_value The default value returned if the variable is not set.
 */
template<typename T>
bear::engine::game_variable_getter<T>::game_variable_getter
( const std::string& var_name, const T& default_value )
  : m_name(var_name), m_default_value(default_value)
{

} // game_variable_getter::game_variable_getter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the name of the variable.
 * \param n The name of the variable.
 */
template<typename T>
void bear::engine::game_variable_getter<T>::set_name( const std::string& n )
{
  m_name = n;
} // game_variable_getter::set_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the variable.
 */
template<typename T>
const std::string& bear::engine::game_variable_getter<T>::get_name() const
{
  return m_name;
} // game_variable_getter::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the default value, returned if the variable is not set.
 * \param v The default value.
 */
template<typename T>
void bear::engine::game_variable_getter<T>::set_default_value( const T& v )
{
  m_default_value = v;
} // game_variable_getter::set_default_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the value of the game variable.
 */
template<typename T>
T bear::engine::game_variable_getter<T>::operator()() const
{
  variable<T> v(m_name, m_default_value);

  if ( game::get_instance().game_variable_exists(v) )
    game::get_instance().get_game_variable(v);

  return v.get_value();
} // game_variable_getter::operator()()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file linear_game_variable_getter.cpp
 * \brief Implementation of the bear::linear_game_variable_getter class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
template<typename T>
bear::engine::linear_game_variable_getter<T>::linear_game_variable_getter()
{
  this->set_default_value(0);
} // linear_game_variable_getter::linear_game_variable_getter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
template<typename T>
bear::engine::linear_game_variable_getter<T>*
bear::engine::linear_game_variable_getter<T>::clone() const
{
  return new linear_game_variable_getter(*this);
} // linear_game_variable_getter::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
template<typename T>
double bear::engine::linear_game_variable_getter<T>::evaluate() const
{
  return (double)(*this)();
} // linear_game_variable_getter::evaluate()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file bool_game_variable_getter.hpp
 * \brief A boolean expression that returns the value of a game variable.
 * \author Sébastien Angibaud
 */
#ifndef __BEAR_BOOL_GAME_VARIABLE_GETTER_HPP__
#define __BEAR_BOOL_GAME_VARIABLE_GETTER_HPP__

#include "engine/function/game_variable_getter.hpp"
#include "expr/boolean_function.hpp"
#include "expr/base_boolean_expression.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A boolean expression that returns the value of a game variable.
     *
     * The valid fields for this item are
     *  - name The name of the variable to get,
     *  - default_value The default value of the variable, if not set,
     *  - any field supported by the parent classes.
     *
     * \author Julien Jorge
     */
    class ENGINE_EXPORT bool_game_variable_getter:
      public expr::base_boolean_expression,
      public game_variable_getter<bool>
    {
    public:
      /** \brief The type of the parent class. */
      typedef expr::base_boolean_expression super;

    public:
      bool_game_variable_getter();

      bool_game_variable_getter* clone() const;
      bool evaluate() const;

    }; // class bool_game_variable_getter
  } // namespace engine
} // namespace bear

#endif // __BEAR_BOOL_GAME_VARIABLE_GETTER_HPP__

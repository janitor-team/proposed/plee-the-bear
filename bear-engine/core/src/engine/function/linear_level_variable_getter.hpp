/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file linear_level_variable_getter.hpp
 * \brief A linear expression that returns the value of a level variable.
 * \author Julien Jorge
 */
#ifndef __BEAR_LINEAR_LEVEL_VARIABLE_GETTER_HPP__
#define __BEAR_LINEAR_LEVEL_VARIABLE_GETTER_HPP__

#include "engine/function/level_variable_getter.hpp"
#include "expr/linear_function.hpp"
#include "expr/base_linear_expression.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A linear expression that returns the value of a level variable.
     *
     * The valid fields for this item are
     *  - name The name of the variable to get,
     *  - default_value The default value of the variable, if not set,
     *  - any field supported by the parent classes.
     *
     * \author Julien Jorge
     */
    template<typename T>
    class linear_level_variable_getter:
      public expr::base_linear_expression,
      public level_variable_getter<T>
    {
    public:
      /** \brief The type of the parent class. */
      typedef expr::base_linear_expression super;

    public:
      linear_level_variable_getter();
      linear_level_variable_getter
      (const level* lvl, const std::string& var_name);

      void build();

      linear_level_variable_getter<T>* clone() const;
      double evaluate() const;

    }; // class linear_level_variable_getter
  } // namespace engine
} // namespace bear

#include "engine/function/impl/linear_level_variable_getter.tpp"

#endif // __BEAR_LINEAR_LEVEL_VARIABLE_GETTER_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file bool_level_variable_getter.cpp
 * \brief Implementation of the bear::bool_level_variable_getter class.
 * \author Sébastien Angibaud
 */

#include "engine/function/bool_level_variable_getter.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::engine::bool_level_variable_getter::bool_level_variable_getter()
{
  set_default_value(false);
} // bool_level_variable_getter::bool_level_variable_getter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::engine::bool_level_variable_getter::bool_level_variable_getter
( const level* lvl, const std::string& var_name )
{
  set_level(lvl);
  set_name(var_name);
  set_default_value(false);
} // bool_level_variable_getter::bool_level_variable_getter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::engine::bool_level_variable_getter*
bear::engine::bool_level_variable_getter::clone() const
{
  return new bool_level_variable_getter(*this);
} // bool_level_variable_getter::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::engine::bool_level_variable_getter::evaluate() const
{
  return (*this)();
} // bool_level_variable_getter::evaluate()

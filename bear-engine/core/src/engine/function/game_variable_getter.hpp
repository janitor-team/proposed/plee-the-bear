/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file game_variable_getter.hpp
 * \brief A function object to get the value of a game variable.
 * \author Julien Jorge
 */
#ifndef __BEAR_ENGINE_GAME_VARIABLE_GETTER_HPP__
#define __BEAR_ENGINE_GAME_VARIABLE_GETTER_HPP__

#include <string>

namespace bear
{
  namespace engine
  {
    /**
     * \brief A function object to get the value of a game variable.
     * \author Julien Jorge
     */
    template<typename T>
    class game_variable_getter
    {
    public:
      game_variable_getter();
      game_variable_getter(const game_variable_getter<T>& that);
      explicit game_variable_getter
      ( const std::string& var_name, const T& default_value = T() );

      void set_name( const std::string& n );
      const std::string& get_name() const;

      void set_default_value( const T& v );

      T operator()() const;

    private:
      /** \brief The name of the variable. */
      std::string m_name;

      /** \brief The default value. */
      T m_default_value;

    }; // class game_variable_getter
  } // namespace engine
} // namespace bear

#include "engine/function/impl/game_variable_getter.tpp"

#endif // __BEAR_ENGINE_GAME_VARIABLE_GETTER_HPP__

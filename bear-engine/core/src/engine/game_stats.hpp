/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_stats.hpp
 * \brief A class that stores some statistics about the game.
 * \author Julien Jorge
 */
#ifndef __ENGINE_GAME_STATS_HPP__
#define __ENGINE_GAME_STATS_HPP__

#include "engine/class_export.hpp"
#include "time/time.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A class that stores some statistics about the game.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT game_stats
    {
    public:
      game_stats();

      void set_destination( const std::string& destination );
      void set_user_id( const std::string& id );

      void send() const;

    private:
      void http_post( const std::string& address ) const;

      std::string generate_xml_stats() const;

    private:
      /** \brief The date at which the game was started. */
      const systime::milliseconds_type m_init_date;

      /** \brief The identifier of the current user. */
      std::string m_user_id;

      /** \brief The destination to which the stats are sent. */
      std::string m_destination;

    }; // class game_stats
  } // namespace engine
} // namespace bear

#endif // __ENGINE_GAME_STATS_HPP__

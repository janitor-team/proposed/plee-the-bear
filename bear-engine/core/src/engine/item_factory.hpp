/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_factory.hpp
 * \brief A factory to instanciate classes inheriting from base_item.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_FACTORY_HPP__
#define __ENGINE_ITEM_FACTORY_HPP__

#include "engine/class_export.hpp"

#include <claw/factory.hpp>
#include <string>

namespace bear
{
  namespace engine
  {
    class base_item;

    /**
     * \brief A factory to instanciate classes inheriting from base_item.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT item_factory
    {
    public:
      /** \brief The type of the factory. */
      typedef claw::pattern::factory<base_item, std::string> factory_type;

    public:
      static factory_type& get_instance();

    private:
      /** \brief A factory that creates items. */
      static factory_type s_factory_instance;

    }; // class item_factory

  } // namespace engine
} // namespace bear

#endif // __ENGINE_ITEM_FACTORY_HPP__

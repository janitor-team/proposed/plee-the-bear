/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file export_function.hpp
 * \brief Macros and function useful for class exportation.
 * \author Julien Jorge
 */
#ifndef __ENGINE_EXPORT_FUNCTION_HPP__
#define __ENGINE_EXPORT_FUNCTION_HPP__

#include <string>

namespace bear
{
  namespace engine
  {
    class base_item;
    class layer;

    /**
     * \brief Get the name of the exported function made for a class.
     * \param class_name The name of the class that has been exported.
     */
    inline std::string export_function_name( const std::string& class_name )
    {
      // prefix must be the string used for prefix in BASE_ITEM_EXPORT
      return "create_" + class_name;
    } // export_function_name()

    typedef base_item* (*base_export_function_type)();

    /**
     * \brief Get the name of the exported function made for a class.
     * \param class_name The name of the class that has been exported.
     */
    inline std::string
    layer_export_function_name( const std::string& class_name )
    {
      // prefix must be the string used for prefix in LAYER_EXPORT
      return "create_layer_" + class_name;
    } // layer_export_function_name()

    typedef
    layer* (*layer_export_function_type)(const universe::size_box_type& s);

  } // namespace engine
} // namespace bear

#endif // __ENGINE_EXPORT_FUNCTION_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_loader.hpp
 * \brief A class that loads a level file.
 * \author Julien Jorge
 */
#ifndef __ENGINE_LEVEL_LOADER_HPP__
#define __ENGINE_LEVEL_LOADER_HPP__

#include "audio/sample.hpp"
#include "universe/types.hpp"

#include "engine/class_export.hpp"
#include <vector>

namespace bear
{
  namespace engine
  {
    class base_item;
    class compiled_file;
    class layer;
    class level;

    /**
     * \brief This class loads a level from a compiled level file.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT level_loader
    {
    public:
      explicit level_loader( compiled_file& f, const std::string& path );
      ~level_loader();

      unsigned int get_item_index() const;
      unsigned int get_items_count() const;

      level* drop_level();

      void complete_run();
      bool one_step();

    private:
      bool one_step_item();
      bool one_step_level();

      void load_layer();

      void load_item_declaration();
      void load_item_definition();
      void load_item();

      void load_item_field_list();

      void load_item_field_int();
      void load_item_field_u_int();
      void load_item_field_real();
      void load_item_field_bool();
      void load_item_field_string();
      void load_item_field_sprite();
      void load_item_field_animation();
      void load_item_field_item();
      void load_item_field_sample();

      void load_item_field_int_list();
      void load_item_field_u_int_list();
      void load_item_field_real_list();
      void load_item_field_bool_list();
      void load_item_field_string_list();
      void load_item_field_sprite_list();
      void load_item_field_animation_list();
      void load_item_field_item_list();
      void load_item_field_sample_list();

      base_item* create_item_from_string( const std::string& name ) const;
      layer* create_layer_from_string
      ( const std::string& name, const universe::size_box_type& s ) const;

      void escape( std::string& str ) const;

      audio::sample* load_sample_data() const;

      template<typename T>
      std::string load_list( std::vector<T>& v );

    private:
      /** \brief The code of the next thing to read. */
      unsigned int m_next_code;

      /** \brief The level that we are building. */
      level* m_level;

      /** \brief The current layer. */
      layer* m_layer;

      /** \brief The file that we are reading. */
      compiled_file& m_file;

      /** \brief The item we are currently loading, if any. */
      base_item* m_current_item;

      /** \brief Referenced items. */
      std::vector<base_item*> m_referenced;

      /** \brief Count of items in the level. */
      unsigned int m_items_count;

      /** \brief Index of the currently built item. */
      unsigned int m_item_index;

      /** \brief Index of the next item definition to read. */
      unsigned int m_referenced_index;

    }; // class level_loader
  } // namespace engine
} // namespace bear

#endif // __ENGINE_LEVEL_LOADER_HPP__

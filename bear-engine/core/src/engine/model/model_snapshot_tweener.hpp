/*
 Bear Engine

 Copyright (C) 2005-2011 Julien Jorge, Sebastien Angibaud

 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 contact: plee-the-bear@gamned.org

 Please add the tag [Bear] in the subject of your mails.
 */
/**
 * \file model_snapshot_tweener.hpp
 * \brief A structure that manages all the values that can be interpolated in a
 *        snapshot.
 * \author Julien Jorge
 */
#ifndef __ENGINE_MODEL_TWEENER_HPP__
#define __ENGINE_MODEL_TWEENER_HPP__

#include "universe/types.hpp"
#include "engine/class_export.hpp"

#include <claw/tween/tweener_group.hpp>

#include <vector>

namespace bear
{
  namespace engine
  {
    class model_action;
    class model_mark_placement;
    class model_snapshot;

    /**
     * \brief A structure that manages all the values that can be interpolated
     *        in a snapshot.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT model_snapshot_tweener
    {
    public:
      typedef std::vector<model_mark_placement>::const_iterator
      const_mark_placement_iterator;

    public:
      explicit model_snapshot_tweener( const model_snapshot& init );
      model_snapshot_tweener
      ( const model_snapshot& init, const model_snapshot& end,
        const model_action& init_action, const model_action& end_action,
        universe::time_type d );

      model_mark_placement& get_mark_placement( std::size_t i );
      const model_mark_placement& get_mark_placement( std::size_t i ) const;

      const_mark_placement_iterator mark_placement_begin() const;
      const_mark_placement_iterator mark_placement_end() const;

      void update( universe::time_type elapsed_time );

    private:
      model_mark_placement get_mark_in_local_coordinates
      ( const model_snapshot& init, const model_snapshot& end,
          std::size_t id ) const;

      void insert_tweener
      ( std::size_t id, const model_mark_placement& end,
        universe::time_type d );

      // not implemented
      model_snapshot_tweener( const model_snapshot_tweener& that );
      model_snapshot_tweener& operator=( const model_snapshot_tweener& that );

    private:
      /** \brief The placement of the marks in this snapshot. */
      std::vector<model_mark_placement> m_placement;

      /** \brief The tweeners for the values of the mark placements. */
      claw::tween::tweener_group m_tweeners;

    }; // class model_snapshot_tweener

  } // namespace engine
} // namespace bear

#endif // __ENGINE_MODEL_TWEENER_HPP__

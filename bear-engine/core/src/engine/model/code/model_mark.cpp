/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file model_mark.cpp
 * \brief Implementation of the bear::engine::model_mark class.
 * \author Julien Jorge
 */
#include "engine/model/model_mark.hpp"

#include "engine/model/model_mark_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::model_mark::model_mark()
  : m_apply_angle_to_animation(false), m_pause_when_hidden(false),
    m_box_item( new model_mark_item )
{

} // model_mark::model_mark()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param label The label of the mark.
 * \param anim The animation displayed on the mark.
 * \param apply_angle Tell if the angle of the mark is applied to the animation.
 * \param pause_hidden Tell if the animation must be paused when hidden.
 */
bear::engine::model_mark::model_mark
( const std::string& label, const model_animation& anim, bool apply_angle,
  bool pause_hidden )
  : m_label(label), m_animation(anim), m_apply_angle_to_animation(apply_angle),
    m_pause_when_hidden(pause_hidden), m_box_item( new model_mark_item )
{

} // model_mark::model_mark()

/*----------------------------------------------------------------------------*/
/**
 * Copy constructor.
 * \param that The instance to copy from.
 */
bear::engine::model_mark::model_mark( const model_mark& that )
  : m_label(that.m_label), m_animation(that.m_animation),
    m_apply_angle_to_animation(that.m_apply_angle_to_animation),
    m_box_item(that.m_box_item->clone())
{

} // model_mark::model_mark()

/*----------------------------------------------------------------------------*/
/**
 * Assignment.
 * \param that The instance to assign from.
 */
bear::engine::model_mark& bear::engine::model_mark::operator=( model_mark that )
{
  swap(that);
  return *this;
} // model_mark::operator=()

/*----------------------------------------------------------------------------*/
/**
 * Swap the mark with an other mark.
 * \param that The mark to swap with.
 */
void bear::engine::model_mark::swap( model_mark& that ) throw()
{
  std::swap(m_label, that.m_label);
  std::swap(m_animation, that.m_animation);
  std::swap(m_apply_angle_to_animation, that.m_apply_angle_to_animation);
  std::swap(m_box_item, that.m_box_item);
} // model_mark::swap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the label of the mark.
 */
const std::string& bear::engine::model_mark::get_label() const
{
  return m_label;
} // model_mark::get_label()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is an animation on the mark.
 */
bool bear::engine::model_mark::has_animation() const
{
 if (m_substitute != NULL)
    return m_substitute->is_valid();
 else if (m_animation != NULL)
   return m_animation->is_valid();
 else
   return false;
} // model_mark::has_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the animation on the mark.
 */
bear::engine::model_animation& bear::engine::model_mark::get_animation()
{
  if ( m_substitute != NULL )
    return m_substitute;
  else
    return m_animation;
} // model_mark::get_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the animation on the mark.
 */
const bear::engine::model_animation&
bear::engine::model_mark::get_animation() const
{
  if ( m_substitute != NULL )
    return m_substitute;
  else
    return m_animation;
} // model_mark::get_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the animation on the mark.
 */
const bear::engine::model_animation&
bear::engine::model_mark::get_main_animation() const
{
  return m_animation;
} // model_mark::get_main_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the main animation for the mark.
 * \param anim The new animation on this mark.
 */
void bear::engine::model_mark::set_main_animation( const model_animation& anim )
{
  m_animation = anim;
} // model_mark::set_main_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the angle is applied to the animation.
 */
bool bear::engine::model_mark::apply_angle_to_animation() const
{
  return m_apply_angle_to_animation;
} // model_mark::apply_angle_to_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the animation must be paused when the mark is hidden.
 */
bool bear::engine::model_mark::pause_when_hidden() const
{
  return m_pause_when_hidden;
} // model_mark::pause_when_hidden()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the substitute for the animation of the mark.
 */
const bear::engine::model_animation&
bear::engine::model_mark::get_substitute() const
{
  return m_substitute;
} // model_mark::get_substitute()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a substitute for the animation of this mark.
 * \param anim The new animation on this mark.
 */
void bear::engine::model_mark::set_substitute( const model_animation& anim )
{
  m_substitute = anim;
} // model_mark::set_substitute()

/*----------------------------------------------------------------------------*/
/**
 * \brief Restore the default animation.
 */
void bear::engine::model_mark::remove_substitute()
{
  m_substitute = NULL;
} // model_mark::remove_substitute()

/*----------------------------------------------------------------------------*/
/**
 * Get the item to keep centered on this mark.
 */
bear::engine::model_mark_item& bear::engine::model_mark::get_box_item()
{
  return *m_box_item;
} // model_mark::get_box_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Swap two marks.
 * \param a The first mark.
 * \param b The second mark.
 */
void bear::engine::swap( engine::model_mark& a, engine::model_mark& b ) throw()
{
  a.swap(b);
} // bear::swap()

/*----------------------------------------------------------------------------*/
/**
 * \brief Swap two marks.
 * \param a The first mark.
 * \param b The second mark.
 */
template<>
void std::swap( bear::engine::model_mark& a, bear::engine::model_mark& b )
{
  a.swap(b);
} // std::swap()

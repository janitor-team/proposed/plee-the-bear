/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file model_animation.hpp
 * \brief A model is a group of animations and sounds.
 * \author Julien Jorge
 */
#ifndef __ENGINE_MODEL_ANIMATION_HPP__
#define __ENGINE_MODEL_ANIMATION_HPP__

#include "visual/animation.hpp"
#include <claw/smart_ptr.hpp>

namespace bear
{
  namespace engine
  {
    /** \brief An animation shared by all the actions and marks of a model. */
    typedef claw::memory::smart_ptr<visual::animation> model_animation;

  } // namespace engine

} // namespace bear

#endif // __ENGINE_MODEL_ANIMATION_HPP__

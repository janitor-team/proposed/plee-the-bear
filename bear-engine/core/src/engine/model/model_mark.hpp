/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file model_mark.hpp
 * \brief A mark in an action.
 * \author Julien Jorge
 */
#ifndef __ENGINE_MODEL_MARK_HPP__
#define __ENGINE_MODEL_MARK_HPP__

#include "universe/types.hpp"
#include "engine/model/model_animation.hpp"
#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class model_mark_item;

    /**
     * \brief A placement for a mark in a snapshot.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT model_mark
    {
    public:
      model_mark();
      model_mark
      ( const std::string& label, const model_animation& anim,
        bool apply_angle, bool pause_hidden );
      model_mark( const model_mark& that );

      model_mark& operator=( model_mark that );
      void swap( model_mark& that ) throw();

      const std::string& get_label() const;
      bool has_animation() const;
      model_animation& get_animation();
      const model_animation& get_animation() const;

      const model_animation& get_main_animation() const;
      void set_main_animation( const model_animation& anim );

      bool apply_angle_to_animation() const;
      bool pause_when_hidden() const;

      const model_animation& get_substitute() const;
      void set_substitute( const model_animation& anim );
      void remove_substitute();

      model_mark_item& get_box_item();

    private:
      /** \brief The label of the mark. */
      std::string m_label;

      /** \brief The animation displayed on the mark. */
      model_animation m_animation;

      /** \brief A substitution to the main animation. */
      model_animation m_substitute;

      /** \brief Tell if the angle is applied to the animation. */
      bool m_apply_angle_to_animation;

      /** \brief Tell if the animation must be paused when hidden. */
      bool m_pause_when_hidden;

      /** \brief The item centered on this mark. */
      model_mark_item* m_box_item;

    }; // class model_mark

    void swap( engine::model_mark& a, engine::model_mark& b ) throw();
  } // namespace engine
} // namespace bear

namespace std
{
  template<>
  void swap( bear::engine::model_mark& a, bear::engine::model_mark& b );
} // namespace std

#endif // __ENGINE_MODEL_MARK_HPP__

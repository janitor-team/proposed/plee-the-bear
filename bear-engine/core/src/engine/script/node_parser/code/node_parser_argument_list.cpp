/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file node_parser_argument_list.cpp
 * \brief Implementation of the bear::engine::node_parser_argument_list class.
 * \author Julien Jorge
 */
#include "engine/script/node_parser/node_parser_argument_list.hpp"

#include "engine/script/method_call.hpp"
#include "engine/script/script_grammar.hpp"
#include "engine/script/node_parser/node_parser_argument.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Parse a node of type argument_list.
 * \param call The method call for which we set the arguments.
 * \param node Node to parse.
 */
void bear::engine::node_parser_argument_list::parse_node
( method_call& call, const tree_node& node ) const
{
  std::vector<std::string> args;
  node_parser_argument parser;
  std::string val;

  if ( node.value.id() == script_grammar::id_argument_list )
    {
      for (std::size_t i=0; i!=node.children.size(); ++i)
        {
          parser.parse_node(val, node.children[i]);
          args.push_back(val);
        }
    }
  else
    {
      parser.parse_node(val, node);
      args.push_back(val);
    }

  call.set_arguments(args);
} // node_parser_argument_list::parse_node()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file node_parser_call_group.cpp
 * \brief Implementation of the bear::engine::node_parser_call_group class.
 * \author Julien Jorge
 */
#include "engine/script/node_parser/node_parser_call_group.hpp"

#include "engine/script/node_parser/node_parser_call.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Parse a node of type call_group.
 * \param seq The sequence in which the calls are inserted.
 * \param node Node to parse.
 * \param date The date of the call.
 */
void bear::engine::node_parser_call_group::parse_node
( call_sequence& seq, const tree_node& node, universe::time_type date ) const
{
  CLAW_PRECOND( node.children.size() >= 1 );

  node_parser_call call;

  for ( std::size_t i=0; i!=node.children.size(); ++i )
    call.parse_node( seq, node.children[i], date );
} // node_parser_call_group::parse_node()

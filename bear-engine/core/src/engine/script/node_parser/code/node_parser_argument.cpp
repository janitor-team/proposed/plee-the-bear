/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file node_parser_argument.cpp
 * \brief Implementation of the bear::engine::node_parser_argument class.
 * \author Julien Jorge
 */
#include "engine/script/node_parser/node_parser_argument.hpp"

#include "engine/script/script_grammar.hpp"
#include <claw/string_algorithm.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Parse a node of type argument.
 * \param val The value of the argument.
 * \param node Node to parse.
 */
void bear::engine::node_parser_argument::parse_node
( std::string& val, const tree_node& node ) const
{
  val = std::string( node.value.begin(), node.value.end() );

  if ( node.value.id() == script_grammar::id_string )
    {
      std::string tmp;
      std::swap(tmp, val);

      claw::text::c_escape
        ( tmp.begin(), tmp.end(), std::inserter(val, val.end()) );
    }
} // node_parser_argument::parse_node()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file node_parser.cpp
 * \brief Implementation of the bear::engine::node_parser class.
 * \author Julien Jorge
 */
#include "engine/script/node_parser/node_parser.hpp"

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Write an error on the log stream.
 * \param first Begining of the incorrect input.
 * \param last End of the incorrect input.
 * \param msg Informative message.
 */
void bear::engine::node_parser::error
( data_iterator first, data_iterator last, const std::string& msg ) const
{
  boost::spirit::classic::file_position pos = first.get_position();

  claw::logger << claw::log_error << pos.file << ": " << pos.line << ": "
               << pos.column << ": " << msg << std::endl;
} // node_parser::error()

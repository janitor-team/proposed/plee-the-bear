/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file node_parser.hpp
 * \brief Base class for node parsers.
 * \author Julien Jorge
 */
#ifndef __ENGINE_NODE_PARSER_HPP__
#define __ENGINE_NODE_PARSER_HPP__

#include "engine/class_export.hpp"

#include <boost/spirit/include/classic.hpp>
#include <boost/spirit/include/classic_position_iterator.hpp>
#include <boost/spirit/include/classic_parse_tree.hpp>

#include <string>

namespace bear
{
  namespace engine
  {
    /**
     * \brief Base class for node parsers.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT node_parser
    {
    public:
      /** \brief The type of an iterator on the input data. */
      typedef
      boost::spirit::classic::position_iterator<const char*> data_iterator;

      typedef
      boost::spirit::classic::node_iter_data_factory<data_iterator>
      node_factory;
      typedef
      boost::spirit::classic::tree_match<data_iterator, node_factory>
      tree_match;

      /** \brief The type of a node of the tree built by the compiler. */
      typedef tree_match::node_t tree_node;

    protected:
      void error
      ( data_iterator first, data_iterator last, const std::string& msg ) const;

    }; // class node_parser
  } // namespace engine
} // namespace bear

#endif // __ENGINE_NODE_PARSER_HPP__

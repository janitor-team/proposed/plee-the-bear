/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file node_parser_call_entry.hpp
 * \brief Compile the node of the "call_entry" rule.
 * \author Julien Jorge
 */
#ifndef __ENGINE_NODE_PARSER_CALL_ENTRY_HPP__
#define __ENGINE_NODE_PARSER_CALL_ENTRY_HPP__

#include "engine/class_export.hpp"

#include "engine/script/node_parser/node_parser.hpp"

namespace bear
{
  namespace engine
  {
    class call_sequence;

    /**
     * \brief Compile the node of the "call" rule.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT node_parser_call_entry:
      public node_parser
    {
    public:
      void parse_node( call_sequence& seq, const tree_node& node ) const;

    }; // class node_parser_call_entry
  } // namespace engine
} // namespace bear

#endif // __ENGINE_NODE_PARSER_CALL_ENTRY_HPP__

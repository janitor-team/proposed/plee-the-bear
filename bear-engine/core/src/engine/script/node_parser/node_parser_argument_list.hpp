/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file node_parser_argument_list.hpp
 * \brief Compile the node of the "argument_list" rule.
 * \author Julien Jorge
 */
#ifndef __ENGINE_NODE_PARSER_ARGUMENT_LIST_HPP__
#define __ENGINE_NODE_PARSER_ARGUMENT_LIST_HPP__

#include "engine/script/node_parser/node_parser.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class method_call;

    /**
     * \brief Compile the node of the "argument_list" rule.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT node_parser_argument_list:
      public node_parser
    {
    public:
      void parse_node( method_call& call, const tree_node& node ) const;

    }; // class node_parser_argument_list
  } // namespace engine
} // namespace bear

#endif // __ENGINE_NODE_PARSER_ARGUMENT_LIST_HPP__

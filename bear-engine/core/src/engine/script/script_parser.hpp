/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file script_parser.hpp
 * \brief The parser for the script files.
 * \author Julien Jorge
 */
#ifndef __ENGINE_SCRIPT_PARSER_HPP__
#define __ENGINE_SCRIPT_PARSER_HPP__

#include "engine/class_export.hpp"

#include <boost/spirit/include/classic.hpp>
#include <boost/spirit/include/classic_position_iterator.hpp>
#include <boost/spirit/include/classic_parse_tree.hpp>

#include <istream>

namespace bear
{
  namespace engine
  {
    class call_sequence;

    /**
     * \brief The parser for the script files.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT script_parser
    {
    public:
      typedef boost::spirit::classic::position_iterator<const char*> iterator;
      typedef
      boost::spirit::classic::node_iter_data_factory<iterator> node_factory;
      typedef
      boost::spirit::classic::tree_match<iterator, node_factory> tree_match;
      typedef tree_match::node_t tree_node;

    private:
      typedef boost::spirit::classic::scanner<iterator> scanner;

    public:
      bool run( call_sequence& seq, const std::string& path );
      bool run( call_sequence& seq, const std::istream& in_file );
      bool run
      ( call_sequence& seq, const char* file_data, unsigned int file_size );

    private:
      void scan_tree( call_sequence& seq, const tree_node& node );

    }; // class script_parser
  } // namespace engine
} // namespace bear

#endif // __ENGINE_SCRIPT_PARSER_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file method_call.hpp
 * \brief The data needed to call a method on an item when executing a script.
 * \author Julien Jorge
 */
#ifndef __ENGINE_METHOD_CALL_HPP__
#define __ENGINE_METHOD_CALL_HPP__

#include "engine/class_export.hpp"

#include <vector>
#include <string>

namespace bear
{
  namespace engine
  {
    /**
     * \brief The context in which the scripts on items are executed.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT method_call
    {
    public:
      void set_actor_name( const std::string& name );
      const std::string& get_actor_name() const;

      void set_method_name( const std::string& name );
      const std::string& get_method_name() const;

      void set_arguments( const std::vector<std::string>& val );
      const std::vector<std::string>& get_arguments() const;

    private:
      /** \brief The name of the actor. */
      std::string m_actor_name;

      /** \brief The name of the method. */
      std::string m_method_name;

      /** \brief The arguments passed to the method. */
      std::vector<std::string> m_arguments;

    }; // class method_call

  } // namespace engine
} // namespace bear

#endif // __ENGINE_METHOD_CALL_HPP__

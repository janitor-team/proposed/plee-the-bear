/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file method_call.cpp
 * \brief Implementation of the bear::engine::method_call class.
 * \author Julien Jorge
 */
#include "engine/script/method_call.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the name of the actor on which the method is called.
 * \param name The name.
 */
void bear::engine::method_call::set_actor_name( const std::string& name )
{
  m_actor_name = name;
} // method_call::set_actor_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the actor on which the method is called.
 */
const std::string& bear::engine::method_call::get_actor_name() const
{
  return m_actor_name;
} // method_call::get_actor_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the name of the method to call.
 * \param name The name.
 */
void bear::engine::method_call::set_method_name( const std::string& name )
{
  m_method_name = name;
} // method_call::set_method_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the method to call.
 */
const std::string& bear::engine::method_call::get_method_name() const
{
  return m_method_name;
} // method_call::get_method_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the argument values passed to the called method.
 * \param val The values of the arguments.
 */
void bear::engine::method_call::set_arguments
( const std::vector<std::string>& val )
{
  m_arguments = val;
} // method_call::set_arguments()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the values passed as arguments to the called method.
 */
const std::vector<std::string>& bear::engine::method_call::get_arguments() const
{
  return m_arguments;
} // method_call::get_arguments()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_local_client.hpp
 * \brief The class managing the levels and the development of the game.
 * \author Julien Jorge
 */
#ifndef __ENGINE_GAME_LOCAL_CLIENT_HPP__
#define __ENGINE_GAME_LOCAL_CLIENT_HPP__

#include <fstream>
#include <queue>

#include "engine/class_export.hpp"
#include "engine/game_description.hpp"
#include "engine/game_stats.hpp"
#include "engine/libraries_pool.hpp"
#include "engine/variable/var_map.hpp"
#include "time/time.hpp"
#include "visual/screen.hpp"
#include "universe/types.hpp"

#include <claw/arguments_table.hpp>

#include <boost/function.hpp>

namespace bear
{
  namespace engine
  {
    class base_variable;
    class game_action;
    class game_action_load_level;
    class game_action_pop_level;
    class game_action_push_level;
    class game_action_set_current_level;
    class game_action_set_current_level;
    class level;

    /**
     * \brief The class managing the levels and the development of the game.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT game_local_client
    {
      friend class game_action;
      friend class game_action_load_level;
      friend class game_action_pop_level;
      friend class game_action_push_level;
      friend class game_action_set_current_level;

    private:
      /** \brief Type of the game specific initialisation procedure. */
      typedef void (*init_game_function_type)();

      /** \brief Type of the game specific ending procedure. */
      typedef void (*end_game_function_type)();

      /**
       * \brief Game status.
       */
      enum status
        {
          /** \brief The game is under initialization. */
          status_init,

          /** \brief The game is running. */
          status_run,

          /** \brief We're quiting. */
          status_quit

        }; // enum status

    public:
      static void print_help();

      game_local_client( int& argc, char** &argv );
      ~game_local_client();

      void run();
      systime::milliseconds_type get_time_step() const;

      void set_fullscreen( bool full );
      bool get_fullscreen() const;

      void set_sound_muted( bool m );
      bool get_sound_muted() const;
      void set_sound_volume( double v );
      double get_sound_volume() const;

      void set_music_muted( bool m );
      bool get_music_muted() const;
      void set_music_volume( double v );
      double get_music_volume() const;

      void screenshot( claw::graphic::image& img ) const;
      void levelshot( claw::graphic::image& img ) const;

      void end();
      void set_waiting_level( const std::string& path );
      void set_waiting_level( level* the_level );
      void push_level( const std::string& path );
      void pop_level();

      const claw::math::coordinate_2d<unsigned int>& get_screen_size() const;
      double get_active_area_margin() const;
      std::string get_custom_game_file( const std::string& name ) const;

      void get_game_variable( base_variable& val ) const;
      void get_game_variables
      ( var_map& vars, const std::string& pattern = ".*");
      void set_game_variable( const base_variable& val );
      void set_game_variables( const var_map& vars );
      void erase_game_variables( const std::string& pattern );
      bool game_variable_exists( const base_variable& val ) const;
      void get_all_game_variables( var_map& vars ) const;

      boost::signals::connection
        listen_int_variable_change
        ( const std::string& name, const boost::function<void (int)>& f );
      boost::signals::connection
        listen_uint_variable_change
        ( const std::string& name,
          const boost::function<void (unsigned int)>& f );
      boost::signals::connection
        listen_bool_variable_change
        ( const std::string& name, const boost::function<void (bool)>& f );
      boost::signals::connection
        listen_double_variable_change
        ( const std::string& name, const boost::function<void (double)>& f );
      boost::signals::connection
        listen_string_variable_change
        ( const std::string& name,
          const boost::function<void (std::string)>& f );

      const std::string& get_name() const;

    private:
      void init_stats();

      void init_game() const;
      void end_game() const;

      std::string get_game_name_as_filename() const;

      std::string get_game_directory() const;
      bool create_game_directory( const std::string& dir ) const;

      void run_level();
      void one_step_beyond();

      void progress( universe::time_type elapsed_time );
      void render();

      void update_inputs();

      void init_environment() const;
      void close_environment() const;

      void load_libraries( const std::list<std::string>& p );
      void init_resource_pool( const std::list<std::string>& p ) const;

      bool do_post_actions();

      void set_current_level( level* the_level );
      void load_level( const std::string& path );
      void close_level();
      void do_push_level( const std::string& path );
      void do_pop_level();

      void start_current_level();

      void clear();

      bool check_arguments( int& argc, char** &argv );

      template<typename T>
      bool set_game_variable_from_arg
        ( const std::list<std::string>& args, const char sep );

      template<typename T>
        boost::signals::connection
        listen_variable_change
        ( const std::string& name, boost::function<void (T)> f );

      static claw::arguments_table get_arguments_table();

    private:
      // must be declared before m_game_description
      /** \brief The libraries in which we take custom functions. */
      libraries_pool m_symbols;

      /** \brief The current status of the game. */
      status m_status;

      /** \brief Description of the game. */
      game_description m_game_description;

      /** \brief Global variables of the game. */
      var_map m_game_variables;

      /** \brief The screen. */
      visual::screen* m_screen;

      /** \brief Tell if we are fullscreen or not. */
      bool m_fullscreen;

      /** \brief The current level. */
      level* m_current_level;

      /** \brief A level in abeyance. */
      level* m_level_in_abeyance;

      /** \brief The name of the next level to load, if any. */
      std::string m_waiting_level;

      /** \brief Actions to do once an iteration is done. */
      std::queue<game_action*> m_post_actions;

      /** \brief Number of milliseconds between two iterations. */
      systime::milliseconds_type m_time_step;

      /** \brief The date of the last call to progress. */
      systime::milliseconds_type m_last_progress;

      /** \brief The statistics sent at the end of the game. */
      game_stats m_stats;

      /** \brief The prefix of the name of the game specific initialization
          procedure. */
      static const std::string s_init_game_function_prefix;

      /** \brief The prefix of the name of the game specific ending
          procedure. */
      static const std::string s_end_game_function_prefix;

    }; // class game_local_client
  } // namespace engine
} // namespace bear

#endif // __ENGINE_GAME_LOCAL_CLIENT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file model_loader.hpp
 * \brief This class loads a model_actor.
 * \author Julien Jorge
 */
#ifndef __ENGINE_MODEL_LOADER_HPP__
#define __ENGINE_MODEL_LOADER_HPP__

#include "engine/compiled_file.hpp"
#include "engine/model/model_animation.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class level_globals;
    class model_action;
    class model_actor;
    class model_snapshot;

    /**
     * \brief This class loads a model_actor.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT model_loader
    {
    private:
      /** \brief The type of the map containing all the animations of the
          model. */
      typedef std::vector<model_animation> anim_map_type;

    public:
      model_loader( std::istream& f, level_globals& glob );

      model_actor* run();

    private:
      void load_actions( model_actor& m, const anim_map_type& anim_map );
      void load_action( model_actor& m, const anim_map_type& anim_map );
      void load_sound( std::string& sound_name, bool& glob );
      void load_marks( model_action& a, const anim_map_type& anim_map );
      void load_snapshots( model_action& a );
      void load_snapshot( model_action& a );
      void load_mark_placements( model_snapshot& s );
      void load_mark_placement( model_snapshot& s );

      void load_animations( anim_map_type& anim_map );

    private:
      /** \brief The file from which we load the model. */
      compiled_file m_file;

      /** \brief The level_globals in which we load the resources. */
      level_globals& m_level_globals;

    }; // class model_loader
  } // namespace engine
} // namespace bear

#endif // __ENGINE_MODEL_LOADER_HPP__

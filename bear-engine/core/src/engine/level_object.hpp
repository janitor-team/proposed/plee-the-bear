/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_object.hpp
 * \brief An item in a level.
 * \author Julien Jorge
 */
#ifndef __ENGINE_LEVEL_OBJECT_HPP__
#define __ENGINE_LEVEL_OBJECT_HPP__

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class level;
    class level_globals;

    /**
     * \brief An item in a level.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT level_object
    {
    public:
      level_object();
      level_object( const level_object& that );
      virtual ~level_object();

      void clear_level();
      void set_level( level& lvl );
      level& get_level() const;

      level_globals& get_level_globals() const;

    private:
      /** \brief The level in which I am. */
      level* m_level;

    }; // class level_object

  } // namespace engine
} // namespace bear

#endif // __ENGINE_LEVEL_OBJECT_HPP__

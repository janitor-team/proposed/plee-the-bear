/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file fade_effect.hpp
 * \brief A color fading in and out.
 * \author Julien Jorge
 */
#ifndef __BEAR_ENGINE_FADE_EFFECT_HPP__
#define __BEAR_ENGINE_FADE_EFFECT_HPP__

#include "engine/transition_effect/transition_effect.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A color fading in and out.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT fade_effect:
      public transition_effect
    {
    public:
      /** \brief The type of a list of scene elements retrieved from the
          layer. */
      typedef std::list<visual::scene_element> scene_element_list;

    public:
      fade_effect();

      bool is_finished() const;

      universe::time_type
      progress( bear::universe::time_type elapsed_time );
      void render( scene_element_list& e ) const;

      void set_duration
      ( universe::time_type in, universe::time_type full,
        universe::time_type out );
      void set_color( double r, double g, double b );
      void set_opacity( double o );

    private:
      void adjust_opacity();

    private:
      /** \brief How long the fade in is. */
      universe::time_type m_fade_in_duration;

      /** \brief How long the full intensity is kept. */
      universe::time_type m_full_duration;

      /** \brief How long the fade out is. */
      universe::time_type m_fade_out_duration;

      /** \brief Elapsed time since the effect was created. */
      universe::time_type m_elapsed_time;

      /** \brief The color displayed. */
      visual::color_type m_color;

      /** \brief The opacity of at the maximum intensity. */
      double m_opacity;

    }; // class fade_effect
  } // namepspace engine
} // namespace bear

#endif // __BEAR_ENGINE_FADE_EFFECT_HPP__

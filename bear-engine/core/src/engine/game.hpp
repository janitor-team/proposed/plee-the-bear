/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game.hpp
 * \brief The class managing the levels and the development of the game.
 * \author Julien Jorge
 */
#ifndef __ENGINE_GAME_HPP__
#define __ENGINE_GAME_HPP__

#include "engine/variable/var_map.hpp"
#include "engine/class_export.hpp"

#include "time/time.hpp"

#include <claw/image.hpp>
#include <claw/coordinate_2d.hpp>

#include <boost/function.hpp>

namespace bear
{
  namespace engine
  {
    class base_variable;
    class game_local_client;
    class level;
    class libraries_pool;

    /**
     * \brief The class managing the levels and the evolution of the game.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT game
    {
    public:
      static game& get_instance();
      static void print_help();

      game( int& argc, char** &argv );
      ~game();

      void run();
      systime::milliseconds_type get_time_step() const;

      void set_fullscreen( bool full );
      bool get_fullscreen() const;
      void toggle_fullscreen();

      void set_sound_muted( bool m );
      bool get_sound_muted() const;
      void toggle_sound_muted();
      void set_sound_volume( double v );
      double get_sound_volume() const;

      void set_music_muted( bool m );
      bool get_music_muted() const;
      void toggle_music_muted();
      void set_music_volume( double v );
      double get_music_volume() const;

      void screenshot( claw::graphic::image& img ) const;
      void levelshot( claw::graphic::image& img ) const;

      void end();
      void set_waiting_level( const std::string& path );
      void set_waiting_level( level* the_level );
      void push_level( const std::string& path );
      void pop_level();

      const claw::math::coordinate_2d<unsigned int>& get_screen_size() const;
      double get_active_area_margin() const;
      std::string get_custom_game_file( const std::string& name ) const;

      void get_game_variable( base_variable& val ) const;
      void get_game_variables
      (var_map& vars, const std::string& pattern = ".*" );
      void set_game_variable( const base_variable& val );
      void set_game_variables( const var_map& vars );
      void erase_game_variables( const std::string& pattern );
      bool game_variable_exists( const base_variable& val ) const;

      void save_game_variables
        ( std::ostream& os, const std::string& pattern = ".*" );

      boost::signals::connection
        listen_int_variable_change
        ( const std::string& name, const boost::function<void (int)>& f );
      boost::signals::connection
        listen_uint_variable_change
        ( const std::string& name,
          const boost::function<void (unsigned int)>& f );
      boost::signals::connection
        listen_bool_variable_change
        ( const std::string& name, const boost::function<void (bool)>& f );
      boost::signals::connection
        listen_double_variable_change
        ( const std::string& name, const boost::function<void (double)>& f );
      boost::signals::connection
        listen_string_variable_change
        ( const std::string& name,
          const boost::function<void (const std::string)>& f );

      const std::string& get_name() const;

    private:
      /** \brief The instance of the game on this computer. */
      static game* s_instance;

      /** \brief The effective implementation of the game. */
      game_local_client* m_game;

    }; // class game
  } // namespace engine
} // namespace bear

#endif // __ENGINE_GAME_HPP__

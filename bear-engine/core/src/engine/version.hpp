/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file version.hpp
 * \brief Version of the engine.
 * \author Julien Jorge
 */
#ifndef __BEAR_VERSION_HPP__
#define __BEAR_VERSION_HPP__

#define BEAR_TO_STR_BIS(v) # v
#define BEAR_TO_STR(v) BEAR_TO_STR_BIS(v)

#define BEAR_MAJOR_VERSION 0
#define BEAR_MINOR_VERSION 6
#define BEAR_RELEASE_NUMBER 0
#define BEAR_VERSION_STRING "Bear Engine " BEAR_TO_STR(BEAR_MAJOR_VERSION) "." \
  BEAR_TO_STR(BEAR_MINOR_VERSION) "." BEAR_TO_STR(BEAR_RELEASE_NUMBER)

#endif // __BEAR_VERSION_HPP__

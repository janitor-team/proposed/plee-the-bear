/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file population.hpp
 * \brief All the items of a level.
 * \author Julien Jorge
 */
#ifndef __ENGINE_POPULATION_HPP__
#define __ENGINE_POPULATION_HPP__

#include <map>
#include <set>
#include <claw/functional.hpp>
#include <claw/iterator.hpp>

#include "engine/base_item.hpp"
#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief All the items of a level.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT population
    {
    private:
      /** brief The type of the map containing the items. */
      typedef std::map<base_item::id_type, base_item*> item_map;

    public:
      /** \brief Iterator on the living items. */
      typedef claw::wrapped_iterator
        < base_item,
          item_map::const_iterator,
          claw::unary_compose
          < claw::dereference<base_item>,
            claw::const_pair_second<item_map::value_type> > >
      ::iterator_type const_iterator;

    public:
      ~population();

      void insert( base_item* item );
      void kill( const base_item* item );
      void drop( const base_item* item );

      bool exists( base_item::id_type id ) const;

      void remove_dead_items();
      void clear();

      const_iterator begin() const;
      const_iterator end() const;

    private:
      /** \brief All items currently in the game. */
      item_map m_items;

      /** \brief The items that will be deleted by calling
          remove_dead_items(). */
      std::set<base_item::id_type> m_dead_items;

      /** \brief The items that will be removed by calling
          remove_dead_items(). */
      std::set<base_item::id_type> m_dropped_items;

    }; // class population
  } // namespace engine
} // namespace bear

#endif // __ENGINE_POPULATION_HPP__

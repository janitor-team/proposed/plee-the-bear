/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file messageable_item.hpp
 * \brief A physical item that can receive messages.
 * \author Julien Jorge
 */
#ifndef __ENGINE_MESSAGEABLE_ITEM_HPP__
#define __ENGINE_MESSAGEABLE_ITEM_HPP__

#include "communication/messageable.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A physical item that can receive messages.
     * \author Julien Jorge
     *
     * \b Template \b arguments:
     * - \a Base: any hierarchy ending by base_item.
     */
    template<class Base>
    class messageable_item :
      public Base,
      public communication::messageable
    {
    public:
      /** \brief The type of the parent class. */
      typedef Base super;

    public:
      messageable_item();
      messageable_item( const std::string& name );

      void build();
      void destroy();

      bool
      set_string_field( const std::string& name, const std::string& value );

      bool is_valid() const;

    protected:
      void to_string( std::string& str ) const;

      bool set_name( const std::string& name );

    }; // class messageable_item
  } // namespace engine
} // namespace bear

#include "engine/impl/messageable_item.tpp"

#endif // __ENGINE_MESSAGEABLE_ITEM_HPP__

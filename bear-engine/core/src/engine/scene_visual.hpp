/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file scene_visual.hpp
 * \brief A class that stores a graphic representation of an object and its
 *        position in the world. This class is used to store the visible items
 *        in the rendering step.
 * \author Julien Jorge
 */
#ifndef __ENGINE_SCENE_VISUAL_HPP__
#define __ENGINE_SCENE_VISUAL_HPP__

#include "visual/scene_element.hpp"
#include "visual/sprite.hpp"
#include "universe/types.hpp"
#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A class that stores a graphic representation of an object and its
     *        position in the world. This class is used to store the visible
     *        items in the rendering step.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT scene_visual
    {
    public:
      /** \brief Compare two scene_visual instances on their z_position. */
      struct ENGINE_EXPORT z_position_compare
      {
        bool operator()( const scene_visual& s1, const scene_visual& s2 ) const;
      }; // struct z_position_compare

    public:
      scene_visual( universe::coordinate_type x, universe::coordinate_type y,
                    const visual::sprite& spr, int z = 0 );
      scene_visual( const universe::position_type& pos,
                    const visual::sprite& spr, int z = 0 );
      scene_visual( const visual::scene_element& e, int z = 0 );
      scene_visual( const visual::base_scene_element& e, int z = 0 );

    public:
      /** \brief The visual to display. */
      visual::scene_element scene_element;

      /** \brief Position of the visual in the rendering procedure. */
      int z_position;

    }; // class scene_visual
  } // namespace engine
} // namespace bear

#endif // __ENGINE_SCENE_VISUAL_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file libraries_pool.tpp
 * \brief Implementation of the template methods of the
 *        bear::engine::libraries_pool class.
 * \author Julien Jorge
 */
#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a symbol from the libraries.
 * \param name The symbol to get.
 * \pre have_symbol( name )
 */
template<class T>
T bear::engine::libraries_pool::get_symbol( const std::string& name ) const
{
  CLAW_PRECOND( have_symbol(name) );

  bool found=false;
  libraries_list::const_iterator it = m_libraries.begin();

  while ( !found )
    if ( (*it)->have_symbol(name) )
      found = true;
    else
      ++it;

  return (*it)->get_symbol<T>( name );
} // libraries_pool::get_symbol()

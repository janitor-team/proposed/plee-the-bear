/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_action_set_current_level.cpp
 * \brief Implementation of the engine::game_action_set_current_level class.
 * \author Julien Jorge
 */
#include "engine/game_action/game_action_set_current_level.hpp"
#include "engine/game_local_client.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param the_level The level to set.
 */
bear::engine::game_action_set_current_level::game_action_set_current_level
( level* the_level )
  : m_level(the_level)
{
  CLAW_PRECOND( the_level != NULL );
} // game_action_set_current_level::game_action_set_current_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the action to a game.
 * \param the_game The game to apply to.
 */
bool bear::engine::game_action_set_current_level::apply
( game_local_client& the_game )
{
  the_game.set_current_level( m_level );
  return true;
} // game_action_set_current_level::apply()

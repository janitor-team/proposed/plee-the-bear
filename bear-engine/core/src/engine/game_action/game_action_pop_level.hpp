/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_action_pop_level.hpp
 * \brief Tell a game to pop the current level.
 * \author Julien Jorge
 */
#ifndef __ENGINE_GAME_ACTION_POP_LEVEL_HPP__
#define __ENGINE_GAME_ACTION_POP_LEVEL_HPP__

#include "engine/game_action/game_action.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief Tell a game to pop the current level.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT game_action_pop_level:
      public game_action
    {
    public:
      bool apply( game_local_client& the_game );

    }; // class game_action_pop_level
  } // namespace engine
} // namespace bear

#endif // __ENGINE_GAME_ACTION_POP_LEVEL_HPP__

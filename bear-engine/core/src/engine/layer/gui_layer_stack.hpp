/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file gui_layer_stack.hpp
 * \brief The stack of the layers for the graphical user interface.
 * \author Julien Jorge
 */
#ifndef __BEAR_GUI_LAYER_STACK_HPP__
#define __BEAR_GUI_LAYER_STACK_HPP__

#include "input/input_listener.hpp"
#include "input/input_status.hpp"
#include "visual/scene_element.hpp"
#include "universe/types.hpp"

#include "engine/class_export.hpp"

#include <vector>

namespace bear
{
  namespace engine
  {
    class gui_layer;

    /**
     * \brief The stack of the layers for the graphical user interface.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT gui_layer_stack:
      public input::input_listener
    {
    public:
      /** \brief The type of the container in which we store the visuals when
          rendering. */
      typedef std::list<visual::scene_element> scene_element_list;

    public:
      ~gui_layer_stack();

      void push_layer( gui_layer* the_layer );

      void progress( universe::time_type elapsed_time );
      void render( scene_element_list& e ) const;

      void clear();

    private:
      bool key_pressed( const input::key_info& key );
      bool key_maintained( const input::key_info& key );
      bool key_released( const input::key_info& key );
      bool char_pressed( const input::key_info& key );
      bool button_pressed
      ( input::joystick::joy_code button, unsigned int joy_index );
      bool button_maintained
      ( input::joystick::joy_code button, unsigned int joy_index );
      bool button_released
      ( input::joystick::joy_code button, unsigned int joy_index );
      bool mouse_pressed( input::mouse::mouse_code key,
                          const claw::math::coordinate_2d<unsigned int>& pos );
      bool mouse_released( input::mouse::mouse_code button,
                           const claw::math::coordinate_2d<unsigned int>& pos );
      bool mouse_maintained
      ( input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );
      bool mouse_move( const claw::math::coordinate_2d<unsigned int>& pos );

    private:
      /** \brief The sub layers */
      std::vector<gui_layer*> m_sub_layers;

      /** \brief The status of the input controllers. */
      input::input_status m_input_status;

    }; // class gui_layer_stack
  } // namespace engine
} // namespace bear

#endif // __BEAR_GUI_LAYER_STACK_HPP__

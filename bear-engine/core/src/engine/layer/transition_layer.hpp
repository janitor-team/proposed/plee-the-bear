/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [BEAR] in the subject of your mails.
*/
/**
 * \file transition_layer.hpp
 * \brief This layer displays transition effects.
 * \author Julien Jorge
 */
#ifndef __BEAR_ENGINE_TRANSITION_LAYER_HPP__
#define __BEAR_ENGINE_TRANSITION_LAYER_HPP__

#include "communication/messageable.hpp"
#include "engine/layer/gui_layer.hpp"

#include "engine/class_export.hpp"

#include <map>

namespace bear
{
  namespace engine
  {
    class transition_effect;

    /**
     * \brief This layer displays transition effects.
     * \author Julien Jorge
     *
     * The progress() method does nothing if the level is paused. Effect coders
     * can prevent the user from setting the pause by returning true in the
     * methods transition_effect::key_pressed() and
     * transition_effect::button_pressed().
     */
    class ENGINE_EXPORT transition_layer:
      public gui_layer,
      public communication::messageable
    {
    public:
      /** \brief The type of a list of scene elements retrieved from the
          layer. */
      typedef gui_layer::scene_element_list scene_element_list;

    private:
      /** \brief The data associated with the effects. */
      struct effect_entry
      {
        effect_entry( transition_effect* e, std::size_t id );

        /** \brief The displayed effect. */
        transition_effect* effect;

        /** \brief The identifier of the effect. */
        std::size_t id;

      }; // struct effect_entry

      /** \brief The type of the map in which are stored the active effects. */
      typedef std::multimap<int, effect_entry> effect_map_type;

    public:
      transition_layer( const std::string& name );
      ~transition_layer();

      void build();
      void progress( universe::time_type elapsed_time );
      void render( scene_element_list& e ) const;

      bool key_pressed( const input::key_info& key );
      bool key_maintained( const input::key_info& key );
      bool key_released( const input::key_info& key );
      bool char_pressed( const input::key_info& key );
      bool button_pressed
      ( input::joystick::joy_code button, unsigned int joy_index );
      bool button_maintained
      ( input::joystick::joy_code button, unsigned int joy_index );
      bool button_released
      ( input::joystick::joy_code button, unsigned int joy_index );
      bool mouse_pressed
      ( input::mouse::mouse_code key,
        const claw::math::coordinate_2d<unsigned int>& pos );
      bool mouse_released
      ( input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );
      bool mouse_maintained
      ( input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );
      bool mouse_move( const claw::math::coordinate_2d<unsigned int>& pos );

      void erase_effect( std::size_t id );
      std::size_t push_effect( transition_effect* e, int p = 0 );
      std::size_t set_effect( transition_effect* e, int p = 0 );

    private:
      void clear();

      template<typename F>
      bool diffuse_call( F f ) const;

    public:
      /** \brief An invalid value for the identifiers of the effects. */
      static const std::size_t not_an_id;

    private:
      /** \brief The current effect displayed. */
      effect_map_type m_effect;

      /** \brief The next available id for the effects. */
      static std::size_t s_next_id;

    }; // class transition_layer
  } // namespace engine
} // namespace bear

#endif // __BEAR_ENGINE_TRANSITION_LAYER_HPP__

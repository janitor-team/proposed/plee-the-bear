/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file gui_layer.cpp
 * \brief Implementation of the bear::engine::gui_layer class.
 * \author Julien Jorge
 */
#include "engine/layer/gui_layer.hpp"

#include "engine/game.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::gui_layer::gui_layer()
  : m_size( game::get_instance().get_screen_size() )
{

} // gui_layer::gui_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the media required by this class.
 */
void bear::engine::gui_layer::pre_cache()
{
  // nothing to do
} // gui_layer::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the layer.
 */
void bear::engine::gui_layer::build()
{
  // nothing to do
} // gui_layer::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one step in the progression of the layer.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::engine::gui_layer::progress( universe::time_type elapsed_time )
{
  // nothing to do
} // gui_layer::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the scene elements of the layer.
 * \param e (out) The scene elements.
 */
void bear::engine::gui_layer::render( scene_element_list& e ) const
{
  // nothing to do
} // gui_layer::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size of the layer.
 */
const claw::math::coordinate_2d<unsigned int>&
bear::engine::gui_layer::get_size() const
{
  return m_size;
} // gui_layer::get_size()


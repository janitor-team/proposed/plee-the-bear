/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file gui_layer.hpp
 * \brief Inherit from the engine::gui_layer class to create a layer in the
 *        graphical user interface.
 * \author Julien Jorge
 */
#ifndef __ENGINE_GUI_LAYER_HPP__
#define __ENGINE_GUI_LAYER_HPP__

#include "engine/level_object.hpp"
#include "input/input_listener.hpp"
#include "visual/screen.hpp"
#include "universe/types.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief Inherit from this class to create a layer in the graphical user
     *        interface.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT gui_layer:
      public input::input_listener,
      virtual public level_object
    {
    public:
      typedef std::list<visual::scene_element> scene_element_list;

    public:
      gui_layer();

      virtual void pre_cache();
      virtual void build();
      virtual void progress( universe::time_type elapsed_time );
      virtual void render( scene_element_list& e ) const;

      const claw::math::coordinate_2d<unsigned int>& get_size() const;

    private:
      /** \brief The size of the layer. */
      const claw::math::coordinate_2d<unsigned int> m_size;

    }; // class gui_layer
  } // namespace engine
} // namespace bear

#endif // __ENGINE_GUI_LAYER_HPP__

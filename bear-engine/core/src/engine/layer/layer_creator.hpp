/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file layer_creator.hpp
 * \brief Base class for the proxy used to instanciate layers.
 * \author Julien Jorge
 */
#ifndef __ENGINE_LAYER_CREATOR_HPP__
#define __ENGINE_LAYER_CREATOR_HPP__

#include "universe/types.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class layer;

    /**
     * \brief Base class for layer creators, classes that instanciate layers.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT layer_creator
    {
    public:
      virtual ~layer_creator() { }

      virtual layer* create( const universe::size_box_type& s ) const = 0;

    }; // class layer_creator

  } // namespace engine
} // namespace bear

#endif // __ENGINE_LAYER_CREATOR_HPP__

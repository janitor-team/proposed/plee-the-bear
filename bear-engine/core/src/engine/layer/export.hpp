/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file export.hpp
 * \brief Macros and function useful for layer exportat.
 * \author Julien Jorge
 */
#ifndef __ENGINE_LAYER_EXPORT_HPP__
#define __ENGINE_LAYER_EXPORT_HPP__

#include "engine/layer/layer_factory.hpp"
#include "engine/layer/typed_layer_creator.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Export a function instanciating a class inheriting from
 *        engine::layer_creator.
 *
 * \param class_name The name of the layer class to instanciate.
 * \param env The full namespace of this class.
 *
 * The env::class_name class must have a constructor taking a const
 * universe::size_box_type& as parameter.
 */
#define LAYER_EXPORT( class_name, env )                                 \
  static bool class_name ## _registered =                               \
    bear::engine::layer_factory::get_instance().register_type           \
    < bear::engine::typed_layer_creator<env::class_name> >              \
  ( #class_name );

#endif // __ENGINE_LAYER_EXPORT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file layer.hpp
 * \brief A layer represent a part of the world, but with an orhtogonal view.
 *        Each layer is a little environment with its own items.
 * \author Julien Jorge
 */
#ifndef __ENGINE_LAYER_HPP__
#define __ENGINE_LAYER_HPP__

#include "concept/region.hpp"
#include "engine/base_item.hpp"
#include "universe/types.hpp"

#include <claw/math.hpp>
#include <set>

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class level;
    class world;

    /**
     * \brief A layer represent a part of the world, but with an orhtogonal
     *        view. Each layer is a little environment with its own items.
     */
    class ENGINE_EXPORT layer:
      virtual public level_object
    {
    public:
      /** \brief The type of the active area passed to the progress() method. */
      typedef concept::region<universe::rectangle_type> region_type;

    public:
      layer( const universe::size_box_type& size );
      virtual ~layer();

      const universe::size_box_type& get_size() const;

      virtual void start();

      virtual void progress
      ( const region_type& active_area, universe::time_type elapsed_time  ) = 0;

      void get_visual
      ( std::list<scene_visual>& visuals,
        const universe::rectangle_type& visible_area ) const;

      void add_item( base_item& item );
      void remove_item( base_item& item );
      void drop_item( base_item& item );

      void set_always_displayed( base_item& item );
      void unset_always_displayed( base_item& item );

      bool has_world() const;
      world& get_world();
      const world& get_world() const;

    private:
      virtual void do_add_item( base_item& item );
      virtual void do_remove_item( base_item& item );
      virtual void do_drop_item( base_item& item );

      virtual void do_get_visual
      ( std::list<scene_visual>& visuals,
        const universe::rectangle_type& visible_area ) const = 0;

      virtual world* do_get_world();
      virtual const world* do_get_world() const;

    protected:
      /** \brief Size of the layer. */
      const universe::size_box_type m_size;

    private:
      /** \brief Always displayed items. */
      std::set<base_item*> m_always_displayed;

    }; // class layer
  } // namespace engine
} // namespace bear

#endif // __ENGINE_LAYER_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_flag_type.hpp
 * \brief The type of the flags passed to a layer when inserting a new item.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_FLAG_TYPE_HPP__
#define __ENGINE_ITEM_FLAG_TYPE_HPP__

namespace bear
{
  namespace engine
  {
    /** \brief Flags that can be passed to the item method. */
    enum item_flag_type
      {
        /** \brief No flag specified. */
        item_flag_none = 0,

        /** \brief Insert the item with a definitive size and position. */
        item_flag_insert_as_static = 1L << 0,

        /** \brief The z position of the item can't change. */
        item_flag_z_fixed = 1L << 1,

        /** \brief The item has been built. */
        item_flag_built = 1L << 2

      }; // enum item_flag_type

    item_flag_type operator&(item_flag_type a, item_flag_type b);
    item_flag_type operator|(item_flag_type a, item_flag_type b);
    item_flag_type operator^(item_flag_type a, item_flag_type b);
    item_flag_type operator~(item_flag_type a);

    item_flag_type& operator&=(item_flag_type& a, item_flag_type b);
    item_flag_type& operator|=(item_flag_type& a, item_flag_type b);
    item_flag_type& operator^=(item_flag_type& a, item_flag_type b);

  } // namespace engine
} // namespace bear

#endif // __ENGINE_ITEM_FLAG_TYPE_HPP__

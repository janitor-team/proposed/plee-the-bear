/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file transition_effect_erase_message.cpp
 * \brief Implementation of the bear::engine::transition_effect_erase_message
 *        class.
 * \author Julien Jorge
 */
#include "engine/message/transition_effect_erase_message.hpp"

#include "engine/layer/transition_layer.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::transition_effect_erase_message::transition_effect_erase_message()
{
  set_id(transition_layer::not_an_id);
} // transition_effect_erase_message::transition_effect_erase_message()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param id The identifier of the effect to erase.
 */
bear::engine::transition_effect_erase_message::transition_effect_erase_message
( std::size_t id )
  : m_id(transition_layer::not_an_id)
{
  set_id(id);
} // transition_effect_erase_message::transition_effect_erase_message()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the message to thelayer.
 * \param that The layer to apply the message to.
 */
bool bear::engine::transition_effect_erase_message::apply_to
( transition_layer& that )
{
  that.erase_effect( m_id );

  return true;
} // transition_effect_erase_message::apply_to()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the identifier of the effect.
 * \param id The identifier.
 */
void bear::engine::transition_effect_erase_message::set_id( std::size_t id )
{
  m_id = id;
} // transition_effect_erase_message::set_id()

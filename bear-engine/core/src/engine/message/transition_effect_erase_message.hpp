/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file transition_effect_erase_message.hpp
 * \brief A message to be sent to the transition_layer to create a transition
 *        effect.
 * \author Julien Jorge
 */
#ifndef __BEAR_ENGINE_TRANSITION_EFFECT_ERASE_MESSAGE_HPP__
#define __BEAR_ENGINE_TRANSITION_EFFECT_ERASE_MESSAGE_HPP__

#include "communication/typed_message.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class transition_layer;

    /**
     * \brief A message to be sent to the transition_layer to erase a
     *        transition effect.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT transition_effect_erase_message:
      public communication::typed_message<transition_layer>
    {
    public:
      transition_effect_erase_message();
      transition_effect_erase_message( std::size_t id );

      bool apply_to( transition_layer& that );
      void set_id( std::size_t id );

    private:
      /** \brief The identifier returned by the layer. */
      std::size_t m_id;

    }; // class transition_effect_erase_message
  } // namespace engine
} // namespace bear

#endif // __BEAR_ENGINE_TRANSITION_EFFECT_ERASE_MESSAGE_HPP__

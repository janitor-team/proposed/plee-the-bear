/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file transition_effect_message.hpp
 * \brief A message to be sent to the transition_layer to create a transition
 *        effect.
 * \author Julien Jorge
 */
#ifndef __BEAR_ENGINE_TRANSITION_EFFECT_MESSAGE_HPP__
#define __BEAR_ENGINE_TRANSITION_EFFECT_MESSAGE_HPP__

#include "communication/typed_message.hpp"
#include "engine/layer/transition_layer.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief A message to be sent to the transition_layer to create a
     *        transition effect.
     * \author Julien Jorge
     */
    template<typename EffectType>
    class transition_effect_message:
      public communication::typed_message<transition_layer>
    {
    public:
      explicit transition_effect_message( int p = 0, bool replace = false );

      bool apply_to( transition_layer& that );
      EffectType& get_effect();
      std::size_t get_id() const;

    private:
      /** \brief The effect to add in the layer. */
      EffectType m_effect;

      /** \brief The identifier returned by the layer. */
      std::size_t m_id;

      /** \brief Tell if the new effect replaces the current ones. */
      bool m_replace;

      /** \brief The prefered position of the effect if several effects are
          played simultaneously. */
      int m_position;

    }; // class transition_effect_message

  } // namespace engine
} // namespace bear

#include "engine/message/impl/transition_effect_message.tpp"

#endif // __BEAR_ENGINE_TRANSITION_EFFECT_MESSAGE_HPP__

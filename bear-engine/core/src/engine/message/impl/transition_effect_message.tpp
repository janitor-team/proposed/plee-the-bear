/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file transition_effect_message.tpp
 * \brief Implementation of the bear::engine::transition_effect_message class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param p The prefered position of the effect if several effects are played
 *        simultaneously.
 * \param replace Tell if the new effect replaces the current ones.
 */
template<typename EffectType>
bear::engine::transition_effect_message<EffectType>::transition_effect_message
( int p, bool replace )
  : m_id(transition_layer::not_an_id), m_replace(replace), m_position(p)
{

} // transition_effect_message::transition_effect_message()

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the message to thelayer.
 * \param that The layer to apply the message to.
 */
template<typename EffectType>
bool bear::engine::transition_effect_message<EffectType>::apply_to
( transition_layer& that )
{
  if ( m_replace )
    m_id = that.set_effect( new EffectType(m_effect), m_position );
  else
    m_id = that.push_effect( new EffectType(m_effect), m_position );

  return true;
} // transition_effect_message::apply_to()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the effect.
 */
template<typename EffectType>
EffectType& bear::engine::transition_effect_message<EffectType>::get_effect()
{
  return m_effect;
} // transition_effect_message::get_effect()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the identifier of the effect added in the layer.
 */
template<typename EffectType>
std::size_t bear::engine::transition_effect_message<EffectType>::get_id() const
{
  return m_id;
} // transition_effect_message::get_id()

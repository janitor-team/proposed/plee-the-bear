/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_globals.hpp
 * \brief Some global classes in a level.
 * \author Julien Jorge
 */
#ifndef __ENGINE_LEVEL_GLOBALS_HPP__
#define __ENGINE_LEVEL_GLOBALS_HPP__

#include "audio/sound_manager.hpp"
#include "visual/animation.hpp"
#include "visual/image_manager.hpp"
#include "communication/post_office.hpp"
#include "engine/model/model_actor.hpp"
#include "visual/font.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief Some global classes in a level: the image_manager, the
     *        sound_manager and the post office.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT level_globals
    {
    public:
      level_globals();

      void load_image( const std::string& file_name );
      void load_sound( const std::string& file_name );
      void load_model( const std::string& file_name );
      void load_animation( const std::string& file_name );
      void load_font( const std::string& file_name );

      const visual::image& get_image( const std::string& name );
      const model_actor& get_model( const std::string& name );
      const visual::animation& get_animation( const std::string& name );
      visual::font get_font( const std::string& name );
      visual::sprite auto_sprite
      ( const std::string& image_name, const std::string& sprite_name );

      void play_sound( const std::string& name );
      void play_sound
      ( const std::string& name, const audio::sound_effect& effect );

      audio::sample* new_sample( const std::string& name );
      audio::sample* new_sample( const audio::sample& s );
      std::size_t play_music( const std::string& name, unsigned int loops = 0 );
      void stop_music( std::size_t id, double fadeout = 0 );
      void stop_all_musics();

      void pause_audio();
      void resume_audio();

      static void global_set_sound_muted( bool m );
      static bool global_get_sound_muted();
      static void global_set_music_muted( bool m );
      static bool global_get_music_muted();

      static void global_set_sound_volume( double m );
      static double global_get_sound_volume();
      static void global_set_music_volume( double m );
      static double global_get_music_volume();

      void set_sound_volume( double v );
      void set_music_volume( double v );

      void mute( bool m );
      void mute_sound( bool m );
      void mute_music( bool m );

      bool image_exists( const std::string& name ) const;
      bool sound_exists( const std::string& name ) const;
      bool model_exists( const std::string& name ) const;
      bool animation_exists( const std::string& name ) const;
      bool font_exists( const std::string& name ) const;

      void register_item( communication::messageable& item );
      void release_item( communication::messageable& item );
      bool send_message
      ( const std::string& target, communication::message& msg ) const;

      void set_ears_position( const claw::math::coordinate_2d<int>& position );

      void restore_images();

    private:
      /** \brief The image resources in the level. */
      visual::image_manager m_image_manager;

      /** \brief The sound resources in the level. */
      audio::sound_manager m_sound_manager;

      /** \brief The post office in the level. */
      communication::post_office m_post_office;

      /** \brief The models of the items in the level. */
      std::map<std::string, model_actor> m_model;

      /** \brief The animations in the level. */
      std::map<std::string, visual::animation> m_animation;

      /** \brief The fonts used in the level. */
      std::map<std::string, visual::font> m_font;

      /** \brief The volume of the sounds of the game. */
      static double s_sound_volume;

      /** \brief The volume of the music of the game. */
      static double s_music_volume;

      /** \brief Tell if the sounds are muted. */
      static bool s_sound_muted;

      /** \brief Tell if the music is muted. */
      static bool s_music_muted;

    }; // struct level_globals
  } // namespace engine
} // namespace bear

#endif // __ENGINE_LEVEL_GLOBALS_HPP__

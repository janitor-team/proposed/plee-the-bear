/*
  Bear Engine - Level compiler

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file compiled_file.hpp
 * \brief This class masks the kind of input (text or binary) to the level
 *        loader.
 * \author Julien Jorge
 */
#ifndef __ENGINE_COMPILED_FILE_HPP__
#define __ENGINE_COMPILED_FILE_HPP__

#include <iostream>

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief This class masks the kind of input (text or binary) to the level
     *        loader.
     */
    class ENGINE_EXPORT compiled_file
    {
    public:
      compiled_file( std::istream& f, bool text );

      compiled_file& operator>>( std::string& s );
      compiled_file& operator>>( unsigned long& i );
      compiled_file& operator>>( long& i );
      compiled_file& operator>>( unsigned int& i );
      compiled_file& operator>>( int& i );
      compiled_file& operator>>( double& i );
      compiled_file& operator>>( bool& b );

      operator bool() const;

    private:
      void input_string_as_binary( std::string& s );
      void input_string_as_text( std::string& s );

      void input_long_as_binary( long& i );
      void input_long_as_text( long& i );

      void input_unsigned_long_as_binary( unsigned long& i );
      void input_unsigned_long_as_text( unsigned long& i );

      void input_integer_as_binary( int& i );
      void input_integer_as_text( int& i );

      void input_unsigned_integer_as_binary( unsigned int& i );
      void input_unsigned_integer_as_text( unsigned int& i );

      void input_real_as_binary( double& r );
      void input_real_as_text( double& r );

      void input_bool_as_binary( bool& b );
      void input_bool_as_text( bool& b );

    private:
      /** \brief The file we are writing in. */
      std::istream& m_file;

      /** \brief Are we in text mode ? */
      bool m_text;

    }; // compiled_file
  } // namespace engine
} // namespace bear

#endif // __ENGINE_COMPILED_FILE_HPP__

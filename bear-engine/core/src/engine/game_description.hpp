/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_description.hpp
 * \brief This class loads and store informations from a game description file.
 * \author Julien Jorge
 */
#ifndef __ENGINE_GAME_DESCRIPTION_HPP__
#define __ENGINE_GAME_DESCRIPTION_HPP__

#include <string>
#include <map>
#include <list>

#include <claw/math.hpp>

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief This class loads and store informations from a game description
     *        file.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT game_description
    {
    public:
      typedef std::list<std::string> string_list;

    public:
      game_description();

      const std::string& start_level() const;
      const std::string& game_name() const;
      const claw::math::coordinate_2d<unsigned int>& screen_size() const;
      double active_area_margin() const;
      const string_list& resources_path() const;
      const string_list& libraries() const;

      void set_game_name( const std::string& value );
      void set_screen_width( unsigned int value );
      void set_screen_height( unsigned int value );
      void set_active_area_margin( unsigned int value );
      void add_resources_path( const string_list& value );
      void add_item_library( const string_list& value );
      void set_start_level( const std::string& value );

    private:
      /** \brief The name of the first level to load. */
      std::string m_start_level;

      /** \brief The name of the game. */
      std::string m_game_name;

      /** \brief The size of the screen. */
      claw::math::coordinate_2d<unsigned int> m_screen_size;

      /** \brief The margin of the active area around the screen. */
      double m_active_area_margin;

      /** \brief The paths to the forder containing the resources. */
      string_list m_resources_path;

      /** \brief The game libraries to link to. */
      string_list m_libraries;

    }; // class game_description
  } // namespace engine
} // namespace bear

#endif // __ENGINE_GAME_DESCRIPTION_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game.cpp
 * \brief Implementation of the bear::engine::game class.
 * \author Julien Jorge
 */
#include "engine/game.hpp"

#include "engine/game_local_client.hpp"
#include "engine/variable/variable_saver.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
bear::engine::game* bear::engine::game::s_instance(NULL);

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the instance of the game.
 * \pre s_instance != NULL
 */
bear::engine::game& bear::engine::game::get_instance()
{
  CLAW_PRECOND( s_instance != NULL );

  return *s_instance;
} // game::get_instance()

/*----------------------------------------------------------------------------*/
/**
 * \brief Print the options of the program.
 */
void bear::engine::game::print_help()
{
  game_local_client::print_help();
} // game::print_help()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param argc Number of program arguments.
 * \param argv Program arguments.
 */
bear::engine::game::game( int& argc, char** &argv )
{
  CLAW_PRECOND( s_instance == NULL );
  s_instance = this;

  m_game = new game_local_client(argc, argv);
} // game::game()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::engine::game::~game()
{
  delete m_game;
  s_instance = NULL;
} // game::~game()

/*----------------------------------------------------------------------------*/
/**
 * \brief Run the game.
 */
void bear::engine::game::run()
{
  m_game->run();
} // game::run()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the time step between to progress on the level.
 */
bear::systime::milliseconds_type bear::engine::game::get_time_step() const
{
  return m_game->get_time_step();
} // game::get_time_step()

/*----------------------------------------------------------------------------*/
/**
 * \brief Turn on/off the full screen mode.
 * \param b Tell if we must activate the fullscreen mode.
 */
void bear::engine::game::set_fullscreen( bool full )
{
  m_game->set_fullscreen(full);
} // game::set_fullscreen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the full screen mode.
 */
bool bear::engine::game::get_fullscreen() const
{
  return m_game->get_fullscreen();
} // game::get_fullscreen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Toggle the status of the fullscreen mode.
 */
void bear::engine::game::toggle_fullscreen()
{
  set_fullscreen( !get_fullscreen() );
} // game::toggle_fullscreen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the sounds.
 * \param m The mute status.
 */
void bear::engine::game::set_sound_muted( bool m )
{
  m_game->set_sound_muted(m);
} // game::set_sound_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the sound is muted.
 */
bool bear::engine::game::get_sound_muted() const
{
  return m_game->get_sound_muted();
} // game::get_sound_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Change the "mute" status of the sounds.
 */
void bear::engine::game::toggle_sound_muted()
{
  set_sound_muted( !get_sound_muted() );
} // game::toggle_sound_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the sounds.
 * \param v The volume.
 */
void bear::engine::game::set_sound_volume( double v )
{
  m_game->set_sound_volume(v);
} // game::set_sound_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the volume of the sounds.
 */
double bear::engine::game::get_sound_volume() const
{
  return m_game->get_sound_volume();
} // game::get_sound_colume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the music.
 * \param m The mute status.
 */
void bear::engine::game::set_music_muted( bool m )
{
  m_game->set_music_muted(m);
} // game::set_music_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the music is muted.
 */
bool bear::engine::game::get_music_muted() const
{
  return m_game->get_music_muted();
} // game::get_music_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Change the "mute" status of the music.
 */
void bear::engine::game::toggle_music_muted()
{
  set_music_muted( !get_music_muted() );
} // game::toggle_music_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the musics.
 * \param v The volume.
 */
void bear::engine::game::set_music_volume( double v )
{
  m_game->set_music_volume(v);
} // game::set_music_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the volume of the musics.
 */
double bear::engine::game::get_music_volume() const
{
  return m_game->get_music_volume();
} // game::get_music_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a shot of the screen.
 * \param img The image in which we save the screen.
 */
void bear::engine::game::screenshot( claw::graphic::image& img ) const
{
  m_game->screenshot( img );
} // game::screenshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a shot of the whole level.
 * \param img The image in which we save the level.
 */
void bear::engine::game::levelshot( claw::graphic::image& img ) const
{
  m_game->levelshot( img );
} // game::levelshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief End the game.
 */
void bear::engine::game::end()
{
  m_game->end();
} // game::end()

#include "engine/variable/variable_list_reader.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Give the name of a level to load as soon as possible.
 * \param path The path of the level to load.
 */
void bear::engine::game::set_waiting_level( const std::string& path )
{
  m_game->set_waiting_level(path);
} // game::set_waiting_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the level to run as soon as possible.
 * \param the_level The level to run as soon as possible.
 */
void bear::engine::game::set_waiting_level( level* the_level )
{
  m_game->set_waiting_level(the_level);
} // game::set_waiting_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give the name of a level to load as soon as possible but keep the
 *        current level in memory for future restoration.
 * \param path The path of the level to load.
 */
void bear::engine::game::push_level( const std::string& path )
{
  m_game->push_level(path);
} // game::push_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Restore the level at the top of the stack.
 */
void bear::engine::game::pop_level()
{
  m_game->pop_level();
} // game::pop_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size of the screen.
 */
const claw::math::coordinate_2d<unsigned int>&
bear::engine::game::get_screen_size() const
{
  return m_game->get_screen_size();
} // game::get_screen_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the margin of the active area around the camera.
 */
double bear::engine::game::get_active_area_margin() const
{
  return m_game->get_active_area_margin();
} // game::get_active_area_margin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the path of a file in the user's game directory.
 * \param name The name of the file.
 */
std::string
bear::engine::game::get_custom_game_file( const std::string& name ) const
{
  return m_game->get_custom_game_file(name);
} // game::get_full_user_path()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the value of a global variable.
 * \param val (in/out) The variable for which we want the value.
 */
void bear::engine::game::get_game_variable( base_variable& val ) const
{
  m_game->get_game_variable(val);
} // game::get_game_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the game variables whose name match a given regular
 *        expression;
 * \param vars The list of matched game variables.
 * \param pattern The expression that has to be matched by the variable names.
 */
void bear::engine::game::get_game_variables
( var_map& vars, const std::string& pattern )
{
  m_game->get_game_variables(vars, pattern);
} // game::get_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of a global variable.
 * \param val (in/out) The variable for which we set the value.
 */
void bear::engine::game::set_game_variable( const base_variable& val )
{
  m_game->set_game_variable(val);
} // game::set_game_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set several game variables at once.
 * \param vars The variables.
 */
void bear::engine::game::set_game_variables( const var_map& vars )
{
  m_game->set_game_variables(vars);
} // game::set_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Erase the variables whose name match a given pattern.
 * \param pattern The pattern to match.
 */
void bear::engine::game::erase_game_variables( const std::string& pattern )
{
  m_game->erase_game_variables(pattern);
} // game::erase_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if a given variable exists in the game.
 * \param val (in/out) The variable to check.
 */
bool bear::engine::game::game_variable_exists( const base_variable& val ) const
{
  return m_game->game_variable_exists(val);
} // game::game_variable_exists()

/*----------------------------------------------------------------------------*/
/**
 * \brief Save all the game variables whose name match a given regular
 *        expression;
 * \param os The stream in which the variables are saved.
 * \param pattern The expression that has to be matched by the variable names.
 */
void bear::engine::game::save_game_variables
( std::ostream& os, const std::string& pattern )
{
  var_map vars;
  m_game->get_all_game_variables(vars);

  vars.for_each( variable_saver(os, boost::regex(pattern)) );
} // game::save_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type int.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game::listen_int_variable_change
( const std::string& name, const boost::function<void (int)>& f )
{
  return m_game->listen_int_variable_change( name, f );
} // game::listen_int_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type
 *        unsigned int.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game::listen_uint_variable_change
( const std::string& name, const boost::function<void (unsigned int)>& f )
{
  return m_game->listen_uint_variable_change( name, f );
} // game::listen_uint_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type bool.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game::listen_bool_variable_change
( const std::string& name, const boost::function<void (bool)>& f )
{
  return m_game->listen_bool_variable_change( name, f );
} // game::listen_bool_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type double.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game::listen_double_variable_change
( const std::string& name, const boost::function<void (double)>& f )
{
  return m_game->listen_double_variable_change( name, f );
} // game::listen_double_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type string.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game::listen_string_variable_change
( const std::string& name, const boost::function<void (std::string)>& f )
{
  return m_game->listen_string_variable_change( name, f );
} // game::listen_string_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the game.
 */
const std::string& bear::engine::game::get_name() const
{
  return m_game->get_name();
} // game::get_name()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_flag_type.cpp
 * \brief Implementation of the methods for the bear::engine::item_flag_type
 *        type.
 * \author Julien Jorge
 */
#include "engine/item_flag_type.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Operator "&" for item_flag_type.
 * \param a Left operand.
 * \param b Right operand.
 */
bear::engine::item_flag_type
bear::engine::operator&
(bear::engine::item_flag_type a, bear::engine::item_flag_type b)
{
  return item_flag_type(static_cast<int>(a) & static_cast<int>(b));
} // operator&() [item_flag_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Operator "|" for item_flag_type.
 * \param a Left operand.
 * \param b Right operand.
 */
bear::engine::item_flag_type
bear::engine::operator|
(bear::engine::item_flag_type a, bear::engine::item_flag_type b)
{
  return item_flag_type(static_cast<int>(a) | static_cast<int>(b));
} // operator|() [item_flag_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Operator "^" for item_flag_type.
 * \param a Left operand.
 * \param b Right operand.
 */
bear::engine::item_flag_type
bear::engine::operator^
(bear::engine::item_flag_type a, bear::engine::item_flag_type b)
{
  return item_flag_type(static_cast<int>(a) ^ static_cast<int>(b));
} // operator^() [item_flag_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Operator "~" for item_flag_type.
 * \param a Operand.
 */
bear::engine::item_flag_type
bear::engine::operator~(bear::engine::item_flag_type a)
{
  return item_flag_type(~static_cast<int>(a));
} // operator~() [item_flag_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Operator "&=" for item_flag_type.
 * \param a Left operand.
 * \param b Right operand.
 */
bear::engine::item_flag_type&
bear::engine::operator&=
(bear::engine::item_flag_type& a, bear::engine::item_flag_type b)
{
  return a = a & b;
} // operator&=() [item_flag_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Operator "|=" for item_flag_type.
 * \param a Left operand.
 * \param b Right operand.
 */
bear::engine::item_flag_type&
bear::engine::operator|=
(bear::engine::item_flag_type& a, bear::engine::item_flag_type b)
{
  return a = a | b;
} // operator|=() [item_flag_type]

/*----------------------------------------------------------------------------*/
/**
 * \brief Operator "^=" for item_flag_type.
 * \param a Left operand.
 * \param b Right operand.
 */
bear::engine::item_flag_type&
bear::engine::operator^=
(bear::engine::item_flag_type& a, bear::engine::item_flag_type b)
{
  return a = a ^ b;
} // operator^=() [item_flag_type]

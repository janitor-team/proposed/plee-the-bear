/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_globals.cpp
 * \brief Implementation of the bear::engine::level_globals class.
 * \author Julien Jorge
 */
#include "engine/level_globals.hpp"

#include "engine/bitmap_font_loader.hpp"
#include "engine/model_loader.hpp"
#include "engine/resource_pool.hpp"
#include "engine/sprite_loader.hpp"

#include <sstream>
#include <cassert>
#include <claw/logger.hpp>
#include <claw/exception.hpp>
#include <claw/string_algorithm.hpp>

/*----------------------------------------------------------------------------*/
double bear::engine::level_globals::s_sound_volume(1);
double bear::engine::level_globals::s_music_volume(1);
bool bear::engine::level_globals::s_sound_muted(false);
bool bear::engine::level_globals::s_music_muted(false);

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::level_globals::level_globals()
{
  if (s_sound_muted)
    m_sound_manager.set_sound_volume(0);
  else
    m_sound_manager.set_sound_volume(s_sound_volume);

  if (s_music_muted)
    m_sound_manager.set_music_volume(0);
  else
    m_sound_manager.set_music_volume(s_music_volume);
} // level_globals::level_globals()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load an image.
 * \param file_name The name of the file to load the image from.
 */
void bear::engine::level_globals::load_image( const std::string& file_name )
{
  if ( !m_image_manager.exists(file_name) )
    {
      claw::logger << claw::log_verbose << "loading image '" << file_name
                   << "'." << std::endl;

      std::stringstream f;
      resource_pool::get_instance().get_file(file_name, f);

      if (f)
        m_image_manager.load_image(file_name, f);
      else
        claw::logger << claw::log_error << "can not open file '" << file_name
                     << "'." << std::endl;
    }
} // level_globals::load_image()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a sound.
 * \param file_name The name of the file to load the sound from.
 */
void bear::engine::level_globals::load_sound( const std::string& file_name )
{
  if ( !m_sound_manager.sound_exists(file_name) )
    {
      claw::logger << claw::log_verbose << "loading sound '" << file_name
                   << "'." << std::endl;

      std::stringstream f;
      resource_pool::get_instance().get_file(file_name, f);

      if (f)
        m_sound_manager.load_sound(file_name, f);
      else
        claw::logger << claw::log_error << "can not open file '" << file_name
                     << "'." << std::endl;
    }
} // level_globals::load_sound()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a model.
 * \param file_name The name of the file to load the model from.
 */
void bear::engine::level_globals::load_model( const std::string& file_name )
{
  if ( !model_exists(file_name) )
    {
      claw::logger << claw::log_verbose << "loading model '" << file_name
                   << "'." << std::endl;

      std::stringstream f;
      resource_pool::get_instance().get_file(file_name, f);

      if (f)
        {
          model_loader ldr( f, *this );
          model_actor* m = ldr.run();
          m_model[file_name] = *m;
          delete m;
        }
      else
        claw::logger << claw::log_error << "can not open file '" << file_name
                     << "'." << std::endl;
    }
} // level_globals::load_model()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load an animation.
 * \param file_name The name of the file to load the animation from.
 */
void bear::engine::level_globals::load_animation( const std::string& file_name )
{
  if ( !animation_exists(file_name) )
    {
      claw::logger << claw::log_verbose << "loading animation '" << file_name
                   << "'." << std::endl;

      std::stringstream f;
      resource_pool::get_instance().get_file(file_name, f);

      if (f)
        {
          compiled_file cf(f, true);
          sprite_loader ldr;
          m_animation[file_name] = ldr.load_animation( cf, *this );
        }
      else
        claw::logger << claw::log_error << "can not open file '" << file_name
                     << "'." << std::endl;
    }
} // level_globals::load_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a font.
 * \param file_name Path to the font file.
 * \todo Implement the support of true type fonts.
 */
void bear::engine::level_globals::load_font( const std::string& file_name )
{
  if ( !font_exists(file_name) )
    {
      claw::logger << claw::log_verbose << "loading font '" << file_name
                   << "'." << std::endl;

      std::stringstream f;
      resource_pool::get_instance().get_file(file_name, f);

      if (f)
        {
          bitmap_font_loader ldr( f, *this );
          m_font[file_name] = ldr.run();
        }
      else
        claw::logger << claw::log_error << "can not open file '" << file_name
                     << "'." << std::endl;
    }
} // level_globals::load_font()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an image.
 * \param name The name of the image to get.
 * \pre There is an image named \a name.
 */
const bear::visual::image&
bear::engine::level_globals::get_image( const std::string& name )
{
  if ( !m_image_manager.exists(name) )
    load_image(name);

  return m_image_manager.get_image(name);
} // level_globals::get_image()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a model.
 * \param name The name of the model to get.
 */
const bear::engine::model_actor&
bear::engine::level_globals::get_model( const std::string& name )
{
  if ( !model_exists(name) )
    load_model(name);

  return m_model[name];
} // level_globals::get_model()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an animation.
 * \param name The name of the animation to get.
 */
const bear::visual::animation&
bear::engine::level_globals::get_animation( const std::string& name )
{
  if ( !animation_exists(name) )
    load_animation(name);

  return m_animation[name];
} // level_globals::get_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a font.
 * \param name The name of the resource.
 */
bear::visual::font
bear::engine::level_globals::get_font( const std::string& name )
{
  if ( !font_exists(name) )
    load_font(name);

  return m_font[name];
} // level_globals::get_font()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a sprite by reading its position and its size in the spritepos
 *        file associated with the image.
 * \param image_name The name of the image.
 * \param sprite_name The name of the sprite in the spritepos file.
 * \remark The spritepos file in opened and read until the sprite is found. It
 *         means that this method is relatively slow and you should avoid
 *         calling it repetitively.
 */
bear::visual::sprite bear::engine::level_globals::auto_sprite
( const std::string& image_name, const std::string& sprite_name )
{
  const visual::image& img( get_image(image_name) );
  visual::sprite result;
  std::size_t pos( image_name.find_last_of('.') );

  if ( pos != std::string::npos )
    {
      std::stringstream f;
      resource_pool::get_instance().get_file
        ( image_name.substr(0, pos) + ".spritepos", f );

      if (f)
        {
          std::string line;
          std::string coord;

          while ( claw::text::getline(f, line) && coord.empty() )
            if ( !line.empty() )
              if ( line[0] != '#' )
                {
                  pos = line.find_first_of(':');
                  std::string n( line.substr(0, pos) );
                  claw::text::trim(n);

                  if ( n == sprite_name )
                    coord = line.substr(pos+1);
                }

          std::istringstream iss(coord);
          claw::math::rectangle<unsigned int> r;

          if ( iss >> r.position.x >> r.position.y >> r.width >> r.height )
            result = visual::sprite( img, r );
          else
            claw::logger << claw::log_error << "can not find a valid sprite '"
                         << sprite_name << "' in the spritepos file of '"
                         << image_name << "'." << std::endl;
        }
      else
        claw::logger << claw::log_error << "can not open spritepos file for '"
                     << image_name << "'." << std::endl;
    }

  return result;
} // level_globals::auto_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start to play a sound.
 * \param name The name of the sound to play.
 */
void bear::engine::level_globals::play_sound( const std::string& name )
{
  if ( !m_sound_manager.sound_exists(name) )
    load_sound(name);

  m_sound_manager.play_sound( name );
} // level_globals::play_sound()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start to play the sound, with an effect.
 * \param name The name of the sound to play.
 * \param effect The effect applied to the sound.
 */
void bear::engine::level_globals::play_sound
( const std::string& name, const audio::sound_effect& effect )
{
  if ( !m_sound_manager.sound_exists(name) )
    load_sound(name);

  m_sound_manager.play_sound( name, effect );
} // level_globals::play_sound()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a new sample of a sound.
 * \param name The name of the sound to get.
 */
bear::audio::sample*
bear::engine::level_globals::new_sample( const std::string& name )
{
  if ( !m_sound_manager.sound_exists(name) )
    load_sound(name);

  return m_sound_manager.new_sample(name);
} // level_globals::new_sample()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of a sample.
 * \param s The sample to copy.
 */
bear::audio::sample*
bear::engine::level_globals::new_sample( const audio::sample& s )
{
  return m_sound_manager.new_sample(s);
} // level_globals::new_sample()

/*----------------------------------------------------------------------------*/
/**
 * \brief Play a music.
 * \param name The name of the music to play.
 * \param loops How many times the music loops (zero means infinite).
 * \return The identifier of the music.
 * \remark identifiers are strictly greater than zero.
 */
std::size_t bear::engine::level_globals::play_music
( const std::string& name, unsigned int loops )
{
  if ( !m_sound_manager.sound_exists(name) )
    load_sound(name);

  return m_sound_manager.play_music(name, loops);
} // level_globals::play_music()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop the music.
 * \param id The identifier of the music.
 * \param fadeout The duration of the fadeout, if any.
 */
void bear::engine::level_globals::stop_music( std::size_t id, double fadeout )
{
  m_sound_manager.stop_music(id, fadeout);
} // level_globals::stop_music()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop all musics.
 */
void bear::engine::level_globals::stop_all_musics()
{
  m_sound_manager.stop_all_musics();
} // level_globals::stop_all_musics()

/*----------------------------------------------------------------------------*/
/**
 * \brief Pause all music and sounds.
 */
void bear::engine::level_globals::pause_audio()
{
  m_sound_manager.pause_all();
} // level_globals::pause_audio()

/*----------------------------------------------------------------------------*/
/**
 * \brief Resume all music and sounds.
 */
void bear::engine::level_globals::resume_audio()
{
  m_sound_manager.resume_all();
} // level_globals::resume_audio()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the sounds.
 * \param m The mute status.
 */
void bear::engine::level_globals::global_set_sound_muted( bool m )
{
  s_sound_muted = m;
} // level_globals::global_set_sound_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the sound is muted.
 */
bool bear::engine::level_globals::global_get_sound_muted()
{
  return s_sound_muted;
} // level_globals::global_get_sound_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the sounds.
 * \param v The volume.
 */
void bear::engine::level_globals::global_set_sound_volume( double v )
{
  s_sound_volume = v;
} // level_globals::global_set_sound_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the volume of the sounds.
 */
double bear::engine::level_globals::global_get_sound_volume()
{
  return s_sound_volume;
} // level_globals::global_get_sound_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the music.
 * \param m The mute status.
 */
void bear::engine::level_globals::global_set_music_muted( bool m )
{
  s_music_muted = m;
} // level_globals::global_set_music_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the music is muted.
 */
bool bear::engine::level_globals::global_get_music_muted()
{
  return s_music_muted;
} // level_globals::global_get_music_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the musics.
 * \param v The volume.
 */
void bear::engine::level_globals::global_set_music_volume( double v )
{
  s_music_volume = v;
} // level_globals::global_set_music_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the volume of the musics.
 */
double bear::engine::level_globals::global_get_music_volume()
{
  return s_music_volume;
} // level_globals::global_get_music_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the sounds.
 * \param v The new volume.
 */
void bear::engine::level_globals::set_sound_volume( double v )
{
  global_set_sound_volume(v);

  if (!s_sound_muted)
    m_sound_manager.set_sound_volume(v);
} // level_globals::set_sound_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the music.
 * \param v The new volume.
 */
void bear::engine::level_globals::set_music_volume( double v )
{
  global_set_music_volume(v);

  if (!s_music_muted)
    m_sound_manager.set_music_volume(v);
} // level_globals::set_music_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the sound and the music.
 * \param m The mute status.
 */
void bear::engine::level_globals::mute( bool m )
{
  mute_sound(m);
  mute_music(m);
} // level_globals::mute()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the sounds.
 * \param m The mute status.
 */
void bear::engine::level_globals::mute_sound( bool m )
{
  global_set_sound_muted(m);

  if ( global_get_sound_muted() )
    m_sound_manager.set_sound_volume(0);
  else
    m_sound_manager.set_sound_volume( global_get_sound_volume() );
} // level_globals::mute_sound()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the music.
 * \param m The mute status.
 */
void bear::engine::level_globals::mute_music( bool m )
{
  global_set_music_muted(m);

  if ( global_get_music_muted() )
    m_sound_manager.set_music_volume(0);
  else
    m_sound_manager.set_music_volume( global_get_music_volume() );
} // level_globals::mute_music()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is an image with a given name.
 * \param name The name of the image to check.
 */
bool bear::engine::level_globals::image_exists( const std::string& name ) const
{
  return m_image_manager.exists(name)
    || resource_pool::get_instance().exists(name);
} // level_globals::image_exists()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is a sound with a given name.
 * \param name The name of the sound to check.
 */
bool bear::engine::level_globals::sound_exists( const std::string& name ) const
{
  return m_sound_manager.sound_exists(name)
    || resource_pool::get_instance().exists(name);
} // level_globals::sound_exists()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is a model with a given name.
 * \param name The name of the model to check.
 */
bool bear::engine::level_globals::model_exists( const std::string& name ) const
{
  return m_model.find(name) != m_model.end();
} // level_globals::model_exists()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is an animation with a given name.
 * \param name The name of the animation to check.
 */
bool
bear::engine::level_globals::animation_exists( const std::string& name ) const
{
  return m_animation.find(name) != m_animation.end();
} // level_globals::animation_exists()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is a font with a given name.
 * \param name The name of the font to check.
 */
bool bear::engine::level_globals::font_exists( const std::string& name ) const
{
  return m_font.find(name) != m_font.end();
} // level_globals::font_exists()

/*----------------------------------------------------------------------------*/
/**
 * \brief Register an item in the post office.
 * \param item The item to register.
 */
void bear::engine::level_globals::register_item
( communication::messageable& item )
{
  m_post_office.register_item( &item );
} // level_globals::register_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Release an item from the post office.
 * \param item The item to release.
 */
void bear::engine::level_globals::release_item
( communication::messageable& item )
{
  m_post_office.release_item( &item );
} // level_globals::release_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Send a message to an item via the post office.
 * \param name The name of the item to contact.
 * \param msg The message to send to this item.
 */
bool bear::engine::level_globals::send_message
( const std::string& target, communication::message& msg ) const
{
  return m_post_office.send_message( target, msg );
} // level_globals::send_message()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the position of the ears.
 * \param position The new position.
 */
void bear::engine::level_globals::set_ears_position
( const claw::math::coordinate_2d<int>& position )
{
  m_sound_manager.set_ears_position(position);
} // level_globals::set_ears_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Reload thg images.
 */
void bear::engine::level_globals::restore_images()
{
  std::vector<std::string> names;

  m_image_manager.get_image_names(names);
  m_image_manager.clear_images();

  for (unsigned int i=0; i!=names.size(); ++i)
    {
      claw::logger << claw::log_verbose << "restoring image '" << names[i]
                   << "'." << std::endl;

      std::stringstream f;
      resource_pool::get_instance().get_file(names[i], f);

      if (f)
        m_image_manager.restore_image(names[i], f);
      else
        claw::logger << claw::log_error << "can not open file '" << names[i]
                     << "'." << std::endl;
    }
} // level_globals::restore_images()


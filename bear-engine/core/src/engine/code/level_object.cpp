/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_object.cpp
 * \brief Implementation of the bear::engine::level_object class.
 * \author Julien Jorge.
 */
#include "engine/level_object.hpp"

#include "engine/layer/layer.hpp"
#include "engine/level.hpp"
#include "engine/level_globals.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::engine::level_object::level_object()
  : m_level(NULL)
{

} // level_object::level_object()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy contructor.
 * \param that The instance to copy from.
 */
bear::engine::level_object::level_object( const level_object& that )
  : m_level(NULL)
{
  // just ignore
} // level_object::level_object()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::engine::level_object::~level_object()
{
  // do nothing
} // level_object::~level_object()

/*----------------------------------------------------------------------------*/
/**
 * \brief Remove the data related to the level containing the object.
 * \post m_level == NULL
 */
void bear::engine::level_object::clear_level()
{
  m_level = NULL;
} // level_object::clear_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the level in which this object is.
 * \param the_level The level in which we are.
 * \pre m_level == NULL
 */
void bear::engine::level_object::set_level( level& the_level )
{
  CLAW_PRECOND( (m_level == NULL) || (m_level == &the_level) );

  m_level = &the_level;
} // level_object::set_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the level in which this item is.
 * \pre m_level != NULL
 */
bear::engine::level& bear::engine::level_object::get_level() const
{
  CLAW_PRECOND( m_level != NULL );

  return *m_level;
} // level_object::get_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the level_globals of the level in which this item is.
 * \pre m_level != NULL
 */
bear::engine::level_globals&
bear::engine::level_object::get_level_globals() const
{
  CLAW_PRECOND( m_level != NULL );

  return m_level->get_globals();
} // level_object::get_level_globals()

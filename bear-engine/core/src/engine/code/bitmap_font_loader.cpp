/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bitmap_font_loader.cpp
 * \brief Implementation of the bitmap_font_loader class.
 * \author Julien Jorge
 */
#include "engine/bitmap_font_loader.hpp"

#include "engine/level_globals.hpp"

#include <claw/exception.hpp>

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param f The file from which we load the file.
 * \param glob The level_globals in which we load the resources.
 */
bear::engine::bitmap_font_loader::bitmap_font_loader
( std::istream& f, level_globals& glob )
  : m_file(f), m_level_globals(glob)
{

} // bitmap_font_loader::bitmap_font_loader()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the font.
 */
bear::visual::font bear::engine::bitmap_font_loader::run()
{
  visual::bitmap_font::symbol_table cs;
  unsigned int n = 0;
  cs.size.x = 0;
  cs.size.y = 0;

  std::istringstream parser(get_next_line());

  parser >> cs.size.x >> cs.size.y;

  parser.clear();
  parser.str( get_next_line() );

  if( parser >> n )
    cs.font_images.reserve(n);

  for ( std::size_t i=0; i!=n; ++i )
    cs.font_images.push_back( m_level_globals.get_image(get_next_line()) );

  std::string line = get_next_line();
  while ( !line.empty() )
    {
      unsigned int x, y;
      std::size_t i;
      char c;
      parser.clear();
      parser.str(line);

      if ( (parser.get(c) >> x >> y >> i) && (i < n) )
        {
          cs.characters[c].image_index = i;
          cs.characters[c].position.set(x, y);
        }

      line = get_next_line();
    }

  if ((cs.font_images.size() == n) && m_file.eof())
    return new visual::bitmap_font(cs);
  else
    throw claw::exception( "Bad font" );
} // bitmap_font_loader::run()

/*----------------------------------------------------------------------------*/
/**
 * Get the next non empty line from the file, if any.
 */
std::string bear::engine::bitmap_font_loader::get_next_line() const
{
  std::string result;

  while ( result.empty() && getline(m_file, result) );

  return result;
} // bitmap_font_loader::get_next_line()

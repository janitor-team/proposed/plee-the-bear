/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_stats.cpp
 * \brief Implementation of the bear::engine::game_stats class.
 * \author Julien Jorge
 */
#include "engine/game_stats.hpp"

#include "engine/version.hpp"

#include <claw/logger.hpp>
#include <claw/socket_stream.hpp>
#include <claw/system_info.hpp>

#include <iomanip>
#include <sstream>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::game_stats::game_stats()
  : m_init_date(systime::get_unix_time())
{

} // game_stats::game_stats()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the destination to which the stats are sent.
 * \param destination The destination.
 */
void bear::engine::game_stats::set_destination( const std::string& destination )
{
  m_destination = destination;
} // game_stats::set_destination()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the identifier of the user to use in the stats.
 * \param id The identifier.
 */
void bear::engine::game_stats::set_user_id( const std::string& id )
{
  m_user_id = id;
} // game_stats::set_user_id()

/*----------------------------------------------------------------------------*/
/**
 * \brief Send the stats to the destination set with set_destination.
 */
void bear::engine::game_stats::send() const
{
  const std::string prefix("http://");

  if ( m_destination.find(prefix) != 0 )
    return;

  http_post( m_destination.substr(prefix.length()) );
} // game_stats::send()

/*----------------------------------------------------------------------------*/
/**
 * \brief Send the stats to a given server via HTTP POST.
 * \param address The address of the server to which the stats are sent.
 */
void bear::engine::game_stats::http_post( const std::string& address ) const
{
  const std::string xml_string(generate_xml_stats());
  std::string page;
  std::string server(address);

  std::size_t pos = server.find_first_of('/');

  if ( pos != std::string::npos )
    {
      page = server.substr( pos );
      server = server.substr( 0, pos );
    }

  const int port(80);
  claw::net::socket_stream os( server.c_str(), port );

  if ( !os )
    claw::logger << claw::log_error << "Cannot connect to " << server
                 << " on port " << port << ".";
  else
    {
      claw::logger << claw::log_verbose << "Connected to " << server
                   << ", requesting " << page << std::endl;

      os << "POST " << page << " HTTP/1.1\n"
         << "Host: " << server << "\n"
         << "From: stats@gamned.org\n"
         << "Content-Length: " << xml_string.size() << '\n'
         << "Content-Type: application/xml\n"
         << '\n'
         << xml_string
         << std::flush;
    }
} // game_stats::http_post()

/*----------------------------------------------------------------------------*/
/**
 * \brief Generate an XML representation of the stats.
 */
std::string bear::engine::game_stats::generate_xml_stats() const
{
  std::ostringstream result;

  result << "<?xml version=\"1.0\"?>"
         << "<bear-stats engine-version-major='" << BEAR_MAJOR_VERSION << "' "
         << "engine-version-minor='" << BEAR_MINOR_VERSION << "' "
         << "engine-version-release='" << BEAR_RELEASE_NUMBER << "' "
         << "user-id='" << m_user_id << "' "
         << "init-time='" << m_init_date << "' "
         << "current-time='" << systime::get_unix_time() << "' "
#ifdef _WIN32
         << "build='win32' "
#else
         << "build='unix' "
#endif
         << ">"
         << "</bear-stats>";

  return result.str();
} // game_stats::generate_xml_stats()

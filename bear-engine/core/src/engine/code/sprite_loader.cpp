/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sprite_loader.cpp
 * \brief Implementation of the bear::engine::sprite_loader class.
 * \author Julien Jorge
 */
#include "engine/sprite_loader.hpp"

#include "engine/compiled_file.hpp"
#include "engine/level_globals.hpp"

#include <claw/exception.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a sprite.
 * \param f The file from which we will read the sprite.
 * \param glob The level globals from which we take the images.
 */
bear::visual::sprite bear::engine::sprite_loader::load_sprite
( compiled_file& f, level_globals& glob )
{
  std::string image_name;
  claw::math::rectangle<unsigned int> clip;
  claw::math::coordinate_2d<unsigned int> size;

  f >> image_name >> clip.position.x >> clip.position.y >> clip.width
    >> clip.height;

  const visual::image& resource = glob.get_image(image_name);
  visual::sprite result( resource, clip );

  load_bitmap_rendering_attributes(f, result);

  return result;
} // sprite_loader::load_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load an animation.
 * \param f The file from which we will read the animation.
 * \param glob The level globals from which we take the images.
 */
bear::visual::animation bear::engine::sprite_loader::load_animation
( compiled_file& f, level_globals& glob )
{
  unsigned int maj(0), min(0), rel(0);

  f >> maj >> min >> rel;

  if ( (maj != 0) || (min <= 4) )
    throw claw::exception
      ( "This version of the animation file is not supported." );
  else
    return load_animation_v0_5(f, glob);
} // sprite_loader::load_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load any animation.
 * \param f The file from which we will read the animation.
 * \param glob The level globals from which we take the images.
 *
 * This function expects the content of f to be wether a complete animation or
 * a path to an animation file, with extra rendering attributes in the latter
 * case.
 *
 */
bear::visual::animation bear::engine::sprite_loader::load_any_animation
( compiled_file& f, level_globals& glob )
{
  std::string content_type;
  visual::animation result;

  f >> content_type;

  if ( content_type == "content_file" )
    {
      std::string path;
      f >> path;
      result = glob.get_animation(path);
      load_bitmap_rendering_attributes(f, result);
    }
  else if ( content_type == "content_animation" )
    result = load_animation_data(f, glob);
  else
    throw claw::exception
      ( "Unknown animation content type: '" + content_type + "'." );

  return result;
} // sprite_loader::load_any_animation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load an animation.
 * \param f The file from which we will read the animation.
 * \param glob The level globals from which we take the images.
 */
bear::visual::animation bear::engine::sprite_loader::load_animation_data
( compiled_file& f, level_globals& glob )
{
  return load_animation_v0_5(f, glob);
} // sprite_loader::load_animation_data()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a bitmap_rendering_attributes.
 * \param f The file from which we will read the attributes.
 * \param result The attributes to set.
 */
void bear::engine::sprite_loader::load_bitmap_rendering_attributes
( compiled_file& f, visual::bitmap_rendering_attributes& result )
{
  claw::math::coordinate_2d<unsigned int> size;
  bool flip, mirror;
  double alpha;
  double r, g, b;
  double angle;

  f >> size.x >> size.y >> mirror >> flip >> alpha >> r >> g >> b >> angle;

  result.flip( flip );
  result.mirror( mirror );
  result.set_opacity( alpha );
  result.set_size( size );
  result.set_intensity( r, g, b );
  result.set_angle(angle);
} // sprite_loader::load_bitmap_rendering_attributes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load an animation.
 * \param f The file from which we will read the animation.
 * \param glob The level globals from which we take the images.
 */
bear::visual::animation bear::engine::sprite_loader::load_animation_v0_5
( compiled_file& f, level_globals& glob )
{
  unsigned int frames_count;

  f >> frames_count;

  std::vector<visual::sprite> frames(frames_count);
  std::vector<double> duration(frames_count);

  for ( unsigned int i=0; i!=frames_count; ++i)
    {
      f >> duration[i];
      frames[i] = load_sprite(f, glob);
    }

  unsigned int loops, first_index, last_index;
  bool loop_back;

  f >> loops >> loop_back >> first_index >> last_index;

  visual::animation result( frames, duration );

  load_bitmap_rendering_attributes(f, result);

  result.set_loops( loops );
  result.set_loop_back( loop_back );
  result.set_first_index(first_index);
  result.set_last_index(last_index);

  return result;
} // sprite_loader::load_animation_v0_5()

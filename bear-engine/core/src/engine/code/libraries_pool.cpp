/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file libraries_pool.cpp
 * \brief Implementation of the bear::engine::libraries_pool class.
 * \author Julien Jorge
 */
#include <algorithm>
#include "engine/libraries_pool.hpp"

#include <claw/functional.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::engine::libraries_pool::~libraries_pool()
{
  std::for_each( m_libraries.begin(), m_libraries.end(),
     claw::delete_function<claw::dynamic_library*>() );
} // libraries_pool::~libraries_pool()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a library in the pool.
 * \param name The name of the library.
 * \param current_program Tell if \a name is the current program.
 */
void bear::engine::libraries_pool::add_library
( const std::string& name, bool current_program )
{
  m_libraries.push_front( new claw::dynamic_library(name, current_program) );
} // libraries_pool::add_library()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if one of the libraries has a given symbol.
 * \param symbol The symbol to find.
 */
bool
bear::engine::libraries_pool::have_symbol( const std::string& symbol ) const
{
  bool result=false;
  libraries_list::const_iterator it;

  for (it=m_libraries.begin(); !result && (it!=m_libraries.end()); ++it)
    result = (*it)->have_symbol( symbol );

  return result;
} // libraries_pool::have_symbol()

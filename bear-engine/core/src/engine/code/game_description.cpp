/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_description.hpp
 * \brief Implementation of the bear::engine::game_description class.
 * \author Julien Jorge
 */
#include "engine/game_description.hpp"

#include <string>
#include <vector>

#include <claw/assert.hpp>
#include <claw/logger.hpp>
#include <claw/string_algorithm.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::game_description::game_description()
  : m_game_name("Anonymous game"), m_screen_size(640, 480),
    m_active_area_margin(500)
{

} // game_description()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the first level to load.
 */
const std::string& bear::engine::game_description::start_level() const
{
  return m_start_level;
} // game_description::start_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the game.
 */
const std::string& bear::engine::game_description::game_name() const
{
  return m_game_name;
} // game_description::game_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size of the screen.
 */
const claw::math::coordinate_2d<unsigned int>&
bear::engine::game_description::screen_size() const
{
  return m_screen_size;
} // game_description::screen_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the margin of the active area around the screen.
 */
double  bear::engine::game_description::active_area_margin() const
{
  return m_active_area_margin;
} // game_description::active_area_margin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the paths to the forder containing the resources.
 */
const bear::engine::game_description::string_list&
bear::engine::game_description::resources_path() const
{
  return m_resources_path;
} // game_description::resources_path()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the paths to the libraries to link to.
 */
const bear::engine::game_description::string_list&
bear::engine::game_description::libraries() const
{
  return m_libraries;
} // game_description::libraries()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the name of the game.
 * \param value The name of the game.
 */
void bear::engine::game_description::set_game_name( const std::string& value )
{
  m_game_name = value;
} // game_description::set_game_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the width of the screen.
 * \param value The width of the screen.
 */
void bear::engine::game_description::set_screen_width( unsigned int value )
{
  m_screen_size.x = value;
} // game_description::set_screen_width()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the height of the screen.
 * \param value The height of the screen.
 */
void bear::engine::game_description::set_screen_height( unsigned int value )
{
  m_screen_size.y = value;
} // game_description::set_screen_height()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the margin of the active_area around the screen.
 * \param value The size of the margin.
 */
void bear::engine::game_description::set_active_area_margin
( unsigned int value )
{
  m_active_area_margin = value;
} // game_description::set_active_area_margin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the name of the first level.
 * \param value The name of the first level.
 */
void bear::engine::game_description::set_start_level( const std::string& value )
{
  m_start_level = value;
} // game_description::set_start_level()


/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_item.cpp
 * \brief Implementation of the bear::engine::base_item class.
 * \author Julien Jorge.
 */
#include "engine/base_item.hpp"

#include <algorithm>
#include <claw/logger.hpp>

#include "engine/layer/layer.hpp"
#include "engine/level.hpp"
#include "universe/collision_info.hpp"

#include "visual/scene_element_sequence.hpp"

/*----------------------------------------------------------------------------*/
bear::engine::base_item::id_type bear::engine::base_item::s_next_id = 1;

#ifndef NDEBUG
std::list<bear::engine::base_item*> bear::engine::base_item::s_allocated;
#endif

/*----------------------------------------------------------------------------*/
/**
 * \brief Print the address of the items that were not deleted.
 */
void bear::engine::base_item::print_allocated()
{
#ifndef NDEBUG
  unsigned int s = s_allocated.size();

  if ( s == 0 )
    claw::logger << claw::log_verbose << "All base_item have been deleted."
                 << std::endl;
  else
    {
      claw::logger << claw::log_verbose << s
                   << " base_item have NOT been deleted." << std::endl;

      std::list<base_item*>::const_iterator it;

      for (it=s_allocated.begin(); it!=s_allocated.end(); ++it)
        {
          std::string str;
          (*it)->to_string( str );

          claw::logger << claw::log_verbose << "-- Item\n" << str
                       << std::endl;
        }
    }
#endif
} // base_item::print_allocated()

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::engine::base_item::base_item()
  : m_id( s_next_id ), m_layer(NULL), m_z_position(0),
    m_flags(item_flag_z_fixed), m_dying(false), m_world(NULL)
{
  ++s_next_id;
#ifndef NDEBUG
  s_allocated.push_front(this);
#endif
} // base_item::base_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy contructor.
 */
bear::engine::base_item::base_item( const base_item& that )
  : level_object(that), physical_item(that), m_id( s_next_id ), m_layer(NULL),
    m_z_position(that.get_z_position()), m_flags(item_flag_z_fixed),
    m_dying(false)
{
  ++s_next_id;
#ifndef NDEBUG
  s_allocated.push_front(this);
#endif
} // base_item::base_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::engine::base_item::~base_item()
{
#ifndef NDEBUG
  s_allocated.erase( std::find(s_allocated.begin(), s_allocated.end(), this) );
#endif
} // base_item::~base_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Instanciate a copy of this item.
 */
bear::engine::base_item* bear::engine::base_item::clone() const
{
  return new base_item(*this);
} // base_item::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Build the item if it has never been built.
 */
void bear::engine::base_item::build_item()
{
  if ( (m_flags & item_flag_built) == item_flag_none )
    {
      m_flags |= item_flag_built;
      build();
    }
} // base_item::build_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Pre-cache the resources you need. This method is called automatically
 *        by the owner.
 */
void bear::engine::base_item::pre_cache()
{
  // nothing to do
} // base_item::pre_cache()

/*----------------------------------------------------------------------------*/
/**
 * \brief Build the item. This method is called automatically by the owner.
 */
void bear::engine::base_item::build()
{
  // nothing to do
} // base_item::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destroy the item. This method is called automatically when the item
 *        is killed.
 * \pre The item has been added in a layer.
 */
void bear::engine::base_item::destroy()
{
  // nothing to do
} // base_item::destroy()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::engine::base_item::progress( universe::time_type elapsed_time )
{
  // nothing to do
} // base_item::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Insert the visual of the item at the end of a given list.
 * \param visuals The list in which the visual is added.
 *
 * This method uses get_visual() to get the visuals and make a
 * visual::scene_element_sequence of them.
 */
void
bear::engine::base_item::insert_visual( std::list<scene_visual>& visuals ) const
{
  std::list<scene_visual> v;
  get_visual( v );

  if ( v.size() > 1 )
    {
      v.sort( scene_visual::z_position_compare() );
      visual::scene_element_sequence e;

      for ( ; !v.empty() ; v.pop_front() )
        {
#ifndef NDEBUG
          const visual::rectangle_type r
            ( v.front().scene_element.get_bounding_box() );
          if ( (r.width() == 0) || (r.height() == 0) )
            claw::logger << claw::log_warning
                         << "Empty visual::scene_element is inserted in a "
                         << "visual::scene_element_sequence by '"
                         << get_class_name() << "'. This should be avoided."
                         << std::endl;
#endif // NDEBUG

          e.push_back(v.front().scene_element);
        }

      visuals.push_back( scene_visual(e, get_z_position()) );
    }
  else if (v.size() == 1)
    {
      visuals.push_back( v.front() );
      visuals.back().z_position = get_z_position();
    }
} // base_item::insert_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the visuals representing the item.
 * \param visuals (out) The sprites of the item, and their positions.
 */
void
bear::engine::base_item::get_visual( std::list<scene_visual>& visuals ) const
{
  // nothing to do
} // base_item::get_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c unsigned \c integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_u_integer_field
( const std::string& name, unsigned int value )
{
  return false;
} // base_item::set_u_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
bear::engine::base_item::set_integer_field( const std::string& name, int value )
{
  bool ok = true;

  if (name == "base_item.position.depth")
    m_z_position = value;
  else
    ok = false;

  return ok;
} // base_item::set_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c real.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
bear::engine::base_item::set_real_field( const std::string& name, double value )
{
  bool ok = true;

  if (name == "base_item.position.left")
    set_left(value);
  else if (name == "base_item.position.bottom")
    set_bottom(value);
  else if (name == "base_item.size.height")
    set_height(value);
  else if (name == "base_item.size.width")
    set_width(value);
  else if (name == "base_item.mass")
    set_mass(value);
  else if (name == "base_item.density")
    set_density(value);
  else if (name == "base_item.elasticity")
    set_elasticity(value);
  else if (name == "base_item.hardness")
    set_hardness(value);
  else if (name == "base_item.system_angle")
    set_system_angle(value);
  else if (name == "base_item.speed.x")
    set_speed( universe::speed_type(value, get_speed().y) );
  else if (name == "base_item.speed.y")
    set_speed( universe::speed_type(get_speed().x, value) );
  else
    ok = false;

  return ok;
} // base_item::set_real_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
bear::engine::base_item::set_bool_field( const std::string& name, bool value )
{
  bool ok = true;

  if (name == "base_item.artificial")
    set_artificial(value);
  else if (name == "base_item.can_move_items")
    set_can_move_items(value);
  else if (name == "base_item.global")
    set_global(value);
  else if (name == "base_item.phantom")
    set_phantom(value);
  else if (name == "base_item.fixed.x")
    {
      if ( value )
        add_position_constraint_x();
    }
  else if (name == "base_item.fixed.y")
    {
      if ( value )
        add_position_constraint_y();
    }
  else if (name == "base_item.fixed.z")
    set_z_fixed(value);
  else
    ok = false;

  return ok;
} // base_item::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_string_field
( const std::string& name, const std::string& value )
{
  return false;
} // base_item::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c base_item.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_item_field
( const std::string& name, base_item* value )
{
  return false;
} // base_item::set_item_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c visual::sprite.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_sprite_field
( const std::string& name, const visual::sprite& value )
{
  return false;
} // base_item::set_sprite_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c visual::animation.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_animation_field
( const std::string& name, const visual::animation& value )
{
  return false;
} // base_item::set_animation_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c audio::sample.
 * \param name The name of the field.
 * \param value The new value of the field. The item receiving this value must
 *        release de memory with a delete at some time.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_sample_field
( const std::string& name, audio::sample* value )
{
  delete value;

  return false;
} // base_item::set_sample_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of unsigned integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_u_integer_list_field
( const std::string& name, const std::vector<unsigned int>& value )
{
  return false;
} // base_item::set_u_integer_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of integer.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
bear::engine::base_item::set_integer_list_field
( const std::string& name, const std::vector<int>& value )
{
  return false;
} // base_item::set_integer_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of real.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
bear::engine::base_item::set_real_list_field
( const std::string& name, const std::vector<double>& value )
{
  return false;
} // base_item::set_real_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool
bear::engine::base_item::set_bool_list_field
( const std::string& name, const std::vector<bool>& value )
{
  return false;
} // base_item::set_bool_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_string_list_field
( const std::string& name, const std::vector<std::string>& value )
{
  return false;
} // base_item::set_string_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of item.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_item_list_field
( const std::string& name, const std::vector<base_item*>& value )
{
  return false;
} // base_item::set_item_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of visual::sprite.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_sprite_list_field
( const std::string& name, const std::vector<visual::sprite>& value )
{
  return false;
} // base_item::set_sprite_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of visual::animation.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_animation_list_field
( const std::string& name, const std::vector<visual::animation>& value )
{
  return false;
} // base_item::set_animation_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type \c list of audio::sample.
 * \param name The name of the field.
 * \param value The new value of the field. The item receiving this value must
 *        release de memory with a delete at some time.
 * \return false if the field "name" is unknow, true otherwise.
 */
bool bear::engine::base_item::set_sample_list_field
( const std::string& name, const std::vector<audio::sample*>& value )
{
  for (std::size_t i=0; i!=value.size(); ++i)
    delete value[i];

  return false;
} // base_item::set_sample_list_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is correctly initialized.
 */
bool bear::engine::base_item::is_valid() const
{
  return true;
} // base_item::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get item's identifier.
 */
bear::engine::base_item::id_type bear::engine::base_item::get_id() const
{
  return m_id;
} // base_item::get_id()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get item's class name.
 */
const char* bear::engine::base_item::get_class_name() const
{
  return "bear::engine::base_item";
} // base_item::get_class_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Kill the item.
 * \pre m_owner != NULL
 */
void bear::engine::base_item::kill()
{
  claw::logger << claw::log_verbose
               << "Killing id #" << m_id << ' ' << *this << std::endl;

  CLAW_PRECOND( m_layer != NULL );
  CLAW_PRECOND( !get_insert_as_static() );

  if (!m_dying)
    {
      m_dying = true;
      destroy();
      m_layer->remove_item(*this);
      m_world = NULL;
    }
} // base_item::kill()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indicates if the fonction "kill" has been called.
 */
bool bear::engine::base_item::is_dead() const
{
  return m_dying;
} // base_item::is_dead()

/*----------------------------------------------------------------------------*/
/**
 * \brief Remove the data about the environment of the item.
 * \post m_layer == NULL
 *
 * This method is called automatically when the item is removed from a layer.
 */
void bear::engine::base_item::clear_environment()
{
  clear_level();

  m_layer = NULL;
} // base_item::clear_environment()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the environment in which the item is.
 * \param the_level The level in which we are.
 * \pre m_layer == NULL
 *
 * This method is called automatically when the item is added in a layer.
 */
void bear::engine::base_item::set_environment( layer& the_layer )
{
  CLAW_PRECOND( m_layer == NULL );

  set_level( the_layer.get_level() );

  m_layer = &the_layer;
} // base_item::set_environment()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is in a given layer.
 * \param the_layer The layer where the item could be.
 */
bool bear::engine::base_item::is_in_layer
( const bear::engine::layer& the_layer ) const
{
  return m_layer == &the_layer;
} // base_item::is_in_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the position of the item on the Z axis.
 * \param z The new position.
 */
void bear::engine::base_item::set_z_position( int z )
{
  m_z_position = z;
} // base_item::set_z_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the position of the item on the Z axis.
 */
int bear::engine::base_item::get_z_position() const
{
  return m_z_position;
} // base_item::get_z_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the z fixed statut.
 * \param fixed The new statut.
 */
void bear::engine::base_item::set_z_fixed( bool fixed )
{
  if (fixed)
    m_flags |= item_flag_z_fixed;
  else
    m_flags &= ~item_flag_z_fixed;
} // base_item::set_z_fixed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the z fixed statut.
 */
bool bear::engine::base_item::is_z_fixed() const
{
  return m_flags & item_flag_z_fixed;
} // base_item::is_z_fixed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call this method before inserting the item in a level to ensure it
 *        will be static in the world.
 */
void bear::engine::base_item::set_insert_as_static()
{
  m_flags |= item_flag_insert_as_static;
} // base_item::set_insert_as_static()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if this item has to be inserted as a static item in the level.
 */
bool bear::engine::base_item::get_insert_as_static() const
{
  return m_flags & item_flag_insert_as_static;
} // base_item::get_insert_as_static()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if this item is in a world.
 */
bool bear::engine::base_item::has_world() const
{
  return m_world != NULL;
} // base_item::has_world()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the world in which this item is.
 */
const bear::engine::world& bear::engine::base_item::get_world() const
{
  CLAW_PRECOND( has_world() );
  return *m_world;
} // base_item::get_world()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the world in which this item is.
 * \param w The world.
 */
void bear::engine::base_item::set_world( const world& w )
{
  m_world = &w;
} // base_item::set_world()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the layer in which the item is.
 * \pre m_layer != NULL
 */
bear::engine::layer& bear::engine::base_item::get_layer() const
{
  CLAW_PRECOND( m_layer != NULL );

  return *m_layer;
} // base_item::get_layer()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell that this item is always displayed.
 * \param b Always displayed or not.
 */
void bear::engine::base_item::set_always_displayed(bool b)
{
  if (m_layer != NULL)
    {
      if (b)
        m_layer->set_always_displayed(*this);
      else
        m_layer->unset_always_displayed(*this);
    }
} // base_item::set_always_displayed()

/*----------------------------------------------------------------------------*/
/**
 * \brief A collision with an other item has occured.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
void bear::engine::base_item::collision
( base_item& that, universe::collision_info& info )
{

} // base_item::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give a string representation of the item.
 * \param str (out) The result of the convertion.
 */
void bear::engine::base_item::to_string( std::string& str ) const
{
  std::ostringstream oss;
  oss << "id/class: " << m_id << '/' << get_class_name() << "\n";
  oss << "pos_z: " << m_z_position << "\n";

  universe::physical_item::to_string(str);

  str = oss.str() + str;
} // base_item::to_string()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add an item in the same environment of \a this.
 * \pre m_layer != NULL
 */
void bear::engine::base_item::new_item( base_item& item ) const
{
  CLAW_PRECOND( m_layer != NULL );

  m_layer->add_item(item);
} // base_item::new_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Export the methods of the class.
 */
void bear::engine::base_item::init_exported_methods()
{
  TEXT_INTERFACE_CONNECT_METHOD_1
    ( base_item, set_z_position, void, int );

  TEXT_INTERFACE_CONNECT_METHOD_0
    ( base_item, kill, void );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_0
    ( base_item, universe::physical_item_state,
      remove_position_constraint_x, void );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_0
    ( base_item, universe::physical_item_state,
      remove_position_constraint_y, void );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_0
    ( base_item, universe::physical_item_state,
      add_position_constraint_x, void );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_0
    ( base_item, universe::physical_item_state,
      add_position_constraint_y, void );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_speed, void, double, double );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_center_of_mass, void,
      const universe::coordinate_type&, const universe::coordinate_type& );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_top_left, void,
      const universe::coordinate_type&, const universe::coordinate_type& );
  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_top_middle, void,
      const universe::coordinate_type&, const universe::coordinate_type& );
  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_top_right, void,
      const universe::coordinate_type&, const universe::coordinate_type& );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_bottom_left, void,
      const universe::coordinate_type&, const universe::coordinate_type& );
  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_bottom_middle, void,
      const universe::coordinate_type&, const universe::coordinate_type& );
  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_bottom_right, void,
      const universe::coordinate_type&, const universe::coordinate_type& );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_left_middle, void,
      const universe::coordinate_type&, const universe::coordinate_type& );
  TEXT_INTERFACE_CONNECT_PARENT_METHOD_2
    ( base_item, universe::physical_item_state,
      set_right_middle, void,
      const universe::coordinate_type&, const universe::coordinate_type& );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_1
    ( base_item, universe::physical_item_state,
      set_horizontal_middle, void, universe::coordinate_type );
  TEXT_INTERFACE_CONNECT_PARENT_METHOD_1
    ( base_item, universe::physical_item_state,
      set_vertical_middle, void, universe::coordinate_type );

  TEXT_INTERFACE_CONNECT_PARENT_METHOD_1
    ( base_item, universe::physical_item_state, set_center_on, void,
      const universe::physical_item_state& );
} // god::init_exported_methods()

/*----------------------------------------------------------------------------*/
/**
 * \brief Cast the other item and call collision(base_item, collision_info).
 * \param info Some informations about the collision.
 */
void bear::engine::base_item::collision( universe::collision_info& info )
{
  base_item* o = dynamic_cast<base_item*>(&info.other_item());

  if (o)
    collision(*o, info);
  else
    claw::logger << claw::log_error
     << "bear::engine::base_item::collision(): Collision with an "
     << "item of type different of bear::engine::base_item."
     << std::endl;
} // base_item::collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::engine::base_item::time_step( universe::time_type elapsed_time )
{
  if (!m_dying)
    progress(elapsed_time);
} // base_item::time_step()

/*----------------------------------------------------------------------------*/
TEXT_INTERFACE_IMPLEMENT_METHOD_LIST( bear::engine::base_item )

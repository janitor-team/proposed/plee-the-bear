/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file game_local_client.cpp
 * \brief Implementation of the bear::engine::game_local_client class.
 * \author Julien Jorge
 */
#include "engine/game_local_client.hpp"

#include "input/system.hpp"
#include "engine/game_action/game_action.hpp"
#include "engine/game_action/game_action_load_level.hpp"
#include "engine/game_action/game_action_pop_level.hpp"
#include "engine/game_action/game_action_push_level.hpp"
#include "engine/game_action/game_action_set_current_level.hpp"
#include "engine/compiled_file.hpp"
#include "engine/level.hpp"
#include "engine/level_globals.hpp"
#include "engine/level_loader.hpp"
#include "engine/resource_pool.hpp"
#include "engine/version.hpp"
#include "engine/variable/base_variable.hpp"
#include "engine/variable/variable_eraser.hpp"
#include "engine/variable/variable_copy.hpp"

#include "bear_gettext.hpp"

#include "debug/timing_log.hpp"

#include <boost/filesystem/convenience.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <claw/exception.hpp>
#include <claw/logger.hpp>
#include <claw/socket_traits.hpp>
#include <claw/string_algorithm.hpp>
#include <claw/system_info.hpp>
#include <sstream>

/*----------------------------------------------------------------------------*/
const std::string
bear::engine::game_local_client::s_init_game_function_prefix("init_");
const std::string
bear::engine::game_local_client::s_end_game_function_prefix("end_");

/*----------------------------------------------------------------------------*/
/**
 * \brief Print the options of the program.
 */
void bear::engine::game_local_client::print_help()
{
  get_arguments_table().help();
} // game_local_client::print_help()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param argc Number of program arguments.
 * \param argv Program arguments.
 */
bear::engine::game_local_client::game_local_client( int& argc, char** &argv )
  : m_status(status_init), m_screen(NULL), m_fullscreen(false),
    m_current_level(NULL), m_level_in_abeyance(NULL), m_time_step(15)
{
  if ( !check_arguments(argc, argv) )
    m_status = status_quit;
  else
    {
      init_environment();

      try
        {
          m_screen = new visual::screen
            ( m_game_description.screen_size(),
              m_game_description.game_name(), m_fullscreen );
        }
      catch(...)
        {
          clear();
          close_environment();
          throw;
        }
    }
} // game_local_client::game_local_client()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::engine::game_local_client::~game_local_client()
{
  clear();
  close_environment();

  base_item::print_allocated();
} // game_local_client::~game_local_client()

/*----------------------------------------------------------------------------*/
/**
 * \brief Run the game.
 */
void bear::engine::game_local_client::run()
{
  if ( m_status != status_quit )
    {
      init_stats();
      init_game();

      load_level( m_game_description.start_level() );

      run_level();

      end_game();

      clear();
      m_stats.send();

      close_environment();
    }
} // game_local_client::run()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the time step between to progress on the level.
 */
bear::systime::milliseconds_type
bear::engine::game_local_client::get_time_step() const
{
  return m_time_step;
} // game_local_client::get_time_step()

/*----------------------------------------------------------------------------*/
/**
 * \brief Turn on/off the full screen mode.
 * \param b Tell if we must activate the fullscreen mode.
 */
void bear::engine::game_local_client::set_fullscreen( bool full )
{
  if ( m_fullscreen != full )
    {
      m_fullscreen = full;
      m_screen->fullscreen(m_fullscreen);
    }
} // game_local_client::set_fullscreen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the full screen mode.
 */
bool bear::engine::game_local_client::get_fullscreen() const
{
  return m_fullscreen;
} // game_local_client::get_fullscreen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the sounds.
 * \param m The mute status.
 */
void bear::engine::game_local_client::set_sound_muted( bool m )
{
  if ( m_current_level == NULL )
    level_globals::global_set_sound_muted(m);
  else
    m_current_level->get_globals().mute_sound(m);
} // game_local_client::set_sound_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the sound is muted.
 */
bool bear::engine::game_local_client::get_sound_muted() const
{
  return level_globals::global_get_sound_muted();
} // game_local_client::get_sound_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the sounds.
 * \param v The volume.
 */
void bear::engine::game_local_client::set_sound_volume( double v )
{
  if ( m_current_level == NULL )
    level_globals::global_set_sound_volume(v);
  else
    m_current_level->get_globals().set_sound_volume(v);
} // game_local_client::set_sound_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the volume of the sounds.
 */
double bear::engine::game_local_client::get_sound_volume() const
{
  return level_globals::global_get_sound_volume();
} // game_local_client::get_sound_colume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Mute/unmute the music.
 * \param m The mute status.
 */
void bear::engine::game_local_client::set_music_muted( bool m )
{
  if ( m_current_level == NULL )
    level_globals::global_set_music_muted(m);
  else
    m_current_level->get_globals().mute_music(m);
} // game_local_client::set_music_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the music is muted.
 */
bool bear::engine::game_local_client::get_music_muted() const
{
  return level_globals::global_get_music_muted();
} // game_local_client::get_music_muted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the volume of the musics.
 * \param v The volume.
 */
void bear::engine::game_local_client::set_music_volume( double v )
{
  if ( m_current_level == NULL )
    level_globals::global_set_music_volume(v);
  else
    m_current_level->get_globals().set_music_volume(v);
} // game_local_client::set_music_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the volume of the musics.
 */
double bear::engine::game_local_client::get_music_volume() const
{
  return level_globals::global_get_music_volume();
} // game_local_client::get_music_volume()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a shot of the screen.
 * \param img The image in which we save the screen.
 */
void
bear::engine::game_local_client::screenshot( claw::graphic::image& img ) const
{
  m_screen->shot( img );
} // game_local_client::screenshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Take a shot of the whole level.
 * \param img The image in which we save the level.
 */
void
bear::engine::game_local_client::levelshot( claw::graphic::image& img ) const
{
  m_current_level->shot( *m_screen, img );
} // game_local_client::levelshot()

/*----------------------------------------------------------------------------*/
/**
 * \brief End the game.
 */
void bear::engine::game_local_client::end()
{
  m_status = status_quit;
} // game_local_client::end()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give the name of a level to load as soon as possible.
 * \param path The path of the level to load.
 */
void
bear::engine::game_local_client::set_waiting_level( const std::string& path )
{
  m_post_actions.push( new game_action_load_level(path) );
} // game_local_client::set_waiting_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the level to run as soon as possible.
 * \param the_level The level to run as soon as possible.
 */
void bear::engine::game_local_client::set_waiting_level( level* the_level )
{
  CLAW_PRECOND( the_level != NULL );

  m_post_actions.push( new game_action_set_current_level(the_level) );
} // game_local_client::set_waiting_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Give the name of a level to load as soon as possible but keep the
 *        current level in memory for future restoration.
 * \param path The path of the level to load.
 */
void bear::engine::game_local_client::push_level( const std::string& path )
{
  m_post_actions.push( new game_action_push_level(path) );
} // game_local_client::push_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Restore the level at the top of the stack.
 */
void bear::engine::game_local_client::pop_level()
{
  m_post_actions.push( new game_action_pop_level );
} // game_local_client::pop_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size of the screen.
 */
const claw::math::coordinate_2d<unsigned int>&
bear::engine::game_local_client::get_screen_size() const
{
  return m_game_description.screen_size();
} // game_local_client::get_screen_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the margin of the active area around the camera.
 */
double bear::engine::game_local_client::get_active_area_margin() const
{
  return m_game_description.active_area_margin();
} // game_local_client::get_active_area_margin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the path of a file in the user's game directory.
 * \param name The name of the file.
 */
std::string bear::engine::game_local_client::get_custom_game_file
( const std::string& name ) const
{
  std::string result = get_game_directory();

  if ( !result.empty() )
    {
      boost::filesystem::path path( result, boost::filesystem::native );
      path /= name;
      result = path.string();
    }
  else
    result = name;

  return result;
} // game_local_client::get_custom_game_file()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the value of a global variable.
 * \param val (in/out) The variable for which we want the value.
 */
void
bear::engine::game_local_client::get_game_variable( base_variable& val ) const
{
  val.get_value_from(m_game_variables);
} // game_local_client::get_game_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the game variables whose name match a given regular
 *        expression;
 * \param vars The list of matched game variables.
 * \param pattern The expression that has to be matched by the variable names.
 */
void bear::engine::game_local_client::get_game_variables
( var_map& vars, const std::string& pattern )
{
  m_game_variables.for_each( variable_copy(vars, boost::regex(pattern)) );
} // game_local_client::get_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of a global variable.
 * \param val (in/out) The variable for which we set the value.
 */
void bear::engine::game_local_client::set_game_variable
( const base_variable& val )
{
  val.assign_value_to(m_game_variables);
} // game_local_client::set_game_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set several game variables at once.
 * \param vars The variables.
 */
void bear::engine::game_local_client::set_game_variables( const var_map& vars )
{
  m_game_variables.set(vars);
} // game_local_client::set_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Erase the variables whose name match a given pattern.
 * \param pattern The pattern to match.
 */
void bear::engine::game_local_client::erase_game_variables
( const std::string& pattern )
{
  m_game_variables.for_each
    ( variable_eraser( m_game_variables, boost::regex(pattern) ) );
} // game_local_client::erase_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if a given variable exists in the game.
 * \param val (in/out) The variable to check.
 */
bool bear::engine::game_local_client::game_variable_exists
( const base_variable& val ) const
{
  return val.exists(m_game_variables);
} // game_local_client::game_variable_exists()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get all the game variables.
 * \param vars (in/out) All the variables.
 */
void
bear::engine::game_local_client::get_all_game_variables( var_map& vars ) const
{
  vars = m_game_variables;
} // game_local_client::get_all_game_variables()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type int.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game_local_client::listen_int_variable_change
( const std::string& name, const boost::function<void (int)>& f )
{
  return listen_variable_change<int>( name, f );
} // game_local_client::listen_int_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type
 *        unsigned int.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game_local_client::listen_uint_variable_change
( const std::string& name, const boost::function<void (unsigned int)>& f )
{
  return listen_variable_change<unsigned int>( name, f );
} // game_local_client::listen_uint_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type bool.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game_local_client::listen_bool_variable_change
( const std::string& name, const boost::function<void (bool)>& f )
{
  return listen_variable_change<bool>( name, f );
} // game_local_client::listen_bool_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type double.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game_local_client::listen_double_variable_change
( const std::string& name, const boost::function<void (double)>& f )
{
  return listen_variable_change<double>( name, f );
} // game_local_client::listen_double_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener to follow the change on a game variable of type string.
 * \param name The name of the variable.
 * \param f The listener.
 */
boost::signals::connection
bear::engine::game_local_client::listen_string_variable_change
( const std::string& name, const boost::function<void (std::string)>& f )
{
  return listen_variable_change<std::string>( name, f );
} // game_local_client::listen_string_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the game.
 */
const std::string& bear::engine::game_local_client::get_name() const
{
  return m_game_description.game_name();
} // game_local_client::get_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the game statistics.
 */
void bear::engine::game_local_client::init_stats()
{
  std::string file_name( get_custom_game_file( "engine-stats" ) );
  std::ifstream f(file_name.c_str());
  boost::uuids::uuid id;

  if ( !f )
    {
      id = boost::uuids::random_generator()();
      std::ofstream of( file_name.c_str() );
      of << id;
    }
  else
    f >> id;

  std::ostringstream oss;
  oss << id;
  m_stats.set_user_id( oss.str() );
} // game_local_client::init_stats()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call the game specific function for initialisation.
 */
void bear::engine::game_local_client::init_game() const
{
  std::string game_proc
    ( s_init_game_function_prefix + get_game_name_as_filename() );

  claw::logger << claw::log_verbose << "Initialising game: '" << game_proc
               << "()'" << std::endl;

  if ( m_symbols.have_symbol( game_proc ) )
    {
      init_game_function_type func =
        m_symbols.get_symbol<init_game_function_type>( game_proc );
      func();
    }
} // game_local_client::init_game()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call the game specific function for ending the game.
 */
void bear::engine::game_local_client::end_game() const
{
  std::string game_proc
    ( s_end_game_function_prefix + get_game_name_as_filename() );

  claw::logger << claw::log_verbose << "Ending game: '" << game_proc
               << "()'" << std::endl;

  if ( m_symbols.have_symbol( game_proc ) )
    {
      end_game_function_type func =
        m_symbols.get_symbol<end_game_function_type>( game_proc );
      func();
    }
} // game_local_client::end_game()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the game in a string usable for a file name.
 */
std::string bear::engine::game_local_client::get_game_name_as_filename() const
{
  std::string result( m_game_description.game_name() );

  std::transform( result.begin(), result.end(), result.begin(), tolower );

  for (unsigned int i=0; i!=result.size(); ++i)
    if (result[i] == ' ')
      result[i] = '_';
    else if (result[i] == '\t')
      result[i] = '_';

  claw::text::squeeze( result, "_" );

  return result;
} // game_local_client::get_game_name_as_filename()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get, and create, the personal game directory of the user.
 */
std::string bear::engine::game_local_client::get_game_directory() const
{
  boost::filesystem::path dir
    (claw::system_info::get_user_directory(), boost::filesystem::native);

  std::string result;
  std::string subdir = '.' + get_game_name_as_filename();

  dir /= boost::filesystem::path(subdir, boost::filesystem::native);

  if ( create_game_directory(dir.string()) )
    result = dir.string();
  else
    claw::logger << claw::log_error << "Can't create game directory '"
                 << dir.string() << "'." << std::endl;

  return result;
} // game_local_client::get_game_directory()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the personal game directory of the user.
 * \param dir The path of the directory.
 */
bool bear::engine::game_local_client::create_game_directory
( const std::string& dir ) const
{
  bool result = false;
  boost::filesystem::path path( dir, boost::filesystem::native );

  if ( boost::filesystem::exists( path ) )
    result = boost::filesystem::is_directory( path );
  else
    result = boost::filesystem::create_directory( path );

  return result;
} // game_local_client::create_game_directory()

/*----------------------------------------------------------------------------*/
/**
 * \brief Run the current_level.
 */
void bear::engine::game_local_client::run_level()
{
  m_status = status_run;

  while (m_status != status_quit)
    {
      m_last_progress = systime::get_date_ms();

      do
        {
          one_step_beyond();
        }
      while ( !do_post_actions() && (m_status != status_quit) );
    }
} // game_local_client::run_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one progress()/render() iteration.
 */
void bear::engine::game_local_client::one_step_beyond()
{
  systime::milliseconds_type current_time;

  current_time = systime::get_date_ms();
  universe::time_type dt( current_time - m_last_progress );

  if ( dt >= m_time_step )
    {
      m_last_progress = current_time;

      do
        {
          progress( (universe::time_type)m_time_step / 1000 ); // seconds
          dt -= m_time_step;
        }
      while ( (dt >= m_time_step) && (m_time_step > 0) );

      m_last_progress -= dt;

      render();
    }

  if ( m_time_step > 0 )
    systime::sleep( m_last_progress + m_time_step - current_time );
} // game_local_client::one_step_beyond()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the game.
 * \param elapsed_time Elapsed time since the last call.
 */
void bear::engine::game_local_client::progress
( universe::time_type elapsed_time )
{
  // effective procedure
  input::system::get_instance().refresh();

  m_current_level->progress( elapsed_time );
} // game_local_client::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the current level.
 */
void bear::engine::game_local_client::render()
{
  if ( m_screen->need_restoration() )
    {
      m_current_level->get_globals().restore_images();

      if ( m_level_in_abeyance != NULL )
        m_level_in_abeyance->get_globals().restore_images();

      m_screen->set_restored();
    }

  // effective procedure
  m_screen->begin_render();
  m_current_level->render( *m_screen );

  if ( !m_screen->end_render() )
    end();
} // game_local_client::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the environment (screen, inputs, sounds).
 */
void bear::engine::game_local_client::init_environment() const
{
  claw::logger << claw::log_verbose << "Initializing screen environment."
               << std::endl;

  visual::screen::initialize( visual::screen::screen_gl );

  claw::logger << claw::log_verbose << "Initializing input environment."
               << std::endl;

  input::system::initialize();

  claw::logger << claw::log_verbose << input::joystick::number_of_joysticks()
               << " joysticks found." << std::endl;

  claw::logger << claw::log_verbose << "Initializing sound environment."
               << std::endl;

  audio::sound_manager::initialize();

  if ( !claw::socket_traits::init() )
    claw::logger << claw::log_error << "Failed to initialize the network."
		 << std::endl;
} // game_local_client::init_environment()

/*----------------------------------------------------------------------------*/
/**
 * \brief Close the environment (screen, inputs, sounds).
 */
void bear::engine::game_local_client::close_environment() const
{
  claw::logger << claw::log_verbose << "Closing screen environment."
               << std::endl;

  visual::screen::release();

  claw::logger << claw::log_verbose << "Closing input environment."
               << std::endl;

  input::system::release();

  claw::logger << claw::log_verbose << "Closing sound environment."
               << std::endl;

  audio::sound_manager::release();

  claw::socket_traits::release();
} // game_local_client::close_environment()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load the libraries containing the items.
 * \param p A list of paths to libraries.
 */
void bear::engine::game_local_client::load_libraries
( const std::list<std::string>& p )
{
  std::list<std::string>::const_iterator it;

  for ( it=p.begin(); it!=p.end(); ++it )
    {
      claw::logger << claw::log_verbose << "Add library '" << *it << "'."
                   << std::endl;
      m_symbols.add_library(*it);
    }
} // game_local_client::load_libraries()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add paths in the resource_pool.
 * \param p A list of paths.
 */
void bear::engine::game_local_client::init_resource_pool
( const std::list<std::string>& p ) const
{
  std::list<std::string>::const_iterator it;

  for ( it=p.begin(); it!=p.end(); ++it )
    {
      claw::logger << claw::log_verbose
                   << "Adding resource path '" << *it << "'."
                   << std::endl;
      resource_pool::get_instance().add_path(*it);
    }
} // game_local_client::init_resource_pool()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do the pending actions.
 */
bool bear::engine::game_local_client::do_post_actions()
{
  bool result = false;

  while ( !m_post_actions.empty() )
    {
      game_action* a=m_post_actions.front();
      m_post_actions.pop();

      result = a->apply(*this);

      delete a;
    }

  return result;
} // game_local_client::do_post_actions()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the current level (and delete the level currently running).
 * \param the_level The new current level.
 */
void bear::engine::game_local_client::set_current_level( level* the_level )
{
  CLAW_PRECOND( the_level != NULL );

  if ( m_current_level != NULL )
    close_level();

  m_current_level = the_level;

  start_current_level();
} // game_local_client::set_current_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Load a level.
 * \param path The path of the level to load.
 */
void bear::engine::game_local_client::load_level( const std::string& path )
{
#ifdef _WIN32
  // This, combined with the level dummy.cl, is a unexplained solution to the
  // OpenGL problem we have otherwise (ie. the game starts with invalid
  // textures).

  // The function is called for dummy.cl, then gamned.cl, then the title screen.
  // Thus we force the call to screen::fullscreen() at the title screen.

  static int resize = 0;

  if ( resize <= 2 )
    {
      ++resize;

      if ( resize == 3 )
	m_screen->fullscreen( m_fullscreen );
    }   
#endif
  claw::logger << claw::log_verbose << "------------ Loading level '"
               << path << "'... ------------" << std::endl;

  std::stringstream f;
  resource_pool::get_instance().get_file( path, f );

  if ( !f )
    throw claw::exception( "Can't open level file '" + path + "'." );

  /// \todo test the file to see if it's text or binary
  compiled_file level_file( f, true );

  level_loader loader(level_file, path);
  loader.complete_run();

  set_current_level( loader.drop_level() );
} // game_local_client::load_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Close the current level.
 * \pre m_current_level != NULL
 * \post m_current_level == NULL
 */
void bear::engine::game_local_client::close_level()
{
  CLAW_PRECOND( m_current_level != NULL );

  delete m_current_level;
  m_current_level = NULL;

  CLAW_POSTCOND( m_current_level == NULL );
} // game_local_client::close_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a level on the stack.
 * \param path The path of the level to load.
 */
void bear::engine::game_local_client::do_push_level( const std::string& path )
{
  claw::logger << claw::log_verbose
               << "------------ Pushing '" << path << "'. ------------"
               << std::endl;

  CLAW_PRECOND( m_level_in_abeyance == NULL );

  m_level_in_abeyance = m_current_level;
  m_level_in_abeyance->set_pause();
  m_current_level = NULL;

  load_level( path );

  CLAW_POSTCOND( m_level_in_abeyance != NULL );
} // game_local_client::do_push_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Restore the level on the top of the stack.
 */
void bear::engine::game_local_client::do_pop_level()
{
  claw::logger << claw::log_verbose
               << "------------ Popping. ------------" << std::endl;
  CLAW_PRECOND( m_level_in_abeyance != NULL );
  CLAW_PRECOND( m_current_level != NULL );

  close_level();

  m_current_level = m_level_in_abeyance;
  m_level_in_abeyance = NULL;

  m_current_level->unset_pause();
  set_music_muted(get_music_muted());
  set_sound_muted(get_sound_muted());
  set_music_volume(get_music_volume());
  set_sound_volume(get_sound_volume());
} // game_local_client::do_pop_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Start the current level.
 */
void bear::engine::game_local_client::start_current_level()
{
  claw::logger << claw::log_verbose
               << "------------ Starting level. ------------" << std::endl;

  CLAW_PRECOND( m_current_level != NULL );

  m_current_level->start();
} // game_local_client::start_current_level()

/*----------------------------------------------------------------------------*/
/**
 * \brief Remove all layers, controllers and the screen.
 */
void bear::engine::game_local_client::clear()
{
  if ( m_current_level != NULL )
    {
      if ( m_level_in_abeyance != NULL )
        do_pop_level();

      close_level();
    }

  if (m_screen != NULL)
    {
      delete m_screen;
      m_screen = NULL;
    }

  // deleting pending actions
  while ( !m_post_actions.empty() )
    {
      delete m_post_actions.front();
      m_post_actions.pop();
    }
} // game_local_client::clear()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check the arguments to initialise the game.
 * \param argc Number of program arguments.
 * \param argv Program arguments.
 */
bool bear::engine::game_local_client::check_arguments( int& argc, char** &argv )
{
  bool result = false;
  std::string help;
  bool auto_exit(false);
  claw::arguments_table arg( get_arguments_table() );
  char game_var_assignment('=');

  arg.parse( argc, argv );

  if ( arg.get_bool("--version") )
    {
      std::cout << BEAR_VERSION_STRING << std::endl;
      auto_exit = true;
    }

  if ( arg.has_value("--game-name") )
    m_game_description.set_game_name( arg.get_string("--game-name") );

  m_stats.set_destination( arg.get_string("--stats-destination") );

  if ( arg.has_value("--active-area") )
    {
      if ( arg.only_integer_values("--active-area") )
        m_game_description.set_active_area_margin
          ( arg.get_integer("--active-area") );
      else
        help = "--active-area=" + arg.get_string("--active-area");
    }

  m_fullscreen = arg.get_bool("--fullscreen") && !arg.get_bool("--windowed");

  if ( arg.has_value("--screen-height") )
    {
      if ( arg.only_integer_values("--screen-height") )
        m_game_description.set_screen_height
          ( arg.get_integer("--screen-height") );
      else
        help = "--screen-height=" + arg.get_string("--screen-height");
    }

  if ( arg.has_value("--screen-width") )
    {
      if ( arg.only_integer_values("--screen-width") )
        m_game_description.set_screen_width
          ( arg.get_integer("--screen-width") );
      else
        help = "--screen-width=" + arg.get_string("--screen-width");
    }

  if ( arg.has_value("--game-var-assignment") )
    {
      const std::string v( arg.get_string("--game-var-assignment") );

      if ( v.length() == 1 )
        game_var_assignment = v[0];
      else
        help = bear_gettext("--game-var-assignment: not a character.");
    }

  if ( arg.has_value("--set-game-var-int") )
    if ( !set_game_variable_from_arg<int>
         ( arg.get_all_of_string("--set-game-var-int"), game_var_assignment ) )
      help = bear_gettext("--set-game-var-int: not an integer");

  if ( arg.has_value("--set-game-var-uint") )
    if ( !set_game_variable_from_arg<unsigned int>
         ( arg.get_all_of_string("--set-game-var-uint"), game_var_assignment ) )
      help = bear_gettext("--set-game-var-uint: not an unsigned integer");

  if ( arg.has_value("--set-game-var-bool") )
    if ( !set_game_variable_from_arg<bool>
         ( arg.get_all_of_string("--set-game-var-bool"), game_var_assignment ) )
      help = bear_gettext("--set-game-var-bool: not a boolean");

  if ( arg.has_value("--set-game-var-real") )
    if ( !set_game_variable_from_arg<double>
         ( arg.get_all_of_string("--set-game-var-real"), game_var_assignment ) )
      help = bear_gettext("--set-game-var-real: not a real number");

  if ( arg.has_value("--set-game-var-string") )
    set_game_variable_from_arg<std::string>
      ( arg.get_all_of_string("--set-game-var-string"), game_var_assignment );

  if ( arg.has_value("--start-level") )
    m_game_description.set_start_level( arg.get_string("--start-level") );
  else
    help = "--start-level";

  if ( !help.empty() )
    {
      std::cout << bear_gettext("Bad argument value: '") << help
                << bear_gettext("'\n");
      arg.help();
    }
  else if ( !auto_exit )
    {
      load_libraries( arg.get_all_of_string("--item-library") );
      init_resource_pool( arg.get_all_of_string("--data-path") );
      result = true;
    }

  return result;
} // game_local_client::check_arguments()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a game variable from a command line argument string formated
 *        "name<sep>value".
 * \param args The values assigned to the command line arguments.
 * \param sep The separator between the name and the value. Must not be
 *        present in the variable's name.
 */
template<typename T>
bool bear::engine::game_local_client::set_game_variable_from_arg
( const std::list<std::string>& args, const char sep )
{
  bool result(true);
  std::list<std::string>::const_iterator it;

  for (it=args.begin(); it!=args.end(); ++it)
    {
      const std::size_t pos( it->find_first_of(sep) );

      if ( pos == std::string::npos )
        result = false;
      else
        {
          const std::string name( it->substr(0, pos) );
          const std::string value( it->substr(pos+1) );

          if ( !claw::text::is_of_type<T>(value) )
            result = false;
          else
            {
              std::istringstream iss(value);
              T v;
              iss >> v;

              m_game_variables.set<T>(name, v);
              result = true;
            }
        }
    }

  return result;
} // game_local_client::set_game_variable_from_arg()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a listener on the changes of a game variable.
 * \param name The name of the variable to listen.
 * \param f The function to call when the value of the variable change.
 */
template<typename T>
boost::signals::connection
bear::engine::game_local_client::listen_variable_change
( const std::string& name, boost::function<void (T)> f )
{
  return m_game_variables.variable_changed<T>(name).connect(f);
} // game_local_client::listen_variable_change()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the arguments table with the arguments of the engine.
 */
claw::arguments_table bear::engine::game_local_client::get_arguments_table()
{
  claw::arguments_table arg( bear_gettext("Engine's options:") );

  arg.add_long
    ( "--game-name", bear_gettext("The name of the game."), true,
      bear_gettext("string") );
  arg.add_long
    ( "--active-area",
      bear_gettext
      ("The margin around the camera in which we check for activity."), true,
      bear_gettext("integer") );
  arg.add_long
    ( "--screen-width", bear_gettext("The width of the screen."), true,
      bear_gettext("integer") );
  arg.add_long
    ( "--screen-height", bear_gettext("The height of the screen."), true,
      bear_gettext("integer") );
  arg.add_long
    ( "--fullscreen", bear_gettext("Run the game in fullscreen mode."), true );
  arg.add_long( "--windowed", bear_gettext("Run the game in a window."), true );
  arg.add_long
    ( "--data-path",
      bear_gettext("Path to the directory containing the data of the game."),
      false, bear_gettext("path") );
  arg.add_long
    ( "--item-library",
      bear_gettext("Path to a library containing items for the game."), false,
      bear_gettext("path") );
  arg.add_long
    ( "--start-level", bear_gettext("The path of the first level to run."),
      false, bear_gettext("string") );
  arg.add_long
    ( "--set-game-var-int",
      bear_gettext("Set the value of an integer game variable."), true,
      bear_gettext("name=value") );
  arg.add_long
    ( "--set-game-var-uint",
      bear_gettext("Set the value of a non negative integer game variable."),
      true, bear_gettext("name=value") );
  arg.add_long
    ( "--set-game-var-bool",
      bear_gettext("Set the value of a boolean game variable."), true,
      bear_gettext("name=value") );
  arg.add_long
    ( "--set-game-var-real",
      bear_gettext("Set the value of a real number game variable."), true,
      bear_gettext("name=value") );
  arg.add_long
    ( "--set-game-var-string",
      bear_gettext("Set the value of a string game variable."), true,
      bear_gettext("name=value") );
  arg.add_long
    ( "--game-var-assignment",
      bear_gettext
      ("Change the delimiter used in --set-game-var-<type> to separate the name"
       " and the value of the variable."), true,
      bear_gettext("character") );
  arg.add_long
    ( "--stats-destination",
      bear_gettext
      ("Set the value of the destination to which game statistics are sent."),
      true );
  arg.add
    ( "-v", "--version",
      bear_gettext("Print the version of the engine and exit."),
      true );

  return arg;
} // game_local_client::get_arguments_table()

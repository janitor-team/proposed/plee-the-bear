/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file scene_visual.cpp
 * \brief Implementation of the bear::engine::scene_visual class.
 * \author Julien Jorge
 */
#include "engine/scene_visual.hpp"

#include "visual/scene_sprite.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Compare two scene_visual instances on their z_position.
 * \return s1.z_position < s2.z_position
 */
bool bear::engine::scene_visual::z_position_compare::operator()
( const scene_visual& s1, const scene_visual& s2 ) const
{
  return s1.z_position < s2.z_position;
} // scene_visual::z_position_compare::operator()()




/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param x X-coordinate of the sprite in the world.
 * \param y Y-coordinate of the sprite in the world.
 * \param spr The sprite to display.
 * \param z The position of the visual in the render procedure.
 */
bear::engine::scene_visual::scene_visual
( universe::coordinate_type x, universe::coordinate_type y,
  const visual::sprite& spr, int z )
  : scene_element( visual::scene_sprite(x, y, spr) ), z_position(z)
{

} // scene_visual::scene_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param pos Position of the sprite in the world.
 * \param spr The sprite to display.
 * \param z The position of the visual in the render procedure.
 */
bear::engine::scene_visual::scene_visual
( const universe::position_type& pos,
  const visual::sprite& spr, int z )
  : scene_element( visual::scene_sprite(pos.x, pos.y, spr) ), z_position(z)
{

} // scene_visual::scene_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param pos Position of the sprite in the world.
 * \param e The element to display.
 * \param z The position of the visual in the render procedure.
 */
bear::engine::scene_visual::scene_visual
( const visual::scene_element& e, int z )
  : scene_element(e), z_position(z)
{

} // scene_visual::scene_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param pos Position of the sprite in the world.
 * \param e The element to display.
 * \param z The position of the visual in the render procedure.
 */
bear::engine::scene_visual::scene_visual
( const visual::base_scene_element& e, int z )
  : scene_element(e), z_position(z)
{

} // scene_visual::scene_visual()

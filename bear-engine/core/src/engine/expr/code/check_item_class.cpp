/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file check_item_class.cpp
 * \brief Implementation of the bear::engine::check_item_class class.
 * \author Julien Jorge.
 */
#include "engine/expr/check_item_class.hpp"

#include "engine/base_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the name of the class.
 * \param class_name The name of the class.
 */
void bear::engine::check_item_class::set_class_name
( const std::string& class_name )
{
  m_class_name = class_name;
} // check_item_class::set_class_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the class.
 */
const std::string& bear::engine::check_item_class::get_class_name() const
{
  return m_class_name;
} // check_item_class::get_class_name()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the data on the colliding item.
 * \param data The data on the colliding item.
 */
void bear::engine::check_item_class::set_collision_data
( const collision_in_expression& data )
{
  m_collision = data;
} // check_item_class::set_collision_data()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the data on the colliding item.
 */
const bear::engine::collision_in_expression&
bear::engine::check_item_class::get_collision_data() const
{
  return m_collision;
} // check_item_class::get_collision_data()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::expr::base_boolean_expression*
bear::engine::check_item_class::clone() const
{
  return new check_item_class(*this);
} // check_item_class::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::engine::check_item_class::evaluate() const
{
  if ( m_collision == NULL )
    return false;
  else
    return m_collision->get_class_name() == m_class_name;
} // check_item_class::evaluate()

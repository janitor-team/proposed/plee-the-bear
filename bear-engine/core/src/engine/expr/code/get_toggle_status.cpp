/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file get_toggle_status.cpp
 * \brief Implementation of the bear::engine::get_toggle_status class.
 * \author Julien Jorge.
 */
#include "engine/expr/get_toggle_status.hpp"

#include "engine/base_item.hpp"
#include "engine/item_brick/with_toggle.hpp"
#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the toggle from which we take the status.
 * \param t The toggle.
 */
void bear::engine::get_toggle_status::set_toggle( const base_item& item )
{
  m_toggle = &item;
} // get_toggle_status::set_toggle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the toggle from which we take the status.
 */
const bear::engine::base_item*
bear::engine::get_toggle_status::get_toggle() const
{
  return m_toggle.get_item();
} // get_toggle_status::get_toggle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::expr::base_boolean_expression*
bear::engine::get_toggle_status::clone() const
{
  return new get_toggle_status(*this);
} // get_toggle_status::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::engine::get_toggle_status::evaluate() const
{
  if ( m_toggle == (with_toggle*)NULL )
    {
      claw::logger << claw::log_warning
                   << "get_toggle_status: the toggle is NULL, the evaluation"
        " is 'false'." << std::endl;

      return false;
    }
  else
    return m_toggle->is_on();
} // get_toggle_status::evaluate()

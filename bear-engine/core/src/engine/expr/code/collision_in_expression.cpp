/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file collision_in_expression.cpp
 * \brief Implementation of the bear::engine::collision_in_expression class.
 * \author Julien Jorge
 */
#include "engine/expr/collision_in_expression.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::collision_in_expression::collision_in_expression()
  : m_colliding_item(NULL), m_collision_info(NULL)
{

} // collision_in_expression::collision_in_expression()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param item A pointer on the pointer that will be updated with the colliding
 *        item.
 * \param info A pointer on the pointer that will be updated with the collision
 *        info.
 */
bear::engine::collision_in_expression::collision_in_expression
( const_item_pointer const* item, const_collision_info_pointer const* info )
  : m_colliding_item(item), m_collision_info(info)
{

} // collision_in_expression::collision_in_expression()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is a collision data in this instance.
 */
bool bear::engine::collision_in_expression::is_valid() const
{
  return (m_colliding_item != NULL ) && (m_collision_info != NULL);
} // collision_in_expression::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a pointer on the colliding item.
 */
const bear::engine::base_item*
bear::engine::collision_in_expression::get_item_ptr() const
{
  return *m_colliding_item;
} // collision_in_expression::get_item_ptr()

/*----------------------------------------------------------------------------*/
/**
 * \brief Member by pointer. Returns a pointer on the colliding item.
 */
const bear::engine::base_item*
bear::engine::collision_in_expression::operator->() const
{
  return get_item_ptr();
} // collision_in_expression::operator->()

/*----------------------------------------------------------------------------*/
/**
 * \brief Indirection. Returns the colliding item.
 */
const bear::engine::base_item&
bear::engine::collision_in_expression::operator*() const
{
  return **m_colliding_item;
} // collision_in_expression::operator*()

/*----------------------------------------------------------------------------*/
/**
 * \brief Equality. Compares the data in collision.
 * \param that The pointer to compare to.
 */
bool
bear::engine::collision_in_expression::operator==( base_item const* that ) const
{
  return *m_colliding_item == that;
} // collision_in_expression::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Disquality. Compares the data in collision.
 * \param that The pointer to compare to.
 */
bool
bear::engine::collision_in_expression::operator!=( base_item const* that ) const
{
  return !operator==(that);
} // collision_in_expression::operator!=()

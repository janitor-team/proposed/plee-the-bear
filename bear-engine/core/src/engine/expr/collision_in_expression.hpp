/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file collision_in_expression.hpp
 * \brief A class that stores some informations on a collision to be used in an
 *        expression.
 * \author Julien Jorge
 */
#ifndef __ENGINE_COLLISION_IN_EXPRESSION_HPP__
#define __ENGINE_COLLISION_IN_EXPRESSION_HPP__

#include "universe/collision_info.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class base_item;

    /**
     * \brief A class that stores some informations on a collision to be used in
     *        an expression.
     *
     * The class stores pointers on the data stored by engine::with_trigger. So,
     * this latter class can change its data and the values are automatically
     * changed in collision_in_expression.
     *
     * Moreover, this class can be used as a kind of pointer on the colliding
     * base_item, thanks to operator overloading.
     *
     * \author Julien Jorge
     */
    class ENGINE_EXPORT collision_in_expression
    {
    public:
      typedef base_item const* const_item_pointer;
      typedef universe::collision_info const* const_collision_info_pointer;

    public:
      collision_in_expression();
      collision_in_expression
      ( const_item_pointer const* item,
        const_collision_info_pointer const* info );

      bool is_valid() const;

      const base_item* get_item_ptr() const;

      const base_item* operator->() const;
      const base_item& operator*() const;
      bool operator==( base_item const* that ) const;
      bool operator!=( base_item const* that ) const;

    private:
      /** \brief The item in collision. */
      const_item_pointer const* m_colliding_item;

      /** \brief The informations about the collision. */
      const_collision_info_pointer const* m_collision_info;

    }; // class collision_in_expression

  } // namespace engine
} // namespace bear

#endif // __ENGINE_COLLISION_IN_EXPRESSION_HPP__

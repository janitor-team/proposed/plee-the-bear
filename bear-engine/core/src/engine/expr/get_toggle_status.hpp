/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file get_toggle_status.hpp
 * \brief An expression that returns the status of a toggle.
 * \author Julien Jorge
 */
#ifndef __ENGINE_GET_TOGGLE_STATUS_HPP__
#define __ENGINE_GET_TOGGLE_STATUS_HPP__

#include "expr/base_boolean_expression.hpp"
#include "universe/derived_item_handle.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class base_item;
    class with_toggle;

    /**
     * \brief An expression that returns the status of a toggle.
     *
     * \author Julien Jorge
     */
    class ENGINE_EXPORT get_toggle_status:
      public expr::base_boolean_expression
    {
    private:
      /** \brief The type of the handles on the toggles. */
      typedef
        universe::const_derived_item_handle<const with_toggle, const base_item>
        toggle_handle;

    public:
      void set_toggle( const base_item& item );
      const base_item* get_toggle() const;

      expr::base_boolean_expression* clone() const;
      bool evaluate() const;

    private:
      /** \brief The toggle to check. */
      toggle_handle m_toggle;

    }; // class get_toggle_status

  } // namespace engine
} // namespace bear

#endif // __ENGINE_GET_TOGGLE_STATUS_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file count_items_by_class_name.hpp
 * \brief An expression that returns the status of a toggle.
 * \author Julien Jorge
 */
#ifndef __ENGINE_COUNT_ITEMS_BY_CLASS_NAME_HPP__
#define __ENGINE_COUNT_ITEMS_BY_CLASS_NAME_HPP__

#include "engine/base_item.hpp"
#include "expr/base_linear_expression.hpp"

#include "universe/derived_item_handle.hpp"

#include "engine/class_export.hpp"

#include <string>

namespace bear
{
  namespace engine
  {

    /**
     * \brief An expression that returns the number of items of a given class in
     *        the world.
     *
     * \remark This expression does not check the parent classes.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT count_items_by_class_name:
      public expr::base_linear_expression
    {
    private:
      /** \brief The type of the handle on an item that will give us the
          world. */
      typedef universe::const_derived_item_handle<const base_item> item_handle;

    public:
      count_items_by_class_name();
      count_items_by_class_name( const base_item& w, const std::string& c );

      void set_world_through( const base_item& w );
      void set_class_name( const std::string& n );

      expr::base_linear_expression* clone() const;
      double evaluate() const;

    private:
      /** \brief An item in the world in which the items are searched. */
      item_handle m_world_proxy;

      /** \brief The name of the class of the items to count. */
      std::string m_class_name;

    }; // class count_items_by_class_name

  } // namespace engine
} // namespace bear

#endif // __ENGINE_COUNT_ITEMS_BY_CLASS_NAME_HPP__

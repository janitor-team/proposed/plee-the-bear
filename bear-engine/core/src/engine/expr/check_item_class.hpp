/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file check_item_class.hpp
 * \brief An expression that checks if a colliding item is an instance of a
 *        given class.
 * \author Julien Jorge
 */
#ifndef __ENGINE_CHECK_ITEM_CLASS_HPP__
#define __ENGINE_CHECK_ITEM_CLASS_HPP__

#include "expr/base_boolean_expression.hpp"
#include "engine/expr/collision_in_expression.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief An expression that checks if a colliding item is an instance of a
     *        given class.
     *
     * \remark This expression does not check the parent classes.
     * \sa check_item_class_hierarchy
     * \author Julien Jorge
     */
    class ENGINE_EXPORT check_item_class:
      public expr::base_boolean_expression
    {
    public:
      void set_class_name( const std::string& class_name );
      const std::string& get_class_name() const;

      void set_collision_data( const collision_in_expression& data );
      const collision_in_expression& get_collision_data() const;

      expr::base_boolean_expression* clone() const;
      bool evaluate() const;

    private:
      /** \brief The name of the class. */
      std::string m_class_name;

      /** \brief The data on the colliding item. */
      collision_in_expression m_collision;

    }; // class check_item_class

  } // namespace engine
} // namespace bear

#endif // __ENGINE_CHECK_ITEM_CLASS_HPP__

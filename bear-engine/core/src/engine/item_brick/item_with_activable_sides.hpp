/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_activable_sides.hpp
 * \brief An item with a boolean associated to each side.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_WITH_ACTIVABLE_SIDES_HPP__
#define __ENGINE_ITEM_WITH_ACTIVABLE_SIDES_HPP__

#include <string>

namespace bear
{
  namespace engine
  {
    /**
     * \brief An item with a boolean associated to each side.
     *
     * \b template \b parameters :
     * - \a Base: the base class for this item. Must inherit from
     *    engine::base_item,
     *
     * The custom fields of this class are:
     * - left_side_is_active: bool, tell if the left side is active
     *   (default = false),
     * - right_side_is_active: bool, tell if the right side is active
     *   (default = false),
     * - top_side_is_active: bool, tell if the top side is active
     *   (default = false),
     * - bottom_side_is_active: bool, tell if the bottom side is active
     *   (default = false).
     *
     * \author Julien Jorge
     */
    template<class Base>
    class item_with_activable_sides:
      public Base
    {
      typedef Base super;

    public:
      item_with_activable_sides();

      bool set_bool_field( const std::string& name, bool value );

      bool left_side_is_active() const;
      bool right_side_is_active() const;
      bool top_side_is_active() const;
      bool bottom_side_is_active() const;

    private:
      /** \brief Tell if the left side is solid. */
      bool m_left_side_is_active;

      /** \brief Tell if the right side is solid. */
      bool m_right_side_is_active;

      /** \brief Tell if the top side is solid. */
      bool m_top_side_is_active;

      /** \brief Tell if the bottom side is solid. */
      bool m_bottom_side_is_active;

    }; // class item_with_activable_sides
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_activable_sides.tpp"

#endif // __ENGINE_ITEM_WITH_ACTIVABLE_SIDES_HPP__

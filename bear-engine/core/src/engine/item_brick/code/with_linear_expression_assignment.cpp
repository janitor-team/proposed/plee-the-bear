/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file with_linear_expression_assignment.cpp
 * \brief Implementation of the bear::engine::with_linear_expression_assignment
 *        class.
 * \author Julien Jorge
 */
#include "engine/item_brick/with_linear_expression_assignment.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::engine::with_linear_expression_assignment::
~with_linear_expression_assignment
()
{
  // nothing to do
} // with_linear_expression_assignment::~with_linear_expression_assignment()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the expression.
 * \param e The expression to assign to the item.
 */
void bear::engine::with_linear_expression_assignment::set_expression
( const expr::linear_expression& e )
{
  do_set_expression(e);
} // with_linear_expression_assignment::set_expression()

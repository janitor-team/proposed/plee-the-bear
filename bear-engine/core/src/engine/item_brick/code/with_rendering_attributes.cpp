/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file engine/item_brick/code/with_rendering_attributes.cpp
 * \brief Implementation of the bear::engine::with_rendering_attributes class.
 * \author Julien Jorge
 */
#include "engine/item_brick/with_rendering_attributes.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the rendering attributes.
 */
bear::visual::bitmap_rendering_attributes&
bear::engine::with_rendering_attributes::get_rendering_attributes()
{
  return m_rendering_attributes;
} // with_rendering_attributes::get_rendering_attributes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the rendering attributes.
 */
const bear::visual::bitmap_rendering_attributes&
bear::engine::with_rendering_attributes::get_rendering_attributes() const
{
  return m_rendering_attributes;
} // with_rendering_attributes::get_rendering_attributes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the rendering attributes.
 * \param attr The rendering attributes.
 */
void bear::engine::with_rendering_attributes::set_rendering_attributes
( const visual::bitmap_rendering_attributes& attr )
{
  m_rendering_attributes = attr;
} // with_rendering_attributes::set_rendering_attributes()

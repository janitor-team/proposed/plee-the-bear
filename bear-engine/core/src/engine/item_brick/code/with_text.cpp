/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file engine/brick/code/with_text.cpp
 * \brief Implementation of the bear::engine::with_text class.
 * \author Julien Jorge
 */
#include "engine/item_brick/with_text.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::engine::with_text::with_text()
  : m_font(NULL)
{

} // with_text::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the text of the item.
 * \param text The text.
 */
void bear::engine::with_text::set_text( const std::string& text )
{
  m_text = text;
  refresh_writing();
} // with_text::set_text()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the font of the text.
 * \param f The font.
 */
void bear::engine::with_text::set_font( const visual::font& f )
{
  m_font = f;
  refresh_writing();
} // with_text::set_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the text of the item.
 */
const std::string& bear::engine::with_text::get_text() const
{
  return m_text;
} // with_text::get_text()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the font of the text.
 */
const bear::visual::font& bear::engine::with_text::get_font() const
{
  return m_font;
} // with_text::get_font()

/*----------------------------------------------------------------------------*/
/**
 * \brief Return the writing of the text with the font.
 */
const bear::visual::writing& bear::engine::with_text::get_writing() const
{
  return m_writing;
} // with_text::get_writing()

/*----------------------------------------------------------------------------*/
/**
 * \brief Fit the text in a box.
 * \param s The bounds of the text.
 */
void bear::engine::with_text::fit_in_box( const universe::size_box_type& s )
{
  m_writing.create( m_font, m_text, s );
} // with_text::fit_in_box()

/*----------------------------------------------------------------------------*/
/**
 * \brief Refresh the content of the writing.
 */
void bear::engine::with_text::refresh_writing()
{
  m_writing.create( m_font, m_text );
} // with_text::refresh_writing()

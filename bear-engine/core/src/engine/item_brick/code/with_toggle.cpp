/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file engine/brick/code/with_toggle.cpp
 * \brief Implementation of the bear::engine::with_toggle class.
 * \author Julien Jorge
 */
#include "engine/item_brick/with_toggle.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Change the state of the toggle.
 * \param activator The item that activates the toggle, if any.
 */
void bear::engine::with_toggle::toggle( base_item* activator )
{
  toggle( !is_on(), activator );
} // with_toggle::toggle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Change the state of the toggle.
 * \param b The new state.
 * \param activator The item that activates the toggle, if any.
 */
void bear::engine::with_toggle::toggle( bool b, base_item* activator )
{
  if ( b )
    {
      if ( !is_on() )
        toggle_on( activator );
    }
  else if ( is_on() )
    toggle_off( activator );
} // with_toggle::toggle()


/*----------------------------------------------------------------------------*/
/**
 * \brief Turn the toggle on.
 * \param activator The item that activates the toggle, if any.
 */
void bear::engine::with_toggle::toggle_on( base_item* activator )
{
  // nothing to do
} // with_toggle::toggle_on()

/*----------------------------------------------------------------------------*/
/*
 * \brief Turn the toggle on.
 * \param activator The item that activates the toggle, if any.
 */
void bear::engine::with_toggle::toggle_off( base_item* activator )
{
  // nothing to do
} // with_toggle::toggle_off()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the toggle is turned on.
 */
bool bear::engine::with_toggle::is_on() const
{
  return false;
} // with_toggle::is_on()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file with_boolean_expression_creation.cpp
 * \brief Implementation of the bear::engine::with_boolean_expression_creation
 *        class.
 * \author Julien Jorge
 */
#include "engine/item_brick/with_boolean_expression_creation.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::
engine::with_boolean_expression_creation::~with_boolean_expression_creation()
{
  // nothing to do
} // with_boolean_expression_creation::~with_boolean_expression_creation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the expression.
 */
bear::expr::boolean_expression
bear::engine::with_boolean_expression_creation::get_expression() const
{
  return do_get_expression();
} // with_boolean_expression_creation::get_expression()

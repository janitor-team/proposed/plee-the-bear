/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_z_shift.hpp
 * \brief An item that shift other items on the z axis.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_WITH_Z_SHIFT_HPP__
#define __ENGINE_ITEM_WITH_Z_SHIFT_HPP__

#include "engine/base_item.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief An item that shift other items on the z axis.
     *
     * \b template \b parameters :
     * - \a Base: the base class for this item. Must inherit from
     *    engine::base_item,
     *
     * The custom fields of this class are:
     * - z_shift: int, Relative z position to put the other item at
     *              (default = 0).
     * - force_z_position : bool, Indicates if the item force the z_position.
     *
     * \author Julien Jorge
     */
    template<class Base>
    class item_with_z_shift:
      public Base
    {
      typedef Base super;

    public:
      item_with_z_shift();

      bool set_integer_field( const std::string& name, int value );
      bool set_bool_field( const std::string& name, bool value );

    protected:
      void z_shift( base_item& that ) const;

      int get_z_shift() const;

    private:
      /** \brief Relative z position to put the other item at. */
      int m_z_shift;

      /** \brief  Indicates if the item force the z_position. */
      bool m_force_z_position;

    }; // class item_with_z_shift
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_z_shift.tpp"

#endif // __ENGINE_ITEM_WITH_Z_SHIFT_HPP__

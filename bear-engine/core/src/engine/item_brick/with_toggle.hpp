/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file with_toggle.hpp
 * \brief This class describes the interface of a two states item.
 * \author Julien Jorge
 */
#ifndef __ENGINE_WITH_TOGGLE_HPP__
#define __ENGINE_WITH_TOGGLE_HPP__

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    class base_item;

    /**
     * \brief This class describes the interface of a two states item.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT with_toggle
    {
    public:
      void toggle( base_item* activator );
      void toggle( bool b, base_item* activator );

      virtual void toggle_on( base_item* activator );
      virtual void toggle_off( base_item* activator );
      virtual bool is_on() const;

    }; // class with_toggle
  } // namespace engine
} // namespace bear

#endif // __ENGINE_WITH_TOGGLE_HPP__

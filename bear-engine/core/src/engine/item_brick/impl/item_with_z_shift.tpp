/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_z_shift.tpp
 * \brief Implementation of the bear::engine::item_with_z_shift class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
template<class Base>
bear::engine::item_with_z_shift<Base>::item_with_z_shift()
  : m_z_shift(0), m_force_z_position(false)
{

} // item_with_z_shift::item_with_z_shift()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type int.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_z_shift<Base>::set_integer_field
( const std::string& name, int value )
{
  bool ok(true);

  if (name == "item_with_z_shift.z_shift")
    m_z_shift = value;
  else
    ok = super::set_integer_field(name, value);

  return ok;
} // item_with_z_shift::set_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_z_shift<Base>::set_bool_field
( const std::string& name, bool value )
{
  bool ok(true);

  if (name == "item_with_z_shift.force_z_position")
    m_force_z_position = value;
  else
    ok = super::set_bool_field(name, value);

  return ok;
} // item_with_z_shift::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Shift a item on the z axis.
 * \param that The item to shift.
 */
template<class Base>
void bear::engine::item_with_z_shift<Base>::z_shift( base_item& that ) const
{
  if ( !that.is_z_fixed() )
    if( ((m_z_shift > 0) && ( that.get_z_position() <= this->get_z_position() ))
        || ((m_z_shift < 0)
            && ( that.get_z_position() >= this->get_z_position() ))
        || m_force_z_position )
        that.set_z_position(this->get_z_position() + m_z_shift);
} // item_with_z_shift::z_shift()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the relative z position.
 */
template<class Base>
int bear::engine::item_with_z_shift<Base>::get_z_shift() const
{
  return m_z_shift;
} // item_with_z_shift::get_z_shift()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_text.cpp
 * \brief Implementation of the bear::engine::item_with_text class.
 * \author Julien Jorge
 */

#include "engine/level_globals.hpp"
#include "visual/scene_writing.hpp"

#include <claw/logger.hpp>
#include <libintl.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
template<class Base>
bear::engine::item_with_text<Base>::item_with_text()
  : m_text_inside(false), m_stretched_text(false)
{

} // item_with_text::item_with_text()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the item.
 */
template<class Base>
void bear::engine::item_with_text<Base>::build()
{
  super::build();

  if ( (this->get_size().x == 0) && (this->get_size().y == 0) )
    this->set_size( this->get_writing().get_size() );
} // item_with_text::build()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type string.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_text<Base>::set_string_field
( const std::string& name, const std::string& value )
{
  bool ok = true;

  if (name == "item_with_text.text")
    this->set_text( gettext(value.c_str()) );
  else if ( name == "item_with_text.font" )
    {
      this->get_level_globals().load_font(value);

      if ( this->get_level_globals().font_exists(value) )
        this->set_font( this->get_level_globals().get_font(value) );
      else
        claw::logger << claw::log_warning
                     << "item_with_text: cannot open font '" << value << "'."
                     << std::endl;
    }
  else
    ok = super::set_string_field(name, value);

  return ok;
} // item_with_text::set_string_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_text<Base>::set_bool_field
( const std::string& name, bool value )
{
  bool ok = true;

  if (name == "item_with_text.text_inside")
    m_text_inside = value;
  else if (name == "item_with_text.stretched_text")
    m_stretched_text = value;
  else
    ok = super::set_bool_field(name, value);

  return ok;
} // item_with_text::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
template<class Base>
void bear::engine::item_with_text<Base>::progress
( universe::time_type elapsed_time )
{
  super::progress(elapsed_time);

  if ( m_text_inside && ( this->get_writing().get_size()!=this->get_size() ) )
    this->fit_in_box( this->get_size() );
} // item_with_text::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sprites representing the item.
 * \param visuals (out) The sprites of the item, and their positions.
 */
template<class Base>
void bear::engine::item_with_text<Base>::get_visual
( std::list<scene_visual>& visuals ) const
{
  super::get_visual(visuals);

  if ( !this->get_text().empty() )
    {
      visual::scene_writing e( 0, 0, this->get_writing() );

      if ( m_stretched_text )
        e.set_scale_factor
          ( this->get_width() / this->get_writing().get_width(),
            this->get_height() / this->get_writing().get_height() );

      this->add_visual( e, visuals );
    }
} // item_with_text::get_visual()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the text is drawn only in the bounds of the item.
 * \param b True if the text must be drawn only in the bounds of the item.
 */
template<class Base>
void bear::engine::item_with_text<Base>::set_text_inside( bool b )
{
  m_text_inside = b;

  if ( m_text_inside )
    this->fit_in_box( this->get_size() );
} // item_with_text::set_text_inside()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the text has to be stretched to fit the bounds of the item.
 * \param b True if the text must be stretched.
 */
template<class Base>
void bear::engine::item_with_text<Base>::set_stretched_text( bool b )
{
  m_stretched_text = b;
} // item_with_text::set_stretched_text()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell the item to adjust its size to the size of the text.
 */
template<class Base>
void bear::engine::item_with_text<Base>::fit_to_text()
{
  this->refresh_writing();
  this->set_size( this->get_writing().get_size() );
} // item_with_text::fit_to_text()

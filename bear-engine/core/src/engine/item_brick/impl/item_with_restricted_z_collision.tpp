/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_restricted_z_collision.tpp
 * \brief Implementation of the bear::engine::item_with_restricted_z_collision
 *        class.
 * \author Julien Jorge
 */
#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
template<class Base>
bear::engine::item_with_restricted_z_collision<Base>::
item_with_restricted_z_collision()
  : m_min_z(std::numeric_limits<int>::min()),
    m_max_z(std::numeric_limits<int>::max())
{

} // item_with_restricted_z_collision::item_with_restricted_z_collision()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type int.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_restricted_z_collision<Base>::set_integer_field
( const std::string& name, int value )
{
  bool ok(true);

  if (name == "item_with_restricted_z_collision.min_z_for_collision")
    m_min_z = value;
  else if (name == "item_with_restricted_z_collision.max_z_for_collision")
    m_max_z = value;
  else
    ok = super::set_integer_field(name, value);

  return ok;
} // item_with_restricted_z_collision::set_integer_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if the other item satisfies the condition for the collision
 *        treatment to be applied.
 * \param that The item to check.
 */
template<class Base>
bool bear::engine::item_with_restricted_z_collision<Base>::
satisfy_collision_condition( const base_item& that ) const
{
  return (that.get_z_position() >= m_min_z)
    && (that.get_z_position() <= m_max_z);
} // item_with_restricted_z_collision::satisfy_collision_condition()

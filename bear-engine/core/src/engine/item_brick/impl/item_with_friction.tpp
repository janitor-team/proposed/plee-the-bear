/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_friction.tpp
 * \brief Implementation of the bear::engine::item_with_friction class.
 * \author Sebastien Angibaud
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
template<class Base>
bear::engine::item_with_friction<Base>::item_with_friction()
  : m_left_friction(1), m_right_friction(1),
    m_top_friction(1), m_bottom_friction(1)
{

} // item_with_friction::item_with_friction()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the left friction.
 */
template<class Base>
double bear::engine::item_with_friction<Base>::get_left_friction() const
{
  return m_left_friction;
} // item_with_friction::get_left_friction()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the right friction.
 */
template<class Base>
double bear::engine::item_with_friction<Base>::get_right_friction() const
{
  return m_right_friction;
} // item_with_friction::get_right_friction()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the top friction.
 */
template<class Base>
double bear::engine::item_with_friction<Base>::get_top_friction() const
{
  return m_top_friction;
} // item_with_friction::get_top_friction()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the bottom.
 */
template<class Base>
double bear::engine::item_with_friction<Base>::get_bottom_friction() const
{
  return m_bottom_friction;
} // item_with_friction::get_bottom_friction()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type real.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_friction<Base>::set_real_field
( const std::string& name, double value )
{
  bool ok(true);

  if (name == "item_with_friction.left_friction")
    m_left_friction = value;
  else if (name == "item_with_friction.right_friction")
    m_right_friction = value;
  else if (name == "item_with_friction.top_friction")
    m_top_friction = value;
  else if (name == "item_with_friction.bottom_friction")
    m_bottom_friction = value;
  else
    ok = super::set_real_field(name, value);

  return ok;
} // invisible_block::set_real_field()

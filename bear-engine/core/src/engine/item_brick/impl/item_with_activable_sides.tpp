/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_activable_sides.tpp
 * \brief Implementation of the bear::engine::item_with_activable_sides class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
template<class Base>
bear::engine::item_with_activable_sides<Base>::item_with_activable_sides()
  : m_left_side_is_active(false), m_right_side_is_active(false),
    m_top_side_is_active(false), m_bottom_side_is_active(false)
{

} // item_with_activable_sides::item_with_activable_sides()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_activable_sides<Base>::set_bool_field
( const std::string& name, bool value )
{
  bool ok(true);

  if (name == "item_with_activable_sides.left_side_is_active")
    m_left_side_is_active = value;
  else if (name == "item_with_activable_sides.right_side_is_active")
    m_right_side_is_active = value;
  else if (name == "item_with_activable_sides.top_side_is_active")
    m_top_side_is_active = value;
  else if (name == "item_with_activable_sides.bottom_side_is_active")
    m_bottom_side_is_active = value;
  else
    ok = super::set_bool_field(name, value);

  return ok;
} // item_with_activable_sides::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the left side is active.
 */
template<class Base>
bool bear::engine::item_with_activable_sides<Base>::left_side_is_active() const
{
  return m_left_side_is_active;
} // item_with_activable_sides::left_side_is_active()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the right side is active.
 */
template<class Base>
bool bear::engine::item_with_activable_sides<Base>::right_side_is_active() const
{
  return m_right_side_is_active;
} // item_with_activable_sides::right_side_is_active()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the top side is active.
 */
template<class Base>
bool bear::engine::item_with_activable_sides<Base>::top_side_is_active() const
{
  return m_top_side_is_active;
} // item_with_activable_sides::top_side_is_active()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the bottom side is active.
 */
template<class Base>
bool
bear::engine::item_with_activable_sides<Base>::bottom_side_is_active() const
{
  return m_bottom_side_is_active;
} // item_with_activable_sides::bottom_side_is_active()

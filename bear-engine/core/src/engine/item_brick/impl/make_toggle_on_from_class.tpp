/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file make_toggle_on_from_class.tpp
 * \brief Implementation of the bear::engine::make_toggle_on_from_class class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief This method is called when the toggles switches on.
 * \param activator The item that activates the toggle, if any.
 */
template<typename Base, void (Base::*Method)()>
void bear::engine::make_toggle_on_from_class<Base, Method>::on_toggle_on
( base_item* activator )
{
  (this->*Method)();
} // make_toggle_on_from_class::on_toggle_on()




/*----------------------------------------------------------------------------*/
/**
 * \brief This method is called when the toggles switches on.
 * \param activator The item that activates the toggle, if any.
 */
template<typename Base, void (Base::*Method)() const>
void bear::engine::make_toggle_on_from_class_const<Base, Method>::on_toggle_on
( base_item* activator )
{
  (this->*Method)();
} // make_toggle_on_from_class_const::on_toggle_on()

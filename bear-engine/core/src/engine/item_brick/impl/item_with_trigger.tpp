/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_trigger.cpp
 * \brief Implementation of the bear::engine::item_with_trigger class.
 * \author Julien Jorge
 */

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
template<class Base>
bear::engine::item_with_trigger<Base>::item_with_trigger()
  : m_check_on_progress(true), m_check_on_collision(false)
{
  this->set_phantom(true);
} // item_with_trigger::item_with_trigger()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a field of type bool.
 * \param name The name of the field.
 * \param value The new value of the field.
 * \return false if the field "name" is unknow, true otherwise.
 */
template<class Base>
bool bear::engine::item_with_trigger<Base>::set_bool_field
( const std::string& name, bool value )
{
  bool ok = true;

  if (name == "item_with_trigger.check_on_collision")
    m_check_on_collision = value;
  else if (name == "item_with_trigger.check_on_progress")
    m_check_on_progress = value;
  else
    ok = super::set_bool_field(name, value);

  return ok;
} // item_with_trigger::set_bool_field()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
template<class Base>
void bear::engine::item_with_trigger<Base>::progress
( universe::time_type elapsed_time )
{
  super::progress(elapsed_time);
  progress_trigger(elapsed_time);
} // item_with_trigger::progress()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check the condition and modify the toggle if true.
 * \param activator The item that activates the toggle.
 */
template<class Base>
void
bear::engine::item_with_trigger<Base>::check_and_toggle( base_item* activator )
{
  check_condition(activator);
} // item_with_trigger::check_and_toggle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do one iteration in the progression of the item.
 * \param elapsed_time Elapsed time since the last call.
 */
template<class Base>
void bear::engine::item_with_trigger<Base>::progress_trigger
( universe::time_type elapsed_time )
{
  if ( m_check_on_progress )
    check_and_toggle(this);
} // item_with_trigger::progress_trigger()

/*----------------------------------------------------------------------------*/
/**
 * \brief A collision occured with an other item.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
template<class Base>
void bear::engine::item_with_trigger<Base>::collision_trigger
( base_item& that, universe::collision_info& info )
{
  if ( m_check_on_collision )
    {
      set_collision_data(that, info);
      check_condition(&that);
      clear_collision_data();
    }
} // item_with_trigger::collision_trigger()

/*----------------------------------------------------------------------------*/
/**
 * \brief A collision occured with an other item.
 * \param that The other item of the collision.
 * \param info Some informations about the collision.
 */
template<class Base>
void bear::engine::item_with_trigger<Base>::collision
( base_item& that, universe::collision_info& info )
{
  collision_trigger(that, info);
} // item_with_trigger::collision()

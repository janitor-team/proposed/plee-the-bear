/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file with_input_reader.cpp
 * \brief Implementation of the bear::engine::with_input_reader.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Read inputs and apply them to the item.
 * \param elapsed_time Elapsed time since the last call.
 */
template<class Base>
void bear::engine::item_with_input_listener<Base>::progress_input_reader
( universe::time_type elapsed_time )
{
  m_elapsed_time = elapsed_time;
  m_input_status.read();
  m_input_status.scan_inputs( *this );
} // item_with_input_listener::progress_input_reader()

/*----------------------------------------------------------------------------*/
/**
 * \brief A keyboard key is maintained.
 * \param elapsed_time How long the key has been pressed.
 * \param key The code of the key.
 */
template<class Base>
bool bear::engine::item_with_input_listener<Base>::key_maintained
( universe::time_type elapsed_time, const input::key_info& key )
{
  return false;
} // item_with_input_listener::key_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief A joystick button is maintained.
 * \param elapsed_time How long the button has been pressed.
 * \param button The code of the button.
 * \param joy_index The index of the joystick.
 */
template<class Base>
bool bear::engine::item_with_input_listener<Base>::button_maintained
( universe::time_type elapsed_time, input::joystick::joy_code button,
  unsigned int joy_index )
{
  return false;
} // item_with_input_listener::button_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief A mouse button is maintained.
 * \param elapsed_time How long the button has been pressed.
 * \param button The code of the button.
 * \param pos The position of the cursor on the screen.
 */
template<class Base>
bool bear::engine::item_with_input_listener<Base>::mouse_maintained
( universe::time_type elapsed_time, input::mouse::mouse_code button,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  return false;
} // item_with_input_listener::mouse_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief A keyboard key is maintained.
 * \param key The code of the key.
 */
template<class Base>
bool bear::engine::item_with_input_listener<Base>::key_maintained
( const input::key_info& key )
{
  return key_maintained(m_elapsed_time, key);
} // item_with_input_listener::key_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief A joystick button is maintained.
 * \param button The code of the button.
 * \param joy_index The index of the joystick.
 */
template<class Base>
bool bear::engine::item_with_input_listener<Base>::button_maintained
( input::joystick::joy_code button, unsigned int joy_index )
{
  return button_maintained( m_elapsed_time, button, joy_index );
} // item_with_input_listener::button_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief A mouse button is maintained.
 * \param button The code of the button.
 * \param pos The position of the cursor on the screen.
 */
template<class Base>
bool bear::engine::item_with_input_listener<Base>::mouse_maintained
( input::mouse::mouse_code button,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  return mouse_maintained( m_elapsed_time, button, pos );
} // item_with_input_listener::mouse_maintained()

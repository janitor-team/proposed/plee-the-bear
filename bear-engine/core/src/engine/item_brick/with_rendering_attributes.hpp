/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file engine/item_brick/with_rendering_attributes.hpp
 * \brief An item with a visual::bitmap_rendering_attributes.
 * \author Julien Jorge
 */
#ifndef __ENGINE_WITH_RENDERING_ATTRIBUTES_HPP__
#define __ENGINE_WITH_RENDERING_ATTRIBUTES_HPP__

#include "visual/bitmap_rendering_attributes.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief An item with a visual::bitmap_rendering_attributes.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT with_rendering_attributes
    {
    public:
      visual::bitmap_rendering_attributes& get_rendering_attributes();
      const visual::bitmap_rendering_attributes&
        get_rendering_attributes() const;

      void set_rendering_attributes
        ( const visual::bitmap_rendering_attributes& attr );

    private:
      /** \brief Global rendering attributes of the item. */
      visual::bitmap_rendering_attributes m_rendering_attributes;

    }; // class with_rendering_attributes
  } // namespace engine
} // namespace bear

#endif // __ENGINE_WITH_RENDERING_ATTRIBUTES_HPP__

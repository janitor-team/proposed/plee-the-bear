/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file with_text.hpp
 * \brief Inherit from this class to add some text in your item.
 * \author Julien Jorge
 */
#ifndef __ENGINE_WITH_TEXT_HPP__
#define __ENGINE_WITH_TEXT_HPP__

#include "universe/types.hpp"
#include "visual/font.hpp"
#include "visual/writing.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief Inherit from this class to add some text in your item.
     * \author Julien Jorge
     */
    class ENGINE_EXPORT with_text
    {
    public:
      with_text();

      void set_text( const std::string& text );
      void set_font( const visual::font& spr );

      const std::string& get_text() const;
      const visual::font& get_font() const;

      const visual::writing& get_writing() const;

      void fit_in_box( const universe::size_box_type& s );

      void refresh_writing();

    private:
      /** \brief The text. */
      std::string m_text;

      /** \brief The font used to display the text. */
      visual::font m_font;

      /** \brief The writing displayed on the screen. */
      visual::writing m_writing;

    }; // class with_text
  } // namespace engine
} // namespace bear

#endif // __ENGINE_WITH_TEXT_HPP__

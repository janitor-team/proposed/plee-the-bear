/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file make_autokill_from_class.hpp
 * \brief This class calls a method of an existing class in its build, then
 *        commit suicide.
 * \author Julien Jorge
 */
#ifndef __ENGINE_MAKE_AUTOKILL_FROM_CLASS_HPP__
#define __ENGINE_MAKE_AUTOKILL_FROM_CLASS_HPP__

namespace bear
{
  namespace engine
  {
    /**
     * \brief This class calls a method of an existing class in its build, then
     *        commit suicide.
     *
     * \b Template \b parameters:
     * - \a Base The base class, in which we call the method.
     * - \a Method The method to call in Base.
     *
     * \author Julien Jorge
     */
    template<typename Base, void (Base::*Method)()>
    class make_autokill_from_class:
      public Base
    {
    private:
      void build();

    }; // class make_autokill_from_class

    /**
     * \brief This class calls a const method of an existing class in its build,
     *        then commit suicide.
     *
     * \b Template \b parameters:
     * - \a Base The base class, in which we call the method.
     * - \a Method The method to call in Base.
     *
     * \author Julien Jorge
     */
    template<typename Base, void (Base::*Method)() const>
    class make_autokill_from_class_const:
      public Base
    {
    private:
      void build();

    }; // class make_autokill_from_class_const

  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/make_autokill_from_class.tpp"

#endif // __ENGINE_MAKE_AUTOKILL_FROM_CLASS_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_trigger.hpp
 * \brief A trigger to control some toggles.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_WITH_TRIGGER_HPP__
#define __ENGINE_ITEM_WITH_TRIGGER_HPP__

#include "engine/item_brick/with_trigger.hpp"
#include "universe/types.hpp"

#include <list>

namespace bear
{
  namespace engine
  {
    /**
     * \brief A trigger to control some toggles.
     *
     * \b template \b parameters :
     * - \a Base: the base class for this item.
     *
     * The custom fields of this class are:
     * - toggle: item list, the toggles controlled by this trigger
     *   (default = none),
     * - check_on_progress: bool, check the condition at each progress
     *   (default = true),
     * - check_on_collision: bool, check the condition at each collision
     *   (default = false),
     * - mode: string in {one_way, switch, condition}, defines the behaviour of
     *   the trigger (see item_with_trigger::mode) (default = one_way),
     * - any field supported by the parent classes.
     *
     * \author Julien Jorge
     */
    template<class Base>
    class item_with_trigger:
      public Base,
      public with_trigger
    {
      typedef Base super;

    public:
      item_with_trigger();

      bool set_bool_field( const std::string& name, bool value );

      void progress( universe::time_type elapsed_time );

    protected:
      void check_and_toggle( base_item* activator );
      void progress_trigger( universe::time_type elapsed_time );
      void collision_trigger( base_item& that, universe::collision_info& info );

    private:
      void collision( base_item& that, universe::collision_info& info );

    private:
      /** \brief Tell if the condition is checked at each progress. */
      bool m_check_on_progress;

      /** \brief Tell if the condition is checked at each collision. */
      bool m_check_on_collision;

    }; // class item_with_trigger
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_trigger.tpp"

#endif // __ENGINE_ITEM_WITH_TRIGGER_HPP__

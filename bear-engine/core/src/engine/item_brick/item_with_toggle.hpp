/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_toggle.hpp
 * \brief An item that can be toggled on or off.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_WITH_TOGGLE_HPP__
#define __ENGINE_ITEM_WITH_TOGGLE_HPP__

#include "audio/sample.hpp"
#include "engine/item_brick/with_toggle.hpp"
#include "universe/types.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief An item that can be toggled on or off.
     *
     * \b template \b parameters :
     * - \a Base: the base class for this item. Must inherit from base_item.
     *
     * The custom fields of this class are:
     * - initial_state: bool, the initial state (default = off/false),
     * - delay: real, the delay, in seconds, before turning off automatically
     *   (default = infinity),
     * - any field supported by the parent classes.
     *
     * \author Julien Jorge
     */
    template<class Base>
    class item_with_toggle:
      public Base,
      public with_toggle
    {
      typedef Base super;

    public:
      item_with_toggle();
      item_with_toggle( const item_with_toggle<Base>& that );
      ~item_with_toggle();

      bool set_real_field( const std::string& name, double value );
      bool set_bool_field( const std::string& name, bool value );
      bool set_sample_field( const std::string& name, audio::sample* value );

      void build();
      void progress( universe::time_type elapsed_time );

      void set_delay( universe::time_type d );
      void set_sample( audio::sample* s );
      void set_fadeout( universe::time_type d );

      bool is_on() const;
      void toggle_on( base_item* activator );
      void toggle_off( base_item* activator );

    protected:
      void progress_toggle( universe::time_type elapsed_time );

      virtual void build_on();
      virtual void build_off();
      virtual void on_toggle_on( base_item* activator );
      virtual void on_toggle_off( base_item* activator );
      virtual void progress_on( universe::time_type elapsed_time );
      virtual void progress_off( universe::time_type elapsed_time );

    private:
      // not implemented
      item_with_toggle<Base>& operator=( const item_with_toggle<Base>& that );

    private:
      void play_sound() const;
      void stop_sound() const;
      void progress_sound() const;

    private:
      /** \brief Tell if the item is turned on. */
      bool m_is_on;

      /** \brief Time elapsed since the item has been turned on. */
      universe::time_type m_elapsed_time;

      /** \brief Delay before turning the item off. */
      universe::time_type m_delay;

      /** \brief Fade out the sample during this duration when stopping. */
      universe::time_type m_fadeout;

      /** \brief The sample played when the toggle is on. */
      audio::sample* m_sample;

    }; // class item_with_toggle
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_toggle.tpp"

#endif // __ENGINE_ITEM_WITH_TOGGLE_HPP__

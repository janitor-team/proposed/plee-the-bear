/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_text.hpp
 * \brief An item with a text inside.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_WITH_TEXT_HPP__
#define __ENGINE_ITEM_WITH_TEXT_HPP__

#include "engine/scene_visual.hpp"
#include "engine/item_brick/with_text.hpp"
#include "universe/types.hpp"
#include "visual/font.hpp"
#include "visual/writing.hpp"

#include <list>

namespace bear
{
  namespace engine
  {
    /**
     * \brief An item with a text inside.
     *
     * \b template \b parameters :
     * - \a Base: the base class for this item. Must inherit from
     *    basic_renderable_item.
     *
     * The custom fields of this class are:
     * - text: string, the text in the item (default = empty string),
     * - font: string, the font to use to display the text (default = none),
     * - text_inside: boolean, tell if the text is drawn only in the bounds of
     *   the item (default = false),
     * - stretched_text: boolean, tell if the text has to be stretched to fit
     *   the bounds of the item (default = false),
     * - any field supported by the parent classes.
     *
     * \author Julien Jorge
     */
    template<class Base>
    class item_with_text:
      public Base,
      public with_text
    {
      typedef Base super;

    public:
      item_with_text();

      void build();

      bool set_string_field
      ( const std::string& name, const std::string& value );
      bool set_bool_field( const std::string& name, bool value );

      void progress( universe::time_type elapsed_time );
      void get_visual( std::list<scene_visual>& visuals ) const;

      void set_text_inside( bool b );
      void set_stretched_text( bool b );
      void fit_to_text();

    private:
      /** \brief Tell if the text is drawn only in the bounds of the item. */
      bool m_text_inside;

      /** \brief Tell if the text has to be stretched to fit the bounds of the
          item. */
      bool m_stretched_text;

    }; // class item_with_text
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_text.tpp"

#endif // __ENGINE_ITEM_WITH_TEXT_HPP__

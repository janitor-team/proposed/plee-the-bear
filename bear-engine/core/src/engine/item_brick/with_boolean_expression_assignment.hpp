/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file with_boolean_expression_assignment.hpp
 * \brief An utility class to serve as a base class for items that can
 *        receive boolean expressions.
 * \author Julien Jorge
 */
#ifndef __ENGINE_WITH_BOOLEAN_EXPRESSION_ASSIGNMENT_HPP__
#define __ENGINE_WITH_BOOLEAN_EXPRESSION_ASSIGNMENT_HPP__

#include "expr/boolean_expression.hpp"

#include "engine/class_export.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief An utility class to serve as a base class for items that can
     *        receive boolean expressions.
     *
     * \author Julien Jorge
     */
    class ENGINE_EXPORT with_boolean_expression_assignment
    {
    public:
      /** \brief The type of the expression created. */
      typedef expr::boolean_expression expression_type;

    public:
      virtual ~with_boolean_expression_assignment();

      void set_expression( const expr::boolean_expression& e );

    private:
      virtual void do_set_expression( const expr::boolean_expression& e ) = 0;

    }; // class with_boolean_expression_assignment

  } // namespace engine
} // namespace bear

#endif // __ENGINE_WITH_BOOLEAN_EXPRESSION_ASSIGNMENT_HPP__

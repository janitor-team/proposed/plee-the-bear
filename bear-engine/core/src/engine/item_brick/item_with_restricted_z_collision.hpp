/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_restricted_z_collision.hpp
 * \brief An item that check if the z-position of the other item is in a given
 *        interval before aligning it.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_WITH_RESTRICTED_Z_COLLISION_HPP__
#define __ENGINE_ITEM_WITH_RESTRICTED_Z_COLLISION_HPP__

#include "engine/base_item.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief An item that check if the z-position of the other item is in a
     *        given interval before aligning it.
     *
     * \b template \b parameters :
     * - \a Base: the base class for this item. Must inherit from
     *    engine::base_item,
     *
     * The custom fields of this class are:
     * - min_z_for_collision: int, minimum z-position of the other item to align
     *   it (default = -inf.),
     * - max_z_for_collision: int, maximum z-position of the other item to align
     *   it (default = -inf.).
     *
     * \author Julien Jorge
     */
    template<class Base>
    class item_with_restricted_z_collision:
      public Base
    {
      typedef Base super;

    public:
      item_with_restricted_z_collision();

      bool set_integer_field( const std::string& name, int value );

    protected:
      bool satisfy_collision_condition( const base_item& that ) const;

    private:
      /** \brief Minimum z-position to be aligned. */
      int m_min_z;

      /** \brief Maximum z-position to be aligned. */
      int m_max_z;

    }; // class item_with_restricted_z_collision
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_restricted_z_collision.tpp"

#endif // __ENGINE_ITEM_WITH_RESTRICTED_Z_COLLISION_HPP__

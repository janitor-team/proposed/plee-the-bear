/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_friction.hpp
 * \brief An item that have friction on its sides.
 * \author Sebastien Angibaud
 */
#ifndef __ENGINE_ITEM_WITH_FRICTION_HPP__
#define __ENGINE_ITEM_WITH_FRICTION_HPP__

#include "engine/base_item.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief An item that have friction its sides.
     *
     * \b template \b parameters :
     * - \a Base: the base class for this item. Must inherit from
     *    engine::base_item,
     *
     * The custom fields of this class are:
     * - left_friction: real, the friction applied to the items in contact
     *   on the left (default = 1).
     * - right_friction: real, the friction applied to the items in contact
     *   on the right (default = 1).
     * - top_friction: real, the friction applied to the items in contact
     *   on the top (default = 1).
     * - bottom_friction: real, the friction applied to the items in contact
     *   on the bottom (default = 1).

     *
     * \author Sebastien Angibaud
     */
    template<class Base>
    class item_with_friction:
      public Base
    {
      typedef Base super;

    public:
      item_with_friction();

      bool set_real_field( const std::string& name, double value );

    protected:
      double get_left_friction() const;
      double get_right_friction() const;
      double get_top_friction() const;
      double get_bottom_friction() const;

    private:
    /** \brief The friction applied to the item colliding on the left. */
    double m_left_friction;

    /** \brief The friction applied to the item colliding on the right. */
    double m_right_friction;

    /** \brief The friction applied to the item colliding on the top. */
    double m_top_friction;

    /** \brief The friction applied to the item colliding on the bottom. */
    double m_bottom_friction;

    }; // class item_with_friction
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_friction.tpp"

#endif // __ENGINE_ITEM_WITH_FRICTION_HPP__

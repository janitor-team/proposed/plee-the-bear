/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_with_input_reader.hpp
 * \brief Inherit from this class to allow your item to read the player's
 *        inputs.
 * \author Julien Jorge
 */
#ifndef __ENGINE_ITEM_WITH_INPUT_LISTENER_HPP__
#define __ENGINE_ITEM_WITH_INPUT_LISTENER_HPP__

#include "input/input_listener.hpp"
#include "input/input_status.hpp"
#include "universe/types.hpp"

namespace bear
{
  namespace engine
  {
    /**
     * \brief Inherit from this class to allow your item to read the controller
     *        inputs.
     *
     * \b template \b parameters :
     * - \a Base : the base class for this item. Must inherit from
     *    engine::base_item.
     *
     * There is no custom field of this class.
     *
     * The processing of the inputs is not done automatically. You must call the
     * method progress_input_reader() when you are ready to process them.
     *
     * \author Julien Jorge
     */
    template<class Base>
    class item_with_input_listener:
      public Base,
      public input::input_listener
    {
    private:
      typedef Base super;

    protected:
      void progress_input_reader( universe::time_type elapsed_time );

      virtual bool key_maintained
      ( universe::time_type elapsed_time, const input::key_info& key );

      virtual bool button_maintained
      ( universe::time_type elapsed_time, input::joystick::joy_code button,
        unsigned int joy_index );

      virtual bool mouse_maintained
      ( universe::time_type elapsed_time, input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );

    private:
      bool key_maintained( const input::key_info& key );

      bool button_maintained
      ( input::joystick::joy_code button, unsigned int joy_index );

      bool mouse_maintained
      ( input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );

    private:
      /** \brief The status of the controllers. */
      input::input_status m_input_status;

      /** \brief Last value of \a elapsed_time passed to
          progress_input_reader(). */
      universe::time_type m_elapsed_time;

    }; // class item_with_input_listener
  } // namespace engine
} // namespace bear

#include "engine/item_brick/impl/item_with_input_listener.tpp"

#endif // __ENGINE_ITEM_WITH_INPUT_LISTENER_HPP__

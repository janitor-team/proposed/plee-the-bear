/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file region.tpp
 * \brief Implementation of the bear::concept::region class.
 * \author Julien Jorge.
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the region intersects a shape.
 * \param The shape to check.
 */
template<class Shape, class Container>
bool bear::concept::region<Shape, Container>::intersects
( const shape_type& shape ) const
{
  bool result = false;

  typename super::const_iterator it;

  for (it=this->begin(); (it!=this->end()) && !result; ++it)
    result = it->intersects( shape );

  return result;
} // region::intersects()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the region includes a shape.
 * \param The shape to check.
 */
template<class Shape, class Container>
bool bear::concept::region<Shape, Container>::includes
( const shape_type& shape ) const
{
  bool result = false;

  typename super::const_iterator it;

  for (it=this->begin(); (it!=this->end()) && !result; ++it)
    result = it->includes( shape );

  return result;
} // region::includes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the region includes a point.
 * \param The point to check.
 */
template<class Shape, class Container>
template<typename Point>
bool bear::concept::region<Shape, Container>::includes( const Point& p ) const
{
  bool result = false;

  typename super::const_iterator it;

  for (it=this->begin(); (it!=this->end()) && !result; ++it)
    result = it->includes( p );

  return result;
} // region::includes()

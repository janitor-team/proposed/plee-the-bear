/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file region.hpp
 * \brief A region is a list of shapes in a 2D space.
 * \author Julien Jorge.
 */
#ifndef __CONCEPT_REGION_HPP__
#define __CONCEPT_REGION_HPP__

#include <list>

namespace bear
{
  namespace concept
  {
    /**
     * \brief A region is a list of shapes in a 2D space.
     *
     * The \a Shape type should have the following methods :
     * - bool intersects( Shape ) : tell if two shapes intersects,
     * - bool includes( Shape ) : tell if a shape is included in an other one,
     * - bool includes( Point ) : tell if a shape includes a coordinate.
     *
     * \author Julien Jorge.
     */
    template <class Shape, class Container = std::list<Shape> >
    class region : public Container
    {
    public:
      /** \brief The type of the shapes. */
      typedef Shape shape_type;

      /** \brief The type of the parent class. */
      typedef Container super;

    public:
      bool intersects( const shape_type& shape ) const;

      bool includes( const shape_type& shape ) const;
      template<typename Point>
      bool includes( const Point& p ) const;

    }; // class region

  } // namespace concept
} // namespace bear

#include "concept/impl/region.tpp"

#endif // __CONCEPT_REGION_HPP__

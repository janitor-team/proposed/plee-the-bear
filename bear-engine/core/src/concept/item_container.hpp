/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_container.hpp
 * \brief A template class for item managing. Use it when you need to store
 *        items that can be added or deleted anytime but must wait if a
 *        process is running.
 * \author Julien Jorge.
 */
#ifndef __CONCEPT_ITEM_CONTAINER_HPP__
#define __CONCEPT_ITEM_CONTAINER_HPP__

#include <list>

namespace bear
{
  namespace concept
  {

    /**
     * \brief A template class for item managing. Use it when you need to store
     *        items that may want to be added or deleted anytime but should wait
     *        a signal to be really added or deleted.
     * \author Julien Jorge.
     */
    template<class ItemType>
    class item_container
    {
    public:
      /** \brief The type of the stored items. */
      typedef ItemType item_type;

    public:
      item_container();
      item_container( const item_container<item_type>& that );
      virtual ~item_container();

      void register_item( const item_type& who );
      void release_item( const item_type& who );

      bool locked() const;

    protected:
      virtual void add( const item_type& who ) = 0;
      virtual void remove( const item_type& who ) = 0;

      void lock();
      void unlock();

    private:
      /** \brief True if items can't be immediatly added. Items will be
          buffered. */
      bool m_locked;

      /** \brief The queue of the items to add. */
      std::list<item_type> m_life_queue;

      /** \brief The queue of the items to delete. */
      std::list<item_type> m_death_queue;

    }; // class item_container

  } // namespace concept
} // namespace bear

// template methods
#include "concept/impl/item_container.tpp"

#endif // __CONCEPT_ITEM_CONTAINER_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sound_effect.hpp
 * \brief Basic class for basic not progressive sounds effects.
 * \author Julien Jorge
 */
#ifndef __AUDIO_SOUND_EFFECT_HPP__
#define __AUDIO_SOUND_EFFECT_HPP__

#include <claw/coordinate_2d.hpp>
#include "audio/class_export.hpp"

namespace bear
{
  namespace audio
  {
    /**
     * \brief Basic class for basic not progressive sounds effects.
     */
    class AUDIO_EXPORT sound_effect
    {
    public:
      sound_effect();
      explicit sound_effect( double volume );
      explicit sound_effect( unsigned int loops, double volume = 1 );
      explicit sound_effect( const claw::math::coordinate_2d<double>& pos );
      sound_effect( const sound_effect& that );
      ~sound_effect();

      sound_effect& operator=( const sound_effect& that );

      void set_volume( double volume );
      double get_volume() const;

      void set_loops( unsigned int loops );
      int get_loops() const;

      bool has_a_position() const;
      void set_position( const claw::math::coordinate_2d<double>& pos );
      const claw::math::coordinate_2d<double>& get_position() const;

    private:
      /** \brief Sound volume, in [0, 1]. */
      double m_volume;

      /** \brief Number of loops (added to a first default play). */
      int m_loops;

      /** \brief The position, if any. */
      claw::math::coordinate_2d<double>* m_position;

    }; // class sound_effect
  } // namespace audio
} // namespace bear

#endif // __AUDIO_SOUND_EFFECT_HPP__

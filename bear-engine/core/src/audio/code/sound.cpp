/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sound.cpp
 * \brief Implementation of the bear::audio::sound class.
 * \author Julien Jorge
 */
#include "audio/sound.hpp"
#include "audio/sound_manager.hpp"
#include "audio/sample.hpp"

#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param name The name of the sound resource.
 * \param owner The instance of sound_manager who stores me.
 */
bear::audio::sound::sound( const std::string& name, sound_manager& owner )
  : m_owner(owner), m_name(name)
{

} // sound::sound()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::audio::sound::~sound()
{

} // sound::~sound()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a new sample of this sound.
 */
bear::audio::sample* bear::audio::sound::new_sample()
{
  return new sample( get_sound_name(), get_manager() );
} // sound::play()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sound_manager who owns this sound.
 */
bear::audio::sound_manager& bear::audio::sound::get_manager()
{
  return m_owner;
} // sound::get_manager()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sound_manager who owns this sound.
 */
const bear::audio::sound_manager& bear::audio::sound::get_manager() const
{
  return m_owner;
} // sound::get_manager()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the name of the sound resource.
 */
const std::string& bear::audio::sound::get_sound_name() const
{
  return m_name;
} // sound::get_sound_name()

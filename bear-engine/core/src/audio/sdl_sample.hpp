/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sdl_sample.hpp
 * \brief A class representing a sound sample. This class uses the SDL_mixer
 *        library.
 * \author Julien Jorge
 */
#ifndef __AUDIO_SDL_SAMPLE_HPP__
#define __AUDIO_SDL_SAMPLE_HPP__

#include "audio/sample.hpp"

#include "audio/class_export.hpp"
#include <vector>

namespace bear
{
  namespace audio
  {
    class sdl_sound;

    /**
     * \brief A class representing a sound sample.
     */
    class AUDIO_EXPORT sdl_sample:
      public sample
    {
    private:
      /**
       * \brief Informations stored in the s_playing_channels vector.
       */
      class AUDIO_EXPORT channel_attribute
      {
      public:
        channel_attribute();
        ~channel_attribute();

        void set_sample( const sdl_sample& s );
        const sdl_sample& get_sample() const;

        void set_effect( const sound_effect& effect );
        const sound_effect& get_effect() const;

        void clear();
        bool is_empty() const;

      private:
        /** \brief The sample in this channel. */
        const sdl_sample* m_sample;

        /** \brief The effect applied to the sound. */
        sound_effect m_effect;

      }; // class channel_attribute

    public:
      sdl_sample( const sdl_sound& s, sound_manager& owner );
      ~sdl_sample();

      void play();
      void play( const sound_effect& effect );
      void pause();
      void resume();
      void stop();
      void stop( double d );

      sound_effect get_effect() const;
      void set_effect( const sound_effect& effect );

      /* for sound_manager only. */
      void set_volume( double v );

      static void channel_finished(int channel);

    private:
      static void distance_tone_down
      ( int channel, void *stream, int length, void *position );
      static void volume
      ( int channel, void *stream, int length, void *position );

      void inside_play();
      void stop_sample();

      void inside_set_effect();

      void global_add_channel();
      void finished();

    private:
      /** \brief The channel in which this sample is played. */
      int m_channel;

      /** \brief The sound of which we are a sample. */
      const sdl_sound* m_sound;

      /** \brief The effects applied to the sample, by default. */
      sound_effect m_effect;

      /** \brief Global vector giving, for a channel, the sample currently
          played. */
      static std::vector<channel_attribute*> s_playing_channels;

      /**
       * \brief Distance from which we can't hear a sound, measured as a
       *        Manhattan distance.
       *
       * \remark This value must be strictly greater than
       *         s_full_volume_distance.
       */
      static unsigned int s_silent_distance;

      /**
       * \brief Distance under which sounds are at maximum volume, measured as a
       *        Manhattan distance.
       *
       * \remark This value must be strictly lower than s_silent_distance.
       */
      static unsigned int s_full_volume_distance;

    }; // class sdl_sample
  } // namespace audio
} // namespace bear

#endif // __AUDIO_SDL_SAMPLE_HPP__

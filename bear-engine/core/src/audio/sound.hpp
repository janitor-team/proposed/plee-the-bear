/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sound.hpp
 * \brief A class representing a sound.
 * \author Julien Jorge
 */
#ifndef __AUDIO_SOUND_HPP__
#define __AUDIO_SOUND_HPP__

#include "audio/sound_manager.hpp"
#include "audio/class_export.hpp"

namespace bear
{
  namespace audio
  {
    class sample;
    class sound_manager;

    /**
     * \brief A class representing a sound.
     */
    class AUDIO_EXPORT sound
    {
    public:
      sound( const std::string& name, sound_manager& owner );
      virtual ~sound();

      virtual sample* new_sample();

      sound_manager& get_manager();
      const sound_manager& get_manager() const;

      const std::string& get_sound_name() const;

    private:
      /** \brief The sound_manager who stores me. */
      sound_manager& m_owner;

      /** \brief The name of the sound resource. */
      const std::string m_name;

    }; // class sound
  } // namespace audio
} // namespace bear

#endif // __AUDIO_SOUND_HPP__

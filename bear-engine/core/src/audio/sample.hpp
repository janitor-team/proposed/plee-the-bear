/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sample.hpp
 * \brief A class representing a sound sample.
 * \author Julien Jorge
 */
#ifndef __AUDIO_SAMPLE_HPP__
#define __AUDIO_SAMPLE_HPP__

#include "audio/sound_effect.hpp"
#include "audio/class_export.hpp"

#include <claw/coordinate_2d.hpp>
#include <claw/non_copyable.hpp>
#include <string>

namespace bear
{
  namespace audio
  {
    class sound_manager;

    /**
     * \brief A class representing a sound.
     */
    class AUDIO_EXPORT sample:
      public claw::pattern::non_copyable
    {
    public:
      explicit sample( const std::string& name );
      sample( const std::string& name, sound_manager& owner );
      virtual ~sample();

      sample* clone() const;

      std::size_t get_id() const;
      const std::string& get_sound_name() const;

      virtual void play();
      virtual void play( const sound_effect& effect );
      virtual void pause();
      virtual void resume();
      virtual void stop();
      virtual void stop( double d );

      virtual sound_effect get_effect() const;
      virtual void set_effect( const sound_effect& effect );

      /* for sound_manager only. */
      virtual void set_volume( double v );

    protected:
      void sample_finished();

    private:
      /** \brief The sound of which we are a sample. */
      sound_manager* m_manager;

      /** \brief The identifier of the sound. */
      const std::size_t m_id;

      /** \brief The name of the sound resource. */
      const std::string& m_name;

      /** \brief The next available identifier. */
      static std::size_t s_next_id;

    }; // class sample
  } // namespace audio
} // namespace bear

#endif // __AUDIO_SAMPLE_HPP__

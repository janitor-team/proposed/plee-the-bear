/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sdl_sound.hpp
 * \brief A class representing a sound. This class uses the SDL_mixer library.
 * \author Julien Jorge
 */
#ifndef __AUDIO_SDL_SOUND_HPP__
#define __AUDIO_SDL_SOUND_HPP__

#include "audio/sound.hpp"

#include <SDL/SDL_mixer.h>
#include <iostream>

#include "audio/class_export.hpp"

namespace bear
{
  namespace audio
  {
    class sound_manager;

    /**
     * \brief A class representing a sound.
     */
    class AUDIO_EXPORT sdl_sound:
      public sound
    {
    public:
      sdl_sound
        ( std::istream& file, const std::string& name, sound_manager& owner );
      ~sdl_sound();

      sample* new_sample();

      int play( unsigned int loops ) const;

      static bool initialize();
      static void release();

      static unsigned int get_audio_format();

    private:
      /** \brief The sound allocated by SDL_mixer. */
      Mix_Chunk* m_sound;

      /** \brief Output audio rate. */
      static unsigned int s_audio_rate;

      /** \brief Output audio format. */
      static unsigned int s_audio_format;

      /** \brief Number of channels. */
      static unsigned int s_audio_channels;

      /** \brief Size of the buffer. */
      static unsigned int s_audio_buffers;

      /** \brief Count of channels for mixing. */
      static unsigned int s_audio_mix_channels;

    }; // class sdl_sound
  } // namespace audio
} // namespace bear

#endif // __AUDIO_SDL_SOUND_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file system.hpp
 * \brief A class containing initialization and releasing methods for using the
 *        classes of the namespace input.
 * \author Julien Jorge
 */
#ifndef __INPUT_SYSTEM_HPP__
#define __INPUT_SYSTEM_HPP__

#include <claw/basic_singleton.hpp>
#include <vector>

#include "input/class_export.hpp"

namespace bear
{
  namespace input
  {
    class joystick;
    class keyboard;
    class mouse;

    /**
     * \brief A class containing initialization and releasing methods for using
     *        the classes of the namespace input.
     */
    class INPUT_EXPORT system:
      public claw::pattern::basic_singleton<system>
    {
      // need an access to the constructor/destructor.
      friend class claw::pattern::basic_singleton<system>;

      typedef claw::pattern::basic_singleton<system> super;

    public:
      static void initialize();
      static void release();

      // Must be redefined to work correctly with dynamic libraries.
      // At least under Windows with MinGW.
      static system& get_instance();

      void refresh();

      keyboard& get_keyboard();
      mouse& get_mouse();
      joystick& get_joystick( unsigned int joy_id );

    private:
      system();
      ~system();

      void refresh_alone();

      void clear();

    private:
      /** \brief The keyboard. */
      keyboard* m_keyboard;

      /** \brief The mouse. */
      mouse* m_mouse;

      /** \brief The joysticks. */
      std::vector<joystick*> m_joystick;

    }; // class system
  } // namespace input
} // namespace bear

#endif // __INPUT_SYSTEM_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file keyboard.hpp
 * \brief A class representing a keyboard.
 * \author Julien Jorge
 */
#ifndef __INPUT_KEYBOARD_HPP__
#define __INPUT_KEYBOARD_HPP__

#include <string>
#include <vector>
#include <list>

#include "charset/def.hpp"
#include "input/key_event.hpp"
#include "input/class_export.hpp"

namespace bear
{
  namespace input
  {
    /**
     * \brief A class representing a keyboard.
     */
    class INPUT_EXPORT keyboard
    {
    public:
      /** \brief Iterator on the pressed keys. */
      typedef std::list<key_code>::const_iterator const_iterator;

      /** \brief Iterator on the events. */
      typedef std::list<key_event> event_list;

    public:
      static std::string get_name_of( key_code k );
      static  std::string get_translated_name_of( key_code k );
      static key_code get_key_named( const std::string& n );

      const_iterator begin() const;
      const_iterator end() const;
      const event_list& get_events() const;
      bool empty() const;

      // only for input::system
      void refresh();

    private:
      void refresh_events();
      void refresh_keys();

      key_code sdl_key_to_local( unsigned int sdl_val, bool shift,
                                 bool alt ) const;

    public:
#include "input/key_codes.hpp"

    private:
      /** \brief Pressed keys. */
      std::list<key_code> m_pressed_keys;

      /** \brief Pressed keys. */
      event_list m_key_events;

    }; // class keyboard
  } // namespace input
} // namespace bear

#endif // __INPUT_KEYBOARD_HPP__

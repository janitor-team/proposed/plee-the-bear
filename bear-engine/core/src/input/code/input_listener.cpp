/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file input_listener.cpp
 * \brief Implementation of the bear::input::input_listener class.
 * \author Julien Jorge
 */
#include "input/input_listener.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::input::input_listener::~input_listener()
{
  // nothing to do
} // input_listener::~input_listener()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a key is pressed.
 * \param key The value of the key.
 * \return true if the key has been processed.
 *
 * This methods use raw key codes as \a key. If you want the character as
 * entered by the user, use char_pressed.
 */
bool bear::input::input_listener::key_pressed( const key_info& key )
{
  return false;
} // input_listener::key_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a key is released.
 * \param key The value of the key.
 * \return true if the key has been processed.
 *
 * This methods use raw key codes as \a key. If you want the character as
 * entered by the user, use char_pressed.
 */
bool bear::input::input_listener::key_released( const key_info& key )
{
  return false;
} // input_listener::key_released()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a key is maintained.
 * \param key The value of the key.
 * \return true if the key has been processed.
 *
 * This methods use raw key codes as \a key. If you want the character as
 * entered by the user, use char_pressed.
 */
bool bear::input::input_listener::key_maintained( const key_info& key )
{
  return false;
} // input_listener::key_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a character is entered by the user.
 * \param key The value of the key.
 * \return true if the key has been processed.
 */
bool bear::input::input_listener::char_pressed( const key_info& key )
{
  return false;
} // input_listener::char_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a joystick button is pressed.
 * \param button The value of the button.
 * \param joy_index The index of the joystick.
 * \return true if the button has been processed.
 */
bool bear::input::input_listener::button_pressed
( joystick::joy_code button, unsigned int joy_index )
{
  return false;
} // input_listener::button_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a joystick button is released.
 * \param button The value of the button.
 * \param joy_index The index of the joystick.
 * \return true if the button has been processed.
 */
bool bear::input::input_listener::button_released
( joystick::joy_code button, unsigned int joy_index )
{
  return false;
} // input_listener::button_released()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a joystick button is maintained.
 * \param button The value of the button.
 * \param joy_index The index of the joystick.
 * \return true if the button has been processed.
 */
bool bear::input::input_listener::button_maintained
( joystick::joy_code button, unsigned int joy_index )
{
  return false;
} // input_listener::button_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a mouse button is pressed.
 * \param button The code of the button.
 * \param pos The current position of the cursor.
 * \return true if the button has been processed.
 */
bool bear::input::input_listener::mouse_pressed
( mouse::mouse_code button, const claw::math::coordinate_2d<unsigned int>& pos )
{
  return false;
} // input_listener::mouse_pressed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a mouse button is released.
 * \param button The code of the button.
 * \param pos The current position of the cursor.
 * \return true if the button has been processed.
 */
bool bear::input::input_listener::mouse_released
( mouse::mouse_code button, const claw::math::coordinate_2d<unsigned int>& pos )
{
  return false;
} // input_listener::mouse_released()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when a mouse button is maintained.
 * \param button The code of the button.
 * \param pos The current position of the cursor.
 * \return true if the button has been processed.
 */
bool bear::input::input_listener::mouse_maintained
( mouse::mouse_code button, const claw::math::coordinate_2d<unsigned int>& pos )
{
  return false;
} // input_listener::mouse_maintained()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called when the position of the mouse changes.
 * \param pos The new position of the cursor.
 * \return true if the event has been processed.
 */
bool bear::input::input_listener::mouse_move
( const claw::math::coordinate_2d<unsigned int>& pos )
{
  return false;
} // input_listener::mouse_move()

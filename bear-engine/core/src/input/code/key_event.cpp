/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file key_event.cpp
 * \brief Implementation of the bear::input::key_event class.
 * \author Julien Jorge
 */
#include "input/key_event.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param t The type of the event.
 * \param info Informations about the key.
 */
bear::input::key_event::key_event( event_type t, const key_info& info )
  : m_type(t), m_info(info)
{

} // key_event::key_event()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the type of the event.
 */
bear::input::key_event::event_type bear::input::key_event::get_type() const
{
  return m_type;
} // key_event::get_type()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the informations about the key.
 */
const bear::input::key_info& bear::input::key_event::get_info() const
{
  return m_info;
} // key_event::get_info()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file mouse.cpp
 * \brief Implementation of the bear::input::mouse class.
 * \author Julien Jorge
 */
#include "input/mouse.hpp"

#include "bear_gettext.hpp"

#include <SDL/SDL.h>
#include <algorithm>
#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::input::mouse::mouse()
{
  update_position();
} // mouse::mouse()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a mouse_code to a human-readable string.
 * \param k The code to convert.
 */
std::string bear::input::mouse::get_name_of( mouse_code b )
{
  switch (b)
    {
    case mc_left_button: return "left click"; break;
    case mc_middle_button: return "middle click"; break;
    case mc_right_button: return "right click"; break;
    case mc_wheel_up: return "wheel up"; break;
    case mc_wheel_down: return "wheel down"; break;
    default:
      {
        CLAW_FAIL( "Invalid mouse code given to mouse::get_name_of()" );
        return "invalid mouse code";
      }
    }
} // mouse::get_name_of()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a mouse_code to a human-readable string translated with
 *        gettext.
 * \param k The code to convert.
 */
std::string bear::input::mouse::get_translated_name_of( mouse_code b )
{
  return bear_gettext( get_name_of(b).c_str() );
} // mouse::get_translated_name_of()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a mouse_code from its human-readable name.
 * \param n The name of the code.
 */
bear::input::mouse::mouse_code
bear::input::mouse::get_button_named( const std::string& n )
{
  for (mouse_code i=mc_range_min; i<=mc_range_max; ++i)
    if ( get_name_of(i) == n )
      return i;

  return mc_invalid;
} // mouse::get_button_named()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an iterator on the first pressed button.
 */
bear::input::mouse::const_iterator bear::input::mouse::begin() const
{
  return m_pressed_buttons.begin();
} // mouse::begin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get an iterator after the last pressed button.
 */
bear::input::mouse::const_iterator bear::input::mouse::end() const
{
  return m_pressed_buttons.end();
} // mouse::end()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if no keys are pressed.
 */
bool bear::input::mouse::empty() const
{
  return m_pressed_buttons.empty();
} // mouse::empty()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the position of the mouse.
 */
const claw::math::coordinate_2d<unsigned int>&
bear::input::mouse::get_position() const
{
  return m_position;
} // mouse::get_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Re-read the status of all keys.
 * \pre The caller is an instance of bear::input::system.
 */
void bear::input::mouse::refresh()
{
  Uint8 buttons;

  update_position();
  buttons = SDL_GetMouseState( NULL, NULL );
  m_pressed_buttons.clear();

  // magic number: 6 is the number of codes for the SDL default mouse buttons
  for (unsigned int i=1; i!=6; ++i)
    if ( buttons & SDL_BUTTON(i) )
      m_pressed_buttons.push_back( sdl_button_to_local(i) );
} // mouse::refresh()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the position of the mouse.
 */
void bear::input::mouse::update_position()
{
  int x, y;

  SDL_GetMouseState( &x, &y );
  SDL_Surface* s( SDL_GetVideoSurface() );

  if ( s!=NULL )
    y = s->h - y;

  m_position.set(x, y);
} // mouse::update_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Convert a SDLK_* value to the corresponding mouse_code.
 * \param sdl_val The SDL value to convert.
 * \param shift Tell if a shift button is considered pressed.
 * \param alt Tell if an alt button is considered pressed.
 */
bear::input::mouse::mouse_code bear::input::mouse::sdl_button_to_local
( unsigned int sdl_val ) const
{
  switch(sdl_val)
    {
    case 1 : return mc_left_button;   break;
    case 2 : return mc_middle_button; break;
    case 3 : return mc_right_button;  break;
    case 4 : return mc_wheel_up;      break;
    case 5 : return mc_wheel_down;    break;
    default: return mc_invalid;
    }

} // mouse::sdl_mouse_to_local()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file keyboard_status.cpp
 * \brief Implementation of the bear::input::keyboard_status class.
 * \author Julien Jorge
 */
#include "input/keyboard_status.hpp"

#include "input/input_listener.hpp"
#include "input/system.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Read the status of the keys.
 */
void bear::input::keyboard_status::read()
{
  const keyboard& kb = system::get_instance().get_keyboard();
  keyboard::const_iterator it;
  set_type current;

  for (it=kb.begin(); it!=kb.end(); ++it)
    current.insert(*it);

  m_released = m_pressed;
  m_released.join(m_maintained);
  m_released.difference(current);
  m_maintained.join(m_pressed).intersection(current);
  (m_pressed = current).difference(m_maintained);

  m_forget_key.difference( m_released );

  m_events = kb.get_events();
} // keyboard_status::read()

/*----------------------------------------------------------------------------*/
/**
 * \brief Send the status of the keys to an input_listener.
 * \param listener The listener to pass informations to.
 */
void bear::input::keyboard_status::scan_inputs( input_listener& listener ) const
{
  set_type::const_iterator it;

  for (it=m_pressed.begin(); it!=m_pressed.end(); ++it)
    listener.key_pressed( key_info(*it) );

  for (it=m_maintained.begin(); it!=m_maintained.end(); ++it)
    listener.key_maintained( key_info(*it) );

  for (it=m_released.begin(); it!=m_released.end(); ++it)
    listener.key_released( key_info(*it) );

  event_list::const_iterator itk;
  for ( itk=m_events.begin(); itk!=m_events.end(); ++itk )
    switch( itk->get_type() )
      {
      case key_event::key_event_character:
        listener.char_pressed( itk->get_info() );
        break;
      default:
        { /* ignored */ }
      }
} // keyboard_status::scan_inputs()

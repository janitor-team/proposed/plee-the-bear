/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file controller_button.cpp
 * \brief Informations on a pressed key.
 * \author Julien Jorge
 */
#include "input/controller_button.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::input::controller_button::controller_button()
  : m_type(controller_not_set)
{

} // controller_button::controller_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param key The keyboard key to store.
 */
bear::input::controller_button::controller_button( const key_info& key )
  : m_type(controller_keyboard), m_keyboard(key)
{

} // controller_button::controller_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param joy The joystick button to store.
 */
bear::input::controller_button::controller_button( const joystick_button& joy )
  : m_type(controller_joystick), m_joystick(joy)
{

} // controller_button::controller_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param m The mouse button to store.
 */
bear::input::controller_button::controller_button( mouse::mouse_code m )
  : m_type(controller_mouse), m_mouse(m)
{

} // controller_button::controller_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assignment operator.
 * \param key The keyboard key to store.
 */
bear::input::controller_button&
bear::input::controller_button::operator=( const key_info& key )
{
  m_type = controller_keyboard;
  m_keyboard = key;
  return *this;
} // controller_button::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assignement operator.
 * \param joy The joystick button to store.
 */
bear::input::controller_button&
bear::input::controller_button::operator=( const joystick_button& joy )
{
  m_type = controller_joystick;
  m_joystick = joy;
  return *this;
} // controller_button::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assignment operator.
 * \param m The mouse button to store.
 */
bear::input::controller_button&
bear::input::controller_button::operator=( mouse::mouse_code m )
{
  m_type = controller_mouse;
  m_mouse = m;
  return *this;
} // controller_button::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the type of the controller for which the button is stored.
 */
bear::input::controller_button::controller_type
bear::input::controller_button::get_type() const
{
  return m_type;
} // controller_button::get_type()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the keyboard button.
 */
const bear::input::key_info&
bear::input::controller_button::get_key_info() const
{
  CLAW_PRECOND( m_type == controller_keyboard );
  return m_keyboard;
} // controller_button::get_key_info()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the joystick button.
 */
const bear::input::joystick_button&
bear::input::controller_button::get_joystick_button() const
{
  CLAW_PRECOND( m_type == controller_joystick );
  return m_joystick;
} // controller_button::get_joystick_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the mouse button.
 */
bear::input::mouse::mouse_code
bear::input::controller_button::get_mouse_code() const
{
  CLAW_PRECOND( m_type == controller_mouse );
  return m_mouse;
} // controller_button::get_mouse_code()

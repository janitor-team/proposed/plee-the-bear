/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file mouse_codes.hpp
 * \brief This file contains all mouse button codes values and must not be
 *        included  anywhere but in the input/mouse.hpp file.
 *
 * Values are here only for readability, but are members of the input::mouse
 * class.
 */
static const mouse_code mc_left_button     = 0;
static const mouse_code mc_middle_button   = 1;
static const mouse_code mc_right_button    = 2;
static const mouse_code mc_wheel_up        = 3;
static const mouse_code mc_wheel_down      = 4;

/**
 * \brief Code representing an invalid code.
 * \remark Currently the same as c_mouse_codes_count.
 */
static const mouse_code mc_invalid = 5;

/** \brief Number of valid key codes. */
static const unsigned int c_mouse_codes_count = 5;

private:
static const mouse_code mc_range_min = mc_left_button;
static const mouse_code mc_range_max = mc_wheel_down;

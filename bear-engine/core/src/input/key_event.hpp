/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file key_event.hpp
 * \brief A class representing a keyboard key event.
 * \author Julien Jorge
 */
#ifndef __INPUT_KEY_EVENT_HPP__
#define __INPUT_KEY_EVENT_HPP__

#include "input/key_info.hpp"

namespace bear
{
  namespace input
  {
    /**
     * \brief A key event.
     * \author Julien Jorge
     */
    class INPUT_EXPORT key_event
    {
    public:
      /** \brief Different types of events. */
      enum event_type
        {
          key_event_pressed,
          key_event_released,
          key_event_maintained,
          key_event_character,
          key_event_unknown
        }; // enum event_type

    public:
      key_event( event_type t, const key_info& info );

      event_type get_type() const;
      const key_info& get_info() const;

    private:
      /** \brief The type of the event. */
      event_type m_type;

      /** \brief Some informations about the concerned key. */
      key_info m_info;

    }; // class key_event

  } // namespace input
} // namespace bear

#endif // __INPUT_KEY_EVENT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file keyboard_status.hpp
 * \brief A class observing the state of the keyboard's buttons.
 * \author Julien Jorge
 */
#ifndef __INPUT_KEYBOARD_STATUS_HPP__
#define __INPUT_KEYBOARD_STATUS_HPP__

#include "input/keyboard.hpp"

#include <claw/ordered_set.hpp>

#include "input/class_export.hpp"

namespace bear
{
  namespace input
  {
    class input_listener;

    /**
     * \brief A class observing the state of the keyboard's buttons.
     * \author Julien Jorge
     */
    class INPUT_EXPORT keyboard_status
    {
    private:
      /** \brief The type of the set storing the keys. */
      typedef claw::math::ordered_set<key_code> set_type;

      /** \brief The type of the event list of the keyboard. */
      typedef keyboard::event_list event_list;

    public:
      void read();

      void scan_inputs( input_listener& listener ) const;

    private:
      /** \brief The set of the keys that have just been pressed. */
      set_type m_pressed;

      /** \brief The set of the keys that have just been released. */
      set_type m_released;

      /** \brief The set of the keys that are actually pressed. */
      set_type m_maintained;

      /** \brief Keys that must not be taken into account. */
      set_type m_forget_key;

      /** \brief The key events. */
      event_list m_events;

    }; // keyboard_status
  } // namespace input
} // namespace bear

#endif // __INPUT_KEYBOARD_STATUS_HPP__

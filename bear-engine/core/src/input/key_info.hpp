/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file key_info.hpp
 * \brief Informations on a pressed key.
 * \author Julien Jorge
 */
#ifndef __INPUT_KEY_INFO_HPP__
#define __INPUT_KEY_INFO_HPP__

#include "charset/def.hpp"
#include "input/class_export.hpp"

namespace bear
{
  namespace input
  {
    /** \brief Code representing a key on the keyboard. */
    typedef unsigned int key_code;

    /**
     * \brief A keyboard entry, by the user.
     * \author Julien Jorge
     */
    class INPUT_EXPORT key_info
    {
    public:
      key_info();
      explicit key_info( key_code c );
      key_info( key_code c, charset::char_type s );

      bool operator==( const key_info& that ) const;
      bool operator<( const key_info& that ) const;

      key_code get_code() const;
      charset::char_type get_symbol() const;

      bool is_escape() const;
      bool is_enter() const;
      bool is_tab() const;
      bool is_control() const;
      bool is_shift() const;
      bool is_alt() const;
      bool is_up() const;
      bool is_down() const;
      bool is_left() const;
      bool is_right() const;
      bool is_home() const;
      bool is_end() const;
      bool is_delete() const;
      bool is_backspace() const;
      bool is_function(unsigned int f) const;

      bool is_printable() const;

    private:
      /** \brief A code representing the keyboard key. */
      key_code m_code;

      /** \brief The translated symbol. */
      charset::char_type  m_symbol;

    }; // class key_info
  } // namespace input
} // namespace bear

#endif // __INPUT_KEY_INFO_HPP__

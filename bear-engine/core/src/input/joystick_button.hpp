/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file joystick_button.hpp
 * \brief This class stores a button of a joystick and the index of this
 *        joystick.
 * \author Julien Jorge
 */
#ifndef __INPUT_JOYSTICK_BUTTON_HPP__
#define __INPUT_JOYSTICK_BUTTON_HPP__

#include "input/joystick.hpp"

#include "input/class_export.hpp"

namespace bear
{
  namespace input
  {
    /**
     * \brief This class stores a button of a joystick and the index of this
     *        joystick.
     * \author Julien Jorge
     */
    class INPUT_EXPORT joystick_button
    {
    public:
      joystick_button();
      joystick_button( unsigned int joy, input::joystick::joy_code b );

      bool operator<( const joystick_button& that ) const;
      bool operator==( const joystick_button& that ) const;

      static std::string get_name_of( const joystick_button& j );
      static std::string get_translated_name_of( const joystick_button& j );
      static joystick_button get_button_named( const std::string& n );

    public:
      /** \brief The index of the joystick containing the key. */
      unsigned int joystick_index;

      /** \brief The code of the button. */
      joystick::joy_code button;

    }; // class joystick_button
  } // namespace input
} // namespace bear

#endif // __INPUT_JOYSTICK_BUTTON_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file controller_button.hpp
 * \brief Informations on a pressed key.
 * \author Julien Jorge
 */
#ifndef __INPUT_CONTROLLER_BUTTON_HPP__
#define __INPUT_CONTROLLER_BUTTON_HPP__

#include "input/key_info.hpp"
#include "input/joystick_button.hpp"
#include "input/mouse.hpp"

#include "input/class_export.hpp"

namespace bear
{
  namespace input
  {
    /**
     * \brief Informations on a pressed key.
     * \author Julien Jorge
     */
    class INPUT_EXPORT controller_button
    {
    public:
      /** \brief The type of the controller for which the button is stored. */
      enum controller_type
        {
          controller_keyboard,
          controller_joystick,
          controller_mouse,
          controller_not_set
        }; // enum controller_type

    public:
      controller_button();
      controller_button( const key_info& key );
      controller_button( const joystick_button& joy );
      controller_button( mouse::mouse_code m );

      controller_button& operator=( const key_info& key );
      controller_button& operator=( const joystick_button& joy );
      controller_button& operator=( mouse::mouse_code m );

      controller_type get_type() const;
      const key_info& get_key_info() const;
      const joystick_button& get_joystick_button() const;
      mouse::mouse_code get_mouse_code() const;

    private:
      /** \brief The type of the controller for which the button is stored. */
      controller_type m_type;

      /** \brief The keyboard key, when the button is a keyboard one. */
      key_info m_keyboard;

      /** \brief The joystick button, when the button is a joystick one. */
      joystick_button m_joystick;

      /** \brief The mouse button, when the button is a mouse one. */
      mouse::mouse_code m_mouse;

    }; // class controller_button

  } // namespace input
} // namespace bear

#endif // __INPUT_CONTROLLER_BUTTON_HPP__

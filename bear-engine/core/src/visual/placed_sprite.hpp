/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file placed_sprite.hpp
 * \brief A sprite and a position.
 * \author Julien Jorge
 */
#ifndef __VISUAL_PLACED_SPRITE_HPP__
#define __VISUAL_PLACED_SPRITE_HPP__

#include "visual/sprite.hpp"
#include "visual/types.hpp"

#include "visual/class_export.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief A sprite and a position.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT placed_sprite
    {
    public:
      placed_sprite();
      placed_sprite( coordinate_type x, coordinate_type y, const sprite& s );
      placed_sprite( const position_type& p, const sprite& s );

      const sprite& get_sprite() const;
      sprite& get_sprite();
      void set_sprite( const sprite& spr );

      const position_type& get_position() const;
      position_type& get_position();
      void set_position( const position_type& p );

    private:
      /** \brief The sprite. */
      sprite m_sprite;

      /** \brief The position. */
      position_type m_position;

    }; // class placed_sprite
  } // namespace visual
} // namespace bear

#endif // __VISUAL_PLACED_SPRITE_HPP__

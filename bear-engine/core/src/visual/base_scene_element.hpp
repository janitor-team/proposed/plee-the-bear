/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_scene_element.hpp
 * \brief The base class for scene elements.
 * \author Julien Jorge
 */
#ifndef __VISUAL_BASE_SCENE_ELEMENT_HPP__
#define __VISUAL_BASE_SCENE_ELEMENT_HPP__

#include "visual/bitmap_rendering_attributes.hpp"
#include "visual/types.hpp"
#include "visual/class_export.hpp"

#include <list>

namespace bear
{
  namespace visual
  {
    class base_screen;
    class scene_element;

    /**
     * \brief The base class for scene elements.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT base_scene_element
    {
    public:
      /** \brief A list of elements of the scene. */
      typedef std::list<scene_element> scene_element_list;

      /** \brief A list of rectangles. */
      typedef std::list<rectangle_type> rectangle_list;

    public:
      base_scene_element( coordinate_type x = 0, coordinate_type y = 0 );
      virtual ~base_scene_element();

      virtual base_scene_element* clone() const;

      virtual rectangle_type get_opaque_box() const;
      virtual rectangle_type get_bounding_box() const;

      virtual void burst
      ( const rectangle_list& boxes, scene_element_list& output ) const;

      virtual void render( base_screen& scr ) const;

      const position_type& get_position() const;
      position_type& get_position();
      void set_position( coordinate_type x, coordinate_type y );

      void set_scale_factor( double x, double y );
      double get_scale_factor_x() const;
      double get_scale_factor_y() const;

      void set_rendering_attributes( const bitmap_rendering_attributes& a );
      const bitmap_rendering_attributes& get_rendering_attributes() const;
      bitmap_rendering_attributes& get_rendering_attributes();

    private:
      /** \brief The position of the element on the screen. */
      position_type m_position;

      /** \brief Scale factor on the x-axis. */
      double m_scale_factor_x;

      /** \brief Scale factor on the y-axis. */
      double m_scale_factor_y;

      /** \brief The attributes applied to the element when rendering. */
      bitmap_rendering_attributes m_rendering_attributes;

    }; // class base_scene_element
  } // namespace visual
} // namespace bear

#endif // __VISUAL_BASE_SCENE_ELEMENT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file impl/text_layout.tpp
 * \brief Implementation of the template methods of bear::visual::text_layout.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief A function that arrange a text in a given box. A user-defined function
 * is called each time a word should be displayed.
 *
 * \b Template \b parameters:
 * - \a Func A function called each time a word should be displayed.
 *
 * \param func The function called when a word should be displayed.
 *
 * The signature of \a func be <tt> void func( coordinate_type x,
 * coordinate_type y, std::size_t first, std::size_t last ) </tt>. The return
 * type can be different but will be ignored. The parameters of this function
 * are
 * - \a x The x-position in the box where the word starts.
 * - \a y The y-position in the box where the word starts.
 * - \a first The first character of the word to display, in the initial string.
 * - \a last The character just past the last character to display.
 */
template<typename Func>
void bear::visual::text_layout::arrange_text( Func func ) const
{
  const std::size_t lines_count = m_size.y / m_font.get_max_glyph_height();

  std::size_t i=0;
  claw::math::coordinate_2d<unsigned int> cursor(0, 0);

  while ( (cursor.y < lines_count) && (i!=m_text.size()) )
    if ( m_text[i] == '\n' )
      {
        ++i;
        ++cursor.y;
        cursor.x = 0;
      }
    else
      arrange_next_word( func, cursor, i );
} // text_layout::arrange_text()

/*----------------------------------------------------------------------------*/
/**
 * \brief Find the next word and display it.
 *
 * \b Template \b parameters:
 * - \a Func A function called each time a word should be displayed.
 *
 * \param func The function called when a word should be displayed.
 * \param cursor (in/out) The position of the cursor in the component.
 * \param i (in/out) Index of the first character of the word. (out) Index of
 *        the next character to print.
 *
 * The signature of \a func be <tt> void func( coordinate_type x,
 * coordinate_type y, std::size_t first, std::size_t last ) </tt>. The return
 * type can be different but will be ignored. The parameters of this function
 * are
 * - \a x The x-position in the box where the word starts.
 * - \a y The y-position in the box where the word starts.
 * - \a first The first character of the word to display, in the initial string.
 * - \a last The character just past the last character to display.
 *
 * \sa arrange_text
 */
template<typename Func>
void bear::visual::text_layout::arrange_next_word
( Func func, claw::math::coordinate_2d<unsigned int>& cursor,
  std::size_t& i ) const
{
  const std::size_t line_length = m_size.x / m_font.get_em();

  // find the first word
  std::size_t word = m_text.find_first_not_of(' ', i);

  if (word == std::string::npos)
    {
      i = m_text.size();
      // Call func on an empty word to simulate the spaces toward the end
      // of the text.
      // The position on the Y-axis is computed such that the text starts on the
      // top of the box.
      func( cursor.x * m_font.get_em(),
            m_size.y - (cursor.y + 1) * m_font.get_max_glyph_height(), i, i );
    }
  else if (m_text[word] == '\n')
    {
      i = word;
      // Call func on an empty word to simulate the spaces toward the end of the
      // text.
      // The position on the Y-axis is computed such that the text starts on the
      // top of the box.
      func( cursor.x * m_font.get_em(),
            m_size.y - (cursor.y + 1) * m_font.get_max_glyph_height(), i, i );
    }
  else
    {
      // the end of the word
      std::size_t space = m_text.find_first_of(" \n", word);

      if (space == std::string::npos)
        space = m_text.size();

      const std::size_t word_length = space - i;

      // the word fits on the line
      if ( cursor.x + word_length <= line_length )
        arrange_word( func, cursor, i, word_length );
      else if ( cursor.x == 0 )         // the word doesn't fit on the full line
        arrange_word( func, cursor, i, line_length );
      else
        {
          // we will retry at the begining of the line in the next
          // loop we remove the spaces at the begining of the line
          ++cursor.y;
          cursor.x = 0;
          i = word;
        }
    }
} // text_layout::arrange_next_word()

/*----------------------------------------------------------------------------*/
/**
 * \brief Find the next word and display it.
 *
 * \b Template \b parameters:
 * - \a Func A function called each time a word should be displayed.
 *
 * \param func The function called when a word should be displayed.
 * \param cursor (in/out) The position of the cursor in the component.
 * \param i (in/out) Index of the first character of the word. (out) Index of
 *        the next character to print.
 * \param n Number of characters to print.
 *
 * The signature of \a func be <tt> void func( coordinate_type x,
 * coordinate_type y, std::size_t first, std::size_t last ) </tt>. The return
 * type can be different but will be ignored. The parameters of this function
 * are
 * - \a x The x-position in the box where the word starts.
 * - \a y The y-position in the box where the word starts.
 * - \a first The first character of the word to display, in the initial string.
 * - \a last The character just past the last character to display.
 *
 * \sa arrange_text
 *
 */
template<typename Func>
void bear::visual::text_layout::arrange_word
( Func func, claw::math::coordinate_2d<unsigned int>& cursor, std::size_t& i,
  const std::size_t n ) const
{
  const std::size_t line_length = m_size.x / m_font.get_em();

  // The position on the Y-axis is computed such that the text starts on the
  // top of the box.
  func( cursor.x * m_font.get_em(),
        m_size.y - (cursor.y + 1) * m_font.get_max_glyph_height(), i, i+n );

  cursor.x += n;
  i += n;

  if ( cursor.x == line_length )
    {
      cursor.x = 0;
      ++cursor.y;

      if ( i < m_text.size() )
        {
          i = m_text.find_first_not_of(' ', i);

          if ( i == std::string::npos )
            i = m_text.size();
          else if ( m_text[i] == '\n' )
            ++i;
        }
    }
} // text_layout::arrange_word()

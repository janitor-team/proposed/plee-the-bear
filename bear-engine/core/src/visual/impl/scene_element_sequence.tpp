/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file visual/impl/scene_element_sequence.tpp
 * \brief Implementation of the visual::scene_element_sequence.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param first Iterator of the first element of the sequence.
 * \param last Iterator just after the last element of the sequence.
 */
template<typename Iterator>
bear::visual::scene_element_sequence::scene_element_sequence
( Iterator first, Iterator last )
  : base_scene_element(0, 0), m_element(first, last)
{

} // scene_element_sequence::scene_element_sequence()


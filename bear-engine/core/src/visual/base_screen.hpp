/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file screen_base.hpp
 * \brief Base class for all screen implementation.
 * \author Julien Jorge
 */
#ifndef __VISUAL_BASE_SCREEN_HPP__
#define __VISUAL_BASE_SCREEN_HPP__

#include "visual/types.hpp"

#include <claw/coordinate_2d.hpp>
#include <claw/image.hpp>
#include <vector>

#include "visual/class_export.hpp"

namespace bear
{
  namespace visual
  {
    class sprite;

    /**
     * \brief Base class for all screen implementation.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT base_screen
    {
    public:
      virtual ~base_screen() {}

      virtual void resize_view(unsigned int width, unsigned int height) = 0;
      virtual void fullscreen( bool b ) = 0;
      virtual claw::math::coordinate_2d<unsigned int> get_size() const = 0;

      virtual bool need_restoration() const { return false; }
      virtual void set_restored() {}

      virtual void set_background_color( const color_type& c ) = 0;
      virtual color_type get_background_color() const = 0;

      virtual void begin_render() {}
      virtual void render( const position_type& pos, const sprite& s ) = 0;
      virtual bool end_render() { return true; }

      virtual void draw_line
      ( const color_type& color,
        const std::vector<position_type>& p, double w, bool close = false ) = 0;
      virtual void draw_polygon
      ( const color_type& color,
        const std::vector<position_type>& p ) = 0;

      virtual void shot( claw::graphic::image& img ) const = 0;

    }; // class base_screen
  } // namespace visual
} // namespace bear

#endif // __VISUAL_BASE_SCREEN_HPP__

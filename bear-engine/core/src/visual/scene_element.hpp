/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file scene_element.hpp
 * \brief A proxy for all base_scene_element derivated classes.
 * \author Julien Jorge
 */
#ifndef __VISUAL_SCENE_ELEMENT_HPP__
#define __VISUAL_SCENE_ELEMENT_HPP__

#include "visual/base_scene_element.hpp"

#include "visual/class_export.hpp"

#include <list>

namespace bear
{
  namespace visual
  {
    /**
     * \brief A proxy for all base_scene_element derivated classes.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT scene_element
    {
    public:
      /** \brief A list of elements of the scene. */
      typedef base_scene_element::scene_element_list scene_element_list;

      /** \brief A list of rectangles. */
      typedef base_scene_element::rectangle_list rectangle_list;

    public:
      scene_element( const base_scene_element& e = base_scene_element() );
      scene_element( const scene_element& that );
      ~scene_element();

      scene_element& operator=( const scene_element& that );

      rectangle_type get_opaque_box() const;
      rectangle_type get_bounding_box() const;

      void
      burst( const rectangle_list& boxes, scene_element_list& output ) const;

      void render( base_screen& scr ) const;

      const position_type& get_position() const;
      position_type& get_position();
      void set_position( const position_type& p );
      void set_position( coordinate_type x, coordinate_type y );

      void set_scale_factor( double x, double y );
      void set_scale_factor( double r );
      double get_scale_factor_x() const;
      double get_scale_factor_y() const;

      size_type get_element_width() const;
      size_type get_element_height() const;

      size_type get_width() const;
      size_type get_height() const;

      void set_rendering_attributes( const bitmap_rendering_attributes& a );
      const bitmap_rendering_attributes& get_rendering_attributes() const;
      bitmap_rendering_attributes& get_rendering_attributes();

    private:
      /** \brief The real visual. */
      base_scene_element* m_elem;

    }; // class scene_element
  } // namespace visual
} // namespace bear

#endif // __VISUAL_SCENE_ELEMENT_HPP__

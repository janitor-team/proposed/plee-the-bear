/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file color.hpp
 * \brief A class representing a color with transparency.
 * \author Julien Jorge
 */
#ifndef __VISUAL_COLOR_HPP__
#define __VISUAL_COLOR_HPP__

#include "visual/class_export.hpp"

#include <claw/pixel.hpp>

namespace bear
{
  namespace visual
  {
    /**
     * \brief A class representing a color with transparency.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT color:
      public claw::graphic::rgba_pixel
    {
    public:
      color();
      color( const claw::graphic::rgba_pixel& c );
      explicit color( const std::string& c );
      color( component_type r, component_type g, component_type b );
      color
      ( component_type r, component_type g, component_type b,
        component_type o );

      void set_red_intensity( double i );
      void set_green_intensity( double i );
      void set_blue_intensity( double i );
      void set_opacity( double o );

    }; // class color

  } // namespace visual
} // namespace bear

#endif // __VISUAL_COLOR_HPP__

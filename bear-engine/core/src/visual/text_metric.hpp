/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file text_metric.hpp
 * \brief A class to give informations about the sizes (characters, on screen)
 *        of a text.
 * \author Julien Jorge
 */
#ifndef __VISUAL_TEXT_METRIC_HPP__
#define __VISUAL_TEXT_METRIC_HPP__

#include <string>
#include <claw/math.hpp>
#include "visual/font.hpp"

#include "visual/class_export.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief A class to give informations about the sizes (characters, on
     *        screen) of a text.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT text_metric
    {
    public:
      text_metric( const std::string& text, const font& f );

      unsigned int width() const;
      unsigned int height() const;

      unsigned int longest_line() const;
      unsigned int lines() const;

    private:
      /** \brief The size of the text in pixel units. */
      claw::math::coordinate_2d<unsigned int> m_pixel_size;

      /** \brief The size of the text in character units. */
      claw::math::coordinate_2d<unsigned int> m_character_size;

    }; // class text_metric
  } // namespace visual
} // namespace bear

#endif // __VISUAL_TEXT_METRIC_HPP__

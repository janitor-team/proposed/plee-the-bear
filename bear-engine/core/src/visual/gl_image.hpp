/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file gl_image.hpp
 * \brief OpenGL implementation of an image.
 * \author Julien Jorge
 */
#ifndef __VISUAL_GL_IMAGE_HPP__
#define __VISUAL_GL_IMAGE_HPP__

#include "visual/base_image.hpp"

#include <GL/gl.h>
#include <claw/image.hpp>

namespace bear
{
  namespace visual
  {
    /**
     * \brief OpenGL implementation of an image.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT gl_image:
      public base_image
    {
    public:
      gl_image( const claw::graphic::image& data );
      ~gl_image();

      GLuint texture_id() const;
      claw::math::coordinate_2d<unsigned int> size() const;
      bool has_transparency() const;

    private:
      void create_texture();
      void copy_scanlines( const claw::graphic::image& pixels );

    private:
      /** \brief OpenGL texture identifier. */
      GLuint m_texture_id;

      /** \brief Image's size. */
      claw::math::coordinate_2d<unsigned int> m_size;

      /** \brief Is there any transparent pixel in the image ? */
      bool m_has_transparency;

    }; // class gl_image
  } // namespace visual
} // namespace bear

#endif // __VISUAL_GL_IMAGE_HPP__

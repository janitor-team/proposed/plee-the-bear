/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file image.hpp
 * \brief An image class, used for sprites.
 * \author Julien Jorge
 */
#ifndef __VISUAL_IMAGE_HPP__
#define __VISUAL_IMAGE_HPP__

#include <GL/gl.h>
#include <string>

#include <claw/pixel.hpp>
#include <claw/image.hpp>
#include <claw/smart_ptr.hpp>

#include "visual/base_image.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief An image class, used for sprites.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT image
    {
    private:
      typedef claw::memory::smart_ptr<base_image> base_image_ptr;

    public:
      image();
      image( const claw::graphic::image& data );

      void clear();
      void restore( const claw::graphic::image& data );

      unsigned int width() const;
      unsigned int height() const;
      claw::math::coordinate_2d<unsigned int> size() const;
      bool has_transparency() const;
      bool is_valid() const;

      const base_image* get_impl() const;

    private:
      /** \brief Implementation of the image. */
      claw::memory::smart_ptr<base_image_ptr> m_impl;

    }; // class image
  } // namespace visual
} // namespace bear

#endif // __VISUAL_IMAGE_HPP__

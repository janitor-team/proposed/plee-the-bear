/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file text_layout.hpp
 * \brief A class that arrange a text in a box (like it should be displayed).
 * \author Julien Jorge
 */
#ifndef __VISUAL_TEXT_LAYOUT_HPP__
#define __VISUAL_TEXT_LAYOUT_HPP__

#include "visual/font.hpp"
#include "visual/class_export.hpp"

#include <string>

namespace bear
{
  namespace visual
  {
    /**
     * \brief A class that arrange a text in a box (like it should be
     *        displayed).
     *
     * \sa writing
     *
     * \author Julien Jorge
     */
    class text_layout
    {
    public:
      VISUAL_EXPORT text_layout
      ( const font& f, const std::string& str, const size_box_type& s );
      VISUAL_EXPORT text_layout
      ( const bitmap_font& f, const std::string& str, const size_box_type& s );

      template<typename Func>
      void arrange_text( Func func ) const;

    private:
      template<typename Func>
      void arrange_next_word
      ( Func func, claw::math::coordinate_2d<unsigned int>& cursor,
        std::size_t& i ) const;

      template<typename Func>
      void arrange_word
      ( Func func, claw::math::coordinate_2d<unsigned int>& cursor,
        std::size_t& i, const std::size_t n ) const;

    private:
      /** \brief The size of the box around the text. */
      const size_box_type& m_size;

      /** \brief The text to arrange. */
      const std::string& m_text;

      /** \brief The font used to display the text. */
      const bitmap_font& m_font;

    }; // class text_layout
  } // namespace visual
} // namespace bear

#include "visual/impl/text_layout.tpp"

#endif // __VISUAL_TEXT_LAYOUT_HPP__

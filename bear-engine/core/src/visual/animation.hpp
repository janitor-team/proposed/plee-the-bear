/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file animation.hpp
 * \brief An animation.
 * \author Julien Jorge
 */
#ifndef __VISUAL_ANIMATION_HPP__
#define __VISUAL_ANIMATION_HPP__

#include "visual/sprite_sequence.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief An animation.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT animation:
      public sprite_sequence
    {
    public:
      animation();
      animation( const sprite& spr );
      animation
      ( const std::vector<sprite>& images, const std::vector<double>& d );

      void set_time_factor( double f );
      double get_time_factor() const;

      void reset();
      void next( double t );
      bool is_finished() const;

    private:
      double get_scaled_duration( std::size_t i ) const;

    private:
      /** \brief The duration of the frames. */
      std::vector<double> m_duration;

      /** \brief Time spent on the current frame. */
      double m_time;

      /** \brief A factor applied to the duration of the frames. */
      double m_time_factor;

    }; // class animation

  } // namespace visual
} // namespace bear

#endif // __VISUAL_ANIMATION_HPP__

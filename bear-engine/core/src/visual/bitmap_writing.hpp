/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bitmap_writing.hpp
 * \brief A text written with some bitmap font.
 * \author Julien Jorge
 */
#ifndef __VISUAL_BITMAP_WRITING_HPP__
#define __VISUAL_BITMAP_WRITING_HPP__

#include "visual/class_export.hpp"

#include "visual/placed_sprite.hpp"

#include <vector>
#include <string>

namespace bear
{
  namespace visual
  {
    class bitmap_font;
    class base_screen;
    class scene_writing;

    /**
     * \brief A text written with some bitmap font.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT bitmap_writing:
      public bitmap_rendering_attributes
    {
    private:
      typedef std::vector<placed_sprite> sprite_list;

    private:
      /** \brief The class passed to text_layout to compute the position of the
          sprites of the text. */
      class arrange_sprite_list
      {
      public:
        arrange_sprite_list
        ( const bitmap_font& f, const std::string& str, sprite_list& list );

        void operator()
          ( coordinate_type x, coordinate_type y, std::size_t first,
            std::size_t last );

      private:
        /** \brief The text to arrange. */
        std::string const& m_text;

        /** \brief The font used to display the text. */
        bitmap_font const& m_font;

        /** \brief The sprites of the text. */
        sprite_list& m_sprites;

      }; // class arrange_sprite_list

    public:
      typedef sprite_list::const_iterator const_iterator;

    public:
      std::size_t get_sprites_count() const;
      placed_sprite get_sprite( std::size_t i ) const;

      void create
      ( const bitmap_font& f, const std::string& str, const size_box_type& s );

      void call_render( const scene_writing& s, base_screen& scr ) const;

    private:
      /** \brief The sprites that make the text. */
      sprite_list m_sprites;

    }; // class bitmap_writing
  } // namespace visual
} // namespace bear

#endif // __VISUAL_BITMAP_WRITING_HPP__

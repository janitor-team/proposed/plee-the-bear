/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file screen.cpp
 * \brief Implementation of the bear::visual::screen class.
 * \author Julien Jorge
 */
#include "visual/screen.hpp"

#include "visual/gl_screen.hpp"

#include <claw/exception.hpp>
#include <claw/bitmap.hpp>
#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
bear::visual::screen::sub_system
bear::visual::screen::s_sub_system(screen_undef);

/*----------------------------------------------------------------------------*/
/**
 * \brief Global initializations common to all screens. Must be called at the
 *        begining of your program.
 */
void bear::visual::screen::initialize( sub_system sub_sys )
{
  if ( s_sub_system != screen_undef )
    release();

  s_sub_system = sub_sys;

  switch( s_sub_system )
    {
    case screen_gl:
      gl_screen::initialize();
      break;
    case screen_undef:
      {
        // nothing to do
      }
    }
} // screen::initialize()

/*----------------------------------------------------------------------------*/
/**
 * \brief Global uninitializations common to all screens. Must be called at the
 *        end of your program.
 */
void bear::visual::screen::release()
{
  switch( s_sub_system )
    {
    case screen_gl:
      gl_screen::release();
      break;
    case screen_undef:
      {
        // nothing to do
      }
    }

  s_sub_system = screen_undef;
} // screen::release()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the type of the sub system.
 */
bear::visual::screen::sub_system bear::visual::screen::get_sub_system()
{
  return s_sub_system;
} // screen::get_sub_system()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param size Size of the screen.
 * \param title The title of the window created.
 * \param full Tell if the window is full screen or not.
 */
bear::visual::screen::screen
( const claw::math::coordinate_2d<unsigned int>& size,
  const std::string& title, bool full )
  : m_mode(SCREEN_IDLE)
{
  switch( s_sub_system )
    {
    case screen_gl:
      m_impl = new gl_screen(size, title, full);
      break;
    case screen_undef:
      claw::exception("screen sub system has not been set.");
    }
} // screen::screen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::visual::screen::~screen()
{
  delete m_impl;
} // screen::~screen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a new dimension for the resulting projection (doesn't change
 *        the screen's size.
 * \param width Target's width.
 * \param height Target's height.
 */
void bear::visual::screen::resize_view(unsigned int width, unsigned int height)
{
  m_impl->resize_view(width, height);
} // screen::resize_view()

/*----------------------------------------------------------------------------*/
/**
 * \brief Turn fullscreen mode on/off.
 * \param b Tell if we want a fullscreen mode.
 */
void bear::visual::screen::fullscreen( bool b )
{
  m_impl->fullscreen(b);
} // screen::fullscreen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size of the screen.
 */
claw::math::coordinate_2d<unsigned int> bear::visual::screen::get_size() const
{
  return m_impl->get_size();
} // screen::get_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the environment needs to be restored.
 */
bool bear::visual::screen::need_restoration() const
{
  CLAW_PRECOND(m_mode == SCREEN_IDLE);

  return m_impl->need_restoration();
} // screen::need_restoration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform that the environment had been restored.
 */
void bear::visual::screen::set_restored()
{
  CLAW_PRECOND(m_mode == SCREEN_IDLE);

  m_impl->set_restored();
} // screen::set_restored()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the color of the background.
 * \param c The color.
 */
void bear::visual::screen::set_background_color( const color_type& c )
{
  m_impl->set_background_color(c);
} // screen::set_background_color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the color of the background.
 */
bear::visual::color_type bear::visual::screen::get_background_color() const
{
  return m_impl->get_background_color();
} // screen::get_background_color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the rendering process.
 */
void bear::visual::screen::begin_render()
{
  CLAW_PRECOND(m_mode == SCREEN_IDLE);

  m_mode = SCREEN_RENDER;
  m_impl->begin_render();
} // screen::begin_render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw something on the screen.
 * \param e Something.
 */
void bear::visual::screen::render( const scene_element& e )
{
  CLAW_PRECOND(m_mode == SCREEN_RENDER);

  m_scene_element.push_back(e);
} // screen::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop the rendering process.
 * \return false if this screen has been closed by the user.
 */
bool bear::visual::screen::end_render()
{
  CLAW_PRECOND(m_mode == SCREEN_RENDER);

  render_elements();

  bool result = m_impl->end_render();
  m_mode = SCREEN_IDLE;

  return result;
} // screen::end_render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a screen shot.
 * \param bitmap_name The name of the bitmap file where we'll dump image.
 */
void bear::visual::screen::shot( const std::string& bitmap_name ) const
{
  std::ofstream f(bitmap_name.c_str());

  if (!f)
    claw::logger << claw::log_warning
                 << "bear::visual::screen::shot: Can't open file '"
                 << bitmap_name << "'" << std::endl;
  else
    {
      claw::graphic::bitmap bmp( get_size().x, get_size().y );
      shot(bmp);
      bmp.save(f);
      f.close();
    }
} // screen::shot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a screen shot.
 * \param img The image in which we save the content of the screen.
 */
void bear::visual::screen::shot( claw::graphic::image& img ) const
{
  m_impl->shot(img);
} // screen::shot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the elements of the scene.
 */
void bear::visual::screen::render_elements()
{
#ifdef BEAR_DUMB_RENDERING
  for ( ; !m_scene_element.empty(); m_scene_element.pop_front() )
    m_scene_element.front().render(*m_impl);
#else
  scene_element_list final_elements; // Elements to render, finally.
  rectangle_list boxes;              // Empty parts of the screen.

  boxes.push_front( rectangle_type(0, 0, get_size().x, get_size().y) );

  // Elements are ordered from the background to the foreground. We split the
  // screen in reverse order so we won't display hidden elements.
  for ( ; !m_scene_element.empty(); m_scene_element.pop_back() )
    if ( intersects_any(m_scene_element.back().get_bounding_box(), boxes) )
      split(m_scene_element.back(), final_elements, boxes);

  // split() push the elements at the end of the list, so they are now ordered
  // from the foreground to the background
  for ( ; !final_elements.empty(); final_elements.pop_back() )
    final_elements.back().render(*m_impl);
#endif // dumb rendering
} // screen::render_elements()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if a rectangle intersects a rectangle from a list.
 * \param r The rectangle to check.
 * \param boxes The boxes to compare to.
 */
bool bear::visual::screen::intersects_any
( const rectangle_type& r, const rectangle_list& boxes ) const
{
  bool result=false;
  rectangle_list::const_iterator it;

  for (it=boxes.begin(); !result && (it!=boxes.end()); ++it)
    if ( r.intersects(*it) )
      {
        const rectangle_type inter = r.intersection(*it);
        result = (inter.width() > 0) && (inter.height() > 0);
      }

  return result;
} // screen::intersects_any()

/*----------------------------------------------------------------------------*/
/**
 * \brief Split a scene element to only keep its visible parts and update the
 *        screen cover.
 * \param e The element that will be rendered.
 * \param output The parts of \a e to render.
 * \param boxes The cover of the screen.
 */
void bear::visual::screen::split
( const scene_element& e, scene_element_list& output,
  rectangle_list& boxes ) const
{
  e.burst(boxes, output);

  const rectangle_type r( e.get_opaque_box() );

  if ( (r.width() > 0) && (r.height() > 0) )
    {
      rectangle_list input_boxes;
      rectangle_list::const_iterator it;

      std::swap( input_boxes, boxes );

      for ( it=input_boxes.begin(); it!=input_boxes.end(); ++it )
        subtract( *it, r, boxes );
    }
} // screen::split()

/*----------------------------------------------------------------------------*/
/**
 * \brief Subtract a rectangle \a b from a rectangle \a a.
 * \param a The target.
 * \param b The rectangle to remove in \a a.
 * \param boxes The subparts of \a a after the subtraction.
 */
void bear::visual::screen::subtract
( const rectangle_type& a, const rectangle_type& b,
  rectangle_list& result ) const
{
  if ( !a.intersects(b) )
    result.push_front(a);
  else
    {
      const rectangle_type inter = a.intersection(b);

      if ( (inter.width() == 0) || (inter.height() == 0) )
        result.push_front(a);
      else
        {
          if ( a.left() != inter.left() )
            result.push_front
              ( rectangle_type( a.left(), a.bottom(), inter.left(), a.top() ) );

          if ( a.top() != inter.top() )
            result.push_front
              ( rectangle_type
                ( inter.left(), inter.top(), inter.right(), a.top() ) );

          if ( a.right() != inter.right() )
            result.push_front
              ( rectangle_type
                ( inter.right(), a.bottom(), a.right(), a.top() ) );

          if ( a.bottom() != inter.bottom() )
            result.push_front
              ( rectangle_type
                ( inter.left(), a.bottom(), inter.right(), inter.bottom() ) );
        }
    }
} // screen::subtract()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file impl/text_layout.tpp
 * \brief Implementation of the template methods of bear::visual::text_layout.
 * \author Julien Jorge
 */
#include "visual/text_layout.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param f The font used to display the text.
 * \param str The text to arrange.
 * \param s The size of the box around the text.
 *
 * The instance keep the references on each parameter. So they must live longen
 * than \a this.
 */
bear::visual::text_layout::text_layout
( const font& f, const std::string& str, const size_box_type& s )
  : m_size(s), m_text(str), m_font(*f)
{
  CLAW_PRECOND( f != NULL );
} // text_layout::text_layout()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param f The font used to display the text.
 * \param str The text to arrange.
 * \param s The size of the box around the text.
 *
 * The instance keep the references on each parameter. So they must live longen
 * than \a this.
 */
bear::visual::text_layout::text_layout
( const bitmap_font& f, const std::string& str, const size_box_type& s )
  : m_size(s), m_text(str), m_font(f)
{

} // text_layout::text_layout()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sprite.cpp
 * \brief Implementation of the  sprite class.
 * \author Julien Jorge
 */
#include "visual/sprite.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::visual::sprite::sprite()
  : m_clip_rectangle(0, 0, 0, 0)
{

} // sprite::sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param img The image for the sprite.
 * \param clip_rectangle Area of "image" for the sprite.
 * \pre clip_rectangle is valid (in the image).
 */
bear::visual::sprite::sprite
( const image& img, const claw::math::rectangle<unsigned int>& clip_rectangle )
  : bitmap_rendering_attributes( clip_rectangle.size() ), m_image(img),
    m_clip_rectangle(clip_rectangle)
{
  CLAW_PRECOND( m_clip_rectangle.position.x
                + m_clip_rectangle.width <= m_image.width() );
  CLAW_PRECOND( m_clip_rectangle.position.y
                + m_clip_rectangle.height <= m_image.height() );
} // sprite::sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param img The image for the sprite.
 */
bear::visual::sprite::sprite( const image& img )
  : bitmap_rendering_attributes(img.size()), m_image(img),
    m_clip_rectangle(0, 0, img.width(), img.height())
{

} // sprite::sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the sprite has a transparency.
 */
bool bear::visual::sprite::has_transparency() const
{
  return (get_opacity() != 1) || m_image.has_transparency();
} // sprite::has_transparency()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the rectangular region of the picture used by this sprite.
 */
const claw::math::rectangle<unsigned int>&
bear::visual::sprite::clip_rectangle() const
{
  return m_clip_rectangle;
} // sprite::clip_rectangle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the rectangular region of the picture used by this sprite.
 */
void bear::visual::sprite::set_clip_rectangle
( const claw::math::rectangle<unsigned int>& clip )
{
  CLAW_PRECOND( clip.position.x + clip.width <= m_image.width() );
  CLAW_PRECOND( clip.position.y + clip.height <= m_image.height() );

  m_clip_rectangle = clip;
} // sprite::set_clip_rectangle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the picture used by this sprite.
 */
const bear::visual::image& bear::visual::sprite::get_image() const
{
  return m_image;
} // sprite::get_image()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the sprite is made with a valid image.
 */
bool bear::visual::sprite::is_valid() const
{
  return m_image.is_valid();
} // sprite::is_valid()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file image.cpp
 * \brief Implementation of the bear::visual::image class.
 * \author Julien Jorge
 */
#include "visual/image.hpp"

#include "visual/screen.hpp"
#include "visual/gl_image.hpp"

#include <claw/exception.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::visual::image::image()
  : m_impl(NULL)
{

} // image::image()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor with a claw::graphic::image object.
 * \param data The image to copy.
 */
bear::visual::image::image(const claw::graphic::image& data)
  : m_impl(new base_image_ptr(NULL))
{
  restore(data);
} // image::image() [claw::graphic::image]

/*----------------------------------------------------------------------------*/
/**
 * \brief Delete the data ofthe image.
 */
void bear::visual::image::clear()
{
  if ( m_impl != NULL )
    *m_impl = NULL;
} // image::clear()

/*----------------------------------------------------------------------------*/
/**
 * \brief Restore the image.
 * \param data The image to restore from.
 */
void bear::visual::image::restore(const claw::graphic::image& data)
{
  if ( m_impl == NULL )
    m_impl = new base_image_ptr(NULL);
  else if (*m_impl != NULL)
    {
      assert( data.width() == width() );
      assert( data.height() == height() );
    }

  switch ( screen::get_sub_system() )
    {
    case screen::screen_gl:
      *m_impl = new gl_image(data);
      break;
    case screen::screen_undef:
      claw::exception("screen sub system has not been set.");
    }
} // image::restore()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get image's width.
 */
unsigned int bear::visual::image::width() const
{
  CLAW_PRECOND( is_valid() );
  return (*m_impl)->width();
} // image::width()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get image's height.
 */
unsigned int bear::visual::image::height() const
{
  CLAW_PRECOND( is_valid() );
  return (*m_impl)->height();
} // image::height()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get image's size.
 */
claw::math::coordinate_2d<unsigned int> bear::visual::image::size() const
{
  CLAW_PRECOND( is_valid() );
  return (*m_impl)->size();
} // image::size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the image has transparent pixels.
 */
bool bear::visual::image::has_transparency() const
{
  if( is_valid() )
    return (*m_impl)->has_transparency();
  else
    return false;
} // image::has_transparency()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the image is initialized.
 */
bool bear::visual::image::is_valid() const
{
  bool result = false;

  if ( m_impl != NULL )
    result = ( *m_impl != NULL );

  return result;
} // image::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assigment operator.
 */
const bear::visual::base_image* bear::visual::image::get_impl() const
{
  CLAW_PRECOND( is_valid() );

  // workaround to get the address of the instance of base_image.
  return (*m_impl).operator->();
} // image::get_impl()

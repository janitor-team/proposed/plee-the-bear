/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file visual/code/scene_sprite.cpp
 * \brief Implementation of the visual::scene_sprite.
 * \author Julien Jorge
 */
#include "visual/scene_sprite.hpp"

#include "visual/base_screen.hpp"
#include "visual/scene_element.hpp"

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param x X-position of the sprite on the screen.
 * \param y Y-position of the sprite on the screen.
 * \param s The sprite to draw.
 */
bear::visual::scene_sprite::scene_sprite
( coordinate_type x, coordinate_type y, const sprite& s )
  : base_scene_element(x, y), m_sprite(s)
{

} // scene_sprite::scene_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Allocate a copy of this instance.
 */
bear::visual::base_scene_element* bear::visual::scene_sprite::clone() const
{
  return new scene_sprite(*this);
} // scene_sprite::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a rectangle where the sprite is completely opaque.
 */
bear::visual::rectangle_type bear::visual::scene_sprite::get_opaque_box() const
{
  if ( m_sprite.has_transparency() || (m_sprite.get_angle() != 0)
       || (get_rendering_attributes().get_opacity() != 1) )
    return rectangle_type(0, 0, 0, 0);
  else
    return get_bounding_box();
} // scene_sprite::get_opaque_box()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a rectangle bounding the sprite.
 */
bear::visual::rectangle_type
bear::visual::scene_sprite::get_bounding_box() const
{
  rectangle_type result;
  const coordinate_type w( m_sprite.width() * get_scale_factor_x() );
  const coordinate_type h( m_sprite.height() * get_scale_factor_y() );

  if ( m_sprite.get_angle() != 0 )
    {
      position_type left_bottom( std::numeric_limits<coordinate_type>::max(),
                              std::numeric_limits<coordinate_type>::max() );
      position_type right_top( 0, 0 );

      position_type center( get_position().x + w / 2, get_position().y + h / 2);

      update_side_box( position_type(get_position().x, get_position().y),
                       center, left_bottom, right_top);

      update_side_box( position_type(get_position().x + w, get_position().y),
                       center, left_bottom, right_top);

      update_side_box( position_type(get_position().x, get_position().y + h),
                       center, left_bottom, right_top);

      update_side_box
        ( position_type(get_position().x + w, get_position().y + h), center,
          left_bottom, right_top);

      result = rectangle_type( left_bottom, right_top );
    }
  else
    result =
      rectangle_type
      ( get_position().x, get_position().y, get_position().x + w,
        get_position().y + h );

  return result;
} // scene_sprite::get_bounding_box()

/*----------------------------------------------------------------------------*/
/**
 * \brief Split the sprite in sub sprites.
 * \param boxes The boxes describing how to split the sprite.
 * \param output The resulting sprites (they are inserted to the back).
 */
void bear::visual::scene_sprite::burst
( const rectangle_list& boxes, scene_element_list& output ) const
{
  bitmap_rendering_attributes attr(get_rendering_attributes());
  attr.combine(m_sprite);

  if ( (m_sprite.get_size() != m_sprite.clip_rectangle().size())
       || (get_scale_factor_x() != 1)
       || (get_scale_factor_y() != 1)
       || attr.is_flipped()
       || attr.is_mirrored()
       || (attr.get_angle() != 0) )
    output.push_back( scene_element(*this) );
  else
    {
      rectangle_list::const_iterator it;
      const rectangle_type my_box
        ( get_position(), get_position() + m_sprite.get_size() );

      for (it=boxes.begin(); it!=boxes.end(); ++it)
        if ( my_box.intersects(*it) )
          {
            const rectangle_type inter = my_box.intersection(*it);

            if( (inter.width() > 0) && (inter.height() > 0) )
              {
                const position_type pos( inter.bottom_left() );
                const claw::math::rectangle<unsigned int> clip
                  ( pos.x - get_position().x
                    + m_sprite.clip_rectangle().position.x,
                    get_position().y + m_sprite.height() - inter.top()
                    + m_sprite.clip_rectangle().position.y,
                    inter.width(), inter.height() );

                sprite spr(m_sprite);
                spr.set_clip_rectangle(clip);
                spr.set_size(inter.size());

                scene_sprite scene_spr(*this);
                scene_spr.set_position( pos.x, pos.y );
                scene_spr.set_sprite( spr );
                output.push_back( scene_spr );
              }
          }
    }
} // scene_sprite::burst()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the sprite on a screen.
 * \param scr The screen on which we render the sprite.
 */
void bear::visual::scene_sprite::render( base_screen& scr ) const
{
  if ( !m_sprite.is_valid() )
    return;

  sprite s(m_sprite);
  s.combine( get_rendering_attributes() );
  s.set_size
    ( s.width() * get_scale_factor_x(), s.height() * get_scale_factor_y() );

  scr.render(get_position(), s);
} // scene_sprite::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Change the sprite of the visual.
 * \param spr The new sprite.
 */
void bear::visual::scene_sprite::set_sprite( const sprite& spr )
{
  m_sprite = spr;
} // scene_sprite::set_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update sides of bounding box when we apply a rotation.
 *
 * \param pos The point to rotate.
 * \param center The center of the rotation.
 * \param left_bottom (out) The left_bottom position of the bounding box.
 * \param right_top (out) The right_top position of the bounding box.
 */
void bear::visual::scene_sprite::update_side_box
( const position_type& pos, const position_type& center,
  position_type& left_bottom, position_type& right_top) const
{
  bear::visual::position_type result(center);

  const double a = m_sprite.get_angle();

  result.x += (pos.x - center.x) * cos(a) - (pos.y - center.y) * sin(a);
  result.y += (pos.x - center.x) * sin(a) + (pos.y - center.y) * cos(a);

  if ( result.x < left_bottom.x )
    left_bottom.x = result.x;

  if ( result.y < left_bottom.y )
    left_bottom.y = result.y;

  if ( result.x > right_top.x )
    right_top.x = result.x;

  if ( result.y > right_top.y )
    right_top.y = result.y;
} // scene_sprite::update_side_box()

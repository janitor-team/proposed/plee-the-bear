/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_scene_element.cpp
 * \brief Implementation of the bear::visual::base_scene_element class.
 * \author Julien Jorge
 */
#include "visual/base_scene_element.hpp"

#include "visual/scene_element.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param x X-position of the element on the screen.
 * \param y Y-position of the element on the screen.
 */
bear::visual::base_scene_element::base_scene_element
( coordinate_type x, coordinate_type y )
  : m_position(x, y), m_scale_factor_x(1), m_scale_factor_y(1)
{

} // base_scene_element::base_scene_element()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::visual::base_scene_element::~base_scene_element()
{
  // nothing to do
} // base_scene_element::~base_scene_element()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy on the element.
 */
bear::visual::base_scene_element*
bear::visual::base_scene_element::clone() const
{
  return new base_scene_element(*this);
} // base_scene_element::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a box where the element is fully opaque.
 */
bear::visual::rectangle_type
bear::visual::base_scene_element::get_opaque_box() const
{
  return rectangle_type(0, 0, 0, 0);
} // base_scene_element::get_opaque_box()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the bounding box of the element.
 */
bear::visual::rectangle_type
bear::visual::base_scene_element::get_bounding_box() const
{
  return
    rectangle_type
    ( m_position.x, m_position.y,
      m_position.x + m_rendering_attributes.width() * get_scale_factor_x(),
      m_position.y + m_rendering_attributes.height() * get_scale_factor_y() );
} // base_scene_element::get_bounding_box()

/*----------------------------------------------------------------------------*/
/**
 * \brief Split the element into subelements.
 * \param boxes The boxes describing how to split the element.
 * \param output The resulting elements.
 */
void bear::visual::base_scene_element::burst
( const rectangle_list& boxes, scene_element_list& output ) const
{
  output.push_back( scene_element(*this) );
} // base_scene_element::burst()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render the sprite on a screen.
 * \param scr The screen on which we render the sprite.
 */
void bear::visual::base_scene_element::render( base_screen& scr ) const
{
  // nothing to do
} // base_scene_element::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the position on the element on the screen.
 */
const bear::visual::position_type&
bear::visual::base_scene_element::get_position() const
{
  return m_position;
} // base_scene_element::get_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the position on the element on the screen.
 */
bear::visual::position_type& bear::visual::base_scene_element::get_position()
{
  return m_position;
} // base_scene_element::get_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the position on the element on the screen.
 * \param x The new x-position.
 * \param y The new y-position.
 */
void bear::visual::base_scene_element::set_position
( coordinate_type x, coordinate_type y )
{
  m_position.set(x, y);
} // base_scene_element::set_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the factor applied to the element when rendered.
 * \param x The factor on the x-axis.
 * \param y The factor on the y-axis.
 */
void bear::visual::base_scene_element::set_scale_factor( double x, double y )
{
  m_scale_factor_x = x;
  m_scale_factor_y = y;
} // base_scene_element::set_scale_factor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the factor applied on the x-axis to the element when rendered.
 */
double bear::visual::base_scene_element::get_scale_factor_x() const
{
  return m_scale_factor_x;
} // base_scene_element::get_scale_factor_x()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the factor applied on the y-axis to the element when rendered.
 */
double bear::visual::base_scene_element::get_scale_factor_y() const
{
  return m_scale_factor_y;
} // base_scene_element::get_scale_factor_y()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the attributes applied to the element when rendering.
 * \param a The attributes.
 */
void bear::visual::base_scene_element::set_rendering_attributes
( const bitmap_rendering_attributes& a )
{
  m_rendering_attributes = a;
} // base_scene_element::get_rendering_attributes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the attributes applied to the element when rendering.
 */
const bear::visual::bitmap_rendering_attributes&
bear::visual::base_scene_element::get_rendering_attributes() const
{
  return m_rendering_attributes;
} // base_scene_element::get_rendering_attributes()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the attributes applied to the element when rendering.
 */
bear::visual::bitmap_rendering_attributes&
bear::visual::base_scene_element::get_rendering_attributes()
{
  return m_rendering_attributes;
} // base_scene_element::get_rendering_attributes()

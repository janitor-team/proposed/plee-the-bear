/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file code/color.cpp
 * \brief Implementation of the bear::visual::color class.
 * \author Julien Jorge
 */
#include "visual/color.hpp"

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::visual::color::color()
{
  // nothing to do
} // color::color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param c A color from libclaw.
 */
bear::visual::color::color( const claw::graphic::rgba_pixel& c )
  : claw::graphic::rgba_pixel(c)
{

} // color::color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param c The name of the color.
 */
bear::visual::color::color( const std::string& c )
  : claw::graphic::rgba_pixel(c)
{

} // color::color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param r Intensity of the red component.
 * \param g Intensity of the green component.
 * \param b Intensity of the blue component.
 */
bear::visual::color::color
( component_type r, component_type g, component_type b )
  : claw::graphic::rgba_pixel
    (r, g, b, std::numeric_limits<component_type>::max())
{

} // color::color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param r Intensity of the red component.
 * \param g Intensity of the green component.
 * \param b Intensity of the blue component.
 * \param o opacity.
 */
bear::visual::color::color
( component_type r, component_type g, component_type b, component_type o )
  : claw::graphic::rgba_pixel(r, g, b, o)
{

} // color::color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the intensity of the red component.
 * \param i The intensity of the component.
 */
void bear::visual::color::set_red_intensity( double i )
{
  const component_type max_comp = std::numeric_limits<component_type>::max();

  if ( i < 0 )
    components.red = 0;
  else if ( i > 1 )
    components.red = 1;
  else
    components.red = max_comp * i;
} // color::set_red_intensity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the intensity of the green component.
 * \param i The intensity of the component.
 */
void bear::visual::color::set_green_intensity( double i )
{
  const component_type max_comp = std::numeric_limits<component_type>::max();

  if ( i < 0 )
    components.green = 0;
  else if ( i > 1 )
    components.green = 1;
  else
    components.green = max_comp * i;
} // color::set_green_intensity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the intensity of the blue component.
 * \param i The intensity of the component.
 */
void bear::visual::color::set_blue_intensity( double i )
{
  const component_type max_comp = std::numeric_limits<component_type>::max();

  if ( i < 0 )
    components.blue = 0;
  else if ( i > 1 )
    components.blue = 1;
  else
    components.blue = max_comp * i;
} // color::set_blue_intensity()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the intensity of the transparency.
 * \param i The intensity.
 */
void bear::visual::color::set_opacity( double o )
{
  const component_type max_comp = std::numeric_limits<component_type>::max();

  if ( o < 0 )
    components.alpha = 0;
  else if ( o > 1 )
    components.alpha = 1;
  else
    components.alpha = max_comp * o;
} // color::set_opacity()

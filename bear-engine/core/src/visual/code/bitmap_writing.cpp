/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
#include "visual/bitmap_writing.hpp"

#include "visual/scene_writing.hpp"
#include "visual/bitmap_font.hpp"
#include "visual/text_layout.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param f The font used to display the text.
 * \param str The text to arrange.
 * \param list (out) The sprites of the text.
 *
 * The instance keep the references on each parameter. So they must live longen
 * than \a this.
 */
bear::visual::bitmap_writing::arrange_sprite_list::arrange_sprite_list
( const bitmap_font& f, const std::string& str, sprite_list& list )
  : m_text(str), m_font(f), m_sprites(list)
{

} // bitmap_writing::arrange_sprite_list::arrange_sprite_list()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw a word on the screen.
 * \param x The x-position where the word starts.
 * \param y The y-position where the word starts.
 * \param first The first character of the word to display.
 * \param last The character just past the last character to display.
 */
void bear::visual::bitmap_writing::arrange_sprite_list::operator()
( coordinate_type x, coordinate_type y, std::size_t first, std::size_t last )
{
  position_type pos(x, y);

  for (; first!=last; ++first)
    {
      const sprite s(m_font.get_sprite(m_text[first]));
      m_sprites.push_back( placed_sprite(pos, s) );
      pos.x += s.width();
    }
} // bitmap_writing::arrange_sprite_list::operator()()




/*----------------------------------------------------------------------------*/
/**
 * \brief Get the number of sprites in the writing.
 */
std::size_t bear::visual::bitmap_writing::get_sprites_count() const
{
  return m_sprites.size();
} // bitmap_writing::get_sprites_count()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a sprite from the writing.
 * \param i The index of the sprite to get.
 */
bear::visual::placed_sprite
bear::visual::bitmap_writing::get_sprite( std::size_t i ) const
{
  placed_sprite result;

  result = m_sprites[i];
  result.get_sprite().combine(*this);

  return result;
} // bitmap_writing::get_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param f The font used to render the text.
 * \param str The text to render.
 * \param s The maximum size of the bitmap_writing.
 */
void bear::visual::bitmap_writing::create
( const bitmap_font& f, const std::string& str, const size_box_type& s )
{
  set_size(s);
  m_sprites.clear();
  m_sprites.reserve( str.length() );

  arrange_sprite_list func(f, str, m_sprites);
  text_layout layout(f, str, get_size());

  layout.arrange_text( func );
} // bitmap_writing::create()

/*----------------------------------------------------------------------------*/
/**
 * \brief Call the render method of a scene_writing (double dispach) .
 * \param s The scene_writing on which we call the method.
 * \param scr The screen to pass to \a s.
 */
void bear::visual::bitmap_writing::call_render
( const scene_writing& s, base_screen& scr ) const
{
  s.render(*this, scr);
} // bitmap_writing::call_render()

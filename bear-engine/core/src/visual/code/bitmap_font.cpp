/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bitmap_font.hpp
 * \brief Implementation of the bear::visual::bitmap_font class.
 * \author Sebastien Angibaud
 */
#include "visual/bitmap_font.hpp"

#include "visual/scene_sprite.hpp"
#include "visual/screen.hpp"

#include <algorithm>
#include <claw/algorithm.hpp>
#include <claw/functional.hpp>
#include <claw/assert.hpp>
#include <claw/exception.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param characters The description of the characters in the font.
 */
bear::visual::bitmap_font::bitmap_font( const symbol_table& characters )
{
  CLAW_PRECOND( !characters.characters.empty() );

  make_sprites( characters );
  make_missing( characters );
} // bitmap_font::bitmap_font()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the height of the 'x' glyph.
 */
bear::visual::size_type bear::visual::bitmap_font::get_ex() const
{
  return get_glyph_size('x').y;
} // bitmap_font::get_ex()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the width of the 'm' glyph.
 */
bear::visual::size_type bear::visual::bitmap_font::get_em() const
{
  return get_glyph_size('m').x;
} // bitmap_font::get_em()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the width of a glyph.
 * \param c The glyph for which we want the size.
 */
bear::visual::size_box_type
bear::visual::bitmap_font::get_glyph_size(charset::char_type c) const
{
  return m_missing.get_size();
} // bitmap_font::get_glyph_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the maximum height of the glyphes.
 */
bear::visual::size_type bear::visual::bitmap_font::get_max_glyph_height() const
{
  return m_missing.get_size().y;
} // bitmap_font::get_max_glyph_height()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw a word on the screen.
 * \param screen The screen on which to draw.
 * \param pos The position where the text begins.
 * \param str The string to write.
 */
void bear::visual::bitmap_font::render_text
( visual::screen& screen, const claw::math::coordinate_2d<unsigned int>& pos,
  const std::string& str ) const
{
  claw::math::coordinate_2d<unsigned int> cur_pos(pos);
  std::string::const_iterator it;

  for (it=str.begin(); it!=str.end(); ++it)
    {
      const sprite& s(get_sprite(*it));
      screen.render( scene_sprite(cur_pos.x, cur_pos.y, s) );
      cur_pos.x += s.width();
    }
} // bitmap_font::render_text()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sprite of a character.
 * \param character The character to get.
 */
const bear::visual::sprite&
bear::visual::bitmap_font::get_sprite( charset::char_type character ) const
{
  const std::map<charset::char_type, sprite>::const_iterator it =
    m_characters.find(character);

  if ( it == m_characters.end() )
    return m_missing;
  else
    return it->second;
} // bitmap_font::get_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the sprites for the characters.
 * \param characters The description of the characters in the font.
 */
void bear::visual::bitmap_font::make_sprites( const symbol_table& characters )
{
  std::map<charset::char_type, symbol_table::char_position>::const_iterator it;

  for (it=characters.characters.begin(); it!=characters.characters.end(); ++it)
    if ( it->second.image_index < characters.font_images.size() )
      if ( it->second.position.x + characters.size.x
           < characters.font_images[it->second.image_index].width() )
      if ( it->second.position.y + characters.size.y
           < characters.font_images[it->second.image_index].height() )
        {
          const claw::math::rectangle<unsigned int> clip
            ( it->second.position, characters.size );

          m_characters[it->first] =
            sprite( characters.font_images[it->second.image_index], clip);
        }
} // bitmap_font::make_sprites()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the sprite for a missing character.
 * \param characters The description of the characters in the font.
 */
void bear::visual::bitmap_font::make_missing( const symbol_table& characters )
{
  CLAW_PRECOND( !characters.characters.empty() );

  claw::math::rectangle<unsigned int> clip
    ( 0, 0, characters.font_images[0].width(),
      characters.font_images[0].height() );

  m_missing = sprite(characters.font_images[0], clip);
  m_missing.set_size( characters.size );
} // bitmap_font::make_missing()

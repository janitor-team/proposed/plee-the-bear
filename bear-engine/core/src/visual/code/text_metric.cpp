/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file text_metric.hpp
 * \brief Implementation of the bear::visual::text_metric class.
 * \author Julien Jorge
 */
#include "visual/text_metric.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param text The text to measure.
 * \param f The font used to print the text.
 */
bear::visual::text_metric::text_metric( const std::string& text, const font& f )
  : m_pixel_size(0, 0), m_character_size(0, 0)
{
  unsigned int line_size(0);
  coordinate_type line_pixel_width(0);

  for (unsigned int i=0; i!=text.size(); ++i)
    if (text[i] == '\n')
      {
        ++m_character_size.y;

        if (line_size > m_character_size.x)
          m_character_size.x = line_size;

        if (line_pixel_width > m_pixel_size.x)
          m_pixel_size.x = line_pixel_width;

        line_size = 0;
        line_pixel_width = 0;
      }
    else
      {
        ++line_size;
        line_pixel_width += f->get_glyph_size(text[i]).x;
      }

  if ( text.size() > 0 )
    if ( text[text.size()-1] != '\n' )
      {
        ++m_character_size.y;

        if (line_size > m_character_size.x)
          m_character_size.x = line_size;

        if (line_pixel_width > m_pixel_size.x)
          m_pixel_size.x = line_pixel_width;
      }

  m_pixel_size.y = m_character_size.y * f->get_max_glyph_height();
} // text_metric::text_metric()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the width of the text, in pixels.
 */
unsigned int bear::visual::text_metric::width() const
{
  return m_pixel_size.x;
} // text_metric::width()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the height of the text, in pixels.
 */
unsigned int bear::visual::text_metric::height() const
{
  return m_pixel_size.y;
} // text_metric::height()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the width of the text, in characters.
 */
unsigned int bear::visual::text_metric::longest_line() const
{
  return m_character_size.x;
} // text_metric::longest_line()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the height of the text, in characters.
 */
unsigned int bear::visual::text_metric::lines() const
{
  return m_character_size.y;
} // text_metric::lines()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file image.cpp
 * \brief Implementation of the bear::visual::image class.
 * \author Julien Jorge
 */
#include <climits>
#include <limits>

#include "visual/gl_image.hpp"

#include <claw/exception.hpp>
#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor with a claw::graphic::gl_image object.
 * \param data The image to copy.
 */
bear::visual::gl_image::gl_image(const claw::graphic::image& data)
  : m_texture_id(0), m_size(data.width(), data.height()),
    m_has_transparency(false)
{
  create_texture();
  copy_scanlines(data);
} // gl_image::gl_image() [claw::graphic::gl_image]

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::visual::gl_image::~gl_image()
{
  glDeleteTextures(1, &m_texture_id);
} // gl_image::~gl_image()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get OpenGL texture identifier.
 */
GLuint bear::visual::gl_image::texture_id() const
{
  return m_texture_id;
} // gl_image::texture_id()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get image's size.
 */
claw::math::coordinate_2d<unsigned int> bear::visual::gl_image::size() const
{
  return m_size;
} // gl_image::size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the image has transparent pixels.
 */
bool bear::visual::gl_image::has_transparency() const
{
  return m_has_transparency;
} // gl_image::has_transparency()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create an empty OpenGL texture.
 */
void bear::visual::gl_image::create_texture()
{
  unsigned int v;
  for ( v=1; (v < m_size.x) && /* overflow */ (v != 0); v *= 2 ) { }

  m_size.x = v;

  for ( v=1; (v < m_size.y) && /* overflow */ (v != 0); v *= 2 ) { }

  m_size.y = v;

  glGenTextures(1, &m_texture_id);
  glBindTexture(GL_TEXTURE_2D, m_texture_id);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_size.x, m_size.y, 0, GL_RGBA,
               GL_UNSIGNED_BYTE, NULL );

  if ( glGetError() != GL_NO_ERROR )
    throw claw::exception( "OpenGL error" );
} // gl_image::create_texture()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy the content of an image to the texture.
 * \param data The image to copy.
 */
void bear::visual::gl_image::copy_scanlines( const claw::graphic::image& data )
{
  const claw::graphic::rgba_pixel_8::component_type opaque =
    std::numeric_limits<claw::graphic::rgba_pixel_8::component_type>::max();

  claw::graphic::rgba_pixel_8* line =
    new claw::graphic::rgba_pixel_8[data.width()];
  for (unsigned int y=0; y!=data.height(); ++y)
    {
      std::copy( data[y].begin(), data[y].end(), line );
      glTexSubImage2D( GL_TEXTURE_2D, 0, 0, y, data.width(), 1, GL_RGBA,
                       GL_UNSIGNED_BYTE, line );

      for ( claw::graphic::rgba_pixel_8* p=line;
            (p!=line + data.width()) && !m_has_transparency; ++p )
        m_has_transparency = p->components.alpha != opaque;
    }
  delete[] line;
} // gl_image::copy_scanlines()

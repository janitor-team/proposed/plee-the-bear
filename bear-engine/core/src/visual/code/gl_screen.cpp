/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file gl_screen.cpp
 * \brief Implementation of the bear::visual::gl_screen class.
 * \author Julien Jorge
 */
#include "visual/gl_screen.hpp"

#include "visual/gl_image.hpp"
#include "visual/sprite.hpp"

#include <claw/exception.hpp>

#include <SDL/SDL.h>
#include <GL/glext.h>

#include <limits>
#include <list>

/*----------------------------------------------------------------------------*/
/**
 * \brief Global initializations common to all gl_screens. Must be called at the
 *        begining of your program.
 */
void bear::visual::gl_screen::initialize()
{
  if ( !SDL_WasInit(SDL_INIT_VIDEO) )
    if ( SDL_InitSubSystem(SDL_INIT_VIDEO) != 0 )
      throw claw::exception( SDL_GetError() );

  if ( SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 ) != 0 )
    {
      SDL_QuitSubSystem(SDL_INIT_VIDEO);
      throw claw::exception( SDL_GetError() );
    }

  for (unsigned int i=0; i!=SDL_USEREVENT; ++i)
    SDL_EventState( i, SDL_DISABLE );

  SDL_EventState( SDL_QUIT, SDL_ENABLE );
  SDL_EventState( SDL_VIDEORESIZE, SDL_ENABLE );

} // gl_screen::initialize()

/*----------------------------------------------------------------------------*/
/**
 * \brief Global uninitializations common to all gl_screens. Must be called at
 *        the end of your program.
 */
void bear::visual::gl_screen::release()
{
  SDL_QuitSubSystem(SDL_INIT_VIDEO);
} // gl_screen::release()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param size Size of the gl_screen.
 * \param title The title of the window created.
 * \param full Tell if the window is full gl_screen or not.
 */
bear::visual::gl_screen::gl_screen
( const claw::math::coordinate_2d<unsigned int>& size,
  const std::string& title, bool full )
  : m_size(size), m_screenshot_buffer(NULL), m_title(title)
{
  fullscreen(full);
  m_need_restoration = false;

  SDL_WM_SetCaption( title.c_str(), NULL );
  glEnable(GL_TEXTURE_2D);

#ifdef BEAR_USE_GL_DEPTH_TEST
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
#endif

  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  resize_view(m_size.x, m_size.y);
} // gl_screen::gl_screen() [constructor]

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::visual::gl_screen::~gl_screen()
{
  delete[] m_screenshot_buffer;
} // gl_screen::~gl_screen)

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a new dimension for the resulting projection (doesn't change
 *        the gl_screen's size.
 * \param width Target's width.
 * \param height Target's height.
 */
void
bear::visual::gl_screen::resize_view(unsigned int width, unsigned int height)
{
  glViewport(0, 0, width, height );
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho( 0, m_size.x, 0, m_size.y, -1, 0 );
  glMatrixMode(GL_MODELVIEW);

  delete[] m_screenshot_buffer;
  m_screenshot_buffer = new claw::graphic::rgba_pixel_8[width * height];

  failure_check( __FUNCTION__ );
} // gl_screen::resize_view()

/*----------------------------------------------------------------------------*/
/**
 * \brief Turn fullscreen mode on/off.
 * \param b Tell if we want a fullscreen mode.
 */
void bear::visual::gl_screen::fullscreen( bool b )
{
  set_video_mode(m_size.x, m_size.y, b);
} // gl_screen::fullscreen()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the size of the gl_screen.
 */
claw::math::coordinate_2d<unsigned int>
bear::visual::gl_screen::get_size() const
{
  return m_size;
} // gl_screen::get_size()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the environment needs to be restored.
 */
bool bear::visual::gl_screen::need_restoration() const
{
  return m_need_restoration;
} // gl_screen::need_restoration()

/*----------------------------------------------------------------------------*/
/**
 * \brief Inform that the environment had been restored.
 */
void bear::visual::gl_screen::set_restored()
{
  m_need_restoration = false;
} // gl_screen::set_restored()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the color of the background, used to clear the screen.
 * \param c The color.
 */
void bear::visual::gl_screen::set_background_color( const color_type& c )
{
  const GLfloat max
    ( std::numeric_limits<claw::graphic::rgb_pixel::component_type>::max() );

  glClearColor( (GLfloat)c.components.red / max,
                (GLfloat)c.components.green / max,
                (GLfloat)c.components.blue / max,
                (GLfloat)c.components.alpha / max );
} // gl_screen::set_background_color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the color of the background, used to clear the screen.
 */
bear::visual::color_type bear::visual::gl_screen::get_background_color() const
{
  const GLfloat max
    ( std::numeric_limits<claw::graphic::rgb_pixel::component_type>::max() );

  GLfloat c[4];
  glGetFloatv( GL_COLOR_CLEAR_VALUE, c );

  color_type result;
  result.components.red = max * c[0];
  result.components.green = max * c[1];
  result.components.blue = max * c[2];
  result.components.alpha = max * c[3];

  return result;
} // gl_screen::get_background_color()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the rendering process.
 */
void bear::visual::gl_screen::begin_render()
{
  glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  m_z_position = 0;
} // gl_screen::begin_render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw a sprite on the gl_screen.
 * \param pos On gl_screen position of the sprite.
 * \param s The sprite to draw.
 */
void bear::visual::gl_screen::render
( const position_type& pos, const sprite& s )
{
  if ( s.has_transparency() )
    glEnable(GL_BLEND);

  glColor4f( s.get_red_intensity(), s.get_green_intensity(),
             s.get_blue_intensity(), s.get_opacity() );

  const gl_image* impl = static_cast<const gl_image*>(s.get_image().get_impl());
  glBindTexture( GL_TEXTURE_2D, impl->texture_id() );

  if ( s.get_angle() == 0 )
    {
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
  else
    {
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

  render_sprite( pos, s );

  if ( s.has_transparency() )
    glDisable(GL_BLEND);

  failure_check( __FUNCTION__ );
} // gl_screen::render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop the rendering process.
 * \return false if this screen has been closed by the user.
 */
bool bear::visual::gl_screen::end_render()
{
  glFlush();
  SDL_GL_SwapBuffers();
  failure_check( __FUNCTION__ );

  return !is_closed();
} // gl_screen::end_render()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw a line.
 * \param color The color of the line.
 * \param p The points of the line.
 * \param w The width of the line.
 * \param close Tell if the line come back to the first coordinate.
 */
void bear::visual::gl_screen::draw_line
( const color_type& color, const std::vector<position_type>& p,
  double w, bool close )
{
  glBindTexture( GL_TEXTURE_2D, 0 );
  glLineWidth(w);

  const GLfloat max =
    std::numeric_limits<color_type::component_type>::max();

  if (color.components.alpha != max)
    glEnable(GL_BLEND);

  glBegin(GL_LINE_STRIP);
  {
    glColor4f( (GLfloat)color.components.red / max,
         (GLfloat)color.components.green / max,
         (GLfloat)color.components.blue / max,
         (GLfloat)color.components.alpha / max );

    for ( unsigned int i=0; i!=p.size(); ++i )
      glVertex3f( p[i].x, p[i].y, m_z_position );

    if ( close )
      glVertex3f( p[0].x, p[0].y, m_z_position );
  }
  glEnd();

  update_z_position();

  if (color.components.alpha != max)
    glDisable(GL_BLEND);

  failure_check( __FUNCTION__ );
} // gl_screen::draw_line()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw a filled polygon.
 * \param color The color of the polygon.
 * \param p The points of the polygon.
 */
void bear::visual::gl_screen::draw_polygon
( const color_type& color, const std::vector<position_type>& p )
{
  glBindTexture( GL_TEXTURE_2D, 0 );

  const GLfloat max = std::numeric_limits<color_type::component_type>::max();

  if (color.components.alpha != max)
    glEnable(GL_BLEND);

  glBegin(GL_QUADS);
  {
    glColor4f( (GLfloat)color.components.red / max,
         (GLfloat)color.components.green / max,
         (GLfloat)color.components.blue / max,
         (GLfloat)color.components.alpha / max );

    for ( unsigned int i=0; i!=p.size(); ++i )
      glVertex3f( p[i].x, p[i].y, m_z_position );
  }
  glEnd();

  update_z_position();

  if (color.components.alpha != max)
    glDisable(GL_BLEND);

  failure_check( __FUNCTION__ );
} // gl_screen::draw_polygon()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do a gl_screen shot.
 * \param img The image in which we save the content of the gl_screen.
 */
void bear::visual::gl_screen::shot( claw::graphic::image& img ) const
{
  GLint p[4];
  glGetIntegerv( GL_VIEWPORT, p );
  const unsigned int w = p[2];
  const unsigned int h = p[3];

  img.set_size( w, h );
  const std::size_t pixels_count(w * h);

  glReadPixels( 0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE, m_screenshot_buffer );

  for ( claw::graphic::rgba_pixel_8* it=m_screenshot_buffer;
        it!=m_screenshot_buffer + pixels_count;
        ++it )
    it->components.alpha = 255;

  for (unsigned int y=0; y!=h; ++y)
    std::copy( m_screenshot_buffer + y * w,
               m_screenshot_buffer + (y+1) * w,
               img[h - y - 1].begin() );

  failure_check( __FUNCTION__ );
} // gl_screen::shot()

/*----------------------------------------------------------------------------*/
/**
 * \brief Render a sprite with transformation (flip or mirror).
 * \param pos On screen position of the sprite.
 * \param s The sprite to draw.
 */
void bear::visual::gl_screen::render_sprite
( const position_type& pos, const sprite& s )
{
  claw::math::box_2d<GLdouble> clip_vertices;
  const claw::math::rectangle<GLdouble> clip_rectangle(s.clip_rectangle());
  const claw::math::coordinate_2d<GLdouble> tex_size(s.get_image().size());
  const GLdouble pixel_width( 1.0 / tex_size.x );
  const GLdouble pixel_height( 1.0 / tex_size.y );

  clip_vertices.first_point.x  = clip_rectangle.position.x / tex_size.x;
  clip_vertices.second_point.x =
    (clip_rectangle.right() - clip_rectangle.width * pixel_width) / tex_size.x;
  clip_vertices.first_point.y  = clip_rectangle.position.y / tex_size.y;
  clip_vertices.second_point.y =
    (clip_rectangle.bottom() - clip_rectangle.height * pixel_height)
    / tex_size.y;

  if ( s.is_mirrored() )
    std::swap(clip_vertices.first_point.x, clip_vertices.second_point.x);

  if ( s.is_flipped() )
    std::swap(clip_vertices.first_point.y, clip_vertices.second_point.y);

  typedef claw::math::coordinate_2d<GLdouble> coord_double;
  coord_double render_coord[4];

  const coord_double center = pos + s.get_size() / 2;

  coord_double top_left( pos );
  coord_double bottom_right( pos );
  top_left.y += s.height();
  bottom_right.x += s.width();

  render_coord[0] = rotate( top_left, s.get_angle(), center );
  render_coord[1] = rotate( pos + s.get_size(), s.get_angle(), center );
  render_coord[2] = rotate( bottom_right, s.get_angle(), center );
  render_coord[3] = rotate( pos, s.get_angle(), center );

  render_image( render_coord, clip_vertices );
} // gl_screen::render_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Draw current texture.
 * \param render_box On gl_screen position and size of the texture.
 * \param clip Part of the texture to draw.
 */
void bear::visual::gl_screen::render_image
( const claw::math::coordinate_2d<GLdouble> render_coord[],
  const claw::math::box_2d<GLdouble>& clip )
{
  glBegin(GL_QUADS);
  {
    // Top-left corner
    glTexCoord2d( clip.first_point.x, clip.first_point.y );
    glVertex3d(render_coord[0].x, render_coord[0].y, m_z_position);

    // Top-right corner
    glTexCoord2d( clip.second_point.x, clip.first_point.y );
    glVertex3d(render_coord[1].x, render_coord[1].y, m_z_position);

    // Bottom-right corner
    glTexCoord2d( clip.second_point.x, clip.second_point.y );
    glVertex3d(render_coord[2].x, render_coord[2].y, m_z_position);

    // Bottom-left corner
    glTexCoord2d( clip.first_point.x, clip.second_point.y );
    glVertex3d(render_coord[3].x, render_coord[3].y, m_z_position);
  }
  glEnd();

  update_z_position();

  failure_check( __FUNCTION__ );
} // gl_screen::render_image()

/*----------------------------------------------------------------------------*/
claw::math::coordinate_2d<GLdouble> bear::visual::gl_screen::rotate
( const claw::math::coordinate_2d<GLdouble>& pos, GLdouble a,
  const claw::math::coordinate_2d<GLdouble>& center ) const
{
  claw::math::coordinate_2d<GLdouble> result(pos);
  result.rotate(center, a);
  return result;
} // gl_screen::rotate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Check if no error has occured.
 * \param where Information on where this function is called.
 */
void bear::visual::gl_screen::failure_check( const std::string& where ) const
{
  GLenum err = glGetError();

  if ( err != GL_NO_ERROR)
    {
      std::string err_string(where + ": ");

      switch (err)
        {
        case GL_NO_ERROR:
          err_string += "no error (?).";
          break;
        case GL_INVALID_ENUM:
          err_string +=
            "unacceptable value is specified for an enumerated argument.";
          break;
        case GL_INVALID_VALUE:
          err_string += "numeric argument is out of range.";
          break;
        case GL_INVALID_OPERATION:
          err_string += "operation is not allowed in the current state.";
          break;
        case GL_STACK_OVERFLOW: err_string += "stack overflow.";
          break;
        case GL_STACK_UNDERFLOW: err_string += "stack underflow.";
          break;
        case GL_OUT_OF_MEMORY:
          err_string += "not enough memory to execute the command.";
          break;
        case GL_TABLE_TOO_LARGE:
          err_string +=
            "table exceeds the implementation's maximum supported table size";
          break;
        default:
          err_string += "unknow error code.";
        }

      throw claw::exception( err_string );
    }
} // gl_screen::failure_check()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the size of the screen.
 * \param w The width of the screen.
 * \param h The height of the screen.
 * \param f Tell if we want a fullscreen mode.
 */
void bear::visual::gl_screen::set_video_mode
( unsigned int w, unsigned int h, bool f )
{
#ifdef _WIN32
  release();
  initialize();
#endif

  Uint32 flags = SDL_OPENGL | SDL_RESIZABLE;

  if (f)
    flags |= SDL_FULLSCREEN;

  SDL_Surface* s = SDL_SetVideoMode( w, h, 32, flags );

  if (!s)
    throw claw::exception( SDL_GetError() );

  SDL_ShowCursor(0);

  glClearColor(0.0, 0.0, 0.0, 0.0);
  glClearDepth(1.0);

  resize_view(w, h);

#ifdef _WIN32
  glEnable(GL_TEXTURE_2D);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  SDL_WM_SetCaption( m_title.c_str(), NULL );

  m_need_restoration = true;
#endif
} // gl_screen::set_video_mode()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the user wants to close this screen.
 */
bool bear::visual::gl_screen::is_closed()
{
  SDL_PumpEvents();

  std::list<SDL_Event> not_mine;
  SDL_Event e;
  bool result = false;

  while ( !result && (SDL_PeepEvents(&e, 1, SDL_GETEVENT, SDL_ALLEVENTS) == 1) )
    if ( e.type == SDL_QUIT )
      result = true;
    else if ( e.type == SDL_VIDEORESIZE )
      set_video_mode( e.resize.w, e.resize.h, false );
    else
      not_mine.push_back(e);

  for ( ; !not_mine.empty(); not_mine.pop_front() )
    SDL_PushEvent( &not_mine.front() );

  return result;
} // gl_screen::is_closed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the z position at with the next element will be rendered.
 */
void bear::visual::gl_screen::update_z_position()
{
  m_z_position += std::numeric_limits<GLdouble>::epsilon();
} // gl_screen::update_z_position()

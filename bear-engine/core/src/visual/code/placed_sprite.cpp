/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file code/placed_sprite.hpp
 * \brief Implementation of the bear::visual::placed_sprite class.
 * \author Julien Jorge
 */
#include "visual/placed_sprite.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Default constructor.
 */
bear::visual::placed_sprite::placed_sprite()
{

} // placed_sprite::placed_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param x The x-positon of the sprite.
 * \param y The y-position of the sprite.
 * \param s The sprite.
 */
bear::visual::placed_sprite::placed_sprite
( coordinate_type x, coordinate_type y, const sprite& s )
  : m_sprite(s), m_position(x, y)
{

} // placed_sprite::placed_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param p The positon of the sprite.
 * \param s The sprite.
 */
bear::visual::placed_sprite::placed_sprite
( const position_type& p, const sprite& s )
  : m_sprite(s), m_position(p)
{

} // placed_sprite::placed_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sprite.
 */
const bear::visual::sprite& bear::visual::placed_sprite::get_sprite() const
{
  return m_sprite;
} // placed_sprite::get_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the sprite.
 */
bear::visual::sprite& bear::visual::placed_sprite::get_sprite()
{
  return m_sprite;
} // placed_sprite::get_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the sprite.
 * \param spr The new sprite.
 */
void bear::visual::placed_sprite::set_sprite( const sprite& spr )
{
  m_sprite = spr;
} // placed_sprite::set_sprite()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the position.
 */
const bear::visual::position_type&
bear::visual::placed_sprite::get_position() const
{
  return m_position;
} // placed_sprite::get_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the position.
 */
bear::visual::position_type& bear::visual::placed_sprite::get_position()
{
  return m_position;
} // placed_sprite::get_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the position.
 * \param p The new position.
 */
void bear::visual::placed_sprite::set_position( const position_type& p )
{
  m_position = p;
} // placed_sprite::set_position()

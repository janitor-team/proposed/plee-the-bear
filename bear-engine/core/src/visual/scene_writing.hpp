/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file scene_writing.hpp
 * \brief A writing on the screen.
 * \author Julien Jorge
 */
#ifndef __VISUAL_SCENE_WRITING_HPP__
#define __VISUAL_SCENE_WRITING_HPP__

#include "visual/base_scene_element.hpp"
#include "visual/writing.hpp"

#include "visual/class_export.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief A writing on the screen.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT scene_writing:
      public base_scene_element
    {
    private:
      /** \brief The type of the writing to display. */
      typedef writing writing_type;

    public:
      scene_writing( coordinate_type x, coordinate_type y, const writing& s );

      base_scene_element* clone() const;

      rectangle_type get_opaque_box() const;
      rectangle_type get_bounding_box() const;

      void burst
      ( const rectangle_list& boxes, scene_element_list& output ) const;

      void render( base_screen& scr ) const;
      void render( const bitmap_writing& w, base_screen& scr ) const;

    private:
      /** \brief The writing. */
      const writing m_writing;

    }; // class scene_writing
  } // namespace visual
} // namespace bear

#endif // __VISUAL_SCENE_WRITING_HPP__

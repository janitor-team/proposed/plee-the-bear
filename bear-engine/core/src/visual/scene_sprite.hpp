/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file scene_sprite.hpp
 * \brief A sprite on the screen.
 * \author Julien Jorge
 */
#ifndef __VISUAL_SCENE_SPRITE_HPP__
#define __VISUAL_SCENE_SPRITE_HPP__

#include "visual/base_scene_element.hpp"
#include "visual/sprite.hpp"

#include "visual/class_export.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief A sprite on the screen.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT scene_sprite:
      public base_scene_element
    {
    public:
      scene_sprite( coordinate_type x, coordinate_type y, const sprite& s );

      base_scene_element* clone() const;

      rectangle_type get_opaque_box() const;
      rectangle_type get_bounding_box() const;

      void burst
      ( const rectangle_list& boxes, scene_element_list& output ) const;

      void render( base_screen& scr ) const;

    private:
      void set_sprite( const sprite& spr );

      void update_side_box
      ( const position_type& pos, const position_type& center,
        position_type& left_bottom, position_type& right_top ) const;

    private:
      /** \brief The sprite. */
      sprite m_sprite;

    }; // class scene_sprite
  } // namespace visual
} // namespace bear

#endif // __VISUAL_SCENE_SPRITE_HPP__

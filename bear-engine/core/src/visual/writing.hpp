/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file writing.hpp
 * \brief A text written with some font.
 * \author Julien Jorge
 */
#ifndef __VISUAL_WRITING_HPP__
#define __VISUAL_WRITING_HPP__

#include "visual/class_export.hpp"

#include "visual/font.hpp"

namespace bear
{
  namespace visual
  {
    class base_screen;
    class bitmap_writing;
    class scene_writing;

    /**
     * \brief A text written with some font.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT writing
    {
    private:
      typedef bitmap_writing writing_type;

    public:
      typedef writing_type& writing_reference;
      typedef writing_type const& const_writing_reference;
      typedef writing_type* writing_pointer;
      typedef writing_type const* const_writing_pointer;

    public:
      writing();
      writing( const writing& that );
      writing( const font& f, const std::string& str );
      writing( const font& f, const std::string& str, const size_box_type& s );
      ~writing();

      writing& operator=( const writing& that );
      void swap( writing& that );

      coordinate_type get_width() const;
      coordinate_type get_height() const;
      const size_box_type& get_size() const;

      writing_reference operator*();
      const_writing_reference operator*() const;
      writing_pointer operator->();
      const_writing_pointer operator->() const;

      const_writing_pointer get_impl() const;

      void create( const font& f, const std::string& str );
      void create
      ( const font& f, const std::string& str, const size_box_type& s );

      void call_render( const scene_writing& s, base_screen& scr ) const;

    private:
      /** \brief The writing on which we work. */
      writing_pointer m_writing;

      /** \brief Tell how many \b other instances of this class share the same
          m_writing. */
      std::size_t* m_counter;

    }; // class writing
  } // namespace visual
} // namespace bear

namespace std
{
  void swap( bear::visual::writing& a, bear::visual::writing& b );
} // namespace std

#endif // __VISUAL_WRITING_HPP__

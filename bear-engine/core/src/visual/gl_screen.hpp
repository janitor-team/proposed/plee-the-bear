/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file gl_screen.hpp
 * \brief OpenGL implementation of the screen.
 * \author Julien Jorge
 */
#ifndef __VISUAL_GL_SCREEN_HPP__
#define __VISUAL_GL_SCREEN_HPP__

#include "visual/base_screen.hpp"
#include <GL/gl.h>

namespace bear
{
  namespace visual
  {
    /**
     * \brief OpenGL implementation of the screen.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT gl_screen:
      public base_screen
    {
    public:
      static void initialize();
      static void release();

      gl_screen( const claw::math::coordinate_2d<unsigned int>& size,
                 const std::string& title="", bool full=false );
      ~gl_screen();

      void resize_view(unsigned int width, unsigned int height);
      void fullscreen( bool b );
      claw::math::coordinate_2d<unsigned int> get_size() const;
      bool need_restoration() const;
      void set_restored();

      void set_background_color( const color_type& c );
      color_type get_background_color() const;

      void begin_render();
      void render( const position_type& pos, const sprite& s );
      bool end_render();

      void draw_line
      ( const color_type& color,
        const std::vector<position_type>& p, double w = 1.0, bool close=false );

      void draw_polygon
      ( const color_type& color,
        const std::vector<position_type>& p );

      void shot( claw::graphic::image& img ) const;

    private:
      void render_sprite( const position_type& pos, const sprite& s );

      void render_image
      ( const claw::math::coordinate_2d<GLdouble> render_coord[],
        const claw::math::box_2d<GLdouble>& clip );

      claw::math::coordinate_2d<GLdouble>
      rotate( const claw::math::coordinate_2d<GLdouble>& pos, GLdouble a,
              const claw::math::coordinate_2d<GLdouble>& center ) const;

      void failure_check( const std::string& where ) const;

      void set_video_mode( unsigned int w, unsigned int y, bool f );
      bool is_closed();

      void update_z_position();

    private:
      /** \brief The width and height of the screen. */
      claw::math::coordinate_2d<unsigned int> m_size;

      /** \brief Tell if the screen needs to be restored. */
      bool m_need_restoration;

      /** \brief A buffer in which we do the screenshots, to avoid an allocation
          at each call. */
      claw::graphic::rgba_pixel_8* m_screenshot_buffer;

      /** \brief The z position of the next item to render. */
      GLdouble m_z_position;

      /** \brief The title of the window. */
      const std::string m_title;

    }; // class gl_screen
  } // namespace visual
} // namespace bear

#endif // __VISUAL_GL_SCREEN_HPP__

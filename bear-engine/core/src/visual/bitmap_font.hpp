/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bitmap_font.hpp
 * \brief A class representing a bitmap font.
 * \author Sebastien Angibaud
 */
#ifndef __VISUAL_BITMAP_FONT_HPP__
#define __VISUAL_BITMAP_FONT_HPP__

#include "charset/def.hpp"
#include "visual/sprite.hpp"

#include "visual/class_export.hpp"

#include <map>
#include <vector>

namespace bear
{
  namespace visual
  {
    class scene_writing;
    class screen;

    /**
     * \brief A class representing a bitmap font.
     * \author Sebastien Angibaud
     */
    class VISUAL_EXPORT bitmap_font
    {
    public:
      /** \brief The characters in the font. */
      struct symbol_table
      {
      public:
        /** \brief The position of a character. */
        struct char_position
        {
        public:
          /** \brief The index of the image. */
          unsigned int image_index;

          /** \brief The position of the character in the image. */
          claw::math::coordinate_2d<unsigned int> position;

        }; // struct char_position

      public:
        /** \brief The size of the characters. */
        claw::math::coordinate_2d<unsigned int> size;

        /** \brief The images in which we take the images of the characters. */
        std::vector<visual::image> font_images;

        /** \brief The position of the characters in the image. */
        std::map<charset::char_type, char_position> characters;

      }; // struct symbol_table

    public:
      bitmap_font( const symbol_table& characters );

      size_type get_ex() const;
      size_type get_em() const;
      size_box_type get_glyph_size(charset::char_type c) const;
      size_type get_max_glyph_height() const;

      void render_text( visual::screen& screen,
                        const claw::math::coordinate_2d<unsigned int>& pos,
                        const std::string& str ) const;

      const sprite& get_sprite(charset::char_type character) const;

    private:
      void make_sprites( const symbol_table& characters );
      void make_missing( const symbol_table& characters );

    private:
      /** \brief The characters in the font. */
      std::map<charset::char_type, sprite> m_characters;

      /** \brief Sprite returned for a missing character. */
      sprite m_missing;

    }; // class bitmap_font
  } // namespace visual
} // namespace bear

#endif // __VISUAL_BITMAP_FONT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file image_manager.hpp
 * \brief A class to manage images resources.
 * \author Julien Jorge
 */
#ifndef __VISUAL_IMAGE_MANAGER_HPP__
#define __VISUAL_IMAGE_MANAGER_HPP__

#include "visual/image.hpp"
#include <iostream>
#include <map>
#include <string>

#include "visual/class_export.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief A class to manage images resources.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT image_manager
    {
    public:
      void clear();
      void load_image( const std::string& name, std::istream& file );

      void clear_images();
      void restore_image( const std::string& name, std::istream& file );

      const image& get_image( const std::string& name ) const;
      void get_image_names( std::vector<std::string>& names ) const;

      bool exists( const std::string& name ) const;

    private:
      /** \brief All images. */
      std::map<std::string, image> m_images;

    }; // class image_manager

  } // namespace visual
} // namespace bear

#endif // __VISUAL_IMAGE_MANAGER_HPP__

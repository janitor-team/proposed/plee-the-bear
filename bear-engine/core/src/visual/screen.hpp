/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file screen.hpp
 * \brief The screen surface, where drawing & blitting is done.
 * \author Julien Jorge
 */
#ifndef __VISUAL_SCREEN_HPP__
#define __VISUAL_SCREEN_HPP__

#include "visual/scene_element.hpp"

#include "visual/class_export.hpp"

#include <claw/image.hpp>

namespace bear
{
  namespace visual
  {
    /**
     * \brief The screen surface, where drawing & blitting is done.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT screen
    {
    public:
      /** \brief The subsystem selected for rendering. */
      enum sub_system
        {
          screen_gl,
          screen_undef
        }; // enum_sub_system

    private:
      /** \brief A list of elements of the scene. */
      typedef std::list<scene_element> scene_element_list;

      /** \brief A list of rectangles. */
      typedef std::list<rectangle_type> rectangle_list;

      /** \brief Defined the current screen process. */
      enum screen_status
        {
          SCREEN_IDLE,
          SCREEN_RENDER
        }; // enum screen_status

    public:
      static void initialize( sub_system sub_sys );
      static void release();
      static sub_system get_sub_system();

      screen( const claw::math::coordinate_2d<unsigned int>& size,
              const std::string& title="", bool full=false );
      ~screen();

      void resize_view(unsigned int width, unsigned int height);
      void fullscreen( bool b );
      claw::math::coordinate_2d<unsigned int> get_size() const;
      bool need_restoration() const;
      void set_restored();

      void set_background_color( const color_type& c );
      color_type get_background_color() const;

      void begin_render();
      void render( const scene_element& e );
      bool end_render();

      void shot( const std::string& bitmap_name ) const;
      void shot( claw::graphic::image& img ) const;

    private:
      void render_elements();

      bool intersects_any
      ( const rectangle_type& r, const rectangle_list& boxes ) const;

      void split
      ( const scene_element& e, scene_element_list& output,
        rectangle_list& boxes ) const;

      void subtract
      ( const rectangle_type& a, const rectangle_type& b,
        rectangle_list& result ) const;

    private:
      /** \brief True if we are rendering. */
      screen_status m_mode;

      /** \brief The width and height of the screen. */
      base_screen* m_impl;

      /** \brief The elements to render. */
      scene_element_list m_scene_element;

      /** \brief The subsystem used for rendering. */
      static sub_system s_sub_system;

    }; // class screen
  } // namespace visual
} // namespace bear

#endif // __VISUAL_SCREEN_HPP__

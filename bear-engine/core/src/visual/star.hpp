/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file star.hpp
 * \brief A star.
 * \author Julien Jorge
 */
#ifndef __VISUAL_STAR_HPP__
#define __VISUAL_STAR_HPP__

#include "visual/types.hpp"

#include "visual/class_export.hpp"
#include <vector>

namespace bear
{
  namespace visual
  {
    /**
     * \brief A star.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT star
    {
    public:
      star( std::size_t branches, double inside_ratio );

      double get_ratio() const;
      void set_ratio( double r );

      std::size_t get_branches() const;
      void set_branches( std::size_t b );

      const std::vector<position_type>& get_coordinates() const;

    private:
      void compute_coordinates( std::size_t branches, double inside_ratio );

    private:
      /** \brief The coordinates of the branches. */
      std::vector<position_type> m_coordinates;

    }; // class star
  } // namespace visual
} // namespace bear

#endif // __VISUAL_STAR_HPP__

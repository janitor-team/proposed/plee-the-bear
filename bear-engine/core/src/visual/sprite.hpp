/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sprite.hpp
 * \brief A class to represent sprites.
 * \author Julien Jorge
 */
#ifndef __VISUAL_SPRITE_HPP__
#define __VISUAL_SPRITE_HPP__

#include "visual/bitmap_rendering_attributes.hpp"
#include "visual/image.hpp"

#include <claw/rectangle.hpp>

#include "visual/class_export.hpp"

namespace bear
{
  namespace visual
  {
    /**
     * \brief A class to represent sprites.
     * \author Julien Jorge
     */
    class VISUAL_EXPORT sprite:
      public bitmap_rendering_attributes
    {
    public:
      sprite();
      sprite( const image& img,
              const claw::math::rectangle<unsigned int>& clip_rectangle );
      sprite( const image& img );

      bool has_transparency() const;

      const claw::math::rectangle<unsigned int>& clip_rectangle() const;
      void
      set_clip_rectangle( const claw::math::rectangle<unsigned int>& clip );

      const image& get_image() const;

      bool is_valid() const;

    private:
      /** \brief The picture where we take the sprite. */
      image m_image;

      /** \brief The sprite is this part of the image. */
      claw::math::rectangle<unsigned int> m_clip_rectangle;

    }; // class sprite

  } // namespace visual
} // namespace bear

#endif // __VISUAL_SPRITE_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file time.cpp
 * \brief Implementation of the free functions of the namespace bear::systime.
 * \author Julien Jorge.
 */
#include "time/time.hpp"

#ifdef _WIN32
  #include "time/code/time_win32.cpp"
#else // _WIN32
  #include "time/code/time_unix.cpp"
#endif

#include <ctime>
#include <locale>
#include <sstream>

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the unix date, in seconds since January 1, 1970.
 */
bear::systime::seconds_type TIME_EXPORT bear::systime::get_unix_time()
{
  std::time_t t = std::time(NULL);
  return static_cast<seconds_type>( t );
} // get_unix_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get a string representation of a time in seconds according to a given
 *        format.
 * \param t The time.
 * \param format The format.
 * \sa std::time_put.
 */
std::string TIME_EXPORT bear::systime::format_time_s
( unsigned long t, const std::string& format )
{
  std::locale loc;
  const std::time_put<char>& tmput =
    std::use_facet< std::time_put<char> >(loc);
  std::tm date;
  std::ostringstream oss;

  date.tm_sec = t % 60;
  t /= 60.;

  date.tm_min = t % 60;
  t /= 60.;

  date.tm_hour = t;

  tmput.put
    ( oss, oss, ' ', &date, format.c_str(), format.c_str() + format.length() );

  return oss.str();
} // format_time()

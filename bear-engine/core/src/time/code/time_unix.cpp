/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file time_unix.cpp
 * \brief Unix implementation of the functions of the namespace bear::systime.
 * \author Julien Jorge.
 */
#include <unistd.h>
#include <cstddef>
#include <sys/time.h>
#include <time.h>

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop the process for a given amount of time.
 * \param ms Stop the process this number of milliseconds.
 */
void bear::systime::sleep( milliseconds_type ms )
{
  struct timespec time;
  time.tv_sec = ms / 1000;
  time.tv_nsec = ( ms % 1000 ) * ( 1000 * 1000 );
  nanosleep( &time, NULL );
} // sleep()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the current date, in milliseconds.
 */
bear::systime::milliseconds_type bear::systime::get_date_ms()
{
  timeval tv;

  gettimeofday( &tv, NULL );

  return tv.tv_sec * 1000 + tv.tv_usec / 1000;
} // get_date_ms()

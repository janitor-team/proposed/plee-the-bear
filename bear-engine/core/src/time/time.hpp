/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file time.hpp
 * \brief Some functions and types related to time management.
 * \author Julien Jorge.
 */
#ifndef __TIME_TIME_HPP__
#define __TIME_TIME_HPP__

#include "time/class_export.hpp"

#include <string>

namespace bear
{
  namespace systime
  {
    typedef unsigned long milliseconds_type;
    typedef unsigned long seconds_type;

    void TIME_EXPORT sleep( milliseconds_type ms );
    milliseconds_type TIME_EXPORT get_date_ms();

    seconds_type TIME_EXPORT get_unix_time();

    std::string TIME_EXPORT format_time_s
    ( unsigned long t, const std::string& format );

  } // namespace systime
} // namespace bear

#endif // __TIME_TIME_HPP__

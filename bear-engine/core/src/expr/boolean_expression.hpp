/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file boolean_expression.hpp
 * \brief A boolean expression.
 * \author Julien Jorge
 */
#ifndef __EXPR_BOOLEAN_EXPRESSION_HPP__
#define __EXPR_BOOLEAN_EXPRESSION_HPP__

#include "expr/class_export.hpp"

namespace bear
{
  namespace expr
  {
    class base_boolean_expression;

    /**
     * \brief A boolean expression.
     * \author Julien Jorge
     */
    class EXPR_EXPORT boolean_expression
    {
    public:
      /** \brief The base class for boolean expressions. */
      typedef base_boolean_expression base_expression_type;

    public:
      boolean_expression();
      boolean_expression( const base_boolean_expression& e );
      boolean_expression( const boolean_expression& that );
      ~boolean_expression();

      bool evaluate() const;
      operator bool() const;

      boolean_expression& operator=( const boolean_expression& that );

      boolean_expression operator==( const boolean_expression& that ) const;
      boolean_expression operator!=( const boolean_expression& that ) const;
      boolean_expression operator&&( const boolean_expression& that ) const;
      boolean_expression operator||( const boolean_expression& that ) const;
      boolean_expression operator^( const boolean_expression& that ) const;

    private:
      /** \brief The implemented expression. */
      base_boolean_expression* m_expr;

    }; // class boolean_expression

  } // namespace expr
} // namespace bear

EXPR_EXPORT bear::expr::boolean_expression
operator!( const bear::expr::boolean_expression& that );

#endif // __EXPR_BOOLEAN_EXPRESSION_HPP__

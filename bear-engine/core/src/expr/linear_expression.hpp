/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file linear_expression.hpp
 * \brief A linear expression.
 * \author Julien Jorge
 */
#ifndef __EXPR_LINEAR_EXPRESSION_HPP__
#define __EXPR_LINEAR_EXPRESSION_HPP__

#include "expr/boolean_expression.hpp"

namespace bear
{
  namespace expr
  {
    class base_linear_expression;

    /**
     * \brief A linear expression.
     * \author Julien Jorge
     */
    class EXPR_EXPORT linear_expression
    {
    public:
      /** \brief The base class for linear expressions. */
      typedef base_linear_expression base_expression_type;

    public:
      linear_expression( double v = 0 );
      linear_expression( const base_linear_expression& e );
      linear_expression( const linear_expression& that );
      ~linear_expression();

      double evaluate() const;

      linear_expression& operator=( const linear_expression& that );

      boolean_expression operator==( const linear_expression& that ) const;
      boolean_expression operator!=( const linear_expression& that ) const;

      boolean_expression operator<( const linear_expression& that ) const;
      boolean_expression operator<=( const linear_expression& that ) const;
      boolean_expression operator>( const linear_expression& that ) const;
      boolean_expression operator>=( const linear_expression& that ) const;

      linear_expression& operator+=( const linear_expression& that );
      linear_expression& operator-=( const linear_expression& that );
      linear_expression& operator*=( const linear_expression& that );
      linear_expression& operator/=( const linear_expression& that );

      linear_expression operator+( const linear_expression& that ) const;
      linear_expression operator-( const linear_expression& that ) const;
      linear_expression operator*( const linear_expression& that ) const;
      linear_expression operator/( const linear_expression& that ) const;

    private:
      /** \brief The implemented expression. */
      base_linear_expression* m_expr;

    }; // class linear_expression

  } // namespace expr
} // namespace bear

EXPR_EXPORT bear::expr::boolean_expression
operator==
( const bear::expr::base_linear_expression& a,
  const bear::expr::base_linear_expression& b );
EXPR_EXPORT bear::expr::boolean_expression
operator!=
( const bear::expr::base_linear_expression& a,
  const bear::expr::base_linear_expression& b );

EXPR_EXPORT bear::expr::boolean_expression
operator==( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::boolean_expression
operator!=( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::boolean_expression
operator<( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::boolean_expression
operator<=( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::boolean_expression
operator>( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::boolean_expression
operator>=( double v, const bear::expr::base_linear_expression& e );

EXPR_EXPORT bear::expr::linear_expression
operator+( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::linear_expression
operator-( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::linear_expression
operator*( double v, const bear::expr::base_linear_expression& e );
EXPR_EXPORT bear::expr::linear_expression
operator/( double v, const bear::expr::base_linear_expression& e );

EXPR_EXPORT bear::expr::boolean_expression
operator==( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::boolean_expression
operator!=( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::boolean_expression
operator<( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::boolean_expression
operator<=( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::boolean_expression
operator>( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::boolean_expression
operator>=( const bear::expr::base_linear_expression& e, double v );

EXPR_EXPORT bear::expr::linear_expression
operator+( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::linear_expression
operator-( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::linear_expression
operator*( const bear::expr::base_linear_expression& e, double v );
EXPR_EXPORT bear::expr::linear_expression
operator/( const bear::expr::base_linear_expression& e, double v );

#endif // __EXPR_LINEAR_EXPRESSION_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file binary_expression.hpp
 * \brief An operation on two expressions.
 * \author Julien Jorge
 */
#ifndef __EXPR_BINARY_EXPRESSION_HPP__
#define __EXPR_BINARY_EXPRESSION_HPP__

namespace bear
{
  namespace expr
  {
    /**
     * \brief A linear expression with two operands.
     * \author Julien Jorge
     */
    template<typename Base, typename Operand, typename Function>
    class binary_expression:
      public Base
    {
    public:
      /** \brief The type of the result. */
      typedef typename Base::result_type result_type;

      /** \brief The type of the operands. */
      typedef Operand operand_type;

      /** \brief The type of this class. */
      typedef binary_expression<Base, Operand, Function> self_type;

    public:
      binary_expression();
      binary_expression(const self_type& that);
      binary_expression
      ( const operand_type& left, const operand_type& right );

      const operand_type& get_left_operand() const;
      void set_left_operand( const operand_type& op );

      const operand_type& get_right_operand() const;
      void set_right_operand( const operand_type& op );

      Base* clone() const;
      result_type evaluate() const;

    private:
      /** \brief The left operand. */
      operand_type m_left;

      /** \brief The right operand. */
      operand_type m_right;

    }; // class binary_expression

  } // namespace expr
} // namespace bear

#include "expr/impl/binary_expression.tpp"

#endif // __EXPR_BINARY_EXPRESSION_HPP__

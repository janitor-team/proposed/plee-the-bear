/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file linear_function.tpp
 * \brief Implementation of the bear::expr::linear_function class.
 * \author Julien Jorge.
 */

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param inst The instance on which we call the function.
 * \param m The function to call with \a *inst as the only parameter.
 */
template<typename FunctionType, typename PointerType>
bear::expr::linear_function<FunctionType, PointerType>::linear_function
( const PointerType& inst, FunctionType m )
  : m_value(inst), m_function(m)
{

} // linear_function::linear_function()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
template<typename FunctionType, typename PointerType>
bear::expr::base_linear_expression*
bear::expr::linear_function<FunctionType, PointerType>::clone() const
{
  return new linear_function<FunctionType, PointerType>(*this);
} // linear_function::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
template<typename FunctionType, typename PointerType>
double
bear::expr::linear_function<FunctionType, PointerType>::evaluate() const
{
  if ( m_value == static_cast<PointerType>(NULL) )
    return std::numeric_limits<double>::quiet_NaN();
  else
    return (double)m_function(*m_value);
} // linear_function::evaluate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Construct a linear_function.
 * \param inst The instance on which we call the function.
 * \param m The function to call with \a *inst as the only parameter.
 *
 * This function is an easier way to create a linear function than using the
 * constructor. Compare those two solutions, both creating a linear function
 * that calls a member function of bear::expr::base_item:
 *
 * \code
 * typedef std::const_mem_fun_ref_t<double, base_item> function_type;
 * linear_expression f
 *   ( linear_function<base_item*, function_type>
 *     ( item, function_type(&base_item::get_left) ) );
 * \endcode
 * and
 * \code
 * linear_expression f
 *   ( linear_expression_maker
 *     ( item, std::mem_fun_ref(&base_item::get_left) ) );
 * \endcode
 */
template<typename FunctionType, typename PointerType>
bear::expr::linear_function<FunctionType, PointerType>
bear::expr::linear_function_maker(const PointerType& inst, FunctionType m)
{
  return linear_function<FunctionType, PointerType>(inst, m);
} // linear_function_maker()

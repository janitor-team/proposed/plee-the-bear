/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file logical_not.cpp
 * \brief Implementation of the bear::expr::logical_not class.
 * \author Julien Jorge.
 */
#include "expr/logical_not.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::expr::logical_not::logical_not()
{

} // logical_not::logical_not()

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 * \param op The operand.
 */
bear::expr::logical_not::logical_not( const boolean_expression& op )
  : m_operand(op)
{

} // logical_not::logical_not()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the operand.
 */
const bear::expr::boolean_expression&
bear::expr::logical_not::get_operand() const
{
  return m_operand;
} // logical_not::get_operand()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the operand.
 * \param op The new operand.
 */
void bear::expr::logical_not::set_operand( const boolean_expression& op )
{
  m_operand = op;
} // logical_not::set_operand()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::expr::base_boolean_expression* bear::expr::logical_not::clone() const
{
  return new logical_not(*this);
} // logical_not::logical_not()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::expr::logical_not::evaluate() const
{
  return !m_operand.evaluate();
} // logical_not::evaluate()

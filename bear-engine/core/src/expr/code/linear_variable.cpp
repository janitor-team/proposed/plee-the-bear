/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file linear_variable.cpp
 * \brief Implementation of the bear::expr::linear_variable class.
 * \author Julien Jorge.
 */
#include "expr/linear_variable.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param v The variable from which we take the value.
 * \remark \a v must live longer than \a this.
 */
bear::expr::linear_variable::linear_variable( const double& v )
  : m_value(v)
{

} // linear_variable::linear_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::expr::base_linear_expression*
bear::expr::linear_variable::clone() const
{
  return new linear_variable(*this);
} // linear_variable::linear_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
double bear::expr::linear_variable::evaluate() const
{
  return m_value;
} // linear_variable::evaluate()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file boolean_expression.cpp
 * \brief Implementation of the bear::expr::boolean_expression class.
 * \author Julien Jorge.
 */
#include "expr/boolean_expression.hpp"

#include "expr/binary_boolean_expression.hpp"
#include "expr/boolean_constant.hpp"
#include "expr/logical_not.hpp"
#include "expr/logical_xor.hpp"

#include <algorithm>

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 */
bear::expr::boolean_expression::boolean_expression()
  : m_expr( new boolean_constant(false) )
{

} // boolean_expression::boolean_expression()

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 * \param e An expression to init from.
 */
bear::expr::boolean_expression::boolean_expression
( const base_boolean_expression& e )
  : m_expr(e.clone())
{

} // boolean_expression::boolean_expression()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy contructor.
 * \param that The instance to copy from.
 */
bear::expr::boolean_expression::boolean_expression
( const boolean_expression& that )
  : m_expr( that.m_expr->clone() )
{

} // boolean_expression::boolean_expression()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::expr::boolean_expression::~boolean_expression()
{
  delete m_expr;
} // boolean_expression::~boolean_expression()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::expr::boolean_expression::evaluate() const
{
  return m_expr->evaluate();
} // boolean_expression::evaluate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bear::expr::boolean_expression::operator bool() const
{
  return evaluate();
} // boolean_expression::operator bool()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assignment.
 * \param that The instance to copy from.
 */
bear::expr::boolean_expression&
bear::expr::boolean_expression::operator=( const boolean_expression& that )
{
  boolean_expression tmp(that);

  std::swap(tmp.m_expr, m_expr);

  return *this;
} // boolean_expression::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Equality.
 * \param that The expression to compare to.
 */
bear::expr::boolean_expression bear::expr::boolean_expression::operator==
( const boolean_expression& that ) const
{
  return boolean_equality( *this, that );
} // boolean_expression::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Disequality.
 * \param that The expression to compare to.
 */
bear::expr::boolean_expression bear::expr::boolean_expression::operator!=
( const boolean_expression& that ) const
{
  return boolean_disequality( *this, that );
} // boolean_expression::operator!=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Logical and.
 * \param that The expression to compare to.
 */
bear::expr::boolean_expression bear::expr::boolean_expression::operator&&
( const boolean_expression& that ) const
{
  return logical_and( *this, that );
} // boolean_expression::operator&&()

/*----------------------------------------------------------------------------*/
/**
 * \brief Logical or.
 * \param that The expression to compare to.
 */
bear::expr::boolean_expression bear::expr::boolean_expression::operator||
( const boolean_expression& that ) const
{
  return logical_or( *this, that );
} // boolean_expression::operator||()

/*----------------------------------------------------------------------------*/
/**
 * \brief Logical xor.
 * \param that The expression to compare to.
 */
bear::expr::boolean_expression bear::expr::boolean_expression::operator^
( const boolean_expression& that ) const
{
  return logical_xor( *this, that );
} // boolean_expression::operator^()

/*----------------------------------------------------------------------------*/
/**
 * \brief Logical not.
 * \param e The expression to negate.
 */
bear::expr::boolean_expression
operator!( const bear::expr::boolean_expression& e )
{
  return bear::expr::logical_not(e);
} // operator!()

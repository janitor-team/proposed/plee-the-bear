/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file boolean_variable.cpp
 * \brief Implementation of the bear::expr::boolean_variable class.
 * \author Julien Jorge.
 */
#include "expr/boolean_variable.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param v The variable from which we take the value.
 * \remark \a v must live longer than \a this.
 */
bear::expr::boolean_variable::boolean_variable( const bool& v )
  : m_value(v)
{

} // boolean_variable::boolean_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::expr::base_boolean_expression*
bear::expr::boolean_variable::clone() const
{
  return new boolean_variable(*this);
} // boolean_variable::boolean_variable()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::expr::boolean_variable::evaluate() const
{
  return m_value;
} // boolean_variable::evaluate()

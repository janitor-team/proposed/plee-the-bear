/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file boolean_constant.cpp
 * \brief Implementation of the bear::expr::boolean_constant class.
 * \author Julien Jorge.
 */
#include "expr/boolean_constant.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param v The constant from which we take the value.
 */
bear::expr::boolean_constant::boolean_constant( bool v )
  : m_value(v)
{

} // boolean_constant::boolean_constant()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::expr::base_boolean_expression*
bear::expr::boolean_constant::clone() const
{
  return new boolean_constant(*this);
} // boolean_constant::boolean_constant()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::expr::boolean_constant::evaluate() const
{
  return m_value;
} // boolean_constant::evaluate()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of the constant.
 * \param b The new value.
 */
void bear::expr::boolean_constant::set_value( bool b )
{
  m_value = b;
} // boolean_constant::set_value()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file logical_xor.cpp
 * \brief Implementation of the bear::expr::logical_xor class.
 * \author Julien Jorge.
 */
#include "expr/logical_xor.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Contructor.
 * \param left The left operand.
 * \param right The right operand.
 */
bear::expr::logical_xor::logical_xor
( const boolean_expression& left, const boolean_expression& right )
  : m_left(left), m_right(right)
{

} // logical_xor::logical_xor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this expression.
 */
bear::expr::base_boolean_expression* bear::expr::logical_xor::clone() const
{
  return new logical_xor(*this);
} // logical_xor::logical_xor()

/*----------------------------------------------------------------------------*/
/**
 * \brief Evaluate the expression.
 */
bool bear::expr::logical_xor::evaluate() const
{
  return m_left.evaluate() ^ m_right.evaluate();
} // logical_xor::evaluate()

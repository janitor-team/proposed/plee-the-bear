/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file binary_boolean_expression.hpp
 * \brief A logical and of two boolean expressions.
 * \author Julien Jorge
 */
#ifndef __EXPR_BINARY_BOOLEAN_EXPRESSION_HPP__
#define __EXPR_BINARY_BOOLEAN_EXPRESSION_HPP__

#include "expr/base_boolean_expression.hpp"
#include "expr/binary_expression.hpp"
#include "expr/boolean_expression.hpp"

#include <functional>

namespace bear
{
  namespace expr
  {
    /**
     * \brief Utility class to create a binary_expression whose operands are
     *        boolean_expressions.
     * \author Julien Jorge
     */
    template<typename Function>
    class logical_expression_maker
    {
    public:
      typedef
      binary_expression<base_boolean_expression, boolean_expression, Function>
      type;
    }; // class logical_expression_maker

    /** Tell if two booleans are equal. */
    typedef
    logical_expression_maker< std::equal_to<bool> >::type boolean_equality;

    /** Tell if two booleans are not equal. */
    typedef
    logical_expression_maker< std::not_equal_to<bool> >::type
    boolean_disequality;

    /** Compute the logical and value of the evaluation of two boolean
        expressions. */
    typedef
    logical_expression_maker< std::logical_and<bool> >::type logical_and;

    /** Compute the logical or value of the evaluation of two boolean
        expressions. */
    typedef logical_expression_maker< std::logical_or<bool> >::type logical_or;

  } // namespace expr
} // namespace bear

#endif // __EXPR_BINARY_BOOLEAN_EXPRESSION_HPP__

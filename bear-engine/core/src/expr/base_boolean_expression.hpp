/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_boolean_expression.hpp
 * \brief The base class for a boolean expression.
 * \author Julien Jorge
 */
#ifndef __EXPR_BASE_BOOLEAN_EXPRESSION_HPP__
#define __EXPR_BASE_BOOLEAN_EXPRESSION_HPP__

#include "expr/class_export.hpp"

namespace bear
{
  namespace expr
  {
    /**
     * \brief The base class for a boolean expression.
     * \author Julien Jorge
     */
    class EXPR_EXPORT base_boolean_expression
    {
    public:
      /** \brief The type of the result of the evaluation. */
      typedef bool result_type;

    public:
      virtual ~base_boolean_expression() {};

      virtual base_boolean_expression* clone() const = 0;
      virtual result_type evaluate() const = 0;

    }; // class base_boolean_expression

  } // namespace expr
} // namespace bear

#endif // __EXPR_BASE_BOOLEAN_EXPRESSION_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file linear_variable.hpp
 * \brief A linear expression made of a linear variable.
 * \author Julien Jorge
 */
#ifndef __EXPR_LINEAR_VARIABLE_HPP__
#define __EXPR_LINEAR_VARIABLE_HPP__

#include "expr/base_linear_expression.hpp"

namespace bear
{
  namespace expr
  {
    /**
     * \brief A linear expression made of a linear variable.
     * \author Julien Jorge
     */
    class EXPR_EXPORT linear_variable:
      public base_linear_expression
    {
    public:
      linear_variable( const double& v );

      base_linear_expression* clone() const;
      double evaluate() const;

    private:
      /** \brief The value of the variable. */
      const double& m_value;

    }; // class linear_variable

  } // namespace expr
} // namespace bear

#endif // __EXPR_LINEAR_VARIABLE_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file linear_constant.hpp
 * \brief A linear expression made of a single constant.
 * \author Julien Jorge
 */
#ifndef __EXPR_LINEAR_CONSTANT_HPP__
#define __EXPR_LINEAR_CONSTANT_HPP__

#include "expr/base_linear_expression.hpp"

namespace bear
{
  namespace expr
  {
    /**
     * \brief A linear expression made of a single constant.
     * \author Julien Jorge
     */
    class EXPR_EXPORT linear_constant:
      public base_linear_expression
    {
    public:
      linear_constant( double v );

      base_linear_expression* clone() const;
      double evaluate() const;

    private:
      /** \brief The value of the constant. */
      const double m_value;

    }; // class linear_constant

  } // namespace expr
} // namespace bear

#endif // __EXPR_LINEAR_CONSTANT_HPP__

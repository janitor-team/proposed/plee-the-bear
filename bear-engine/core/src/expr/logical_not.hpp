/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file logical_not.hpp
 * \brief A logical not of a boolean expression.
 * \author Julien Jorge
 */
#ifndef __EXPR_LOGICAL_NOT_HPP__
#define __EXPR_LOGICAL_NOT_HPP__

#include "expr/base_boolean_expression.hpp"
#include "expr/boolean_expression.hpp"

namespace bear
{
  namespace expr
  {
    /**
     * \brief A logical not of a boolean expression.
     * \author Julien Jorge
     */
    class EXPR_EXPORT logical_not:
      public base_boolean_expression
    {
    public:
      logical_not();
      logical_not( const boolean_expression& op );

      const boolean_expression& get_operand() const;
      void set_operand( const boolean_expression& op );

      base_boolean_expression* clone() const;
      bool evaluate() const;

    private:
      /** \brief The operand. */
      boolean_expression m_operand;

    }; // class logical_not

  } // namespace expr
} // namespace bear

#endif // __EXPR_LOGICAL_NOT_HPP__

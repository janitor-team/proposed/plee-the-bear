/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file linear_function.hpp
 * \brief An expression returning the result of a call of a function on any
 *        class instance.
 * \author Julien Jorge
 */
#ifndef __EXPR_LINEAR_FUNCTION_HPP__
#define __EXPR_LINEAR_FUNCTION_HPP__

#include "expr/base_linear_expression.hpp"

namespace bear
{
  namespace expr
  {
    /**
     * \brief An expression returning the result of a call of a function on any
     *        class instance.
     *
     * The evaluation of this expression is m(*inst), where \a m is the function
     * passed to the constructor, and \a inst the pointer passed to the
     * constructor.
     *
     * \author Julien Jorge
     */
    template<typename FunctionType, typename PointerType>
    class linear_function:
      public base_linear_expression
    {
    public:
      linear_function( const PointerType& inst, FunctionType m );

      base_linear_expression* clone() const;
      double evaluate() const;

    private:
      /** \brief The class on which we call the function. */
      PointerType m_value;

      /** \brief The member function to call. */
      FunctionType m_function;

    }; // class linear_function

    template<typename FunctionType, typename PointerType>
    linear_function<FunctionType, PointerType>
    linear_function_maker( const PointerType& inst, FunctionType m );

  } // namespace expr
} // namespace bear

#include "expr/impl/linear_function.tpp"

#endif // __EXPR_LINEAR_FUNCTION_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file boolean_constant.hpp
 * \brief A boolean expression made of a boolean constant.
 * \author Julien Jorge
 */
#ifndef __EXPR_BOOLEAN_CONSTANT_HPP__
#define __EXPR_BOOLEAN_CONSTANT_HPP__

#include "expr/base_boolean_expression.hpp"

namespace bear
{
  namespace expr
  {
    /**
     * \brief A boolean expression made of a boolean constant.
     * \author Julien Jorge
     */
    class EXPR_EXPORT boolean_constant:
      public base_boolean_expression
    {
    public:
      boolean_constant( bool v );

      base_boolean_expression* clone() const;
      bool evaluate() const;

      void set_value( bool b );

    private:
      /** \brief The value of the constant. */
      bool m_value;

    }; // class boolean_constant

  } // namespace expr
} // namespace bear

#endif // __EXPR_BOOLEAN_CONSTANT_HPP__

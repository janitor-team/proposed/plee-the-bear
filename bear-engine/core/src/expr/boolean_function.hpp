/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file boolean_function.hpp
 * \brief An expression returning the result of a call of a function on any
 *        class instance.
 * \author Julien Jorge
 */
#ifndef __EXPR_BOOLEAN_FUNCTION_HPP__
#define __EXPR_BOOLEAN_FUNCTION_HPP__

#include "expr/base_boolean_expression.hpp"

namespace bear
{
  namespace expr
  {
    /**
     * \brief An expression returning the result of a call of a function on any
     *        class instance.
     *
     * The evaluation of this expression is m(*inst), where \a m is the function
     * passed to the constructor, and \a inst the pointer passed to the
     * constructor.
     *
     * \author Julien Jorge
     */
    template<typename FunctionType, typename PointerType>
    class boolean_function:
      public base_boolean_expression
    {
    public:
      boolean_function();
      boolean_function
      ( const boolean_function<FunctionType, PointerType>& that);
      boolean_function( const PointerType& inst, FunctionType m );

      base_boolean_expression* clone() const;
      bool evaluate() const;

      void set_function( FunctionType m );

    private:
      /** \brief The class on which we call the function. */
      const PointerType m_value;

      /** \brief The member function to call. */
      FunctionType m_function;

    }; // class boolean_function

    /**
     * \brief An expression returning the result of a call of a function.
     *
     * The evaluation of this expression is m(), where \a m is the function
     * passed to the constructor.
     *
     * \author Julien Jorge
     */
    template<typename FunctionType>
    class boolean_function<FunctionType, void>:
      public base_boolean_expression
    {
    public:
      boolean_function();
      boolean_function( FunctionType m );

      base_boolean_expression* clone() const;
      bool evaluate() const;

      void set_function( FunctionType m );

    private:
      /** \brief The member function to call. */
      FunctionType m_function;

    }; // class boolean_function

    template<typename FunctionType, typename PointerType>
    boolean_function<FunctionType, PointerType>
    boolean_function_maker( const PointerType& inst, FunctionType m);

    template<typename FunctionType>
    boolean_function<FunctionType, void>
    boolean_function_maker( FunctionType m);

  } // namespace expr
} // namespace bear

#include "expr/impl/boolean_function.tpp"

#endif // __EXPR_BOOLEAN_FUNCTION_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file messageable.hpp
 * \brief A class for items that can receaive message events.
 * \author Julien Jorge
 */
#ifndef __COMMUNICATION_MESSAGEABLE_HPP__
#define __COMMUNICATION_MESSAGEABLE_HPP__

#include "communication/message.hpp"
#include "concept/item_container.hpp"

#include "communication/class_export.hpp"

#include <string>

namespace bear
{
  namespace communication
  {
    /**
     * \brief A class for items that can receaive message events.
     * \author Julien Jorge
     */
    class COMMUNICATION_EXPORT messageable:
      private concept::item_container<message*>
    {
    public:
      /** \brief The type of the parent class. */
      typedef concept::item_container<message*> super;

    public:
      messageable();
      messageable( const std::string& name );
      messageable( const messageable& that );
      virtual ~messageable();

      void set_name(const std::string& name);
      const std::string& get_name() const;

      void post_message( message& msg );
      bool send_message( message& msg );
      void process_messages();

    private:
      virtual bool process_message( message& msg );

      virtual void add( message* const& who);
      virtual void remove( message* const& who);

    private:
      /** \brief The (unique) name of the item. */
      std::string m_name;

      /** \brief Message queue. */
      std::list<message*> m_message_queue;

    }; // class messageable
  } // namespace communication
} // namespace bear

#endif // __COMMUNICATION_MESSAGEABLE_HPP__

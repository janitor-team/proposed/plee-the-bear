/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file typed_message.cpp
 * \brief Implementation of the bear::communication::typed_message class.
 * \author Julien Jorge
 */
#include <cstdlib>

/*----------------------------------------------------------------------------*/
/**
 * \brief Apply the message to something.
 * \param that The thing to apply the message to.
 */
template<typename T>
bool bear::communication::typed_message<T>::apply_to(messageable& that)
{
  T* p = (T*)&that;

  if (p != NULL)
    return apply_to(*p);
  else
    return false;
} // typed_message::apply_to()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file post_office.hpp
 * \brief A class to transfer message to items.
 * \author Julien Jorge
 */
#ifndef __COMMUNICATION_POST_OFFICE_HPP__
#define __COMMUNICATION_POST_OFFICE_HPP__

#include "communication/messageable.hpp"
#include "concept/item_container.hpp"

#include <map>
#include <string>

namespace bear
{
  namespace communication
  {
    class message;

    /**
     * \brief A class to transfer message to items.
     * \author Julien Jorge
     */
    class COMMUNICATION_EXPORT post_office:
      public concept::item_container<messageable*>
    {
    public:
      bool send_message( const std::string& target, message& msg ) const;
      void process_messages();

      bool exists( const std::string& name ) const;

      void clear();

    protected:
      void add( messageable* const& who );
      void remove( messageable* const& who );

    public:
      /** \brief The name of items that do not have a name... */
      static const std::string no_name;

    private:
      std::map<std::string, messageable*> m_items;

    }; // class post_office

  } // namespace communication
} // namespace bear

#endif // __COMMUNICATION_POST_OFFICE_HPP__

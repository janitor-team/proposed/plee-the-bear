/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_picking_filter.hpp
 * \brief A filter passed to bear::universe::world::pick_items to get only
 *        interesting items.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_ITEM_PICKING_FILTER_HPP__
#define __UNIVERSE_ITEM_PICKING_FILTER_HPP__

#include "universe/class_export.hpp"
#include "universe/types.hpp"

namespace bear
{
  namespace universe
  {
    class physical_item;

    /**
     * \brief A filter passed to bear::universe::world::pick_items to get only
     *        interesting items.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT item_picking_filter
    {
    public:
      item_picking_filter();
      virtual ~item_picking_filter();

      bool satisfies_condition( const physical_item& item ) const;

      void set_artificial_value( bool v );
      void set_phantom_value( bool v );
      void set_can_move_items_value( bool v );
      void set_fixed_value( bool v );
      void set_forbidden_position( const position_type& pos );

    protected:
      virtual bool do_satisfies_condition( const physical_item& item ) const;

    private:
      /** \brief Tell if the "artificial" flag must be checked. */
      bool m_check_artificial;

      /** \brief The value of the "artificial" flag, if checked. */
      bool m_artificial_value;

      /** \brief Tell if the "phantom" flag must be checked. */
      bool m_check_phantom;

      /** \brief The value of the "phantom" flag, if checked. */
      bool m_phantom_value;

      /** \brief Tell if the "can_move_items" flag must be checked. */
      bool m_check_can_move_items;

      /** \brief The value of the "can_move_items" flag, if checked. */
      bool m_can_move_items_value;

      /** \brief Tell if the "fixed" flag must be checked. */
      bool m_check_fixed;

      /** \brief The value of the "fixed" flag, if checked. */
      bool m_fixed_value;

      /** \brief Tell if we check that the item is not at the forbidden
          position. */
      bool m_check_forbidden_position;

      /** \brief A position where the item must not be. */
      position_type m_forbidden_position;

    }; // class item_picking_filter

  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_ITEM_PICKING_FILTER_HPP__

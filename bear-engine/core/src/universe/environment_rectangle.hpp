/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file universe/environment_rectangle.hpp
 * \brief A rectangle in the world, where the environment is different from the
 *        default environment.
 * \author Sébastien Angibaud
 */
#ifndef __UNIVERSE_ENVIRONMENT_RECTANGLE_HPP__
#define __UNIVERSE_ENVIRONMENT_RECTANGLE_HPP__

#include "universe/types.hpp"
#include "universe/class_export.hpp"
#include "universe/environment_type.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief A rectangle in the world, where the environment is different
     * from the default environment.
     * \author Sebastien Angibaud.
     */
    class UNIVERSE_EXPORT environment_rectangle
    {
    public:
      environment_rectangle();
      environment_rectangle
      ( const rectangle_type& r, const universe::environment_type e );

    public:
      /** \brief The rectangle where the environment is different. */
      rectangle_type rectangle;

      /** \brief The environment in this rectangle. */
      universe::environment_type environment;

    }; // class environment_rectangle
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_ENVIRONMENT_RECTANGLE_HPP__

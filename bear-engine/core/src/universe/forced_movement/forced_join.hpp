/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_join.hpp
 * \brief A forced movement that will try to bring an item to an other item.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_FORCED_JOIN_HPP__
#define __UNIVERSE_FORCED_JOIN_HPP__

#include "universe/forced_movement/base_forced_movement.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief A forced movement that will try to bring an item to an other item.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT forced_join:
      public base_forced_movement
    {
    public:
      forced_join( time_type length = 0 );

      base_forced_movement* clone() const;

      void set_total_time( time_type length );

      bool is_finished() const;

    private:
      void do_init();
      time_type do_next_position( time_type elapsed_time );

    private:
      /** \brief Total time to reach the target. */
      time_type m_total_time;

      /** \brief Remaining time to reach the target. */
      time_type m_remaining_time;

    }; // class forced_join
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_FORCED_JOIN_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_movement_function.cpp
 * \brief Implementation of the bear::universe::forced_movement_function class.
 * \author Julien Jorge
 */
#include "universe/forced_movement/forced_movement_function.hpp"

#include "universe/physical_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param f The function to call to get the position of the item.
 * \param length The duration of the movement.
 */
bear::universe::forced_movement_function::forced_movement_function
( const function_type& f, time_type length )
  : m_total_time( length ), m_remaining_time(m_total_time), m_function(f)
{

} // forced_movement_function::forced_movement_function()

/*----------------------------------------------------------------------------*/
/**
 * \brief Instanciate a copy of this movement.
 */
bear::universe::base_forced_movement*
bear::universe::forced_movement_function::clone() const
{
  return new forced_movement_function(*this);
} // forced_movement_function::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the duration of the movement.
 * \param length The duration of the movement.
 */
void
bear::universe::forced_movement_function::set_total_time( time_type length )
{
  m_total_time = length;
} // forced_movement_function::set_total_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the movement is finished.
 */
bool bear::universe::forced_movement_function::is_finished() const
{
  return m_remaining_time == 0;
} // forced_movement_function::is_finished()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the movement.
 */
void bear::universe::forced_movement_function::do_init()
{
  m_remaining_time = m_total_time;
} // forced_movement_function::do_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do an iteration of the movement.
 * \param elapsed_time Elapsed time since the last call.
 * \return The remaining time if the movement is finished.
 */
bear::universe::time_type
bear::universe::forced_movement_function::do_next_position
( time_type elapsed_time )
{
  time_type result;

  if ( m_remaining_time < elapsed_time )
    {
      result = elapsed_time - m_remaining_time;
      m_remaining_time = 0;
    }
  else
    {
      m_remaining_time -= elapsed_time;
      result = 0;
    }

  get_item().set_center_of_mass( m_function() );

  return result;
} // forced_movement_function::do_next_position()

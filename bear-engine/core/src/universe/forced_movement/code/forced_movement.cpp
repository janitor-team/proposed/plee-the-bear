/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_movement.cpp
 * \brief Implementation of the forced_movement class.
 * \author Julien Jorge.
 */
#include "universe/forced_movement/forced_movement.hpp"

#include "universe/forced_movement/base_forced_movement.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::forced_movement::forced_movement()
  : m_movement(NULL)
{

} // forced_movement::forced_movement()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 * \param that The instance to copy from.
 */
bear::universe::forced_movement::forced_movement( const forced_movement& that )
{
  if ( that.is_null() )
    m_movement = NULL;
  else
    m_movement = that.m_movement->clone();
} // forced_movement::forced_movement()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param m The effective movement.
 */
bear::universe::forced_movement::forced_movement
( const base_forced_movement& m )
  : m_movement( m.clone() )
{

} // forced_movement::forced_movement()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::universe::forced_movement::~forced_movement()
{
  clear();
} // forced_movement::~forced_movement()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assignment.
 * \param that The instance to copy from.
 */
bear::universe::forced_movement&
bear::universe::forced_movement::operator=( const forced_movement& that )
{
  forced_movement tmp(that);
  std::swap(m_movement, tmp.m_movement);
  return *this;
} // forced_movement::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the forced movement contain an effective movement.
 */
bool bear::universe::forced_movement::is_null() const
{
  return m_movement == NULL;
} // forced_movement::is_null()

/*----------------------------------------------------------------------------*/
/**
 * \brief Remove the effective movement.
 */
void bear::universe::forced_movement::clear()
{
  delete m_movement;
  m_movement = NULL;
} // forced_movement::clear()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
void bear::universe::forced_movement::init()
{
  CLAW_PRECOND( !is_null() );
  m_movement->init();
} // forced_movement::init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop applying this movement to the item.
 */
void bear::universe::forced_movement::clear_item()
{
  CLAW_PRECOND( !is_null() );
  m_movement->clear_item();
} // forced_movement::clear_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the item concerned by this movement.
 * \param item The item to which is applied this movement.
 */
void bear::universe::forced_movement::set_item( physical_item& item )
{
  CLAW_PRECOND( !is_null() );
  m_movement->set_item(item);
} // forced_movement::set_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the reference point used by this movement is valid.
 */
bool bear::universe::forced_movement::has_reference_point() const
{
  CLAW_PRECOND( !is_null() );
  return m_movement->has_reference_point();
} // forced_movement::has_reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the reference point used by this movement.
 * \param r The reference point.
 */
void bear::universe::forced_movement::set_reference_point
( const reference_point& r )
{
  CLAW_PRECOND( !is_null() );
  m_movement->set_reference_point(r);
} // forced_movement::set_reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the point used as the reference for the movement.
 */
bear::universe::position_type
bear::universe::forced_movement::get_reference_position() const
{
  CLAW_PRECOND( !is_null() );
  return m_movement->get_reference_position();
} // forced_movement::get_reference_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the item used as a reference by this movement.
 * \param item The item used as a reference by this movement.
 */
void bear::universe::forced_movement::set_reference_point_on_center
( physical_item& item )
{
  CLAW_PRECOND( !is_null() );
  m_movement->set_reference_point_on_center( item );
} // forced_movement::set_reference_point_on_center()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if this movement has a reference item.
 */
bool bear::universe::forced_movement::has_reference_item() const
{
  CLAW_PRECOND( !is_null() );
  return m_movement->has_reference_item();
} // forced_movement::has_reference_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the item used as a reference in the movement.
 * \pre has_reference_item()
 */
bear::universe::physical_item&
bear::universe::forced_movement::get_reference_item() const
{
  CLAW_PRECOND( !is_null() );
  return m_movement->get_reference_item();
} // forced_movement::get_reference_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the movement removes himself from the moving item when
 *        finished.
 */
void bear::universe::forced_movement::set_auto_remove(bool b)
{
  CLAW_PRECOND( !is_null() );
  m_movement->set_auto_remove(b);
} // forced_movement::set_auto_remove()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the item to the next position.
 * \param elapsed_time Elapsed time since the last call.
 * \return The remaining time if the movement is finished.
 */
bear::universe::time_type
bear::universe::forced_movement::next_position( time_type elapsed_time )
{
  CLAW_PRECOND( !is_null() );
  return m_movement->next_position(elapsed_time);
} // forced_movement::next_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the movement is finished.
 */
bool bear::universe::forced_movement::is_finished() const
{
  CLAW_PRECOND( !is_null() );
  return m_movement->is_finished();
} // forced_movement::is_finished()

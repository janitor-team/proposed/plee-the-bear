/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file reference_point.cpp
 * \brief Implementation of the reference_point class.
 * \author Julien Jorge.
 */
#include "universe/forced_movement/reference_point.hpp"

#include "universe/forced_movement/base_reference_point.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::reference_point::reference_point()
  : m_reference(NULL)
{

} // reference_point::reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 * \param that The instance to copy from.
 */
bear::universe::reference_point::reference_point( const reference_point& that )
{
  if ( that.m_reference == NULL )
    m_reference = NULL;
  else
    m_reference = that.m_reference->clone();
} // reference_point::reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param p The effective reference point.
 */
bear::universe::reference_point::reference_point
( const base_reference_point& p )
  : m_reference( p.clone() )
{

} // reference_point::reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::universe::reference_point::~reference_point()
{
  delete m_reference;
} // reference_point::~reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assignment.
 * \param that The instance to copy from.
 */
bear::universe::reference_point&
bear::universe::reference_point::operator=( const reference_point& that )
{
  reference_point tmp(that);
  std::swap(m_reference, tmp.m_reference);
  return *this;
} // reference_point::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if this reference is usable.
 */
bool bear::universe::reference_point::is_valid() const
{
  return (m_reference != NULL) && m_reference->is_valid();
} // reference_point::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the point the forced movement has to use.
 */
bear::universe::position_type bear::universe::reference_point::get_point() const
{
  CLAW_PRECOND( is_valid() );
  return m_reference->get_point();
} // reference_point::get_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the reference use an item to compute the point.
 */
bool bear::universe::reference_point::has_item() const
{
  if ( m_reference == NULL )
    return false;
  else
    return m_reference->has_item();
} // reference_point::has_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the item this instance uses to compute the point.
 */
bear::universe::physical_item& bear::universe::reference_point::get_item() const
{
  CLAW_PRECOND( is_valid() );
  return m_reference->get_item();
} // reference_point::get_item()

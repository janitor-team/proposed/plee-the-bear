/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_forced_movement.cpp
 * \brief Implementation of the base_forced_movement class.
 * \author Julien Jorge.
 */
#include "universe/forced_movement/base_forced_movement.hpp"

#include "universe/forced_movement/center_of_mass_reference_point.hpp"
#include "universe/physical_item.hpp"

#include <claw/assert.hpp>
#include <claw/logger.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::base_forced_movement::base_forced_movement()
  : m_moving_item(NULL), m_auto_remove(false)
{

} // base_forced_movement::base_forced_movement()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::universe::base_forced_movement::~base_forced_movement()
{
  // nothing to do.
} // base_forced_movement::~base_forced_movement()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialise the item.
 */
void bear::universe::base_forced_movement::init()
{
  if ( m_moving_item != NULL )
    do_init();
  else
    claw::logger << claw::log_warning
                 << "base_forced_movement::init(): no item." << std::endl;
} // base_forced_movement::init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Stop applying this movement to the item.
 */
void bear::universe::base_forced_movement::clear_item()
{
  m_moving_item = NULL;
} // base_forced_movement::clear_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the item concerned by this movement.
 * \param item The item to which is applied this movement.
 */
void bear::universe::base_forced_movement::set_item( physical_item& item )
{
  m_moving_item = &item;
} // base_forced_movement::set_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the reference point used by this movement is valid.
 */
bool bear::universe::base_forced_movement::has_reference_point() const
{
  return m_reference_point.is_valid();
} // base_forced_movement::has_reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the reference point used by this movement.
 * \param r The reference point.
 */
void bear::universe::base_forced_movement::set_reference_point
( const reference_point& r )
{
  m_reference_point = r;
} // base_forced_movement::set_reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the point used as the reference for the movement.
 */
bear::universe::position_type
bear::universe::base_forced_movement::get_reference_position() const
{
  CLAW_PRECOND( has_reference_item() );
  return m_reference_point.get_point();
} // base_forced_movement::get_reference_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the reference point on the center of mass of a given item.
 * \param item The item of which we use the center of mass.
 */
void bear::universe::base_forced_movement::set_reference_point_on_center
( physical_item& item )
{
  set_reference_point( center_of_mass_reference_point(item) );
} // base_forced_movement::set_reference_point_on_center()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if this movement has a reference item.
 */
bool bear::universe::base_forced_movement::has_reference_item() const
{
  return m_reference_point.has_item();
} // base_forced_movement::has_reference_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the item used as a reference in the movement.
 * \pre has_reference_item()
 */
bear::universe::physical_item&
bear::universe::base_forced_movement::get_reference_item() const
{
  CLAW_PRECOND( has_reference_item() );

  return m_reference_point.get_item();
} // base_forced_movement::get_reference_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the movement removes himself from the moving item when
 *        finished.
 */
void bear::universe::base_forced_movement::set_auto_remove(bool b)
{
  m_auto_remove = b;
} // base_forced_movement::set_auto_remove()

/*----------------------------------------------------------------------------*/
/**
 * \brief Move the item to the next position.
 * \param elapsed_time Elapsed time since the last call.
 * \return The remaining time if the movement is finished.
 */
bear::universe::time_type
bear::universe::base_forced_movement::next_position( time_type elapsed_time )
{
  time_type remaining_time(elapsed_time);

  if ( m_moving_item != NULL )
    {
      const position_type pos( m_moving_item->get_top_left() );

      remaining_time = do_next_position(elapsed_time);

      if ( m_moving_item->get_top_left() ==  pos )
        m_moving_item->set_speed( speed_type(0, 0) );
      else if ( elapsed_time != remaining_time )
        m_moving_item->set_speed
          ( (m_moving_item->get_top_left() - pos)
            / (elapsed_time - remaining_time) );

      if (is_finished() && m_auto_remove)
        m_moving_item->clear_forced_movement();
    }
  else
    claw::logger << claw::log_warning
                 << "base_forced_movement::next_position(): no item."
                 << std::endl;

  return remaining_time;
} // base_forced_movement::next_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the item concerned by this movement.
 */
bear::universe::physical_item& bear::universe::base_forced_movement::get_item()
{
  CLAW_PRECOND( m_moving_item != NULL );

  return *m_moving_item;
} // base_forced_movement::get_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the item concerned by this movement.
 */
const bear::universe::physical_item&
bear::universe::base_forced_movement::get_item() const
{
  CLAW_PRECOND( m_moving_item != NULL );

  return *m_moving_item;
} // base_forced_movement::get_item()

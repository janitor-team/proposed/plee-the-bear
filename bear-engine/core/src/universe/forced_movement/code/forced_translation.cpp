/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_translation.cpp
 * \brief Implementation of the bear::universe::forced_translation class.
 * \author Sébastien Angibaud
 */
#include "universe/forced_movement/forced_translation.hpp"

#include "universe/physical_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param length The duration of the movement.
 */
bear::universe::forced_translation::forced_translation( time_type length )
  : m_speed(0, 0), m_total_time(length), m_remaining_time(m_total_time),
    m_angle(0), m_force_angle(false)
{

} // forced_translation::forced_translation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param speed The speed of the item.
 * \param length The duration of the movement.
 */
bear::universe::forced_translation::forced_translation
( const speed_type& speed, time_type length )
  : m_speed(speed), m_total_time(length), m_remaining_time(m_total_time),
    m_angle(0), m_force_angle(false)
{

} // forced_translation::forced_translation()

/*----------------------------------------------------------------------------*/
/**
 * \brief Instanciate a copy of this movement.
 */
bear::universe::base_forced_movement*
bear::universe::forced_translation::clone() const
{
  return new forced_translation(*this);
} // forced_translation::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the angle applied to the item.
 * \param angle The angle.
 */
void bear::universe::forced_translation::set_angle( double angle )
{
  m_angle = angle;
} // forced_translation::set_angle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if we force the angle of the item (otherwise the angles are
 *        added).
 * \param bool f Force the angle.
 */
void bear::universe::forced_translation::set_force_angle( bool f )
{
  m_force_angle = f;
} // forced_translation::set_force_angle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the speed of the item.
 * \param speed The speed.
 */
void bear::universe::forced_translation::set_speed( const speed_type& speed )
{
  m_speed = speed;
} // forced_translation::set_speed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the duration of the movement.
 * \param length The duration of the movement.
 */
void bear::universe::forced_translation::set_total_time( time_type length )
{
  m_total_time = length;
} // forced_translation::set_total_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the speed of the movement.
 */
const bear::universe::speed_type&
bear::universe::forced_translation::get_speed() const
{
  return m_speed;
} // forced_translation::get_speed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the movement is finished.
 */
bool bear::universe::forced_translation::is_finished() const
{
  return m_remaining_time == 0;
} // forced_translation::is_finished()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the movement.
 */
void bear::universe::forced_translation::do_init()
{
  m_remaining_time = m_total_time;
} // forced_translation::do_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do an iteration of the movement.
 * \param elapsed_time Elapsed time since the last call.
 * \return The remaining time if the movement is finished.
 */
bear::universe::time_type
bear::universe::forced_translation::do_next_position( time_type elapsed_time )
{
  time_type remaining_time(0);

  if ( elapsed_time > m_remaining_time )
    {
      remaining_time = elapsed_time - m_remaining_time;
      elapsed_time = m_remaining_time;
    }

  get_item().set_top_left( get_item().get_top_left() + m_speed * elapsed_time );

  if ( m_force_angle )
    get_item().set_system_angle( m_angle );
  else
    get_item().set_system_angle( get_item().get_system_angle() + m_angle );

  m_remaining_time -= elapsed_time;

  return remaining_time;
} // forced_translation::do_next_position()

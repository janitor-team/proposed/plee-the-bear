/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_sequence.cpp
 * \brief Implementation of the bear::universe::forced_sequence class.
 * \author Julien Jorge
 */
#include "universe/forced_movement/forced_sequence.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::forced_sequence::forced_sequence()
  : m_index(0), m_loops(0), m_play_count(0)
{

} // forced_sequence::forced_sequence()

/*----------------------------------------------------------------------------*/
/**
 * \brief Instanciate a copy of this movement.
 */
bear::universe::base_forced_movement*
bear::universe::forced_sequence::clone() const
{
  return new forced_sequence(*this);
} // forced_sequence::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a movement at the end of the sequence.
 * \param m The movement to add.
 */
void bear::universe::forced_sequence::push_back( const forced_movement& m )
{
  m_sub_sequence.push_back( m );
  m_sub_sequence.back().set_auto_remove(false);
} // forced_sequence::push_back()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell how many times the sequence must be done.
 * \param loops The number of loops.
 */
void bear::universe::forced_sequence::set_loops( unsigned int loops )
{
  m_loops = loops;
} // forced_sequence::set_loops()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the movement is finished.
 */
bool bear::universe::forced_sequence::is_finished() const
{
  return (m_play_count == m_loops) && m_loops;
} // forced_sequence::is_finished()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the movement.
 */
void bear::universe::forced_sequence::do_init()
{
  m_play_count = 0;
  m_index = 0;

  if ( !m_sub_sequence.empty() )
    {
      m_sub_sequence[0].set_item( get_item() );
      m_sub_sequence[0].init();
    }
} // forced_sequence::do_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do an iteration of the movement.
 * \param elapsed_time Elapsed time since the last call.
 * \return The remaining time if the movement is finished.
 */
bear::universe::time_type
bear::universe::forced_sequence::do_next_position( time_type elapsed_time )
{
  time_type remaining_time(elapsed_time);

  if ( !m_sub_sequence.empty() )
    {
      remaining_time = m_sub_sequence[m_index].next_position(remaining_time);

      if ( m_sub_sequence[m_index].is_finished() )
        {
          next_sequence();

          if ( remaining_time > 0 )
            if ( !is_finished() )
              remaining_time = next_position(remaining_time);
        }
    }

  return remaining_time;
} // forced_sequence::do_next_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Go to the next sub sequence.
 */
void bear::universe::forced_sequence::next_sequence()
{
  m_sub_sequence[m_index].clear_item();
  ++m_index;

  if ( m_index == m_sub_sequence.size() )
    {
      ++m_play_count;
      m_index = 0;
    }

  if ( !is_finished() )
    {
      m_sub_sequence[m_index].set_item( get_item() );
      m_sub_sequence[m_index].init();
    }
} // forced_sequence::next_sequence()

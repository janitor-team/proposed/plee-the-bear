/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file code/center_of_mass_reference_point.cpp
 * \brief Implementation of the center_of_mass_reference_point class.
 * \author Julien Jorge
 */
#include "universe/forced_movement/center_of_mass_reference_point.hpp"

#include "universe/physical_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param item The item of which the center of mass is returned.
 */
bear::universe::center_of_mass_reference_point::center_of_mass_reference_point
( physical_item& item )
  : m_item(item)
{

} // center_of_mass_reference_point::center_of_mass_reference_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Duplicate this instance.
 */
bear::universe::base_reference_point*
bear::universe::center_of_mass_reference_point::clone() const
{
  return new center_of_mass_reference_point(*this);
} // center_of_mass_reference_point::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if this reference is usable.
 */
bool bear::universe::center_of_mass_reference_point::is_valid() const
{
  return has_item();
} // center_of_mass_reference_point::is_valid()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the center of mass of the item.
 */
bear::universe::position_type
bear::universe::center_of_mass_reference_point::get_point() const
{
  CLAW_PRECOND( has_item() );
  return m_item->get_center_of_mass();
} // center_of_mass_reference_point::get_point()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the item is still valid.
 */
bool bear::universe::center_of_mass_reference_point::has_item() const
{
  return m_item != NULL;
} // center_of_mass_reference_point::has_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the item of which the center of mass is returned.
 */
bear::universe::physical_item&
bear::universe::center_of_mass_reference_point::get_item() const
{
  CLAW_PRECOND( has_item() );
  return *m_item;
} // center_of_mass_reference_point::get_item()

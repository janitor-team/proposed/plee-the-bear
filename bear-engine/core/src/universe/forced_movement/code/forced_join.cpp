/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_join.cpp
 * \brief Implementation of the bear::universe::forced_join class.
 * \author Julien Jorge.
 */
#include "universe/forced_movement/forced_join.hpp"

#include "universe/physical_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \steps length Maximum time to reach the target.
 */
bear::universe::forced_join::forced_join( time_type length )
  : m_total_time(length), m_remaining_time(m_total_time)
{

} // forced_join::forced_join()

/*----------------------------------------------------------------------------*/
/**
 * \brief Instanciate a copy of this movement.
 */
bear::universe::base_forced_movement* bear::universe::forced_join::clone() const
{
  return new forced_join(*this);
} // forced_join::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the total time to reach the target.
 * \steps length Maximum time to reach the target.
 */
void bear::universe::forced_join::set_total_time( time_type length )
{
  m_total_time = length;
} // forced_join::set_total_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if the movement is finished.
 */
bool bear::universe::forced_join::is_finished() const
{
  return !has_reference_point() || (m_remaining_time == 0);
} // forced_join::is_finished()

/*----------------------------------------------------------------------------*/
/**
 * \brief Initialize the movement.
 */
void bear::universe::forced_join::do_init()
{
  m_remaining_time = m_total_time;
} // forced_join::do_init()

/*----------------------------------------------------------------------------*/
/**
 * \brief Do an iteration of the movement.
 * \param elapsed_time Elapsed time since the last call.
 * \return The remaining time if the movement is finished.
 */
bear::universe::time_type
bear::universe::forced_join::do_next_position( time_type elapsed_time )
{
  time_type remaining_time(0);

  if ( has_reference_point() )
    {
      const position_type target_center = get_reference_position();
      const position_type item_center = get_item().get_center_of_mass();
      position_type dp = target_center - item_center;

      if ( m_remaining_time > elapsed_time )
        {
          dp = dp / m_remaining_time * elapsed_time;
          m_remaining_time -= elapsed_time;
        }
      else
        remaining_time = elapsed_time - m_remaining_time;

      get_item().set_top_left( get_item().get_top_left() + dp );

      if ( (item_center + dp) == target_center )
        m_remaining_time = 0;
    }

  return remaining_time;
} // forced_join::do_next_position()

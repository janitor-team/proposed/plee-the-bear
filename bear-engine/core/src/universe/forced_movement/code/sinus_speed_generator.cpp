/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sinus_speed_generator.cpp
 * \brief Implementation of the bear::universe::sinus_speed_generator class.
 * \author Julien Jorge.
 */
#include "universe/forced_movement/sinus_speed_generator.hpp"

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::sinus_speed_generator::sinus_speed_generator()
  : m_total_time(std::numeric_limits<time_type>::infinity()),
    m_acceleration_time(m_total_time),
    m_distance(std::numeric_limits<coordinate_type>::infinity())
{

} // sinus_speed_generator::sinus_speed_generator()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the total time of the movement.
 * \param length Total time of the movement.
 */
void bear::universe::sinus_speed_generator::set_total_time( time_type length )
{
  m_total_time = length;
} // sinus_speed_generator::set_total_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the total time of the movement.
 */
bear::universe::time_type
bear::universe::sinus_speed_generator::get_total_time() const
{
  return m_total_time;
} // sinus_speed_generator::get_total_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the duration of the acceleration and deceleration.
 * \param length The duration of the acceleration and deceleration.
 */
void
bear::universe::sinus_speed_generator::set_acceleration_time( time_type length )
{
  m_acceleration_time = length;
} // sinus_speed_generator::set_acceleration_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the duration of the acceleration and deceleration.
 */
bear::universe::time_type
bear::universe::sinus_speed_generator::get_acceleration_time() const
{
  return m_acceleration_time;
} // sinus_speed_generator::get_acceleration_time()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the distance of the journey.
 * \param d The distance
 */
void bear::universe::sinus_speed_generator::set_distance( coordinate_type d )
{
  m_distance = d;
} // sinus_speed_generator::set_distance()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the distance of the journey.
 */
bear::universe::coordinate_type
bear::universe::sinus_speed_generator::get_distance() const
{
  return m_distance;
} // sinus_speed_generator::get_distance()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the speed at a given time.
 * \param t The date at which we want the speed.
 */
bear::universe::coordinate_type
bear::universe::sinus_speed_generator::get_speed( time_type t ) const
{
  const coordinate_type vmax =
    m_distance / ( m_total_time - m_acceleration_time );
  coordinate_type result;

  if ( t < m_acceleration_time )
    result = (-std::cos(t * 3.14159 / m_acceleration_time) + 1) * vmax / 2;
  else if ( t > m_total_time - m_acceleration_time )
    {
      t -= m_total_time - m_acceleration_time;
      result = (std::cos(t * 3.14159 / m_acceleration_time) + 1) * vmax / 2;
    }
  else
    result = vmax;

  return result;
} // sinus_speed_generator::get_speed()

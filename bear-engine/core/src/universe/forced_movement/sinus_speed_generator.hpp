/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file sinus_speed_generator.hpp
 * \brief A speed generator which, for a given time-window and a distance,
 *        applies a sinuso�dal speed curve during the acceleration time and the
 *        deceleration time.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_SINUS_SPEED_GENERATOR_HPP__
#define __UNIVERSE_SINUS_SPEED_GENERATOR_HPP__

#include "universe/types.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief A speed generator which, for a given time-window and a distance,
     *        applies a sinuso�dal speed curve during the acceleration time and
     *        the deceleration time.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT sinus_speed_generator
    {
    public:
      sinus_speed_generator();

      void set_total_time( time_type length );
      time_type get_total_time() const;

      void set_acceleration_time( time_type length );
      time_type get_acceleration_time() const;

      void set_distance( coordinate_type d );
      coordinate_type get_distance() const;

      coordinate_type get_speed( time_type t ) const;

    private:
      /** \brief Total time to reach the target. */
      time_type m_total_time;

      /** \brief The duration of the acceleration and deceleration. */
      time_type m_acceleration_time;

      /** \brief The total distance of the journey. */
      coordinate_type m_distance;

    }; // class sinus_speed_generator
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_SINUS_SPEED_GENERATOR_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_reference_point.hpp
 * \brief Instances of base_reference_point compute the reference
 *        point used in forced movements.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_BASE_REFERENCE_POINT_HPP__
#define __UNIVERSE_BASE_REFERENCE_POINT_HPP__

#include "universe/types.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    class physical_item;

    /**
     * \brief Instances of base_reference_point compute the reference
     *        point used in forced movements.
     *
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT base_reference_point
    {
    public:
      /** \brief Destructor. */
      virtual ~base_reference_point() {}

      /** \brief Create a copy of this instance using the operator new. */
      virtual base_reference_point* clone() const = 0;

      /** \brief Tell if get_point() can be safely called. */
      virtual bool is_valid() const = 0;

      /** \brief Get the position of the reference point. */
      virtual position_type get_point() const = 0;

      /** \brief Tell if a call to get_item() can be done. */
      virtual bool has_item() const = 0;

      /** \brief Get the item used to compute the position. */
      virtual physical_item& get_item() const = 0;

    }; // class base_reference_point
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_BASE_REFERENCE_POINT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_sequence.hpp
 * \brief A forced movement made of a sequence of forced movements.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_FORCED_SEQUENCE_HPP__
#define __UNIVERSE_FORCED_SEQUENCE_HPP__

#include "universe/forced_movement/base_forced_movement.hpp"
#include "universe/forced_movement/forced_movement.hpp"

#include "universe/class_export.hpp"

#include <vector>

namespace bear
{
  namespace universe
  {
    /**
     * \brief A forced movement made of a sequence of forced movements.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT forced_sequence:
      public base_forced_movement
    {
    public:
      forced_sequence();

      base_forced_movement* clone() const;

      void push_back( const forced_movement& m );
      void set_loops( unsigned int loops );

      bool is_finished() const;

    private:
      void do_init();
      time_type do_next_position( time_type elapsed_time );
      void next_sequence();

    private:
      /** \brief The sub sequences. */
      std::vector<forced_movement> m_sub_sequence;

      /** \brief Index of the current sub sequence. */
      unsigned int m_index;

      /** \brief How many times the sequence will be done. */
      unsigned int m_loops;

      /** \brief How many full play have we done ? */
      unsigned int m_play_count;

    }; // class forced_sequence
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_FORCED_SEQUENCE_HPP__

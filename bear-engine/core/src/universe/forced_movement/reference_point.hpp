/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file reference_point.hpp
 * \brief Instances of reference_point compute the reference point used in
 *        forced movements.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_REFERENCE_POINT_HPP__
#define __UNIVERSE_REFERENCE_POINT_HPP__

#include "universe/types.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    class physical_item;
    class base_reference_point;

    /**
     * \brief Instances of reference_point compute the reference
     *        point used in forced movements.
     *
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT reference_point
    {
    public:
      reference_point();
      reference_point( const reference_point& that );
      reference_point( const base_reference_point& p );
      ~reference_point();

      reference_point& operator=( const reference_point& that );

      bool is_valid() const;
      position_type get_point() const;
      bool has_item() const;
      physical_item& get_item() const;

    private:
      /** \brief The effective reference point. */
      base_reference_point* m_reference;

    }; // class reference_point
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_REFERENCE_POINT_HPP__

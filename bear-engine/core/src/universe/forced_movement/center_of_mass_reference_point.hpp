/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file center_of_mass_reference_point.hpp
 * \brief Instances of center_of_mass_reference_point compute the reference
 *        point of in forced movements as the center of mass of a given item.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_CENTER_OF_MASS_REFERENCE_POINT_HPP__
#define __UNIVERSE_CENTER_OF_MASS_REFERENCE_POINT_HPP__

#include "universe/forced_movement/base_reference_point.hpp"
#include "universe/item_handle.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief Instances of center_of_mass_reference_point compute the reference
     *        point of in forced movements as the center of mass of a given
     *        item.
     *
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT center_of_mass_reference_point:
      public base_reference_point
    {
    public:
      explicit center_of_mass_reference_point( physical_item& item );

      virtual base_reference_point* clone() const;

      virtual bool is_valid() const;
      virtual position_type get_point() const;

      virtual bool has_item() const;
      virtual physical_item& get_item() const;

    private:
      /** The item on which we take the center of mass. */
      item_handle m_item;

    }; // class center_of_mass_reference_point
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_CENTER_OF_MASS_REFERENCE_POINT_HPP__

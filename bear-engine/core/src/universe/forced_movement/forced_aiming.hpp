/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_aiming.hpp
 * \brief A forced movement that will make the current item rotating around
 *        another item.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_FORCED_AIMING_HPP__
#define __UNIVERSE_FORCED_AIMING_HPP__

#include "universe/forced_movement/base_forced_movement.hpp"

#include "universe/class_export.hpp"

#include <limits>

namespace bear
{
  namespace universe
  {
    /**
     * \brief A forced movement that will make the current item going in the
     *        direction of an other item.
     *
     * The positions considered for the items are their center of mass.
     *
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT forced_aiming:
      public base_forced_movement
    {
    public:
      forced_aiming
      ( time_type length = std::numeric_limits<time_type>::infinity() );

      base_forced_movement* clone() const;

      void set_max_angle( double a );
      void set_max_speed( double s );
      void set_acceleration( double a );
      void set_apply_angle( bool b );
      void set_total_time( time_type length );

      bool is_finished() const;

    private:
      void do_init();
      time_type do_next_position( time_type elapsed_time );

      time_type compute_remaining_time( time_type& elapsed_time );
      double compute_speed( time_type elapsed_time ) const;
      double compute_direction( vector_type& dir ) const;

    private:
      /** \brief Maximum angle when the direction changes. */
      double m_max_angle;

      /** \brief The maximum speed of the moving item. */
      double m_max_speed;

      /** \brief The acceleration of the moving item. */
      double m_acceleration;

      /** \brief Total time of the movement. */
      time_type m_total_time;

      /** \brief Remaining time. */
      time_type m_remaining_time;

      /** \brief Tell if we apply the angle of the movement to the item. */
      bool m_apply_angle;

    }; // class forced_aiming
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_FORCED_AIMING_HPP__

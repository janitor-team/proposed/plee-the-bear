/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_goto.hpp
 * \brief A forced movement that will bring an item to a given position.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_FORCED_GOTO_HPP__
#define __UNIVERSE_FORCED_GOTO_HPP__

#include "universe/forced_movement/base_forced_movement.hpp"
#include "universe/forced_movement/sinus_speed_generator.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief A forced movement that will bring an item to a given position.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT forced_goto:
      public base_forced_movement
    {
    public:
      forced_goto( time_type length = 1 );

      base_forced_movement* clone() const;

      void set_total_time( time_type length );
      void set_acceleration_time( time_type length );
      void set_length( const vector_type& v );
      void set_x_length( coordinate_type v );
      void set_y_length( coordinate_type v );

      bool is_finished() const;

    private:
      void do_init();
      time_type do_next_position( time_type elapsed_time );

      coordinate_type get_speed( time_type t ) const;

    private:
      /** \brief Total time to reach the target. */
      time_type m_total_time;

      /** \brief Time elapsed since the begining of the movement. */
      time_type m_elapsed_time;

      /** \brief The position to reach. */
      position_type m_target_position;

      /** \brief The length of the journey. */
      vector_type m_length;

      /** \brief The speed generator used to move the item. */
      sinus_speed_generator m_speed_generator;

    }; // class forced_goto
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_FORCED_GOTO_HPP__

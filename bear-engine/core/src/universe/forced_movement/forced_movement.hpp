/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file forced_movement.hpp
 * \brief An interface to the forced movements.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_FORCED_MOVEMENT_HPP__
#define __UNIVERSE_FORCED_MOVEMENT_HPP__

#include "universe/forced_movement/reference_point.hpp"
#include "universe/types.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    class base_forced_movement;
    class physical_item;

    /**
     * \brief An interface to the forced movements.
     *
     * If an item has a forced movement, the physical rules won't apply to him.
     *
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT forced_movement
    {
    public:
      forced_movement();
      forced_movement( const forced_movement& that );
      forced_movement( const base_forced_movement& m );
      ~forced_movement();

      forced_movement& operator=( const forced_movement& that );

      bool is_null() const;
      void clear();

      void init();

      void clear_item();
      void set_item( physical_item& item );

      bool has_reference_point() const;
      void set_reference_point( const reference_point& r );
      position_type get_reference_position() const;

      void set_reference_point_on_center( physical_item& item );
      bool has_reference_item() const;
      physical_item& get_reference_item() const;

      void set_auto_remove(bool b);

      time_type next_position( time_type elapsed_time );
      bool is_finished() const;

    private:
      /** \brief The effective movement. */
      base_forced_movement* m_movement;

    }; // class forced_movement
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_FORCED_MOVEMENT_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_handle.hpp
 * \brief Safe way to point an item that could die between two uses.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_ITEM_HANDLE_HPP__
#define __UNIVERSE_ITEM_HANDLE_HPP__

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    class physical_item;

    /**
     * \brief Safe way to point an item that could die between two uses.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT item_handle
    {
    public:
      typedef physical_item item_type;

    public:
      item_handle();
      item_handle( item_type* item );
      item_handle( item_type& item );
      item_handle( const item_handle& that );
      ~item_handle();

      item_type* get() const;

      item_type& operator*() const;
      item_type* operator->() const;

      item_handle& operator=( item_type* item );
      item_handle& operator=( const item_handle& that );

      bool operator==( const item_type* item ) const;
      bool operator==( const item_handle& that ) const;
      bool operator!=( const item_type* item ) const;
      bool operator!=( const item_handle& that ) const;
      bool operator<( const item_handle& that ) const;

    private:
      /** \brief The critical item. */
      item_type* m_item;

    }; // class item_handle

  } // namespace universe
} // namespace bear

bool operator==
( const bear::universe::physical_item* a,
  const bear::universe::item_handle& b );
bool operator!=
( const bear::universe::physical_item* a,
  const bear::universe::item_handle& b );

#endif // __UNIVERSE_ITEM_HANDLE_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file bottom_contact_is_lower.hpp
 * \brief Check, in a collision context, if the bottom of the "that" item was
 *        lower than a given value, relative to the top of the "self" item.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_BOTTOM_CONTACT_IS_LOWER_HPP__
#define __UNIVERSE_BOTTOM_CONTACT_IS_LOWER_HPP__

#include "universe/collision_info.hpp"
#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief Check, in a collision context, if the bottom of the "that" item
     *        was lower than a given value, relative to the top of the "self"
     *        item.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT bottom_contact_is_lower
    {
    public:
      /** \brief The type of the line that separate the block into two
          regions. */
      typedef claw::math::line_2d<coordinate_type> line_type;

    public:
      bottom_contact_is_lower( const line_type& line );
      bottom_contact_is_lower( coordinate_type val );

      bool operator()( const collision_info& info, physical_item& self,
                       physical_item& that ) const;

    private:
      /** \brief We will check if the bottom of the other item was in the part
          at the top of this line. */
      const line_type m_line;

    }; // class bottom_contact_is_lower
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_BOTTOM_CONTACT_IS_LOWER_HPP__

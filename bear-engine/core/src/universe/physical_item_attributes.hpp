/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file physical_item_attributes.hpp
 * \brief Description of a physical item.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_PHYSICAL_ITEM_ATTRIBUTES_HPP__
#define __UNIVERSE_PHYSICAL_ITEM_ATTRIBUTES_HPP__

#include "universe/types.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief Description of a physical item.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT physical_item_attributes
    {
    public:
      physical_item_attributes();

    protected:
      /** \brief Item's mass. */
      double m_mass;

      /** \brief Item's density. */
      double m_density;

      /** \brief Current item's angular speed. */
      double m_angular_speed;

      /** \brief Current item's speed. */
      speed_type m_speed;

      /** \brief Current item's acceleration (forces coming from the item). */
      force_type m_acceleration;

      /** \brief Current item's internal force. */
      force_type m_internal_force;

      /** \brief Current item's external force. */
      force_type m_external_force;

      /** \brief Item friction as a percentage of the kept movement. */
      double m_self_friction;

      /** \brief Friction applied to the item by a contact with an other item,
          as a percentage of the kept movement. */
      double m_contact_friction;

      /** \brief Item elasticity. */
      double m_elasticity;

      /** \brief Item hardness. */
      double m_hardness;

      /** \brief Current item's position. */
      position_type m_position;

      /** \brief Item's size. */
      size_box_type m_size;

      /** \brief Item's orientation. */
      double m_system_angle;

      /** \brief Tell if the item can move an other item. */
      bool m_can_move_items;

      /** \brief Tell if the item is in contact with an other item on its
          left. */
      bool m_left_contact;

      /** \brief Tell if the item is in contact with an other item on its
          right. */
      bool m_right_contact;

      /** \brief Tell if the item is in contact with an other item on its
          top. */
      bool m_top_contact;

      /** \brief Tell if the item is in contact with an other item on its
          bottom (ie. the current item is laid down an item. */
      bool m_bottom_contact;

      /** \brief Tell if the item is in contact with an other item
          in the middle zone. */
      bool m_middle_contact;

      /** \brief Indicate if the item is a phantom, i.e. the item is never
          aligned by an other item. */
      bool m_is_phantom;

      /** \brief Indicate if the item is artificial, i.e. we don't care about
          the collisions. */
      bool m_is_artificial;

      /** \brief Tell if the item has to be considered as weak collisions. */
      bool m_weak_collisions;

    }; // class physical_item_attributes
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_PHYSICAL_ITEM_ATTRIBUTES_HPP__

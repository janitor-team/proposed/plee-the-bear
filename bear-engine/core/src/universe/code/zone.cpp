/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file zone.cpp
 * \brief Implementation of the bear::universe::zone class.
 * \author Julien Jorge
 */
#include "universe/zone.hpp"

/*----------------------------------------------------------------------------*/
const unsigned int bear::universe::zone::cardinality = 9;

/*----------------------------------------------------------------------------*/
/**
 * \brief Find the zone where is a box.
 * \param that_box The box to locate.
 * \param this_box The considered box (the box who split the space).
 */
bear::universe::zone::position
bear::universe::zone::find
( const rectangle_type& that_box, const rectangle_type& this_box )
{
  position result;

  // he's on my left
  if ( that_box.right() <= this_box.left() )
    {
      if ( that_box.bottom() >= this_box.top() )
        result = top_left_zone;
      else if ( that_box.top() <= this_box.bottom() )
        result = bottom_left_zone;
      else
        result = middle_left_zone;
    }
  // on my right
  else if ( that_box.left() >= this_box.right() )
    {
      if ( that_box.bottom() >= this_box.top() )
        result = top_right_zone;
      else if ( that_box.top() <= this_box.bottom() )
        result = bottom_right_zone;
      else
        result = middle_right_zone;
    }
  else // in the middle
    {
      if ( that_box.bottom() >= this_box.top() )
        result = top_zone;
      else if ( that_box.top() <= this_box.bottom() )
        result = bottom_zone;
      else
        result = middle_zone;
    }

  return result;
} // zone::find()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the opposite side of a given side.
 * \param side The side from which we want the opposite.
 */
bear::universe::zone::position
bear::universe::zone::opposite_of( position side )
{
  switch(side)
  {
  case top_left_zone: return bottom_right_zone;
  case top_zone: return bottom_zone;
  case top_right_zone: return bottom_left_zone;
  case middle_left_zone: return middle_right_zone;
  case middle_zone: return middle_zone;
  case middle_right_zone: return middle_left_zone;
  case bottom_left_zone: return top_right_zone;
  case bottom_zone: return top_zone;
  case bottom_right_zone: return top_left_zone;
  }
} // zone::opposite_of()

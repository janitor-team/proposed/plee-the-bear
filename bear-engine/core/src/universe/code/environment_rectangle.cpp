/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file universe/code/environment_rectangle.cpp
 * \brief Implementation of the bear::universe::environment_rectangle class.
 * \author Sébastien Angibaud
 */
#include "universe/environment_rectangle.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Default constructor.
 */
bear::universe::environment_rectangle::environment_rectangle()
{

} // environment_rectangle::environment_rectangle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param r The rectangle where the environment is different.
 * \param e The environment in this rectangle.
 */
bear::universe::environment_rectangle::environment_rectangle
( const rectangle_type& r, const environment_type e )
  : rectangle(r), environment(e)
{

} // environment_rectangle::environment_rectangle()

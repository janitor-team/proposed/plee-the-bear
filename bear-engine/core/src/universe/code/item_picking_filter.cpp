/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file universe/code/item_picking_filter.cpp
 * \brief Implementation of the bear::universe::item_picking_filter class.
 * \author Julien Jorge
 */
#include "universe/item_picking_filter.hpp"

#include "universe/physical_item.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::item_picking_filter::item_picking_filter()
  : m_check_artificial(false), m_check_phantom(false),
    m_check_can_move_items(false), m_check_fixed(false),
    m_check_forbidden_position(false)
{

} // item_picking_filter::item_picking_filter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::universe::item_picking_filter::~item_picking_filter()
{

} // item_picking_filter::~item_picking_filter()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if an item satisfies the condition of the filter.
 * \param item The item to check.
 */
bool bear::universe::item_picking_filter::satisfies_condition
( const physical_item& item ) const
{
  return (!m_check_artificial || (item.is_artificial() == m_artificial_value))
    && (!m_check_phantom || (item.is_phantom() == m_phantom_value))
    && (!m_check_can_move_items
        || (item.can_move_items() == m_can_move_items_value))
    && (!m_check_fixed || (item.is_fixed() == m_fixed_value))
    && (!m_check_forbidden_position ||
        !item.get_bounding_box().includes(m_forbidden_position) )
    && do_satisfies_condition(item);
} // item_picking_filter::satisfies_condition()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of the flag "artificial" for the selected items.
 * \param v The value this flag must have.
 */
void bear::universe::item_picking_filter::set_artificial_value( bool v )
{
  m_check_artificial = true;
  m_artificial_value = v;
} // item_picking_filter::set_artificial_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of the flag "phantom" for the selected items.
 * \param v The value this flag must have.
 */
void bear::universe::item_picking_filter::set_phantom_value( bool v )
{
  m_check_phantom = true;
  m_phantom_value = v;
} // item_picking_filter::set_phantom_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of the flag "can_move_items" for the selected items.
 * \param v The value this flag must have.
 */
void bear::universe::item_picking_filter::set_can_move_items_value( bool v )
{
  m_check_can_move_items = true;
  m_can_move_items_value = v;
} // item_picking_filter::set_can_move_items_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the value of the flag "fixed" for the selected items.
 * \param v The value this flag must have.
 */
void bear::universe::item_picking_filter::set_fixed_value( bool v )
{
  m_check_fixed = true;
  m_fixed_value = v;
} // item_picking_filter::set_fixed_value()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set a position where the item must not be.
 * \param pos The position of the origin.
 */
void bear::universe::item_picking_filter::set_forbidden_position
( const position_type& pos )
{
  m_check_forbidden_position = true;
  m_forbidden_position = pos;
} // item_picking_filter::set_forbidden_position()

/*----------------------------------------------------------------------------*/
/**
 * \brief Overridden condition checking.
 * \param The item to check
 */
bool bear::universe::item_picking_filter::do_satisfies_condition
( const physical_item& item ) const
{
  return true;
} // item_picking_filter::do_satisfies_condition()


/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file item_handle.cpp
 * \brief Implementation of the bear::universe::item_handle class.
 * \author Julien Jorge
 */
#include "universe/item_handle.hpp"

#include "universe/physical_item.hpp"
#include <cstdlib>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::item_handle::item_handle()
  : m_item(NULL)
{

} // item_handle::item_handle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param item The item to handle.
 */
bear::universe::item_handle::item_handle( item_type* item )
  : m_item(item)
{
  if ( m_item )
    m_item->add_handle( this );
} // item_handle::item_handle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param item The item to handle.
 */
bear::universe::item_handle::item_handle( item_type& item )
  : m_item(&item)
{
  m_item->add_handle( this );
} // item_handle::item_handle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Copy constructor.
 * \param that The handle from which we copy.
 */
bear::universe::item_handle::item_handle( const item_handle& that )
  : m_item(that.m_item)
{
  if ( m_item )
    m_item->add_handle( this );
} // item_handle::item_handle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::universe::item_handle::~item_handle()
{
  if ( m_item )
    m_item->remove_handle( this );
} // item_handle::item_handle()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the pointer.
 */
bear::universe::item_handle::item_type* bear::universe::item_handle::get() const
{
  return m_item;
} // item_handle::get()

/*----------------------------------------------------------------------------*/
/**
 * \brief Dereference operator.
 */
bear::universe::item_handle::item_type&
bear::universe::item_handle::operator*() const
{
  return *m_item;
} // item_handle::operator*()

/*----------------------------------------------------------------------------*/
/**
 * \brief Pointer-to-member operator.
 */
bear::universe::item_handle::item_type*
bear::universe::item_handle::operator->() const
{
  return m_item;
} // item_handle::operator->()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assigment operator.
 * \param item The item to assign.
 */
bear::universe::item_handle&
bear::universe::item_handle::operator=( item_type* item )
{
  if ( m_item != NULL )
    m_item->remove_handle( this );

  m_item = item;

  if ( m_item != NULL )
    m_item->add_handle( this );

  return *this;
} // item_handle::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Assigment operator.
 * \param that The item to assign.
 */
bear::universe::item_handle&
bear::universe::item_handle::operator=( const item_handle& that )
{
  return *this = that.m_item;
} // item_handle::operator=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Equality.
 * \param item The pointer to compare to.
 */
bool bear::universe::item_handle::operator==
( const item_type* item ) const
{
  return m_item == item;
} // item_handle::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Equality.
 * \param that The pointer to compare to.
 */
bool bear::universe::item_handle::operator==
( const item_handle& that ) const
{
  return m_item == that.m_item;
} // item_handle::operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Disequality.
 * \param item The pointer to compare to.
 */
bool bear::universe::item_handle::operator!=
( const item_type* item ) const
{
  return m_item != item;
} // item_handle::operator!=()

/*----------------------------------------------------------------------------*/
/**
 * \brief Disequality.
 * \param that The instance to compare to.
 */
bool bear::universe::item_handle::operator!=
( const item_handle& that ) const
{
  return m_item != that.m_item;
} // item_handle::operator!=()

/*----------------------------------------------------------------------------*/
/**
 * \brief "Less than" operator.
 * \param that The pointer to compare to.
 */
bool bear::universe::item_handle::operator<
  ( const item_handle& that ) const
{
  return m_item < that.m_item;
} // item_handle::operator<()




/*----------------------------------------------------------------------------*/
/**
 * \brief Compare a pointer to a physical_item with an item_handle.
 * \param a The pointer.
 * \param b The item handle.
 * \return b == a
 */
bool operator==
( const bear::universe::physical_item* a, const bear::universe::item_handle& b )
{
  return b == a;
} // operator==()

/*----------------------------------------------------------------------------*/
/**
 * \brief Compare a pointer to a physical_item with an item_handle.
 * \param a The pointer.
 * \param b The item handle.
 * \return b != a
 */
bool operator!=
( const bear::universe::physical_item* a, const bear::universe::item_handle& b )
{
  return b != a;
} // operator!=()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file physical_item_attributes.cpp
 * \brief Implementation of the bear::universe::physical_item_attributes class.
 * \author Julien Jorge
 */
#include "universe/physical_item_attributes.hpp"

#include <limits>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 */
bear::universe::physical_item_attributes::physical_item_attributes()
  : m_mass(std::numeric_limits<double>::infinity()), m_density(1),
    m_angular_speed(0), m_speed(0, 0), m_acceleration(0, 0),
    m_internal_force(0, 0), m_external_force(0, 0), m_self_friction(0.98),
    m_contact_friction(1), m_elasticity(0), m_hardness(1), m_position(0, 0),
    m_size(0, 0), m_system_angle(0), m_can_move_items(true),
    m_left_contact(false), m_right_contact(false), m_top_contact(false),
    m_bottom_contact(false), m_middle_contact(false), m_is_phantom(false),
    m_is_artificial(false), m_weak_collisions(false)
{

} // physical_item_attributes::physical_item_attributes()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file collision_repair.hpp
 * \brief This class stores the needs for repairing a collision.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_COLLISION_REPAIR_HPP__
#define __UNIVERSE_COLLISION_REPAIR_HPP__

#include "universe/types.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    class physical_item;

    /**
     * \brief This class stores the needs for repairing a collision.
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT collision_repair
    {
    public:
      collision_repair( physical_item& first_item, physical_item& second_item );

      void
      set_contact_normal( const physical_item& ref, const vector_type& normal );

      void apply();

    private:
      void apply_force_transfert();

    private:
      /** \brief The first item in the collision. */
      physical_item& m_first_item;

      /** \brief The second item in the collision. */
      physical_item& m_second_item;

      /** \brief The vector normal of the contact. */
      vector_type m_contact_normal;

      /** \brief The item to use as the reference for the contact normal. */
      physical_item* m_contact_reference;

    }; // class collision_repair
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_COLLISION_REPAIR_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file alignment.cpp
 * \brief implementation of the bear::universe::alignment class.
 * \author Julien Jorge
 */
#include "universe/alignment/alignment.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::universe::alignment::~alignment()
{
  // nothing to do
} // alignment::~alignment()

/*----------------------------------------------------------------------------*/
/**
 * \brief Align one bow with another.
 * \param this_box The box to which we will align the other.
 * \param that_old_pos The position from where comes the other box.
 * \param that_new_box (in/out) The box we will align.
 * \remark This method does nothing.
 */
void bear::universe::alignment::align
( const bear::universe::rectangle_type& this_box,
  const bear::universe::position_type& that_old_pos,
  bear::universe::rectangle_type& that_new_box ) const
{
  // nothing to do
} // alignment::align()

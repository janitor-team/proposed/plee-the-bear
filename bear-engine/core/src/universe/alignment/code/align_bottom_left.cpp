/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file align_bottom_left.cpp
 * \brief Implementation of the bear::universe::align_bottom_left class.
 * \author Julien Jorge
 */
#include "universe/alignment/align_bottom_left.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Align a box at the bottom or on the left, according to the first
 *        touched edge.
 * \param this_box The box to which we will align the other.
 * \param that_old_pos The position from where comes the other box.
 * \param that_new_box (in/out) The position of the box we will align.
 */
void bear::universe::align_bottom_left::align
( const rectangle_type& this_box, const position_type& that_old_pos,
  rectangle_type& that_new_box ) const
{
  claw::math::line_2d<coordinate_type> dir, ortho;
  position_type inter;

  dir.origin = that_old_pos + that_new_box.size();
  dir.direction = dir.origin - that_new_box.top_right();

  ortho.origin = this_box.bottom_left();
  ortho.direction = dir.direction.get_orthonormal_anticlockwise();

  inter = dir.intersection( ortho );

  if ( inter.x < this_box.left() )
    align_left(this_box, that_old_pos, that_new_box, dir);
  else if ( inter.x > this_box.left() )
    align_bottom(this_box, that_old_pos, that_new_box, dir);
  else
    that_new_box.top_right( inter );
} // align_bottom_left::align()

/*----------------------------------------------------------------------------*/
/**
 * \brief Align a box at the bottom.
 * \param this_box The box to which we will align the other.
 * \param that_old_pos The position from where comes the other box.
 * \param that_new_box (in/out) The position of the box we will align.
 * \param dir A line representing the movement of the top right corner of the
 *        other box.
 */
void bear::universe::align_bottom_left::align_bottom
( const rectangle_type& this_box, const position_type& that_old_pos,
  rectangle_type& that_new_box,
  const claw::math::line_2d<coordinate_type>& dir ) const
{
  claw::math::line_2d<coordinate_type> edge;
  position_type inter;

  edge.origin = this_box.bottom_left();
  edge.direction.x = 1;
  edge.direction.y = 0;

  inter = dir.intersection( edge );

  that_new_box.top_right( inter );
} // align_bottom_left::align_bottom()

/*----------------------------------------------------------------------------*/
/**
 * \brief Align a box on the left.
 * \param this_box The box to which we will align the other.
 * \param that_old_pos The position from where comes the other box.
 * \param that_new_box (in/out) The position of the box we will align.
 * \param dir A line representing the movement of the top right corner of the
 *        other box.
 */
void bear::universe::align_bottom_left::align_left
( const rectangle_type& this_box, const position_type& that_old_pos,
  rectangle_type& that_new_box,
  const claw::math::line_2d<coordinate_type>& dir ) const
{
  claw::math::line_2d<coordinate_type> edge;
  position_type inter;

  edge.origin = this_box.top_left();
  edge.direction.x = 0;
  edge.direction.y = 1;

  inter = edge.intersection( dir );

  that_new_box.top_right( inter );
} // align_bottom_left::align_left()

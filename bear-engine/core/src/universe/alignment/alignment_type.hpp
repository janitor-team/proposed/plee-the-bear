/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file alignment_type.hpp
 * \brief This is tool class that helps us to get the alignment class
 *        corresponding to a given zone.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_ALIGNMENT_TYPE_HPP__
#define __UNIVERSE_ALIGNMENT_TYPE_HPP__

#include "universe/zone.hpp"
#include "universe/alignment/align_top_left.hpp"
#include "universe/alignment/align_top.hpp"
#include "universe/alignment/align_top_right.hpp"
#include "universe/alignment/align_left.hpp"
#include "universe/alignment/align_right.hpp"
#include "universe/alignment/align_bottom_left.hpp"
#include "universe/alignment/align_bottom.hpp"
#include "universe/alignment/align_bottom_right.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief This is tool class that helps us to get the alignment class
     *        corresponding to a given zone.
     * \author Julien Jorge
     */
    template<zone::position Z>
    class alignment_type
    {
      // nothing
    }; // class alignment_type

    /**
     * \brief Specialization for the top left zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::top_left_zone>
    {
    public:
      typedef align_top_left type;
    }; // class alignment_type [top_left_zone]

    /**
     * \brief Specialization for the top zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::top_zone>
    {
    public:
      typedef align_top type;
    }; // class alignment_type [top_zone]

    /**
     * \brief Specialization for the top right zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::top_right_zone>
    {
    public:
      typedef align_top_right type;
    }; // class alignment_type [top_right_zone]

    /**
     * \brief Specialization for the middle left zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::middle_left_zone>
    {
    public:
      typedef align_left type;
    }; // class alignment_type [middle_left_zone]

    /**
     * \brief Specialization for the middle right zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::middle_right_zone>
    {
    public:
      typedef align_right type;
    }; // class alignment_type [middle_right_zone]

    /**
     * \brief Specialization for the bottom left zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::bottom_left_zone>
    {
    public:
      typedef align_bottom_left type;
    }; // class alignment_type [bottom_left_zone]

    /**
     * \brief Specialization for the bottom zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::bottom_zone>
    {
    public:
      typedef align_bottom type;
    }; // class alignment_type [bottom_zone]

    /**
     * \brief Specialization for the bottom right zone.
     * \author Julien Jorge
     */
    template<>
    class alignment_type<zone::bottom_right_zone>
    {
    public:
      typedef align_bottom_right type;
    }; // class alignment_type [bottom_right_zone]

  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_ALIGNMENT_TYPE_HPP__

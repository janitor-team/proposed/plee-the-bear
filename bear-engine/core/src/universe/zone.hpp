/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file zone.hpp
 * \brief Watching a rectangle placed in a 2D space, we can consider nine zones,
 *        described here.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_ZONE_HPP__
#define __UNIVERSE_ZONE_HPP__

#include "universe/types.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief Watching a rectangle placed in a 2D space, we can consider nine
     *        zones, described here.
     */
    class UNIVERSE_EXPORT zone
    {
    public:
      enum position
        {
          top_left_zone = 0,
          top_zone,
          top_right_zone,
          middle_left_zone,
          middle_zone,
          middle_right_zone,
          bottom_left_zone,
          bottom_zone,
          bottom_right_zone
        }; // enum position

      /** \brief Number of zones considered. */
      static const unsigned int cardinality;

      static position find
      ( const rectangle_type& that_box, const rectangle_type& this_box );

      static position opposite_of( position p );

    }; // class zone
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_ZONE_HPP__

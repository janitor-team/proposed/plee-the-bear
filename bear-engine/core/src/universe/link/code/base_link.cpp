/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_link.cpp
 * \brief Implementation of the bear::universe::base_link class.
 * \author Julien Jorge
 */
#include "universe/link/base_link.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
std::size_t bear::universe::base_link::base_link::not_an_id(0);
std::size_t bear::universe::base_link::base_link::s_next_id(1);

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param first_item The first linked item.
 * \param second_item The second linked item.
 * \pre \a first_item and \a second_item are distinct.
 */
bear::universe::base_link::base_link
( physical_item& first_item, physical_item& second_item )
  : m_first_item(&first_item), m_second_item(&second_item), m_id(s_next_id++)
{
  CLAW_PRECOND( &first_item != &second_item );

  m_first_item->add_link(*this);
  m_second_item->add_link(*this);
} // base_link::base_link()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::universe::base_link::~base_link()
{
  unlink();
} // base_link::~base_link()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the identifier of the link.
 */
std::size_t bear::universe::base_link::get_id() const
{
  return m_id;
} // base_link::get_id()

/*----------------------------------------------------------------------------*/
/**
 * \brief Unlink the two items.
 */
void bear::universe::base_link::unlink()
{
  if (m_first_item)
    {
      m_first_item->remove_link(*this);
      m_first_item = NULL;
    }

  if (m_second_item)
    {
      m_second_item->remove_link(*this);
      m_second_item = NULL;
    }
} // base_link::unlink()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the first item concerned by this link.
 */
const bear::universe::physical_item&
bear::universe::base_link::get_first_item() const
{
  return *m_first_item;
} // base_link::get_first_item()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the second item concerned by this link.
 */
const bear::universe::physical_item&
bear::universe::base_link::get_second_item() const
{
  return *m_second_item;
} // base_link::get_second_item()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file link.cpp
 * \brief Implementation of the bear::universe::link class.
 * \author Julien Jorge
 */
#include "universe/link/link.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param first_item The first linked item.
 * \param second_item The second linked item.
 * \param strength The strength of the elastic.
 * \param length The length of the elastic.
 */
bear::universe::link::link
( physical_item& first_item, physical_item& second_item,
  double strength, bear::universe::coordinate_type minimal_length,
  bear::universe::coordinate_type maximal_length )
  : base_link(first_item, second_item), m_strength(strength),
    m_minimal_length(minimal_length), m_maximal_length(maximal_length)
{

} // link::link()

/*----------------------------------------------------------------------------*/
/**
 * \brief Update the forces applied to each item.
 */
void bear::universe::link::adjust()
{
  force_type dir( m_second_item->get_center_of_mass(),
                  m_first_item->get_center_of_mass() );

  double d = dir.length();
  double delta(0);

  if (d > m_maximal_length)
    delta = d - m_maximal_length;
  else if (d < m_minimal_length)
    delta = d - m_minimal_length; // negative value to move the items apart

  dir.normalize();
  dir *= m_strength * delta / d;

  m_first_item->add_external_force(-dir);
  m_second_item->add_external_force(dir);
} // link::adjust()

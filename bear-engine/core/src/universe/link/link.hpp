/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file link.hpp
 * \brief This class is an elastic link between two items.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_LINK_HPP__
#define __UNIVERSE_LINK_HPP__

#include "universe/link/base_link.hpp"

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief This class is an elastic link between two items.
     *
     * An link makes hard to move two items away from each other.
     *
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT link:
      public base_link
    {
    public:
      link( physical_item& first_item, physical_item& second_item,
            double strength,
            universe::coordinate_type minimal_length,
            universe::coordinate_type maximal_length );

      virtual void adjust();

    private:
      /** \brief The strength of the elastic. */
      const double m_strength;

      /** \brief The minimum length of the link. */
      const universe::coordinate_type m_minimal_length;

      /** \brief The maximum length of the link. */
      const universe::coordinate_type m_maximal_length;
    }; // class link
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_LINK_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_link.hpp
 * \brief Base class for links between items.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_BASE_LINK_HPP__
#define __UNIVERSE_BASE_LINK_HPP__

#include "universe/physical_item.hpp"
#include <claw/non_copyable.hpp>

#include "universe/class_export.hpp"

namespace bear
{
  namespace universe
  {
    /**
     * \brief Base class for links between items.
     *
     * A link is an abstract entity representing an interaction between two
     * items. For example:
     *  - an elastic connecting two items. It's hard for an item to move from
     *    away from the other.
     *  - a magnetic field attract two items.
     *
     * \author Julien Jorge
     */
    class UNIVERSE_EXPORT base_link:
      public claw::pattern::non_copyable
    {
    public:
      base_link( physical_item& first_item, physical_item& second_item );
      virtual ~base_link();

      virtual void adjust() = 0;

      std::size_t get_id() const;

      void unlink();

      const physical_item& get_first_item() const;
      const physical_item& get_second_item() const;

    public:
      /** \brief An incorrect identifier for the links. */
      static std::size_t not_an_id;

    protected:
      /** \brief One of the linked items. */
      physical_item* m_first_item;

      /** \brief The second linked item. */
      physical_item* m_second_item;

    private:
      /** \brief The identifier of the link. */
      std::size_t m_id;

      /** \brief The next identifier for the links. */
      static std::size_t s_next_id;

    }; // class base_link
  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_BASE_LINK_HPP__

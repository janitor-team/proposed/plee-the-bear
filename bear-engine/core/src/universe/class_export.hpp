/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file class_export.hpp
 * \brief Definition of the UNIVERSE_EXPORT macro according to the fact that we
 *        create the library or link to it.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_CLASS_EXPORT_HPP__
#define __UNIVERSE_CLASS_EXPORT_HPP__

#ifndef UNIVERSE_EXPORT
#  ifdef _WIN32
#    ifdef bear_universe_EXPORTS
#      define UNIVERSE_EXPORT __declspec(dllexport)
#    else
#      define UNIVERSE_EXPORT __declspec(dllimport)
#    endif // def bear_universe_EXPORTS
#  else // def _WIN32
#    define UNIVERSE_EXPORT
#  endif // def _WIN32
#endif // ndef UNIVERSE_EXPORT

#endif // __UNIVERSE_CLASS_EXPORT_HPP__

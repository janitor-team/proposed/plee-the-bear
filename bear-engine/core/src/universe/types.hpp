/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file types.hpp
 * \brief Data types for the universe:: namespace.
 * \author Julien Jorge
 */
#ifndef __UNIVERSE_TYPES_HPP__
#define __UNIVERSE_TYPES_HPP__

#include <claw/vector_2d.hpp>
#include <claw/rectangle.hpp>

namespace bear
{
  namespace universe
  {
    /** \brief The type of the vectors. */
    typedef claw::math::vector_2d<double> vector_type;

    /** \brief The type of the vectors used for representing forces. */
    typedef vector_type force_type;

    /** \brief The type of the vectors used for representing speed. */
    typedef vector_type speed_type;

    /** \brief The type of a coordinate. */
    typedef double coordinate_type;

    /** \brief The type of the vectors used for representing coordinates. */
    typedef claw::math::coordinate_2d<coordinate_type> position_type;

    /** \brief The type of the rectangles. */
    typedef claw::math::box_2d<coordinate_type> rectangle_type;

    /** \brief The type of a size. */
    typedef double size_type;

    /** \brief The type of a box size (width and height). */
    typedef claw::math::coordinate_2d<size_type> size_box_type;

    /** \brief Type used for representing time. */
    typedef double time_type;

  } // namespace universe
} // namespace bear

#endif // __UNIVERSE_TYPES_HPP__

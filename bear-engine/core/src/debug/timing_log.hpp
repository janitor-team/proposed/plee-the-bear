/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file timing_log.hpp
 * \brief This class keep traces of different durations and output the total
 *        and average durations.
 * \author Julien Jorge
 */
#ifndef __DEBUG_TIMING_LOG_HPP__
#define __DEBUG_TIMING_LOG_HPP__

#include "time/time.hpp"

#include "debug/class_export.hpp"

#include <string>
#include <list>

namespace bear
{
  namespace debug
  {
    /**
     * \brief This class keep traces of different durations and output the total
     *        and average durations.
     * \author Julien Jorge
     */
    class DEBUG_EXPORT timing_log
    {
    public:
      timing_log
      ( const std::string& name, systime::milliseconds_type avg_interval );
      ~timing_log();

      void start();
      void stop();

    private:
      /** \brief The durations in m_avg_interval interval of time. */
      std::list<double> m_duration;

      /** \brief The name of this timer. */
      const std::string m_name;

      /** \brief The last time saved by a call to start(). */
      systime::milliseconds_type m_last_date;

      /** \brief The count of measures since the begining. */
      unsigned int m_duration_counter;

      /** \brief The sum of measures since the begining. */
      systime::milliseconds_type m_duration_sum;

      /** \brief The duration of the time intervals in which we compute the
          average duration. */
      systime::milliseconds_type m_avg_interval;

      /** \brief The date of the last computation of an average time. */
      systime::milliseconds_type m_last_avg;

      /** \brief The count of measures since the last computation of an average
          time. */
      unsigned int m_avg_counter;

      /** \brief The sum of measures since the last computation of an average
          time. */
      systime::milliseconds_type m_avg_sum;

    }; // class timing_log
  } // namespace debug
} // namespace bear

#ifdef BEAR_PROFILE
  /**
   * \brief Create a static timer.
   * \param name The name of the timer.
   * \param d The duration of the time window in which we compute the average
   *        duration.
   */
  #define BEAR_CREATE_TIMELOG( name, d )                            \
    static bear::debug::timing_log name ## _time_log( #name, d )

  /**
   * \brief Start a timer with a given name.
   * \param name The name of the timer.
   */
  #define BEAR_START_TIMELOG( name ) name ## _time_log.start()

  /**
   * \brief Stop a timer with a given name.
   * \param name The name of the timer.
   */
  #define BEAR_STOP_TIMELOG( name ) name ## _time_log.stop()
#else // def BEAR_PROFILE
  #define BEAR_CREATE_TIMELOG( name, d )
  #define BEAR_START_TIMELOG( name )
  #define BEAR_STOP_TIMELOG( name )
#endif // def BEAR_PROFILE

#endif // __DEBUG_TIMING_LOG_HPP__

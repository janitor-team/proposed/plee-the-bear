/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file callback_function.hpp
 * \brief A callback that calls a given function.
 * \author Julien Jorge
 */
#ifndef __GUI_CALLBACK_FUNCTION_HPP__
#define __GUI_CALLBACK_FUNCTION_HPP__

#include "gui/base_callback.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief A callback that calls a given function.
     * \author Julien Jorge
     */
    template<typename F>
    class callback_function:
      public base_callback
    {
    public:
      callback_function( const F& f );

      callback_function<F>* clone() const;

      void execute();

    private:
      /** \brief The function called by this callback. */
      F m_function;

    }; // class callback_function

    /**
     * \brief A function to ease the creation of a callback_function.
     * \param f The function called by the callback.
     */
    template<typename F>
    callback_function<F> callback_function_maker( const F& f )
    {
      return callback_function<F>(f);
    } // callback_function_maker()

  } // namespace gui
} // namespace bear

#include "gui/impl/callback_function.tpp"

#endif // __GUI_CALLBACK_FUNCTION_HPP__

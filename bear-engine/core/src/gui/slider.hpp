/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file slider.hpp
 * \brief A slider.
 * \author Julien Jorge
 */
#ifndef __GUI_SLIDER_HPP__
#define __GUI_SLIDER_HPP__

#include "gui/callback_group.hpp"
#include "gui/visual_component.hpp"

#include "visual/sprite.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief A slider.
     * \author Julien Jorge
     */
    template<typename T>
    class slider:
      public visual_component
    {
    public:
      slider( const visual::sprite& bar, const visual::sprite& slider,
              T min, T max, T value );
      slider( const visual::sprite& bar, const visual::sprite& slider,
              T min, T max, T value, const callback& value_changed );

      void set_delta( T v );
      void set_value( T v );
      T get_value() const;

    private:
      void display( std::list<visual::scene_element>& e ) const;

      void set_slider_at( unsigned int x );

      void on_resized();
      bool on_key_press( const input::key_info& key );
      bool on_button_press
        ( input::joystick::joy_code button, unsigned int joy_index );
      bool on_mouse_press
      ( input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );
      bool on_mouse_released
      ( input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );
      bool on_mouse_maintained
      ( input::mouse::mouse_code button,
        const claw::math::coordinate_2d<unsigned int>& pos );

    private:
      /** \brief The bar on which the slider slides. */
      visual::sprite m_bar;

      /** \brief The slider. */
      visual::sprite m_slider;

      /** \brief The minimum value. */
      const T m_min;

      /** \brief The maximum value. */
      const T m_max;

      /** \brief The current value. */
      T m_value;

      /** \brief The delta applied to the value. */
      T m_delta;

      /** \brief Callback executed when the value changed. */
      callback_group m_value_changed_callback;

    }; // class slider
  } // namespace gui
} // namespace bear

#include "gui/impl/slider.tpp"

#endif // __GUI_SLIDER_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file checkbox.hpp
 * \brief A checkbox with a text.
 * \author Julien Jorge
 */
#ifndef __GUI_CHECKBOX_HPP__
#define __GUI_CHECKBOX_HPP__

#include "gui/checkable.hpp"

#include "gui/class_export.hpp"

#include "visual/font.hpp"
#include "visual/sprite.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief A checkbox with a text.
     * \author Julien Jorge
     */
    class GUI_EXPORT checkbox:
      public checkable
    {
    public:
      checkbox( const visual::sprite& off, const visual::sprite& on );
      checkbox
        ( const visual::sprite& off, const visual::sprite& on, font_type f );

    private:
      bool on_key_press( const input::key_info& key );
      bool on_button_press
        ( input::joystick::joy_code button, unsigned int joy_index );
      bool on_mouse_press
        ( input::mouse::mouse_code key,
          const claw::math::coordinate_2d<unsigned int>& pos );

    }; // class checkbox
  } // namespace gui
} // namespace bear

#endif // __GUI_CHECKBOX_HPP__

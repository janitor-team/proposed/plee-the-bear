/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file picture.hpp
 * \brief A simple component to display any visual::scene_element
 *        in windows or any visual component.
 * \author Julien Jorge
 */
#ifndef __GUI_SCENE_ELEMENT_HPP__
#define __GUI_SCENE_ELEMENT_HPP__

#include "gui/visual_component.hpp"
#include "visual/scene_element.hpp"

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief A simple component to display any visual::scene_element
     *        in windows or any visual component.
     * \author Julien Jorge
     */
    class GUI_EXPORT scene_element:
      public visual_component
    {
    public:
      explicit scene_element
      ( const visual::scene_element& e = visual::scene_element() );

      void set_scene_element( const visual::scene_element& e );

    private:
      void display( scene_element_list& e ) const;

      virtual void on_resized();

      void stretch_element();

    private:
      /** \brief The element to render. */
      visual::scene_element m_element;

      /** \brief Tell if the element is stretched to fill the control. */
      bool m_stretch;

    }; // class scene_element
  } // namespace gui
} // namespace bear

#endif // __GUI_SCENE_ELEMENT_HPP__

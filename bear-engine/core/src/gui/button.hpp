/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file button.hpp
 * \brief A button on which the user click to produce an action.
 * \author Julien Jorge
 */
#ifndef __GUI_BUTTON_HPP__
#define __GUI_BUTTON_HPP__

#include "gui/callback_group.hpp"
#include "gui/visual_component.hpp"

#include "gui/class_export.hpp"

#include "visual/font.hpp"

namespace bear
{
  namespace gui
  {
    class static_text;

    /**
     * \brief A button on which the user click to produce an action.
     * \author Julien Jorge
     */
    class GUI_EXPORT button:
      public visual_component
    {
    public:
      /** \brief The type of the font used to display the text. */
      typedef visual::font font_type;

    public:
      button( const font_type& f, const std::string& label );
      button( const font_type& f, const std::string& label, const callback& c );

      void set_font( font_type f );
      void set_margin( size_type m );
      void set_text( const std::string& text );

      const std::string& get_text() const;

      void add_callback( const callback& c );

    private:
      void create();

      bool on_key_press( const input::key_info& key );
      bool on_button_press
        ( input::joystick::joy_code joy_button, unsigned int joy_index );
      bool on_mouse_press
        ( input::mouse::mouse_code key,
          const claw::math::coordinate_2d<unsigned int>& pos );

    private:
      /** \brief The text in the button. */
      static_text* m_text;

      /** \brief Callback executed when the button is clicked. */
      callback_group m_click_callback;

      /** \brief The size of the margin around the text of the button. */
      size_type m_margin;

    }; // class button
  } // namespace gui
} // namespace bear

#endif // __GUI_BUTTON_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file picture.hpp
 * \brief A simple component to display an image (sprite) in windows or any
 *        visual component.
 * \author Julien Jorge
 */
#ifndef __GUI_PICTURE_HPP__
#define __GUI_PICTURE_HPP__

#include "gui/scene_element.hpp"
#include "visual/sprite.hpp"

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief A simple component to display an image (sprite) in windows or any
     *        visual component.
     * \author Julien Jorge
     */
    class GUI_EXPORT picture:
      public scene_element
    {
    public:
      explicit picture
      ( const visual::sprite& pict = visual::sprite() );

      void set_picture( const visual::sprite& pict );

    }; // class picture
  } // namespace gui
} // namespace bear

#endif // __GUI_PICTURE_HPP__

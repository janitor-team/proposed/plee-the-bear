/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file horizontal_flow.hpp
 * \brief A horizontal flow displays his children from the left to the right and
 *        from the top to the bottom, keeping them aligned on a grid.
 * \author Julien Jorge
 */
#ifndef __GUI_HORIZONTAL_FLOW_HPP__
#define __GUI_HORIZONTAL_FLOW_HPP__

#include "gui/visual_component.hpp"

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief A horizontal flow displays his children from the left to the right
     *        and from the top to the bottom, keeping them aligned on a grid.
     * \author Julien Jorge
     */
    class GUI_EXPORT horizontal_flow:
      public visual_component
    {
    public:
      explicit horizontal_flow
        ( size_type horizontal_margin = 0, size_type vertical_margin = 0 );

      void set_horizontal_margin( size_type m );
      void set_vertical_margin( size_type m );
      void set_margins( size_type horizontal, size_type vertical );

    private:
      virtual void on_resized();
      virtual void on_child_inserted( visual_component* child );
      virtual void on_child_removed( visual_component* child );

      void adjust_children_positions() const;

    private:
      /** \brief The horizontal margin between the components. */
      size_type m_horizontal_margin;

      /** \brief The vertical margin between the components. */
      size_type m_vertical_margin;

    }; // class horizontal_flow
  } // namespace gui
} // namespace bear

#endif // __GUI_HORIZONTAL_FLOW_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file types.hpp
 * \brief Data types for the gui:: namespace.
 * \author Julien Jorge
 */
#ifndef __GUI_TYPES_HPP__
#define __GUI_TYPES_HPP__

#include "visual/types.hpp"

namespace bear
{
  namespace gui
  {
    /** \brief The type of a coordinate. */
    typedef visual::coordinate_type coordinate_type;

    /** \brief The type of the vectors used for representing coordinates. */
    typedef visual::position_type position_type;

    /** \brief The type of a size. */
    typedef visual::size_type size_type;

    /** \brief The type of a box size (width and height). */
    typedef visual::size_box_type size_box_type;

    /** \brief The type of a rectangle. */
    typedef visual::rectangle_type rectangle_type;

    /** \brief A color. */
    typedef visual::color_type color_type;

  } // namespace gui
} // namespace bear

#endif // __GUI_TYPES_HPP__

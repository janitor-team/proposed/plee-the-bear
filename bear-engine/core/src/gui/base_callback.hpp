/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file base_callback.hpp
 * \brief Base class for the callbacks of the controls.
 * \author Julien Jorge
 */
#ifndef __GUI_BASE_CALLBACK_HPP__
#define __GUI_BASE_CALLBACK_HPP__

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief Base class for the callbacks of the controls.
     * \author Julien Jorge
     */
    class GUI_EXPORT base_callback
    {
    public:
      virtual ~base_callback() {}

      virtual base_callback* clone() const = 0;

      /**
       * \brief Execute the callback.
       */
      virtual void execute() = 0;

    }; // class base_callback
  } // namespace gui
} // namespace bear

#endif // __GUI_BASE_CALLBACK_HPP__

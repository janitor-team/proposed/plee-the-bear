/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file callback_function.tpp
 * \brief Implementation of the bear::gui::callback_function class.
 * \author Julien Jorge
 */

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param f The function called by this callback.
 */
template<typename F>
bear::gui::callback_function<F>::callback_function( const F& f )
  : m_function(f)
{

} // callback_function::callback_function()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this callback.
 */
template<typename F>
bear::gui::callback_function<F>* bear::gui::callback_function<F>::clone() const
{
  return new callback_function<F>(*this);
} // callback_function::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Execute the callback.
 */
template<typename F>
void bear::gui::callback_function<F>::execute()
{
  m_function();
} // callback_function::execute()

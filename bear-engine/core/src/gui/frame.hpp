/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file frame.hpp
 * \brief A frame: a box with a border and a background.
 * \author Julien Jorge
 */
#ifndef __GUI_FRAME_HPP__
#define __GUI_FRAME_HPP__

#include "gui/visual_component.hpp"
#include "visual/writing.hpp"

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief A frame: a box with a border and a background.
     * \author Julien Jorge
     */
    class GUI_EXPORT frame:
      public visual_component
    {
    public:
      frame();
      explicit frame( const std::string& title );
      frame( const std::string& title, const visual::font& f,
             visual::size_type font_size = 0 );

      void set_title( const std::string& t );
      const std::string& get_title() const;

      void set_font( const visual::font& f );
      const visual::font& get_font() const;

      void set_font_size( visual::size_type s );
      visual::size_type get_font_size() const;

      bool close();

    protected:
      virtual bool on_close();

      visual_component& get_content();
      void fit( size_type margin = 10 );
      void display( scene_element_list& e ) const;

    private:
      void on_resized();
      visual::size_type compute_title_height() const;
      void update_displayed_title();

    private:
      /** \brief The component in this frame. */
      visual_component* m_content_frame;

      /** \brief The title of the frame. */
      std::string m_title;

      /** \brief The font used to display the title of the frame. */
      visual::font m_font;

      /** \brief The size of the font. */
      visual::size_type m_font_size;

      /** \brief The title as displayed. */
      visual::writing m_displayed_title;

    }; // class frame
  } // namespace gui
} // namespace bear

#endif // __GUI_FRAME_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file gui/radio_button.cpp
 * \brief Implementation of the gui::radio_button class.
 * \author Julien Jorge
 */
#include "gui/radio_button.hpp"

#include "input/keyboard.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param off The sprite displayed when the box is not checked.
 * \param on The sprite displayed when the box is checked.
 */
bear::gui::radio_button::radio_button
( const visual::sprite& off, const visual::sprite& on )
  : checkable(off, on)
{

} // radio_button::radio_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param off The sprite displayed when the box is not checked.
 * \param on The sprite displayed when the box is checked.
 * \param f The font used to display the text.
 */
bear::gui::radio_button::radio_button
( const visual::sprite& off, const visual::sprite& on, font_type f )
  : checkable(off, on, f)
{

} // radio_button::radio_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell that a key has been pressed.
 * \param key The code of the key.
 */
bool bear::gui::radio_button::on_key_press( const bear::input::key_info& key )
{
  bool result = true;

  if ( key.is_enter() || (key.get_code() == input::keyboard::kc_space) )
    check();
  else
    result = false;

  return result;
} // radio_button::on_key_press()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell that a joystick button has been pressed.
 * \param button The code of the button.
 * \param joy_index The index of the joytick.
 */
bool bear::gui::radio_button::on_button_press
( bear::input::joystick::joy_code button, unsigned int joy_index )
{
  bool result = true;

  switch( button )
    {
    case bear::input::joystick::jc_button_1:
    case bear::input::joystick::jc_button_2:
    case bear::input::joystick::jc_button_3:
    case bear::input::joystick::jc_button_4:
    case bear::input::joystick::jc_button_5:
    case bear::input::joystick::jc_button_6:
    case bear::input::joystick::jc_button_7:
    case bear::input::joystick::jc_button_8:
    case bear::input::joystick::jc_button_9:
    case bear::input::joystick::jc_button_10:
    case bear::input::joystick::jc_button_11:
    case bear::input::joystick::jc_button_12:
    case bear::input::joystick::jc_button_13:
    case bear::input::joystick::jc_button_14:
    case bear::input::joystick::jc_button_15:
    case bear::input::joystick::jc_button_16:
      check();
      break;
    default:
      result = false;
    }

  return result;
} // radio_button::on_button_press()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell that a mouse button has been pressed.
 * \param key The code of the button.
 * \param pos The position of the mouse.
 */
bool bear::gui::radio_button::on_mouse_press
( input::mouse::mouse_code key,
  const claw::math::coordinate_2d<unsigned int>& pos )
{
  check();
  return true;
} // radio_button::on_mouse_press()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file horizontal_flow.cpp
 * \brief Implementation of the bear::gui::horizontal_flow class.
 * \author Julien Jorge
 */
#include "gui/horizontal_flow.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param horizontal_margin The horizontal margin between the children
 *        components.
 * \param vertical_margin The vertical margin between the children components.
 */
bear::gui::horizontal_flow::horizontal_flow
( size_type horizontal_margin, size_type vertical_margin )
  : visual_component(), m_horizontal_margin(horizontal_margin),
    m_vertical_margin(vertical_margin)
{
  // nothing to do
} // horizontal_flow::horizontal_flow()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the horizontal margin between the children components.
 * \param m The horizontal margin between the children components.
 */
void bear::gui::horizontal_flow::set_horizontal_margin( size_type m )
{
  set_margins( m, m_vertical_margin );
} // horizontal_flow::set_horizontal_margin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the vertical margin between the children components.
 * \param m The vertical margin between the children components.
 */
void bear::gui::horizontal_flow::set_vertical_margin( size_type m )
{
  set_margins( m_horizontal_margin, m );
} // horizontal_flow::set_vertical_margin()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the margins between the children components.
 * \param horizontal The horizontal margin between the chidren components.
 * \param vertical The vertical margin between the children components.
 */
void bear::gui::horizontal_flow::set_margins
( size_type horizontal, size_type vertical )
{
  m_horizontal_margin = horizontal;
  m_vertical_margin = vertical;
  adjust_children_positions();
} // horizontal_flow::set_margins()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called after the component has been resized. The position of
 *        the children are updated to fill the component.
 */
void bear::gui::horizontal_flow::on_resized()
{
  adjust_children_positions();
} // horizontal_flow::on_resized()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called after the insertion of a child.
 * \param child The child inserted in this component.
 */
void bear::gui::horizontal_flow::on_child_inserted( visual_component* child )
{
  adjust_children_positions();
} // horizontal_flow::on_child_inserted()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called after the removal of a child.
 * \param child The child removed from this component.
 */
void bear::gui::horizontal_flow::on_child_removed( visual_component* child )
{
  adjust_children_positions();
} // horizontal_flow::on_child_removed()

/*----------------------------------------------------------------------------*/
/**
 * \brief Adjust he position of the children to fill the component.
 */
void bear::gui::horizontal_flow::adjust_children_positions() const
{
  iterator first=begin();
  coordinate_type top = height() - m_vertical_margin;

  while ( first != end() )
    {
      bool stop = false;
      size_type line_width = 2 * m_horizontal_margin;
      size_type line_height = 0;
      iterator last = first;

      while ( !stop && last!=end() )
        if ( line_width + last->width() > width() )
          stop = true;
        else
          {
            line_width += last->width() + m_horizontal_margin;
            line_height = std::max( line_height, last->height() );
            ++last;
          }

      if ( top < line_height ) // no more child will be visible.
        for ( ; first!=end(); ++first )
          first->set_visible(false);
      else
        {
          coordinate_type x = m_horizontal_margin;
          for ( ; first!=last; ++first )
            {
              first->set_visible(true);
              first->set_position
                ( x, top - line_height + (line_height - first->height()) / 2 );
              x += first->width() + m_horizontal_margin;
            }
        }

      top -= line_height + m_vertical_margin;
    }
} // horizontal_flow::adjust_children_positions()

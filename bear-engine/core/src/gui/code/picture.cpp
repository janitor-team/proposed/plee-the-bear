/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file picture.cpp
 * \brief Implementation of the bear::gui::picture class.
 * \author Julien Jorge
 */
#include "gui/picture.hpp"

#include "visual/scene_sprite.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param pict The sprite to display.
 */
bear::gui::picture::picture( const visual::sprite& pict )
  : scene_element(visual::scene_sprite(0, 0, pict))
{

} // picture::picture()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the displayed picture.
 * \param pict The picture to display.
 */
void bear::gui::picture::set_picture( const visual::sprite& pict )
{
  set_scene_element( visual::scene_sprite(0, 0, pict) );
} // picture::set_picture()

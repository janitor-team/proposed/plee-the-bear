/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file scene_element.cpp
 * \brief Implementation of the bear::gui::scene_element class.
 * \author Julien Jorge
 */
#include "gui/scene_element.hpp"

#include <claw/assert.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param e The element to display.
 */
bear::gui::scene_element::scene_element( const visual::scene_element& e )
  : visual_component(), m_element(e)
{
  set_size( e.get_element_width(), e.get_element_height() );
} // scene_element::scene_element()

/*----------------------------------------------------------------------------*/
/**
 * \brief Set the displayed scene_element.
 * \param e The scene_element to display.
 */
void
bear::gui::scene_element::set_scene_element( const visual::scene_element& e )
{
  m_element = e;
  stretch_element();
} // scene_element::set_scene_element()

/*----------------------------------------------------------------------------*/
/**
 * \brief Get the scene elements of the component.
 * \param e (out) The scene elements.
 */
void bear::gui::scene_element::display
( std::list<visual::scene_element>& e ) const
{
  visual::scene_element elem(m_element);
  elem.set_position(left(), bottom());
  e.push_back(elem);
} // scene_element::display()

/*----------------------------------------------------------------------------*/
/**
 * \brief Method called after the component has been resized.
 */
void bear::gui::scene_element::on_resized()
{
  visual_component::on_resized();

  stretch_element();
} // visual_component::on_resized()

/*----------------------------------------------------------------------------*/
/**
 * \brief Make the scene element to fit the component
 */
void bear::gui::scene_element::stretch_element()
{
  double rx = width() / m_element.get_element_width();
  double ry = height() / m_element.get_element_height();

  m_element.set_scale_factor( std::min(rx, ry) );

  m_element.set_position
    ( (width() - m_element.get_width()) / 2,
      (height() - m_element.get_height()) / 2 );
} // visual_component::stretch_element()

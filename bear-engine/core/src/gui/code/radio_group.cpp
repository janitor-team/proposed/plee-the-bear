/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file radio_group.cpp
 * \brief Implementation of the bear::gui::radio_group class.
 * \author Julien Jorge
 */
#include "gui/radio_group.hpp"

#include "gui/callback_function.hpp"
#include "gui/radio_button.hpp"

#include <boost/bind.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a button in the group.
 * \param b The button to add.
 * \param margin The margin with the previous button in the group.
 */
void bear::gui::radio_group::add_button
( radio_button* b, visual::size_type margin )
{
  if ( m_buttons.empty() )
    b->set_bottom(margin);
  else
    b->set_bottom( m_buttons.back()->top() + margin );

  b->add_checked_callback
    ( callback_function_maker
      ( boost::bind( &radio_group::on_check, this, m_buttons.size() ) ) );
  m_buttons.push_back(b);
} // radio_group::add_button()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a button in the group.
 */

const bear::gui::radio_button* bear::gui::radio_group::get_selection() const
{
  for ( std::size_t i=0; i!=m_buttons.size(); ++i )
    if ( m_buttons[i]->checked() )
      return m_buttons[i];

  return NULL;
} // radio_group::get_selection( )

/*----------------------------------------------------------------------------*/
/**
 * \brief Answer to a click on a button of the group;
 * \param b The index of the selected button.
 */
void bear::gui::radio_group::on_check( std::size_t b ) const
{
  for ( std::size_t i=0; i!=m_buttons.size(); ++i )
    if ( b != i )
      m_buttons[i]->set_value(false);
} // radio_group::on_check()

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file callback_group.tpp
 * \brief Implementation of the bear::gui::callback_group class.
 * \author Julien Jorge
 */
#include "gui/callback_group.hpp"

/*----------------------------------------------------------------------------*/
/**
 * \brief Create a copy of this callback.
 */
bear::gui::callback_group* bear::gui::callback_group::clone() const
{
  return new callback_group(*this);
} // callback_group::clone()

/*----------------------------------------------------------------------------*/
/**
 * \brief Execute the callback.
 */
void bear::gui::callback_group::execute()
{
  for ( std::size_t i=0; i!=m_group.size(); ++i )
    m_group[i].execute();
} // callback_group::execute()

/*----------------------------------------------------------------------------*/
/**
 * \brief Add a callback in the group.
 * \param c The callback to add.
 */
void bear::gui::callback_group::add( const callback& c )
{
  m_group.push_back(c);
} // callback_group::execute()

/*----------------------------------------------------------------------------*/
/**
 * \brief Tell if there is no callback in the group.
 */
bool bear::gui::callback_group::empty() const
{
  return m_group.empty();
} // callback_group::empty()

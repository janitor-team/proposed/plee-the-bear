/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file text_input.hpp
 * \brief A component that display a modifiable text.
 * \author Julien Jorge
 */
#ifndef __GUI_TEXT_INPUT_HPP__
#define __GUI_TEXT_INPUT_HPP__

#include "gui/callback_group.hpp"
#include "gui/visual_component.hpp"
#include "input/keyboard.hpp"
#include "visual/font.hpp"

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    class static_text;

    /**
     * \brief A component that display a modifiable text.
     * \author Julien Jorge
     */
    class GUI_EXPORT text_input:
      public visual_component
    {
    public:
      /** \brief The type of the font used to display the text. */
      typedef visual::font font_type;

    public:
      text_input( font_type f, visual::color_type cursor_color );

      void clear();
      void set_text( const std::string& text );
      const std::string&  get_text() const;

      void add_enter_callback( const callback& c );

    private:
      bool on_key_press( const input::key_info& key );
      bool on_char_pressed( const input::key_info& key );

      void on_resized();

      void display( std::list<visual::scene_element>& e ) const;

      void insert_character( char key );
      bool special_code( const input::key_info& key );

      void move_left();
      void move_right();
      void adjust_text_by_left();
      void adjust_text_by_right();

      void adjust_visible_part_of_text();

    private:
      /** \brief A static text, used for displaying the visible part of the
          text. */
      static_text* m_static_text;

      /** \brief Position for next character. */
      unsigned int m_cursor;

      /** \brief The input text. */
      std::string m_text;

      /** \brief The color of the cursor. */
      visual::color_type m_cursor_color;

      /** \brief Position of the first visible character in the text. */
      std::string::size_type m_first;

      /** \brief Position of the last visible character in the text. */
      std::string::size_type m_last;

      /** \brief Maximum number of characters in the line. */
      std::string::size_type m_line_length;

      /** \brief Callback executed when enter is pressed. */
      callback_group m_enter_callback;

    }; // class text_input
  } // namespace gui
} // namespace bear

#endif // __GUI_TEXT_INPUT_HPP__

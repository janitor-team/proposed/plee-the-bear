/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file callback_group.hpp
 * \brief A callback that calls several callbacks.
 * \author Julien Jorge
 */
#ifndef __GUI_CALLBACK_GROUP_HPP__
#define __GUI_CALLBACK_GROUP_HPP__

#include "gui/base_callback.hpp"
#include "gui/callback.hpp"

#include <vector>

namespace bear
{
  namespace gui
  {
    class callback;

    /**
     * \brief A callback that calls several callbacks.
     * \author Julien Jorge
     */
    class GUI_EXPORT callback_group:
      public base_callback
    {
    public:
      callback_group* clone() const;

      void execute();

      void add( const callback& c );
      bool empty() const;

    private:
      /** \brief The group called by this callback. */
      std::vector<callback> m_group;

    }; // class callback_group

  } // namespace gui
} // namespace bear

#endif // __GUI_CALLBACK_GROUP_HPP__

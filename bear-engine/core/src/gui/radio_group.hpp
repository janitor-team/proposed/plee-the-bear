/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file radio_group.hpp
 * \brief A radio_group is a group of radio_button ensuring that at most one of
 *        them is checked at once.
 * \author Julien Jorge
 */
#ifndef __GUI_RADIO_GROUP_HPP__
#define __GUI_RADIO_GROUP_HPP__

#include "gui/visual_component.hpp"

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    class radio_button;

    /**
     * \brief A radio_group is a group of radio_button ensuring that at most one
     *        of them is checked at once.
     * \author Julien Jorge
     */
    class GUI_EXPORT radio_group:
      public visual_component
    {
    public:
      void add_button( radio_button* b, visual::size_type margin );
      const radio_button* get_selection() const;

    private:
      void on_check( std::size_t i ) const;

    private:
      /** \brief The buttons in this group. */
      std::vector<radio_button*> m_buttons;

    }; // class radio_group
  } // namespace gui
} // namespace bear

#endif // __GUI_RADIO_GROUP_HPP__

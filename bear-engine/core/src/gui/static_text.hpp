/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file static_text.hpp
 * \brief Simple component to print a text on the screen.
 * \author Julien Jorge
 */
#ifndef __GUI_STATIC_TEXT_HPP__
#define __GUI_STATIC_TEXT_HPP__

#include "gui/visual_component.hpp"
#include "visual/font.hpp"
#include "visual/writing.hpp"

#include <claw/coordinate_2d.hpp>

#include "gui/class_export.hpp"

namespace bear
{
  namespace gui
  {
    /**
     * \brief Simple component to print a text on the screen.
     * \author Julien Jorge
     */
    class GUI_EXPORT static_text:
      public visual_component
    {
    public:
      /** \brief The type of the font used to display the text. */
      typedef visual::font font_type;

    private:
      /** \brief The class passed to text_layout to compute the longest text
          that can be displayed. */
      class arrange_longest_text
      {
      public:
        arrange_longest_text( std::size_t& result );

        void operator()
          ( visual::coordinate_type x, visual::coordinate_type y,
            std::size_t first, std::size_t last );

      private:
        /** \brief The length of the text. */
        std::size_t& m_result;

      }; // class arrange_sprite_list

      /** \brief The class passed to text_layout to compute the size of the
          control with a maximum width. */
      class arrange_max_size
      {
      public:
        arrange_max_size
        ( const std::string& text, const visual::font& f,
          size_box_type& result );

        void operator()
          ( coordinate_type x, coordinate_type y, std::size_t first,
            std::size_t last );

      private:
        /** \brief The text to arrange. */
        std::string const& m_text;

        /** \brief The size of the control. */
        size_box_type& m_result;

        /** \brief The font used to display the text. */
        visual::font const& m_font;

        /** \brief The initial height of the control (text area). */
        const coordinate_type m_top;

      }; // class arrange_max_size

    public:
      static_text();
      explicit static_text( font_type f );

      void set_font( font_type f );
      void set_auto_size( bool b );
      void clear();

      void set_text( const std::string& text );

      const std::string& get_text() const;
      font_type get_font() const;

      void expand_vertically();

      std::size_t
      get_longest_text( const std::string& text, std::size_t i ) const;

      void set_margin( coordinate_type x, coordinate_type y );
      void set_margin( const size_box_type& m );

      size_type get_min_height_with_text() const;

    private:
      size_box_type get_auto_size_with_max_width( size_type w ) const;

      void display( std::list<visual::scene_element>& e ) const;

      void adjust_size_to_text();

      void on_resized();

      void refresh_writing();

    private:
      /** \brief The text to draw. */
      std::string m_text;

      /** \brief The font used to draw the text. */
      font_type m_font;

      /** \brief Tell if we must adjust the size of the component to the size of
          the text. */
      bool m_auto_size;

      /** \brief The text displayed. */
      visual::writing m_writing;

      /** \brief The margins around the text. */
      visual::size_box_type m_margin;

    }; // class static_text
  } // namespace gui
} // namespace bear

#endif // __GUI_STATIC_TEXT_HPP__

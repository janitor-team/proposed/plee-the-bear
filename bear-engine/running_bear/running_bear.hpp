/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file running_bear.hpp
 * \brief The main class of the application.
 * \author Julien Jorge
 */
#ifndef __BEAR_RUNNING_BEAR_HPP__
#define __BEAR_RUNNING_BEAR_HPP__

#include <claw/application.hpp>
#include "engine/game.hpp"

namespace bear
{
  /**
   * \brief The main class of the application.
   * \author Julien Jorge
   */
  class running_bear : public claw::application
  {
  public:
    running_bear( int& argc, char** &argv );
    virtual ~running_bear();

    virtual int run();

  private:
    void create_game( int& argc, char** &argv );
    void help() const;

  private:
    /** \brief The game we are running. */
    engine::game* m_game;

  }; // class running_bear
} // namespace bear

#endif // __BEAR_RUNNING_BEAR_HPP__

/*
  Bear Engine

  Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  contact: plee-the-bear@gamned.org

  Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file running_bear.cpp
 * \brief Implementation of the bear::running_bear class.
 * \author Julien Jorge
 */
#include "running_bear.hpp"

#include "bear_gettext.hpp"

#include <claw/logger.hpp>
#include <boost/preprocessor/stringize.hpp>

/*----------------------------------------------------------------------------*/
/**
 * \brief Constructor.
 * \param argc Number of program arguments.
 * \param argv Program arguments.
 */
bear::running_bear::running_bear( int& argc, char** &argv )
  : claw::application(argc, argv), m_game(NULL)
{
#ifdef BEAR_TEXT_DOMAIN_PATH
  bindtextdomain( "bear-engine", BOOST_PP_STRINGIZE(BEAR_TEXT_DOMAIN_PATH) );
#endif

  bind_textdomain_codeset( "bear-engine", "ISO-8859-15" );
  textdomain( "bear-engine" );

  m_arguments.add
    ("-h", "--help", bear_gettext("Print this help screen and exit."), true);
  m_arguments.parse(argc, argv);

  if ( m_arguments.get_bool("--help") )
    help();
  else
    create_game( argc, argv );
} // running_bear::running_bear()

/*----------------------------------------------------------------------------*/
/**
 * \brief Destructor.
 */
bear::running_bear::~running_bear()
{
  delete m_game;
} // running_bear::~running_bear()

/*----------------------------------------------------------------------------*/
/**
 * \brief Run the application.
 */
int bear::running_bear::run()
{
  try
    {
      if ( m_game != NULL )
        {
          m_game->run();
          delete m_game;
          m_game = NULL;
        }

      return EXIT_SUCCESS;
    }
  catch( std::exception& e )
    {
      claw::logger << claw::log_error << "(exception) " << e.what()
                   << std::endl;
      delete m_game;
      m_game = NULL;
      return EXIT_FAILURE;
    }
} // running_bear::run()

/*----------------------------------------------------------------------------*/
/**
 * \brief Create the game.
 * \param argc Number of program arguments.
 * \param argv Program arguments.
 */
void bear::running_bear::create_game( int& argc, char** &argv )
{
  try
    {
      m_game = new engine::game( argc, argv );
    }
  catch( std::exception& e )
    {
      std::cerr << "Exception: " << e.what() << std::endl;
      help();
    }
} // running_bear::create_game()

/*----------------------------------------------------------------------------*/
/**
 * \brief Print some help about the usage of the program.
 */
void bear::running_bear::help() const
{
  m_arguments.help( bear_gettext("engine_options") );
  std::cout << '\n';
  engine::game::print_help();
} // running_bear::help()

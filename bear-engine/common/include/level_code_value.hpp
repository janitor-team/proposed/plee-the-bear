/*
    Bear Engine - Level compiler

    Copyright (C) 20052011 Julien Jorge, Sebastien Angibaud

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

    contact: plee-the-bear@gamned.org

    Please add the tag [Bear] in the subject of your mails.
*/
/**
 * \file level_code_value.hpp
 * \brief Codes describing level parameters in a compiled level file.
 * \author Julien Jorge
 */
#ifndef __BEAR_LEVEL_CODE_VALUE_HPP__
#define __BEAR_LEVEL_CODE_VALUE_HPP__

namespace bear
{
  /**
   * \brief Codes describing level parameters in a compiled level file.
   */
  class level_code_value
  {
  public:
    /** \brief The type of the codes. */
    typedef unsigned int value_type;

  public:
    /** \brief End of file. */
    static const value_type eof = 0;

    /** \brief Declare that an item will be defined. */
    static const value_type item_declaration       = 30;

    /** \brief Defition of the fields of a declared item. */
    static const value_type item_definition        = 31;

    /** \brief Known class of type bear::engine::base_item. */
    static const value_type base_item              = 32;

    /** \brief Dynamic field of type integer. */
    static const value_type field_int       = 40;

    /** \brief Dynamic field of type value_typeeger. */
    static const value_type field_u_int     = 41;

    /** \brief Dynamic field of type real. */
    static const value_type field_real      = 42;

    /** \brief Dynamic field of type boolean. */
    static const value_type field_bool      = 43;

    /** \brief Dynamic field of type string. */
    static const value_type field_string    = 44;

    /** \brief Dynamic field of type sprite. */
    static const value_type field_sprite    = 45;

    /** \brief Dynamic field of type animation. */
    static const value_type field_animation = 46;

    /** \brief Dynamic field of type item. */
    static const value_type field_item      = 47;

    /** \brief Dynamic field of type item. */
    static const value_type field_sample    = 48;

    /** \brief Dynamic field taking a list of values. */
    static const value_type field_list      = 50;

    /** \brief Layer. */
    static const value_type layer           = 70;

  }; // class level_code_value
} // namespace bear

#endif // __BEAR_LEVEL_CODE_VALUE_HPP__
